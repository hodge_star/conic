/*
 * Copyright (c) 2019 Jack Poulson <jack@hodgestar.com>
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
#define CATCH_CONFIG_MAIN
#include <iostream>
#include <limits>
#include "catch2/catch.hpp"
#include "conic.hpp"
#include "quotient/timer.hpp"

// If defined, the tests are also run in double-double arithmetic.
#define TEST_DOUBLEDOUBLE

// Tests that the relative entrywise difference between a computed and
// expected vector are within the given tolerance.
template <typename Real>
void CompareVectors(
    const catamari::ConstBlasMatrixView<Real>& column_vector,
    const catamari::ConstBlasMatrixView<Real>& target_column_vector,
    Real relative_tolerance) {
  const conic::Int height = target_column_vector.height;
  REQUIRE(column_vector.height == height);
  REQUIRE(column_vector.width == 1);
  REQUIRE(target_column_vector.width == 1);

  for (conic::Int i = 0; i < height; ++i) {
    const Real value = column_vector(i);
    const Real target_value = target_column_vector(i);
    const Real relative_error =
        std::abs(value - target_value) / (std::abs(target_value) + 1);
    REQUIRE(relative_error <= relative_tolerance);
  }
}

// Tests that the relative entrywise difference between a computed and
// expected sparse matrix are within the given tolerance.
template <typename Real>
void CompareSparseMatrices(
    const catamari::CoordinateMatrix<Real>& matrix,
    const catamari::CoordinateMatrix<Real>& target_matrix,
    Real relative_tolerance) {
  REQUIRE(matrix.NumRows() == target_matrix.NumRows());
  REQUIRE(matrix.NumColumns() == target_matrix.NumColumns());
  REQUIRE(matrix.NumEntries() == target_matrix.NumEntries());
  const catamari::Buffer<catamari::MatrixEntry<Real>>& entries =
      matrix.Entries();
  const catamari::Buffer<catamari::MatrixEntry<Real>>& target_entries =
      target_matrix.Entries();
  for (conic::Int i = 0; i < conic::Int(entries.Size()); ++i) {
    const catamari::MatrixEntry<Real>& entry = entries[i];
    const catamari::MatrixEntry<Real>& target_entry = target_entries[i];
    REQUIRE(entry.row == target_entry.row);
    REQUIRE(entry.column == target_entry.column);
    const Real value = entry.value;
    const Real target_value = target_entry.value;
    const Real relative_error =
        std::abs(value - target_value) / (std::abs(target_value) + 1);
    REQUIRE(relative_error <= relative_tolerance);
  }
}

// Tests that the relative entrywise difference between the computed and
// expected definition of a standard-form quadratic program is below the
// given tolerance.
template <typename Real>
void CompareStandardFormProblems(
    const conic::hsd_qp::Problem<Real>& problem,
    const conic::hsd_qp::Problem<Real>& problem_target, Real objective_shift,
    Real objective_shift_target, Real relative_tolerance) {
  // Compare the objective shift.
  std::cout << "Testing objective shift." << std::endl;
  const Real relative_obj_error =
      std::abs(objective_shift - objective_shift_target) /
      (std::abs(objective_shift_target) + 1);
  REQUIRE(relative_obj_error <= relative_tolerance);

  // Compare the linear objective.
  std::cout << "Testing linear objective." << std::endl;
  CompareVectors(problem.linear_objective.ConstView(),
                 problem_target.linear_objective.ConstView(),
                 relative_tolerance);

  // Compare the quadratic objective.
  std::cout << "Testing quadratic objective." << std::endl;
  CompareSparseMatrices(problem.quadratic_objective,
                        problem_target.quadratic_objective, relative_tolerance);

  // Compare the equality matrix.
  std::cout << "Testing equality matrix." << std::endl;
  CompareSparseMatrices(problem.equality_matrix, problem_target.equality_matrix,
                        relative_tolerance);

  // Compare the equality right-hand side vector.
  std::cout << "Testing equality right-hand side." << std::endl;
  CompareVectors(problem.equality_rhs.ConstView(),
                 problem_target.equality_rhs.ConstView(), relative_tolerance);
}

// Tests that the relative entrywise difference between the computed and
// expected bounded-form iterates are within the given tolerance.
template <typename Real>
void CompareBoundedIterates(
    const conic::bounded_qp::Iterate<Real>& iterate,
    const conic::bounded_qp::Iterate<Real>& target_iterate,
    Real relative_tolerance) {
  std::cout << "Testing primal solution." << std::endl;
  CompareVectors(iterate.PrimalSolution(), target_iterate.PrimalSolution(),
                 relative_tolerance);
  std::cout << "Testing dual solution." << std::endl;
  CompareVectors(iterate.DualSolution(), target_iterate.DualSolution(),
                 relative_tolerance);
  std::cout << "Testing dual lower slack." << std::endl;
  CompareVectors(iterate.DualLowerSlack(), target_iterate.DualLowerSlack(),
                 relative_tolerance);
  std::cout << "Testing dual upper slack." << std::endl;
  CompareVectors(iterate.DualUpperSlack(), target_iterate.DualUpperSlack(),
                 relative_tolerance);
}

// We test the reduction of the bounded quadratic programming problem
//
//   arginf_x { c' x + (1 / 2) x' Q x : A x = b, l <= x <= u },
//
// into the form
//
//   arginf_x { c' x + (1 / 2) x' Q x : A x = b, x >= 0 },
//
// for the input matrices:
//
//       | 2 |       |  3, -1 | -1 |
//   c = | 3 |,  Q = | -1,  3 | -1 |,  A = | 1, 2 | 3 |,  b = | 5 |.
//       |---|       |--------|----|
//       | 4 |       | -1, -1 |  3 |
//
// and bounds, x_1, x_2 >= 0, x_3 = 6.
//
// The reduction of the original bounded problem by eliminating the fixed
// variable should be:
//
//   c = | -4 |,  Q = |  3, -1 |,  A = | 1, 2 |,  b = | -13 |,
//       | -3 |       | -1,  3 |
//
//   x >= 0,
//
// with an objective shift of
//
//   4 * 6 + (1 / 2) * 6^2 * 3 = 78.
//
template <typename Real>
void Test1(bool print_data, Real relative_tolerance) {
  // We begin by constructing the original bounded problem.
  const conic::Int num_primal_bounded = 3;
  const conic::Int num_dual_bounded = 1;
  const conic::Int num_fixed = 1;
  conic::bounded_qp::Problem<Real> bounded_problem;
  bounded_problem.num_fixed = num_fixed;

  // Construct the bounded linear objective,
  //
  //       | 2 |
  //   c = | 3 |.
  //       |---|
  //       | 4 |
  //
  bounded_problem.linear_objective.Resize(num_primal_bounded, 1);
  bounded_problem.linear_objective(0) = Real(2);
  bounded_problem.linear_objective(1) = Real(3);
  bounded_problem.linear_objective(2) = Real(4);

  // Construct the bounded quadratic objective,
  //
  //       |  3, -1 | -1 |
  //   Q = | -1,  3 | -1 |.
  //       |--------|----|
  //       | -1, -1 |  3 |
  //
  bounded_problem.quadratic_objective.Resize(num_primal_bounded,
                                             num_primal_bounded);
  bounded_problem.quadratic_objective.AddEntry(0, 0, Real(3));
  bounded_problem.quadratic_objective.AddEntry(0, 1, Real(-1));
  bounded_problem.quadratic_objective.AddEntry(0, 2, Real(-1));
  bounded_problem.quadratic_objective.AddEntry(1, 0, Real(-1));
  bounded_problem.quadratic_objective.AddEntry(1, 1, Real(3));
  bounded_problem.quadratic_objective.AddEntry(1, 2, Real(-1));
  bounded_problem.quadratic_objective.AddEntry(2, 0, Real(-1));
  bounded_problem.quadratic_objective.AddEntry(2, 1, Real(-1));
  bounded_problem.quadratic_objective.AddEntry(2, 2, Real(3));

  // Construct the bounded equality matrix,
  //
  //   A = | 1, 2 | 3 |.
  //
  bounded_problem.equality_matrix.Resize(num_dual_bounded, num_primal_bounded);
  bounded_problem.equality_matrix.AddEntry(0, 0, Real(1));
  bounded_problem.equality_matrix.AddEntry(0, 1, Real(2));
  bounded_problem.equality_matrix.AddEntry(0, 2, Real(3));

  // Construct the bounded equality right-hand side vector,
  //
  //   b = | 5 |.
  //
  bounded_problem.equality_rhs.Resize(num_dual_bounded, 1);
  bounded_problem.equality_rhs(0) = 5;

  // Construct the bounds.
  bounded_problem.lower_bounds.Resize(num_primal_bounded, Real(0));
  bounded_problem.lower_bounds[2] = Real(6);
  bounded_problem.upper_bounds.Resize(num_primal_bounded,
                                      std::numeric_limits<Real>::infinity());
  bounded_problem.upper_bounds[2] = Real(6);

  bounded_problem.FillNorms();
  if (print_data) {
    std::cout << "Bounded problem:\n" << bounded_problem << std::endl;
  }

  // We now construct the expected equivalent standard-form problem.
  const conic::Int num_primal_standard = 2;
  const conic::Int num_dual_standard = 1;
  conic::hsd_qp::Problem<Real> standard_problem_target;

  // We construct the standard linear objective,
  //
  //   c = | -4 |.
  //       | -3 |
  //
  standard_problem_target.linear_objective.Resize(num_primal_standard, 1);
  standard_problem_target.linear_objective(0) = Real(-4);
  standard_problem_target.linear_objective(1) = Real(-3);

  // We construct the standard quadratic objective,
  //
  //   Q = |  3, -1 |.
  //       | -1,  3 |
  //
  standard_problem_target.quadratic_objective.Resize(num_primal_standard,
                                                     num_primal_standard);
  standard_problem_target.quadratic_objective.AddEntry(0, 0, Real(3));
  standard_problem_target.quadratic_objective.AddEntry(0, 1, Real(-1));
  standard_problem_target.quadratic_objective.AddEntry(1, 0, Real(-1));
  standard_problem_target.quadratic_objective.AddEntry(1, 1, Real(3));

  // We construct the standard equality matrix,
  //
  //   A = | 1, 2 |.
  //
  standard_problem_target.equality_matrix.Resize(num_dual_standard,
                                                 num_primal_standard);
  standard_problem_target.equality_matrix.AddEntry(0, 0, Real(1));
  standard_problem_target.equality_matrix.AddEntry(0, 1, Real(2));

  // We construct the standard equality right-hand side vector,
  //
  //   b = | -13 |.
  //
  standard_problem_target.equality_rhs.Resize(num_dual_standard, 1);
  standard_problem_target.equality_rhs(0) = Real(-13);

  const Real objective_shift_target = Real(78);
  if (print_data) {
    std::cout << "Target reduced standard problem:\n"
              << standard_problem_target << std::endl;
    std::cout << "Target objective shift: " << objective_shift_target
              << std::endl;
  }

  conic::hsd_qp::Problem<Real> standard_problem;
  conic::BoundedToStandardQPReduction<Real> reduction(bounded_problem);
  reduction.FormStandardProblem(&standard_problem);
  const Real objective_shift = reduction.ObjectiveShift();
  if (print_data) {
    std::cout << "Reduced standard problem:\n" << standard_problem << std::endl;
    std::cout << "Objective shift: " << objective_shift << std::endl;
  }

  CompareStandardFormProblems(standard_problem, standard_problem_target,
                              objective_shift, objective_shift_target,
                              relative_tolerance);
}

// We test the reduction of the bounded quadratic programming problem
//
//   arginf_x { c' x + (1 / 2) x' Q x : A x = b, l <= x <= u },
//
// into the form
//
//   arginf_x { c' x + (1 / 2) x' Q x : A x = b, x >= 0 },
//
// for the input matrices:
//
//       | 2 |       |  3, -1 | -1 |
//   c = | 3 |,  Q = | -1,  3 | -1 |,  A = | 1, 2 | 3 |,  b = | 5 |,
//       |---|       |--------|----|
//       | 4 |       | -1, -1 |  3 |
//
// and bounds x_1 >= -5, x_2 >= 0, x_3 = 6.
//
// The reduction of the original bounded problem by eliminating the fixed
// variable should be:
//
//   c = | -4 |,  Q = |  3, -1 |,  A = | 1, 2 |,  b = | -13 |,
//       | -3 |       | -1,  3 |
//
//   x_1 >= -5, x_2 >= 0,
//
// with an objective shift of
//
//   4 * 6 + (1 / 2) * 6^2 * 3 = 78.
//
// The elimination of the non-trivial lower bound on x_1 leads to the modified
// problem
//
//   c = | -19 |,  Q = |  3, -1 |,  A = | 1, 2 |,  b = | -8 |,
//       |   2 |       | -1,  3 |
//
//   x >= 0,
//
// with an objective shift of
//
//   78 + (-4 * -5) + (1 / 2) * (-5)^2 * 3 = 135.5.
//
template <typename Real>
void Test2(bool print_data, Real relative_tolerance) {
  // We begin by constructing the original bounded problem.
  const conic::Int num_primal_bounded = 3;
  const conic::Int num_dual_bounded = 1;
  const conic::Int num_fixed = 1;
  conic::bounded_qp::Problem<Real> bounded_problem;
  bounded_problem.num_fixed = num_fixed;

  // Construct the bounded linear objective,
  //
  //       | 2 |
  //   c = | 3 |.
  //       |---|
  //       | 4 |
  //
  bounded_problem.linear_objective.Resize(num_primal_bounded, 1);
  bounded_problem.linear_objective(0) = Real(2);
  bounded_problem.linear_objective(1) = Real(3);
  bounded_problem.linear_objective(2) = Real(4);

  // Construct the bounded quadratic objective,
  //
  //       |  3, -1 | -1 |
  //   Q = | -1,  3 | -1 |.
  //       |--------|----|
  //       | -1, -1 |  3 |
  //
  bounded_problem.quadratic_objective.Resize(num_primal_bounded,
                                             num_primal_bounded);
  bounded_problem.quadratic_objective.AddEntry(0, 0, Real(3));
  bounded_problem.quadratic_objective.AddEntry(0, 1, Real(-1));
  bounded_problem.quadratic_objective.AddEntry(0, 2, Real(-1));
  bounded_problem.quadratic_objective.AddEntry(1, 0, Real(-1));
  bounded_problem.quadratic_objective.AddEntry(1, 1, Real(3));
  bounded_problem.quadratic_objective.AddEntry(1, 2, Real(-1));
  bounded_problem.quadratic_objective.AddEntry(2, 0, Real(-1));
  bounded_problem.quadratic_objective.AddEntry(2, 1, Real(-1));
  bounded_problem.quadratic_objective.AddEntry(2, 2, Real(3));

  // Construct the bounded equality matrix,
  //
  //   A = | 1, 2 | 3 |.
  //
  bounded_problem.equality_matrix.Resize(num_dual_bounded, num_primal_bounded);
  bounded_problem.equality_matrix.AddEntry(0, 0, Real(1));
  bounded_problem.equality_matrix.AddEntry(0, 1, Real(2));
  bounded_problem.equality_matrix.AddEntry(0, 2, Real(3));

  // Construct the bounded equality right-hand side vector,
  //
  //   b = | 5 |.
  //
  bounded_problem.equality_rhs.Resize(num_dual_bounded, 1);
  bounded_problem.equality_rhs(0) = 5;

  // Construct the bounds.
  bounded_problem.lower_bounds.Resize(num_primal_bounded, Real(0));
  bounded_problem.lower_bounds[0] = Real(-5);
  bounded_problem.lower_bounds[2] = Real(6);
  bounded_problem.upper_bounds.Resize(num_primal_bounded,
                                      std::numeric_limits<Real>::infinity());
  bounded_problem.upper_bounds[2] = Real(6);

  bounded_problem.FillNorms();
  if (print_data) {
    std::cout << "Bounded problem:\n" << bounded_problem << std::endl;
  }

  // We now construct the expected equivalent standard-form problem.
  const conic::Int num_primal_standard = 2;
  const conic::Int num_dual_standard = 1;
  conic::hsd_qp::Problem<Real> standard_problem_target;

  // We construct the standard linear objective,
  //
  //   c = | -19 |.
  //       |   2 |
  //
  standard_problem_target.linear_objective.Resize(num_primal_standard, 1);
  standard_problem_target.linear_objective(0) = Real(-19);
  standard_problem_target.linear_objective(1) = Real(2);

  // We construct the standard quadratic objective,
  //
  //   Q = |  3, -1 |.
  //       | -1,  3 |
  //
  standard_problem_target.quadratic_objective.Resize(num_primal_standard,
                                                     num_primal_standard);
  standard_problem_target.quadratic_objective.AddEntry(0, 0, Real(3));
  standard_problem_target.quadratic_objective.AddEntry(0, 1, Real(-1));
  standard_problem_target.quadratic_objective.AddEntry(1, 0, Real(-1));
  standard_problem_target.quadratic_objective.AddEntry(1, 1, Real(3));

  // We construct the standard equality matrix,
  //
  //   A = | 1, 2 |.
  //
  standard_problem_target.equality_matrix.Resize(num_dual_standard,
                                                 num_primal_standard);
  standard_problem_target.equality_matrix.AddEntry(0, 0, Real(1));
  standard_problem_target.equality_matrix.AddEntry(0, 1, Real(2));

  // We construct the standard equality right-hand side,
  //
  //   b = | -8 |.
  //
  standard_problem_target.equality_rhs.Resize(num_dual_standard, 1);
  standard_problem_target.equality_rhs(0) = Real(-8);

  const Real objective_shift_target = Real(271) / Real(2);
  if (print_data) {
    std::cout << "Target reduced standard problem:\n"
              << standard_problem_target << std::endl;
    std::cout << "Target objective shift: " << objective_shift_target
              << std::endl;
  }

  conic::hsd_qp::Problem<Real> standard_problem;
  conic::BoundedToStandardQPReduction<Real> reduction(bounded_problem);
  reduction.FormStandardProblem(&standard_problem);
  const Real objective_shift = reduction.ObjectiveShift();
  if (print_data) {
    std::cout << "Reduced standard problem:\n" << standard_problem << std::endl;
    std::cout << "Objective shift: " << objective_shift << std::endl;
  }

  CompareStandardFormProblems(standard_problem, standard_problem_target,
                              objective_shift, objective_shift_target,
                              relative_tolerance);
}

// We test the reduction of the bounded quadratic programming problem
//
//   arginf_x { c' x + (1 / 2) x' Q x : A x = b, l <= x <= u },
//
// into the form
//
//   arginf_x { c' x + (1 / 2) x' Q x : A x = b, x >= 0 },
//
// for the input matrices:
//
//       | 2 |       |  3, -1 | -1 |
//   c = | 3 |,  Q = | -1,  3 | -1 |,  A = | 1, 2 | 3 |,  b = | 5 |.
//       |---|       |--------|----|
//       | 4 |       | -1, -1 |  3 |
//
// and bounds x_1 >= -5, x_2 <= 7, x_3 = 6.
//
// The reduction of the original bounded problem by eliminating the fixed
// variable should be:
//
//   c = | -4 |,  Q = |  3, -1 |,  A = | 1, 2 |,  b = | -13 |,
//       | -3 |       | -1,  3 |
//
//   x_1 >= -5, x_2 <= 7,
//
// with an objective shift of
//
//   4 * 6 + (1 / 2) * 6^2 * 3 = 78.
//
// The elimination of the non-trivial lower bound on x_1 leads to the modified
// problem
//
//   c = | -19 |,  Q = |  3, -1 |,  A = | 1, 2 |, b = | -8 |,
//       |   2 |       | -1,  3 |
//
//   x_1 >= 0, x_2 <= 7,
//
// with an objective shift of
//
//   78 + (-4 * -5) + (1 / 2) * (-5)^2 * 3 = 135.5.
//
// The further elimination of the non-trivial upper bound on x_2 leads to the
// modified problem
//
//   c = | -26 |,  Q = | 3,  1 |,  A = | 1, -2 |,  b = | -22 |,
//       | -23 |       | 1,  3 |
//
//   x >= 0,
//
// with an objective shift of
//
//   135.5 + (2 * 7) + (1 / 2) * 7^2 * 3 = 223.
//
template <typename Real>
void Test3(bool print_data, Real relative_tolerance) {
  // We begin by constructing the original bounded problem.
  const conic::Int num_primal_bounded = 3;
  const conic::Int num_dual_bounded = 1;
  const conic::Int num_fixed = 1;
  conic::bounded_qp::Problem<Real> bounded_problem;
  bounded_problem.num_fixed = num_fixed;

  // Construct the bounded linear objective,
  //
  //       | 2 |
  //   c = | 3 |.
  //       |---|
  //       | 4 |
  //
  bounded_problem.linear_objective.Resize(num_primal_bounded, 1);
  bounded_problem.linear_objective(0) = Real(2);
  bounded_problem.linear_objective(1) = Real(3);
  bounded_problem.linear_objective(2) = Real(4);

  // Construct the bounded quadratic objective,
  //
  //       |  3, -1 | -1 |
  //   Q = | -1,  3 | -1 |.
  //       |--------|----|
  //       | -1, -1 |  3 |
  //
  bounded_problem.quadratic_objective.Resize(num_primal_bounded,
                                             num_primal_bounded);
  bounded_problem.quadratic_objective.AddEntry(0, 0, Real(3));
  bounded_problem.quadratic_objective.AddEntry(0, 1, Real(-1));
  bounded_problem.quadratic_objective.AddEntry(0, 2, Real(-1));
  bounded_problem.quadratic_objective.AddEntry(1, 0, Real(-1));
  bounded_problem.quadratic_objective.AddEntry(1, 1, Real(3));
  bounded_problem.quadratic_objective.AddEntry(1, 2, Real(-1));
  bounded_problem.quadratic_objective.AddEntry(2, 0, Real(-1));
  bounded_problem.quadratic_objective.AddEntry(2, 1, Real(-1));
  bounded_problem.quadratic_objective.AddEntry(2, 2, Real(3));

  // Construct the bounded equality matrix,
  //
  //   A = | 1, 2 | 3 |.
  //
  bounded_problem.equality_matrix.Resize(num_dual_bounded, num_primal_bounded);
  bounded_problem.equality_matrix.AddEntry(0, 0, Real(1));
  bounded_problem.equality_matrix.AddEntry(0, 1, Real(2));
  bounded_problem.equality_matrix.AddEntry(0, 2, Real(3));

  // Construct the bounded equality right-hand side vector,
  //
  //   b = | 5 |.
  //
  bounded_problem.equality_rhs.Resize(num_dual_bounded, 1);
  bounded_problem.equality_rhs(0) = 5;

  // Construct the bounds.
  bounded_problem.lower_bounds.Resize(num_primal_bounded,
                                      -std::numeric_limits<Real>::infinity());
  bounded_problem.lower_bounds[0] = Real(-5);
  bounded_problem.lower_bounds[2] = Real(6);
  bounded_problem.upper_bounds.Resize(num_primal_bounded,
                                      std::numeric_limits<Real>::infinity());
  bounded_problem.upper_bounds[1] = Real(7);
  bounded_problem.upper_bounds[2] = Real(6);

  bounded_problem.FillNorms();
  if (print_data) {
    std::cout << "Bounded problem:\n" << bounded_problem << std::endl;
  }

  // We now construct the expected equivalent standard-form problem.
  const conic::Int num_primal_standard = 2;
  const conic::Int num_dual_standard = 1;
  conic::hsd_qp::Problem<Real> standard_problem_target;

  // We construct the standard linear objective,
  //
  //   c = | -26 |.
  //       | -23 |
  //
  standard_problem_target.linear_objective.Resize(num_primal_standard, 1);
  standard_problem_target.linear_objective(0) = Real(-26);
  standard_problem_target.linear_objective(1) = Real(-23);

  // We construct the standard quadratic objective,
  //
  //   Q = | 3,  1 |.
  //       | 1,  3 |
  //
  standard_problem_target.quadratic_objective.Resize(num_primal_standard,
                                                     num_primal_standard);
  standard_problem_target.quadratic_objective.AddEntry(0, 0, Real(3));
  standard_problem_target.quadratic_objective.AddEntry(0, 1, Real(1));
  standard_problem_target.quadratic_objective.AddEntry(1, 0, Real(1));
  standard_problem_target.quadratic_objective.AddEntry(1, 1, Real(3));

  // We construct the standard equality matrix,
  //
  //   A = | 1, -2 |.
  //
  standard_problem_target.equality_matrix.Resize(num_dual_standard,
                                                 num_primal_standard);
  standard_problem_target.equality_matrix.AddEntry(0, 0, Real(1));
  standard_problem_target.equality_matrix.AddEntry(0, 1, Real(-2));

  // We construct the standard equality right-hand side,
  //
  //   b = | -22 |.
  //
  standard_problem_target.equality_rhs.Resize(num_dual_standard, 1);
  standard_problem_target.equality_rhs(0) = Real(-22);

  const Real objective_shift_target = Real(223);
  if (print_data) {
    std::cout << "Target reduced standard problem:\n"
              << standard_problem_target << std::endl;
    std::cout << "Target objective shift: " << objective_shift_target
              << std::endl;
  }

  conic::hsd_qp::Problem<Real> standard_problem;
  conic::BoundedToStandardQPReduction<Real> reduction(bounded_problem);
  reduction.FormStandardProblem(&standard_problem);
  const Real objective_shift = reduction.ObjectiveShift();
  if (print_data) {
    std::cout << "Reduced standard problem:\n" << standard_problem << std::endl;
    std::cout << "Objective shift: " << objective_shift << std::endl;
  }

  CompareStandardFormProblems(standard_problem, standard_problem_target,
                              objective_shift, objective_shift_target,
                              relative_tolerance);
}

// We test the reduction of the bounded quadratic programming problem
//
//   arginf_x { c' x + (1 / 2) x' Q x : A x = b, l <= x <= u },
//
// into the form
//
//   arginf_x { c' x + (1 / 2) x' Q x : A x = b, x >= 0 },
//
// for the input matrices:
//
//       | 2 |       |  3, -1, -1 | -1 |
//   c = | 3 |,  Q = | -1,  3, -1 | -1 |,  A = | 1, 2, 3 | 4 |,  b = | 5 |,
//       | 4 |       | -1, -1,  3 | -1 |
//       |---|       |------------|----|
//       | 5 |       | -1, -1, -1 |  3 |
//
// and bounds x_1 >= -5, x_2 <= 7, 1 <= x_3 <= 3, x_4 = 6.
//
// The reduction of the original bounded problem by eliminating the fixed
// variable should be:
//
//   c = | -4 |,  Q = |  3, -1, -1 |,  A = | 1, 2, 3 |,  b = | -19 |,
//       | -3 |       | -1,  3, -1 |
//       | -2 |       | -1, -1,  3 |
//
//   x_1 >= -5, x_2 <= 7, 1 <= x_3 <= 3,
//
// with an objective shift of
//
//   5 * 6 + (1 / 2) * 6^2 * 3 = 84.
//
// The elimination of the non-trivial lower bound on x_1 leads to the modified
// problem
//
//   c = | -19 |,  Q = |  3, -1, -1 |,  A = | 1, 2, 3 |,  b = | -14 |,
//       |   2 |       | -1,  3, -1 |
//       |   3 |       | -1, -1,  3 |
//
//   x_1 >= 0, x_2 <= 7, 1 <= x_3 <= 3,
//
// with an objective shift of
//
//   84 + (-4 * -5) + (1 / 2) * (-5)^2 * 3 = 141.5.
//
// The further elimination of the non-trivial upper bound on x_2 leads to the
// modified problem
//
//   c = | -26 |,  Q = |  3,  1, -1 |,  A = | 1, -2, 3 |,  b = | -28 |,
//       | -23 |       |  1,  3,  1 |
//       |  -4 |       | -1,  1,  3 |
//
//   x_1 >= 0, x_2 >= 0, 1 <= x_3 <= 3,
//
// with an objective shift of
//
//   141.5 + (2 * 7) + (1 / 2) * 7^2 * 3 = 229.
//
// The elimination of the two-sided bound on x_3 leads to
//
//   c = | -27 |,  Q = |  3,  1, -1 | 0 |,  A = | 1, -2, 3 | 0 |,  b = | -31 |,
//       | -22 |       |  1,  3,  1 | 0 |       | 0,  0, 1 | 1 |       |   2 |
//       |  -1 |       | -1,  1,  3 | 0 |
//       |-----|       |------------|---|
//       |   0 |       |  0,  0,  0 | 0 |
//
//   x >= 0,
//
// with an objective shift of
//
//   229 + (-4 * 1) + (1 / 2) * 1^2 * 3 = 226.5.
//
template <typename Real>
void Test4(bool print_data, Real relative_tolerance) {
  // We begin by constructing the original bounded problem.
  const conic::Int num_primal_bounded = 4;
  const conic::Int num_dual_bounded = 1;
  const conic::Int num_fixed = 1;
  conic::bounded_qp::Problem<Real> bounded_problem;
  bounded_problem.num_fixed = num_fixed;

  // Construct the bounded linear objective,
  //
  //       | 2 |
  //   c = | 3 |.
  //       | 4 |
  //       |---|
  //       | 5 |
  //
  bounded_problem.linear_objective.Resize(num_primal_bounded, 1);
  bounded_problem.linear_objective(0) = Real(2);
  bounded_problem.linear_objective(1) = Real(3);
  bounded_problem.linear_objective(2) = Real(4);
  bounded_problem.linear_objective(3) = Real(5);

  // Construct the bounded quadratic objective,
  //
  //       |  3, -1, -1 | -1 |
  //   Q = | -1,  3, -1 | -1 |.
  //       | -1, -1,  3 | -1 |
  //       |------------|----|
  //       | -1, -1, -1 |  3 |
  //
  bounded_problem.quadratic_objective.Resize(num_primal_bounded,
                                             num_primal_bounded);
  bounded_problem.quadratic_objective.AddEntry(0, 0, Real(3));
  bounded_problem.quadratic_objective.AddEntry(0, 1, Real(-1));
  bounded_problem.quadratic_objective.AddEntry(0, 2, Real(-1));
  bounded_problem.quadratic_objective.AddEntry(0, 3, Real(-1));
  bounded_problem.quadratic_objective.AddEntry(1, 0, Real(-1));
  bounded_problem.quadratic_objective.AddEntry(1, 1, Real(3));
  bounded_problem.quadratic_objective.AddEntry(1, 2, Real(-1));
  bounded_problem.quadratic_objective.AddEntry(1, 3, Real(-1));
  bounded_problem.quadratic_objective.AddEntry(2, 0, Real(-1));
  bounded_problem.quadratic_objective.AddEntry(2, 1, Real(-1));
  bounded_problem.quadratic_objective.AddEntry(2, 2, Real(3));
  bounded_problem.quadratic_objective.AddEntry(2, 3, Real(-1));
  bounded_problem.quadratic_objective.AddEntry(3, 0, Real(-1));
  bounded_problem.quadratic_objective.AddEntry(3, 1, Real(-1));
  bounded_problem.quadratic_objective.AddEntry(3, 2, Real(-1));
  bounded_problem.quadratic_objective.AddEntry(3, 3, Real(3));

  // Construct the bounded equality matrix,
  //
  //   A = | 1, 2, 3 | 4 |.
  //
  bounded_problem.equality_matrix.Resize(num_dual_bounded, num_primal_bounded);
  bounded_problem.equality_matrix.AddEntry(0, 0, Real(1));
  bounded_problem.equality_matrix.AddEntry(0, 1, Real(2));
  bounded_problem.equality_matrix.AddEntry(0, 2, Real(3));
  bounded_problem.equality_matrix.AddEntry(0, 3, Real(4));

  // Construct the bounded equality right-hand side vector,
  //
  //   b = | 5 |.
  //
  bounded_problem.equality_rhs.Resize(num_dual_bounded, 1);
  bounded_problem.equality_rhs(0) = 5;

  // Construct the bounds.
  bounded_problem.lower_bounds.Resize(num_primal_bounded,
                                      -std::numeric_limits<Real>::infinity());
  bounded_problem.lower_bounds[0] = Real(-5);
  bounded_problem.lower_bounds[2] = Real(1);
  bounded_problem.lower_bounds[3] = Real(6);
  bounded_problem.upper_bounds.Resize(num_primal_bounded,
                                      std::numeric_limits<Real>::infinity());
  bounded_problem.upper_bounds[1] = Real(7);
  bounded_problem.upper_bounds[2] = Real(3);
  bounded_problem.upper_bounds[3] = Real(6);

  bounded_problem.FillNorms();
  if (print_data) {
    std::cout << "Bounded problem:\n" << bounded_problem << std::endl;
  }

  // We now construct the expected equivalent standard-form problem.
  const conic::Int num_primal_standard = 4;
  const conic::Int num_dual_standard = 2;
  conic::hsd_qp::Problem<Real> standard_problem_target;

  // We construct the standard linear objective,
  //
  //   c = | -27 |.
  //       | -22 |
  //       |  -1 |
  //       |-----|
  //       |   0 |
  //
  standard_problem_target.linear_objective.Resize(num_primal_standard, 1);
  standard_problem_target.linear_objective(0) = Real(-27);
  standard_problem_target.linear_objective(1) = Real(-22);
  standard_problem_target.linear_objective(2) = Real(-1);
  standard_problem_target.linear_objective(3) = Real(0);

  // We construct the standard quadratic objective,
  //
  //   Q = |  3,  1, -1 | 0 |.
  //       |  1,  3,  1 | 0 |
  //       | -1,  1,  3 | 0 |
  //       |------------|---|
  //       |  0,  0,  0 | 0 |
  //
  standard_problem_target.quadratic_objective.Resize(num_primal_standard,
                                                     num_primal_standard);
  standard_problem_target.quadratic_objective.AddEntry(0, 0, Real(3));
  standard_problem_target.quadratic_objective.AddEntry(0, 1, Real(1));
  standard_problem_target.quadratic_objective.AddEntry(0, 2, Real(-1));
  standard_problem_target.quadratic_objective.AddEntry(1, 0, Real(1));
  standard_problem_target.quadratic_objective.AddEntry(1, 1, Real(3));
  standard_problem_target.quadratic_objective.AddEntry(1, 2, Real(1));
  standard_problem_target.quadratic_objective.AddEntry(2, 0, Real(-1));
  standard_problem_target.quadratic_objective.AddEntry(2, 1, Real(1));
  standard_problem_target.quadratic_objective.AddEntry(2, 2, Real(3));

  // We construct the standard equality matrix,
  //
  //   A = | 1, -2, 3 | 0 |.
  //       | 0,  0, 1 | 1 |
  //
  standard_problem_target.equality_matrix.Resize(num_dual_standard,
                                                 num_primal_standard);
  standard_problem_target.equality_matrix.AddEntry(0, 0, Real(1));
  standard_problem_target.equality_matrix.AddEntry(0, 1, Real(-2));
  standard_problem_target.equality_matrix.AddEntry(0, 2, Real(3));
  standard_problem_target.equality_matrix.AddEntry(1, 2, Real(1));
  standard_problem_target.equality_matrix.AddEntry(1, 3, Real(1));

  // We construct the standard equality right-hand side,
  //
  //   b = | -31 |.
  //       |   2 |
  //
  standard_problem_target.equality_rhs.Resize(num_dual_standard, 1);
  standard_problem_target.equality_rhs(0) = Real(-31);
  standard_problem_target.equality_rhs(1) = Real(2);

  const Real objective_shift_target = Real(453) / Real(2);
  if (print_data) {
    std::cout << "Target reduced standard problem:\n"
              << standard_problem_target << std::endl;
    std::cout << "Target objective shift: " << objective_shift_target
              << std::endl;
  }

  conic::hsd_qp::Problem<Real> standard_problem;
  conic::BoundedToStandardQPReduction<Real> reduction(bounded_problem);
  reduction.FormStandardProblem(&standard_problem);
  const Real objective_shift = reduction.ObjectiveShift();
  if (print_data) {
    std::cout << "Reduced standard problem:\n" << standard_problem << std::endl;
    std::cout << "Objective shift: " << objective_shift << std::endl;
  }

  CompareStandardFormProblems(standard_problem, standard_problem_target,
                              objective_shift, objective_shift_target,
                              relative_tolerance);
}

// We test the reduction of the bounded quadratic programming problem
//
//   arginf_x { c' x + (1 / 2) x' Q x : A x = b, l <= x <= u },
//
// into the form
//
//   arginf_x { c' x + (1 / 2) x' Q x : A x = b, x >= 0 },
//
// for the input matrices:
//
//       | 2 |       |  6, -1, -1, -1 | -1 |
//       | 3 |       | -1,  6, -1, -1 | -1 |
//   c = | 4 |,  Q = | -1, -1,  6, -1 | -1 |,
//       | 5 |       | -1, -1, -1,  6 | -1 |
//       |---|       |----------------|----|
//       | 6 |       | -1, -1, -1, -1 |  6 |
//
//       | 1, 2, 3, 4 |  5 |       | 7 |
//   A = | 6, 7, 8, 9 | 10 |,  b = | 8 |.
//
// and bounds x_1 >= -5, x_2 <= 7, 1 <= x_3 <= 3, x_4 free, x_5 = 9.
//
// The reduction of the original bounded problem by eliminating the fixed
// variable should be:
//
//       | -7 |       |  6, -1, -1, -1 |
//   c = | -6 |,  Q = | -1,  6, -1, -1 |,
//       | -5 |       | -1, -1,  6, -1 |
//       | -4 |       | -1, -1, -1,  6 |
//
//   A = | 1, 2, 3, 4 |,  b = | -38 |,
//       | 6, 7, 8, 9 |       | -82 |
//
// with an objective shift of
//
//   6 * 9 + (1 / 2) * 9^2 * 6 = 297.
//
// The elimination of the non-trivial lower bound on x_1 leads to the modified
// problem
//
//       | -37 |       |  6, -1, -1, -1 |
//   c = |  -1 |,  Q = | -1,  6, -1, -1 |,
//       |   0 |       | -1, -1,  6, -1 |
//       |   1 |       | -1, -1, -1,  6 |
//
//   A = | 1, 2, 3, 4 |,  b = | -33 |,
//       | 6, 7, 8, 9 |       | -52 |
//
//   x_1 >= 0, x_2 <= 7, 1 <= x_3 <= 3, x_4 free,
//
// with an objective shift of
//
//   297 + -7 * -5 + (1 / 2) * (-5)^2 * 6 = 407.
//
// The further elimination of the non-trivial upper bound on x_2 leads to the
// modified problem
//
//       | -44 |       |  6,  1, -1, -1 |
//   c = | -41 |,  Q = |  1,  6,  1,  1 |,
//       |  -7 |       | -1,  1,  6, -1 |
//       |  -6 |       | -1,  1, -1,  6 |
//
//   A = | 1, -2, 3, 4 |,  b = |  -47 |,
//       | 6, -7, 8, 9 |       | -101 |
//
//   x_1 >= 0, x_2 >= 0, 1 <= x_3 <= 3, x_4 free,
//
// with an objective shift of
//
//   407 + -1 * 7 + (1 / 2) * 7^2 * 6 = 547.
//
// The elimination of the two-sided bound on x_3 leads to
//
//       | -45 |       |  6,  1, -1, -1 | 0 |
//       | -40 |       |  1,  6,  1,  1 | 0 |
//  c =  |  -1 |,  Q = | -1,  1,  6, -1 | 0 |,
//       |  -7 |       | -1,  1, -1,  6 | 0 |
//       |-----|       |----------------|---|
//       |   0 |       |  0,  0,  0,  0 | 0 |
//
//       | 1, -2,  3,  4 |  0 |       |  -50 |
//   A = | 6, -7,  8,  9 |  0 |,  b = | -109 |,
//       |---------------|----|       |------|
//       | 0,  0,  1,  0 |  1 |       |    2 |
//
//   x_1 >= 0, x_2 >= 0, x_3 >= 0, x_4 free, x_5 >= 0,
//
// with an objective shift of
//
//   547 + -7 * 1 + (1 / 2) 1^2 * 6 = 543.
//
// Lastly, the transformation of the free variable, x_4, leads to
//
//       | -45 |       |  6,  1, -1, -1 | 0,  1 |
//       | -40 |       |  1,  6,  1,  1 | 0, -1 |
//  c =  |  -1 |,  Q = | -1,  1,  6, -1 | 0,  1 |,
//       |  -7 |       | -1,  1, -1,  6 | 0,  0 |
//       |-----|       |----------------|-------|
//       |   0 |       |  0,  0,  0,  0 | 0,  0 |
//       |   7 |       |  1, -1,  1,  0 | 0,  6 |
//
//       | 1, -2,  3,  4 | 0, -4 |       |  -50 |
//   A = | 6, -7,  8,  9 | 0, -9 |,  b = | -109 |,
//       |---------------|-------|       |------|
//       | 0,  0,  1,  0 | 1,  0 |       |    2 |
//
//   x >= 0,
//
// with an objective shift of 543.
//
template <typename Real>
void Test5(bool print_data, Real relative_tolerance) {
  const conic::Int num_primal_bounded = 5;
  const conic::Int num_dual_bounded = 2;
  const conic::Int num_fixed = 1;
  conic::bounded_qp::Problem<Real> bounded_problem;
  bounded_problem.num_fixed = num_fixed;

  // Construct the bounded linear objective,
  //
  //       | 2 |
  //       | 3 |
  //   c = | 4 |.
  //       | 5 |
  //       |---|
  //       | 6 |
  //
  bounded_problem.linear_objective.Resize(num_primal_bounded, 1);
  bounded_problem.linear_objective(0) = Real(2);
  bounded_problem.linear_objective(1) = Real(3);
  bounded_problem.linear_objective(2) = Real(4);
  bounded_problem.linear_objective(3) = Real(5);
  bounded_problem.linear_objective(4) = Real(6);

  // Construct the bounded quadratic objective.
  //
  //       |  6, -1, -1, -1 | -1 |
  //       | -1,  6, -1, -1 | -1 |
  //   Q = | -1, -1,  6, -1 | -1 |.
  //       | -1, -1, -1,  6 | -1 |
  //       |----------------|----|
  //       | -1, -1, -1, -1 |  6 |
  //
  bounded_problem.quadratic_objective.Resize(num_primal_bounded,
                                             num_primal_bounded);
  bounded_problem.quadratic_objective.AddEntry(0, 0, Real(6));
  bounded_problem.quadratic_objective.AddEntry(0, 1, Real(-1));
  bounded_problem.quadratic_objective.AddEntry(0, 2, Real(-1));
  bounded_problem.quadratic_objective.AddEntry(0, 3, Real(-1));
  bounded_problem.quadratic_objective.AddEntry(0, 4, Real(-1));

  bounded_problem.quadratic_objective.AddEntry(1, 0, Real(-1));
  bounded_problem.quadratic_objective.AddEntry(1, 1, Real(6));
  bounded_problem.quadratic_objective.AddEntry(1, 2, Real(-1));
  bounded_problem.quadratic_objective.AddEntry(1, 3, Real(-1));
  bounded_problem.quadratic_objective.AddEntry(1, 4, Real(-1));

  bounded_problem.quadratic_objective.AddEntry(2, 0, Real(-1));
  bounded_problem.quadratic_objective.AddEntry(2, 1, Real(-1));
  bounded_problem.quadratic_objective.AddEntry(2, 2, Real(6));
  bounded_problem.quadratic_objective.AddEntry(2, 3, Real(-1));
  bounded_problem.quadratic_objective.AddEntry(2, 4, Real(-1));

  bounded_problem.quadratic_objective.AddEntry(3, 0, Real(-1));
  bounded_problem.quadratic_objective.AddEntry(3, 1, Real(-1));
  bounded_problem.quadratic_objective.AddEntry(3, 2, Real(-1));
  bounded_problem.quadratic_objective.AddEntry(3, 3, Real(6));
  bounded_problem.quadratic_objective.AddEntry(3, 4, Real(-1));

  bounded_problem.quadratic_objective.AddEntry(4, 0, Real(-1));
  bounded_problem.quadratic_objective.AddEntry(4, 1, Real(-1));
  bounded_problem.quadratic_objective.AddEntry(4, 2, Real(-1));
  bounded_problem.quadratic_objective.AddEntry(4, 3, Real(-1));
  bounded_problem.quadratic_objective.AddEntry(4, 4, Real(6));

  // Construct the bounded equality matrix.
  //
  //       | 1, 2, 3, 4 |  5 |
  //   A = | 6, 7, 8, 9 | 10 |.
  //
  bounded_problem.equality_matrix.Resize(num_dual_bounded, num_primal_bounded);
  bounded_problem.equality_matrix.AddEntry(0, 0, Real(1));
  bounded_problem.equality_matrix.AddEntry(0, 1, Real(2));
  bounded_problem.equality_matrix.AddEntry(0, 2, Real(3));
  bounded_problem.equality_matrix.AddEntry(0, 3, Real(4));
  bounded_problem.equality_matrix.AddEntry(0, 4, Real(5));

  bounded_problem.equality_matrix.AddEntry(1, 0, Real(6));
  bounded_problem.equality_matrix.AddEntry(1, 1, Real(7));
  bounded_problem.equality_matrix.AddEntry(1, 2, Real(8));
  bounded_problem.equality_matrix.AddEntry(1, 3, Real(9));
  bounded_problem.equality_matrix.AddEntry(1, 4, Real(10));

  // Construct the bounded equality right-hand side vector.
  //
  //       | 7 |
  //   b = | 8 |.
  //
  bounded_problem.equality_rhs.Resize(num_dual_bounded, 1);
  bounded_problem.equality_rhs(0) = 7;
  bounded_problem.equality_rhs(1) = 8;

  // Construct the bounds.
  bounded_problem.lower_bounds.Resize(num_primal_bounded,
                                      -std::numeric_limits<Real>::infinity());
  bounded_problem.lower_bounds[0] = Real(-5);
  bounded_problem.lower_bounds[2] = Real(1);
  bounded_problem.lower_bounds[4] = Real(9);
  bounded_problem.upper_bounds.Resize(num_primal_bounded,
                                      std::numeric_limits<Real>::infinity());
  bounded_problem.upper_bounds[1] = Real(7);
  bounded_problem.upper_bounds[2] = Real(3);
  bounded_problem.upper_bounds[4] = Real(9);

  bounded_problem.FillNorms();
  if (print_data) {
    std::cout << "Bounded problem:\n" << bounded_problem << std::endl;
  }

  // We now construct the expected equivalent standard-form problem.
  const conic::Int num_primal_standard = 6;
  const conic::Int num_dual_standard = 3;
  conic::hsd_qp::Problem<Real> standard_problem_target;

  // We construct the standard linear objective,
  //
  //       | -45 |
  //       | -40 |
  //  c =  |  -1 |.
  //       |  -7 |
  //       |-----|
  //       |   0 |
  //       |   7 |
  //
  standard_problem_target.linear_objective.Resize(num_primal_standard, 1);
  standard_problem_target.linear_objective(0) = Real(-45);
  standard_problem_target.linear_objective(1) = Real(-40);
  standard_problem_target.linear_objective(2) = Real(-1);
  standard_problem_target.linear_objective(3) = Real(-7);
  standard_problem_target.linear_objective(4) = Real(0);
  standard_problem_target.linear_objective(5) = Real(7);

  // We construct the standard quadratic objective,
  //
  //      |  6,  1, -1, -1 | 0,  1 |
  //      |  1,  6,  1,  1 | 0, -1 |
  //  Q = | -1,  1,  6, -1 | 0,  1 |.
  //      | -1,  1, -1,  6 | 0,  0 |
  //      |----------------|-------|
  //      |  0,  0,  0,  0 | 0,  0 |
  //      |  1, -1,  1,  0 | 0,  6 |
  //
  standard_problem_target.quadratic_objective.Resize(num_primal_standard,
                                                     num_primal_standard);
  standard_problem_target.quadratic_objective.AddEntry(0, 0, Real(6));
  standard_problem_target.quadratic_objective.AddEntry(0, 1, Real(1));
  standard_problem_target.quadratic_objective.AddEntry(0, 2, Real(-1));
  standard_problem_target.quadratic_objective.AddEntry(0, 3, Real(-1));
  standard_problem_target.quadratic_objective.AddEntry(0, 5, Real(1));

  standard_problem_target.quadratic_objective.AddEntry(1, 0, Real(1));
  standard_problem_target.quadratic_objective.AddEntry(1, 1, Real(6));
  standard_problem_target.quadratic_objective.AddEntry(1, 2, Real(1));
  standard_problem_target.quadratic_objective.AddEntry(1, 3, Real(1));
  standard_problem_target.quadratic_objective.AddEntry(1, 5, Real(-1));

  standard_problem_target.quadratic_objective.AddEntry(2, 0, Real(-1));
  standard_problem_target.quadratic_objective.AddEntry(2, 1, Real(1));
  standard_problem_target.quadratic_objective.AddEntry(2, 2, Real(6));
  standard_problem_target.quadratic_objective.AddEntry(2, 3, Real(-1));
  standard_problem_target.quadratic_objective.AddEntry(2, 5, Real(1));

  standard_problem_target.quadratic_objective.AddEntry(3, 0, Real(-1));
  standard_problem_target.quadratic_objective.AddEntry(3, 1, Real(1));
  standard_problem_target.quadratic_objective.AddEntry(3, 2, Real(-1));
  standard_problem_target.quadratic_objective.AddEntry(3, 3, Real(6));

  standard_problem_target.quadratic_objective.AddEntry(5, 0, Real(1));
  standard_problem_target.quadratic_objective.AddEntry(5, 1, Real(-1));
  standard_problem_target.quadratic_objective.AddEntry(5, 2, Real(1));
  standard_problem_target.quadratic_objective.AddEntry(5, 5, Real(6));

  // We construct the standard equality matrix,
  //
  //       | 1, -2,  3,  4 | 0, -4 |
  //   A = | 6, -7,  8,  9 | 0, -9 |.
  //       |---------------|-------|
  //       | 0,  0,  1,  0 | 1,  0 |
  //
  standard_problem_target.equality_matrix.Resize(num_dual_standard,
                                                 num_primal_standard);
  standard_problem_target.equality_matrix.AddEntry(0, 0, Real(1));
  standard_problem_target.equality_matrix.AddEntry(0, 1, Real(-2));
  standard_problem_target.equality_matrix.AddEntry(0, 2, Real(3));
  standard_problem_target.equality_matrix.AddEntry(0, 3, Real(4));
  standard_problem_target.equality_matrix.AddEntry(0, 5, Real(-4));
  standard_problem_target.equality_matrix.AddEntry(1, 0, Real(6));
  standard_problem_target.equality_matrix.AddEntry(1, 1, Real(-7));
  standard_problem_target.equality_matrix.AddEntry(1, 2, Real(8));
  standard_problem_target.equality_matrix.AddEntry(1, 3, Real(9));
  standard_problem_target.equality_matrix.AddEntry(1, 5, Real(-9));
  standard_problem_target.equality_matrix.AddEntry(2, 2, Real(1));
  standard_problem_target.equality_matrix.AddEntry(2, 4, Real(1));

  // We construct the standard equality right-hand side,
  //
  //       |  -50 |
  //   b = | -109 |.
  //       |------|
  //       |    2 |
  //
  standard_problem_target.equality_rhs.Resize(num_dual_standard, 1);
  standard_problem_target.equality_rhs(0) = Real(-50);
  standard_problem_target.equality_rhs(1) = Real(-109);
  standard_problem_target.equality_rhs(2) = Real(2);

  const Real objective_shift_target = Real(543);
  if (print_data) {
    std::cout << "Target reduced standard problem:\n"
              << standard_problem_target << std::endl;
    std::cout << "Target objective shift: " << objective_shift_target
              << std::endl;
  }

  conic::hsd_qp::Problem<Real> standard_problem;
  conic::BoundedToStandardQPReduction<Real> reduction(bounded_problem);
  reduction.FormStandardProblem(&standard_problem);
  const Real objective_shift = reduction.ObjectiveShift();
  if (print_data) {
    std::cout << "Reduced standard problem:\n" << standard_problem << std::endl;
    std::cout << "Objective shift: " << objective_shift << std::endl;
  }

  CompareStandardFormProblems(standard_problem, standard_problem_target,
                              objective_shift, objective_shift_target,
                              relative_tolerance);
}

// We test the reduction of the bounded quadratic programming problem
//
//   arginf_x { c' x + (1 / 2) x' Q x : A x = b, l <= x <= u },
//
// into the form
//
//   arginf_x { c' x + (1 / 2) x' Q x : A x = b, x >= 0 },
//
// for the input matrices:
//
//       | 2 |       |  6, -1, -1, -1, -1 | -1 |
//       | 3 |       | -1,  6, -1, -1, -1 | -1 |
//   c = | 4 |,  Q = | -1, -1,  6, -1, -1 | -1 |,
//       | 5 |       | -1, -1, -1,  6, -1 | -1 |
//       | 6 |       | -1, -1, -1, -1,  6 | -1 |
//       |---|       |--------------------|----|
//       | 7 |       | -1, -1, -1, -1, -1 |  6 |
//
//   A = | 1, 2, 3, 4,  5 |  6 |   b = | 7 |
//       | 6, 7, 8, 9, 10 | 11 |,      | 8 |.
//
// and bounds x_1 >= -5, x_2 free, x_3 <= 7, 1 <= x_4 <= 3, x_5 free, x_6 = 9.
//
// The reduction of the original bounded problem by eliminating the fixed
// variable should be:
//
//       | -7 |       |  6, -1, -1, -1, -1 |
//       | -6 |       | -1,  6, -1, -1, -1 |
//   c = | -5 |,  Q = | -1, -1,  6, -1, -1 |,
//       | -4 |       | -1, -1, -1,  6, -1 |
//       | -3 |       | -1, -1, -1, -1,  6 |
//
//   A = | 1, 2, 3, 4,  5 |   b = | -47 |
//       | 6, 7, 8, 9, 10 |,      | -91 |.
//
// with an objective shift of
//
//   7 * 9 + (1 / 2) * 9^2 * 6 = 306.
//
// The elimination of the non-trivial lower bound on x_1 leads to the modified
// problem
//
//       | -37 |       |  6, -1, -1, -1, -1 |
//       |  -1 |       | -1,  6, -1, -1, -1 |
//   c = |   0 |,  Q = | -1, -1,  6, -1, -1 |,
//       |   1 |       | -1, -1, -1,  6, -1 |
//       |   2 |       | -1, -1, -1, -1,  6 |
//
//   A = | 1, 2, 3, 4,  5 |   b = | -42 |
//       | 6, 7, 8, 9, 10 |,      | -61 |,
//
//   x_1 >= 0, x_2 free, x_3 <= 7, 1 <= x_4 <= 3, x_5 free,
//
// with an objective shift of
//
//   306 + -7 * -5 + (1 / 2) * (-5)^2 * 6 = 416.
//
// The further transformation of the free variable x_2 leads to
//
//       | -37 |       |  6, -1, -1, -1, -1 | 1 |
//       |  -1 |       | -1,  6, -1, -1, -1 | 0 |
//   c = |   0 |,  Q = | -1, -1,  6, -1, -1 | 1 |,
//       |   1 |       | -1, -1, -1,  6, -1 | 1 |
//       |   2 |       | -1, -1, -1, -1,  6 | 1 |
//       |-----|       |--------------------|---|
//       |   1 |       |  1,  0,  1,  1,  1 | 6 |
//
//   A = | 1, 2, 3, 4,  5, | -2 |   b = | -42 |
//       | 6, 7, 8, 9, 10, | -7 |,      | -61 |,
//
//   x_1 >= 0, x_2 >= 0, x_3 <= 7, 1 <= x_4 <= 3, x_5 free, x_6 >= 0
//
// with an objective shift of 416.
//
// The elimination of the upper bound on x_3 leads to
//
//       | -44 |       |  6, -1,  1, -1, -1 |  1 |
//       |  -8 |       | -1,  6,  1, -1, -1 |  0 |
//   c = | -42 |,  Q = |  1,  1,  6,  1,  1 | -1 |,
//       |  -6 |       | -1, -1,  1,  6, -1 |  1 |
//       |  -5 |       | -1, -1,  1, -1,  6 |  1 |
//       |-----|       |--------------------|----|
//       |   8 |       |  1,  0, -1,  1,  1 |  6 |
//
//   A = | 1, 2, -3, 4,  5 | -2 |   b = |  -63 |
//       | 6, 7, -8, 9, 10 | -7 |,      | -117 |,
//
//   x_1 >= 0, x_2 >= 0, x_3 >= 0, 1 <= x_4 <= 3, x_5 free, x_6 >= 0
//
// with an objective shift of
//
//   416 + 7 * 0 + (1 / 2) 7^2 * 6 = 563.
//
// Eliminating the two-sided bound on x_4, 1 <= x_4 <= 3, leads to
//
//       | -45 |       |  6, -1,  1, -1, -1 |  1,  0 |
//       |  -9 |       | -1,  6,  1, -1, -1 |  0,  0 |
//       | -41 |       |  1,  1,  6,  1,  1 | -1,  0 |
//  c =  |   0 |,  Q = | -1, -1,  1,  6, -1 |  1,  0 |,
//       |  -6 |       | -1, -1,  1, -1,  6 |  1,  0 |
//       |-----|       |--------------------|--------|
//       |   9 |       |  1,  0, -1,  1,  1 |  6,  0 |
//       |   0 |       |  0,  0,  0,  0,  0 |  0,  0 |
//
//       | 1, 2, -3, 4,  5 | -2, 0 |       |  -67 |
//   A = | 6, 7, -8, 9, 10 | -7, 0 |,  b = | -126 |,
//       |-----------------|-------|       |------|
//       | 0, 0,  0, 1,  0 |  0, 1 |       |    2 |
//
//   x_1 >= 0, x_2 >= 0, x_3 >= 0, x_4 >= 0, x_5 free, x_6 >= 0, x_7 >= 0,
//
// with an objective shift of
//
//   563 + -6 * 1 + (1 / 2) 1^2 6 = 560.
//
// Finally, transforming the free variable x_5 yields
//
//       | -45 |       |  6, -1,  1, -1, -1 |  1,  0,  1 |
//       |  -9 |       | -1,  6,  1, -1, -1 |  0,  0,  1 |
//       | -41 |       |  1,  1,  6,  1,  1 | -1,  0, -1 |
//  c =  |   0 |,  Q = | -1, -1,  1,  6, -1 |  1,  0,  1 |,
//       |  -6 |       | -1, -1,  1, -1,  6 |  1,  0,  0 |
//       |-----|       |--------------------|------------|
//       |   9 |       |  1,  0, -1,  1,  1 |  6,  0, -1 |
//       |   0 |       |  0,  0,  0,  0,  0 |  0,  0,  0 |
//       |   6 |       |  1,  1, -1,  1,  0 | -1,  0,  6 |
//
//       | 1, 2, -3, 4,  5 | -2, 0,  -5 |       |  -67 |
//   A = | 6, 7, -8, 9, 10 | -7, 0, -10 |,  b = | -126 |,
//       |-----------------|------------|       |------|
//       | 0, 0,  0, 1,  0 |  0, 1,   0 |       |    2 |
//
//   x >= 0,
//
// with objective shift 560.
//
// We then test that the backtransformation of the (arbitrary, infeasible,
// though satisfying
// standard-form iterate
//
//       |    1 |                    |  0 |
//       |    2 |                    |  0 |
//       |    3 |       |  9 |       |  0 |
//   x = |  0.1 |,  y = | 10 |,  z = |  0 |, tau = 0.1,
//       |    0 |       |----|       | 16 |
//       |------|       | 11 |       |----|
//       |    0 |                    | 17 |
//       |  0.1 |                    |  0 |
//       |    8 |                    |  0 |
//
// which has primal objective
//
//   shift + c' (x / tau) + (1 / 2) (x / tau)' Q (x / tau) = 23363.
//
// yields the bounded-form iterate
//
//       |   5 |                     |   0 |       |    0 |
//       |  20 |                     |   0 |       |    0 |
//   x = | -23 |,  y = |  90 |,  z = |   0 |,  w = |    0 |,
//       |   2 |       | 100 |       |   0 |       |    0 |
//       | -80 |                     |   0 |       |    0 |
//       |-----|                     |-----|       |------|
//       |   9 |                     |   0 |       | 1503 |
//
// which has primal objective
//
//   c' x + (1 / 2) x' Q x = 23363.
//
template <typename Real>
void Test6(bool print_data, Real relative_tolerance) {
  const conic::Int num_primal_bounded = 6;
  const conic::Int num_dual_bounded = 2;
  const conic::Int num_fixed = 1;
  conic::bounded_qp::Problem<Real> bounded_problem;
  bounded_problem.num_fixed = num_fixed;

  // Construct the bounded linear objective,
  //
  //       | 2 |
  //       | 3 |
  //   c = | 4 |.
  //       | 5 |
  //       | 6 |
  //       |---|
  //       | 7 |
  //
  bounded_problem.linear_objective.Resize(num_primal_bounded, 1);
  bounded_problem.linear_objective(0) = Real(2);
  bounded_problem.linear_objective(1) = Real(3);
  bounded_problem.linear_objective(2) = Real(4);
  bounded_problem.linear_objective(3) = Real(5);
  bounded_problem.linear_objective(4) = Real(6);
  bounded_problem.linear_objective(5) = Real(7);

  // Construct the bounded quadratic objective.
  //
  //       |  6, -1, -1, -1, -1 | -1 |
  //       | -1,  6, -1, -1, -1 | -1 |
  //   Q = | -1, -1,  6, -1, -1 | -1 |.
  //       | -1, -1, -1,  6, -1 | -1 |
  //       | -1, -1, -1, -1,  6 | -1 |
  //       |--------------------|----|
  //       | -1, -1, -1, -1, -1 |  6 |
  //
  bounded_problem.quadratic_objective.Resize(num_primal_bounded,
                                             num_primal_bounded);
  bounded_problem.quadratic_objective.AddEntries(
      std::vector<catamari::MatrixEntry<Real>>{
          {0, 0, Real(6)},  {0, 1, Real(-1)}, {0, 2, Real(-1)},
          {0, 3, Real(-1)}, {0, 4, Real(-1)}, {0, 5, Real(-1)},
          {1, 0, Real(-1)}, {1, 1, Real(6)},  {1, 2, Real(-1)},
          {1, 3, Real(-1)}, {1, 4, Real(-1)}, {1, 5, Real(-1)},
          {2, 0, Real(-1)}, {2, 1, Real(-1)}, {2, 2, Real(6)},
          {2, 3, Real(-1)}, {2, 4, Real(-1)}, {2, 5, Real(-1)},
          {3, 0, Real(-1)}, {3, 1, Real(-1)}, {3, 2, Real(-1)},
          {3, 3, Real(6)},  {3, 4, Real(-1)}, {3, 5, Real(-1)},
          {4, 0, Real(-1)}, {4, 1, Real(-1)}, {4, 2, Real(-1)},
          {4, 3, Real(-1)}, {4, 4, Real(6)},  {4, 5, Real(-1)},
          {5, 0, Real(-1)}, {5, 1, Real(-1)}, {5, 2, Real(-1)},
          {5, 3, Real(-1)}, {5, 4, Real(-1)}, {5, 5, Real(6)},
      });

  // Construct the bounded equality matrix.
  //
  //   A = | 1, 2, 3, 4,  5 |  6 |.
  //       | 6, 7, 8, 9, 10 | 11 |
  //
  bounded_problem.equality_matrix.Resize(num_dual_bounded, num_primal_bounded);
  bounded_problem.equality_matrix.AddEntries(
      std::vector<catamari::MatrixEntry<Real>>{
          {0, 0, Real(1)},
          {0, 1, Real(2)},
          {0, 2, Real(3)},
          {0, 3, Real(4)},
          {0, 4, Real(5)},
          {0, 5, Real(6)},
          {1, 0, Real(6)},
          {1, 1, Real(7)},
          {1, 2, Real(8)},
          {1, 3, Real(9)},
          {1, 4, Real(10)},
          {1, 5, Real(11)},
      });

  // Construct the bounded equality right-hand side vector.
  //
  //   b = | 7 |
  //       | 8 |.
  //
  bounded_problem.equality_rhs.Resize(num_dual_bounded, 1);
  bounded_problem.equality_rhs(0) = 7;
  bounded_problem.equality_rhs(1) = 8;

  // Construct the bounds.
  bounded_problem.lower_bounds.Resize(num_primal_bounded,
                                      -std::numeric_limits<Real>::infinity());
  bounded_problem.lower_bounds[0] = Real(-5);
  bounded_problem.lower_bounds[3] = Real(1);
  bounded_problem.lower_bounds[5] = Real(9);
  bounded_problem.upper_bounds.Resize(num_primal_bounded,
                                      std::numeric_limits<Real>::infinity());
  bounded_problem.upper_bounds[2] = Real(7);
  bounded_problem.upper_bounds[3] = Real(3);
  bounded_problem.upper_bounds[5] = Real(9);

  bounded_problem.FillNorms();
  if (print_data) {
    std::cout << "Bounded problem:\n" << bounded_problem << std::endl;
  }

  // We now construct the expected equivalent standard-form problem.
  const conic::Int num_primal_standard = 8;
  const conic::Int num_dual_standard = 3;
  conic::hsd_qp::Problem<Real> standard_problem_target;

  // We construct the standard linear objective,
  //
  //       | -45 |
  //       |  -9 |
  //       | -41 |
  //  c =  |   0 |.
  //       |  -6 |
  //       |-----|
  //       |   9 |
  //       |   0 |
  //       |   6 |
  //
  standard_problem_target.linear_objective.Resize(num_primal_standard, 1);
  standard_problem_target.linear_objective(0) = Real(-45);
  standard_problem_target.linear_objective(1) = Real(-9);
  standard_problem_target.linear_objective(2) = Real(-41);
  standard_problem_target.linear_objective(3) = Real(0);
  standard_problem_target.linear_objective(4) = Real(-6);
  standard_problem_target.linear_objective(5) = Real(9);
  standard_problem_target.linear_objective(6) = Real(0);
  standard_problem_target.linear_objective(7) = Real(6);

  // We construct the standard quadratic objective,
  //
  //
  //      |  6, -1,  1, -1, -1 |  1,  0,  1 |
  //      | -1,  6,  1, -1, -1 |  0,  0,  1 |
  //      |  1,  1,  6,  1,  1 | -1,  0, -1 |
  //  Q = | -1, -1,  1,  6, -1 |  1,  0,  1 |.
  //      | -1, -1,  1, -1,  6 |  1,  0,  0 |
  //      |--------------------|------------|
  //      |  1,  0, -1,  1,  1 |  6,  0, -1 |
  //      |  0,  0,  0,  0,  0 |  0,  0,  0 |
  //      |  1,  1, -1,  1,  0 | -1,  0,  6 |
  //
  standard_problem_target.quadratic_objective.Resize(num_primal_standard,
                                                     num_primal_standard);
  standard_problem_target.quadratic_objective.AddEntries(
      std::vector<catamari::MatrixEntry<Real>>{
          {0, 0, Real(6)},  {0, 1, Real(-1)}, {0, 2, Real(1)},
          {0, 3, Real(-1)}, {0, 4, Real(-1)}, {0, 5, Real(1)},
          {0, 7, Real(1)},  {1, 0, Real(-1)}, {1, 1, Real(6)},
          {1, 2, Real(1)},  {1, 3, Real(-1)}, {1, 4, Real(-1)},
          {1, 7, Real(1)},  {2, 0, Real(1)},  {2, 1, Real(1)},
          {2, 2, Real(6)},  {2, 3, Real(1)},  {2, 4, Real(1)},
          {2, 5, Real(-1)}, {2, 7, Real(-1)}, {3, 0, Real(-1)},
          {3, 1, Real(-1)}, {3, 2, Real(1)},  {3, 3, Real(6)},
          {3, 4, Real(-1)}, {3, 5, Real(1)},  {3, 7, Real(1)},
          {4, 0, Real(-1)}, {4, 1, Real(-1)}, {4, 2, Real(1)},
          {4, 3, Real(-1)}, {4, 4, Real(6)},  {4, 5, Real(1)},
          {5, 0, Real(1)},  {5, 2, Real(-1)}, {5, 3, Real(1)},
          {5, 4, Real(1)},  {5, 5, Real(6)},  {5, 7, Real(-1)},
          {7, 0, Real(1)},  {7, 1, Real(1)},  {7, 2, Real(-1)},
          {7, 3, Real(1)},  {7, 5, Real(-1)}, {7, 7, Real(6)},
      });

  // We construct the standard equality matrix,
  //
  //       | 1, 2, -3, 4,  5 | -2, 0,  -5 |
  //   A = | 6, 7, -8, 9, 10 | -7, 0, -10 |.
  //       |-----------------|------------|
  //       | 0, 0,  0, 1,  0 |  0, 1,   0 |
  //
  standard_problem_target.equality_matrix.Resize(num_dual_standard,
                                                 num_primal_standard);
  standard_problem_target.equality_matrix.AddEntry(0, 0, Real(1));
  standard_problem_target.equality_matrix.AddEntry(0, 1, Real(2));
  standard_problem_target.equality_matrix.AddEntry(0, 2, Real(-3));
  standard_problem_target.equality_matrix.AddEntry(0, 3, Real(4));
  standard_problem_target.equality_matrix.AddEntry(0, 4, Real(5));
  standard_problem_target.equality_matrix.AddEntry(0, 5, Real(-2));
  standard_problem_target.equality_matrix.AddEntry(0, 7, Real(-5));

  standard_problem_target.equality_matrix.AddEntry(1, 0, Real(6));
  standard_problem_target.equality_matrix.AddEntry(1, 1, Real(7));
  standard_problem_target.equality_matrix.AddEntry(1, 2, Real(-8));
  standard_problem_target.equality_matrix.AddEntry(1, 3, Real(9));
  standard_problem_target.equality_matrix.AddEntry(1, 4, Real(10));
  standard_problem_target.equality_matrix.AddEntry(1, 5, Real(-7));
  standard_problem_target.equality_matrix.AddEntry(1, 7, Real(-10));

  standard_problem_target.equality_matrix.AddEntry(2, 3, Real(1));
  standard_problem_target.equality_matrix.AddEntry(2, 6, Real(1));

  // We construct the standard equality right-hand side,
  //
  //       |  -67 |
  //   b = | -126 |.
  //       |------|
  //       |    2 |
  //
  standard_problem_target.equality_rhs.Resize(num_dual_standard, 1);
  standard_problem_target.equality_rhs(0) = Real(-67);
  standard_problem_target.equality_rhs(1) = Real(-126);
  standard_problem_target.equality_rhs(2) = Real(2);

  const Real objective_shift_target = Real(560);
  if (print_data) {
    std::cout << "Target reduced standard problem:\n"
              << standard_problem_target << std::endl;
    std::cout << "Target objective shift: " << objective_shift_target
              << std::endl;
  }

  conic::hsd_qp::Problem<Real> standard_problem;
  conic::BoundedToStandardQPReduction<Real> reduction(bounded_problem);
  reduction.FormStandardProblem(&standard_problem);
  const Real objective_shift = reduction.ObjectiveShift();
  if (print_data) {
    std::cout << "Reduced standard problem:\n" << standard_problem << std::endl;
    std::cout << "Objective shift: " << objective_shift << std::endl;
  }

  CompareStandardFormProblems(standard_problem, standard_problem_target,
                              objective_shift, objective_shift_target,
                              relative_tolerance);

  // Form the standard-form iterate
  //
  //       |    1 |                    |  0 |
  //       |    2 |                    |  0 |
  //       |    3 |       |  9 |       |  0 |
  //   x = |  0.1 |,  y = | 10 |,  z = |  0 |, tau = 0.1,
  //       |    0 |       |----|       | 16 |
  //       |------|       | 11 |       |----|
  //       |    0 |                    | 17 |
  //       |  0.1 |                    |  0 |
  //       |    8 |                    |  0 |
  //
  // and its expected shifted primal objective value of 23363.
  //
  conic::hsd_qp::Iterate<Real> standard_iterate;
  standard_iterate.stacked_vectors.Resize(num_primal_standard,
                                          num_dual_standard);
  catamari::BlasMatrixView<Real> standard_primal_solution =
      standard_iterate.PrimalSolution();
  catamari::BlasMatrixView<Real> standard_dual_solution =
      standard_iterate.DualSolution();
  catamari::BlasMatrixView<Real> standard_dual_slack =
      standard_iterate.DualSlack();
  standard_iterate.Tau() = Real(0.1);
  standard_primal_solution(0) = 1;
  standard_primal_solution(1) = 2;
  standard_primal_solution(2) = 3;
  standard_primal_solution(3) = Real(0.1);
  standard_primal_solution(4) = 0;
  standard_primal_solution(5) = 0;
  standard_primal_solution(6) = Real(0.1);
  standard_primal_solution(7) = 8;
  standard_dual_solution(0) = 9;
  standard_dual_solution(1) = 10;
  standard_dual_solution(2) = 11;
  standard_dual_slack(0) = 0;
  standard_dual_slack(1) = 0;
  standard_dual_slack(2) = 0;
  standard_dual_slack(3) = 0;
  standard_dual_slack(4) = 16;
  standard_dual_slack(5) = 17;
  standard_dual_slack(6) = 0;
  standard_dual_slack(7) = 0;
  const Real target_standard_primal_objective = Real(23363);

  Real standard_primal_objective = objective_shift;
  for (conic::Int i = 0; i < num_primal_standard; ++i) {
    standard_primal_objective += standard_problem.linear_objective(i) *
                                 standard_primal_solution(i) /
                                 standard_iterate.Tau();
  }
  for (const auto& entry : standard_problem.quadratic_objective.Entries()) {
    standard_primal_objective +=
        standard_primal_solution(entry.row) * entry.value *
        standard_primal_solution(entry.column) /
        (Real(2) * standard_iterate.Tau() * standard_iterate.Tau());
  }
  const Real standard_primal_obj_deviation =
      std::abs(standard_primal_objective - target_standard_primal_objective) /
      (std::abs(target_standard_primal_objective) + 1);
  REQUIRE(standard_primal_obj_deviation <= relative_tolerance);

  // Form the expected bounded equivalent of the standard-form iterate:
  //
  //       |   5 |                     |   0 |       |    0 |
  //       |  20 |                     |   0 |       |    0 |
  //   x = | -23 |,  y = |  90 |,  z = |   0 |,  w = |    0 |,
  //       |   2 |       | 100 |       |   0 |       |    0 |
  //       | -80 |                     |   0 |       |    0 |
  //       |-----|                     |-----|       |------|
  //       |   9 |                     |   0 |       | 1503 |
  //
  // and its expected objective value of 23363.
  //
  conic::bounded_qp::Iterate<Real> target_bounded_iterate;
  target_bounded_iterate.stacked_vectors.Resize(num_primal_bounded,
                                                num_dual_bounded);
  catamari::BlasMatrixView<Real> target_bounded_primal_solution =
      target_bounded_iterate.PrimalSolution();
  catamari::BlasMatrixView<Real> target_bounded_dual_solution =
      target_bounded_iterate.DualSolution();
  catamari::BlasMatrixView<Real> target_bounded_dual_lower_slack =
      target_bounded_iterate.DualLowerSlack();
  catamari::BlasMatrixView<Real> target_bounded_dual_upper_slack =
      target_bounded_iterate.DualUpperSlack();
  target_bounded_primal_solution(0) = 5;
  target_bounded_primal_solution(1) = 20;
  target_bounded_primal_solution(2) = -23;
  target_bounded_primal_solution(3) = 2;
  target_bounded_primal_solution(4) = -80;
  target_bounded_primal_solution(5) = 9;
  target_bounded_dual_solution(0) = 90;
  target_bounded_dual_solution(1) = 100;
  target_bounded_dual_lower_slack(0) = 0;
  target_bounded_dual_lower_slack(1) = 0;
  target_bounded_dual_lower_slack(2) = 0;
  target_bounded_dual_lower_slack(3) = 0;
  target_bounded_dual_lower_slack(4) = 0;
  target_bounded_dual_lower_slack(5) = 0;
  target_bounded_dual_upper_slack(0) = 0;
  target_bounded_dual_upper_slack(1) = 0;
  target_bounded_dual_upper_slack(2) = 0;
  target_bounded_dual_upper_slack(3) = 0;
  target_bounded_dual_upper_slack(4) = 0;
  target_bounded_dual_upper_slack(5) = 1503;
  const Real target_bounded_primal_objective = Real(23363);

  // Compute the equivalent bounded iterate and compute its primal objective.
  conic::bounded_qp::Iterate<Real> bounded_iterate;
  reduction.BacktransformIterate(standard_iterate, &bounded_iterate);
  CompareBoundedIterates(bounded_iterate, target_bounded_iterate,
                         relative_tolerance);

  catamari::BlasMatrixView<Real> bounded_primal_solution =
      bounded_iterate.PrimalSolution();
  Real bounded_primal_objective = 0;
  for (conic::Int i = 0; i < num_primal_bounded; ++i) {
    bounded_primal_objective +=
        bounded_problem.linear_objective(i) * bounded_primal_solution(i);
  }
  for (const auto& entry : bounded_problem.quadratic_objective.Entries()) {
    bounded_primal_objective += bounded_primal_solution(entry.row) *
                                entry.value *
                                bounded_primal_solution(entry.column) / Real(2);
  }
  const Real bounded_primal_obj_deviation =
      std::abs(bounded_primal_objective - target_bounded_primal_objective) /
      (std::abs(target_bounded_primal_objective) + 1);
  REQUIRE(bounded_primal_obj_deviation <= relative_tolerance);
}

TEST_CASE("Test1 float", "[Test1 float]") {
  const bool kPrintData = false;
  const float relative_obj_tolerance = 1e-3;
  Test1<float>(kPrintData, relative_obj_tolerance);
}

TEST_CASE("Test1 double", "[Test1 double]") {
  const bool kPrintData = false;
  const double relative_obj_tolerance = 1e-6;
  Test1<double>(kPrintData, relative_obj_tolerance);
}

#ifdef TEST_DOUBLEDOUBLE
TEST_CASE("Test1 DoubleDouble", "[Test1 DoubleDouble]") {
  typedef mantis::DoubleMantissa<double> DoubleDouble;
  const bool kPrintData = false;
  const DoubleDouble relative_obj_tolerance = 1e-13;
  Test1<DoubleDouble>(kPrintData, relative_obj_tolerance);
}
#endif  // ifdef TEST_DOUBLEDOUBLE

TEST_CASE("Test2 float", "[Test2 float]") {
  const bool kPrintData = false;
  const float relative_obj_tolerance = 1e-3;
  Test2<float>(kPrintData, relative_obj_tolerance);
}

TEST_CASE("Test2 double", "[Test2 double]") {
  const bool kPrintData = false;
  const double relative_obj_tolerance = 1e-6;
  Test2<double>(kPrintData, relative_obj_tolerance);
}

#ifdef TEST_DOUBLEDOUBLE
TEST_CASE("Test2 DoubleDouble", "[Test2 DoubleDouble]") {
  typedef mantis::DoubleMantissa<double> DoubleDouble;
  const bool kPrintData = false;
  const DoubleDouble relative_obj_tolerance = 1e-13;
  Test2<DoubleDouble>(kPrintData, relative_obj_tolerance);
}
#endif  // ifdef TEST_DOUBLEDOUBLE

TEST_CASE("Test3 float", "[Test3 float]") {
  const bool kPrintData = false;
  const float relative_obj_tolerance = 1e-3;
  Test3<float>(kPrintData, relative_obj_tolerance);
}

TEST_CASE("Test3 double", "[Test3 double]") {
  const bool kPrintData = false;
  const double relative_obj_tolerance = 1e-6;
  Test3<double>(kPrintData, relative_obj_tolerance);
}

#ifdef TEST_DOUBLEDOUBLE
TEST_CASE("Test3 DoubleDouble", "[Test3 DoubleDouble]") {
  typedef mantis::DoubleMantissa<double> DoubleDouble;
  const bool kPrintData = false;
  const DoubleDouble relative_obj_tolerance = 1e-13;
  Test3<DoubleDouble>(kPrintData, relative_obj_tolerance);
}
#endif  // ifdef TEST_DOUBLEDOUBLE

TEST_CASE("Test4 float", "[Test4 float]") {
  const bool kPrintData = false;
  const float relative_obj_tolerance = 1e-3;
  Test4<float>(kPrintData, relative_obj_tolerance);
}

TEST_CASE("Test4 double", "[Test4 double]") {
  const bool kPrintData = false;
  const double relative_obj_tolerance = 1e-6;
  Test4<double>(kPrintData, relative_obj_tolerance);
}

#ifdef TEST_DOUBLEDOUBLE
TEST_CASE("Test4 DoubleDouble", "[Test4 DoubleDouble]") {
  typedef mantis::DoubleMantissa<double> DoubleDouble;
  const bool kPrintData = false;
  const DoubleDouble relative_obj_tolerance = 1e-13;
  Test4<DoubleDouble>(kPrintData, relative_obj_tolerance);
}
#endif  // ifdef TEST_DOUBLEDOUBLE

TEST_CASE("Test5 float", "[Test5 float]") {
  const bool kPrintData = false;
  const float relative_obj_tolerance = 1e-3;
  Test5<float>(kPrintData, relative_obj_tolerance);
}

TEST_CASE("Test5 double", "[Test5 double]") {
  const bool kPrintData = false;
  const double relative_obj_tolerance = 1e-6;
  Test5<double>(kPrintData, relative_obj_tolerance);
}

#ifdef TEST_DOUBLEDOUBLE
TEST_CASE("Test5 DoubleDouble", "[Test5 DoubleDouble]") {
  typedef mantis::DoubleMantissa<double> DoubleDouble;
  const bool kPrintData = false;
  const DoubleDouble relative_obj_tolerance = 1e-13;
  Test5<DoubleDouble>(kPrintData, relative_obj_tolerance);
}
#endif  // ifdef TEST_DOUBLEDOUBLE

TEST_CASE("Test6 float", "[Test6 float]") {
  const bool kPrintData = false;
  const float relative_obj_tolerance = 1e-3;
  Test6<float>(kPrintData, relative_obj_tolerance);
}

TEST_CASE("Test6 double", "[Test6 double]") {
  const bool kPrintData = false;
  const double relative_obj_tolerance = 1e-6;
  Test6<double>(kPrintData, relative_obj_tolerance);
}

#ifdef TEST_DOUBLEDOUBLE
TEST_CASE("Test6 DoubleDouble", "[Test6 DoubleDouble]") {
  typedef mantis::DoubleMantissa<double> DoubleDouble;
  const bool kPrintData = false;
  const DoubleDouble relative_obj_tolerance = 1e-13;
  Test6<DoubleDouble>(kPrintData, relative_obj_tolerance);
}
#endif  // ifdef TEST_DOUBLEDOUBLE
