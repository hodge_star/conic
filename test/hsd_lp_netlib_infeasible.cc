/*
 * Copyright (c) 2019 Jack Poulson <jack@hodgestar.com>
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
#define CATCH_CONFIG_MAIN
#include <iostream>
#include <limits>
#include "catch2/catch.hpp"
#include "conic.hpp"
#include "quotient/timer.hpp"

using conic::ConstBlasMatrixView;
using conic::EuclideanNorm;
using conic::Int;

// If defined, the tests are also run in double-double arithmetic.
#define TEST_DOUBLEDOUBLE

// The woodinfe problem has a clear infeasibility issue of a fixed value of 0
// conflicting with a lower bound of 10.
//
// Ideally we would cleanly handle this, but our MPS reader flags the conflict
// and proceeds in an undefined manner at the moment.
//#define TEST_WOODINFE

// The location of the infeasible NETLIB LP tests.
static const char kInfeasibleNetlib[] = "../data/netlib_lp_infeas/";

// Configuration for a netlib LP test.
template <typename Real>
struct TestConfig {
  // The location of the file containing the (uncompressed) MPS file.
  std::string filename;

  // Whether the solver should have 'verbose' level printing.
  bool verbose = false;

  // Whether the problem data and solution should be printed.
  bool print_data = false;

  // The acceptable relative error tolerance.
  Real relative_error_tol;

  // True if the primal is infeasible.
  bool primal_infeasible = false;

  // True if the dual is infeasible.
  bool dual_infeasible = false;

  // True if the problem is sufficiently close to feasible to be acceptable.
  bool approximately_feasible = false;
};

template <typename Real>
void Test(const TestConfig<Real>& lp_config) {
  conic::ReadQuadraticProgramMPSConfig config;
  config.filename = lp_config.filename;

  conic::hsd_qp::Problem<Real> problem;
  auto info = conic::ReadQuadraticProgramMPS(config, &problem);
  const Int num_primal = problem.equality_matrix.NumColumns();
  const Int num_dual = problem.equality_matrix.NumRows();
  std::cout << "info.bounds_section.num_fixed_bounds: "
            << info.bounds_section.num_fixed_bounds << "\n"
            << "info.bounds_section.num_upper_bounds: "
            << info.bounds_section.num_upper_bounds << "\n"
            << "info.bounds_section.num_lower_bounds: "
            << info.bounds_section.num_lower_bounds << "\n"
            << "info.bounds_section.num_free_bounds: "
            << info.bounds_section.num_free_bounds << "\n"
            << "info.row_section.num_equality_rows: "
            << info.row_section.num_equality_rows << "\n"
            << "info.objective_shift: " << info.objective_shift << std::endl;
  if (lp_config.print_data) {
    std::cout << problem << std::endl;
  }

  conic::hsd_qp::SolveControl<Real> control;
  control.output.verbose = lp_config.verbose;
  conic::hsd_qp::Iterate<Real> certificate;
  quotient::Timer timer;
  timer.Start();
  const Int num_iterations =
      conic::hsd_qp::Solve(problem, control, &certificate);
  timer.Stop();
  if (lp_config.print_data) {
    std::cout << certificate << std::endl;
  }
  problem.PrintNorms(std::cout);
  certificate.PrintNorms(std::cout);
  std::cout << certificate.certificate_type << std::endl;

  conic::hsd_qp::Residuals<Real> residuals;
  residuals.iterate_normalization = control.optimality.iterate_normalization;
  residuals.FillRelativeError(problem, certificate);
  std::cout << lp_config.filename << " ran for " << num_iterations
            << " iterations (" << timer.TotalSeconds() << " seconds)"
            << std::endl;

  const ConstBlasMatrixView<Real> primal_solution =
      certificate.PrimalSolution();
  const ConstBlasMatrixView<Real> dual_solution = certificate.DualSolution();

  Real unscaled_primal = 0;
  for (Int i = 0; i < num_primal; ++i) {
    unscaled_primal += problem.linear_objective(i) * primal_solution(i);
  }
  for (const auto& entry : problem.quadratic_objective.Entries()) {
    unscaled_primal += entry.value * primal_solution(entry.row) *
                       primal_solution(entry.column) / Real(2);
  }

  Real unscaled_dual = 0;
  for (Int i = 0; i < num_dual; ++i) {
    unscaled_dual += problem.equality_rhs(i) * dual_solution(i);
  }
  for (const auto& entry : problem.quadratic_objective.Entries()) {
    unscaled_dual -= entry.value * primal_solution(entry.row) *
                     primal_solution(entry.column) / Real(2);
  }

  std::cout << "primal: " << unscaled_primal << ", dual: " << unscaled_dual
            << std::endl;
  REQUIRE(num_iterations < control.max_iterations);

  if (lp_config.approximately_feasible &&
      certificate.certificate_type == conic::OPTIMALITY_CERTIFICATE) {
    conic::hsd_qp::Residuals<Real> residuals;
    residuals.FillRelativeError(problem, certificate);
    residuals.PrintRelativeError(std::cout);
  } else if (lp_config.approximately_feasible &&
             certificate.certificate_type == conic::INFEASIBILITY_CERTIFICATE) {
    // We only allow detection of infeasibility without knowledge of primal or
    // dual infeasibility when approximately feasible.
  } else if (lp_config.primal_infeasible && lp_config.dual_infeasible) {
    REQUIRE((certificate.certificate_type ==
                 conic::PRIMAL_INFEASIBILITY_CERTIFICATE ||
             certificate.certificate_type ==
                 conic::DUAL_INFEASIBILITY_CERTIFICATE));
  } else if (lp_config.primal_infeasible) {
    REQUIRE(certificate.certificate_type ==
            conic::PRIMAL_INFEASIBILITY_CERTIFICATE);
  } else {
    REQUIRE(lp_config.dual_infeasible);
    REQUIRE(certificate.certificate_type ==
            conic::DUAL_INFEASIBILITY_CERTIFICATE);
  }
  std::cout << std::endl;
}

TEST_CASE("ceria3d", "[ceria3d]") {
  typedef double Real;
  const TestConfig<Real> lp_config{
      kInfeasibleNetlib + std::string("ceria3d.mps"), /* filename */
      false,                                          /* verbose */
      false,                                          /* print_data */
      Real(5e-6),                                     /* relative_error_tol */
      true,                                           /* primal_infeasible */
      false,                                          /* dual_infeasible */
      false, /* approximately_feasible */
  };
  Test(lp_config);
}

#ifdef TEST_DOUBLEDOUBLE
TEST_CASE("ceria3d DoubleDouble", "[ceria3d DoubleDouble]") {
  typedef mantis::DoubleMantissa<double> Real;
  const TestConfig<Real> lp_config{
      kInfeasibleNetlib + std::string("ceria3d.mps"), /* filename */
      false,                                          /* verbose */
      false,                                          /* print_data */
      Real(5e-13),                                    /* relative_error_tol */
      true,                                           /* primal_infeasible */
      false,                                          /* dual_infeasible */
      false, /* approximately_feasible */
  };
  Test(lp_config);
}
#endif  // ifdef TEST_DOUBLEDOUBLE

TEST_CASE("chemcom", "[chemcom]") {
  typedef double Real;
  const TestConfig<Real> lp_config{
      kInfeasibleNetlib + std::string("chemcom.mps"), /* filename */
      false,                                          /* verbose */
      false,                                          /* print_data */
      Real(5e-6),                                     /* relative_error_tol */
      true,                                           /* primal_infeasible */
      false,                                          /* dual_infeasible */
      false, /* approximately_feasible */
  };
  Test(lp_config);
}

#ifdef TEST_DOUBLEDOUBLE
TEST_CASE("chemcom DoubleDouble", "[chemcom DoubleDouble]") {
  typedef mantis::DoubleMantissa<double> Real;
  const TestConfig<Real> lp_config{
      kInfeasibleNetlib + std::string("chemcom.mps"), /* filename */
      false,                                          /* verbose */
      false,                                          /* print_data */
      Real(5e-13),                                    /* relative_error_tol */
      true,                                           /* primal_infeasible */
      false,                                          /* dual_infeasible */
      false, /* approximately_feasible */
  };
  Test(lp_config);
}
#endif  // ifdef TEST_DOUBLEDOUBLE

TEST_CASE("cplex1", "[cplex1]") {
  typedef double Real;
  const TestConfig<Real> lp_config{
      kInfeasibleNetlib + std::string("cplex1.mps"), /* filename */
      false,                                         /* verbose */
      false,                                         /* print_data */
      Real(5e-6),                                    /* relative_error_tol */
      false,                                         /* primal_infeasible */
      true,                                          /* dual_infeasible */
      false, /* approximately_feasible */
  };
  Test(lp_config);
}

#ifdef TEST_DOUBLEDOUBLE
TEST_CASE("cplex1 DoubleDouble", "[cplex1 DoubleDouble]") {
  typedef mantis::DoubleMantissa<double> Real;
  const TestConfig<Real> lp_config{
      kInfeasibleNetlib + std::string("cplex1.mps"), /* filename */
      false,                                         /* verbose */
      false,                                         /* print_data */
      Real(5e-13),                                   /* relative_error_tol */
      false,                                         /* primal_infeasible */
      true,                                          /* dual_infeasible */
      false, /* approximately_feasible */
  };
  Test(lp_config);
}
#endif  // ifdef TEST_DOUBLEDOUBLE

TEST_CASE("cplex2", "[cplex2]") {
  typedef double Real;
  const TestConfig<Real> lp_config{
      kInfeasibleNetlib + std::string("cplex2.mps"), /* filename */
      false,                                         /* verbose */
      false,                                         /* print_data */
      Real(5e-6),                                    /* relative_error_tol */
      true,                                          /* primal_infeasible */
      false,                                         /* dual_infeasible */
      true, /* approximately_feasible */
  };
  Test(lp_config);
}

#ifdef TEST_DOUBLEDOUBLE
TEST_CASE("cplex2 DoubleDouble", "[cplex2 DoubleDouble]") {
  typedef mantis::DoubleMantissa<double> Real;
  const TestConfig<Real> lp_config{
      kInfeasibleNetlib + std::string("cplex2.mps"), /* filename */
      false,                                         /* verbose */
      false,                                         /* print_data */
      Real(5e-13),                                   /* relative_error_tol */
      true,                                          /* primal_infeasible */
      false,                                         /* dual_infeasible */
      false, /* approximately_feasible */
  };
  Test(lp_config);
}
#endif  // ifdef TEST_DOUBLEDOUBLE

TEST_CASE("forest6", "[forest6]") {
  typedef double Real;
  const TestConfig<Real> lp_config{
      kInfeasibleNetlib + std::string("forest6.mps"), /* filename */
      false,                                          /* verbose */
      false,                                          /* print_data */
      Real(5e-6),                                     /* relative_error_tol */
      true,                                           /* primal_infeasible */
      false,                                          /* dual_infeasible */
      false, /* approximately_feasible */
  };
  Test(lp_config);
}

#ifdef TEST_DOUBLEDOUBLE
TEST_CASE("forest6 DoubleDouble", "[forest6 DoubleDouble]") {
  typedef mantis::DoubleMantissa<double> Real;
  const TestConfig<Real> lp_config{
      kInfeasibleNetlib + std::string("forest6.mps"), /* filename */
      false,                                          /* verbose */
      false,                                          /* print_data */
      Real(5e-13),                                    /* relative_error_tol */
      true,                                           /* primal_infeasible */
      false,                                          /* dual_infeasible */
      false, /* approximately_feasible */
  };
  Test(lp_config);
}
#endif  // ifdef TEST_DOUBLEDOUBLE

TEST_CASE("galenet", "[galenet]") {
  typedef double Real;
  const TestConfig<Real> lp_config{
      kInfeasibleNetlib + std::string("galenet.mps"), /* filename */
      false,                                          /* verbose */
      false,                                          /* print_data */
      Real(5e-6),                                     /* relative_error_tol */
      true,                                           /* primal_infeasible */
      false,                                          /* dual_infeasible */
      false, /* approximately_feasible */
  };
  Test(lp_config);
}

#ifdef TEST_DOUBLEDOUBLE
TEST_CASE("galenet DoubleDouble", "[galenet DoubleDouble]") {
  typedef mantis::DoubleMantissa<double> Real;
  const TestConfig<Real> lp_config{
      kInfeasibleNetlib + std::string("galenet.mps"), /* filename */
      false,                                          /* verbose */
      false,                                          /* print_data */
      Real(5e-13),                                    /* relative_error_tol */
      true,                                           /* primal_infeasible */
      false,                                          /* dual_infeasible */
      false, /* approximately_feasible */
  };
  Test(lp_config);
}
#endif  // ifdef TEST_DOUBLEDOUBLE

TEST_CASE("greenbea", "[greenbea]") {
  typedef double Real;
  const TestConfig<Real> lp_config{
      kInfeasibleNetlib + std::string("greenbea.mps"), /* filename */
      false,                                           /* verbose */
      false,                                           /* print_data */
      Real(5e-6),                                      /* relative_error_tol */
      true,                                            /* primal_infeasible */
      false,                                           /* dual_infeasible */
      false, /* approximately_feasible */
  };
  Test(lp_config);
}

#ifdef TEST_DOUBLEDOUBLE
TEST_CASE("greenbea DoubleDouble", "[greenbea DoubleDouble]") {
  typedef mantis::DoubleMantissa<double> Real;
  const TestConfig<Real> lp_config{
      kInfeasibleNetlib + std::string("greenbea.mps"), /* filename */
      false,                                           /* verbose */
      false,                                           /* print_data */
      Real(5e-13),                                     /* relative_error_tol */
      true,                                            /* primal_infeasible */
      false,                                           /* dual_infeasible */
      false, /* approximately_feasible */
  };
  Test(lp_config);
}
#endif  // ifdef TEST_DOUBLEDOUBLE

TEST_CASE("itest2", "[itest2]") {
  typedef double Real;
  const TestConfig<Real> lp_config{
      kInfeasibleNetlib + std::string("itest2.mps"), /* filename */
      false,                                         /* verbose */
      false,                                         /* print_data */
      Real(5e-6),                                    /* relative_error_tol */
      true,                                          /* primal_infeasible */
      false,                                         /* dual_infeasible */
      false, /* approximately_feasible */
  };
  Test(lp_config);
}

#ifdef TEST_DOUBLEDOUBLE
TEST_CASE("itest2 DoubleDouble", "[itest2 DoubleDouble]") {
  typedef mantis::DoubleMantissa<double> Real;
  const TestConfig<Real> lp_config{
      kInfeasibleNetlib + std::string("itest2.mps"), /* filename */
      false,                                         /* verbose */
      false,                                         /* print_data */
      Real(5e-13),                                   /* relative_error_tol */
      true,                                          /* primal_infeasible */
      false,                                         /* dual_infeasible */
      false, /* approximately_feasible */
  };
  Test(lp_config);
}
#endif  // ifdef TEST_DOUBLEDOUBLE

TEST_CASE("itest6", "[itest6]") {
  typedef double Real;
  const TestConfig<Real> lp_config{
      kInfeasibleNetlib + std::string("itest6.mps"), /* filename */
      false,                                         /* verbose */
      false,                                         /* print_data */
      Real(5e-6),                                    /* relative_error_tol */
      true,                                          /* primal_infeasible */
      false,                                         /* dual_infeasible */
      false, /* approximately_feasible */
  };
  Test(lp_config);
}

#ifdef TEST_DOUBLEDOUBLE
TEST_CASE("itest6 DoubleDouble", "[itest6 DoubleDouble]") {
  typedef mantis::DoubleMantissa<double> Real;
  const TestConfig<Real> lp_config{
      kInfeasibleNetlib + std::string("itest6.mps"), /* filename */
      false,                                         /* verbose */
      false,                                         /* print_data */
      Real(5e-13),                                   /* relative_error_tol */
      true,                                          /* primal_infeasible */
      false,                                         /* dual_infeasible */
      false, /* approximately_feasible */
  };
  Test(lp_config);
}
#endif  // ifdef TEST_DOUBLEDOUBLE

TEST_CASE("klein1", "[klein1]") {
  typedef double Real;
  const TestConfig<Real> lp_config{
      kInfeasibleNetlib + std::string("klein1.mps"), /* filename */
      false,                                         /* verbose */
      false,                                         /* print_data */
      Real(5e-6),                                    /* relative_error_tol */
      true,                                          /* primal_infeasible */
      false,                                         /* dual_infeasible */
      false, /* approximately_feasible */
  };
  Test(lp_config);
}

#ifdef TEST_DOUBLEDOUBLE
TEST_CASE("klein1 DoubleDouble", "[klein1 DoubleDouble]") {
  typedef mantis::DoubleMantissa<double> Real;
  const TestConfig<Real> lp_config{
      kInfeasibleNetlib + std::string("klein1.mps"), /* filename */
      false,                                         /* verbose */
      false,                                         /* print_data */
      Real(5e-13),                                   /* relative_error_tol */
      true,                                          /* primal_infeasible */
      false,                                         /* dual_infeasible */
      false, /* approximately_feasible */
  };
  Test(lp_config);
}
#endif  // ifdef TEST_DOUBLEDOUBLE

TEST_CASE("klein2", "[klein2]") {
  typedef double Real;
  const TestConfig<Real> lp_config{
      kInfeasibleNetlib + std::string("klein2.mps"), /* filename */
      false,                                         /* verbose */
      false,                                         /* print_data */
      Real(5e-6),                                    /* relative_error_tol */
      true,                                          /* primal_infeasible */
      false,                                         /* dual_infeasible */
      false, /* approximately_feasible */
  };
  Test(lp_config);
}

#ifdef TEST_DOUBLEDOUBLE
TEST_CASE("klein2 DoubleDouble", "[klein2 DoubleDouble]") {
  typedef mantis::DoubleMantissa<double> Real;
  const TestConfig<Real> lp_config{
      kInfeasibleNetlib + std::string("klein2.mps"), /* filename */
      false,                                         /* verbose */
      false,                                         /* print_data */
      Real(5e-13),                                   /* relative_error_tol */
      true,                                          /* primal_infeasible */
      false,                                         /* dual_infeasible */
      false, /* approximately_feasible */
  };
  Test(lp_config);
}
#endif  // ifdef TEST_DOUBLEDOUBLE

TEST_CASE("klein3", "[klein3]") {
  typedef double Real;
  const TestConfig<Real> lp_config{
      kInfeasibleNetlib + std::string("klein3.mps"), /* filename */
      false,                                         /* verbose */
      false,                                         /* print_data */
      Real(5e-6),                                    /* relative_error_tol */
      true,                                          /* primal_infeasible */
      false,                                         /* dual_infeasible */
      false, /* approximately_feasible */
  };
  Test(lp_config);
}

#ifdef TEST_DOUBLEDOUBLE
TEST_CASE("klein3 DoubleDouble", "[klein3 DoubleDouble]") {
  typedef mantis::DoubleMantissa<double> Real;
  const TestConfig<Real> lp_config{
      kInfeasibleNetlib + std::string("klein3.mps"), /* filename */
      false,                                         /* verbose */
      false,                                         /* print_data */
      Real(5e-13),                                   /* relative_error_tol */
      true,                                          /* primal_infeasible */
      false,                                         /* dual_infeasible */
      false, /* approximately_feasible */
  };
  Test(lp_config);
}
#endif  // ifdef TEST_DOUBLEDOUBLE

TEST_CASE("mondou2", "[mondou2]") {
  typedef double Real;
  const TestConfig<Real> lp_config{
      kInfeasibleNetlib + std::string("mondou2.mps"), /* filename */
      false,                                          /* verbose */
      false,                                          /* print_data */
      Real(5e-6),                                     /* relative_error_tol */
      true,                                           /* primal_infeasible */
      false,                                          /* dual_infeasible */
      false, /* approximately_feasible */
  };
  Test(lp_config);
}

#ifdef TEST_DOUBLEDOUBLE
TEST_CASE("mondou2 DoubleDouble", "[mondou2 DoubleDouble]") {
  typedef mantis::DoubleMantissa<double> Real;
  const TestConfig<Real> lp_config{
      kInfeasibleNetlib + std::string("mondou2.mps"), /* filename */
      false,                                          /* verbose */
      false,                                          /* print_data */
      Real(5e-13),                                    /* relative_error_tol */
      true,                                           /* primal_infeasible */
      false,                                          /* dual_infeasible */
      false, /* approximately_feasible */
  };
  Test(lp_config);
}
#endif  // ifdef TEST_DOUBLEDOUBLE

TEST_CASE("qual", "[qual]") {
  typedef double Real;
  const TestConfig<Real> lp_config{
      kInfeasibleNetlib + std::string("qual.mps"), /* filename */
      false,                                       /* verbose */
      false,                                       /* print_data */
      Real(5e-6),                                  /* relative_error_tol */
      true,                                        /* primal_infeasible */
      false,                                       /* dual_infeasible */
      false,                                       /* approximately_feasible */
  };
  Test(lp_config);
}

#ifdef TEST_DOUBLEDOUBLE
TEST_CASE("qual DoubleDouble", "[qual DoubleDouble]") {
  typedef mantis::DoubleMantissa<double> Real;
  const TestConfig<Real> lp_config{
      kInfeasibleNetlib + std::string("qual.mps"), /* filename */
      false,                                       /* verbose */
      false,                                       /* print_data */
      Real(5e-13),                                 /* relative_error_tol */
      true,                                        /* primal_infeasible */
      false,                                       /* dual_infeasible */
      false,                                       /* approximately_feasible */
  };
  Test(lp_config);
}
#endif  // ifdef TEST_DOUBLEDOUBLE

TEST_CASE("reactor", "[reactor]") {
  typedef double Real;
  const TestConfig<Real> lp_config{
      kInfeasibleNetlib + std::string("reactor.mps"), /* filename */
      false,                                          /* verbose */
      false,                                          /* print_data */
      Real(5e-6),                                     /* relative_error_tol */
      true,                                           /* primal_infeasible */
      false,                                          /* dual_infeasible */
      false, /* approximately_feasible */
  };
  Test(lp_config);
}

#ifdef TEST_DOUBLEDOUBLE
TEST_CASE("reactor DoubleDouble", "[reactor DoubleDouble]") {
  typedef mantis::DoubleMantissa<double> Real;
  const TestConfig<Real> lp_config{
      kInfeasibleNetlib + std::string("reactor.mps"), /* filename */
      false,                                          /* verbose */
      false,                                          /* print_data */
      Real(5e-13),                                    /* relative_error_tol */
      true,                                           /* primal_infeasible */
      false,                                          /* dual_infeasible */
      false, /* approximately_feasible */
  };
  Test(lp_config);
}
#endif  // ifdef TEST_DOUBLEDOUBLE

TEST_CASE("refinery", "[refinery]") {
  typedef double Real;
  const TestConfig<Real> lp_config{
      kInfeasibleNetlib + std::string("refinery.mps"), /* filename */
      false,                                           /* verbose */
      false,                                           /* print_data */
      Real(5e-6),                                      /* relative_error_tol */
      true,                                            /* primal_infeasible */
      false,                                           /* dual_infeasible */
      false, /* approximately_feasible */
  };
  Test(lp_config);
}

#ifdef TEST_DOUBLEDOUBLE
TEST_CASE("refinery DoubleDouble", "[refinery DoubleDouble]") {
  typedef mantis::DoubleMantissa<double> Real;
  const TestConfig<Real> lp_config{
      kInfeasibleNetlib + std::string("refinery.mps"), /* filename */
      false,                                           /* verbose */
      false,                                           /* print_data */
      Real(5e-13),                                     /* relative_error_tol */
      true,                                            /* primal_infeasible */
      false,                                           /* dual_infeasible */
      false, /* approximately_feasible */
  };
  Test(lp_config);
}
#endif  // ifdef TEST_DOUBLEDOUBLE

TEST_CASE("vol1", "[vol1]") {
  typedef double Real;
  const TestConfig<Real> lp_config{
      kInfeasibleNetlib + std::string("vol1.mps"), /* filename */
      false,                                       /* verbose */
      false,                                       /* print_data */
      Real(5e-6),                                  /* relative_error_tol */
      true,                                        /* primal_infeasible */
      false,                                       /* dual_infeasible */
      false,                                       /* approximately_feasible */
  };
  Test(lp_config);
}

#ifdef TEST_DOUBLEDOUBLE
TEST_CASE("vol1 DoubleDouble", "[vol1 DoubleDouble]") {
  typedef mantis::DoubleMantissa<double> Real;
  const TestConfig<Real> lp_config{
      kInfeasibleNetlib + std::string("vol1.mps"), /* filename */
      false,                                       /* verbose */
      false,                                       /* print_data */
      Real(5e-13),                                 /* relative_error_tol */
      true,                                        /* primal_infeasible */
      false,                                       /* dual_infeasible */
      false,                                       /* approximately_feasible */
  };
  Test(lp_config);
}
#endif  // ifdef TEST_DOUBLEDOUBLE

#ifdef TEST_WOODINFE
TEST_CASE("woodinfe", "[woodinfe]") {
  typedef double Real;
  const TestConfig<Real> lp_config{
      kInfeasibleNetlib + std::string("woodinfe.mps"), /* filename */
      false,                                           /* verbose */
      false,                                           /* print_data */
      Real(5e-6),                                      /* relative_error_tol */
      true,                                            /* primal_infeasible */
      false,                                           /* dual_infeasible */
      true, /* approximately_feasible */
  };
  Test(lp_config);
}

#ifdef TEST_DOUBLEDOUBLE
TEST_CASE("woodinfe DoubleDouble", "[woodinfe DoubleDouble]") {
  typedef mantis::DoubleMantissa<double> Real;
  const TestConfig<Real> lp_config{
      kInfeasibleNetlib + std::string("woodinfe.mps"), /* filename */
      false,                                           /* verbose */
      false,                                           /* print_data */
      Real(5e-13),                                     /* relative_error_tol */
      true,                                            /* primal_infeasible */
      false,                                           /* dual_infeasible */
      true, /* approximately_feasible */
  };
  Test(lp_config);
}
#endif  // ifdef TEST_DOUBLEDOUBLE
#endif  // ifdef TEST_WOODINFE
