/*
 * Copyright (c) 2019 Jack Poulson <jack@hodgestar.com>
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
#define CATCH_CONFIG_MAIN
#include <iostream>
#include <limits>
#include "catch2/catch.hpp"
#include "conic.hpp"
#include "quotient/timer.hpp"

// This test suite attempts to compute Delsarte upper bounds for the number of
// code words in linear codes over F_q^n with a given minimal distance.

// If defined, we also run tests using double-double arithmetic.
#define TEST_DOUBLEDOUBLE

// We use the test from SAGE:
//
//   sage: codes.bounds.krawtchouk(24,2,5,4)
//   2224
//
template <typename Real>
void SageKravchukExample(Real relative_tol) {
  const conic::Int n = 24;
  const conic::Int field_order = 2;
  const conic::Int degree = 5;
  const conic::Int argument = 4;
  const Real value =
      conic::hsd_qp::KravchukPolynomial<Real>(field_order, n, degree, argument);

  const Real target_value = 2224;
  const Real relative_error =
      std::abs(value - target_value) / (std::abs(target_value) + 1);
  REQUIRE(relative_error <= relative_tol);
}

// The SAGE documentation at
//   http://doc.sagemath.org/html/en/reference/coding/sage/coding/delsarte_bounds.html
// gives the example:
//
//   The bound on the size of the F2-codes of length 11 and minimal distance 6:
//
//     sage: codes.bounds.delsarte_bound_hamming_space(11, 6, 2)
//     12
//
// where 11 is the code length, 6 is the minimal distance, and 2 is the field
// order.
template <typename Real>
void SageExample1(bool verbose, Real relative_objective_tol) {
  const conic::Int field_order = 2;
  const conic::Int code_length = 11;
  const conic::Int distance = 6;
  const Real target_bound = Real(12);

  conic::hsd_qp::SolveControl<Real> control;
  control.output.verbose = verbose;
  const Real bound =
      conic::hsd_qp::DelsarteBound(field_order, code_length, distance, control);

  const Real relative_obj_deviation =
      std::abs((bound)-target_bound) / (std::abs(target_bound) + 1);
  REQUIRE(relative_obj_deviation <= relative_objective_tol);
}

// We test an analogue of Example 1 which SAGE gives as:
//
//   The bound on the size of the F2-codes of length 24 and minimal distance 8,
//   i.e. parameters of the extended binary Golay code:
//
//     sage: a,p,x = codes.bounds.delsarte_bound_hamming_space(
//         24,8,2,return_data=True)
//     sage: x
//     4096
//     sage: [j for i,j in p.get_values(a).items()]
//     [1, 0, 0, 0, 0, 0, 0, 0, 759, 0, 0, 0, 2576, 0, 0, 0, 759, 0, 0, 0, 0, 0,
//         0, 0, 1]
//
template <typename Real>
void SageExample2(bool verbose, Real relative_objective_tol) {
  const conic::Int field_order = 2;
  const conic::Int code_length = 24;
  const conic::Int distance = 8;
  const Real target_bound = Real(4096);

  conic::hsd_qp::SolveControl<Real> control;
  control.output.verbose = verbose;
  const Real bound =
      conic::hsd_qp::DelsarteBound(field_order, code_length, distance, control);

  const Real relative_obj_deviation =
      std::abs((bound)-target_bound) / (std::abs(target_bound) + 1);
  REQUIRE(relative_obj_deviation <= relative_objective_tol);
}

// An analogue of the above test:
//   sage: codes.bounds.delsarte_bound_additive_hamming_space(11,3,4)
//   327680/3
template <typename Real>
void SageExample3(bool verbose, Real relative_objective_tol) {
  const conic::Int field_order = 4;
  const conic::Int code_length = 11;
  const conic::Int distance = 3;
  const Real target_bound = Real(327680) / Real(3);

  conic::hsd_qp::SolveControl<Real> control;
  control.output.verbose = verbose;
  const Real bound =
      conic::hsd_qp::DelsarteBound(field_order, code_length, distance, control);

  const Real relative_obj_deviation =
      std::abs((bound)-target_bound) / (std::abs(target_bound) + 1);
  REQUIRE(relative_obj_deviation <= relative_objective_tol);
}

// The last test from SAGE corresponds to a demonstration that an integer LP
// solver can achieve much tighter bounds in some cases than a standard LP
// solver:
//
//   An improvement of a known upper bound (150) from
//   https://www.win.tue.nl/~aeb/codes/binary-1.html
//
//     sage: a,p,x = codes.bounds.delsarte_bound_hamming_space(23,10,2,
//         return_data=True,isinteger=True); x # long time
//     148
//     sage: [j for i,j in p.get_values(a).items()]   # long time
//     [1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 95, 0, 2, 0, 36, 0, 14, 0, 0, 0, 0, 0, 0,
//         0]
//
//   Note that a usual LP, without integer variables, won’t do the trick
//
//     sage: codes.bounds.delsarte_bound_hamming_space(23,10,2).n(20)
//     151.86
//
template <typename Real>
void SageExample4(bool verbose, Real relative_objective_tol) {
  const conic::Int field_order = 2;
  const conic::Int code_length = 23;
  const conic::Int distance = 10;
  const Real target_bound = Real(1.51864406779661016948991859134914e2);

  conic::hsd_qp::SolveControl<Real> control;
  control.output.verbose = verbose;
  const Real bound =
      conic::hsd_qp::DelsarteBound(field_order, code_length, distance, control);

  const Real relative_obj_deviation =
      std::abs((bound)-target_bound) / (std::abs(target_bound) + 1);
  REQUIRE(relative_obj_deviation <= relative_objective_tol);
}

TEST_CASE("SageKravchuk double", "[SageKravchuk double]") {
  const double relative_tol = 1e-5;
  SageKravchukExample(relative_tol);
}

#ifdef TEST_DOUBLEDOUBLE
TEST_CASE("SageKravchuk DoubleDouble", "[SageKravchuk DoubleDouble]") {
  const mantis::DoubleMantissa<double> relative_tol = 1e-12;
  SageKravchukExample(relative_tol);
}
#endif  // ifdef TEST_DOUBLEDOUBLE

TEST_CASE("SageExample1 double", "[SageExample1 double]") {
  const bool kVerbose = false;
  const double relative_objective_tol = 1e-5;
  SageExample1(kVerbose, relative_objective_tol);
}

#ifdef TEST_DOUBLEDOUBLE
TEST_CASE("SageExample1 DoubleDouble", "[SageExample1 DoubleDouble]") {
  const bool kVerbose = false;
  const mantis::DoubleMantissa<double> relative_objective_tol = 1e-12;
  SageExample1(kVerbose, relative_objective_tol);
}
#endif  // ifdef TEST_DOUBLEDOUBLE

TEST_CASE("SageExample2 double", "[SageExample2 double]") {
  const bool kVerbose = false;
  const double relative_objective_tol = 1e-5;
  SageExample2(kVerbose, relative_objective_tol);
}

#ifdef TEST_DOUBLEDOUBLE
TEST_CASE("SageExample2 DoubleDouble", "[SageExample2 DoubleDouble]") {
  const bool kVerbose = false;
  const mantis::DoubleMantissa<double> relative_objective_tol = 1e-12;
  SageExample2(kVerbose, relative_objective_tol);
}
#endif  // ifdef TEST_DOUBLEDOUBLE

TEST_CASE("SageExample3 double", "[SageExample3 double]") {
  const bool kVerbose = false;
  const double relative_objective_tol = 1e-5;
  SageExample3(kVerbose, relative_objective_tol);
}

#ifdef TEST_DOUBLEDOUBLE
TEST_CASE("SageExample3 DoubleDouble", "[SageExample3 DoubleDouble]") {
  const bool kVerbose = false;
  const mantis::DoubleMantissa<double> relative_objective_tol = 1e-12;
  SageExample3(kVerbose, relative_objective_tol);
}
#endif  // ifdef TEST_DOUBLEDOUBLE

TEST_CASE("SageExample4 double", "[SageExample4 double]") {
  const bool kVerbose = false;
  const double relative_objective_tol = 1e-5;
  SageExample4(kVerbose, relative_objective_tol);
}

#ifdef TEST_DOUBLEDOUBLE
TEST_CASE("SageExample4 DoubleDouble", "[SageExample4 DoubleDouble]") {
  const bool kVerbose = false;
  const mantis::DoubleMantissa<double> relative_objective_tol = 1e-12;
  SageExample4(kVerbose, relative_objective_tol);
}
#endif  // ifdef TEST_DOUBLEDOUBLE
