/*
 * Copyright (c) 2019 Jack Poulson <jack@hodgestar.com>
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
#define CATCH_CONFIG_MAIN
#include <iostream>
#include <limits>
#include "catch2/catch.hpp"
#include "conic.hpp"
#include "quotient/timer.hpp"

using conic::ConstBlasMatrixView;
using conic::EuclideanNorm;
using conic::Int;

// If defined, the tests are also run in double-double arithmetic.
#define TEST_DOUBLEDOUBLE

// It appears that there is an approximate primal infeasibility certificate
// computable in double-double arithmetic for qpilotno.
//
// After 61 iterations:
//
// tau:         8.12788686575013599130981322228064e-30
// kappa:       9.78070751643210002711592677415343e-16
// || y ||_2:   3.01468753765991551761233341119413e7
// || z ||_2:   3.01511470623819041528071150604771e7
// || b ||_2:   2.54419737957198961098746020422616e5
//
// || A' y + z || / |b' y|: 6.18888294373045029678649865448285e-15
//
// Given that the only difference between this quadratic program, qpilotno,
// and the NETLIB LP feasibility test pilotnov, is the quadratic objective,
// this should also imply an approximate primal infeasibility certificate for
// the linear test. But we have not encountered said result in practice.
//
//#define TEST_QPILOTNO_DOUBLEDOUBLE

// The location of the Maros/Meszaros tests.
static const char kMarosMeszaros[] = "../data/maros_meszaros/";

// Configuration for test of Maros/Meszaros test.
template <typename Real>
struct TestConfig {
  // The location of the file containing the (uncompressed) MPS file.
  std::string filename;

  // Whether the solver should have 'verbose' level printing.
  bool verbose = false;

  // Whether the presolve should attempt to tighten infinite lower and upper
  // bounds (which introduces additional variables in the standard form).
  bool tighten_infinite_bounds = false;

  // Whether the problem data and solution should be printed.
  bool print_data = false;

  // The target primal objective value.
  Real objective;

  // The acceptable relative objective deviation from the target value.
  Real relative_objective_tol;
};

template <typename Real>
void Solve(const TestConfig<Real>& config,
           const conic::bounded_qp::Problem<Real>& bounded_problem,
           const conic::QuadraticProgramMPSInfo<Real>& bounded_info,
           const conic::BoundedToStandardQPReduction<Real>& reduction,
           const conic::hsd_qp::Problem<Real>& standard_problem,
           bool iterate_normalized) {
  conic::hsd_qp::SolveControl<Real> control;

  const std::string normalization_string =
      iterate_normalized ? "iterate" : "RHS";

  std::cout << "Testing " << config.filename << " with " << normalization_string
            << "-normalization" << std::endl;

  control.output.verbose = config.verbose;
  control.optimality.iterate_normalization = iterate_normalized;

  static const Real kEpsilon = std::numeric_limits<Real>::epsilon();
  const Real relative_error_tol =
      control.optimality.relative_tolerance_coefficient *
      std::pow(kEpsilon, control.optimality.relative_tolerance_exponent);

  conic::hsd_qp::Iterate<Real> standard_certificate;
  quotient::Timer timer;
  timer.Start();
  const Int num_iterations =
      conic::hsd_qp::Solve(standard_problem, control, &standard_certificate);
  timer.Stop();
  if (config.print_data) {
    std::cout << "Standard certificate:\n" << standard_certificate << std::endl;
  }
  standard_problem.PrintNorms(std::cout);
  standard_certificate.PrintNorms(std::cout);

  conic::hsd_qp::Residuals<Real> standard_residuals;
  standard_residuals.iterate_normalization =
      control.optimality.iterate_normalization;
  standard_residuals.FillRelativeError(standard_problem, standard_certificate);
  std::cout << config.filename << " ran for " << num_iterations
            << " iterations (" << timer.TotalSeconds()
            << " seconds) w/ relative error "
            << standard_residuals.relative_error << std::endl;
  const Real standard_primal_objective =
      standard_residuals.objective.primal + reduction.ObjectiveShift();
  const Real standard_relative_obj_deviation =
      std::abs(standard_primal_objective - config.objective) /
      (std::abs(config.objective) + 1);

  std::cout << "standard_primal_objective: " << standard_primal_objective
            << std::endl;
  std::cout << "relative_obj_deviation: " << standard_relative_obj_deviation
            << std::endl;
  standard_residuals.PrintRelativeError(std::cout);

  // Backtransform the standard-form iterate and compute its primal objective.
  conic::bounded_qp::Iterate<Real> bounded_certificate;
  reduction.BacktransformIterate(standard_certificate, &bounded_certificate);
  if (config.print_data) {
    std::cout << "Bounded certificate:\n" << bounded_certificate << std::endl;
  }
  const catamari::ConstBlasMatrixView<Real> bounded_primal_solution =
      bounded_certificate.PrimalSolution();
  Real bounded_primal_objective = bounded_info.objective_shift;
  const conic::Int num_primal_bounded =
      bounded_problem.equality_matrix.NumColumns();
  for (conic::Int i = 0; i < num_primal_bounded; ++i) {
    bounded_primal_objective +=
        bounded_problem.linear_objective(i) * bounded_primal_solution(i);
  }
  for (const auto& entry : bounded_problem.quadratic_objective.Entries()) {
    bounded_primal_objective += bounded_primal_solution(entry.row) *
                                entry.value *
                                bounded_primal_solution(entry.column) / Real(2);
  }
  std::cout << "bounded_primal_objective: " << bounded_primal_objective
            << std::endl;
  const Real bounded_relative_obj_deviation =
      std::abs(bounded_primal_objective - config.objective) /
      (std::abs(config.objective) + 1);

  // Manually compute the primal feasibility residual for the bounded cert.
  const Real bounded_equality_rhs_two_norm =
      catamari::EuclideanNorm(bounded_problem.equality_rhs.ConstView());
  catamari::BlasMatrix<Real> bounded_residual = bounded_problem.equality_rhs;
  catamari::ApplySparse(Real(-1), bounded_problem.equality_matrix,
                        bounded_primal_solution, Real(1),
                        &bounded_residual.view);
  const Real bounded_primal_equality_residual_norm =
      catamari::EuclideanNorm(bounded_residual.ConstView());
  const Real bounded_primal_equality_relative_residual =
      bounded_primal_equality_residual_norm /
      (bounded_equality_rhs_two_norm + 1);
  std::cout << "bounded || b ||_2: " << bounded_equality_rhs_two_norm << "\n"
            << "bounded || b - A x ||_2 / (|| b ||_2 + 1): "
            << bounded_primal_equality_relative_residual << std::endl;

  // Print any entries of the bounded problem which are outside of the bounds.
  Real max_distance = 0;
  Real max_relative_distance = 0;
  for (conic::Int i = 0; i < num_primal_bounded; ++i) {
    const Real lower_bound = bounded_problem.lower_bounds[i];
    const Real upper_bound = bounded_problem.upper_bounds[i];
    const Real value = bounded_primal_solution(i);
    if (std::isfinite(lower_bound) && std::isfinite(upper_bound)) {
      if (value < lower_bound || value > upper_bound) {
        const Real distance =
            value < lower_bound ? lower_bound - value : value - upper_bound;
        const Real relative_distance =
            value < lower_bound ? distance / (std::abs(lower_bound) + 1)
                                : distance / (std::abs(upper_bound) + 1);

        std::cout << "bounded primal solution entry " << i << " was "
                  << static_cast<double>(value) << ", which is " << distance
                  << " away from [" << static_cast<double>(lower_bound) << ", "
                  << static_cast<double>(upper_bound) << "]" << std::endl;

        max_distance = std::max(max_distance, distance);
        max_relative_distance =
            std::max(max_relative_distance, relative_distance);
      }
    } else if (std::isfinite(lower_bound)) {
      if (value < lower_bound) {
        const Real distance = lower_bound - value;
        const Real relative_distance = distance / (std::abs(lower_bound) + 1);

        std::cout << "bounded primal solution entry " << i << " was "
                  << static_cast<double>(value) << ", which was less than "
                  << static_cast<double>(lower_bound) << std::endl;

        max_distance = std::max(max_distance, distance);
        max_relative_distance =
            std::max(max_relative_distance, relative_distance);
      }
    } else if (std::isfinite(upper_bound)) {
      if (value > upper_bound) {
        const Real distance = value - upper_bound;
        const Real relative_distance = distance / (std::abs(upper_bound) + 1);

        max_distance = std::max(max_distance, distance);
        std::cout << "bounded primal solution entry " << i << " was "
                  << static_cast<double>(value) << ", which was greater than "
                  << static_cast<double>(upper_bound) << std::endl;

        max_distance = std::max(max_distance, distance);
        max_relative_distance =
            std::max(max_relative_distance, relative_distance);
      }
    }
  }
  if (max_distance > Real(0)) {
    std::cout << "Maximum distance from bound was " << max_distance
              << std::endl;
    std::cout << "Maximum relative distance was " << max_relative_distance
              << std::endl;
  }

  REQUIRE(standard_certificate.certificate_type ==
          conic::OPTIMALITY_CERTIFICATE);
  REQUIRE(standard_residuals.relative_error <= relative_error_tol);
  REQUIRE(standard_relative_obj_deviation <= config.relative_objective_tol);
  REQUIRE(num_iterations < control.max_iterations);
  REQUIRE(bounded_relative_obj_deviation <= config.relative_objective_tol);

  std::cout << std::endl;
}

template <typename Real>
void Test(const TestConfig<Real>& test_config) {
  conic::ReadQuadraticProgramMPSConfig read_config;
  read_config.filename = test_config.filename;

  conic::bounded_qp::Problem<Real> bounded_problem;
  auto bounded_info =
      conic::ReadQuadraticProgramMPS(read_config, &bounded_problem);

  // Run a tightening procedure on the lower and upper bounds.
  bounded_problem.TightenBounds(test_config.tighten_infinite_bounds,
                                test_config.verbose);

  conic::BoundedToStandardQPReduction<Real> reduction(bounded_problem);
  conic::hsd_qp::Problem<Real> standard_problem;
  reduction.FormStandardProblem(&standard_problem);
  const Real objective_shift = reduction.ObjectiveShift();

  std::cout << "info.bounds_section.num_fixed_bounds: "
            << bounded_info.bounds_section.num_fixed_bounds << "\n"
            << "info.bounds_section.num_upper_bounds: "
            << bounded_info.bounds_section.num_upper_bounds << "\n"
            << "info.bounds_section.num_lower_bounds: "
            << bounded_info.bounds_section.num_lower_bounds << "\n"
            << "info.bounds_section.num_free_bounds: "
            << bounded_info.bounds_section.num_free_bounds << "\n"
            << "info.row_section.num_equality_rows: "
            << bounded_info.row_section.num_equality_rows << "\n"
            << "info.objective_shift: " << bounded_info.objective_shift << "\n"
            << "reduction.ObjectiveShift(): " << objective_shift << std::endl;

  if (test_config.print_data) {
    std::cout << "Bounded problem:\n" << bounded_problem << std::endl;
    std::cout << "Standard problem:\n" << standard_problem << std::endl;
  }

  // We no longer test iterate-normalization, because the BOYD1 example fails
  // and it is not clear why we should normalize a residual || b - A x ||
  // with || x || when || A || and || b || are large but || x || is small.
  Solve(test_config, bounded_problem, bounded_info, reduction, standard_problem,
        false /* iterate_normalized */);
}

// The listing on http://www.doc.ic.ac.uk/%7Eim/00README.QP is:
//     1.6874118e+06.

TEST_CASE("aug2d double", "[aug2d double]") {
  typedef double Real;
  const TestConfig<Real> test_config{
      kMarosMeszaros + std::string("aug2d.sif"), /* filename */
      false,                                     /* verbose */
      false,                                     /* tighten_infinite_bounds */
      false,                                     /* print_data */
      Real(1.687411752896737e6),                 /* objective */
      Real(1e-7),                                /* relative_objective_tol */
  };
  Test(test_config);
}

#ifdef TEST_DOUBLEDOUBLE
TEST_CASE("aug2d DoubleDouble", "[aug2d DoubleDouble]") {
  typedef mantis::DoubleMantissa<double> Real;
  const TestConfig<Real> test_config{
      kMarosMeszaros + std::string("aug2d.sif"), /* filename */
      false,                                     /* verbose */
      false,                                     /* tighten_infinite_bounds */
      false,                                     /* print_data */
      Real(1.687411752896737e6),                 /* objective */
      Real(1e-14),                               /* relative_objective_tol */
  };
  Test(test_config);
}
#endif  // ifdef TEST_DOUBLEDOUBLE

// The listing on http://www.doc.ic.ac.uk/%7Eim/00README.QP is:
//     1.8183681e+06.

TEST_CASE("aug2dc double", "[aug2dc double]") {
  typedef double Real;
  const TestConfig<Real> test_config{
      kMarosMeszaros + std::string("aug2dc.sif"), /* filename */
      false,                                      /* verbose */
      false,                                      /* tighten_infinite_bounds */
      false,                                      /* print_data */
      Real(1.818368065570202e6),                  /* objective */
      Real(1e-7),                                 /* relative_objective_tol */
  };
  Test(test_config);
}

#ifdef TEST_DOUBLEDOUBLE
TEST_CASE("aug2dc DoubleDouble", "[aug2dc DoubleDouble]") {
  typedef mantis::DoubleMantissa<double> Real;
  const TestConfig<Real> test_config{
      kMarosMeszaros + std::string("aug2dc.sif"), /* filename */
      false,                                      /* verbose */
      false,                                      /* tighten_infinite_bounds */
      false,                                      /* print_data */
      Real(1.818368065570202e6),                  /* objective */
      Real(1e-14),                                /* relative_objective_tol */
  };
  Test(test_config);
}
#endif  // ifdef TEST_DOUBLEDOUBLE

// The listing on http://www.doc.ic.ac.uk/%7Eim/00README.QP is:
//     6.4981348e+06.

TEST_CASE("aug2dcqp double", "[aug2dcqp double]") {
  typedef double Real;
  const TestConfig<Real> test_config{
      kMarosMeszaros + std::string("aug2dcqp.sif"), /* filename */
      false,                                        /* verbose */
      false,                     /* tighten_infinite_bounds */
      false,                     /* print_data */
      Real(6.498134739473210e6), /* objective */
      Real(1e-7),                /* relative_objective_tol */
  };
  Test(test_config);
}

#ifdef TEST_DOUBLEDOUBLE
TEST_CASE("aug2dcqp DoubleDouble", "[aug2dcqp DoubleDouble]") {
  typedef mantis::DoubleMantissa<double> Real;
  const TestConfig<Real> test_config{
      kMarosMeszaros + std::string("aug2dcqp.sif"), /* filename */
      false,                                        /* verbose */
      false,                     /* tighten_infinite_bounds */
      false,                     /* print_data */
      Real(6.498134739473210e6), /* objective */
      Real(1e-14),               /* relative_objective_tol */
  };
  Test(test_config);
}
#endif  // ifdef TEST_DOUBLEDOUBLE

// The listing on http://www.doc.ic.ac.uk/%7Eim/00README.QP is:
//     6.2370121e+06.

TEST_CASE("aug2dqp double", "[aug2dqp double]") {
  typedef double Real;
  const TestConfig<Real> test_config{
      kMarosMeszaros + std::string("aug2dqp.sif"), /* filename */
      false,                                       /* verbose */
      false,                                       /* tighten_infinite_bounds */
      false,                                       /* print_data */
      Real(6.237012025567496e6),                   /* objective */
      Real(1e-7),                                  /* relative_objective_tol */
  };
  Test(test_config);
}

#ifdef TEST_DOUBLEDOUBLE
TEST_CASE("aug2dqp DoubleDouble", "[aug2dqp DoubleDouble]") {
  typedef mantis::DoubleMantissa<double> Real;
  const TestConfig<Real> test_config{
      kMarosMeszaros + std::string("aug2dqp.sif"), /* filename */
      false,                                       /* verbose */
      false,                                       /* tighten_infinite_bounds */
      false,                                       /* print_data */
      Real(6.237012025567496e6),                   /* objective */
      Real(1e-14),                                 /* relative_objective_tol */
  };
  Test(test_config);
}
#endif  // ifdef TEST_DOUBLEDOUBLE

// The listing on http://www.doc.ic.ac.uk/%7Eim/00README.QP is:
//     5.5406773e+02.

TEST_CASE("aug3d double", "[aug3d double]") {
  typedef double Real;
  const TestConfig<Real> test_config{
      kMarosMeszaros + std::string("aug3d.sif"), /* filename */
      false,                                     /* verbose */
      false,                                     /* tighten_infinite_bounds */
      false,                                     /* print_data */
      Real(5.540677257925280e2),                 /* objective */
      Real(1e-7),                                /* relative_objective_tol */
  };
  Test(test_config);
}

#ifdef TEST_DOUBLEDOUBLE
TEST_CASE("aug3d DoubleDouble", "[aug3d DoubleDouble]") {
  typedef mantis::DoubleMantissa<double> Real;
  const TestConfig<Real> test_config{
      kMarosMeszaros + std::string("aug3d.sif"), /* filename */
      false,                                     /* verbose */
      false,                                     /* tighten_infinite_bounds */
      false,                                     /* print_data */
      Real(5.540677257925280e2),                 /* objective */
      Real(1e-14),                               /* relative_objective_tol */
  };
  Test(test_config);
}
#endif  // ifdef TEST_DOUBLEDOUBLE

// The listing on http://www.doc.ic.ac.uk/%7Eim/00README.QP is:
//     -6.1735220e+07.

TEST_CASE("boyd1 double", "[boyd1 double]") {
  typedef double Real;
  const TestConfig<Real> test_config{
      kMarosMeszaros + std::string("boyd1.sif"), /* filename */
      false,                                     /* verbose */
      false,                                     /* tighten_infinite_bounds */
      false,                                     /* print_data */
      Real(-6.173521964094309e7),                /* objective */
      Real(1e-7),                                /* relative_objective_tol */
  };
  Test(test_config);
}

#ifdef TEST_DOUBLEDOUBLE
TEST_CASE("boyd1 DoubleDouble", "[boyd1 DoubleDouble]") {
  typedef mantis::DoubleMantissa<double> Real;
  const TestConfig<Real> test_config{
      kMarosMeszaros + std::string("boyd1.sif"), /* filename */
      false,                                     /* verbose */
      false,                                     /* tighten_infinite_bounds */
      false,                                     /* print_data */
      Real(-6.173521964094309e7),                /* objective */
      Real(1e-14),                               /* relative_objective_tol */
  };
  Test(test_config);
}
#endif  // ifdef TEST_DOUBLEDOUBLE

// The listing on http://www.doc.ic.ac.uk/%7Eim/00README.QP is:
//     2.1256767e+01.

TEST_CASE("boyd2 double", "[boyd2 double]") {
  typedef double Real;
  const TestConfig<Real> test_config{
      kMarosMeszaros + std::string("boyd2.sif"), /* filename */
      false,                                     /* verbose */
      false,                                     /* tighten_infinite_bounds */
      false,                                     /* print_data */
      Real(2.125676700384852e1),                 /* objective */
      Real(1e-7),                                /* relative_objective_tol */
  };
  Test(test_config);
}

#ifdef TEST_DOUBLEDOUBLE
TEST_CASE("boyd2 DoubleDouble", "[boyd2 DoubleDouble]") {
  typedef mantis::DoubleMantissa<double> Real;
  const TestConfig<Real> test_config{
      kMarosMeszaros + std::string("boyd2.sif"), /* filename */
      false,                                     /* verbose */
      false,                                     /* tighten_infinite_bounds */
      false,                                     /* print_data */
      Real(2.125676700384852e1),                 /* objective */
      Real(1e-14),                               /* relative_objective_tol */
  };
  Test(test_config);
}
#endif  // ifdef TEST_DOUBLEDOUBLE

// The listing on http://www.doc.ic.ac.uk/%7Eim/00README.QP is:
//     -4.5638509e+00.

TEST_CASE("cont-050 double", "[cont-050 double]") {
  typedef double Real;
  const TestConfig<Real> test_config{
      kMarosMeszaros + std::string("cont-050.sif"), /* filename */
      false,                                        /* verbose */
      false,                    /* tighten_infinite_bounds */
      false,                    /* print_data */
      Real(-4.563850904324670), /* objective */
      Real(1e-7),               /* relative_objective_tol */
  };
  Test(test_config);
}

#ifdef TEST_DOUBLEDOUBLE
TEST_CASE("cont-050 DoubleDouble", "[cont-050 DoubleDouble]") {
  typedef mantis::DoubleMantissa<double> Real;
  const TestConfig<Real> test_config{
      kMarosMeszaros + std::string("cont-050.sif"), /* filename */
      false,                                        /* verbose */
      false,                    /* tighten_infinite_bounds */
      false,                    /* print_data */
      Real(-4.563850904324670), /* objective */
      Real(1e-14),              /* relative_objective_tol */
  };
  Test(test_config);
}
#endif  // ifdef TEST_DOUBLEDOUBLE

// The listing on http://www.doc.ic.ac.uk/%7Eim/00README.QP is:
//     -4.6443979e+00.

TEST_CASE("cont-100 double", "[cont-100 double]") {
  typedef double Real;
  const TestConfig<Real> test_config{
      kMarosMeszaros + std::string("cont-100.sif"), /* filename */
      false,                                        /* verbose */
      false,                    /* tighten_infinite_bounds */
      false,                    /* print_data */
      Real(-4.644397868763516), /* objective */
      Real(1e-7),               /* relative_objective_tol */
  };
  Test(test_config);
}

#ifdef TEST_DOUBLEDOUBLE
TEST_CASE("cont-100 DoubleDouble", "[cont-100 DoubleDouble]") {
  typedef mantis::DoubleMantissa<double> Real;
  const TestConfig<Real> test_config{
      kMarosMeszaros + std::string("cont-100.sif"), /* filename */
      false,                                        /* verbose */
      false,                    /* tighten_infinite_bounds */
      false,                    /* print_data */
      Real(-4.644397868763516), /* objective */
      Real(1e-14),              /* relative_objective_tol */
  };
  Test(test_config);
}
#endif  // ifdef TEST_DOUBLEDOUBLE

// The listing on http://www.doc.ic.ac.uk/%7Eim/00README.QP is:
//     1.9552733e-01.

TEST_CASE("cont-101 double", "[cont-101 double]") {
  typedef double Real;
  const TestConfig<Real> test_config{
      kMarosMeszaros + std::string("cont-101.sif"), /* filename */
      false,                                        /* verbose */
      false,                      /* tighten_infinite_bounds */
      false,                      /* print_data */
      Real(1.955273246764465e-1), /* objective */
      Real(1e-7),                 /* relative_objective_tol */
  };
  Test(test_config);
}

#ifdef TEST_DOUBLEDOUBLE
TEST_CASE("cont-101 DoubleDouble", "[cont-101 DoubleDouble]") {
  typedef mantis::DoubleMantissa<double> Real;
  const TestConfig<Real> test_config{
      kMarosMeszaros + std::string("cont-101.sif"), /* filename */
      false,                                        /* verbose */
      false,                      /* tighten_infinite_bounds */
      false,                      /* print_data */
      Real(1.955273246764465e-1), /* objective */
      Real(1e-14),                /* relative_objective_tol */
  };
  Test(test_config);
}
#endif  // ifdef TEST_DOUBLEDOUBLE

// The listing on http://www.doc.ic.ac.uk/%7Eim/00README.QP is:
//     -4.6848759e+00.

TEST_CASE("cont-200 double", "[cont-200 double]") {
  typedef double Real;
  const TestConfig<Real> test_config{
      kMarosMeszaros + std::string("cont-200.sif"), /* filename */
      false,                                        /* verbose */
      false,                    /* tighten_infinite_bounds */
      false,                    /* print_data */
      Real(-4.684875920369524), /* objective */
      Real(1e-7),               /* relative_objective_tol */
  };
  Test(test_config);
}

#ifdef TEST_DOUBLEDOUBLE
TEST_CASE("cont-200 DoubleDouble", "[cont-200 DoubleDouble]") {
  typedef mantis::DoubleMantissa<double> Real;
  const TestConfig<Real> test_config{
      kMarosMeszaros + std::string("cont-200.sif"), /* filename */
      false,                                        /* verbose */
      false,                    /* tighten_infinite_bounds */
      false,                    /* print_data */
      Real(-4.684875920369524), /* objective */
      Real(1e-14),              /* relative_objective_tol */
  };
  Test(test_config);
}
#endif  // ifdef TEST_DOUBLEDOUBLE

// The listing on http://www.doc.ic.ac.uk/%7Eim/00README.QP is:
//     1.9248337e-01.

TEST_CASE("cont-201 double", "[cont-201 double]") {
  typedef double Real;
  const TestConfig<Real> test_config{
      kMarosMeszaros + std::string("cont-201.sif"), /* filename */
      false,                                        /* verbose */
      false,                      /* tighten_infinite_bounds */
      false,                      /* print_data */
      Real(1.924833730281630e-1), /* objective */
      Real(1e-7),                 /* relative_objective_tol */
  };
  Test(test_config);
}

#ifdef TEST_DOUBLEDOUBLE
TEST_CASE("cont-201 DoubleDouble", "[cont-201 DoubleDouble]") {
  typedef mantis::DoubleMantissa<double> Real;
  const TestConfig<Real> test_config{
      kMarosMeszaros + std::string("cont-201.sif"), /* filename */
      false,                                        /* verbose */
      false,                      /* tighten_infinite_bounds */
      false,                      /* print_data */
      Real(1.924833730281630e-1), /* objective */
      Real(1e-14),                /* relative_objective_tol */
  };
  Test(test_config);
}
#endif  // ifdef TEST_DOUBLEDOUBLE

// The listing on http://www.doc.ic.ac.uk/%7Eim/00README.QP is:
//     1.9151232e-01.

TEST_CASE("cont-300 double", "[cont-300 double]") {
  typedef double Real;
  const TestConfig<Real> test_config{
      kMarosMeszaros + std::string("cont-300.sif"), /* filename */
      false,                                        /* verbose */
      false,                      /* tighten_infinite_bounds */
      false,                      /* print_data */
      Real(1.915122860727015e-1), /* objective */
      Real(1e-7),                 /* relative_objective_tol */
  };
  Test(test_config);
}

#ifdef TEST_DOUBLEDOUBLE
TEST_CASE("cont-300 DoubleDouble", "[cont-300 DoubleDouble]") {
  typedef mantis::DoubleMantissa<double> Real;
  const TestConfig<Real> test_config{
      kMarosMeszaros + std::string("cont-300.sif"), /* filename */
      false,                                        /* verbose */
      false,                      /* tighten_infinite_bounds */
      false,                      /* print_data */
      Real(1.915122860727015e-1), /* objective */
      Real(1e-14),                /* relative_objective_tol */
  };
  Test(test_config);
}
#endif  // ifdef TEST_DOUBLEDOUBLE

// The listing on http://www.doc.ic.ac.uk/%7Eim/00README.QP is:
//     1.0870480e+08.

TEST_CASE("cvxqp1l double", "[cvxqp1l double]") {
  typedef double Real;
  const TestConfig<Real> test_config{
      kMarosMeszaros + std::string("cvxqp1l.sif"), /* filename */
      false,                                       /* verbose */
      false,                                       /* tighten_infinite_bounds */
      false,                                       /* print_data */
      Real(1.087047999154672e8),                   /* objective */
      Real(1e-7),                                  /* relative_objective_tol */
  };
  Test(test_config);
}

#ifdef TEST_DOUBLEDOUBLE
TEST_CASE("cvxqp1l DoubleDouble", "[cvxqp1l DoubleDouble]") {
  typedef mantis::DoubleMantissa<double> Real;
  const TestConfig<Real> test_config{
      kMarosMeszaros + std::string("cvxqp1l.sif"), /* filename */
      false,                                       /* verbose */
      false,                                       /* tighten_infinite_bounds */
      false,                                       /* print_data */
      Real(1.087047999154672e8),                   /* objective */
      Real(1e-14),                                 /* relative_objective_tol */
  };
  Test(test_config);
}
#endif  // ifdef TEST_DOUBLEDOUBLE

// The listing on http://www.doc.ic.ac.uk/%7Eim/00README.QP is:
//     1.0875116e+06.

TEST_CASE("cvxqp1m double", "[cvxqp1m double]") {
  typedef double Real;
  const TestConfig<Real> test_config{
      kMarosMeszaros + std::string("cvxqp1m.sif"), /* filename */
      false,                                       /* verbose */
      false,                                       /* tighten_infinite_bounds */
      false,                                       /* print_data */
      Real(1.087511567321501e6),                   /* objective */
      Real(1e-7),                                  /* relative_objective_tol */
  };
  Test(test_config);
}

#ifdef TEST_DOUBLEDOUBLE
TEST_CASE("cvxqp1m DoubleDouble", "[cvxqp1m DoubleDouble]") {
  typedef mantis::DoubleMantissa<double> Real;
  const TestConfig<Real> test_config{
      kMarosMeszaros + std::string("cvxqp1m.sif"), /* filename */
      false,                                       /* verbose */
      false,                                       /* tighten_infinite_bounds */
      false,                                       /* print_data */
      Real(1.087511567321501e6),                   /* objective */
      Real(1e-14),                                 /* relative_objective_tol */
  };
  Test(test_config);
}
#endif  // ifdef TEST_DOUBLEDOUBLE

// The listing on http://www.doc.ic.ac.uk/%7Eim/00README.QP is:
//     1.1590718e+04.

TEST_CASE("cvxqp1s double", "[cvxqp1s double]") {
  typedef double Real;
  const TestConfig<Real> test_config{
      kMarosMeszaros + std::string("cvxqp1s.sif"), /* filename */
      false,                                       /* verbose */
      false,                                       /* tighten_infinite_bounds */
      false,                                       /* print_data */
      Real(1.159071811942677e4),                   /* objective */
      Real(1e-7),                                  /* relative_objective_tol */
  };
  Test(test_config);
}

#ifdef TEST_DOUBLEDOUBLE
TEST_CASE("cvxqp1s DoubleDouble", "[cvxqp1s DoubleDouble]") {
  typedef mantis::DoubleMantissa<double> Real;
  const TestConfig<Real> test_config{
      kMarosMeszaros + std::string("cvxqp1s.sif"), /* filename */
      false,                                       /* verbose */
      false,                                       /* tighten_infinite_bounds */
      false,                                       /* print_data */
      Real(1.159071811942677e4),                   /* objective */
      Real(1e-14),                                 /* relative_objective_tol */
  };
  Test(test_config);
}
#endif  // ifdef TEST_DOUBLEDOUBLE

// The listing on http://www.doc.ic.ac.uk/%7Eim/00README.QP is:
//     8.1842458e+07.

TEST_CASE("cvxqp2l double", "[cvxqp2l double]") {
  typedef double Real;
  const TestConfig<Real> test_config{
      kMarosMeszaros + std::string("cvxqp2l.sif"), /* filename */
      false,                                       /* verbose */
      false,                                       /* tighten_infinite_bounds */
      false,                                       /* print_data */
      Real(8.184245826284120e7),                   /* objective */
      Real(1e-7),                                  /* relative_objective_tol */
  };
  Test(test_config);
}

#ifdef TEST_DOUBLEDOUBLE
TEST_CASE("cvxqp2l DoubleDouble", "[cvxqp2l DoubleDouble]") {
  typedef mantis::DoubleMantissa<double> Real;
  const TestConfig<Real> test_config{
      kMarosMeszaros + std::string("cvxqp2l.sif"), /* filename */
      false,                                       /* verbose */
      false,                                       /* tighten_infinite_bounds */
      false,                                       /* print_data */
      Real(8.184245826284120e7),                   /* objective */
      Real(1e-14),                                 /* relative_objective_tol */
  };
  Test(test_config);
}
#endif  // ifdef TEST_DOUBLEDOUBLE

// The listing on http://www.doc.ic.ac.uk/%7Eim/00README.QP is:
//     8.2015543e+05.

TEST_CASE("cvxqp2m double", "[cvxqp2m double]") {
  typedef double Real;
  const TestConfig<Real> test_config{
      kMarosMeszaros + std::string("cvxqp2m.sif"), /* filename */
      false,                                       /* verbose */
      false,                                       /* tighten_infinite_bounds */
      false,                                       /* print_data */
      Real(8.201554310156988e5),                   /* objective */
      Real(1e-7),                                  /* relative_objective_tol */
  };
  Test(test_config);
}

#ifdef TEST_DOUBLEDOUBLE
TEST_CASE("cvxqp2m DoubleDouble", "[cvxqp2m DoubleDouble]") {
  typedef mantis::DoubleMantissa<double> Real;
  const TestConfig<Real> test_config{
      kMarosMeszaros + std::string("cvxqp2m.sif"), /* filename */
      false,                                       /* verbose */
      false,                                       /* tighten_infinite_bounds */
      false,                                       /* print_data */
      Real(8.201554310156988e5),                   /* objective */
      Real(1e-14),                                 /* relative_objective_tol */
  };
  Test(test_config);
}
#endif  // ifdef TEST_DOUBLEDOUBLE

// The listing on http://www.doc.ic.ac.uk/%7Eim/00README.QP is:
//     8.1209405e+03.

TEST_CASE("cvxqp2s double", "[cvxqp2s double]") {
  typedef double Real;
  const TestConfig<Real> test_config{
      kMarosMeszaros + std::string("cvxqp2s.sif"), /* filename */
      false,                                       /* verbose */
      false,                                       /* tighten_infinite_bounds */
      false,                                       /* print_data */
      Real(8.120940477250691e3),                   /* objective */
      Real(1e-7),                                  /* relative_objective_tol */
  };
  Test(test_config);
}

#ifdef TEST_DOUBLEDOUBLE
TEST_CASE("cvxqp2s DoubleDouble", "[cvxqp2s DoubleDouble]") {
  typedef mantis::DoubleMantissa<double> Real;
  const TestConfig<Real> test_config{
      kMarosMeszaros + std::string("cvxqp2s.sif"), /* filename */
      false,                                       /* verbose */
      false,                                       /* tighten_infinite_bounds */
      false,                                       /* print_data */
      Real(8.120940477250691e3),                   /* objective */
      Real(1e-14),                                 /* relative_objective_tol */
  };
  Test(test_config);
}
#endif  // ifdef TEST_DOUBLEDOUBLE

// The listing on http://www.doc.ic.ac.uk/%7Eim/00README.QP is:
//     1.1571110e+08.

TEST_CASE("cvxqp3l double", "[cvxqp3l double]") {
  typedef double Real;
  const TestConfig<Real> test_config{
      kMarosMeszaros + std::string("cvxqp3l.sif"), /* filename */
      false,                                       /* verbose */
      false,                                       /* tighten_infinite_bounds */
      false,                                       /* print_data */
      Real(1.157111044942059e8),                   /* objective */
      Real(1e-7),                                  /* relative_objective_tol */
  };
  Test(test_config);
}

#ifdef TEST_DOUBLEDOUBLE
TEST_CASE("cvxqp3l DoubleDouble", "[cvxqp3l DoubleDouble]") {
  typedef mantis::DoubleMantissa<double> Real;
  const TestConfig<Real> test_config{
      kMarosMeszaros + std::string("cvxqp3l.sif"), /* filename */
      false,                                       /* verbose */
      false,                                       /* tighten_infinite_bounds */
      false,                                       /* print_data */
      Real(1.157111044942059e8),                   /* objective */
      Real(1e-14),                                 /* relative_objective_tol */
  };
  Test(test_config);
}
#endif  // ifdef TEST_DOUBLEDOUBLE

// The listing on http://www.doc.ic.ac.uk/%7Eim/00README.QP is:
//     1.3628287e+06.

TEST_CASE("cvxqp3m double", "[cvxqp3m double]") {
  typedef double Real;
  const TestConfig<Real> test_config{
      kMarosMeszaros + std::string("cvxqp3m.sif"), /* filename */
      false,                                       /* verbose */
      false,                                       /* tighten_infinite_bounds */
      false,                                       /* print_data */
      Real(1.362828741602883e6),                   /* objective */
      Real(1e-7),                                  /* relative_objective_tol */
  };
  Test(test_config);
}

#ifdef TEST_DOUBLEDOUBLE
TEST_CASE("cvxqp3m DoubleDouble", "[cvxqp3m DoubleDouble]") {
  typedef mantis::DoubleMantissa<double> Real;
  const TestConfig<Real> test_config{
      kMarosMeszaros + std::string("cvxqp3m.sif"), /* filename */
      false,                                       /* verbose */
      false,                                       /* tighten_infinite_bounds */
      false,                                       /* print_data */
      Real(1.362828741602883e6),                   /* objective */
      Real(1e-14),                                 /* relative_objective_tol */
  };
  Test(test_config);
}
#endif  // ifdef TEST_DOUBLEDOUBLE

// The listing on http://www.doc.ic.ac.uk/%7Eim/00README.QP is:
//     1.1943432e+04.

TEST_CASE("cvxqp3s double", "[cvxqp3s double]") {
  typedef double Real;
  const TestConfig<Real> test_config{
      kMarosMeszaros + std::string("cvxqp3s.sif"), /* filename */
      false,                                       /* verbose */
      false,                                       /* tighten_infinite_bounds */
      false,                                       /* print_data */
      Real(1.194343220230996e4),                   /* objective */
      Real(1e-7),                                  /* relative_objective_tol */
  };
  Test(test_config);
}

#ifdef TEST_DOUBLEDOUBLE
TEST_CASE("cvxqp3s DoubleDouble", "[cvxqp3s DoubleDouble]") {
  typedef mantis::DoubleMantissa<double> Real;
  const TestConfig<Real> test_config{
      kMarosMeszaros + std::string("cvxqp3s.sif"), /* filename */
      false,                                       /* verbose */
      false,                                       /* tighten_infinite_bounds */
      false,                                       /* print_data */
      Real(1.194343220230996e4),                   /* objective */
      Real(1e-14),                                 /* relative_objective_tol */
  };
  Test(test_config);
}
#endif  // ifdef TEST_DOUBLEDOUBLE

// The listing on http://www.doc.ic.ac.uk/%7Eim/00README.QP is:
//     3.7009622e-01.

TEST_CASE("dpklo1 double", "[dpklo1 double]") {
  typedef double Real;
  const TestConfig<Real> test_config{
      kMarosMeszaros + std::string("dpklo1.sif"), /* filename */
      false,                                      /* verbose */
      false,                                      /* tighten_infinite_bounds */
      false,                                      /* print_data */
      Real(3.700962171142686e-1),                 /* objective */
      Real(1e-7),                                 /* relative_objective_tol */
  };
  Test(test_config);
}

#ifdef TEST_DOUBLEDOUBLE
TEST_CASE("dpklo1 DoubleDouble", "[dpklo1 DoubleDouble]") {
  typedef mantis::DoubleMantissa<double> Real;
  const TestConfig<Real> test_config{
      kMarosMeszaros + std::string("dpklo1.sif"), /* filename */
      false,                                      /* verbose */
      false,                                      /* tighten_infinite_bounds */
      false,                                      /* print_data */
      Real(3.700962171142686e-1),                 /* objective */
      Real(1e-14),                                /* relative_objective_tol */
  };
  Test(test_config);
}
#endif  // ifdef TEST_DOUBLEDOUBLE

// The listing on http://www.doc.ic.ac.uk/%7Eim/00README.QP is:
//     2.3526248e+02.

TEST_CASE("dtoc3 double", "[dtoc3 double]") {
  typedef double Real;
  const TestConfig<Real> test_config{
      kMarosMeszaros + std::string("dtoc3.sif"), /* filename */
      false,                                     /* verbose */
      false,                                     /* tighten_infinite_bounds */
      false,                                     /* print_data */
      Real(2.352624810352253e2),                 /* objective */
      Real(1e-7),                                /* relative_objective_tol */
  };
  Test(test_config);
}

#ifdef TEST_DOUBLEDOUBLE
TEST_CASE("dtoc3 DoubleDouble", "[dtoc3 DoubleDouble]") {
  typedef mantis::DoubleMantissa<double> Real;
  const TestConfig<Real> test_config{
      kMarosMeszaros + std::string("dtoc3.sif"), /* filename */
      false,                                     /* verbose */
      false,                                     /* tighten_infinite_bounds */
      false,                                     /* print_data */
      Real(2.352624810352253e2),                 /* objective */
      Real(1e-14),                               /* relative_objective_tol */
  };
  Test(test_config);
}
#endif  // ifdef TEST_DOUBLEDOUBLE

// The listing on http://www.doc.ic.ac.uk/%7Eim/00README.QP is:
//     3.5012966e-02.

TEST_CASE("dual1 double", "[dual1 double]") {
  typedef double Real;
  const TestConfig<Real> test_config{
      kMarosMeszaros + std::string("dual1.sif"), /* filename */
      false,                                     /* verbose */
      false,                                     /* tighten_infinite_bounds */
      false,                                     /* print_data */
      Real(3.501296573347734e-2),                /* objective */
      Real(1e-7),                                /* relative_objective_tol */
  };
  Test(test_config);
}

#ifdef TEST_DOUBLEDOUBLE
TEST_CASE("dual1 DoubleDouble", "[dual1 DoubleDouble]") {
  typedef mantis::DoubleMantissa<double> Real;
  const TestConfig<Real> test_config{
      kMarosMeszaros + std::string("dual1.sif"), /* filename */
      false,                                     /* verbose */
      false,                                     /* tighten_infinite_bounds */
      false,                                     /* print_data */
      Real(3.501296573347734e-2),                /* objective */
      Real(1e-14),                               /* relative_objective_tol */
  };
  Test(test_config);
}
#endif  // ifdef TEST_DOUBLEDOUBLE

// The listing on http://www.doc.ic.ac.uk/%7Eim/00README.QP is:
//     3.3733676e-02.

TEST_CASE("dual2 double", "[dual2 double]") {
  typedef double Real;
  const TestConfig<Real> test_config{
      kMarosMeszaros + std::string("dual2.sif"), /* filename */
      false,                                     /* verbose */
      false,                                     /* tighten_infinite_bounds */
      false,                                     /* print_data */
      Real(3.373367612271853e-2),                /* objective */
      Real(1e-7),                                /* relative_objective_tol */
  };
  Test(test_config);
}

#ifdef TEST_DOUBLEDOUBLE
TEST_CASE("dual2 DoubleDouble", "[dual2 DoubleDouble]") {
  typedef mantis::DoubleMantissa<double> Real;
  const TestConfig<Real> test_config{
      kMarosMeszaros + std::string("dual2.sif"), /* filename */
      false,                                     /* verbose */
      false,                                     /* tighten_infinite_bounds */
      false,                                     /* print_data */
      Real(3.373367612271853e-2),                /* objective */
      Real(1e-14),                               /* relative_objective_tol */
  };
  Test(test_config);
}
#endif  // ifdef TEST_DOUBLEDOUBLE

// The listing on http://www.doc.ic.ac.uk/%7Eim/00README.QP is:
//     1.3575584e-01.

TEST_CASE("dual3 double", "[dual3 double]") {
  typedef double Real;
  const TestConfig<Real> test_config{
      kMarosMeszaros + std::string("dual3.sif"), /* filename */
      false,                                     /* verbose */
      false,                                     /* tighten_infinite_bounds */
      false,                                     /* print_data */
      Real(1.357558368660210e-1),                /* objective */
      Real(1e-7),                                /* relative_objective_tol */
  };
  Test(test_config);
}

#ifdef TEST_DOUBLEDOUBLE
TEST_CASE("dual3 DoubleDouble", "[dual3 DoubleDouble]") {
  typedef mantis::DoubleMantissa<double> Real;
  const TestConfig<Real> test_config{
      kMarosMeszaros + std::string("dual3.sif"), /* filename */
      false,                                     /* verbose */
      false,                                     /* tighten_infinite_bounds */
      false,                                     /* print_data */
      Real(1.357558368660210e-1),                /* objective */
      Real(1e-14),                               /* relative_objective_tol */
  };
  Test(test_config);
}
#endif  // ifdef TEST_DOUBLEDOUBLE

// The listing on http://www.doc.ic.ac.uk/%7Eim/00README.QP is:
//     7.4609084e-01.

TEST_CASE("dual4 double", "[dual4 double]") {
  typedef double Real;
  const TestConfig<Real> test_config{
      kMarosMeszaros + std::string("dual4.sif"), /* filename */
      false,                                     /* verbose */
      false,                                     /* tighten_infinite_bounds */
      false,                                     /* print_data */
      Real(7.460908418021001e-1),                /* objective */
      Real(1e-7),                                /* relative_objective_tol */
  };
  Test(test_config);
}

#ifdef TEST_DOUBLEDOUBLE
TEST_CASE("dual4 DoubleDouble", "[dual4 DoubleDouble]") {
  typedef mantis::DoubleMantissa<double> Real;
  const TestConfig<Real> test_config{
      kMarosMeszaros + std::string("dual4.sif"), /* filename */
      false,                                     /* verbose */
      false,                                     /* tighten_infinite_bounds */
      false,                                     /* print_data */
      Real(7.460908418021001e-1),                /* objective */
      Real(1e-14),                               /* relative_objective_tol */
  };
  Test(test_config);
}
#endif  // ifdef TEST_DOUBLEDOUBLE

// The listing on http://www.doc.ic.ac.uk/%7Eim/00README.QP is:
//     6.1552508e+03.

TEST_CASE("dualc1 double", "[dualc1 double]") {
  typedef double Real;
  const TestConfig<Real> test_config{
      kMarosMeszaros + std::string("dualc1.sif"), /* filename */
      false,                                      /* verbose */
      false,                                      /* tighten_infinite_bounds */
      false,                                      /* print_data */
      Real(6.155250829462685e3),                  /* objective */
      Real(1e-7),                                 /* relative_objective_tol */
  };
  Test(test_config);
}

#ifdef TEST_DOUBLEDOUBLE
TEST_CASE("dualc1 DoubleDouble", "[dualc1 DoubleDouble]") {
  typedef mantis::DoubleMantissa<double> Real;
  const TestConfig<Real> test_config{
      kMarosMeszaros + std::string("dualc1.sif"), /* filename */
      false,                                      /* verbose */
      false,                                      /* tighten_infinite_bounds */
      false,                                      /* print_data */
      Real(6.155250829462685e3),                  /* objective */
      Real(1e-14),                                /* relative_objective_tol */
  };
  Test(test_config);
}
#endif  // ifdef TEST_DOUBLEDOUBLE

// The listing on http://www.doc.ic.ac.uk/%7Eim/00README.QP is:
//     3.5513077e+03.

TEST_CASE("dualc2 double", "[dualc2 double]") {
  typedef double Real;
  const TestConfig<Real> test_config{
      kMarosMeszaros + std::string("dualc2.sif"), /* filename */
      false,                                      /* verbose */
      false,                                      /* tighten_infinite_bounds */
      false,                                      /* print_data */
      Real(3.551307692670643e3),                  /* objective */
      Real(1e-7),                                 /* relative_objective_tol */
  };
  Test(test_config);
}

#ifdef TEST_DOUBLEDOUBLE
TEST_CASE("dualc2 DoubleDouble", "[dualc2 DoubleDouble]") {
  typedef mantis::DoubleMantissa<double> Real;
  const TestConfig<Real> test_config{
      kMarosMeszaros + std::string("dualc2.sif"), /* filename */
      false,                                      /* verbose */
      false,                                      /* tighten_infinite_bounds */
      false,                                      /* print_data */
      Real(3.551307692670643e3),                  /* objective */
      Real(1e-14),                                /* relative_objective_tol */
  };
  Test(test_config);
}
#endif  // ifdef TEST_DOUBLEDOUBLE

// The listing on http://www.doc.ic.ac.uk/%7Eim/00README.QP is:
//     4.2723233e+02.

TEST_CASE("dualc5 double", "[dualc5 double]") {
  typedef double Real;
  const TestConfig<Real> test_config{
      kMarosMeszaros + std::string("dualc5.sif"), /* filename */
      false,                                      /* verbose */
      false,                                      /* tighten_infinite_bounds */
      false,                                      /* print_data */
      Real(4.272323267763890e2),                  /* objective */
      Real(1e-7),                                 /* relative_objective_tol */
  };
  Test(test_config);
}

#ifdef TEST_DOUBLEDOUBLE
TEST_CASE("dualc5 DoubleDouble", "[dualc5 DoubleDouble]") {
  typedef mantis::DoubleMantissa<double> Real;
  const TestConfig<Real> test_config{
      kMarosMeszaros + std::string("dualc5.sif"), /* filename */
      false,                                      /* verbose */
      false,                                      /* tighten_infinite_bounds */
      false,                                      /* print_data */
      Real(4.272323267763890e2),                  /* objective */
      Real(1e-14),                                /* relative_objective_tol */
  };
  Test(test_config);
}
#endif  // ifdef TEST_DOUBLEDOUBLE

// The listing on http://www.doc.ic.ac.uk/%7Eim/00README.QP is:
//     1.8309359e+04.

TEST_CASE("dualc8 double", "[dualc8 double]") {
  typedef double Real;
  const TestConfig<Real> test_config{
      kMarosMeszaros + std::string("dualc8.sif"), /* filename */
      false,                                      /* verbose */
      false,                                      /* tighten_infinite_bounds */
      false,                                      /* print_data */
      Real(1.830935883273415e4),                  /* objective */
      Real(1e-7),                                 /* relative_objective_tol */
  };
  Test(test_config);
}

#ifdef TEST_DOUBLEDOUBLE
TEST_CASE("dualc8 DoubleDouble", "[dualc8 DoubleDouble]") {
  typedef mantis::DoubleMantissa<double> Real;
  const TestConfig<Real> test_config{
      kMarosMeszaros + std::string("dualc8.sif"), /* filename */
      false,                                      /* verbose */
      false,                                      /* tighten_infinite_bounds */
      false,                                      /* print_data */
      Real(1.830935883273415e4),                  /* objective */
      Real(1e-14),                                /* relative_objective_tol */
  };
  Test(test_config);
}
#endif  // ifdef TEST_DOUBLEDOUBLE

// The listing on http://www.doc.ic.ac.uk/%7Eim/00README.QP is:
//     -1.4184343e+02.

TEST_CASE("exdata double", "[exdata double]") {
  typedef double Real;
  const TestConfig<Real> test_config{
      kMarosMeszaros + std::string("exdata.sif"), /* filename */
      false,                                      /* verbose */
      false,                                      /* tighten_infinite_bounds */
      false,                                      /* print_data */
      Real(-1.418434321893292e2),                 /* objective */
      Real(5e-7),                                 /* relative_objective_tol */
  };
  Test(test_config);
}

#ifdef TEST_DOUBLEDOUBLE
TEST_CASE("exdata DoubleDouble", "[exdata DoubleDouble]") {
  typedef mantis::DoubleMantissa<double> Real;
  const TestConfig<Real> test_config{
      kMarosMeszaros + std::string("exdata.sif"), /* filename */
      false,                                      /* verbose */
      false,                                      /* tighten_infinite_bounds */
      false,                                      /* print_data */
      Real(-1.418434321893292e2),                 /* objective */
      Real(1e-12),                                /* relative_objective_tol */
  };
  Test(test_config);
}
#endif  // ifdef TEST_DOUBLEDOUBLE

// The listing on http://www.doc.ic.ac.uk/%7Eim/00README.QP is:
//     9.2717369e-01.

TEST_CASE("genhs28 double", "[genhs28 double]") {
  typedef double Real;
  const TestConfig<Real> test_config{
      kMarosMeszaros + std::string("genhs28.sif"), /* filename */
      false,                                       /* verbose */
      false,                                       /* tighten_infinite_bounds */
      false,                                       /* print_data */
      Real(9.271736937663910e-1),                  /* objective */
      Real(1e-7),                                  /* relative_objective_tol */
  };
  Test(test_config);
}

#ifdef TEST_DOUBLEDOUBLE
TEST_CASE("genhs28 DoubleDouble", "[genhs28 DoubleDouble]") {
  typedef mantis::DoubleMantissa<double> Real;
  const TestConfig<Real> test_config{
      kMarosMeszaros + std::string("genhs28.sif"), /* filename */
      false,                                       /* verbose */
      false,                                       /* tighten_infinite_bounds */
      false,                                       /* print_data */
      Real(9.271736937663910e-1),                  /* objective */
      Real(1e-14),                                 /* relative_objective_tol */
  };
  Test(test_config);
}
#endif  // ifdef TEST_DOUBLEDOUBLE

// The listing on http://www.doc.ic.ac.uk/%7Eim/00README.QP is:
//     1.8427534e-04.

TEST_CASE("gouldqp2 double", "[gouldqp2 double]") {
  typedef double Real;
  const TestConfig<Real> test_config{
      kMarosMeszaros + std::string("gouldqp2.sif"), /* filename */
      false,                                        /* verbose */
      false,                      /* tighten_infinite_bounds */
      false,                      /* print_data */
      Real(1.842745033625465e-4), /* objective */
      Real(1e-7),                 /* relative_objective_tol */
  };
  Test(test_config);
}

#ifdef TEST_DOUBLEDOUBLE
TEST_CASE("gouldqp2 DoubleDouble", "[gouldqp2 DoubleDouble]") {
  typedef mantis::DoubleMantissa<double> Real;
  const TestConfig<Real> test_config{
      kMarosMeszaros + std::string("gouldqp2.sif"), /* filename */
      false,                                        /* verbose */
      false,                      /* tighten_infinite_bounds */
      false,                      /* print_data */
      Real(1.842745033625465e-4), /* objective */
      Real(1e-14),                /* relative_objective_tol */
  };
  Test(test_config);
}
#endif  // ifdef TEST_DOUBLEDOUBLE

// The listing on http://www.doc.ic.ac.uk/%7Eim/00README.QP is:
//     2.0627840e+00.

TEST_CASE("gouldqp3 double", "[gouldqp3 double]") {
  typedef double Real;
  const TestConfig<Real> test_config{
      kMarosMeszaros + std::string("gouldqp3.sif"), /* filename */
      false,                                        /* verbose */
      false,                   /* tighten_infinite_bounds */
      false,                   /* print_data */
      Real(2.062783971464402), /* objective */
      Real(1e-7),              /* relative_objective_tol */
  };
  Test(test_config);
}

#ifdef TEST_DOUBLEDOUBLE
TEST_CASE("gouldqp3 DoubleDouble", "[gouldqp3 DoubleDouble]") {
  typedef mantis::DoubleMantissa<double> Real;
  const TestConfig<Real> test_config{
      kMarosMeszaros + std::string("gouldqp3.sif"), /* filename */
      false,                                        /* verbose */
      false,                   /* tighten_infinite_bounds */
      false,                   /* print_data */
      Real(2.062783971464402), /* objective */
      Real(1e-14),             /* relative_objective_tol */
  };
  Test(test_config);
}
#endif  // ifdef TEST_DOUBLEDOUBLE

// The listing on http://www.doc.ic.ac.uk/%7Eim/00README.QP is:
//     6.6482045e+02.

TEST_CASE("hs118 double", "[hs118 double]") {
  typedef double Real;
  const TestConfig<Real> test_config{
      kMarosMeszaros + std::string("hs118.sif"), /* filename */
      false,                                     /* verbose */
      false,                                     /* tighten_infinite_bounds */
      false,                                     /* print_data */
      Real(6.648204500000000e+02),               /* objective */
      Real(1e-7),                                /* relative_objective_tol */
  };
  Test(test_config);
}

#ifdef TEST_DOUBLEDOUBLE
TEST_CASE("hs118 DoubleDouble", "[hs118 DoubleDouble]") {
  typedef mantis::DoubleMantissa<double> Real;
  const TestConfig<Real> test_config{
      kMarosMeszaros + std::string("hs118.sif"), /* filename */
      false,                                     /* verbose */
      false,                                     /* tighten_infinite_bounds */
      false,                                     /* print_data */
      Real(6.648204500000000e+02),               /* objective */
      Real(1e-14),                               /* relative_objective_tol */
  };
  Test(test_config);
}
#endif  // ifdef TEST_DOUBLEDOUBLE

// The listing on http://www.doc.ic.ac.uk/%7Eim/00README.QP is:
//     -9.9960000e+01.

TEST_CASE("hs21 double", "[hs21 double]") {
  typedef double Real;
  const TestConfig<Real> test_config{
      kMarosMeszaros + std::string("hs21.sif"), /* filename */
      false,                                    /* verbose */
      false,                                    /* tighten_infinite_bounds */
      false,                                    /* print_data */
      Real(-9.996000000000000e+01),             /* objective */
      Real(1e-7),                               /* relative_objective_tol */
  };
  Test(test_config);
}

#ifdef TEST_DOUBLEDOUBLE
TEST_CASE("hs21 DoubleDouble", "[hs21 DoubleDouble]") {
  typedef mantis::DoubleMantissa<double> Real;
  const TestConfig<Real> test_config{
      kMarosMeszaros + std::string("hs21.sif"), /* filename */
      false,                                    /* verbose */
      false,                                    /* tighten_infinite_bounds */
      false,                                    /* print_data */
      Real(-9.996000000000000e+01),             /* objective */
      Real(1e-14),                              /* relative_objective_tol */
  };
  Test(test_config);
}
#endif  // ifdef TEST_DOUBLEDOUBLE

// The listing on http://www.doc.ic.ac.uk/%7Eim/00README.QP is:
//     5.7310705e-07.
// It appears that there are zero correct significant digits and that the
// optimal solution is converging to zero as the precision increases.

TEST_CASE("hs268 double", "[hs268 double]") {
  typedef double Real;
  const TestConfig<Real> test_config{
      kMarosMeszaros + std::string("hs268.sif"), /* filename */
      false,                                     /* verbose */
      false,                                     /* tighten_infinite_bounds */
      false,                                     /* print_data */
      Real(-8.057379427496146e-28),              /* objective */
      Real(1e-7),                                /* relative_objective_tol */
  };
  Test(test_config);
}

#ifdef TEST_DOUBLEDOUBLE
TEST_CASE("hs268 DoubleDouble", "[hs268 DoubleDouble]") {
  typedef mantis::DoubleMantissa<double> Real;
  const TestConfig<Real> test_config{
      kMarosMeszaros + std::string("hs268.sif"), /* filename */
      false,                                     /* verbose */
      false,                                     /* tighten_infinite_bounds */
      false,                                     /* print_data */
      Real(-8.057379427496146e-28),              /* objective */
      Real(1e-14),                               /* relative_objective_tol */
  };
  Test(test_config);
}
#endif  // ifdef TEST_DOUBLEDOUBLE

// The listing on http://www.doc.ic.ac.uk/%7Eim/00README.QP is:
//     1.1111111e-01.

TEST_CASE("hs35 double", "[hs35 double]") {
  typedef double Real;
  const TestConfig<Real> test_config{
      kMarosMeszaros + std::string("hs35.sif"), /* filename */
      false,                                    /* verbose */
      false,                                    /* tighten_infinite_bounds */
      false,                                    /* print_data */
      Real(1.111111111111111e-1),               /* objective */
      Real(1e-7),                               /* relative_objective_tol */
  };
  Test(test_config);
}

#ifdef TEST_DOUBLEDOUBLE
TEST_CASE("hs35 DoubleDouble", "[hs35 DoubleDouble]") {
  typedef mantis::DoubleMantissa<double> Real;
  const TestConfig<Real> test_config{
      kMarosMeszaros + std::string("hs35.sif"), /* filename */
      false,                                    /* verbose */
      false,                                    /* tighten_infinite_bounds */
      false,                                    /* print_data */
      Real(1.111111111111111e-1),               /* objective */
      Real(1e-14),                              /* relative_objective_tol */
  };
  Test(test_config);
}
#endif  // ifdef TEST_DOUBLEDOUBLE

// The listing on http://www.doc.ic.ac.uk/%7Eim/00README.QP is:
//     2.5000000e-01.

TEST_CASE("hs35mod double", "[hs35mod double]") {
  typedef double Real;
  const TestConfig<Real> test_config{
      kMarosMeszaros + std::string("hs35mod.sif"), /* filename */
      false,                                       /* verbose */
      false,                                       /* tighten_infinite_bounds */
      false,                                       /* print_data */
      Real(2.500000000000000e-01),                 /* objective */
      Real(1e-7),                                  /* relative_objective_tol */
  };
  Test(test_config);
}

#ifdef TEST_DOUBLEDOUBLE
TEST_CASE("hs35mod DoubleDouble", "[hs35mod DoubleDouble]") {
  typedef mantis::DoubleMantissa<double> Real;
  const TestConfig<Real> test_config{
      kMarosMeszaros + std::string("hs35mod.sif"), /* filename */
      false,                                       /* verbose */
      false,                                       /* tighten_infinite_bounds */
      false,                                       /* print_data */
      Real(2.500000000000000e-01),                 /* objective */
      Real(1e-14),                                 /* relative_objective_tol */
  };
  Test(test_config);
}
#endif  // ifdef TEST_DOUBLEDOUBLE

// The listing on http://www.doc.ic.ac.uk/%7Eim/00README.QP is:
//     8.8817842e-16.
// Similarly to hs268, it seems that the optimal solution converges to zero
// as the precision increases. And that the reported solution has zero
// significant digits.

TEST_CASE("hs51 double", "[hs51 double]") {
  typedef double Real;
  const TestConfig<Real> test_config{
      kMarosMeszaros + std::string("hs51.sif"), /* filename */
      false,                                    /* verbose */
      false,                                    /* tighten_infinite_bounds */
      false,                                    /* print_data */
      Real(-2.168740776019975e-32),             /* objective */
      Real(1e-7),                               /* relative_objective_tol */
  };
  Test(test_config);
}

#ifdef TEST_DOUBLEDOUBLE
TEST_CASE("hs51 DoubleDouble", "[hs51 DoubleDouble]") {
  typedef mantis::DoubleMantissa<double> Real;
  const TestConfig<Real> test_config{
      kMarosMeszaros + std::string("hs51.sif"), /* filename */
      false,                                    /* verbose */
      false,                                    /* tighten_infinite_bounds */
      false,                                    /* print_data */
      Real(-2.168740776019975e-32),             /* objective */
      Real(1e-14),                              /* relative_objective_tol */
  };
  Test(test_config);
}
#endif  // ifdef TEST_DOUBLEDOUBLE

// The listing on http://www.doc.ic.ac.uk/%7Eim/00README.QP is:
//     5.3266476e+00.

TEST_CASE("hs52 double", "[hs52 double]") {
  typedef double Real;
  const TestConfig<Real> test_config{
      kMarosMeszaros + std::string("hs52.sif"), /* filename */
      false,                                    /* verbose */
      false,                                    /* tighten_infinite_bounds */
      false,                                    /* print_data */
      Real(5.326647564469914),                  /* objective */
      Real(1e-7),                               /* relative_objective_tol */
  };
  Test(test_config);
}

#ifdef TEST_DOUBLEDOUBLE
TEST_CASE("hs52 DoubleDouble", "[hs52 DoubleDouble]") {
  typedef mantis::DoubleMantissa<double> Real;
  const TestConfig<Real> test_config{
      kMarosMeszaros + std::string("hs52.sif"), /* filename */
      false,                                    /* verbose */
      false,                                    /* tighten_infinite_bounds */
      false,                                    /* print_data */
      Real(5.326647564469914),                  /* objective */
      Real(1e-14),                              /* relative_objective_tol */
  };
  Test(test_config);
}
#endif  // ifdef TEST_DOUBLEDOUBLE

// The listing on http://www.doc.ic.ac.uk/%7Eim/00README.QP is:
//     4.0930233e+00.

TEST_CASE("hs53 double", "[hs53 double]") {
  typedef double Real;
  const TestConfig<Real> test_config{
      kMarosMeszaros + std::string("hs53.sif"), /* filename */
      false,                                    /* verbose */
      false,                                    /* tighten_infinite_bounds */
      false,                                    /* print_data */
      Real(4.093023255813953),                  /* objective */
      Real(1e-7),                               /* relative_objective_tol */
  };
  Test(test_config);
}

#ifdef TEST_DOUBLEDOUBLE
TEST_CASE("hs53 DoubleDouble", "[hs53 DoubleDouble]") {
  typedef mantis::DoubleMantissa<double> Real;
  const TestConfig<Real> test_config{
      kMarosMeszaros + std::string("hs53.sif"), /* filename */
      false,                                    /* verbose */
      false,                                    /* tighten_infinite_bounds */
      false,                                    /* print_data */
      Real(4.093023255813953),                  /* objective */
      Real(1e-14),                              /* relative_objective_tol */
  };
  Test(test_config);
}
#endif  // ifdef TEST_DOUBLEDOUBLE

// The listing on http://www.doc.ic.ac.uk/%7Eim/00README.QP is:
//     -4.6818182e+00.

TEST_CASE("hs76 double", "[hs76 double]") {
  typedef double Real;
  const TestConfig<Real> test_config{
      kMarosMeszaros + std::string("hs76.sif"), /* filename */
      false,                                    /* verbose */
      false,                                    /* tighten_infinite_bounds */
      false,                                    /* print_data */
      Real(-4.681818181818182),                 /* objective */
      Real(1e-7),                               /* relative_objective_tol */
  };
  Test(test_config);
}

#ifdef TEST_DOUBLEDOUBLE
TEST_CASE("hs76 DoubleDouble", "[hs76 DoubleDouble]") {
  typedef mantis::DoubleMantissa<double> Real;
  const TestConfig<Real> test_config{
      kMarosMeszaros + std::string("hs76.sif"), /* filename */
      false,                                    /* verbose */
      false,                                    /* tighten_infinite_bounds */
      false,                                    /* print_data */
      Real(-4.681818181818182),                 /* objective */
      Real(1e-14),                              /* relative_objective_tol */
  };
  Test(test_config);
}
#endif  // ifdef TEST_DOUBLEDOUBLE

// The listing on http://www.doc.ic.ac.uk/%7Eim/00README.QP is:
//     3.4824690e+07.

TEST_CASE("hues-mod double", "[hues-mod double]") {
  typedef double Real;
  const TestConfig<Real> test_config{
      kMarosMeszaros + std::string("hues-mod.sif"), /* filename */
      false,                                        /* verbose */
      false,                     /* tighten_infinite_bounds */
      false,                     /* print_data */
      Real(3.482446387334524e7), /* objective */
      Real(1e-7),                /* relative_objective_tol */
  };
  Test(test_config);
}

#ifdef TEST_DOUBLEDOUBLE
TEST_CASE("hues-mod DoubleDouble", "[hues-mod DoubleDouble]") {
  typedef mantis::DoubleMantissa<double> Real;
  const TestConfig<Real> test_config{
      kMarosMeszaros + std::string("hues-mod.sif"), /* filename */
      false,                                        /* verbose */
      false,                     /* tighten_infinite_bounds */
      false,                     /* print_data */
      Real(3.482446387334524e7), /* objective */
      Real(1e-14),               /* relative_objective_tol */
  };
  Test(test_config);
}
#endif  // ifdef TEST_DOUBLEDOUBLE

// The listing on http://www.doc.ic.ac.uk/%7Eim/00README.QP is:
//     3.4824690e+11.

TEST_CASE("huestis double", "[huestis double]") {
  typedef double Real;
  const TestConfig<Real> test_config{
      kMarosMeszaros + std::string("huestis.sif"), /* filename */
      false,                                       /* verbose */
      false,                                       /* tighten_infinite_bounds */
      false,                                       /* print_data */
      Real(3.482446387334523e11),                  /* objective */
      Real(1e-7),                                  /* relative_objective_tol */
  };
  Test(test_config);
}

#ifdef TEST_DOUBLEDOUBLE
TEST_CASE("huestis DoubleDouble", "[huestis DoubleDouble]") {
  typedef mantis::DoubleMantissa<double> Real;
  const TestConfig<Real> test_config{
      kMarosMeszaros + std::string("huestis.sif"), /* filename */
      false,                                       /* verbose */
      false,                                       /* tighten_infinite_bounds */
      false,                                       /* print_data */
      Real(3.482446387334523e11),                  /* objective */
      Real(1e-14),                                 /* relative_objective_tol */
  };
  Test(test_config);
}
#endif  // ifdef TEST_DOUBLEDOUBLE

// The listing on http://www.doc.ic.ac.uk/%7Eim/00README.QP is:
//     5.7579794e-01.

TEST_CASE("ksip double", "[ksip double]") {
  typedef double Real;
  const TestConfig<Real> test_config{
      kMarosMeszaros + std::string("ksip.sif"), /* filename */
      false,                                    /* verbose */
      false,                                    /* tighten_infinite_bounds */
      false,                                    /* print_data */
      Real(5.757979412401069e-1),               /* objective */
      Real(1e-7),                               /* relative_objective_tol */
  };
  Test(test_config);
}

#ifdef TEST_DOUBLEDOUBLE
TEST_CASE("ksip DoubleDouble", "[ksip DoubleDouble]") {
  typedef mantis::DoubleMantissa<double> Real;
  const TestConfig<Real> test_config{
      kMarosMeszaros + std::string("ksip.sif"), /* filename */
      false,                                    /* verbose */
      false,                                    /* tighten_infinite_bounds */
      false,                                    /* print_data */
      Real(5.757979412401069e-1),               /* objective */
      Real(1e-14),                              /* relative_objective_tol */
  };
  Test(test_config);
}
#endif  // ifdef TEST_DOUBLEDOUBLE

// The listing on http://www.doc.ic.ac.uk/%7Eim/00README.QP is:
//     2.4096014e+06.

TEST_CASE("laser double", "[laser double]") {
  typedef double Real;
  const TestConfig<Real> test_config{
      kMarosMeszaros + std::string("laser.sif"), /* filename */
      false,                                     /* verbose */
      false,                                     /* tighten_infinite_bounds */
      false,                                     /* print_data */
      Real(2.409601345596794e6),                 /* objective */
      Real(1e-7),                                /* relative_objective_tol */
  };
  Test(test_config);
}

#ifdef TEST_DOUBLEDOUBLE
TEST_CASE("laser DoubleDouble", "[laser DoubleDouble]") {
  typedef mantis::DoubleMantissa<double> Real;
  const TestConfig<Real> test_config{
      kMarosMeszaros + std::string("laser.sif"), /* filename */
      false,                                     /* verbose */
      false,                                     /* tighten_infinite_bounds */
      false,                                     /* print_data */
      Real(2.409601345596794e6),                 /* objective */
      Real(1e-14),                               /* relative_objective_tol */
  };
  Test(test_config);
}
#endif  // ifdef TEST_DOUBLEDOUBLE

// The listing on http://www.doc.ic.ac.uk/%7Eim/00README.QP is:
//     3.6122402e+01.

TEST_CASE("liswet1 double", "[liswet1 double]") {
  typedef double Real;
  const TestConfig<Real> test_config{
      kMarosMeszaros + std::string("liswet1.sif"), /* filename */
      false,                                       /* verbose */
      false,                                       /* tighten_infinite_bounds */
      false,                                       /* print_data */
      Real(3.612240208495135e1),                   /* objective */
      Real(1e-7),                                  /* relative_objective_tol */
  };
  Test(test_config);
}

#ifdef TEST_DOUBLEDOUBLE
TEST_CASE("liswet1 DoubleDouble", "[liswet1 DoubleDouble]") {
  typedef mantis::DoubleMantissa<double> Real;
  const TestConfig<Real> test_config{
      kMarosMeszaros + std::string("liswet1.sif"), /* filename */
      false,                                       /* verbose */
      false,                                       /* tighten_infinite_bounds */
      false,                                       /* print_data */
      Real(3.612240208495135e1),                   /* objective */
      Real(1e-14),                                 /* relative_objective_tol */
  };
  Test(test_config);
}
#endif  // ifdef TEST_DOUBLEDOUBLE

// The listing on http://www.doc.ic.ac.uk/%7Eim/00README.QP is:
//     2.4998076e+01.

TEST_CASE("liswet2 double", "[liswet2 double]") {
  typedef double Real;
  const TestConfig<Real> test_config{
      kMarosMeszaros + std::string("liswet2.sif"), /* filename */
      false,                                       /* verbose */
      false,                                       /* tighten_infinite_bounds */
      false,                                       /* print_data */
      Real(2.499807610288072e1),                   /* objective */
      Real(1e-7),                                  /* relative_objective_tol */
  };
  Test(test_config);
}

#ifdef TEST_DOUBLEDOUBLE
TEST_CASE("liswet2 DoubleDouble", "[liswet2 DoubleDouble]") {
  typedef mantis::DoubleMantissa<double> Real;
  const TestConfig<Real> test_config{
      kMarosMeszaros + std::string("liswet2.sif"), /* filename */
      false,                                       /* verbose */
      false,                                       /* tighten_infinite_bounds */
      false,                                       /* print_data */
      Real(2.499807610288072e1),                   /* objective */
      Real(1e-14),                                 /* relative_objective_tol */
  };
  Test(test_config);
}
#endif  // ifdef TEST_DOUBLEDOUBLE

// The listing on http://www.doc.ic.ac.uk/%7Eim/00README.QP is:
//     2.5001220e+01.

TEST_CASE("liswet3 double", "[liswet3 double]") {
  typedef double Real;
  const TestConfig<Real> test_config{
      kMarosMeszaros + std::string("liswet3.sif"), /* filename */
      false,                                       /* verbose */
      false,                                       /* tighten_infinite_bounds */
      false,                                       /* print_data */
      Real(2.500122000019373e1),                   /* objective */
      Real(1e-7),                                  /* relative_objective_tol */
  };
  Test(test_config);
}

#ifdef TEST_DOUBLEDOUBLE
TEST_CASE("liswet3 DoubleDouble", "[liswet3 DoubleDouble]") {
  typedef mantis::DoubleMantissa<double> Real;
  const TestConfig<Real> test_config{
      kMarosMeszaros + std::string("liswet3.sif"), /* filename */
      false,                                       /* verbose */
      false,                                       /* tighten_infinite_bounds */
      false,                                       /* print_data */
      Real(2.500122000019373e1),                   /* objective */
      Real(1e-14),                                 /* relative_objective_tol */
  };
  Test(test_config);
}
#endif  // ifdef TEST_DOUBLEDOUBLE

// The listing on http://www.doc.ic.ac.uk/%7Eim/00README.QP is:
//     2.5000112e+01.

TEST_CASE("liswet4 double", "[liswet4 double]") {
  typedef double Real;
  const TestConfig<Real> test_config{
      kMarosMeszaros + std::string("liswet4.sif"), /* filename */
      false,                                       /* verbose */
      false,                                       /* tighten_infinite_bounds */
      false,                                       /* print_data */
      Real(2.500011208647153e1),                   /* objective */
      Real(1e-7),                                  /* relative_objective_tol */
  };
  Test(test_config);
}

#ifdef TEST_DOUBLEDOUBLE
TEST_CASE("liswet4 DoubleDouble", "[liswet4 DoubleDouble]") {
  typedef mantis::DoubleMantissa<double> Real;
  const TestConfig<Real> test_config{
      kMarosMeszaros + std::string("liswet4.sif"), /* filename */
      false,                                       /* verbose */
      false,                                       /* tighten_infinite_bounds */
      false,                                       /* print_data */
      Real(2.500011208647153e1),                   /* objective */
      Real(1e-14),                                 /* relative_objective_tol */
  };
  Test(test_config);
}
#endif  // ifdef TEST_DOUBLEDOUBLE

// The listing on http://www.doc.ic.ac.uk/%7Eim/00README.QP is:
//     2.5034253e+01.

TEST_CASE("liswet5 double", "[liswet5 double]") {
  typedef double Real;
  const TestConfig<Real> test_config{
      kMarosMeszaros + std::string("liswet5.sif"), /* filename */
      false,                                       /* verbose */
      false,                                       /* tighten_infinite_bounds */
      false,                                       /* print_data */
      Real(2.503425340169750e1),                   /* objective */
      Real(1e-7),                                  /* relative_objective_tol */
  };
  Test(test_config);
}

#ifdef TEST_DOUBLEDOUBLE
TEST_CASE("liswet5 DoubleDouble", "[liswet5 DoubleDouble]") {
  typedef mantis::DoubleMantissa<double> Real;
  const TestConfig<Real> test_config{
      kMarosMeszaros + std::string("liswet5.sif"), /* filename */
      false,                                       /* verbose */
      false,                                       /* tighten_infinite_bounds */
      false,                                       /* print_data */
      Real(2.503425340169750e1),                   /* objective */
      Real(1e-14),                                 /* relative_objective_tol */
  };
  Test(test_config);
}
#endif  // ifdef TEST_DOUBLEDOUBLE

// The listing on http://www.doc.ic.ac.uk/%7Eim/00README.QP is:
//     2.4995748e+01.

TEST_CASE("liswet6 double", "[liswet6 double]") {
  typedef double Real;
  const TestConfig<Real> test_config{
      kMarosMeszaros + std::string("liswet6.sif"), /* filename */
      false,                                       /* verbose */
      false,                                       /* tighten_infinite_bounds */
      false,                                       /* print_data */
      Real(2.499574755706173e1),                   /* objective */
      Real(1e-7),                                  /* relative_objective_tol */
  };
  Test(test_config);
}

#ifdef TEST_DOUBLEDOUBLE
TEST_CASE("liswet6 DoubleDouble", "[liswet6 DoubleDouble]") {
  typedef mantis::DoubleMantissa<double> Real;
  const TestConfig<Real> test_config{
      kMarosMeszaros + std::string("liswet6.sif"), /* filename */
      false,                                       /* verbose */
      false,                                       /* tighten_infinite_bounds */
      false,                                       /* print_data */
      Real(2.499574755706173e1),                   /* objective */
      Real(1e-14),                                 /* relative_objective_tol */
  };
  Test(test_config);
}
#endif  // ifdef TEST_DOUBLEDOUBLE

// The listing on http://www.doc.ic.ac.uk/%7Eim/00README.QP is:
//     4.9884089e+02.

TEST_CASE("liswet7 double", "[liswet7 double]") {
  typedef double Real;
  const TestConfig<Real> test_config{
      kMarosMeszaros + std::string("liswet7.sif"), /* filename */
      false,                                       /* verbose */
      false,                                       /* tighten_infinite_bounds */
      false,                                       /* print_data */
      Real(4.988408907861149e2),                   /* objective */
      Real(1e-7),                                  /* relative_objective_tol */
  };
  Test(test_config);
}

#ifdef TEST_DOUBLEDOUBLE
TEST_CASE("liswet7 DoubleDouble", "[liswet7 DoubleDouble]") {
  typedef mantis::DoubleMantissa<double> Real;
  const TestConfig<Real> test_config{
      kMarosMeszaros + std::string("liswet7.sif"), /* filename */
      false,                                       /* verbose */
      false,                                       /* tighten_infinite_bounds */
      false,                                       /* print_data */
      Real(4.988408907861149e2),                   /* objective */
      Real(1e-14),                                 /* relative_objective_tol */
  };
  Test(test_config);
}
#endif  // ifdef TEST_DOUBLEDOUBLE

// The listing on http://www.doc.ic.ac.uk/%7Eim/00README.QP is:
//     7.1447006e+03.

TEST_CASE("liswet8 double", "[liswet8 double]") {
  typedef double Real;
  const TestConfig<Real> test_config{
      kMarosMeszaros + std::string("liswet8.sif"), /* filename */
      false,                                       /* verbose */
      false,                                       /* tighten_infinite_bounds */
      false,                                       /* print_data */
      Real(7.144700589499147e2),                   /* objective */
      Real(5e-7),                                  /* relative_objective_tol */
  };
  Test(test_config);
}

#ifdef TEST_DOUBLEDOUBLE
TEST_CASE("liswet8 DoubleDouble", "[liswet8 DoubleDouble]") {
  typedef mantis::DoubleMantissa<double> Real;
  const TestConfig<Real> test_config{
      kMarosMeszaros + std::string("liswet8.sif"), /* filename */
      false,                                       /* verbose */
      false,                                       /* tighten_infinite_bounds */
      false,                                       /* print_data */
      Real(7.144700589499147e2),                   /* objective */
      Real(5e-10),                                 /* relative_objective_tol */
  };
  Test(test_config);
}
#endif  // ifdef TEST_DOUBLEDOUBLE

// The listing on http://www.doc.ic.ac.uk/%7Eim/00README.QP is:
//     1.9632513e+03.

TEST_CASE("liswet9 double", "[liswet9 double]") {
  typedef double Real;
  const TestConfig<Real> test_config{
      kMarosMeszaros + std::string("liswet9.sif"), /* filename */
      false,                                       /* verbose */
      false,                                       /* tighten_infinite_bounds */
      false,                                       /* print_data */
      Real(1.963251260894656e3),                   /* objective */
      Real(1e-7),                                  /* relative_objective_tol */
  };
  Test(test_config);
}

#ifdef TEST_DOUBLEDOUBLE
TEST_CASE("liswet9 DoubleDouble", "[liswet9 DoubleDouble]") {
  typedef mantis::DoubleMantissa<double> Real;
  const TestConfig<Real> test_config{
      kMarosMeszaros + std::string("liswet9.sif"), /* filename */
      false,                                       /* verbose */
      false,                                       /* tighten_infinite_bounds */
      false,                                       /* print_data */
      Real(1.963251260894656e3),                   /* objective */
      Real(1e-14),                                 /* relative_objective_tol */
  };
  Test(test_config);
}
#endif  // ifdef TEST_DOUBLEDOUBLE

// The listing on http://www.doc.ic.ac.uk/%7Eim/00README.QP is:
//     4.9485785e+01.

TEST_CASE("liswet10 double", "[liswet10 double]") {
  typedef double Real;
  const TestConfig<Real> test_config{
      kMarosMeszaros + std::string("liswet10.sif"), /* filename */
      false,                                        /* verbose */
      false,                     /* tighten_infinite_bounds */
      false,                     /* print_data */
      Real(4.948578468800290e1), /* objective */
      Real(5e-6),                /* relative_objective_tol */
  };
  Test(test_config);
}

#ifdef TEST_DOUBLEDOUBLE
TEST_CASE("liswet10 DoubleDouble", "[liswet10 DoubleDouble]") {
  typedef mantis::DoubleMantissa<double> Real;
  const TestConfig<Real> test_config{
      kMarosMeszaros + std::string("liswet10.sif"), /* filename */
      false,                                        /* verbose */
      false,                     /* tighten_infinite_bounds */
      false,                     /* print_data */
      Real(4.948578468800290e1), /* objective */
      Real(1e-14),               /* relative_objective_tol */
  };
  Test(test_config);
}
#endif  // ifdef TEST_DOUBLEDOUBLE

// The listing on http://www.doc.ic.ac.uk/%7Eim/00README.QP is:
//     4.9523957e+01.

TEST_CASE("liswet11 double", "[liswet11 double]") {
  typedef double Real;
  const TestConfig<Real> test_config{
      kMarosMeszaros + std::string("liswet11.sif"), /* filename */
      false,                                        /* verbose */
      false,                     /* tighten_infinite_bounds */
      false,                     /* print_data */
      Real(4.952395713866009e1), /* objective */
      Real(5e-6),                /* relative_objective_tol */
  };
  Test(test_config);
}

#ifdef TEST_DOUBLEDOUBLE
TEST_CASE("liswet11 DoubleDouble", "[liswet11 DoubleDouble]") {
  typedef mantis::DoubleMantissa<double> Real;
  const TestConfig<Real> test_config{
      kMarosMeszaros + std::string("liswet11.sif"), /* filename */
      false,                                        /* verbose */
      false,                     /* tighten_infinite_bounds */
      false,                     /* print_data */
      Real(4.952395713866009e1), /* objective */
      Real(1e-14),               /* relative_objective_tol */
  };
  Test(test_config);
}
#endif  // ifdef TEST_DOUBLEDOUBLE

// The listing on http://www.doc.ic.ac.uk/%7Eim/00README.QP is:
//     1.7369274e+03.

TEST_CASE("liswet12 double", "[liswet12 double]") {
  typedef double Real;
  const TestConfig<Real> test_config{
      kMarosMeszaros + std::string("liswet12.sif"), /* filename */
      false,                                        /* verbose */
      false,                     /* tighten_infinite_bounds */
      false,                     /* print_data */
      Real(1.736927426115806e3), /* objective */
      Real(1e-7),                /* relative_objective_tol */
  };
  Test(test_config);
}

#ifdef TEST_DOUBLEDOUBLE
TEST_CASE("liswet12 DoubleDouble", "[liswet12 DoubleDouble]") {
  typedef mantis::DoubleMantissa<double> Real;
  const TestConfig<Real> test_config{
      kMarosMeszaros + std::string("liswet12.sif"), /* filename */
      false,                                        /* verbose */
      false,                     /* tighten_infinite_bounds */
      false,                     /* print_data */
      Real(1.736927426115806e3), /* objective */
      Real(1e-14),               /* relative_objective_tol */
  };
  Test(test_config);
}
#endif  // ifdef TEST_DOUBLEDOUBLE

// The listing on http://www.doc.ic.ac.uk/%7Eim/00README.QP is:
//     2.3984159e+03.

TEST_CASE("lotschd double", "[lotschd double]") {
  typedef double Real;
  const TestConfig<Real> test_config{
      kMarosMeszaros + std::string("lotschd.sif"), /* filename */
      false,                                       /* verbose */
      false,                                       /* tighten_infinite_bounds */
      false,                                       /* print_data */
      Real(2.398415891448896e3),                   /* objective */
      Real(1e-7),                                  /* relative_objective_tol */
  };
  Test(test_config);
}

#ifdef TEST_DOUBLEDOUBLE
TEST_CASE("lotschd DoubleDouble", "[lotschd DoubleDouble]") {
  typedef mantis::DoubleMantissa<double> Real;
  const TestConfig<Real> test_config{
      kMarosMeszaros + std::string("lotschd.sif"), /* filename */
      false,                                       /* verbose */
      false,                                       /* tighten_infinite_bounds */
      false,                                       /* print_data */
      Real(2.398415891448896e3),                   /* objective */
      Real(1e-14),                                 /* relative_objective_tol */
  };
  Test(test_config);
}
#endif  // ifdef TEST_DOUBLEDOUBLE

// The listing on http://www.doc.ic.ac.uk/%7Eim/00README.QP is:
//     -9.5287544e+02.

TEST_CASE("mosarqp1 double", "[mosarqp1 double]") {
  typedef double Real;
  const TestConfig<Real> test_config{
      kMarosMeszaros + std::string("mosarqp1.sif"), /* filename */
      false,                                        /* verbose */
      false,                      /* tighten_infinite_bounds */
      false,                      /* print_data */
      Real(-9.528754430312603e2), /* objective */
      Real(1e-7),                 /* relative_objective_tol */
  };
  Test(test_config);
}

#ifdef TEST_DOUBLEDOUBLE
TEST_CASE("mosarqp1 DoubleDouble", "[mosarqp1 DoubleDouble]") {
  typedef mantis::DoubleMantissa<double> Real;
  const TestConfig<Real> test_config{
      kMarosMeszaros + std::string("mosarqp1.sif"), /* filename */
      false,                                        /* verbose */
      false,                      /* tighten_infinite_bounds */
      false,                      /* print_data */
      Real(-9.528754430312603e2), /* objective */
      Real(1e-14),                /* relative_objective_tol */
  };
  Test(test_config);
}
#endif  // ifdef TEST_DOUBLEDOUBLE

// The listing on http://www.doc.ic.ac.uk/%7Eim/00README.QP is:
//     -1.5974821e+03.

TEST_CASE("mosarqp2 double", "[mosarqp2 double]") {
  typedef double Real;
  const TestConfig<Real> test_config{
      kMarosMeszaros + std::string("mosarqp2.sif"), /* filename */
      false,                                        /* verbose */
      false,                      /* tighten_infinite_bounds */
      false,                      /* print_data */
      Real(-1.597482117523427e3), /* objective */
      Real(1e-7),                 /* relative_objective_tol */
  };
  Test(test_config);
}

#ifdef TEST_DOUBLEDOUBLE
TEST_CASE("mosarqp2 DoubleDouble", "[mosarqp2 DoubleDouble]") {
  typedef mantis::DoubleMantissa<double> Real;
  const TestConfig<Real> test_config{
      kMarosMeszaros + std::string("mosarqp2.sif"), /* filename */
      false,                                        /* verbose */
      false,                      /* tighten_infinite_bounds */
      false,                      /* print_data */
      Real(-1.597482117523427e3), /* objective */
      Real(1e-14),                /* relative_objective_tol */
  };
  Test(test_config);
}
#endif  // ifdef TEST_DOUBLEDOUBLE

// The listing on http://www.doc.ic.ac.uk/%7Eim/00README.QP is:
//     5.2089583e+10.

TEST_CASE("powell20 double", "[powell20 double]") {
  typedef double Real;
  const TestConfig<Real> test_config{
      kMarosMeszaros + std::string("powell20.sif"), /* filename */
      false,                                        /* verbose */
      false,                      /* tighten_infinite_bounds */
      false,                      /* print_data */
      Real(5.208958281250000e10), /* objective */
      Real(1e-7),                 /* relative_objective_tol */
  };
  Test(test_config);
}

#ifdef TEST_DOUBLEDOUBLE
TEST_CASE("powell20 DoubleDouble", "[powell20 DoubleDouble]") {
  typedef mantis::DoubleMantissa<double> Real;
  const TestConfig<Real> test_config{
      kMarosMeszaros + std::string("powell20.sif"), /* filename */
      false,                                        /* verbose */
      false,                      /* tighten_infinite_bounds */
      false,                      /* print_data */
      Real(5.208958281250000e10), /* objective */
      Real(1e-14),                /* relative_objective_tol */
  };
  Test(test_config);
}
#endif  // ifdef TEST_DOUBLEDOUBLE

// The listing on http://www.doc.ic.ac.uk/%7Eim/00README.QP is:
//     -3.5012965e-02.

TEST_CASE("primal1 double", "[primal1 double]") {
  typedef double Real;
  const TestConfig<Real> test_config{
      kMarosMeszaros + std::string("primal1.sif"), /* filename */
      false,                                       /* verbose */
      false,                                       /* tighten_infinite_bounds */
      false,                                       /* print_data */
      Real(-3.501296573347734e-2),                 /* objective */
      Real(1e-7),                                  /* relative_objective_tol */
  };
  Test(test_config);
}

#ifdef TEST_DOUBLEDOUBLE
TEST_CASE("primal1 DoubleDouble", "[primal1 DoubleDouble]") {
  typedef mantis::DoubleMantissa<double> Real;
  const TestConfig<Real> test_config{
      kMarosMeszaros + std::string("primal1.sif"), /* filename */
      false,                                       /* verbose */
      false,                                       /* tighten_infinite_bounds */
      false,                                       /* print_data */
      Real(-3.501296573347734e-2),                 /* objective */
      Real(1e-14),                                 /* relative_objective_tol */
  };
  Test(test_config);
}
#endif  // ifdef TEST_DOUBLEDOUBLE

// The listing on http://www.doc.ic.ac.uk/%7Eim/00README.QP is:
//     -3.3733676e-02.

TEST_CASE("primal2 double", "[primal2 double]") {
  typedef double Real;
  const TestConfig<Real> test_config{
      kMarosMeszaros + std::string("primal2.sif"), /* filename */
      false,                                       /* verbose */
      false,                                       /* tighten_infinite_bounds */
      false,                                       /* print_data */
      Real(-3.373367612271853e-2),                 /* objective */
      Real(1e-7),                                  /* relative_objective_tol */
  };
  Test(test_config);
}

#ifdef TEST_DOUBLEDOUBLE
TEST_CASE("primal2 DoubleDouble", "[primal2 DoubleDouble]") {
  typedef mantis::DoubleMantissa<double> Real;
  const TestConfig<Real> test_config{
      kMarosMeszaros + std::string("primal2.sif"), /* filename */
      false,                                       /* verbose */
      false,                                       /* tighten_infinite_bounds */
      false,                                       /* print_data */
      Real(-3.373367612271853e-2),                 /* objective */
      Real(1e-14),                                 /* relative_objective_tol */
  };
  Test(test_config);
}
#endif  // ifdef TEST_DOUBLEDOUBLE

// The listing on http://www.doc.ic.ac.uk/%7Eim/00README.QP is:
//     -1.3575584e-01.

TEST_CASE("primal3 double", "[primal3 double]") {
  typedef double Real;
  const TestConfig<Real> test_config{
      kMarosMeszaros + std::string("primal3.sif"), /* filename */
      false,                                       /* verbose */
      false,                                       /* tighten_infinite_bounds */
      false,                                       /* print_data */
      Real(-1.357558368660210e-1),                 /* objective */
      Real(1e-7),                                  /* relative_objective_tol */
  };
  Test(test_config);
}

#ifdef TEST_DOUBLEDOUBLE
TEST_CASE("primal3 DoubleDouble", "[primal3 DoubleDouble]") {
  typedef mantis::DoubleMantissa<double> Real;
  const TestConfig<Real> test_config{
      kMarosMeszaros + std::string("primal3.sif"), /* filename */
      false,                                       /* verbose */
      false,                                       /* tighten_infinite_bounds */
      false,                                       /* print_data */
      Real(-1.357558368660210e-1),                 /* objective */
      Real(1e-14),                                 /* relative_objective_tol */
  };
  Test(test_config);
}
#endif  // ifdef TEST_DOUBLEDOUBLE

// The listing on http://www.doc.ic.ac.uk/%7Eim/00README.QP is:
//     -7.4609083e-01.

TEST_CASE("primal4 double", "[primal4 double]") {
  typedef double Real;
  const TestConfig<Real> test_config{
      kMarosMeszaros + std::string("primal4.sif"), /* filename */
      false,                                       /* verbose */
      false,                                       /* tighten_infinite_bounds */
      false,                                       /* print_data */
      Real(-7.460908418021001e-1),                 /* objective */
      Real(1e-7),                                  /* relative_objective_tol */
  };
  Test(test_config);
}

#ifdef TEST_DOUBLEDOUBLE
TEST_CASE("primal4 DoubleDouble", "[primal4 DoubleDouble]") {
  typedef mantis::DoubleMantissa<double> Real;
  const TestConfig<Real> test_config{
      kMarosMeszaros + std::string("primal4.sif"), /* filename */
      false,                                       /* verbose */
      false,                                       /* tighten_infinite_bounds */
      false,                                       /* print_data */
      Real(-7.460908418021001e-1),                 /* objective */
      Real(1e-14),                                 /* relative_objective_tol */
  };
  Test(test_config);
}
#endif  // ifdef TEST_DOUBLEDOUBLE

// The listing on http://www.doc.ic.ac.uk/%7Eim/00README.QP is:
//     -6.1552508e+03.

TEST_CASE("primalc1 double", "[primalc1 double]") {
  typedef double Real;
  const TestConfig<Real> test_config{
      kMarosMeszaros + std::string("primalc1.sif"), /* filename */
      false,                                        /* verbose */
      false,                      /* tighten_infinite_bounds */
      false,                      /* print_data */
      Real(-6.155250829462685e3), /* objective */
      Real(1e-7),                 /* relative_objective_tol */
  };
  Test(test_config);
}

#ifdef TEST_DOUBLEDOUBLE
TEST_CASE("primalc1 DoubleDouble", "[primalc1 DoubleDouble]") {
  typedef mantis::DoubleMantissa<double> Real;
  const TestConfig<Real> test_config{
      kMarosMeszaros + std::string("primalc1.sif"), /* filename */
      false,                                        /* verbose */
      false,                      /* tighten_infinite_bounds */
      false,                      /* print_data */
      Real(-6.155250829462685e3), /* objective */
      Real(1e-14),                /* relative_objective_tol */
  };
  Test(test_config);
}
#endif  // ifdef TEST_DOUBLEDOUBLE

// The listing on http://www.doc.ic.ac.uk/%7Eim/00README.QP is:
//     -3.5513077e+03.

TEST_CASE("primalc2 double", "[primalc2 double]") {
  typedef double Real;
  const TestConfig<Real> test_config{
      kMarosMeszaros + std::string("primalc2.sif"), /* filename */
      false,                                        /* verbose */
      false,                      /* tighten_infinite_bounds */
      false,                      /* print_data */
      Real(-3.551307692670643e3), /* objective */
      Real(1e-7),                 /* relative_objective_tol */
  };
  Test(test_config);
}

#ifdef TEST_DOUBLEDOUBLE
TEST_CASE("primalc2 DoubleDouble", "[primalc2 DoubleDouble]") {
  typedef mantis::DoubleMantissa<double> Real;
  const TestConfig<Real> test_config{
      kMarosMeszaros + std::string("primalc2.sif"), /* filename */
      false,                                        /* verbose */
      false,                      /* tighten_infinite_bounds */
      false,                      /* print_data */
      Real(-3.551307692670643e3), /* objective */
      Real(1e-14),                /* relative_objective_tol */
  };
  Test(test_config);
}
#endif  // ifdef TEST_DOUBLEDOUBLE

// The listing on http://www.doc.ic.ac.uk/%7Eim/00README.QP is:
//     -4.2723233e+02.

TEST_CASE("primalc5 double", "[primalc5 double]") {
  typedef double Real;
  const TestConfig<Real> test_config{
      kMarosMeszaros + std::string("primalc5.sif"), /* filename */
      false,                                        /* verbose */
      false,                      /* tighten_infinite_bounds */
      false,                      /* print_data */
      Real(-4.272323267763890e2), /* objective */
      Real(1e-7),                 /* relative_objective_tol */
  };
  Test(test_config);
}

#ifdef TEST_DOUBLEDOUBLE
TEST_CASE("primalc5 DoubleDouble", "[primalc5 DoubleDouble]") {
  typedef mantis::DoubleMantissa<double> Real;
  const TestConfig<Real> test_config{
      kMarosMeszaros + std::string("primalc5.sif"), /* filename */
      false,                                        /* verbose */
      false,                      /* tighten_infinite_bounds */
      false,                      /* print_data */
      Real(-4.272323267763890e2), /* objective */
      Real(1e-14),                /* relative_objective_tol */
  };
  Test(test_config);
}
#endif  // ifdef TEST_DOUBLEDOUBLE

// The listing on http://www.doc.ic.ac.uk/%7Eim/00README.QP is:
//     -1.8309430e+04.

TEST_CASE("primalc8 double", "[primalc8 double]") {
  typedef double Real;
  const TestConfig<Real> test_config{
      kMarosMeszaros + std::string("primalc8.sif"), /* filename */
      false,                                        /* verbose */
      false,                      /* tighten_infinite_bounds */
      false,                      /* print_data */
      Real(-1.830942978842186e4), /* objective */
      Real(1e-7),                 /* relative_objective_tol */
  };
  Test(test_config);
}

#ifdef TEST_DOUBLEDOUBLE
TEST_CASE("primalc8 DoubleDouble", "[primalc8 DoubleDouble]") {
  typedef mantis::DoubleMantissa<double> Real;
  const TestConfig<Real> test_config{
      kMarosMeszaros + std::string("primalc8.sif"), /* filename */
      false,                                        /* verbose */
      false,                      /* tighten_infinite_bounds */
      false,                      /* print_data */
      Real(-1.830942978842186e4), /* objective */
      Real(1e-14),                /* relative_objective_tol */
  };
  Test(test_config);
}
#endif  // ifdef TEST_DOUBLEDOUBLE

// The listing on http://www.doc.ic.ac.uk/%7Eim/00README.QP is:
//     1.3744448e+07.

TEST_CASE("q25fv47 double", "[q25fv47 double]") {
  typedef double Real;
  const TestConfig<Real> test_config{
      kMarosMeszaros + std::string("q25fv47.sif"), /* filename */
      false,                                       /* verbose */
      false,                                       /* tighten_infinite_bounds */
      false,                                       /* print_data */
      Real(1.374444789486296e7),                   /* objective */
      Real(1e-7),                                  /* relative_objective_tol */
  };
  Test(test_config);
}

#ifdef TEST_DOUBLEDOUBLE
TEST_CASE("q25fv47 DoubleDouble", "[q25fv47 DoubleDouble]") {
  typedef mantis::DoubleMantissa<double> Real;
  const TestConfig<Real> test_config{
      kMarosMeszaros + std::string("q25fv47.sif"), /* filename */
      false,                                       /* verbose */
      false,                                       /* tighten_infinite_bounds */
      false,                                       /* print_data */
      Real(1.374444789486296e7),                   /* objective */
      Real(1e-14),                                 /* relative_objective_tol */
  };
  Test(test_config);
}
#endif  // ifdef TEST_DOUBLEDOUBLE

// The listing on http://www.doc.ic.ac.uk/%7Eim/00README.QP is:
//     4.8031886e+05.

TEST_CASE("qadlittl double", "[qadlittl double]") {
  typedef double Real;
  const TestConfig<Real> test_config{
      kMarosMeszaros + std::string("qadlittl.sif"), /* filename */
      false,                                        /* verbose */
      false,                     /* tighten_infinite_bounds */
      false,                     /* print_data */
      Real(4.803188585447707e5), /* objective */
      Real(1e-7),                /* relative_objective_tol */
  };
  Test(test_config);
}

#ifdef TEST_DOUBLEDOUBLE
TEST_CASE("qadlittl DoubleDouble", "[qadlittl DoubleDouble]") {
  typedef mantis::DoubleMantissa<double> Real;
  const TestConfig<Real> test_config{
      kMarosMeszaros + std::string("qadlittl.sif"), /* filename */
      false,                                        /* verbose */
      false,                     /* tighten_infinite_bounds */
      false,                     /* print_data */
      Real(4.803188585447707e5), /* objective */
      Real(1e-14),               /* relative_objective_tol */
  };
  Test(test_config);
}
#endif  // ifdef TEST_DOUBLEDOUBLE

// The listing on http://www.doc.ic.ac.uk/%7Eim/00README.QP is:
//     -1.5907818e+00.

TEST_CASE("qafiro double", "[qafiro double]") {
  typedef double Real;
  const TestConfig<Real> test_config{
      kMarosMeszaros + std::string("qafiro.sif"), /* filename */
      false,                                      /* verbose */
      false,                                      /* tighten_infinite_bounds */
      false,                                      /* print_data */
      Real(-1.590781793905533),                   /* objective */
      Real(1e-7),                                 /* relative_objective_tol */
  };
  Test(test_config);
}

#ifdef TEST_DOUBLEDOUBLE
TEST_CASE("qafiro DoubleDouble", "[qafiro DoubleDouble]") {
  typedef mantis::DoubleMantissa<double> Real;
  const TestConfig<Real> test_config{
      kMarosMeszaros + std::string("qafiro.sif"), /* filename */
      false,                                      /* verbose */
      false,                                      /* tighten_infinite_bounds */
      false,                                      /* print_data */
      Real(-1.590781793905533),                   /* objective */
      Real(1e-14),                                /* relative_objective_tol */
  };
  Test(test_config);
}
#endif  // ifdef TEST_DOUBLEDOUBLE

// The listing on http://www.doc.ic.ac.uk/%7Eim/00README.QP is:
//     1.6352342e+04.

TEST_CASE("qbandm double", "[qbandm double]") {
  typedef double Real;
  const TestConfig<Real> test_config{
      kMarosMeszaros + std::string("qbandm.sif"), /* filename */
      false,                                      /* verbose */
      false,                                      /* tighten_infinite_bounds */
      false,                                      /* print_data */
      Real(1.635234203665042e4),                  /* objective */
      Real(1e-7),                                 /* relative_objective_tol */
  };
  Test(test_config);
}

#ifdef TEST_DOUBLEDOUBLE
TEST_CASE("qbandm DoubleDouble", "[qbandm DoubleDouble]") {
  typedef mantis::DoubleMantissa<double> Real;
  const TestConfig<Real> test_config{
      kMarosMeszaros + std::string("qbandm.sif"), /* filename */
      false,                                      /* verbose */
      false,                                      /* tighten_infinite_bounds */
      false,                                      /* print_data */
      Real(1.635234203665042e4),                  /* objective */
      Real(1e-14),                                /* relative_objective_tol */
  };
  Test(test_config);
}
#endif  // ifdef TEST_DOUBLEDOUBLE

// The listing on http://www.doc.ic.ac.uk/%7Eim/00README.QP is:
//     1.6471206e+05.

TEST_CASE("qbeaconf double", "[qbeaconf double]") {
  typedef double Real;
  const TestConfig<Real> test_config{
      kMarosMeszaros + std::string("qbeaconf.sif"), /* filename */
      false,                                        /* verbose */
      false,                     /* tighten_infinite_bounds */
      false,                     /* print_data */
      Real(1.647120601497000e5), /* objective */
      Real(1e-7),                /* relative_objective_tol */
  };
  Test(test_config);
}

#ifdef TEST_DOUBLEDOUBLE
TEST_CASE("qbeaconf DoubleDouble", "[qbeaconf DoubleDouble]") {
  typedef mantis::DoubleMantissa<double> Real;
  const TestConfig<Real> test_config{
      kMarosMeszaros + std::string("qbeaconf.sif"), /* filename */
      false,                                        /* verbose */
      false,                     /* tighten_infinite_bounds */
      false,                     /* print_data */
      Real(1.647120601497000e5), /* objective */
      Real(1e-14),               /* relative_objective_tol */
  };
  Test(test_config);
}
#endif  // ifdef TEST_DOUBLEDOUBLE

// The listing on http://www.doc.ic.ac.uk/%7Eim/00README.QP is:
//     3.1002008e+03.

TEST_CASE("qbore3d double", "[qbore3d double]") {
  typedef double Real;
  const TestConfig<Real> test_config{
      kMarosMeszaros + std::string("qbore3d.sif"), /* filename */
      false,                                       /* verbose */
      false,                                       /* tighten_infinite_bounds */
      false,                                       /* print_data */
      Real(3.100200801756597e3),                   /* objective */
      Real(1e-7),                                  /* relative_objective_tol */
  };
  Test(test_config);
}

#ifdef TEST_DOUBLEDOUBLE
TEST_CASE("qbore3d DoubleDouble", "[qbore3d DoubleDouble]") {
  typedef mantis::DoubleMantissa<double> Real;
  const TestConfig<Real> test_config{
      kMarosMeszaros + std::string("qbore3d.sif"), /* filename */
      false,                                       /* verbose */
      false,                                       /* tighten_infinite_bounds */
      false,                                       /* print_data */
      Real(3.100200801756597e3),                   /* objective */
      Real(1e-14),                                 /* relative_objective_tol */
  };
  Test(test_config);
}
#endif  // ifdef TEST_DOUBLEDOUBLE

// The listing on http://www.doc.ic.ac.uk/%7Eim/00README.QP is:
//     2.8375115e+04.

TEST_CASE("qbrandy double", "[qbrandy double]") {
  typedef double Real;
  const TestConfig<Real> test_config{
      kMarosMeszaros + std::string("qbrandy.sif"), /* filename */
      false,                                       /* verbose */
      false,                                       /* tighten_infinite_bounds */
      false,                                       /* print_data */
      Real(2.837511485667098e4),                   /* objective */
      Real(1e-7),                                  /* relative_objective_tol */
  };
  Test(test_config);
}

#ifdef TEST_DOUBLEDOUBLE
TEST_CASE("qbrandy DoubleDouble", "[qbrandy DoubleDouble]") {
  typedef mantis::DoubleMantissa<double> Real;
  const TestConfig<Real> test_config{
      kMarosMeszaros + std::string("qbrandy.sif"), /* filename */
      false,                                       /* verbose */
      false,                                       /* tighten_infinite_bounds */
      false,                                       /* print_data */
      Real(2.837511485667098e4),                   /* objective */
      Real(1e-14),                                 /* relative_objective_tol */
  };
  Test(test_config);
}
#endif  // ifdef TEST_DOUBLEDOUBLE

// The listing on http://www.doc.ic.ac.uk/%7Eim/00README.QP is:
//     6.6793293e+07.

TEST_CASE("qcapri double", "[qcapri double]") {
  typedef double Real;
  const TestConfig<Real> test_config{
      kMarosMeszaros + std::string("qcapri.sif"), /* filename */
      false,                                      /* verbose */
      false,                                      /* tighten_infinite_bounds */
      false,                                      /* print_data */
      Real(6.679329326638888e7),                  /* objective */
      Real(5e-6),                                 /* relative_objective_tol */
  };
  Test(test_config);
}

#ifdef TEST_DOUBLEDOUBLE
TEST_CASE("qcapri DoubleDouble", "[qcapri DoubleDouble]") {
  typedef mantis::DoubleMantissa<double> Real;
  const TestConfig<Real> test_config{
      kMarosMeszaros + std::string("qcapri.sif"), /* filename */
      false,                                      /* verbose */
      false,                                      /* tighten_infinite_bounds */
      false,                                      /* print_data */
      Real(6.679329326638888e7),                  /* objective */
      Real(1e-14),                                /* relative_objective_tol */
  };
  Test(test_config);
}
#endif  // ifdef TEST_DOUBLEDOUBLE

// The listing on http://www.doc.ic.ac.uk/%7Eim/00README.QP is:
//     2.1265343e+02.

TEST_CASE("qe226 double", "[qe226 double]") {
  typedef double Real;
  const TestConfig<Real> test_config{
      kMarosMeszaros + std::string("qe226.sif"), /* filename */
      false,                                     /* verbose */
      false,                                     /* tighten_infinite_bounds */
      false,                                     /* print_data */
      Real(2.126534328684435e2),                 /* objective */
      Real(1e-7),                                /* relative_objective_tol */
  };
  Test(test_config);
}

#ifdef TEST_DOUBLEDOUBLE
TEST_CASE("qe226 DoubleDouble", "[qe226 DoubleDouble]") {
  typedef mantis::DoubleMantissa<double> Real;
  const TestConfig<Real> test_config{
      kMarosMeszaros + std::string("qe226.sif"), /* filename */
      false,                                     /* verbose */
      false,                                     /* tighten_infinite_bounds */
      false,                                     /* print_data */
      Real(2.126534328684435e2),                 /* objective */
      Real(1e-14),                               /* relative_objective_tol */
  };
  Test(test_config);
}
#endif  // ifdef TEST_DOUBLEDOUBLE

// The listing on http://www.doc.ic.ac.uk/%7Eim/00README.QP is:
//     8.6760370e+04.

TEST_CASE("qetamacr double", "[qetamacr double]") {
  typedef double Real;
  const TestConfig<Real> test_config{
      kMarosMeszaros + std::string("qetamacr.sif"), /* filename */
      false,                                        /* verbose */
      false,                     /* tighten_infinite_bounds */
      false,                     /* print_data */
      Real(8.676036962587479e4), /* objective */
      Real(1e-7),                /* relative_objective_tol */
  };
  Test(test_config);
}

#ifdef TEST_DOUBLEDOUBLE
TEST_CASE("qetamacr DoubleDouble", "[qetamacr DoubleDouble]") {
  typedef mantis::DoubleMantissa<double> Real;
  const TestConfig<Real> test_config{
      kMarosMeszaros + std::string("qetamacr.sif"), /* filename */
      false,                                        /* verbose */
      false,                     /* tighten_infinite_bounds */
      false,                     /* print_data */
      Real(8.676036962587479e4), /* objective */
      Real(1e-14),               /* relative_objective_tol */
  };
  Test(test_config);
}
#endif  // ifdef TEST_DOUBLEDOUBLE

// The listing on http://www.doc.ic.ac.uk/%7Eim/00README.QP is:
//     8.7314747e+05.

TEST_CASE("qfffff80 double", "[qfffff80 double]") {
  typedef double Real;
  const TestConfig<Real> test_config{
      kMarosMeszaros + std::string("qfffff80.sif"), /* filename */
      false,                                        /* verbose */
      false,                     /* tighten_infinite_bounds */
      false,                     /* print_data */
      Real(8.731474605185596e5), /* objective */
      Real(1e-7),                /* relative_objective_tol */
  };
  Test(test_config);
}

#ifdef TEST_DOUBLEDOUBLE
TEST_CASE("qfffff80 DoubleDouble", "[qfffff80 DoubleDouble]") {
  typedef mantis::DoubleMantissa<double> Real;
  const TestConfig<Real> test_config{
      kMarosMeszaros + std::string("qfffff80.sif"), /* filename */
      false,                                        /* verbose */
      false,                     /* tighten_infinite_bounds */
      false,                     /* print_data */
      Real(8.731474605185596e5), /* objective */
      Real(1e-14),               /* relative_objective_tol */
  };
  Test(test_config);
}
#endif  // ifdef TEST_DOUBLEDOUBLE

// The listing on http://www.doc.ic.ac.uk/%7Eim/00README.QP is:
//     7.4566315e+09.

TEST_CASE("qforplan double", "[qforplan double]") {
  typedef double Real;
  const TestConfig<Real> test_config{
      kMarosMeszaros + std::string("qforplan.sif"), /* filename */
      false,                                        /* verbose */
      false,                     /* tighten_infinite_bounds */
      false,                     /* print_data */
      Real(7.456631460807258e9), /* objective */
      Real(1e-7),                /* relative_objective_tol */
  };
  Test(test_config);
}

#ifdef TEST_DOUBLEDOUBLE
TEST_CASE("qforplan DoubleDouble", "[qforplan DoubleDouble]") {
  typedef mantis::DoubleMantissa<double> Real;
  const TestConfig<Real> test_config{
      kMarosMeszaros + std::string("qforplan.sif"), /* filename */
      false,                                        /* verbose */
      false,                     /* tighten_infinite_bounds */
      false,                     /* print_data */
      Real(7.456631460807258e9), /* objective */
      Real(1e-14),               /* relative_objective_tol */
  };
  Test(test_config);
}
#endif  // ifdef TEST_DOUBLEDOUBLE

// The listing on http://www.doc.ic.ac.uk/%7Eim/00README.QP is:
//     1.0079059e+11.

TEST_CASE("qgfrdxpn double", "[qgfrdxpn double]") {
  typedef double Real;
  const TestConfig<Real> test_config{
      kMarosMeszaros + std::string("qgfrdxpn.sif"), /* filename */
      false,                                        /* verbose */
      false,                      /* tighten_infinite_bounds */
      false,                      /* print_data */
      Real(1.007905848704238e11), /* objective */
      Real(1e-7),                 /* relative_objective_tol */
  };
  Test(test_config);
}

#ifdef TEST_DOUBLEDOUBLE
TEST_CASE("qgfrdxpn DoubleDouble", "[qgfrdxpn DoubleDouble]") {
  typedef mantis::DoubleMantissa<double> Real;
  const TestConfig<Real> test_config{
      kMarosMeszaros + std::string("qgfrdxpn.sif"), /* filename */
      false,                                        /* verbose */
      false,                      /* tighten_infinite_bounds */
      false,                      /* print_data */
      Real(1.007905848704238e11), /* objective */
      Real(1e-14),                /* relative_objective_tol */
  };
  Test(test_config);
}
#endif  // ifdef TEST_DOUBLEDOUBLE

// The listing on http://www.doc.ic.ac.uk/%7Eim/00README.QP is:
//     -1.0169364e+08.

TEST_CASE("qgrow15 double", "[qgrow15 double]") {
  typedef double Real;
  const TestConfig<Real> test_config{
      kMarosMeszaros + std::string("qgrow15.sif"), /* filename */
      false,                                       /* verbose */
      false,                                       /* tighten_infinite_bounds */
      false,                                       /* print_data */
      Real(-1.016936404682663e8),                  /* objective */
      Real(1e-7),                                  /* relative_objective_tol */
  };
  Test(test_config);
}

#ifdef TEST_DOUBLEDOUBLE
TEST_CASE("qgrow15 DoubleDouble", "[qgrow15 DoubleDouble]") {
  typedef mantis::DoubleMantissa<double> Real;
  const TestConfig<Real> test_config{
      kMarosMeszaros + std::string("qgrow15.sif"), /* filename */
      false,                                       /* verbose */
      false,                                       /* tighten_infinite_bounds */
      false,                                       /* print_data */
      Real(-1.016936404682663e8),                  /* objective */
      Real(1e-14),                                 /* relative_objective_tol */
  };
  Test(test_config);
}
#endif  // ifdef TEST_DOUBLEDOUBLE

// The listing on http://www.doc.ic.ac.uk/%7Eim/00README.QP is:
//     -1.4962895e+08.

TEST_CASE("qgrow22 double", "[qgrow22 double]") {
  typedef double Real;
  const TestConfig<Real> test_config{
      kMarosMeszaros + std::string("qgrow22.sif"), /* filename */
      false,                                       /* verbose */
      false,                                       /* tighten_infinite_bounds */
      false,                                       /* print_data */
      Real(-1.496289534713761e8),                  /* objective */
      Real(1e-7),                                  /* relative_objective_tol */
  };
  Test(test_config);
}

#ifdef TEST_DOUBLEDOUBLE
TEST_CASE("qgrow22 DoubleDouble", "[qgrow22 DoubleDouble]") {
  typedef mantis::DoubleMantissa<double> Real;
  const TestConfig<Real> test_config{
      kMarosMeszaros + std::string("qgrow22.sif"), /* filename */
      false,                                       /* verbose */
      false,                                       /* tighten_infinite_bounds */
      false,                                       /* print_data */
      Real(-1.496289534713761e8),                  /* objective */
      Real(1e-14),                                 /* relative_objective_tol */
  };
  Test(test_config);
}
#endif  // ifdef TEST_DOUBLEDOUBLE

// The listing on http://www.doc.ic.ac.uk/%7Eim/00README.QP is:
//     -4.2798714e+07.

TEST_CASE("qgrow7 double", "[qgrow7 double]") {
  typedef double Real;
  const TestConfig<Real> test_config{
      kMarosMeszaros + std::string("qgrow7.sif"), /* filename */
      false,                                      /* verbose */
      false,                                      /* tighten_infinite_bounds */
      false,                                      /* print_data */
      Real(-4.279871387254134e7),                 /* objective */
      Real(1e-7),                                 /* relative_objective_tol */
  };
  Test(test_config);
}

#ifdef TEST_DOUBLEDOUBLE
TEST_CASE("qgrow7 DoubleDouble", "[qgrow7 DoubleDouble]") {
  typedef mantis::DoubleMantissa<double> Real;
  const TestConfig<Real> test_config{
      kMarosMeszaros + std::string("qgrow7.sif"), /* filename */
      false,                                      /* verbose */
      false,                                      /* tighten_infinite_bounds */
      false,                                      /* print_data */
      Real(-4.279871387254134e7),                 /* objective */
      Real(1e-14),                                /* relative_objective_tol */
  };
  Test(test_config);
}
#endif  // ifdef TEST_DOUBLEDOUBLE

// The listing on http://www.doc.ic.ac.uk/%7Eim/00README.QP is:
//     2.5347838e+07.

TEST_CASE("qisrael double", "[qisrael double]") {
  typedef double Real;
  const TestConfig<Real> test_config{
      kMarosMeszaros + std::string("qisrael.sif"), /* filename */
      false,                                       /* verbose */
      false,                                       /* tighten_infinite_bounds */
      false,                                       /* print_data */
      Real(2.534783778912154e7),                   /* objective */
      Real(1e-7),                                  /* relative_objective_tol */
  };
  Test(test_config);
}

#ifdef TEST_DOUBLEDOUBLE
TEST_CASE("qisrael DoubleDouble", "[qisrael DoubleDouble]") {
  typedef mantis::DoubleMantissa<double> Real;
  const TestConfig<Real> test_config{
      kMarosMeszaros + std::string("qisrael.sif"), /* filename */
      false,                                       /* verbose */
      false,                                       /* tighten_infinite_bounds */
      false,                                       /* print_data */
      Real(2.534783778912154e7),                   /* objective */
      Real(1e-14),                                 /* relative_objective_tol */
  };
  Test(test_config);
}
#endif  // ifdef TEST_DOUBLEDOUBLE

// The listing on http://www.doc.ic.ac.uk/%7Eim/00README.QP is:
//     -7.8425409e-03.

TEST_CASE("qpcblend double", "[qpcblend double]") {
  typedef double Real;
  const TestConfig<Real> test_config{
      kMarosMeszaros + std::string("qpcblend.sif"), /* filename */
      false,                                        /* verbose */
      false,                       /* tighten_infinite_bounds */
      false,                       /* print_data */
      Real(-7.842543074204642e-3), /* objective */
      Real(1e-7),                  /* relative_objective_tol */
  };
  Test(test_config);
}

#ifdef TEST_DOUBLEDOUBLE
TEST_CASE("qpcblend DoubleDouble", "[qpcblend DoubleDouble]") {
  typedef mantis::DoubleMantissa<double> Real;
  const TestConfig<Real> test_config{
      kMarosMeszaros + std::string("qpcblend.sif"), /* filename */
      false,                                        /* verbose */
      false,                       /* tighten_infinite_bounds */
      false,                       /* print_data */
      Real(-7.842543074204642e-3), /* objective */
      Real(1e-14),                 /* relative_objective_tol */
  };
  Test(test_config);
}
#endif  // ifdef TEST_DOUBLEDOUBLE

// The listing on http://www.doc.ic.ac.uk/%7Eim/00README.QP is:
//     1.1503914e+07.

TEST_CASE("qpcboei1 double", "[qpcboei1 double]") {
  typedef double Real;
  const TestConfig<Real> test_config{
      kMarosMeszaros + std::string("qpcboei1.sif"), /* filename */
      false,                                        /* verbose */
      false,                     /* tighten_infinite_bounds */
      false,                     /* print_data */
      Real(1.150391400976823e7), /* objective */
      Real(1e-7),                /* relative_objective_tol */
  };
  Test(test_config);
}

#ifdef TEST_DOUBLEDOUBLE
TEST_CASE("qpcboei1 DoubleDouble", "[qpcboei1 DoubleDouble]") {
  typedef mantis::DoubleMantissa<double> Real;
  const TestConfig<Real> test_config{
      kMarosMeszaros + std::string("qpcboei1.sif"), /* filename */
      false,                                        /* verbose */
      false,                     /* tighten_infinite_bounds */
      false,                     /* print_data */
      Real(1.150391400976823e7), /* objective */
      Real(1e-14),               /* relative_objective_tol */
  };
  Test(test_config);
}
#endif  // ifdef TEST_DOUBLEDOUBLE

// The listing on http://www.doc.ic.ac.uk/%7Eim/00README.QP is:
//     8.1719623e+06.

TEST_CASE("qpcboei2 double", "[qpcboei2 double]") {
  typedef double Real;
  const TestConfig<Real> test_config{
      kMarosMeszaros + std::string("qpcboei2.sif"), /* filename */
      false,                                        /* verbose */
      false,                     /* tighten_infinite_bounds */
      false,                     /* print_data */
      Real(8.171962244330412e6), /* objective */
      Real(1e-7),                /* relative_objective_tol */
  };
  Test(test_config);
}

#ifdef TEST_DOUBLEDOUBLE
TEST_CASE("qpcboei2 DoubleDouble", "[qpcboei2 DoubleDouble]") {
  typedef mantis::DoubleMantissa<double> Real;
  const TestConfig<Real> test_config{
      kMarosMeszaros + std::string("qpcboei2.sif"), /* filename */
      false,                                        /* verbose */
      false,                     /* tighten_infinite_bounds */
      false,                     /* print_data */
      Real(8.171962244330412e6), /* objective */
      Real(1e-14),               /* relative_objective_tol */
  };
  Test(test_config);
}
#endif  // ifdef TEST_DOUBLEDOUBLE

// The listing on http://www.doc.ic.ac.uk/%7Eim/00README.QP is:
//     6.2043875e+06.

TEST_CASE("qpcstair double", "[qpcstair double]") {
  typedef double Real;
  const TestConfig<Real> test_config{
      kMarosMeszaros + std::string("qpcstair.sif"), /* filename */
      false,                                        /* verbose */
      false,                     /* tighten_infinite_bounds */
      false,                     /* print_data */
      Real(6.204387476082529e6), /* objective */
      Real(1e-7),                /* relative_objective_tol */
  };
  Test(test_config);
}

#ifdef TEST_DOUBLEDOUBLE
TEST_CASE("qpcstair DoubleDouble", "[qpcstair DoubleDouble]") {
  typedef mantis::DoubleMantissa<double> Real;
  const TestConfig<Real> test_config{
      kMarosMeszaros + std::string("qpcstair.sif"), /* filename */
      false,                                        /* verbose */
      false,                     /* tighten_infinite_bounds */
      false,                     /* print_data */
      Real(6.204387476082529e6), /* objective */
      Real(1e-14),               /* relative_objective_tol */
  };
  Test(test_config);
}
#endif  // ifdef TEST_DOUBLEDOUBLE

// The listing on http://www.doc.ic.ac.uk/%7Eim/00README.QP is:
//     4.7285869e+06.

TEST_CASE("qpilotno double", "[qpilotno double]") {
  typedef double Real;
  const TestConfig<Real> test_config{
      kMarosMeszaros + std::string("qpilotno.sif"), /* filename */
      false,                                        /* verbose */
      false,               /* tighten_infinite_bounds */
      false,               /* print_data */
      Real(4.7285869e+06), /* objective */
      Real(1e-7),          /* relative_objective_tol */
  };
  Test(test_config);
}

#ifdef TEST_DOUBLEDOUBLE
#ifdef TEST_QPILOTNO_DOUBLEDOUBLE
TEST_CASE("qpilotno DoubleDouble", "[qpilotno DoubleDouble]") {
  typedef mantis::DoubleMantissa<double> Real;
  const TestConfig<Real> test_config{
      kMarosMeszaros + std::string("qpilotno.sif"), /* filename */
      false,                                        /* verbose */
      false,               /* tighten_infinite_bounds */
      false,               /* print_data */
      Real(4.7285869e+06), /* objective */
      Real(1e-14),         /* relative_objective_tol */
  };
  Test(test_config);
}
#endif  // ifdef TEST_QPILOTNO_DOUBLEDOUBLE
#endif  // ifdef TEST_DOUBLEDOUBLE

// The listing on http://www.doc.ic.ac.uk/%7Eim/00README.QP is:
//     4.3718750e+00.

TEST_CASE("qptest double", "[qptest double]") {
  typedef double Real;
  const TestConfig<Real> test_config{
      kMarosMeszaros + std::string("qptest.sif"), /* filename */
      false,                                      /* verbose */
      false,                                      /* tighten_infinite_bounds */
      false,                                      /* print_data */
      Real(4.371875000000000),                    /* objective */
      Real(1e-7),                                 /* relative_objective_tol */
  };
  Test(test_config);
}

#ifdef TEST_DOUBLEDOUBLE
TEST_CASE("qptest DoubleDouble", "[qptest DoubleDouble]") {
  typedef mantis::DoubleMantissa<double> Real;
  const TestConfig<Real> test_config{
      kMarosMeszaros + std::string("qptest.sif"), /* filename */
      false,                                      /* verbose */
      false,                                      /* tighten_infinite_bounds */
      false,                                      /* print_data */
      Real(4.371875000000000),                    /* objective */
      Real(1e-14),                                /* relative_objective_tol */
  };
  Test(test_config);
}
#endif  // ifdef TEST_DOUBLEDOUBLE

// The listing on http://www.doc.ic.ac.uk/%7Eim/00README.QP is:
//     -2.6661600e+02.

TEST_CASE("qrecipe double", "[qrecipe double]") {
  typedef double Real;
  const TestConfig<Real> test_config{
      kMarosMeszaros + std::string("qrecipe.sif"), /* filename */
      false,                                       /* verbose */
      false,                                       /* tighten_infinite_bounds */
      false,                                       /* print_data */
      Real(-2.666160000000000e+02),                /* objective */
      Real(1e-7),                                  /* relative_objective_tol */
  };
  Test(test_config);
}

#ifdef TEST_DOUBLEDOUBLE
TEST_CASE("qrecipe DoubleDouble", "[qrecipe DoubleDouble]") {
  typedef mantis::DoubleMantissa<double> Real;
  const TestConfig<Real> test_config{
      kMarosMeszaros + std::string("qrecipe.sif"), /* filename */
      false,                                       /* verbose */
      false,                                       /* tighten_infinite_bounds */
      false,                                       /* print_data */
      Real(-2.666160000000000e+02),                /* objective */
      Real(1e-14),                                 /* relative_objective_tol */
  };
  Test(test_config);
}
#endif  // ifdef TEST_DOUBLEDOUBLE

// The listing on http://www.doc.ic.ac.uk/%7Eim/00README.QP is:
//     -5.8139518e-03.

TEST_CASE("qsc205 double", "[qsc205 double]") {
  typedef double Real;
  const TestConfig<Real> test_config{
      kMarosMeszaros + std::string("qsc205.sif"), /* filename */
      false,                                      /* verbose */
      false,                                      /* tighten_infinite_bounds */
      false,                                      /* print_data */
      Real(-5.813953488372093e-3),                /* objective */
      Real(1e-7),                                 /* relative_objective_tol */
  };
  Test(test_config);
}

#ifdef TEST_DOUBLEDOUBLE
TEST_CASE("qsc205 DoubleDouble", "[qsc205 DoubleDouble]") {
  typedef mantis::DoubleMantissa<double> Real;
  const TestConfig<Real> test_config{
      kMarosMeszaros + std::string("qsc205.sif"), /* filename */
      false,                                      /* verbose */
      false,                                      /* tighten_infinite_bounds */
      false,                                      /* print_data */
      Real(-5.813953488372093e-3),                /* objective */
      Real(1e-14),                                /* relative_objective_tol */
  };
  Test(test_config);
}
#endif  // ifdef TEST_DOUBLEDOUBLE

// The listing on http://www.doc.ic.ac.uk/%7Eim/00README.QP is:
//     2.0173794e+08.

TEST_CASE("qscagr25 double", "[qscagr25 double]") {
  typedef double Real;
  const TestConfig<Real> test_config{
      kMarosMeszaros + std::string("qscagr25.sif"), /* filename */
      false,                                        /* verbose */
      false,                     /* tighten_infinite_bounds */
      false,                     /* print_data */
      Real(2.017379383707121e8), /* objective */
      Real(1e-7),                /* relative_objective_tol */
  };
  Test(test_config);
}

#ifdef TEST_DOUBLEDOUBLE
TEST_CASE("qscagr25 DoubleDouble", "[qscagr25 DoubleDouble]") {
  typedef mantis::DoubleMantissa<double> Real;
  const TestConfig<Real> test_config{
      kMarosMeszaros + std::string("qscagr25.sif"), /* filename */
      false,                                        /* verbose */
      false,                     /* tighten_infinite_bounds */
      false,                     /* print_data */
      Real(2.017379383707121e8), /* objective */
      Real(1e-14),               /* relative_objective_tol */
  };
  Test(test_config);
}
#endif  // ifdef TEST_DOUBLEDOUBLE

// The listing on http://www.doc.ic.ac.uk/%7Eim/00README.QP is:
//     2.6865949e+07.

TEST_CASE("qscagr7 double", "[qscagr7 double]") {
  typedef double Real;
  const TestConfig<Real> test_config{
      kMarosMeszaros + std::string("qscagr7.sif"), /* filename */
      false,                                       /* verbose */
      false,                                       /* tighten_infinite_bounds */
      false,                                       /* print_data */
      Real(2.686594858902268e7),                   /* objective */
      Real(1e-7),                                  /* relative_objective_tol */
  };
  Test(test_config);
}

#ifdef TEST_DOUBLEDOUBLE
TEST_CASE("qscagr7 DoubleDouble", "[qscagr7 DoubleDouble]") {
  typedef mantis::DoubleMantissa<double> Real;
  const TestConfig<Real> test_config{
      kMarosMeszaros + std::string("qscagr7.sif"), /* filename */
      false,                                       /* verbose */
      false,                                       /* tighten_infinite_bounds */
      false,                                       /* print_data */
      Real(2.686594858902268e7),                   /* objective */
      Real(1e-14),                                 /* relative_objective_tol */
  };
  Test(test_config);
}
#endif  // ifdef TEST_DOUBLEDOUBLE

// The listing on http://www.doc.ic.ac.uk/%7Eim/00README.QP is:
//     1.6882692e+07.

TEST_CASE("qscfxm1 double", "[qscfxm1 double]") {
  typedef double Real;
  const TestConfig<Real> test_config{
      kMarosMeszaros + std::string("qscfxm1.sif"), /* filename */
      false,                                       /* verbose */
      false,                                       /* tighten_infinite_bounds */
      false,                                       /* print_data */
      Real(1.688269163931455e7),                   /* objective */
      Real(1e-7),                                  /* relative_objective_tol */
  };
  Test(test_config);
}

#ifdef TEST_DOUBLEDOUBLE
TEST_CASE("qscfxm1 DoubleDouble", "[qscfxm1 DoubleDouble]") {
  typedef mantis::DoubleMantissa<double> Real;
  const TestConfig<Real> test_config{
      kMarosMeszaros + std::string("qscfxm1.sif"), /* filename */
      false,                                       /* verbose */
      false,                                       /* tighten_infinite_bounds */
      false,                                       /* print_data */
      Real(1.688269163931455e7),                   /* objective */
      Real(1e-14),                                 /* relative_objective_tol */
  };
  Test(test_config);
}
#endif  // ifdef TEST_DOUBLEDOUBLE

// The listing on http://www.doc.ic.ac.uk/%7Eim/00README.QP is:
//     2.7776162e+07.

TEST_CASE("qscfxm2 double", "[qscfxm2 double]") {
  typedef double Real;
  const TestConfig<Real> test_config{
      kMarosMeszaros + std::string("qscfxm2.sif"), /* filename */
      false,                                       /* verbose */
      false,                                       /* tighten_infinite_bounds */
      false,                                       /* print_data */
      Real(2.777616157861072e7),                   /* objective */
      Real(1e-7),                                  /* relative_objective_tol */
  };
  Test(test_config);
}

#ifdef TEST_DOUBLEDOUBLE
TEST_CASE("qscfxm2 DoubleDouble", "[qscfxm2 DoubleDouble]") {
  typedef mantis::DoubleMantissa<double> Real;
  const TestConfig<Real> test_config{
      kMarosMeszaros + std::string("qscfxm2.sif"), /* filename */
      false,                                       /* verbose */
      false,                                       /* tighten_infinite_bounds */
      false,                                       /* print_data */
      Real(2.777616157861072e7),                   /* objective */
      Real(1e-14),                                 /* relative_objective_tol */
  };
  Test(test_config);
}
#endif  // ifdef TEST_DOUBLEDOUBLE

// The listing on http://www.doc.ic.ac.uk/%7Eim/00README.QP is:
//     3.0816355e+07.

TEST_CASE("qscfxm3 double", "[qscfxm3 double]") {
  typedef double Real;
  const TestConfig<Real> test_config{
      kMarosMeszaros + std::string("qscfxm3.sif"), /* filename */
      false,                                       /* verbose */
      false,                                       /* tighten_infinite_bounds */
      false,                                       /* print_data */
      Real(3.081635447086658e7),                   /* objective */
      Real(1e-7),                                  /* relative_objective_tol */
  };
  Test(test_config);
}

#ifdef TEST_DOUBLEDOUBLE
TEST_CASE("qscfxm3 DoubleDouble", "[qscfxm3 DoubleDouble]") {
  typedef mantis::DoubleMantissa<double> Real;
  const TestConfig<Real> test_config{
      kMarosMeszaros + std::string("qscfxm3.sif"), /* filename */
      false,                                       /* verbose */
      false,                                       /* tighten_infinite_bounds */
      false,                                       /* print_data */
      Real(3.081635447086658e7),                   /* objective */
      Real(1e-14),                                 /* relative_objective_tol */
  };
  Test(test_config);
}
#endif  // ifdef TEST_DOUBLEDOUBLE

// The listing on http://www.doc.ic.ac.uk/%7Eim/00README.QP is:
//     1.8805096e+03.

TEST_CASE("qscorpio double", "[qscorpio double]") {
  typedef double Real;
  const TestConfig<Real> test_config{
      kMarosMeszaros + std::string("qscorpio.sif"), /* filename */
      false,                                        /* verbose */
      false,                     /* tighten_infinite_bounds */
      false,                     /* print_data */
      Real(1.880509552981970e3), /* objective */
      Real(1e-7),                /* relative_objective_tol */
  };
  Test(test_config);
}

#ifdef TEST_DOUBLEDOUBLE
TEST_CASE("qscorpio DoubleDouble", "[qscorpio DoubleDouble]") {
  typedef mantis::DoubleMantissa<double> Real;
  const TestConfig<Real> test_config{
      kMarosMeszaros + std::string("qscorpio.sif"), /* filename */
      false,                                        /* verbose */
      false,                     /* tighten_infinite_bounds */
      false,                     /* print_data */
      Real(1.880509552981970e3), /* objective */
      Real(1e-14),               /* relative_objective_tol */
  };
  Test(test_config);
}
#endif  // ifdef TEST_DOUBLEDOUBLE

// The listing on http://www.doc.ic.ac.uk/%7Eim/00README.QP is:
//     9.0456001e+02.

TEST_CASE("qscrs8 double", "[qscrs8 double]") {
  typedef double Real;
  const TestConfig<Real> test_config{
      kMarosMeszaros + std::string("qscrs8.sif"), /* filename */
      false,                                      /* verbose */
      false,                                      /* tighten_infinite_bounds */
      false,                                      /* print_data */
      Real(9.045600138507914e2),                  /* objective */
      Real(1e-7),                                 /* relative_objective_tol */
  };
  Test(test_config);
}

#ifdef TEST_DOUBLEDOUBLE
TEST_CASE("qscrs8 DoubleDouble", "[qscrs8 DoubleDouble]") {
  typedef mantis::DoubleMantissa<double> Real;
  const TestConfig<Real> test_config{
      kMarosMeszaros + std::string("qscrs8.sif"), /* filename */
      false,                                      /* verbose */
      false,                                      /* tighten_infinite_bounds */
      false,                                      /* print_data */
      Real(9.045600138507914e2),                  /* objective */
      Real(1e-14),                                /* relative_objective_tol */
  };
  Test(test_config);
}
#endif  // ifdef TEST_DOUBLEDOUBLE

// The listing on http://www.doc.ic.ac.uk/%7Eim/00README.QP is:
//     8.6666667e+00.

TEST_CASE("qscsd1 double", "[qscsd1 double]") {
  typedef double Real;
  const TestConfig<Real> test_config{
      kMarosMeszaros + std::string("qscsd1.sif"), /* filename */
      false,                                      /* verbose */
      false,                                      /* tighten_infinite_bounds */
      false,                                      /* print_data */
      Real(8.666666674333365),                    /* objective */
      Real(1e-7),                                 /* relative_objective_tol */
  };
  Test(test_config);
}

#ifdef TEST_DOUBLEDOUBLE
TEST_CASE("qscsd1 DoubleDouble", "[qscsd1 DoubleDouble]") {
  typedef mantis::DoubleMantissa<double> Real;
  const TestConfig<Real> test_config{
      kMarosMeszaros + std::string("qscsd1.sif"), /* filename */
      false,                                      /* verbose */
      false,                                      /* tighten_infinite_bounds */
      false,                                      /* print_data */
      Real(8.666666674333365),                    /* objective */
      Real(1e-14),                                /* relative_objective_tol */
  };
  Test(test_config);
}
#endif  // ifdef TEST_DOUBLEDOUBLE

// The listing on http://www.doc.ic.ac.uk/%7Eim/00README.QP is:
//     5.0808214e+01.

TEST_CASE("qscsd6 double", "[qscsd6 double]") {
  typedef double Real;
  const TestConfig<Real> test_config{
      kMarosMeszaros + std::string("qscsd6.sif"), /* filename */
      false,                                      /* verbose */
      false,                                      /* tighten_infinite_bounds */
      false,                                      /* print_data */
      Real(5.080821389927335e1),                  /* objective */
      Real(1e-7),                                 /* relative_objective_tol */
  };
  Test(test_config);
}

#ifdef TEST_DOUBLEDOUBLE
TEST_CASE("qscsd6 DoubleDouble", "[qscsd6 DoubleDouble]") {
  typedef mantis::DoubleMantissa<double> Real;
  const TestConfig<Real> test_config{
      kMarosMeszaros + std::string("qscsd6.sif"), /* filename */
      false,                                      /* verbose */
      false,                                      /* tighten_infinite_bounds */
      false,                                      /* print_data */
      Real(5.080821389927335e1),                  /* objective */
      Real(1e-14),                                /* relative_objective_tol */
  };
  Test(test_config);
}
#endif  // ifdef TEST_DOUBLEDOUBLE

// The listing on http://www.doc.ic.ac.uk/%7Eim/00README.QP is:
//     9.4076357e+02.

TEST_CASE("qscsd8 double", "[qscsd8 double]") {
  typedef double Real;
  const TestConfig<Real> test_config{
      kMarosMeszaros + std::string("qscsd8.sif"), /* filename */
      false,                                      /* verbose */
      false,                                      /* tighten_infinite_bounds */
      false,                                      /* print_data */
      Real(9.407635742185008e2),                  /* objective */
      Real(1e-7),                                 /* relative_objective_tol */
  };
  Test(test_config);
}

#ifdef TEST_DOUBLEDOUBLE
TEST_CASE("qscsd8 DoubleDouble", "[qscsd8 DoubleDouble]") {
  typedef mantis::DoubleMantissa<double> Real;
  const TestConfig<Real> test_config{
      kMarosMeszaros + std::string("qscsd8.sif"), /* filename */
      false,                                      /* verbose */
      false,                                      /* tighten_infinite_bounds */
      false,                                      /* print_data */
      Real(9.407635742185008e2),                  /* objective */
      Real(1e-14),                                /* relative_objective_tol */
  };
  Test(test_config);
}
#endif  // ifdef TEST_DOUBLEDOUBLE

// The listing on http://www.doc.ic.ac.uk/%7Eim/00README.QP is:
//     1.4158611e+03.

TEST_CASE("qsctap1 double", "[qsctap1 double]") {
  typedef double Real;
  const TestConfig<Real> test_config{
      kMarosMeszaros + std::string("qsctap1.sif"), /* filename */
      false,                                       /* verbose */
      false,                                       /* tighten_infinite_bounds */
      false,                                       /* print_data */
      Real(1.415861111111111e3),                   /* objective */
      Real(1e-7),                                  /* relative_objective_tol */
  };
  Test(test_config);
}

#ifdef TEST_DOUBLEDOUBLE
TEST_CASE("qsctap1 DoubleDouble", "[qsctap1 DoubleDouble]") {
  typedef mantis::DoubleMantissa<double> Real;
  const TestConfig<Real> test_config{
      kMarosMeszaros + std::string("qsctap1.sif"), /* filename */
      false,                                       /* verbose */
      false,                                       /* tighten_infinite_bounds */
      false,                                       /* print_data */
      Real(1.415861111111111e3),                   /* objective */
      Real(1e-14),                                 /* relative_objective_tol */
  };
  Test(test_config);
}
#endif  // ifdef TEST_DOUBLEDOUBLE

// The listing on http://www.doc.ic.ac.uk/%7Eim/00README.QP is:
//     1.7350265e+03.

TEST_CASE("qsctap2 double", "[qsctap2 double]") {
  typedef double Real;
  const TestConfig<Real> test_config{
      kMarosMeszaros + std::string("qsctap2.sif"), /* filename */
      false,                                       /* verbose */
      false,                                       /* tighten_infinite_bounds */
      false,                                       /* print_data */
      Real(1.735026497695853e3),                   /* objective */
      Real(1e-7),                                  /* relative_objective_tol */
  };
  Test(test_config);
}

#ifdef TEST_DOUBLEDOUBLE
TEST_CASE("qsctap2 DoubleDouble", "[qsctap2 DoubleDouble]") {
  typedef mantis::DoubleMantissa<double> Real;
  const TestConfig<Real> test_config{
      kMarosMeszaros + std::string("qsctap2.sif"), /* filename */
      false,                                       /* verbose */
      false,                                       /* tighten_infinite_bounds */
      false,                                       /* print_data */
      Real(1.735026497695853e3),                   /* objective */
      Real(1e-14),                                 /* relative_objective_tol */
  };
  Test(test_config);
}
#endif  // ifdef TEST_DOUBLEDOUBLE

// The listing on http://www.doc.ic.ac.uk/%7Eim/00README.QP is:
//     1.4387547e+03.

TEST_CASE("qsctap3 double", "[qsctap3 double]") {
  typedef double Real;
  const TestConfig<Real> test_config{
      kMarosMeszaros + std::string("qsctap3.sif"), /* filename */
      false,                                       /* verbose */
      false,                                       /* tighten_infinite_bounds */
      false,                                       /* print_data */
      Real(1.438754680925666e3),                   /* objective */
      Real(1e-7),                                  /* relative_objective_tol */
  };
  Test(test_config);
}

#ifdef TEST_DOUBLEDOUBLE
TEST_CASE("qsctap3 DoubleDouble", "[qsctap3 DoubleDouble]") {
  typedef mantis::DoubleMantissa<double> Real;
  const TestConfig<Real> test_config{
      kMarosMeszaros + std::string("qsctap3.sif"), /* filename */
      false,                                       /* verbose */
      false,                                       /* tighten_infinite_bounds */
      false,                                       /* print_data */
      Real(1.438754680925666e3),                   /* objective */
      Real(1e-14),                                 /* relative_objective_tol */
  };
  Test(test_config);
}
#endif  // ifdef TEST_DOUBLEDOUBLE

// The listing on http://www.doc.ic.ac.uk/%7Eim/00README.QP is:
//     8.1481801e+07.

TEST_CASE("qseba double", "[qseba double]") {
  typedef double Real;
  const TestConfig<Real> test_config{
      kMarosMeszaros + std::string("qseba.sif"), /* filename */
      false,                                     /* verbose */
      false,                                     /* tighten_infinite_bounds */
      false,                                     /* print_data */
      Real(8.148180035682763e7),                 /* objective */
      Real(1e-7),                                /* relative_objective_tol */
  };
  Test(test_config);
}

#ifdef TEST_DOUBLEDOUBLE
TEST_CASE("qseba DoubleDouble", "[qseba DoubleDouble]") {
  typedef mantis::DoubleMantissa<double> Real;
  const TestConfig<Real> test_config{
      kMarosMeszaros + std::string("qseba.sif"), /* filename */
      false,                                     /* verbose */
      false,                                     /* tighten_infinite_bounds */
      false,                                     /* print_data */
      Real(8.148180035682763e7),                 /* objective */
      Real(1e-14),                               /* relative_objective_tol */
  };
  Test(test_config);
}
#endif  // ifdef TEST_DOUBLEDOUBLE

// The listing on http://www.doc.ic.ac.uk/%7Eim/00README.QP is:
//     7.2007832e+05.

TEST_CASE("qshare1b double", "[qshare1b double]") {
  typedef double Real;
  const TestConfig<Real> test_config{
      kMarosMeszaros + std::string("qshare1b.sif"), /* filename */
      false,                                        /* verbose */
      false,                     /* tighten_infinite_bounds */
      false,                     /* print_data */
      Real(7.200783181538213e5), /* objective */
      Real(1e-7),                /* relative_objective_tol */
  };
  Test(test_config);
}

#ifdef TEST_DOUBLEDOUBLE
TEST_CASE("qshare1b DoubleDouble", "[qshare1b DoubleDouble]") {
  typedef mantis::DoubleMantissa<double> Real;
  const TestConfig<Real> test_config{
      kMarosMeszaros + std::string("qshare1b.sif"), /* filename */
      false,                                        /* verbose */
      false,                     /* tighten_infinite_bounds */
      false,                     /* print_data */
      Real(7.200783181538213e5), /* objective */
      Real(1e-14),               /* relative_objective_tol */
  };
  Test(test_config);
}
#endif  // ifdef TEST_DOUBLEDOUBLE

// The listing on http://www.doc.ic.ac.uk/%7Eim/00README.QP is:
//     1.1703692e+04.

TEST_CASE("qshare2b double", "[qshare2b double]") {
  typedef double Real;
  const TestConfig<Real> test_config{
      kMarosMeszaros + std::string("qshare2b.sif"), /* filename */
      false,                                        /* verbose */
      false,                     /* tighten_infinite_bounds */
      false,                     /* print_data */
      Real(1.170369172151640e4), /* objective */
      Real(1e-7),                /* relative_objective_tol */
  };
  Test(test_config);
}

#ifdef TEST_DOUBLEDOUBLE
TEST_CASE("qshare2b DoubleDouble", "[qshare2b DoubleDouble]") {
  typedef mantis::DoubleMantissa<double> Real;
  const TestConfig<Real> test_config{
      kMarosMeszaros + std::string("qshare2b.sif"), /* filename */
      false,                                        /* verbose */
      false,                     /* tighten_infinite_bounds */
      false,                     /* print_data */
      Real(1.170369172151640e4), /* objective */
      Real(1e-14),               /* relative_objective_tol */
  };
  Test(test_config);
}
#endif  // ifdef TEST_DOUBLEDOUBLE

// The listing on http://www.doc.ic.ac.uk/%7Eim/00README.QP is:
//     1.5726368e+12.

TEST_CASE("qshell double", "[qshell double]") {
  typedef double Real;
  const TestConfig<Real> test_config{
      kMarosMeszaros + std::string("qshell.sif"), /* filename */
      false,                                      /* verbose */
      false,                                      /* tighten_infinite_bounds */
      false,                                      /* print_data */
      Real(1.572636842999026e12),                 /* objective */
      Real(1e-7),                                 /* relative_objective_tol */
  };
  Test(test_config);
}

#ifdef TEST_DOUBLEDOUBLE
TEST_CASE("qshell DoubleDouble", "[qshell DoubleDouble]") {
  typedef mantis::DoubleMantissa<double> Real;
  const TestConfig<Real> test_config{
      kMarosMeszaros + std::string("qshell.sif"), /* filename */
      false,                                      /* verbose */
      false,                                      /* tighten_infinite_bounds */
      false,                                      /* print_data */
      Real(1.572636842999026e12),                 /* objective */
      Real(1e-14),                                /* relative_objective_tol */
  };
  Test(test_config);
}
#endif  // ifdef TEST_DOUBLEDOUBLE

// The listing on http://www.doc.ic.ac.uk/%7Eim/00README.QP is:
//     2.4200155e+06.

TEST_CASE("qship04l double", "[qship04l double]") {
  typedef double Real;
  const TestConfig<Real> test_config{
      kMarosMeszaros + std::string("qship04l.sif"), /* filename */
      false,                                        /* verbose */
      false,                     /* tighten_infinite_bounds */
      false,                     /* print_data */
      Real(2.420015534110433e6), /* objective */
      Real(1e-7),                /* relative_objective_tol */
  };
  Test(test_config);
}

#ifdef TEST_DOUBLEDOUBLE
TEST_CASE("qship04l DoubleDouble", "[qship04l DoubleDouble]") {
  typedef mantis::DoubleMantissa<double> Real;
  const TestConfig<Real> test_config{
      kMarosMeszaros + std::string("qship04l.sif"), /* filename */
      false,                                        /* verbose */
      false,                     /* tighten_infinite_bounds */
      false,                     /* print_data */
      Real(2.420015534110433e6), /* objective */
      Real(1e-14),               /* relative_objective_tol */
  };
  Test(test_config);
}
#endif  // ifdef TEST_DOUBLEDOUBLE

// The listing on http://www.doc.ic.ac.uk/%7Eim/00README.QP is:
//     2.4249937e+06.

TEST_CASE("qship04s double", "[qship04s double]") {
  typedef double Real;
  const TestConfig<Real> test_config{
      kMarosMeszaros + std::string("qship04s.sif"), /* filename */
      false,                                        /* verbose */
      false,                     /* tighten_infinite_bounds */
      false,                     /* print_data */
      Real(2.424993673004610e6), /* objective */
      Real(1e-7),                /* relative_objective_tol */
  };
  Test(test_config);
}

#ifdef TEST_DOUBLEDOUBLE
TEST_CASE("qship04s DoubleDouble", "[qship04s DoubleDouble]") {
  typedef mantis::DoubleMantissa<double> Real;
  const TestConfig<Real> test_config{
      kMarosMeszaros + std::string("qship04s.sif"), /* filename */
      false,                                        /* verbose */
      false,                     /* tighten_infinite_bounds */
      false,                     /* print_data */
      Real(2.424993673004610e6), /* objective */
      Real(1e-14),               /* relative_objective_tol */
  };
  Test(test_config);
}
#endif  // ifdef TEST_DOUBLEDOUBLE

// The listing on http://www.doc.ic.ac.uk/%7Eim/00README.QP is:
//     2.3760406e+06.

TEST_CASE("qship08l double", "[qship08l double]") {
  typedef double Real;
  const TestConfig<Real> test_config{
      kMarosMeszaros + std::string("qship08l.sif"), /* filename */
      false,                                        /* verbose */
      false,                     /* tighten_infinite_bounds */
      false,                     /* print_data */
      Real(2.376040616579164e6), /* objective */
      Real(1e-7),                /* relative_objective_tol */
  };
  Test(test_config);
}

#ifdef TEST_DOUBLEDOUBLE
TEST_CASE("qship08l DoubleDouble", "[qship08l DoubleDouble]") {
  typedef mantis::DoubleMantissa<double> Real;
  const TestConfig<Real> test_config{
      kMarosMeszaros + std::string("qship08l.sif"), /* filename */
      false,                                        /* verbose */
      false,                     /* tighten_infinite_bounds */
      false,                     /* print_data */
      Real(2.376040616579164e6), /* objective */
      Real(1e-14),               /* relative_objective_tol */
  };
  Test(test_config);
}
#endif  // ifdef TEST_DOUBLEDOUBLE

// The listing on http://www.doc.ic.ac.uk/%7Eim/00README.QP is:
//     2.3857289e+06.

TEST_CASE("qship08s double", "[qship08s double]") {
  typedef double Real;
  const TestConfig<Real> test_config{
      kMarosMeszaros + std::string("qship08s.sif"), /* filename */
      false,                                        /* verbose */
      false,                     /* tighten_infinite_bounds */
      false,                     /* print_data */
      Real(2.385728851390566e6), /* objective */
      Real(1e-7),                /* relative_objective_tol */
  };
  Test(test_config);
}

#ifdef TEST_DOUBLEDOUBLE
TEST_CASE("qship08s DoubleDouble", "[qship08s DoubleDouble]") {
  typedef mantis::DoubleMantissa<double> Real;
  const TestConfig<Real> test_config{
      kMarosMeszaros + std::string("qship08s.sif"), /* filename */
      false,                                        /* verbose */
      false,                     /* tighten_infinite_bounds */
      false,                     /* print_data */
      Real(2.385728851390566e6), /* objective */
      Real(1e-14),               /* relative_objective_tol */
  };
  Test(test_config);
}
#endif  // ifdef TEST_DOUBLEDOUBLE

// The listing on http://www.doc.ic.ac.uk/%7Eim/00README.QP is:
//     3.0188766e+06.

TEST_CASE("qship12l double", "[qship12l double]") {
  typedef double Real;
  const TestConfig<Real> test_config{
      kMarosMeszaros + std::string("qship12l.sif"), /* filename */
      false,                                        /* verbose */
      false,                     /* tighten_infinite_bounds */
      false,                     /* print_data */
      Real(3.018876577180347e6), /* objective */
      Real(1e-7),                /* relative_objective_tol */
  };
  Test(test_config);
}

#ifdef TEST_DOUBLEDOUBLE
TEST_CASE("qship12l DoubleDouble", "[qship12l DoubleDouble]") {
  typedef mantis::DoubleMantissa<double> Real;
  const TestConfig<Real> test_config{
      kMarosMeszaros + std::string("qship12l.sif"), /* filename */
      false,                                        /* verbose */
      false,                     /* tighten_infinite_bounds */
      false,                     /* print_data */
      Real(3.018876577180347e6), /* objective */
      Real(1e-14),               /* relative_objective_tol */
  };
  Test(test_config);
}
#endif  // ifdef TEST_DOUBLEDOUBLE

// The listing on http://www.doc.ic.ac.uk/%7Eim/00README.QP is:
//     3.0569623e+06.

TEST_CASE("qship12s double", "[qship12s double]") {
  typedef double Real;
  const TestConfig<Real> test_config{
      kMarosMeszaros + std::string("qship12s.sif"), /* filename */
      false,                                        /* verbose */
      false,                     /* tighten_infinite_bounds */
      false,                     /* print_data */
      Real(3.056962249278296e6), /* objective */
      Real(1e-7),                /* relative_objective_tol */
  };
  Test(test_config);
}

#ifdef TEST_DOUBLEDOUBLE
TEST_CASE("qship12s DoubleDouble", "[qship12s DoubleDouble]") {
  typedef mantis::DoubleMantissa<double> Real;
  const TestConfig<Real> test_config{
      kMarosMeszaros + std::string("qship12s.sif"), /* filename */
      false,                                        /* verbose */
      false,                     /* tighten_infinite_bounds */
      false,                     /* print_data */
      Real(3.056962249278296e6), /* objective */
      Real(1e-14),               /* relative_objective_tol */
  };
  Test(test_config);
}
#endif  // ifdef TEST_DOUBLEDOUBLE

// The listing on http://www.doc.ic.ac.uk/%7Eim/00README.QP is:
//     2.3750458e+07.

TEST_CASE("qsierra double", "[qsierra double]") {
  typedef double Real;
  const TestConfig<Real> test_config{
      kMarosMeszaros + std::string("qsierra.sif"), /* filename */
      false,                                       /* verbose */
      false,                                       /* tighten_infinite_bounds */
      false,                                       /* print_data */
      Real(2.375045817874551e7),                   /* objective */
      Real(1e-7),                                  /* relative_objective_tol */
  };
  Test(test_config);
}

#ifdef TEST_DOUBLEDOUBLE
TEST_CASE("qsierra DoubleDouble", "[qsierra DoubleDouble]") {
  typedef mantis::DoubleMantissa<double> Real;
  const TestConfig<Real> test_config{
      kMarosMeszaros + std::string("qsierra.sif"), /* filename */
      false,                                       /* verbose */
      false,                                       /* tighten_infinite_bounds */
      false,                                       /* print_data */
      Real(2.375045817874551e7),                   /* objective */
      Real(1e-14),                                 /* relative_objective_tol */
  };
  Test(test_config);
}
#endif  // ifdef TEST_DOUBLEDOUBLE

// The listing on http://www.doc.ic.ac.uk/%7Eim/00README.QP is:
//     7.9854528e+06.

TEST_CASE("qstair double", "[qstair double]") {
  typedef double Real;
  const TestConfig<Real> test_config{
      kMarosMeszaros + std::string("qstair.sif"), /* filename */
      false,                                      /* verbose */
      false,                                      /* tighten_infinite_bounds */
      false,                                      /* print_data */
      Real(7.985452756288456e6),                  /* objective */
      Real(1e-7),                                 /* relative_objective_tol */
  };
  Test(test_config);
}

#ifdef TEST_DOUBLEDOUBLE
TEST_CASE("qstair DoubleDouble", "[qstair DoubleDouble]") {
  typedef mantis::DoubleMantissa<double> Real;
  const TestConfig<Real> test_config{
      kMarosMeszaros + std::string("qstair.sif"), /* filename */
      false,                                      /* verbose */
      false,                                      /* tighten_infinite_bounds */
      false,                                      /* print_data */
      Real(7.985452756288456e6),                  /* objective */
      Real(1e-14),                                /* relative_objective_tol */
  };
  Test(test_config);
}
#endif  // ifdef TEST_DOUBLEDOUBLE

// The listing on http://www.doc.ic.ac.uk/%7Eim/00README.QP is:
//     6.4118384e+03.

TEST_CASE("qstandat double", "[qstandat double]") {
  typedef double Real;
  const TestConfig<Real> test_config{
      kMarosMeszaros + std::string("qstandat.sif"), /* filename */
      false,                                        /* verbose */
      false,                     /* tighten_infinite_bounds */
      false,                     /* print_data */
      Real(6.411838388888889e3), /* objective */
      Real(1e-7),                /* relative_objective_tol */
  };
  Test(test_config);
}

#ifdef TEST_DOUBLEDOUBLE
TEST_CASE("qstandat DoubleDouble", "[qstandat DoubleDouble]") {
  typedef mantis::DoubleMantissa<double> Real;
  const TestConfig<Real> test_config{
      kMarosMeszaros + std::string("qstandat.sif"), /* filename */
      false,                                        /* verbose */
      false,                     /* tighten_infinite_bounds */
      false,                     /* print_data */
      Real(6.411838388888889e3), /* objective */
      Real(1e-14),               /* relative_objective_tol */
  };
  Test(test_config);
}
#endif  // ifdef TEST_DOUBLEDOUBLE

// The listing on http://www.doc.ic.ac.uk/%7Eim/00README.QP is:
//     5.7310705e-07.
// NOTE: It seems that the true solution is zero.

TEST_CASE("s268 double", "[s268 double]") {
  typedef double Real;
  const TestConfig<Real> test_config{
      kMarosMeszaros + std::string("s268.sif"), /* filename */
      false,                                    /* verbose */
      false,                                    /* tighten_infinite_bounds */
      false,                                    /* print_data */
      Real(1.617390099022355e-28),              /* objective */
      Real(1e-7),                               /* relative_objective_tol */
  };
  Test(test_config);
}

#ifdef TEST_DOUBLEDOUBLE
TEST_CASE("s268 DoubleDouble", "[s268 DoubleDouble]") {
  typedef mantis::DoubleMantissa<double> Real;
  const TestConfig<Real> test_config{
      kMarosMeszaros + std::string("s268.sif"), /* filename */
      false,                                    /* verbose */
      false,                                    /* tighten_infinite_bounds */
      false,                                    /* print_data */
      Real(1.617390099022355e-28),              /* objective */
      Real(1e-14),                              /* relative_objective_tol */
  };
  Test(test_config);
}
#endif  // ifdef TEST_DOUBLEDOUBLE

// The listing on http://www.doc.ic.ac.uk/%7Eim/00README.QP is:
//     -2.8526864e+07.

TEST_CASE("stadat1 double", "[stadat1 double]") {
  typedef double Real;
  const TestConfig<Real> test_config{
      kMarosMeszaros + std::string("stadat1.sif"), /* filename */
      false,                                       /* verbose */
      false,                                       /* tighten_infinite_bounds */
      false,                                       /* print_data */
      Real(-2.852686404498493e7),                  /* objective */
      Real(1e-7),                                  /* relative_objective_tol */
  };
  Test(test_config);
}

#ifdef TEST_DOUBLEDOUBLE
TEST_CASE("stadat1 DoubleDouble", "[stadat1 DoubleDouble]") {
  typedef mantis::DoubleMantissa<double> Real;
  const TestConfig<Real> test_config{
      kMarosMeszaros + std::string("stadat1.sif"), /* filename */
      false,                                       /* verbose */
      false,                                       /* tighten_infinite_bounds */
      false,                                       /* print_data */
      Real(-2.852686404498493e7),                  /* objective */
      Real(1e-14),                                 /* relative_objective_tol */
  };
  Test(test_config);
}
#endif  // ifdef TEST_DOUBLEDOUBLE

// The listing on http://www.doc.ic.ac.uk/%7Eim/00README.QP is:
//     -3.2626665e+01.

TEST_CASE("stadat2 double", "[stadat2 double]") {
  typedef double Real;
  const TestConfig<Real> test_config{
      kMarosMeszaros + std::string("stadat2.sif"), /* filename */
      false,                                       /* verbose */
      false,                                       /* tighten_infinite_bounds */
      false,                                       /* print_data */
      Real(-3.262666487260224e1),                  /* objective */
      Real(1e-7),                                  /* relative_objective_tol */
  };
  Test(test_config);
}

#ifdef TEST_DOUBLEDOUBLE
TEST_CASE("stadat2 DoubleDouble", "[stadat2 DoubleDouble]") {
  typedef mantis::DoubleMantissa<double> Real;
  const TestConfig<Real> test_config{
      kMarosMeszaros + std::string("stadat2.sif"), /* filename */
      false,                                       /* verbose */
      false,                                       /* tighten_infinite_bounds */
      false,                                       /* print_data */
      Real(-3.262666487260224e1),                  /* objective */
      Real(1e-14),                                 /* relative_objective_tol */
  };
  Test(test_config);
}
#endif  // ifdef TEST_DOUBLEDOUBLE

// The listing on http://www.doc.ic.ac.uk/%7Eim/00README.QP is:
//     -3.5779453e+01.

TEST_CASE("stadat3 double", "[stadat3 double]") {
  typedef double Real;
  const TestConfig<Real> test_config{
      kMarosMeszaros + std::string("stadat3.sif"), /* filename */
      false,                                       /* verbose */
      false,                                       /* tighten_infinite_bounds */
      false,                                       /* print_data */
      Real(-3.577945295251064e1),                  /* objective */
      Real(1e-7),                                  /* relative_objective_tol */
  };
  Test(test_config);
}

#ifdef TEST_DOUBLEDOUBLE
TEST_CASE("stadat3 DoubleDouble", "[stadat3 DoubleDouble]") {
  typedef mantis::DoubleMantissa<double> Real;
  const TestConfig<Real> test_config{
      kMarosMeszaros + std::string("stadat3.sif"), /* filename */
      false,                                       /* verbose */
      false,                                       /* tighten_infinite_bounds */
      false,                                       /* print_data */
      Real(-3.577945295251064e1),                  /* objective */
      Real(1e-14),                                 /* relative_objective_tol */
  };
  Test(test_config);
}
#endif  // ifdef TEST_DOUBLEDOUBLE

// The listing on http://www.doc.ic.ac.uk/%7Eim/00README.QP is:
//     1.5514356e+05.

TEST_CASE("stcqp1 double", "[stcqp1 double]") {
  typedef double Real;
  const TestConfig<Real> test_config{
      kMarosMeszaros + std::string("stcqp1.sif"), /* filename */
      false,                                      /* verbose */
      false,                                      /* tighten_infinite_bounds */
      false,                                      /* print_data */
      Real(1.551435547039571e5),                  /* objective */
      Real(1e-7),                                 /* relative_objective_tol */
  };
  Test(test_config);
}

#ifdef TEST_DOUBLEDOUBLE
TEST_CASE("stcqp1 DoubleDouble", "[stcqp1 DoubleDouble]") {
  typedef mantis::DoubleMantissa<double> Real;
  const TestConfig<Real> test_config{
      kMarosMeszaros + std::string("stcqp1.sif"), /* filename */
      false,                                      /* verbose */
      false,                                      /* tighten_infinite_bounds */
      false,                                      /* print_data */
      Real(1.551435547039571e5),                  /* objective */
      Real(1e-14),                                /* relative_objective_tol */
  };
  Test(test_config);
}
#endif  // ifdef TEST_DOUBLEDOUBLE

// The listing on http://www.doc.ic.ac.uk/%7Eim/00README.QP is:
//     2.2327313e+04.

TEST_CASE("stcqp2 double", "[stcqp2 double]") {
  typedef double Real;
  const TestConfig<Real> test_config{
      kMarosMeszaros + std::string("stcqp2.sif"), /* filename */
      false,                                      /* verbose */
      false,                                      /* tighten_infinite_bounds */
      false,                                      /* print_data */
      Real(2.232731327241668e4),                  /* objective */
      Real(1e-7),                                 /* relative_objective_tol */
  };
  Test(test_config);
}

#ifdef TEST_DOUBLEDOUBLE
TEST_CASE("stcqp2 DoubleDouble", "[stcqp2 DoubleDouble]") {
  typedef mantis::DoubleMantissa<double> Real;
  const TestConfig<Real> test_config{
      kMarosMeszaros + std::string("stcqp2.sif"), /* filename */
      false,                                      /* verbose */
      false,                                      /* tighten_infinite_bounds */
      false,                                      /* print_data */
      Real(2.232731327241668e4),                  /* objective */
      Real(1e-14),                                /* relative_objective_tol */
  };
  Test(test_config);
}
#endif  // ifdef TEST_DOUBLEDOUBLE

// The listing on http://www.doc.ic.ac.uk/%7Eim/00README.QP is:
//     0.0000000e+00.

TEST_CASE("tame double", "[tame double]") {
  typedef double Real;
  const TestConfig<Real> test_config{
      kMarosMeszaros + std::string("tame.sif"), /* filename */
      false,                                    /* verbose */
      false,                                    /* tighten_infinite_bounds */
      false,                                    /* print_data */
      Real(0.0000000e+00),                      /* objective */
      Real(1e-7),                               /* relative_objective_tol */
  };
  Test(test_config);
}

#ifdef TEST_DOUBLEDOUBLE
TEST_CASE("tame DoubleDouble", "[tame DoubleDouble]") {
  typedef mantis::DoubleMantissa<double> Real;
  const TestConfig<Real> test_config{
      kMarosMeszaros + std::string("tame.sif"), /* filename */
      false,                                    /* verbose */
      false,                                    /* tighten_infinite_bounds */
      false,                                    /* print_data */
      Real(0.0000000e+00),                      /* objective */
      Real(1e-14),                              /* relative_objective_tol */
  };
  Test(test_config);
}
#endif  // ifdef TEST_DOUBLEDOUBLE

// The listing on http://www.doc.ic.ac.uk/%7Eim/00README.QP is:
//     1.1160008e+00.

TEST_CASE("ubh1 double", "[ubh1 double]") {
  typedef double Real;
  const TestConfig<Real> test_config{
      kMarosMeszaros + std::string("ubh1.sif"), /* filename */
      false,                                    /* verbose */
      false,                                    /* tighten_infinite_bounds */
      false,                                    /* print_data */
      Real(1.116000815694815),                  /* objective */
      Real(1e-7),                               /* relative_objective_tol */
  };
  Test(test_config);
}

#ifdef TEST_DOUBLEDOUBLE
TEST_CASE("ubh1 DoubleDouble", "[ubh1 DoubleDouble]") {
  typedef mantis::DoubleMantissa<double> Real;
  const TestConfig<Real> test_config{
      kMarosMeszaros + std::string("ubh1.sif"), /* filename */
      false,                                    /* verbose */
      false,                                    /* tighten_infinite_bounds */
      false,                                    /* print_data */
      Real(1.116000815694815),                  /* objective */
      Real(1e-14),                              /* relative_objective_tol */
  };
  Test(test_config);
}
#endif  // ifdef TEST_DOUBLEDOUBLE

// The listing on http://www.doc.ic.ac.uk/%7Eim/00README.QP is:
//     -1.3966211e+00.

TEST_CASE("values double", "[values double]") {
  typedef double Real;
  const TestConfig<Real> test_config{
      kMarosMeszaros + std::string("values.sif"), /* filename */
      false,                                      /* verbose */
      false,                                      /* tighten_infinite_bounds */
      false,                                      /* print_data */
      Real(-1.396621144714058),                   /* objective */
      Real(1e-7),                                 /* relative_objective_tol */
  };
  Test(test_config);
}

#ifdef TEST_DOUBLEDOUBLE
TEST_CASE("values DoubleDouble", "[values DoubleDouble]") {
  typedef mantis::DoubleMantissa<double> Real;
  const TestConfig<Real> test_config{
      kMarosMeszaros + std::string("values.sif"), /* filename */
      false,                                      /* verbose */
      false,                                      /* tighten_infinite_bounds */
      false,                                      /* print_data */
      Real(-1.396621144714058),                   /* objective */
      Real(1e-14),                                /* relative_objective_tol */
  };
  Test(test_config);
}
#endif  // ifdef TEST_DOUBLEDOUBLE

// The listing on http://www.doc.ic.ac.uk/%7Eim/00README.QP is:
//     1.9770426e+02.

TEST_CASE("yao double", "[yao double]") {
  typedef double Real;
  const TestConfig<Real> test_config{
      kMarosMeszaros + std::string("yao.sif"), /* filename */
      false,                                   /* verbose */
      false,                                   /* tighten_infinite_bounds */
      false,                                   /* print_data */
      Real(1.977042559403128e2),               /* objective */
      Real(1e-7),                              /* relative_objective_tol */
  };
  Test(test_config);
}

#ifdef TEST_DOUBLEDOUBLE
TEST_CASE("yao DoubleDouble", "[yao DoubleDouble]") {
  typedef mantis::DoubleMantissa<double> Real;
  const TestConfig<Real> test_config{
      kMarosMeszaros + std::string("yao.sif"), /* filename */
      false,                                   /* verbose */
      false,                                   /* tighten_infinite_bounds */
      false,                                   /* print_data */
      Real(1.977042559403128e2),               /* objective */
      Real(1e-14),                             /* relative_objective_tol */
  };
  Test(test_config);
}
#endif  // ifdef TEST_DOUBLEDOUBLE

// The listing on http://www.doc.ic.ac.uk/%7Eim/00README.QP is:
//     -4.1250000e+00.

TEST_CASE("zecevic2 double", "[zecevic2 double]") {
  typedef double Real;
  const TestConfig<Real> test_config{
      kMarosMeszaros + std::string("zecevic2.sif"), /* filename */
      false,                                        /* verbose */
      false,                    /* tighten_infinite_bounds */
      false,                    /* print_data */
      Real(-4.125000000000000), /* objective */
      Real(1e-7),               /* relative_objective_tol */
  };
  Test(test_config);
}

#ifdef TEST_DOUBLEDOUBLE
TEST_CASE("zecevic2 DoubleDouble", "[zecevic2 DoubleDouble]") {
  typedef mantis::DoubleMantissa<double> Real;
  const TestConfig<Real> test_config{
      kMarosMeszaros + std::string("zecevic2.sif"), /* filename */
      false,                                        /* verbose */
      false,                    /* tighten_infinite_bounds */
      false,                    /* print_data */
      Real(-4.125000000000000), /* objective */
      Real(1e-14),              /* relative_objective_tol */
  };
  Test(test_config);
}
#endif  // ifdef TEST_DOUBLEDOUBLE
