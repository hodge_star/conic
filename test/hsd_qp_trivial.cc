/*
 * Copyright (c) 2019 Jack Poulson <jack@hodgestar.com>
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
#define CATCH_CONFIG_MAIN
#include <iostream>
#include <limits>
#include "catch2/catch.hpp"
#include "conic.hpp"
#include "quotient/timer.hpp"

// If defined, the tests are also run in double-double arithmetic.
#define TEST_DOUBLEDOUBLE

// We test the primal problem
//
//   min_x { 1' x + (1 / 2) x' 1 x : x >= 0 }.
//
// The dual is then:
//
//   max_{x, y, z} { b' y - (1 / 2) x' Q x : A' y + z = Q x + c, z >= 0 },
//
// which simplifies to
//
//   max_{x, z} { -(1 / 2) x' x : z = 1 + x, x, z >= 0 }.
//
// The target objective should be zero.
//
template <typename Real>
void TrivialFeasible(bool verbose, bool print_data,
                     Real relative_obj_tolerance) {
  conic::hsd_qp::Problem<Real> problem;
  problem.equality_matrix.Resize(0, 1);
  problem.equality_rhs.Resize(0, 1);
  problem.linear_objective.Resize(1, 1);
  problem.linear_objective(0) = Real(1);
  problem.quadratic_objective.Resize(1, 1);
  problem.quadratic_objective.AddEntry(0, 0, Real(1));
  problem.FillNorms();
  if (print_data) {
    std::cout << problem << std::endl;
  }

  const Real target_objective = Real(0);

  conic::hsd_qp::SolveControl<Real> control;
  control.output.verbose = verbose;
  static const Real kEpsilon = std::numeric_limits<Real>::epsilon();
  const Real relative_error_tol =
      control.optimality.relative_tolerance_coefficient *
      std::pow(kEpsilon, control.optimality.relative_tolerance_exponent);

  conic::hsd_qp::Iterate<Real> certificate;
  const conic::Int num_iterations =
      conic::hsd_qp::Solve(problem, control, &certificate);
  if (print_data) {
    std::cout << certificate << std::endl;
  }

  // Measure the primal residual norm.
  conic::hsd_qp::Residuals<Real> state;
  state.FillRelativeError(problem, certificate);
  std::cout << "Ran for " << num_iterations << " iterations w/ relative error "
            << state.relative_error << std::endl;
  if (verbose) {
    certificate.PrintNorms(std::cout);
    state.PrintRelativeError(std::cout);
  }
  REQUIRE(certificate.certificate_type == conic::OPTIMALITY_CERTIFICATE);
  REQUIRE(state.relative_error <= relative_error_tol);

  std::cout << "primal: " << state.objective.primal << std::endl;
  const Real relative_obj_error =
      std::abs(state.objective.primal - target_objective) /
      (std::abs(target_objective) + 1);
  REQUIRE(relative_obj_error <= relative_obj_tolerance);
}

// We test
//
//   min_x { 1' x + (1 / 2) x' 1 x : x = -1, x >= 0 }.
//
// The dual is then:
//
//   max_{x, y, z} { b' y - (1 / 2) x' 1 x : A' y + z = x + c, z >= 0 },
//
// which simplifies to
//
//   max_{x, y, z} { -y - (1 / 2) x' x : y + z = x + 1, z >= 0 }.
//
// The dual solution is then unbounded, as maximizing -y involves minimizing
// y. The constraint y + z = 1 can be satisfied with y = -phi, z = phi + 1,
// for any phi >= -1. Thus, the dual solution will produce unbounded |y| and
// |z|.
//
template <typename Real>
void TrivialPrimalInfeasible(bool verbose, bool print_data) {
  conic::hsd_qp::Problem<Real> problem;
  problem.equality_matrix.Resize(1, 1);
  problem.equality_matrix.AddEntry(0, 0, Real(1));
  problem.equality_rhs.Resize(1, 1);
  problem.equality_rhs(0) = Real(-1);
  problem.linear_objective.Resize(1, 1);
  problem.linear_objective(0) = Real(1);
  problem.quadratic_objective.Resize(1, 1);
  problem.quadratic_objective.AddEntry(0, 0, Real(1));
  problem.FillNorms();
  if (print_data) {
    std::cout << problem << std::endl;
  }

  conic::hsd_qp::SolveControl<Real> control;
  control.output.verbose = verbose;
  conic::hsd_qp::Iterate<Real> certificate;
  const conic::Int num_iterations =
      conic::hsd_qp::Solve(problem, control, &certificate);
  if (print_data) {
    std::cout << certificate << std::endl;
  }

  std::cout << "Ran for " << num_iterations << " iterations" << std::endl;
  if (verbose) {
    certificate.PrintNorms(std::cout);
  }
  REQUIRE(certificate.certificate_type ==
          conic::PRIMAL_INFEASIBILITY_CERTIFICATE);
}

// We test
//
//   min_x { | -1 |' | x_1 | + (1 / 2) | x_1 | | 1 0 | | x_1 | : x >= 0 },
//           | -1 |  | x_2 |           | x_2 | | 0 0 | | x_2 |
//
// which has dual
//
//   max_{x, y, z} { b' y - (1 / 2) x' Q x : A' y + z = Q x + c, z >= 0 },
//
// which simplifies to
//
//   max_{x, y} { -(1 / 2) x_1^2 : | x_1 | >= | 1 |, x >= 0 }.
//                                 |  0  |    | 1 |
//
// The primal solution is unbounded and the dual is infeasible.
//
template <typename Real>
void TrivialDualInfeasible(bool verbose, bool print_data) {
  conic::hsd_qp::Problem<Real> problem;
  problem.equality_matrix.Resize(0, 2);
  problem.equality_rhs.Resize(0, 1);
  problem.linear_objective.Resize(2, 1);
  problem.linear_objective(0) = Real(-1);
  problem.linear_objective(1) = Real(-1);
  problem.quadratic_objective.Resize(2, 1);
  problem.quadratic_objective.AddEntry(0, 0, Real(1));
  problem.FillNorms();
  if (print_data) {
    std::cout << problem << std::endl;
  }

  conic::hsd_qp::SolveControl<Real> control;
  control.output.verbose = verbose;
  conic::hsd_qp::Iterate<Real> certificate;
  const conic::Int num_iterations =
      conic::hsd_qp::Solve(problem, control, &certificate);
  if (print_data) {
    std::cout << certificate << std::endl;
  }

  std::cout << "Ran for " << num_iterations << " iterations" << std::endl;
  if (verbose) {
    certificate.PrintNorms(std::cout);
  }
  REQUIRE(certificate.certificate_type ==
          conic::DUAL_INFEASIBILITY_CERTIFICATE);
}

// We test
//
//   min_x { | -1 |' | x_1 | + (1 / 2) | x_1 | | 1 0 | | x_1 | : x >= 0 },
//           | -1 |  | x_2 |           | x_2 | | 0 1 | | x_2 |
//
// which has dual
//
//   max_{x, y, z} { b' y - (1 / 2) x' Q x : A' y + z = Q x + c, z >= 0 },
//
// which simplifies to
//
//   max_{x, y} { -(1 / 2) x' x : | x_1 | >= | 1 |, x >= 0 }.
//                                | x_2 |    | 1 |
//
// The dual objective value is clearly from x = 1, leading to a value of -1.
// Notice that this problem is a small modification of the trivial dual
// infeasible problem.
//
template <typename Real>
void TrivialBalanced(bool verbose, bool print_data,
                     Real relative_obj_tolerance) {
  conic::hsd_qp::Problem<Real> problem;
  problem.equality_matrix.Resize(0, 2);
  problem.equality_rhs.Resize(0, 1);
  problem.linear_objective.Resize(2, 1);
  problem.linear_objective(0) = Real(-1);
  problem.linear_objective(1) = Real(-1);
  problem.quadratic_objective.Resize(2, 1);
  problem.quadratic_objective.AddEntry(0, 0, Real(1));
  problem.quadratic_objective.AddEntry(1, 1, Real(1));
  problem.FillNorms();
  if (print_data) {
    std::cout << problem << std::endl;
  }

  const Real target_objective = Real(-1);

  conic::hsd_qp::SolveControl<Real> control;
  control.output.verbose = verbose;
  static const Real kEpsilon = std::numeric_limits<Real>::epsilon();
  const Real relative_error_tol =
      control.optimality.relative_tolerance_coefficient *
      std::pow(kEpsilon, control.optimality.relative_tolerance_exponent);

  conic::hsd_qp::Iterate<Real> certificate;
  const conic::Int num_iterations =
      conic::hsd_qp::Solve(problem, control, &certificate);
  if (print_data) {
    std::cout << certificate << std::endl;
  }

  // Measure the primal residual norm.
  conic::hsd_qp::Residuals<Real> state;
  state.FillRelativeError(problem, certificate);
  std::cout << "Ran for " << num_iterations << " iterations w/ relative error "
            << state.relative_error << std::endl;
  if (verbose) {
    certificate.PrintNorms(std::cout);
    state.PrintRelativeError(std::cout);
  }
  REQUIRE(certificate.certificate_type == conic::OPTIMALITY_CERTIFICATE);
  REQUIRE(state.relative_error <= relative_error_tol);

  std::cout << "primal: " << state.objective.primal << std::endl;
  const Real relative_obj_error =
      std::abs(state.objective.primal - target_objective) /
      (std::abs(target_objective) + 1);
  REQUIRE(relative_obj_error <= relative_obj_tolerance);
}

TEST_CASE("TrivialFeasible float", "[TrivialFeasible float]") {
  const bool kVerbose = false;
  const bool kPrintData = false;
  const float relative_obj_tolerance = 1e-3;
  TrivialFeasible<float>(kVerbose, kPrintData, relative_obj_tolerance);
}

TEST_CASE("TrivialFeasible double", "[TrivialFeasible double]") {
  const bool kVerbose = false;
  const bool kPrintData = false;
  const double relative_obj_tolerance = 1e-6;
  TrivialFeasible<double>(kVerbose, kPrintData, relative_obj_tolerance);
}

#ifdef TEST_DOUBLEDOUBLE
TEST_CASE("TrivialFeasible DoubleDouble", "[TrivialFeasible DoubleDouble]") {
  typedef mantis::DoubleMantissa<double> DoubleDouble;
  const bool kVerbose = false;
  const bool kPrintData = false;
  const DoubleDouble relative_obj_tolerance = 1e-13;
  TrivialFeasible<DoubleDouble>(kVerbose, kPrintData, relative_obj_tolerance);
}
#endif  // ifdef TEST_DOUBLEDOUBLE

TEST_CASE("TrivialPrimalIneasible float", "[TrivialPrimalInfeasible float]") {
  const bool kVerbose = false;
  const bool kPrintData = false;
  TrivialPrimalInfeasible<float>(kVerbose, kPrintData);
}

TEST_CASE("TrivialPrimalInfeasible double",
          "[TrivialPrimalInfeasible double]") {
  const bool kVerbose = false;
  const bool kPrintData = false;
  TrivialPrimalInfeasible<double>(kVerbose, kPrintData);
}

#ifdef TEST_DOUBLEDOUBLE
TEST_CASE("TrivialPrimalInfeasible DoubleDouble",
          "[TrivialPrimalInfeasible DoubleDouble]") {
  typedef mantis::DoubleMantissa<double> DoubleDouble;
  const bool kVerbose = true;
  const bool kPrintData = false;
  TrivialPrimalInfeasible<DoubleDouble>(kVerbose, kPrintData);
}
#endif  // ifdef TEST_DOUBLEDOUBLE

TEST_CASE("TrivialDualIneasible float", "[TrivialDualInfeasible float]") {
  const bool kVerbose = false;
  const bool kPrintData = false;
  TrivialDualInfeasible<float>(kVerbose, kPrintData);
}

TEST_CASE("TrivialDualInfeasible double", "[TrivialDualInfeasible double]") {
  const bool kVerbose = false;
  const bool kPrintData = false;
  TrivialDualInfeasible<double>(kVerbose, kPrintData);
}

#ifdef TEST_DOUBLEDOUBLE
TEST_CASE("TrivialDualInfeasible DoubleDouble",
          "[TrivialDualInfeasible DoubleDouble]") {
  typedef mantis::DoubleMantissa<double> DoubleDouble;
  const bool kVerbose = true;
  const bool kPrintData = false;
  TrivialDualInfeasible<DoubleDouble>(kVerbose, kPrintData);
}
#endif  // ifdef TEST_DOUBLEDOUBLE

TEST_CASE("TrivialBalanced float", "[TrivialBalanced float]") {
  const bool kVerbose = false;
  const bool kPrintData = false;
  const float relative_obj_tolerance = 1e-3;
  TrivialBalanced(kVerbose, kPrintData, relative_obj_tolerance);
}

TEST_CASE("TrivialBalanced double", "[TrivialBalanced double]") {
  const bool kVerbose = false;
  const bool kPrintData = false;
  const double relative_obj_tolerance = 1e-6;
  TrivialBalanced(kVerbose, kPrintData, relative_obj_tolerance);
}

#ifdef TEST_DOUBLEDOUBLE
TEST_CASE("TrivialBalanced DoubleDouble", "[TrivialBalanced DoubleDouble]") {
  typedef mantis::DoubleMantissa<double> DoubleDouble;
  const bool kVerbose = true;
  const bool kPrintData = false;
  const DoubleDouble relative_obj_tolerance = 1e-13;
  TrivialBalanced(kVerbose, kPrintData, relative_obj_tolerance);
}
#endif  // ifdef TEST_DOUBLEDOUBLE
