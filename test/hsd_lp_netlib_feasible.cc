/*
 * Copyright (c) 2019 Jack Poulson <jack@hodgestar.com>
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
#define CATCH_CONFIG_MAIN
#include <iostream>
#include <limits>
#include "catch2/catch.hpp"
#include "conic.hpp"
#include "quotient/timer.hpp"

// These tests ensure that conic's Homogeneous Self-Dual Interior Point Method
// for Quadratic Programs, when restricted to Linear Programs, successfully
// solves each of the problems in the famous "netlib/lp/data" suite at:
//
//   http://www.netlib.org/lp/data/.
//
// The double-precision tests demand a relative accuracy of 1e-8 for the
// optimality certificate, while the manits::DoubleMantissa<double> tests
// demand 1e-14.
//
// The demanded relative accuracy of the objective value itself is a bit ad-hoc,
// as the objective value can be several digits less accurate than the maximum
// of the primal equality feasibility, dual equality feasibility, and relative
// gap.
//

// In double-double arithmetic, we have encountered a primal infeasibility
// certificate with relative error
//
//   || A' y + z ||_2 / |b' y| <=~ 1e-15
//
// for the pilot.ja problem.
//
//#define TEST_PILOTJA_DOUBLEDOUBLE

// In double-double arithmetic, we have encountered a primal infeasibility
// certificate with relative error
//
//   || A' y + z ||_2 / |b' y| <=~ 1e-15
//
// for the pilotnov problem.
//
//#define TEST_PILOTNOV_DOUBLEDOUBLE

using conic::ConstBlasMatrixView;
using conic::EuclideanNorm;
using conic::Int;

// If defined, the tests are also run in double-double arithmetic.
#define TEST_DOUBLEDOUBLE

// The location of the feasible NETLIB LP tests.
static const char kFeasibleNetlib[] = "../data/netlib_lp_feas/";

// Configuration for a netlib LP test.
template <typename Real>
struct TestConfig {
  // The location of the file containing the (uncompressed) MPS file.
  std::string filename;

  // Whether the solver should have 'verbose' level printing.
  bool verbose = false;

  // Whether the presolve should attempt to tighten infinite lower and upper
  // bounds (which introduces additional variables in the standard form).
  bool tighten_infinite_bounds = false;

  // Whether the problem data and solution should be printed.
  bool print_data = false;

  // The target primal objective value.
  Real objective;

  // The acceptable relative objective deviation from the target value.
  Real relative_objective_tol;
};

template <typename Real>
void Solve(const TestConfig<Real>& lp_config,
           const conic::bounded_qp::Problem<Real>& bounded_problem,
           const conic::QuadraticProgramMPSInfo<Real>& bounded_info,
           const conic::BoundedToStandardQPReduction<Real>& reduction,
           const conic::hsd_qp::Problem<Real>& standard_problem,
           bool use_augmented, bool iterate_normalized) {
  conic::hsd_qp::SolveControl<Real> control;

  const std::string linear_form_string =
      use_augmented ? "augmented" : "Schur-complement";
  const std::string normalization_string =
      iterate_normalized ? "iterate" : "RHS";

  std::cout << "Testing " << lp_config.filename << " with "
            << linear_form_string << " system solver and "
            << normalization_string << "-normalization" << std::endl;

  control.linear_solve.use_augmented = use_augmented;
  control.output.verbose = lp_config.verbose;
  control.optimality.iterate_normalization = iterate_normalized;

  static const Real kEpsilon = std::numeric_limits<Real>::epsilon();
  const Real relative_error_tol =
      control.optimality.relative_tolerance_coefficient *
      std::pow(kEpsilon, control.optimality.relative_tolerance_exponent);

  conic::hsd_qp::Iterate<Real> standard_certificate;
  quotient::Timer timer;
  timer.Start();
  const Int num_iterations =
      conic::hsd_qp::Solve(standard_problem, control, &standard_certificate);
  timer.Stop();
  if (lp_config.print_data) {
    std::cout << "Standard certificate:\n" << standard_certificate << std::endl;
  }
  standard_problem.PrintNorms(std::cout);
  standard_certificate.PrintNorms(std::cout);

  conic::hsd_qp::Residuals<Real> standard_residuals;
  standard_residuals.iterate_normalization =
      control.optimality.iterate_normalization;
  standard_residuals.FillRelativeError(standard_problem, standard_certificate);
  std::cout << lp_config.filename << " ran for " << num_iterations
            << " iterations (" << timer.TotalSeconds()
            << " seconds) w/ relative error "
            << standard_residuals.relative_error << std::endl;
  const Real standard_primal_objective =
      standard_residuals.objective.primal + reduction.ObjectiveShift();
  const Real standard_relative_obj_deviation =
      std::abs(standard_primal_objective - lp_config.objective) /
      (std::abs(lp_config.objective) + 1);

  std::cout << "standard_primal_objective: " << standard_primal_objective
            << std::endl;
  std::cout << "standard_relative_obj_deviation: "
            << standard_relative_obj_deviation << std::endl;
  standard_residuals.PrintRelativeError(std::cout);

  // Backtransform the standard-form iterate and compute its primal objective.
  conic::bounded_qp::Iterate<Real> bounded_certificate;
  reduction.BacktransformIterate(standard_certificate, &bounded_certificate);
  if (lp_config.print_data) {
    std::cout << "Bounded certificate:\n" << bounded_certificate << std::endl;
  }
  const catamari::ConstBlasMatrixView<Real> bounded_primal_solution =
      bounded_certificate.PrimalSolution();
  Real bounded_primal_objective = bounded_info.objective_shift;
  const conic::Int num_primal_bounded =
      bounded_problem.equality_matrix.NumColumns();
  for (conic::Int i = 0; i < num_primal_bounded; ++i) {
    bounded_primal_objective +=
        bounded_problem.linear_objective(i) * bounded_primal_solution(i);
  }
  std::cout << "bounded_primal_objective: " << bounded_primal_objective
            << std::endl;
  const Real bounded_relative_obj_deviation =
      std::abs(bounded_primal_objective - lp_config.objective) /
      (std::abs(lp_config.objective) + 1);

  // Manually compute the primal feasibility residual for the bounded cert.
  const Real bounded_equality_rhs_two_norm =
      catamari::EuclideanNorm(bounded_problem.equality_rhs.ConstView());
  catamari::BlasMatrix<Real> bounded_residual = bounded_problem.equality_rhs;
  catamari::ApplySparse(Real(-1), bounded_problem.equality_matrix,
                        bounded_primal_solution, Real(1),
                        &bounded_residual.view);
  const Real bounded_primal_equality_residual_norm =
      catamari::EuclideanNorm(bounded_residual.ConstView());
  const Real bounded_primal_equality_relative_residual =
      bounded_primal_equality_residual_norm /
      (bounded_equality_rhs_two_norm + 1);
  std::cout << "bounded || b ||_2: " << bounded_equality_rhs_two_norm << "\n"
            << "bounded || b - A x ||_2 / (|| b ||_2 + 1): "
            << bounded_primal_equality_relative_residual << std::endl;

  // Print any entries of the bounded problem which are outside of the bounds.
  Real max_distance = 0;
  Real max_relative_distance = 0;
  for (conic::Int i = 0; i < num_primal_bounded; ++i) {
    const Real lower_bound = bounded_problem.lower_bounds[i];
    const Real upper_bound = bounded_problem.upper_bounds[i];
    const Real value = bounded_primal_solution(i);
    if (std::isfinite(lower_bound) && std::isfinite(upper_bound)) {
      if (value < lower_bound || value > upper_bound) {
        const Real distance =
            value < lower_bound ? lower_bound - value : value - upper_bound;
        const Real relative_distance =
            value < lower_bound ? distance / (std::abs(lower_bound) + 1)
                                : distance / (std::abs(upper_bound) + 1);

        std::cout << "bounded primal solution entry " << i << " was "
                  << static_cast<double>(value) << ", which is " << distance
                  << " away from [" << static_cast<double>(lower_bound) << ", "
                  << static_cast<double>(upper_bound) << "]" << std::endl;

        max_distance = std::max(max_distance, distance);
        max_relative_distance =
            std::max(max_relative_distance, relative_distance);
      }
    } else if (std::isfinite(lower_bound)) {
      if (value < lower_bound) {
        const Real distance = lower_bound - value;
        const Real relative_distance = distance / (std::abs(lower_bound) + 1);

        std::cout << "bounded primal solution entry " << i << " was "
                  << static_cast<double>(value) << ", which was less than "
                  << static_cast<double>(lower_bound) << std::endl;

        max_distance = std::max(max_distance, distance);
        max_relative_distance =
            std::max(max_relative_distance, relative_distance);
      }
    } else if (std::isfinite(upper_bound)) {
      if (value > upper_bound) {
        const Real distance = value - upper_bound;
        const Real relative_distance = distance / (std::abs(upper_bound) + 1);

        max_distance = std::max(max_distance, distance);
        std::cout << "bounded primal solution entry " << i << " was "
                  << static_cast<double>(value) << ", which was greater than "
                  << static_cast<double>(upper_bound) << std::endl;

        max_distance = std::max(max_distance, distance);
        max_relative_distance =
            std::max(max_relative_distance, relative_distance);
      }
    }
  }
  if (max_distance > Real(0)) {
    std::cout << "Maximum distance from bound was " << max_distance
              << std::endl;
    std::cout << "Maximum relative distance was " << max_relative_distance
              << std::endl;
  }

  REQUIRE(standard_certificate.certificate_type ==
          conic::OPTIMALITY_CERTIFICATE);
  REQUIRE(standard_residuals.relative_error <= relative_error_tol);
  REQUIRE(standard_relative_obj_deviation <= lp_config.relative_objective_tol);
  REQUIRE(num_iterations < control.max_iterations);
  REQUIRE(bounded_relative_obj_deviation <= lp_config.relative_objective_tol);

  std::cout << std::endl;
}

template <typename Real>
void Test(const TestConfig<Real>& lp_config) {
  conic::ReadQuadraticProgramMPSConfig config;
  config.filename = lp_config.filename;

  conic::bounded_qp::Problem<Real> bounded_problem;
  auto bounded_info = conic::ReadQuadraticProgramMPS(config, &bounded_problem);

  // Attempt to tighten the bounds.
  bounded_problem.TightenBounds(lp_config.tighten_infinite_bounds,
                                lp_config.verbose);

  conic::BoundedToStandardQPReduction<Real> reduction(bounded_problem);
  conic::hsd_qp::Problem<Real> standard_problem;
  reduction.FormStandardProblem(&standard_problem);
  const Real objective_shift = reduction.ObjectiveShift();

  std::cout << "info.bounds_section.num_fixed_bounds: "
            << bounded_info.bounds_section.num_fixed_bounds << "\n"
            << "info.bounds_section.num_upper_bounds: "
            << bounded_info.bounds_section.num_upper_bounds << "\n"
            << "info.bounds_section.num_lower_bounds: "
            << bounded_info.bounds_section.num_lower_bounds << "\n"
            << "info.bounds_section.num_free_bounds: "
            << bounded_info.bounds_section.num_free_bounds << "\n"
            << "info.row_section.num_equality_rows: "
            << bounded_info.row_section.num_equality_rows << "\n"
            << "info.objective_shift: " << bounded_info.objective_shift << "\n"
            << "reduction.ObjectiveShift(): " << objective_shift << std::endl;

  if (lp_config.print_data) {
    std::cout << "Bounded problem:\n" << bounded_problem << std::endl;
    std::cout << "Standard problem:\n" << standard_problem << std::endl;
  }

  Solve(lp_config, bounded_problem, bounded_info, reduction, standard_problem,
        false /* use_augmented */, false /* iterate_normalized */);
  Solve(lp_config, bounded_problem, bounded_info, reduction, standard_problem,
        true /* use_augmented */, false /* iterate_normalized */);
  Solve(lp_config, bounded_problem, bounded_info, reduction, standard_problem,
        false /* use_augmented */, true /* iterate_normalized */);
  Solve(lp_config, bounded_problem, bounded_info, reduction, standard_problem,
        true /* use_augmented */, true /* iterate_normalized */);
}

// The listing on https://netlib.org/lp/data/readme is:
//     5.5018458883E+03.

TEST_CASE("25fv47 double", "[25fv47 double]") {
  typedef double Real;
  const TestConfig<Real> lp_config{
      kFeasibleNetlib + std::string("25fv47.mps"), /* filename */
      false,                                       /* verbose */
      false,                                       /* tighten_infinite_bounds */
      false,                                       /* print_data */
      Real(5.501845888286745e3),                   /* objective */
      Real(1e-7),                                  /* relative_objective_tol */
  };
  Test(lp_config);
}

#ifdef TEST_DOUBLEDOUBLE
TEST_CASE("25fv47 DoubleDouble", "[25fv47 DoubleDouble]") {
  typedef mantis::DoubleMantissa<double> Real;
  const TestConfig<Real> lp_config{
      kFeasibleNetlib + std::string("25fv47.mps"), /* filename */
      false,                                       /* verbose */
      false,                                       /* tighten_infinite_bounds */
      false,                                       /* print_data */
      Real(5.501845888286745e3),                   /* objective */
      Real(1e-14),                                 /* relative_objective_tol */
  };
  Test(lp_config);
}
#endif  // ifdef TEST_DOUBLEDOUBLE

// The listing on https://netlib.org/lp/data/readme is:
//     9.8723216072E+05.

TEST_CASE("80bau3b double", "[80bau3b double]") {
  typedef double Real;
  const TestConfig<Real> lp_config{
      kFeasibleNetlib + std::string("80bau3b.mps"), /* filename */
      false,                                        /* verbose */
      false,                     /* tighten_infinite_bounds */
      false,                     /* print_data */
      Real(9.872241924090902e5), /* objective */
      Real(5e-7),                /* relative_objective_tol */
  };
  Test(lp_config);
}

#ifdef TEST_DOUBLEDOUBLE
TEST_CASE("80bau3b DoubleDouble", "[80bau3b DoubleDouble]") {
  typedef mantis::DoubleMantissa<double> Real;
  const TestConfig<Real> lp_config{
      kFeasibleNetlib + std::string("80bau3b.mps"), /* filename */
      false,                                        /* verbose */
      false,                     /* tighten_infinite_bounds */
      false,                     /* print_data */
      Real(9.872241924090902e5), /* objective */
      Real(1e-14),               /* relative_objective_tol */
  };
  Test(lp_config);
}
#endif  // ifdef TEST_DOUBLEDOUBLE

// The listing on https://netlib.org/lp/data/readme is:
//     2.2549496316E+05.

TEST_CASE("adlittle double", "[adlittle double]") {
  typedef double Real;
  const TestConfig<Real> lp_config{
      kFeasibleNetlib + std::string("adlittle.mps"), /* filename */
      false,                                         /* verbose */
      false,                     /* tighten_infinite_bounds */
      false,                     /* print_data */
      Real(2.254949631623804e5), /* objective */
      Real(1e-7),                /* relative_objective_tol */
  };
  Test(lp_config);
}

#ifdef TEST_DOUBLEDOUBLE
TEST_CASE("adlittle DoubleDouble", "[adlittle DoubleDouble]") {
  typedef mantis::DoubleMantissa<double> Real;
  const TestConfig<Real> lp_config{
      kFeasibleNetlib + std::string("adlittle.mps"), /* filename */
      false,                                         /* verbose */
      false,                     /* tighten_infinite_bounds */
      false,                     /* print_data */
      Real(2.254949631623804e5), /* objective */
      Real(1e-14),               /* relative_objective_tol */
  };
  Test(lp_config);
}
#endif  // ifdef TEST_DOUBLEDOUBLE

// The listing on https://netlib.org/lp/data/readme is:
//     -4.6475314286E+02.

TEST_CASE("afiro double", "[afiro double]") {
  typedef double Real;
  const TestConfig<Real> lp_config{
      kFeasibleNetlib + std::string("afiro.mps"), /* filename */
      false,                                      /* verbose */
      false,                                      /* tighten_infinite_bounds */
      false,                                      /* print_data */
      Real(-4.647531428571429e2),                 /* objective */
      Real(1e-7),                                 /* relative_objective_tol */
  };
  Test(lp_config);
}

#ifdef TEST_DOUBLEDOUBLE
TEST_CASE("afiro DoubleDouble", "[afiro DoubleDouble]") {
  typedef mantis::DoubleMantissa<double> Real;
  const TestConfig<Real> lp_config{
      kFeasibleNetlib + std::string("afiro.mps"), /* filename */
      false,                                      /* verbose */
      false,                                      /* tighten_infinite_bounds */
      false,                                      /* print_data */
      Real(-4.647531428571429e2),                 /* objective */
      Real(1e-14),                                /* relative_objective_tol */
  };
  Test(lp_config);
}
#endif  // ifdef TEST_DOUBLEDOUBLE

// The listing on https://netlib.org/lp/data/readme is:
//     -3.5991767287E+07.

TEST_CASE("agg double", "[agg double]") {
  typedef double Real;
  const TestConfig<Real> lp_config{
      kFeasibleNetlib + std::string("agg.mps"), /* filename */
      false,                                    /* verbose */
      false,                                    /* tighten_infinite_bounds */
      false,                                    /* print_data */
      Real(-3.599176728657650e7),               /* objective */
      Real(1e-7),                               /* relative_objective_tol */
  };
  Test(lp_config);
}

#ifdef TEST_DOUBLEDOUBLE
TEST_CASE("agg DoubleDouble", "[agg DoubleDouble]") {
  typedef mantis::DoubleMantissa<double> Real;
  const TestConfig<Real> lp_config{
      kFeasibleNetlib + std::string("agg.mps"), /* filename */
      false,                                    /* verbose */
      false,                                    /* tighten_infinite_bounds */
      false,                                    /* print_data */
      Real(-3.599176728657650e7),               /* objective */
      Real(1e-14),                              /* relative_objective_tol */
  };
  Test(lp_config);
}
#endif  // ifdef TEST_DOUBLEDOUBLE

// The listing on https://netlib.org/lp/data/readme is:
//     -2.0239252356E+07.

TEST_CASE("agg2 double", "[agg2 double]") {
  typedef double Real;
  const TestConfig<Real> lp_config{
      kFeasibleNetlib + std::string("agg2.mps"), /* filename */
      false,                                     /* verbose */
      false,                                     /* tighten_infinite_bounds */
      false,                                     /* print_data */
      Real(-2.023925235597711e7),                /* objective */
      Real(2e-7),                                /* relative_objective_tol */
  };
  Test(lp_config);
}

#ifdef TEST_DOUBLEDOUBLE
TEST_CASE("agg2 DoubleDouble", "[agg2 DoubleDouble]") {
  typedef mantis::DoubleMantissa<double> Real;
  const TestConfig<Real> lp_config{
      kFeasibleNetlib + std::string("agg2.mps"), /* filename */
      false,                                     /* verbose */
      false,                                     /* tighten_infinite_bounds */
      false,                                     /* print_data */
      Real(-2.023925235597711e7),                /* objective */
      Real(1e-14),                               /* relative_objective_tol */
  };
  Test(lp_config);
}
#endif  // ifdef TEST_DOUBLEDOUBLE

// The listing on https://netlib.org/lp/data/readme is:
//     1.0312115935E+07.

TEST_CASE("agg3 double", "[agg3 double]") {
  typedef double Real;
  const TestConfig<Real> lp_config{
      kFeasibleNetlib + std::string("agg3.mps"), /* filename */
      false,                                     /* verbose */
      false,                                     /* tighten_infinite_bounds */
      false,                                     /* print_data */
      Real(1.031211593508922e7),                 /* objective */
      Real(5e-7),                                /* relative_objective_tol */
  };
  Test(lp_config);
}

#ifdef TEST_DOUBLEDOUBLE
TEST_CASE("agg3 DoubleDouble", "[agg3 DoubleDouble]") {
  typedef mantis::DoubleMantissa<double> Real;
  const TestConfig<Real> lp_config{
      kFeasibleNetlib + std::string("agg3.mps"), /* filename */
      false,                                     /* verbose */
      false,                                     /* tighten_infinite_bounds */
      false,                                     /* print_data */
      Real(1.031211593508922e7),                 /* objective */
      Real(1e-14),                               /* relative_objective_tol */
  };
  Test(lp_config);
}
#endif  // ifdef TEST_DOUBLEDOUBLE

// The listing on https://netlib.org/lp/data/readme is:
//     -1.5862801845E+02.

TEST_CASE("bandm double", "[bandm double]") {
  typedef double Real;
  const TestConfig<Real> lp_config{
      kFeasibleNetlib + std::string("bandm.mps"), /* filename */
      false,                                      /* verbose */
      false,                                      /* tighten_infinite_bounds */
      false,                                      /* print_data */
      Real(-1.586280184501207e2),                 /* objective */
      Real(1e-7),                                 /* relative_objective_tol */
  };
  Test(lp_config);
}

#ifdef TEST_DOUBLEDOUBLE
TEST_CASE("bandm DoubleDouble", "[bandm DoubleDouble]") {
  typedef mantis::DoubleMantissa<double> Real;
  const TestConfig<Real> lp_config{
      kFeasibleNetlib + std::string("bandm.mps"), /* filename */
      false,                                      /* verbose */
      false,                                      /* tighten_infinite_bounds */
      false,                                      /* print_data */
      Real(-1.586280184501207e2),                 /* objective */
      Real(1e-14),                                /* relative_objective_tol */
  };
  Test(lp_config);
}
#endif  // ifdef TEST_DOUBLEDOUBLE

// The listing on https://netlib.org/lp/data/readme is:
//     3.3592485807E+04.

TEST_CASE("beaconfd double", "[beaconfd double]") {
  typedef double Real;
  const TestConfig<Real> lp_config{
      kFeasibleNetlib + std::string("beaconfd.mps"), /* filename */
      false,                                         /* verbose */
      false,                     /* tighten_infinite_bounds */
      false,                     /* print_data */
      Real(3.359248580720000e4), /* objective */
      Real(1e-7),                /* relative_objective_tol */
  };
  Test(lp_config);
}

#ifdef TEST_DOUBLEDOUBLE
TEST_CASE("beaconfd DoubleDouble", "[beaconfd DoubleDouble]") {
  typedef mantis::DoubleMantissa<double> Real;
  const TestConfig<Real> lp_config{
      kFeasibleNetlib + std::string("beaconfd.mps"), /* filename */
      false,                                         /* verbose */
      false,                     /* tighten_infinite_bounds */
      false,                     /* print_data */
      Real(3.359248580720000e4), /* objective */
      Real(1e-14),               /* relative_objective_tol */
  };
  Test(lp_config);
}
#endif  // ifdef TEST_DOUBLEDOUBLE

// The listing on https://netlib.org/lp/data/readme is:
//     -3.0812149846E+01.

TEST_CASE("blend double", "[blend double]") {
  typedef double Real;
  const TestConfig<Real> lp_config{
      kFeasibleNetlib + std::string("blend.mps"), /* filename */
      false,                                      /* verbose */
      false,                                      /* tighten_infinite_bounds */
      false,                                      /* print_data */
      Real(-3.081214984582822e1),                 /* objective */
      Real(1e-7),                                 /* relative_objective_tol */
  };
  Test(lp_config);
}

#ifdef TEST_DOUBLEDOUBLE
TEST_CASE("blend DoubleDouble", "[blend DoubleDouble]") {
  typedef mantis::DoubleMantissa<double> Real;
  const TestConfig<Real> lp_config{
      kFeasibleNetlib + std::string("blend.mps"), /* filename */
      false,                                      /* verbose */
      false,                                      /* tighten_infinite_bounds */
      false,                                      /* print_data */
      Real(-3.081214984582822e1),                 /* objective */
      Real(1e-14),                                /* relative_objective_tol */
  };
  Test(lp_config);
}
#endif  // ifdef TEST_DOUBLEDOUBLE

// The listing on https://netlib.org/lp/data/readme is:
//     1.9776292856E+03.

TEST_CASE("bnl1 double", "[bnl1 double]") {
  typedef double Real;
  const TestConfig<Real> lp_config{
      kFeasibleNetlib + std::string("bnl1.mps"), /* filename */
      false,                                     /* verbose */
      false,                                     /* tighten_infinite_bounds */
      false,                                     /* print_data */
      Real(1.977629561522888e3),                 /* objective */
      Real(5e-7),                                /* relative_objective_tol */
  };
  Test(lp_config);
}

#ifdef TEST_DOUBLEDOUBLE
TEST_CASE("bnl1 DoubleDouble", "[bnl1 DoubleDouble]") {
  typedef mantis::DoubleMantissa<double> Real;
  const TestConfig<Real> lp_config{
      kFeasibleNetlib + std::string("bnl1.mps"), /* filename */
      false,                                     /* verbose */
      false,                                     /* tighten_infinite_bounds */
      false,                                     /* print_data */
      Real(1.977629561522888e3),                 /* objective */
      Real(1e-14),                               /* relative_objective_tol */
  };
  Test(lp_config);
}
#endif  // ifdef TEST_DOUBLEDOUBLE

// The listing on https://netlib.org/lp/data/readme is:
//     1.8112365404E+03.

TEST_CASE("bnl2 double", "[bnl2 double]") {
  typedef double Real;
  const TestConfig<Real> lp_config{
      kFeasibleNetlib + std::string("bnl2.mps"), /* filename */
      false,                                     /* verbose */
      false,                                     /* tighten_infinite_bounds */
      false,                                     /* print_data */
      Real(1.811236540358545e3),                 /* objective */
      Real(1e-7),                                /* relative_objective_tol */
  };
  Test(lp_config);
}

#ifdef TEST_DOUBLEDOUBLE
TEST_CASE("bnl2 DoubleDouble", "[bnl2 DoubleDouble]") {
  typedef mantis::DoubleMantissa<double> Real;
  const TestConfig<Real> lp_config{
      kFeasibleNetlib + std::string("bnl2.mps"), /* filename */
      false,                                     /* verbose */
      false,                                     /* tighten_infinite_bounds */
      false,                                     /* print_data */
      Real(1.811236540358545e3),                 /* objective */
      Real(1e-14),                               /* relative_objective_tol */
  };
  Test(lp_config);
}
#endif  // ifdef TEST_DOUBLEDOUBLE

// The listing on https://netlib.org/lp/data/readme is:
//     -3.3521356751E+02.

TEST_CASE("boeing1 double", "[boeing1 double]") {
  typedef double Real;
  const TestConfig<Real> lp_config{
      kFeasibleNetlib + std::string("boeing1.mps"), /* filename */
      false,                                        /* verbose */
      false,                      /* tighten_infinite_bounds */
      false,                      /* print_data */
      Real(-3.352135675071266e2), /* objective */
      Real(5e-7),                 /* relative_objective_tol */
  };
  Test(lp_config);
}

#ifdef TEST_DOUBLEDOUBLE
TEST_CASE("boeing1 DoubleDouble", "[boeing1 DoubleDouble]") {
  typedef mantis::DoubleMantissa<double> Real;
  const TestConfig<Real> lp_config{
      kFeasibleNetlib + std::string("boeing1.mps"), /* filename */
      false,                                        /* verbose */
      false,                      /* tighten_infinite_bounds */
      false,                      /* print_data */
      Real(-3.352135675071266e2), /* objective */
      Real(1e-14),                /* relative_objective_tol */
  };
  Test(lp_config);
}
#endif  // ifdef TEST_DOUBLEDOUBLE

// The listing on https://netlib.org/lp/data/readme is:
//     -3.1501872802E+02.

TEST_CASE("boeing2 double", "[boeing2 double]") {
  typedef double Real;
  const TestConfig<Real> lp_config{
      kFeasibleNetlib + std::string("boeing2.mps"), /* filename */
      false,                                        /* verbose */
      false,                      /* tighten_infinite_bounds */
      false,                      /* print_data */
      Real(-3.150187280152029e2), /* objective */
      Real(1e-7),                 /* relative_objective_tol */
  };
  Test(lp_config);
}

#ifdef TEST_DOUBLEDOUBLE
TEST_CASE("boeing2 DoubleDouble", "[boeing2 DoubleDouble]") {
  typedef mantis::DoubleMantissa<double> Real;
  const TestConfig<Real> lp_config{
      kFeasibleNetlib + std::string("boeing2.mps"), /* filename */
      false,                                        /* verbose */
      false,                      /* tighten_infinite_bounds */
      false,                      /* print_data */
      Real(-3.150187280152029e2), /* objective */
      Real(1e-14),                /* relative_objective_tol */
  };
  Test(lp_config);
}
#endif  // ifdef TEST_DOUBLEDOUBLE

// The listing on https://netlib.org/lp/data/readme is:
//     1.3730803942E+03.

TEST_CASE("bore3d double", "[bore3d double]") {
  typedef double Real;
  const TestConfig<Real> lp_config{
      kFeasibleNetlib + std::string("bore3d.mps"), /* filename */
      false,                                       /* verbose */
      false,                                       /* tighten_infinite_bounds */
      false,                                       /* print_data */
      Real(1.373080394208493e3),                   /* objective */
      Real(1e-7),                                  /* relative_objective_tol */
  };
  Test(lp_config);
}

#ifdef TEST_DOUBLEDOUBLE
TEST_CASE("bore3d DoubleDouble", "[bore3d DoubleDouble]") {
  typedef mantis::DoubleMantissa<double> Real;
  const TestConfig<Real> lp_config{
      kFeasibleNetlib + std::string("bore3d.mps"), /* filename */
      false,                                       /* verbose */
      false,                                       /* tighten_infinite_bounds */
      false,                                       /* print_data */
      Real(1.373080394208493e3),                   /* objective */
      Real(1e-14),                                 /* relative_objective_tol */
  };
  Test(lp_config);
}
#endif  // ifdef TEST_DOUBLEDOUBLE

// The listing on https://netlib.org/lp/data/readme is:
//     1.5185098965E+03.

TEST_CASE("brandy double", "[brandy double]") {
  typedef double Real;
  const TestConfig<Real> lp_config{
      kFeasibleNetlib + std::string("brandy.mps"), /* filename */
      false,                                       /* verbose */
      false,                                       /* tighten_infinite_bounds */
      false,                                       /* print_data */
      Real(1.518509896488128e3),                   /* objective */
      Real(1e-7),                                  /* relative_objective_tol */
  };
  Test(lp_config);
}

#ifdef TEST_DOUBLEDOUBLE
TEST_CASE("brandy DoubleDouble", "[brandy DoubleDouble]") {
  typedef mantis::DoubleMantissa<double> Real;
  const TestConfig<Real> lp_config{
      kFeasibleNetlib + std::string("brandy.mps"), /* filename */
      false,                                       /* verbose */
      false,                                       /* tighten_infinite_bounds */
      false,                                       /* print_data */
      Real(1.518509896488128e3),                   /* objective */
      Real(1e-14),                                 /* relative_objective_tol */
  };
  Test(lp_config);
}
#endif  // ifdef TEST_DOUBLEDOUBLE

// The listing on https://netlib.org/lp/data/readme is:
//     2.6900129138E+03.

TEST_CASE("capri double", "[capri double]") {
  typedef double Real;
  const TestConfig<Real> lp_config{
      kFeasibleNetlib + std::string("capri.mps"), /* filename */
      false,                                      /* verbose */
      false,                                      /* tighten_infinite_bounds */
      false,                                      /* print_data */
      Real(2.690012913768161e3),                  /* objective */
      Real(5e-7),                                 /* relative_objective_tol */
  };
  Test(lp_config);
}

#ifdef TEST_DOUBLEDOUBLE
TEST_CASE("capri DoubleDouble", "[capri DoubleDouble]") {
  typedef mantis::DoubleMantissa<double> Real;
  const TestConfig<Real> lp_config{
      kFeasibleNetlib + std::string("capri.mps"), /* filename */
      false,                                      /* verbose */
      false,                                      /* tighten_infinite_bounds */
      false,                                      /* print_data */
      Real(2.690012913768161e3),                  /* objective */
      Real(1e-14),                                /* relative_objective_tol */
  };
  Test(lp_config);
}
#endif  // ifdef TEST_DOUBLEDOUBLE

// The listing on https://netlib.org/lp/data/readme is:
//     -5.2263930249E+00.

TEST_CASE("cycle double", "[cycle double]") {
  typedef double Real;
  const TestConfig<Real> lp_config{
      kFeasibleNetlib + std::string("cycle.mps"), /* filename */
      false,                                      /* verbose */
      false,                                      /* tighten_infinite_bounds */
      false,                                      /* print_data */
      Real(-5.226393024894102),                   /* objective */
      Real(1e-5),                                 /* relative_objective_tol */
  };
  Test(lp_config);
}

#ifdef TEST_DOUBLEDOUBLE
TEST_CASE("cycle DoubleDouble", "[cycle DoubleDouble]") {
  typedef mantis::DoubleMantissa<double> Real;
  const TestConfig<Real> lp_config{
      kFeasibleNetlib + std::string("cycle.mps"), /* filename */
      false,                                      /* verbose */
      false,                                      /* tighten_infinite_bounds */
      false,                                      /* print_data */
      Real(-5.226393024894102),                   /* objective */
      Real(1e-14),                                /* relative_objective_tol */
  };
  Test(lp_config);
}
#endif  // ifdef TEST_DOUBLEDOUBLE

// The listing on https://netlib.org/lp/data/readme is:
//     2.1851966989E+06.

TEST_CASE("czprob double", "[czprob double]") {
  typedef double Real;
  const TestConfig<Real> lp_config{
      kFeasibleNetlib + std::string("czprob.mps"), /* filename */
      false,                                       /* verbose */
      false,                                       /* tighten_infinite_bounds */
      false,                                       /* print_data */
      Real(2.185196698856577e6),                   /* objective */
      Real(5e-7),                                  /* relative_objective_tol */
  };
  Test(lp_config);
}

#ifdef TEST_DOUBLEDOUBLE
TEST_CASE("czprob DoubleDouble", "[czprob DoubleDouble]") {
  typedef mantis::DoubleMantissa<double> Real;
  const TestConfig<Real> lp_config{
      kFeasibleNetlib + std::string("czprob.mps"), /* filename */
      false,                                       /* verbose */
      false,                                       /* tighten_infinite_bounds */
      false,                                       /* print_data */
      Real(2.185196698856577e6),                   /* objective */
      Real(1e-14),                                 /* relative_objective_tol */
  };
  Test(lp_config);
}
#endif  // ifdef TEST_DOUBLEDOUBLE

// The listing on https://netlib.org/lp/data/readme is:
//     1.2278423615E+05.

TEST_CASE("d2q06c double", "[d2q06c double]") {
  typedef double Real;
  const TestConfig<Real> lp_config{
      kFeasibleNetlib + std::string("d2q06c.mps"), /* filename */
      false,                                       /* verbose */
      false,                                       /* tighten_infinite_bounds */
      false,                                       /* print_data */
      Real(1.227842108141894e5),                   /* objective */
      Real(5e-7),                                  /* relative_objective_tol */
  };
  Test(lp_config);
}

#ifdef TEST_DOUBLEDOUBLE
TEST_CASE("d2q06c DoubleDouble", "[d2q06c DoubleDouble]") {
  typedef mantis::DoubleMantissa<double> Real;
  const TestConfig<Real> lp_config{
      kFeasibleNetlib + std::string("d2q06c.mps"), /* filename */
      false,                                       /* verbose */
      false,                                       /* tighten_infinite_bounds */
      false,                                       /* print_data */
      Real(1.227842108141894e5),                   /* objective */
      Real(1e-14),                                 /* relative_objective_tol */
  };
  Test(lp_config);
}
#endif  // ifdef TEST_DOUBLEDOUBLE

// The listing on https://netlib.org/lp/data/readme is:
//     3.1549166667E+02.

TEST_CASE("d6cube double", "[d6cube double]") {
  typedef double Real;
  const TestConfig<Real> lp_config{
      kFeasibleNetlib + std::string("d6cube.mps"), /* filename */
      false,                                       /* verbose */
      false,                                       /* tighten_infinite_bounds */
      false,                                       /* print_data */
      Real(3.154916666666667e2),                   /* objective */
      Real(1e-7),                                  /* relative_objective_tol */
  };
  Test(lp_config);
}

#ifdef TEST_DOUBLEDOUBLE
TEST_CASE("d6cube DoubleDouble", "[d6cube DoubleDouble]") {
  typedef mantis::DoubleMantissa<double> Real;
  const TestConfig<Real> lp_config{
      kFeasibleNetlib + std::string("d6cube.mps"), /* filename */
      false,                                       /* verbose */
      false,                                       /* tighten_infinite_bounds */
      false,                                       /* print_data */
      Real(3.154916666666667e2),                   /* objective */
      Real(1e-14),                                 /* relative_objective_tol */
  };
  Test(lp_config);
}
#endif  // ifdef TEST_DOUBLEDOUBLE

// The listing on https://netlib.org/lp/data/readme is:
//     -1.4351780000E+03.

TEST_CASE("degen2 double", "[degen2 double]") {
  typedef double Real;
  const TestConfig<Real> lp_config{
      kFeasibleNetlib + std::string("degen2.mps"), /* filename */
      false,                                       /* verbose */
      false,                                       /* tighten_infinite_bounds */
      false,                                       /* print_data */
      Real(-1.435178000000000e3),                  /* objective */
      Real(1e-7),                                  /* relative_objective_tol */
  };
  Test(lp_config);
}

#ifdef TEST_DOUBLEDOUBLE
TEST_CASE("degen2 DoubleDouble", "[degen2 DoubleDouble]") {
  typedef mantis::DoubleMantissa<double> Real;
  const TestConfig<Real> lp_config{
      kFeasibleNetlib + std::string("degen2.mps"), /* filename */
      false,                                       /* verbose */
      false,                                       /* tighten_infinite_bounds */
      false,                                       /* print_data */
      Real(-1.435178000000000e3),                  /* objective */
      Real(1e-14),                                 /* relative_objective_tol */
  };
  Test(lp_config);
}
#endif  // ifdef TEST_DOUBLEDOUBLE

// The listing on https://netlib.org/lp/data/readme is:
//     -9.8729400000E+02.

TEST_CASE("degen3 double", "[degen3 double]") {
  typedef double Real;
  const TestConfig<Real> lp_config{
      kFeasibleNetlib + std::string("degen3.mps"), /* filename */
      false,                                       /* verbose */
      false,                                       /* tighten_infinite_bounds */
      false,                                       /* print_data */
      Real(-9.872940000000000e2),                  /* objective */
      Real(1e-7),                                  /* relative_objective_tol */
  };
  Test(lp_config);
}

#ifdef TEST_DOUBLEDOUBLE
TEST_CASE("degen3 DoubleDouble", "[degen3 DoubleDouble]") {
  typedef mantis::DoubleMantissa<double> Real;
  const TestConfig<Real> lp_config{
      kFeasibleNetlib + std::string("degen3.mps"), /* filename */
      false,                                       /* verbose */
      false,                                       /* tighten_infinite_bounds */
      false,                                       /* print_data */
      Real(-9.872940000000000e2),                  /* objective */
      Real(1e-14),                                 /* relative_objective_tol */
  };
  Test(lp_config);
}
#endif  // ifdef TEST_DOUBLEDOUBLE

// The listing on https://netlib.org/lp/data/readme is:
//     1.12664E+07.

TEST_CASE("dfl001 double", "[dfl001 double]") {
  typedef double Real;
  const TestConfig<Real> lp_config{
      kFeasibleNetlib + std::string("dfl001.mps"), /* filename */
      false,                                       /* verbose */
      false,                                       /* tighten_infinite_bounds */
      false,                                       /* print_data */
      Real(1.126639604667141e7),                   /* objective */
      Real(5e-4),                                  /* relative_objective_tol */
  };
  Test(lp_config);
}

#ifdef TEST_DOUBLEDOUBLE
TEST_CASE("dfl001 DoubleDouble", "[dfl001 DoubleDouble]") {
  typedef mantis::DoubleMantissa<double> Real;
  const TestConfig<Real> lp_config{
      kFeasibleNetlib + std::string("dfl001.mps"), /* filename */
      false,                                       /* verbose */
      false,                                       /* tighten_infinite_bounds */
      false,                                       /* print_data */
      Real(1.126639604667141e7),                   /* objective */
      Real(1e-11),                                 /* relative_objective_tol */
  };
  Test(lp_config);
}
#endif  // ifdef TEST_DOUBLEDOUBLE

// The listing on https://netlib.org/lp/data/readme is:
//     -1.8751929066E+01,
// but it ignores the RHS objective value of -7.113, which leads to a constant
// shift to the objective of 7.113, leading to a translated NETLIB objective
// value of
//     -1.1638929066E+01.
//

TEST_CASE("e226 double", "[e226 double]") {
  typedef double Real;
  const TestConfig<Real> lp_config{
      kFeasibleNetlib + std::string("e226.mps"), /* filename */
      false,                                     /* verbose */
      false,                                     /* tighten_infinite_bounds */
      false,                                     /* print_data */
      Real(-1.163892906637055e1),                /* objective */
      Real(1e-7),                                /* relative_objective_tol */
  };
  Test(lp_config);
}

#ifdef TEST_DOUBLEDOUBLE
TEST_CASE("e226 DoubleDouble", "[e226 DoubleDouble]") {
  typedef mantis::DoubleMantissa<double> Real;
  const TestConfig<Real> lp_config{
      kFeasibleNetlib + std::string("e226.mps"), /* filename */
      false,                                     /* verbose */
      false,                                     /* tighten_infinite_bounds */
      false,                                     /* print_data */
      Real(-1.163892906637055e1),                /* objective */
      Real(1e-14),                               /* relative_objective_tol */
  };
  Test(lp_config);
}
#endif  // ifdef TEST_DOUBLEDOUBLE

// The listing on https://netlib.org/lp/data/readme is:
//     -7.5571521774E+02.

TEST_CASE("etamacro", "[etamacro]") {
  typedef double Real;
  const TestConfig<Real> lp_config{
      kFeasibleNetlib + std::string("etamacro.mps"), /* filename */
      false,                                         /* verbose */
      false,                      /* tighten_infinite_bounds */
      false,                      /* print_data */
      Real(-7.557152333749134e2), /* objective */
      Real(1e-7),                 /* relative_objective_tol */
  };
  Test(lp_config);
}

#ifdef TEST_DOUBLEDOUBLE
TEST_CASE("etamacro DoubleDouble", "[etamacro DoubleDouble]") {
  typedef mantis::DoubleMantissa<double> Real;
  const TestConfig<Real> lp_config{
      kFeasibleNetlib + std::string("etamacro.mps"), /* filename */
      false,                                         /* verbose */
      false,                      /* tighten_infinite_bounds */
      false,                      /* print_data */
      Real(-7.557152333749134e2), /* objective */
      Real(1e-14),                /* relative_objective_tol */
  };
  Test(lp_config);
}
#endif  // ifdef TEST_DOUBLEDOUBLE

// The listing on https://netlib.org/lp/data/readme is:
//     5.5567961165E+05.

TEST_CASE("fffff800", "[fffff800]") {
  typedef double Real;
  const TestConfig<Real> lp_config{
      kFeasibleNetlib + std::string("fffff800.mps"), /* filename */
      false,                                         /* verbose */
      false,                     /* tighten_infinite_bounds */
      false,                     /* print_data */
      Real(5.556795648174964e5), /* objective */
      Real(1e-7),                /* relative_objective_tol */
  };
  Test(lp_config);
}

#ifdef TEST_DOUBLEDOUBLE
TEST_CASE("fffff800 DoubleDouble", "[fffff800 DoubleDouble]") {
  typedef mantis::DoubleMantissa<double> Real;
  const TestConfig<Real> lp_config{
      kFeasibleNetlib + std::string("fffff800.mps"), /* filename */
      false,                                         /* verbose */
      false,                     /* tighten_infinite_bounds */
      false,                     /* print_data */
      Real(5.556795648174964e5), /* objective */
      Real(1e-14),               /* relative_objective_tol */
  };
  Test(lp_config);
}
#endif  // ifdef TEST_DOUBLEDOUBLE

// The listing on https://netlib.org/lp/data/readme is:
//     1.7279096547E+05.
// This appears to only be accurate to six digits.

TEST_CASE("finnis", "[finnis]") {
  typedef double Real;
  const TestConfig<Real> lp_config{
      kFeasibleNetlib + std::string("finnis.mps"), /* filename */
      false,                                       /* verbose */
      false,                                       /* tighten_infinite_bounds */
      false,                                       /* print_data */
      Real(1.727910655956116e5),                   /* objective */
      Real(5e-5),                                  /* relative_objective_tol */
  };
  Test(lp_config);
}

#ifdef TEST_DOUBLEDOUBLE
TEST_CASE("finnis DoubleDouble", "[finnis DoubleDouble]") {
  typedef mantis::DoubleMantissa<double> Real;
  const TestConfig<Real> lp_config{
      kFeasibleNetlib + std::string("finnis.mps"), /* filename */
      false,                                       /* verbose */
      false,                                       /* tighten_infinite_bounds */
      false,                                       /* print_data */
      Real(1.727910655956116e5),                   /* objective */
      Real(5e-14),                                 /* relative_objective_tol */
  };
  Test(lp_config);
}
#endif  // ifdef TEST_DOUBLEDOUBLE

// The listing on https://netlib.org/lp/data/readme is:
//     -9.1463780924E+03.

TEST_CASE("fit1d", "[fit1d]") {
  typedef double Real;
  const TestConfig<Real> lp_config{
      kFeasibleNetlib + std::string("fit1d.mps"), /* filename */
      false,                                      /* verbose */
      false,                                      /* tighten_infinite_bounds */
      false,                                      /* print_data */
      Real(-9.146378092420927e3),                 /* objective */
      Real(1e-7),                                 /* relative_objective_tol */
  };
  Test(lp_config);
}

#ifdef TEST_DOUBLEDOUBLE
TEST_CASE("fit1d DoubleDouble", "[fit1d DoubleDouble]") {
  typedef mantis::DoubleMantissa<double> Real;
  const TestConfig<Real> lp_config{
      kFeasibleNetlib + std::string("fit1d.mps"), /* filename */
      false,                                      /* verbose */
      false,                                      /* tighten_infinite_bounds */
      false,                                      /* print_data */
      Real(-9.146378092420927e3),                 /* objective */
      Real(1e-14),                                /* relative_objective_tol */
  };
  Test(lp_config);
}
#endif  // ifdef TEST_DOUBLEDOUBLE

// The listing on https://netlib.org/lp/data/readme is:
//     9.1463780924E+03.

TEST_CASE("fit1p", "[fit1p]") {
  typedef double Real;
  const TestConfig<Real> lp_config{
      kFeasibleNetlib + std::string("fit1p.mps"), /* filename */
      false,                                      /* verbose */
      false,                                      /* tighten_infinite_bounds */
      false,                                      /* print_data */
      Real(9.146378092420927e3),                  /* objective */
      Real(1e-7),                                 /* relative_objective_tol */
  };
  Test(lp_config);
}

#ifdef TEST_DOUBLEDOUBLE
TEST_CASE("fit1p DoubleDouble", "[fit1p DoubleDouble]") {
  typedef mantis::DoubleMantissa<double> Real;
  const TestConfig<Real> lp_config{
      kFeasibleNetlib + std::string("fit1p.mps"), /* filename */
      false,                                      /* verbose */
      false,                                      /* tighten_infinite_bounds */
      false,                                      /* print_data */
      Real(9.146378092420927e3),                  /* objective */
      Real(1e-14),                                /* relative_objective_tol */
  };
  Test(lp_config);
}
#endif  // ifdef TEST_DOUBLEDOUBLE

// The listing on https://netlib.org/lp/data/readme is:
//     -6.8464293294E+04.

TEST_CASE("fit2d", "[fit2d]") {
  typedef double Real;
  const TestConfig<Real> lp_config{
      kFeasibleNetlib + std::string("fit2d.mps"), /* filename */
      false,                                      /* verbose */
      false,                                      /* tighten_infinite_bounds */
      false,                                      /* print_data */
      Real(-6.846429329383207e4),                 /* objective */
      Real(1e-7),                                 /* relative_objective_tol */
  };
  Test(lp_config);
}

#ifdef TEST_DOUBLEDOUBLE
TEST_CASE("fit2d DoubleDouble", "[fit2d DoubleDouble]") {
  typedef mantis::DoubleMantissa<double> Real;
  const TestConfig<Real> lp_config{
      kFeasibleNetlib + std::string("fit2d.mps"), /* filename */
      false,                                      /* verbose */
      false,                                      /* tighten_infinite_bounds */
      false,                                      /* print_data */
      Real(-6.846429329383207e4),                 /* objective */
      Real(1e-14),                                /* relative_objective_tol */
  };
  Test(lp_config);
}
#endif  // ifdef TEST_DOUBLEDOUBLE

// The listing on https://netlib.org/lp/data/readme is:
//     6.8464293232E+04.

TEST_CASE("fit2p", "[fit2p]") {
  typedef double Real;
  const TestConfig<Real> lp_config{
      kFeasibleNetlib + std::string("fit2p.mps"), /* filename */
      false,                                      /* verbose */
      false,                                      /* tighten_infinite_bounds */
      false,                                      /* print_data */
      Real(6.846429329383207e4),                  /* objective */
      Real(1e-7),                                 /* relative_objective_tol */
  };
  Test(lp_config);
}

#ifdef TEST_DOUBLEDOUBLE
TEST_CASE("fit2p DoubleDouble", "[fit2p DoubleDouble]") {
  typedef mantis::DoubleMantissa<double> Real;
  const TestConfig<Real> lp_config{
      kFeasibleNetlib + std::string("fit2p.mps"), /* filename */
      false,                                      /* verbose */
      false,                                      /* tighten_infinite_bounds */
      false,                                      /* print_data */
      Real(6.846429329383207e4),                  /* objective */
      Real(1e-14),                                /* relative_objective_tol */
  };
  Test(lp_config);
}
#endif  // ifdef TEST_DOUBLEDOUBLE

// The listing on https://netlib.org/lp/data/readme is:
//     -6.6421873953E+02.

TEST_CASE("forplan", "[forplan]") {
  typedef double Real;
  const TestConfig<Real> lp_config{
      kFeasibleNetlib + std::string("forplan.mps"), /* filename */
      false,                                        /* verbose */
      false,                      /* tighten_infinite_bounds */
      false,                      /* print_data */
      Real(-6.642189612722046e2), /* objective */
      Real(5e-7),                 /* relative_objective_tol */
  };
  Test(lp_config);
}

#ifdef TEST_DOUBLEDOUBLE
TEST_CASE("forplan DoubleDouble", "[forplan DoubleDouble]") {
  typedef mantis::DoubleMantissa<double> Real;
  const TestConfig<Real> lp_config{
      kFeasibleNetlib + std::string("forplan.mps"), /* filename */
      false,                                        /* verbose */
      false,                      /* tighten_infinite_bounds */
      false,                      /* print_data */
      Real(-6.642189612722046e2), /* objective */
      Real(1e-14),                /* relative_objective_tol */
  };
  Test(lp_config);
}
#endif  // ifdef TEST_DOUBLEDOUBLE

// The listing on https://netlib.org/lp/data/readme is:
//     -1.0958636356E+05.

TEST_CASE("ganges", "[ganges]") {
  typedef double Real;
  const TestConfig<Real> lp_config{
      kFeasibleNetlib + std::string("ganges.mps"), /* filename */
      false,                                       /* verbose */
      false,                                       /* tighten_infinite_bounds */
      false,                                       /* print_data */
      Real(-1.095857361292782e5),                  /* objective */
      Real(5e-6),                                  /* relative_objective_tol */
  };
  Test(lp_config);
}

#ifdef TEST_DOUBLEDOUBLE
TEST_CASE("ganges DoubleDouble", "[ganges DoubleDouble]") {
  typedef mantis::DoubleMantissa<double> Real;
  const TestConfig<Real> lp_config{
      kFeasibleNetlib + std::string("ganges.mps"), /* filename */
      false,                                       /* verbose */
      false,                                       /* tighten_infinite_bounds */
      false,                                       /* print_data */
      Real(-1.095857361292782e5),                  /* objective */
      Real(5e-13),                                 /* relative_objective_tol */
  };
  Test(lp_config);
}
#endif  // ifdef TEST_DOUBLEDOUBLE

// The listing on https://netlib.org/lp/data/readme is:
//     6.9022359995E+06.

TEST_CASE("gfrd-pnc", "[gfrd-pnc]") {
  typedef double Real;
  const TestConfig<Real> lp_config{
      kFeasibleNetlib + std::string("gfrd-pnc.mps"), /* filename */
      false,                                         /* verbose */
      false,                     /* tighten_infinite_bounds */
      false,                     /* print_data */
      Real(6.902235999548808e6), /* objective */
      Real(1e-7),                /* relative_objective_tol */
  };
  Test(lp_config);
}

#ifdef TEST_DOUBLEDOUBLE
TEST_CASE("gfrd-pnc DoubleDouble", "[gfrd-pnc DoubleDouble]") {
  typedef mantis::DoubleMantissa<double> Real;
  const TestConfig<Real> lp_config{
      kFeasibleNetlib + std::string("gfrd-pnc.mps"), /* filename */
      false,                                         /* verbose */
      false,                     /* tighten_infinite_bounds */
      false,                     /* print_data */
      Real(6.902235999548808e6), /* objective */
      Real(1e-14),               /* relative_objective_tol */
  };
  Test(lp_config);
}
#endif  // ifdef TEST_DOUBLEDOUBLE

// The listed objective value for greenbea on
// https://netlib.org/lp/data/readme is:
//     -7.2462405908E+07,
// which is matched to several digits when running in double-precision,
// but running in double-double precision produces a value of:
//     -7.255524812983e7
// with smaller residuals and two orders of magnitude larger solution
// norms (roughly 10^8 rather than 10^6). It would appear that this latter
// value, which is a better minimizer in all senses, is more correct.
//
// It is not clear if there are even larger solutions producing smaller
// objective values.
//
// But this latter value matches that reported by CPLEX further down the
// netlib lp data page:
//     -7.2555248130E+07.

TEST_CASE("greenbea", "[greenbea]") {
  typedef double Real;
  const TestConfig<Real> lp_config{
      kFeasibleNetlib + std::string("greenbea.mps"), /* filename */
      false,                                         /* verbose */
      false,                      /* tighten_infinite_bounds */
      false,                      /* print_data */
      Real(-7.255524812984598e7), /* objective */
      Real(3e-3),                 /* relative_objective_tol */
  };
  Test(lp_config);
}

#ifdef TEST_DOUBLEDOUBLE
TEST_CASE("greenbea DoubleDouble", "[greenbea DoubleDouble]") {
  typedef mantis::DoubleMantissa<double> Real;
  const TestConfig<Real> lp_config{
      kFeasibleNetlib + std::string("greenbea.mps"), /* filename */
      false,                                         /* verbose */
      false,                      /* tighten_infinite_bounds */
      false,                      /* print_data */
      Real(-7.255524812984598e7), /* objective */
      Real(1e-12),                /* relative_objective_tol */
  };
  Test(lp_config);
}
#endif  // ifdef TEST_DOUBLEDOUBLE

// The listing on https://netlib.org/lp/data/readme is:
//     -4.3021476065E+06.
// It is only accurate in its first four digits.

TEST_CASE("greenbeb", "[greenbeb]") {
  typedef double Real;
  const TestConfig<Real> lp_config{
      kFeasibleNetlib + std::string("greenbeb.mps"), /* filename */
      false,                                         /* verbose */
      false,                      /* tighten_infinite_bounds */
      false,                      /* print_data */
      Real(-4.302260261206587e6), /* objective */
      Real(5e-5),                 /* relative_objective_tol */
  };
  Test(lp_config);
}

#ifdef TEST_DOUBLEDOUBLE
TEST_CASE("greenbeb DoubleDouble", "[greenbeb DoubleDouble]") {
  typedef mantis::DoubleMantissa<double> Real;
  const TestConfig<Real> lp_config{
      kFeasibleNetlib + std::string("greenbeb.mps"), /* filename */
      false,                                         /* verbose */
      false,                      /* tighten_infinite_bounds */
      false,                      /* print_data */
      Real(-4.302260261206587e6), /* objective */
      Real(1e-14),                /* relative_objective_tol */
  };
  Test(lp_config);
}
#endif  // ifdef TEST_DOUBLEDOUBLE

// The listing on https://netlib.org/lp/data/readme is:
//     -1.0687094129E+08.

TEST_CASE("grow15", "[grow15]") {
  typedef double Real;
  const TestConfig<Real> lp_config{
      kFeasibleNetlib + std::string("grow15.mps"), /* filename */
      false,                                       /* verbose */
      false,                                       /* tighten_infinite_bounds */
      false,                                       /* print_data */
      Real(-1.068709412935753e8),                  /* objective */
      Real(1e-7),                                  /* relative_objective_tol */
  };
  Test(lp_config);
}

#ifdef TEST_DOUBLEDOUBLE
TEST_CASE("grow15 DoubleDouble", "[grow15 DoubleDouble]") {
  typedef mantis::DoubleMantissa<double> Real;
  const TestConfig<Real> lp_config{
      kFeasibleNetlib + std::string("grow15.mps"), /* filename */
      false,                                       /* verbose */
      false,                                       /* tighten_infinite_bounds */
      false,                                       /* print_data */
      Real(-1.068709412935753e8),                  /* objective */
      Real(1e-14),                                 /* relative_objective_tol */
  };
  Test(lp_config);
}
#endif  // ifdef TEST_DOUBLEDOUBLE

// The listing on https://netlib.org/lp/data/readme is:
//     -1.6083433648E+08.

TEST_CASE("grow22", "[grow22]") {
  typedef double Real;
  const TestConfig<Real> lp_config{
      kFeasibleNetlib + std::string("grow22.mps"), /* filename */
      false,                                       /* verbose */
      false,                                       /* tighten_infinite_bounds */
      false,                                       /* print_data */
      Real(-1.608343364825630e8),                  /* objective */
      Real(1e-7),                                  /* relative_objective_tol */
  };
  Test(lp_config);
}

#ifdef TEST_DOUBLEDOUBLE
TEST_CASE("grow22 DoubleDouble", "[grow22 DoubleDouble]") {
  typedef mantis::DoubleMantissa<double> Real;
  const TestConfig<Real> lp_config{
      kFeasibleNetlib + std::string("grow22.mps"), /* filename */
      false,                                       /* verbose */
      false,                                       /* tighten_infinite_bounds */
      false,                                       /* print_data */
      Real(-1.608343364825630e8),                  /* objective */
      Real(1e-14),                                 /* relative_objective_tol */
  };
  Test(lp_config);
}
#endif  // ifdef TEST_DOUBLEDOUBLE

// The listing on https://netlib.org/lp/data/readme is:
//     -4.7787811815E+07.

TEST_CASE("grow7", "[grow7]") {
  typedef double Real;
  const TestConfig<Real> lp_config{
      kFeasibleNetlib + std::string("grow7.mps"), /* filename */
      false,                                      /* verbose */
      false,                                      /* tighten_infinite_bounds */
      false,                                      /* print_data */
      Real(-4.778781181471150e7),                 /* objective */
      Real(1e-7),                                 /* relative_objective_tol */
  };
  Test(lp_config);
}

#ifdef TEST_DOUBLEDOUBLE
TEST_CASE("grow7 DoubleDouble", "[grow7 DoubleDouble]") {
  typedef mantis::DoubleMantissa<double> Real;
  const TestConfig<Real> lp_config{
      kFeasibleNetlib + std::string("grow7.mps"), /* filename */
      false,                                      /* verbose */
      false,                                      /* tighten_infinite_bounds */
      false,                                      /* print_data */
      Real(-4.778781181471150e7),                 /* objective */
      Real(1e-14),                                /* relative_objective_tol */
  };
  Test(lp_config);
}
#endif  // ifdef TEST_DOUBLEDOUBLE

// The listing on https://netlib.org/lp/data/readme is:
//     -8.9664482186E+05.

TEST_CASE("israel", "[israel]") {
  typedef double Real;
  const TestConfig<Real> lp_config{
      kFeasibleNetlib + std::string("israel.mps"), /* filename */
      false,                                       /* verbose */
      false,                                       /* tighten_infinite_bounds */
      false,                                       /* print_data */
      Real(-8.966448218630457e5),                  /* objective */
      Real(1e-7),                                  /* relative_objective_tol */
  };
  Test(lp_config);
}

#ifdef TEST_DOUBLEDOUBLE
TEST_CASE("israel DoubleDouble", "[israel DoubleDouble]") {
  typedef mantis::DoubleMantissa<double> Real;
  const TestConfig<Real> lp_config{
      kFeasibleNetlib + std::string("israel.mps"), /* filename */
      false,                                       /* verbose */
      false,                                       /* tighten_infinite_bounds */
      false,                                       /* print_data */
      Real(-8.966448218630457e5),                  /* objective */
      Real(1e-14),                                 /* relative_objective_tol */
  };
  Test(lp_config);
}
#endif  // ifdef TEST_DOUBLEDOUBLE

// The listing on https://netlib.org/lp/data/readme is:
//     -1.7499001299E+03.

TEST_CASE("kb2", "[kb2]") {
  typedef double Real;
  const TestConfig<Real> lp_config{
      kFeasibleNetlib + std::string("kb2.mps"), /* filename */
      false,                                    /* verbose */
      false,                                    /* tighten_infinite_bounds */
      false,                                    /* print_data */
      Real(-1.749900129906206e3),               /* objective */
      Real(1e-7),                               /* relative_objective_tol */
  };
  Test(lp_config);
}

#ifdef TEST_DOUBLEDOUBLE
TEST_CASE("kb2 DoubleDouble", "[kb2 DoubleDouble]") {
  typedef mantis::DoubleMantissa<double> Real;
  const TestConfig<Real> lp_config{
      kFeasibleNetlib + std::string("kb2.mps"), /* filename */
      false,                                    /* verbose */
      false,                                    /* tighten_infinite_bounds */
      false,                                    /* print_data */
      Real(-1.749900129906206e3),               /* objective */
      Real(1e-14),                              /* relative_objective_tol */
  };
  Test(lp_config);
}
#endif  // ifdef TEST_DOUBLEDOUBLE

// The listing on https://netlib.org/lp/data/readme is:
//     -2.5264706062E+01.

TEST_CASE("lofti", "[lofti]") {
  typedef double Real;
  const TestConfig<Real> lp_config{
      kFeasibleNetlib + std::string("lofti.mps"), /* filename */
      false,                                      /* verbose */
      false,                                      /* tighten_infinite_bounds */
      false,                                      /* print_data */
      Real(-2.526470606188000e1),                 /* objective */
      Real(1e-7),                                 /* relative_objective_tol */
  };
  Test(lp_config);
}

#ifdef TEST_DOUBLEDOUBLE
TEST_CASE("lofti DoubleDouble", "[lofti DoubleDouble]") {
  typedef mantis::DoubleMantissa<double> Real;
  const TestConfig<Real> lp_config{
      kFeasibleNetlib + std::string("lofti.mps"), /* filename */
      false,                                      /* verbose */
      false,                                      /* tighten_infinite_bounds */
      false,                                      /* print_data */
      Real(-2.526470606188000e1),                 /* objective */
      Real(1e-14),                                /* relative_objective_tol */
  };
  Test(lp_config);
}
#endif  // ifdef TEST_DOUBLEDOUBLE

// The listing on https://netlib.org/lp/data/readme is:
//     -5.8063743701E+04.

TEST_CASE("maros", "[maros]") {
  typedef double Real;
  const TestConfig<Real> lp_config{
      kFeasibleNetlib + std::string("maros.mps"), /* filename */
      false,                                      /* verbose */
      false,                                      /* tighten_infinite_bounds */
      false,                                      /* print_data */
      Real(-5.806374370112589e4),                 /* objective */
      Real(5e-7),                                 /* relative_objective_tol */
  };
  Test(lp_config);
}

#ifdef TEST_DOUBLEDOUBLE
TEST_CASE("maros DoubleDouble", "[maros DoubleDouble]") {
  typedef mantis::DoubleMantissa<double> Real;
  const TestConfig<Real> lp_config{
      kFeasibleNetlib + std::string("maros.mps"), /* filename */
      false,                                      /* verbose */
      false,                                      /* tighten_infinite_bounds */
      false,                                      /* print_data */
      Real(-5.806374370112589e4),                 /* objective */
      Real(1e-14),                                /* relative_objective_tol */
  };
  Test(lp_config);
}
#endif  // ifdef TEST_DOUBLEDOUBLE

// The listing on https://netlib.org/lp/data/readme is:
//     1.4971851665E+06.

TEST_CASE("maros-r7", "[maros-r7]") {
  typedef double Real;
  const TestConfig<Real> lp_config{
      kFeasibleNetlib + std::string("maros-r7.mps"), /* filename */
      false,                                         /* verbose */
      false,                     /* tighten_infinite_bounds */
      false,                     /* print_data */
      Real(1.497185166479644e6), /* objective */
      Real(1e-7),                /* relative_objective_tol */
  };
  Test(lp_config);
}

#ifdef TEST_DOUBLEDOUBLE
TEST_CASE("maros-r7 DoubleDouble", "[maros-r7 DoubleDouble]") {
  typedef mantis::DoubleMantissa<double> Real;
  const TestConfig<Real> lp_config{
      kFeasibleNetlib + std::string("maros-r7.mps"), /* filename */
      false,                                         /* verbose */
      false,                     /* tighten_infinite_bounds */
      false,                     /* print_data */
      Real(1.497185166479644e6), /* objective */
      Real(1e-14),               /* relative_objective_tol */
  };
  Test(lp_config);
}
#endif  // ifdef TEST_DOUBLEDOUBLE

// The listing on https://netlib.org/lp/data/readme is:
//     3.2061972906E+02.

TEST_CASE("modszk1", "[modszk1]") {
  typedef double Real;
  const TestConfig<Real> lp_config{
      kFeasibleNetlib + std::string("modszk1.mps"), /* filename */
      false,                                        /* verbose */
      false,                     /* tighten_infinite_bounds */
      false,                     /* print_data */
      Real(3.206197290642565e2), /* objective */
      Real(1e-7),                /* relative_objective_tol */
  };
  Test(lp_config);
}

#ifdef TEST_DOUBLEDOUBLE
TEST_CASE("modszk1 DoubleDouble", "[modszk1 DoubleDouble]") {
  typedef mantis::DoubleMantissa<double> Real;
  const TestConfig<Real> lp_config{
      kFeasibleNetlib + std::string("modszk1.mps"), /* filename */
      false,                                        /* verbose */
      false,                     /* tighten_infinite_bounds */
      false,                     /* print_data */
      Real(3.206197290642565e2), /* objective */
      Real(5e-13),               /* relative_objective_tol */
  };
  Test(lp_config);
}
#endif  // ifdef TEST_DOUBLEDOUBLE

// The listing on https://netlib.org/lp/data/readme is:
//     1.4076073035E+07.

TEST_CASE("nesm", "[nesm]") {
  typedef double Real;
  const TestConfig<Real> lp_config{
      kFeasibleNetlib + std::string("nesm.mps"), /* filename */
      false,                                     /* verbose */
      false,                                     /* tighten_infinite_bounds */
      false,                                     /* print_data */
      Real(1.407603648756273e7),                 /* objective */
      Real(5e-7),                                /* relative_objective_tol */
  };
  Test(lp_config);
}

#ifdef TEST_DOUBLEDOUBLE
TEST_CASE("nesm DoubleDouble", "[nesm DoubleDouble]") {
  typedef mantis::DoubleMantissa<double> Real;
  const TestConfig<Real> lp_config{
      kFeasibleNetlib + std::string("nesm.mps"), /* filename */
      false,                                     /* verbose */
      false,                                     /* tighten_infinite_bounds */
      false,                                     /* print_data */
      Real(1.407603648756273e7),                 /* objective */
      Real(1e-14),                               /* relative_objective_tol */
  };
  Test(lp_config);
}
#endif  // ifdef TEST_DOUBLEDOUBLE

// The listing on https://netlib.org/lp/data/readme is:
//     -9.3807580773E+03.

TEST_CASE("perold", "[perold]") {
  typedef double Real;
  const TestConfig<Real> lp_config{
      kFeasibleNetlib + std::string("perold.mps"), /* filename */
      false,                                       /* verbose */
      false,                                       /* tighten_infinite_bounds */
      false,                                       /* print_data */
      Real(-9.380755278235161e3),                  /* objective */
      Real(1e-6),                                  /* relative_objective_tol */
  };
  Test(lp_config);
}

#ifdef TEST_DOUBLEDOUBLE
TEST_CASE("perold DoubleDouble", "[perold DoubleDouble]") {
  typedef mantis::DoubleMantissa<double> Real;
  const TestConfig<Real> lp_config{
      kFeasibleNetlib + std::string("perold.mps"), /* filename */
      false,                                       /* verbose */
      false,                                       /* tighten_infinite_bounds */
      false,                                       /* print_data */
      Real(-9.380755278235161e3),                  /* objective */
      Real(1e-12),                                 /* relative_objective_tol */
  };
  Test(lp_config);
}
#endif  // ifdef TEST_DOUBLEDOUBLE

// The objective listed on https://netlib.org/lp/data/readme for pilot is:
//     -5.5740430007E+02,
// but the value returned by CPLEX is reported as:
//     -5.5748972928E+02.

TEST_CASE("pilot", "[pilot]") {
  typedef double Real;
  const TestConfig<Real> lp_config{
      kFeasibleNetlib + std::string("pilot.mps"), /* filename */
      false,                                      /* verbose */
      false,                                      /* tighten_infinite_bounds */
      false,                                      /* print_data */
      Real(-5.574897292840682e2),                 /* objective */
      Real(5e-6),                                 /* relative_objective_tol */
  };
  Test(lp_config);
}

#ifdef TEST_DOUBLEDOUBLE
TEST_CASE("pilot DoubleDouble", "[pilot DoubleDouble]") {
  typedef mantis::DoubleMantissa<double> Real;
  const TestConfig<Real> lp_config{
      kFeasibleNetlib + std::string("pilot.mps"), /* filename */
      false,                                      /* verbose */
      false,                                      /* tighten_infinite_bounds */
      false,                                      /* print_data */
      Real(-5.574897292840682e2),                 /* objective */
      Real(5e-14),                                /* relative_objective_tol */
  };
  Test(lp_config);
}
#endif  // ifdef TEST_DOUBLEDOUBLE

// The listing on https://netlib.org/lp/data/readme is:
//     -6.1131344111E+03.

TEST_CASE("pilot.ja", "[pilot.ja]") {
  typedef double Real;
  const TestConfig<Real> lp_config{
      kFeasibleNetlib + std::string("pilot.ja.mps"), /* filename */
      false,                                         /* verbose */
      false,                      /* tighten_infinite_bounds */
      false,                      /* print_data */
      Real(-6.113136465581344e3), /* objective */
      Real(2e-5),                 /* relative_objective_tol */
  };
  Test(lp_config);
}

#ifdef TEST_DOUBLEDOUBLE
#ifdef TEST_PILOTJA_DOUBLEDOUBLE
TEST_CASE("pilot.ja DoubleDouble", "[pilot.ja DoubleDouble]") {
  typedef mantis::DoubleMantissa<double> Real;
  const TestConfig<Real> lp_config{
      kFeasibleNetlib + std::string("pilot.ja.mps"), /* filename */
      false,                                         /* verbose */
      false,                      /* tighten_infinite_bounds */
      false,                      /* print_data */
      Real(-6.113136465581344e3), /* objective */
      Real(1e-13),                /* relative_objective_tol */
  };
  Test(lp_config);
}
#endif  // ifdef TEST_PILOTJA_DOUBLEDOUBLE
#endif  // ifdef TEST_DOUBLEDOUBLE

// The listing on https://netlib.org/lp/data/readme is:
//     -2.7201027439E+06.

TEST_CASE("pilot.we", "[pilot.we]") {
  typedef double Real;
  const TestConfig<Real> lp_config{
      kFeasibleNetlib + std::string("pilot.we.mps"), /* filename */
      false,                                         /* verbose */
      false,                      /* tighten_infinite_bounds */
      false,                      /* print_data */
      Real(-2.720107532844964e6), /* objective */
      Real(5e-6),                 /* relative_objective_tol */
  };
  Test(lp_config);
}

#ifdef TEST_DOUBLEDOUBLE
TEST_CASE("pilot.we DoubleDouble", "[pilot.we DoubleDouble]") {
  typedef mantis::DoubleMantissa<double> Real;
  const TestConfig<Real> lp_config{
      kFeasibleNetlib + std::string("pilot.we.mps"), /* filename */
      false,                                         /* verbose */
      false,                      /* tighten_infinite_bounds */
      false,                      /* print_data */
      Real(-2.720107532844964e6), /* objective */
      Real(5e-12),                /* relative_objective_tol */
  };
  Test(lp_config);
}
#endif  // ifdef TEST_DOUBLEDOUBLE

// The listing on https://netlib.org/lp/data/readme is:
//     -2.5811392641E+03.

TEST_CASE("pilot4", "[pilot4]") {
  typedef double Real;
  const TestConfig<Real> lp_config{
      kFeasibleNetlib + std::string("pilot4.mps"), /* filename */
      false,                                       /* verbose */
      false,                                       /* tighten_infinite_bounds */
      false,                                       /* print_data */
      Real(-2.581139258883889e3),                  /* objective */
      Real(5e-7),                                  /* relative_objective_tol */
  };
  Test(lp_config);
}

#ifdef TEST_DOUBLEDOUBLE
TEST_CASE("pilot4 DoubleDouble", "[pilot4 DoubleDouble]") {
  typedef mantis::DoubleMantissa<double> Real;
  const TestConfig<Real> lp_config{
      kFeasibleNetlib + std::string("pilot4.mps"), /* filename */
      false,                                       /* verbose */
      false,                                       /* tighten_infinite_bounds */
      false,                                       /* print_data */
      Real(-2.581139258883889e3),                  /* objective */
      Real(1e-14),                                 /* relative_objective_tol */
  };
  Test(lp_config);
}
#endif  // ifdef TEST_DOUBLEDOUBLE

// The listing on https://netlib.org/lp/data/readme is:
//     3.0171072827E+02.

TEST_CASE("pilot87", "[pilot87]") {
  typedef double Real;
  const TestConfig<Real> lp_config{
      kFeasibleNetlib + std::string("pilot87.mps"), /* filename */
      false,                                        /* verbose */
      false,                     /* tighten_infinite_bounds */
      false,                     /* print_data */
      Real(3.017103473331105e2), /* objective */
      Real(1e-7),                /* relative_objective_tol */
  };
  Test(lp_config);
}

#ifdef TEST_DOUBLEDOUBLE
TEST_CASE("pilot87 DoubleDouble", "[pilot87 DoubleDouble]") {
  typedef mantis::DoubleMantissa<double> Real;
  const TestConfig<Real> lp_config{
      kFeasibleNetlib + std::string("pilot87.mps"), /* filename */
      false,                                        /* verbose */
      false,                     /* tighten_infinite_bounds */
      false,                     /* print_data */
      Real(3.017103473331105e2), /* objective */
      Real(1e-14),               /* relative_objective_tol */
  };
  Test(lp_config);
}
#endif  // ifdef TEST_DOUBLEDOUBLE

// The listing on https://netlib.org/lp/data/readme is:
//     -4.4972761882E+03.

TEST_CASE("pilotnov", "[pilotnov]") {
  typedef double Real;
  const TestConfig<Real> lp_config{
      kFeasibleNetlib + std::string("pilotnov.mps"), /* filename */
      false,                                         /* verbose */
      false,                      /* tighten_infinite_bounds */
      false,                      /* print_data */
      Real(-4.497276188218871e3), /* objective */
      Real(1e-7),                 /* relative_objective_tol */
  };
  Test(lp_config);
}

#ifdef TEST_DOUBLEDOUBLE
#ifdef TEST_PILOTNOV_DOUBLEDOUBLE
TEST_CASE("pilotnov DoubleDouble", "[pilotnov DoubleDouble]") {
  typedef mantis::DoubleMantissa<double> Real;
  const TestConfig<Real> lp_config{
      kFeasibleNetlib + std::string("pilotnov.mps"), /* filename */
      false,                                         /* verbose */
      false,                      /* tighten_infinite_bounds */
      false,                      /* print_data */
      Real(-4.497276188218871e3), /* objective */
      Real(1e-14),                /* relative_objective_tol */
  };
  Test(lp_config);
}
#endif  // ifdef TEST_PILOTNOV_DOUBLEDOUBLE
#endif  // ifdef TEST_DOUBLEDOUBLE

// The listing on https://netlib.org/lp/data/readme is:
//     -2.6661600000E+02.

TEST_CASE("recipe", "[recipe]") {
  typedef double Real;
  const TestConfig<Real> lp_config{
      kFeasibleNetlib + std::string("recipe.mps"), /* filename */
      false,                                       /* verbose */
      false,                                       /* tighten_infinite_bounds */
      false,                                       /* print_data */
      Real(-2.666160000000000e2),                  /* objective */
      Real(1e-7),                                  /* relative_objective_tol */
  };
  Test(lp_config);
}

#ifdef TEST_DOUBLEDOUBLE
TEST_CASE("recipe DoubleDouble", "[recipe DoubleDouble]") {
  typedef mantis::DoubleMantissa<double> Real;
  const TestConfig<Real> lp_config{
      kFeasibleNetlib + std::string("recipe.mps"), /* filename */
      false,                                       /* verbose */
      false,                                       /* tighten_infinite_bounds */
      false,                                       /* print_data */
      Real(-2.666160000000000e2),                  /* objective */
      Real(1e-14),                                 /* relative_objective_tol */
  };
  Test(lp_config);
}
#endif  // ifdef TEST_DOUBLEDOUBLE

// The listing on https://netlib.org/lp/data/readme is:
//     -5.2202061212E+01.

TEST_CASE("sc105", "[sc105]") {
  typedef double Real;
  const TestConfig<Real> lp_config{
      kFeasibleNetlib + std::string("sc105.mps"), /* filename */
      false,                                      /* verbose */
      false,                                      /* tighten_infinite_bounds */
      false,                                      /* print_data */
      Real(-5.220206121170724e1),                 /* objective */
      Real(1e-7),                                 /* relative_objective_tol */
  };
  Test(lp_config);
}

#ifdef TEST_DOUBLEDOUBLE
TEST_CASE("sc105 DoubleDouble", "[sc105 DoubleDouble]") {
  typedef mantis::DoubleMantissa<double> Real;
  const TestConfig<Real> lp_config{
      kFeasibleNetlib + std::string("sc105.mps"), /* filename */
      false,                                      /* verbose */
      false,                                      /* tighten_infinite_bounds */
      false,                                      /* print_data */
      Real(-5.220206121170724e1),                 /* objective */
      Real(1e-14),                                /* relative_objective_tol */
  };
  Test(lp_config);
}
#endif  // ifdef TEST_DOUBLEDOUBLE

// The listing on https://netlib.org/lp/data/readme is:
//     -5.2202061212E+01.

TEST_CASE("sc205", "[sc205]") {
  typedef double Real;
  const TestConfig<Real> lp_config{
      kFeasibleNetlib + std::string("sc205.mps"), /* filename */
      false,                                      /* verbose */
      false,                                      /* tighten_infinite_bounds */
      false,                                      /* print_data */
      Real(-5.220206121170723e1),                 /* objective */
      Real(1e-7),                                 /* relative_objective_tol */
  };
  Test(lp_config);
}

#ifdef TEST_DOUBLEDOUBLE
TEST_CASE("sc205 DoubleDouble", "[sc205 DoubleDouble]") {
  typedef mantis::DoubleMantissa<double> Real;
  const TestConfig<Real> lp_config{
      kFeasibleNetlib + std::string("sc205.mps"), /* filename */
      false,                                      /* verbose */
      false,                                      /* tighten_infinite_bounds */
      false,                                      /* print_data */
      Real(-5.220206121170723e1),                 /* objective */
      Real(1e-14),                                /* relative_objective_tol */
  };
  Test(lp_config);
}
#endif  // ifdef TEST_DOUBLEDOUBLE

// The listing on https://netlib.org/lp/data/readme is:
//     -6.4575077059E+01.

TEST_CASE("sc50a", "[sc50a]") {
  typedef double Real;
  const TestConfig<Real> lp_config{
      kFeasibleNetlib + std::string("sc50a.mps"), /* filename */
      false,                                      /* verbose */
      false,                                      /* tighten_infinite_bounds */
      false,                                      /* print_data */
      Real(-6.457507705856450e1),                 /* objective */
      Real(1e-7),                                 /* relative_objective_tol */
  };
  Test(lp_config);
}

#ifdef TEST_DOUBLEDOUBLE
TEST_CASE("sc50a DoubleDouble", "[sc50a DoubleDouble]") {
  typedef mantis::DoubleMantissa<double> Real;
  const TestConfig<Real> lp_config{
      kFeasibleNetlib + std::string("sc50a.mps"), /* filename */
      false,                                      /* verbose */
      false,                                      /* tighten_infinite_bounds */
      false,                                      /* print_data */
      Real(-6.457507705856450e1),                 /* objective */
      Real(1e-14),                                /* relative_objective_tol */
  };
  Test(lp_config);
}
#endif  // ifdef TEST_DOUBLEDOUBLE

// The listing on https://netlib.org/lp/data/readme is:
//     -7.0000000000E+01.

TEST_CASE("sc50b", "[sc50b]") {
  typedef double Real;
  const TestConfig<Real> lp_config{
      kFeasibleNetlib + std::string("sc50b.mps"), /* filename */
      false,                                      /* verbose */
      false,                                      /* tighten_infinite_bounds */
      false,                                      /* print_data */
      Real(-6.999999999999999e1),                 /* objective */
      Real(1e-7),                                 /* relative_objective_tol */
  };
  Test(lp_config);
}

#ifdef TEST_DOUBLEDOUBLE
TEST_CASE("sc50b DoubleDouble", "[sc50b DoubleDouble]") {
  typedef mantis::DoubleMantissa<double> Real;
  const TestConfig<Real> lp_config{
      kFeasibleNetlib + std::string("sc50b.mps"), /* filename */
      false,                                      /* verbose */
      false,                                      /* tighten_infinite_bounds */
      false,                                      /* print_data */
      Real(-6.999999999999999e1),                 /* objective */
      Real(1e-14),                                /* relative_objective_tol */
  };
  Test(lp_config);
}
#endif  // ifdef TEST_DOUBLEDOUBLE

// The listing on https://netlib.org/lp/data/readme is:
//     -1.4753433061E+07.

TEST_CASE("scagr25", "[scagr25]") {
  typedef double Real;
  const TestConfig<Real> lp_config{
      kFeasibleNetlib + std::string("scagr25.mps"), /* filename */
      false,                                        /* verbose */
      false,                      /* tighten_infinite_bounds */
      false,                      /* print_data */
      Real(-1.475343306076852e7), /* objective */
      Real(1e-7),                 /* relative_objective_tol */
  };
  Test(lp_config);
}

#ifdef TEST_DOUBLEDOUBLE
TEST_CASE("scagr25 DoubleDouble", "[scagr25 DoubleDouble]") {
  typedef mantis::DoubleMantissa<double> Real;
  const TestConfig<Real> lp_config{
      kFeasibleNetlib + std::string("scagr25.mps"), /* filename */
      false,                                        /* verbose */
      false,                      /* tighten_infinite_bounds */
      false,                      /* print_data */
      Real(-1.475343306076852e7), /* objective */
      Real(1e-14),                /* relative_objective_tol */
  };
  Test(lp_config);
}
#endif  // ifdef TEST_DOUBLEDOUBLE

// The listing on https://netlib.org/lp/data/readme is:
//     -2.3313892548E+06.

TEST_CASE("scagr7", "[scagr7]") {
  typedef double Real;
  const TestConfig<Real> lp_config{
      kFeasibleNetlib + std::string("scagr7.mps"), /* filename */
      false,                                       /* verbose */
      false,                                       /* tighten_infinite_bounds */
      false,                                       /* print_data */
      Real(-2.331389824330984e6),                  /* objective */
      Real(1e-7),                                  /* relative_objective_tol */
  };
  Test(lp_config);
}

#ifdef TEST_DOUBLEDOUBLE
TEST_CASE("scagr7 DoubleDouble", "[scagr7 DoubleDouble]") {
  typedef mantis::DoubleMantissa<double> Real;
  const TestConfig<Real> lp_config{
      kFeasibleNetlib + std::string("scagr7.mps"), /* filename */
      false,                                       /* verbose */
      false,                                       /* tighten_infinite_bounds */
      false,                                       /* print_data */
      Real(-2.331389824330984e6),                  /* objective */
      Real(1e-14),                                 /* relative_objective_tol */
  };
  Test(lp_config);
}
#endif  // ifdef TEST_DOUBLEDOUBLE

// The listing on https://netlib.org/lp/data/readme is:
//     1.8416759028E+04.

TEST_CASE("scfxm1", "[scfxm1]") {
  typedef double Real;
  const TestConfig<Real> lp_config{
      kFeasibleNetlib + std::string("scfxm1.mps"), /* filename */
      false,                                       /* verbose */
      false,                                       /* tighten_infinite_bounds */
      false,                                       /* print_data */
      Real(1.841675902834894e4),                   /* objective */
      Real(1e-7),                                  /* relative_objective_tol */
  };
  Test(lp_config);
}

#ifdef TEST_DOUBLEDOUBLE
TEST_CASE("scfxm1 DoubleDouble", "[scfxm1 DoubleDouble]") {
  typedef mantis::DoubleMantissa<double> Real;
  const TestConfig<Real> lp_config{
      kFeasibleNetlib + std::string("scfxm1.mps"), /* filename */
      false,                                       /* verbose */
      false,                                       /* tighten_infinite_bounds */
      false,                                       /* print_data */
      Real(1.841675902834894e4),                   /* objective */
      Real(1e-14),                                 /* relative_objective_tol */
  };
  Test(lp_config);
}
#endif  // ifdef TEST_DOUBLEDOUBLE

// The listing on https://netlib.org/lp/data/readme is:
//     3.6660261565E+04.

TEST_CASE("scfxm2", "[scfxm2]") {
  typedef double Real;
  const TestConfig<Real> lp_config{
      kFeasibleNetlib + std::string("scfxm2.mps"), /* filename */
      false,                                       /* verbose */
      false,                                       /* tighten_infinite_bounds */
      false,                                       /* print_data */
      Real(3.666026156499881e4),                   /* objective */
      Real(1e-7),                                  /* relative_objective_tol */
  };
  Test(lp_config);
}

#ifdef TEST_DOUBLEDOUBLE
TEST_CASE("scfxm2 DoubleDouble", "[scfxm2 DoubleDouble]") {
  typedef mantis::DoubleMantissa<double> Real;
  const TestConfig<Real> lp_config{
      kFeasibleNetlib + std::string("scfxm2.mps"), /* filename */
      false,                                       /* verbose */
      false,                                       /* tighten_infinite_bounds */
      false,                                       /* print_data */
      Real(3.666026156499881e4),                   /* objective */
      Real(1e-14),                                 /* relative_objective_tol */
  };
  Test(lp_config);
}
#endif  // ifdef TEST_DOUBLEDOUBLE

// The listing on https://netlib.org/lp/data/readme is:
//     5.4901254550E+04.

TEST_CASE("scfxm3", "[scfxm3]") {
  typedef double Real;
  const TestConfig<Real> lp_config{
      kFeasibleNetlib + std::string("scfxm3.mps"), /* filename */
      false,                                       /* verbose */
      false,                                       /* tighten_infinite_bounds */
      false,                                       /* print_data */
      Real(5.490125454975145e4),                   /* objective */
      Real(5e-7),                                  /* relative_objective_tol */
  };
  Test(lp_config);
}

#ifdef TEST_DOUBLEDOUBLE
TEST_CASE("scfxm3 DoubleDouble", "[scfxm3 DoubleDouble]") {
  typedef mantis::DoubleMantissa<double> Real;
  const TestConfig<Real> lp_config{
      kFeasibleNetlib + std::string("scfxm3.mps"), /* filename */
      false,                                       /* verbose */
      false,                                       /* tighten_infinite_bounds */
      false,                                       /* print_data */
      Real(5.490125454975145e4),                   /* objective */
      Real(1e-14),                                 /* relative_objective_tol */
  };
  Test(lp_config);
}
#endif  // ifdef TEST_DOUBLEDOUBLE

// The listing on https://netlib.org/lp/data/readme is:
//     1.8781248227E+03.

TEST_CASE("scorpion", "[scorpion]") {
  typedef double Real;
  const TestConfig<Real> lp_config{
      kFeasibleNetlib + std::string("scorpion.mps"), /* filename */
      false,                                         /* verbose */
      false,                     /* tighten_infinite_bounds */
      false,                     /* print_data */
      Real(1.878124822738107e3), /* objective */
      Real(1e-7),                /* relative_objective_tol */
  };
  Test(lp_config);
}

#ifdef TEST_DOUBLEDOUBLE
TEST_CASE("scorpion DoubleDouble", "[scorpion DoubleDouble]") {
  typedef mantis::DoubleMantissa<double> Real;
  const TestConfig<Real> lp_config{
      kFeasibleNetlib + std::string("scorpion.mps"), /* filename */
      false,                                         /* verbose */
      false,                     /* tighten_infinite_bounds */
      false,                     /* print_data */
      Real(1.878124822738107e3), /* objective */
      Real(1e-14),               /* relative_objective_tol */
  };
  Test(lp_config);
}
#endif  // ifdef TEST_DOUBLEDOUBLE

// The listing on https://netlib.org/lp/data/readme is:
//     9.0429998619E+02.

TEST_CASE("scrs8", "[scrs8]") {
  typedef double Real;
  const TestConfig<Real> lp_config{
      kFeasibleNetlib + std::string("scrs8.mps"), /* filename */
      false,                                      /* verbose */
      false,                                      /* tighten_infinite_bounds */
      false,                                      /* print_data */
      Real(9.042969538007914e2),                  /* objective */
      Real(1e-7),                                 /* relative_objective_tol */
  };
  Test(lp_config);
}

#ifdef TEST_DOUBLEDOUBLE
TEST_CASE("scrs8 DoubleDouble", "[scrs8 DoubleDouble]") {
  typedef mantis::DoubleMantissa<double> Real;
  const TestConfig<Real> lp_config{
      kFeasibleNetlib + std::string("scrs8.mps"), /* filename */
      false,                                      /* verbose */
      false,                                      /* tighten_infinite_bounds */
      false,                                      /* print_data */
      Real(9.042969538007914e2),                  /* objective */
      Real(1e-14),                                /* relative_objective_tol */
  };
  Test(lp_config);
}
#endif  // ifdef TEST_DOUBLEDOUBLE

// The listing on https://netlib.org/lp/data/readme is:
//     8.6666666743E+00.

TEST_CASE("scsd1", "[scsd1]") {
  typedef double Real;
  const TestConfig<Real> lp_config{
      kFeasibleNetlib + std::string("scsd1.mps"), /* filename */
      false,                                      /* verbose */
      false,                                      /* tighten_infinite_bounds */
      false,                                      /* print_data */
      Real(8.666666674333365),                    /* objective */
      Real(1e-7),                                 /* relative_objective_tol */
  };
  Test(lp_config);
}

#ifdef TEST_DOUBLEDOUBLE
TEST_CASE("scsd1 DoubleDouble", "[scsd1 DoubleDouble]") {
  typedef mantis::DoubleMantissa<double> Real;
  const TestConfig<Real> lp_config{
      kFeasibleNetlib + std::string("scsd1.mps"), /* filename */
      false,                                      /* verbose */
      false,                                      /* tighten_infinite_bounds */
      false,                                      /* print_data */
      Real(8.666666674333365),                    /* objective */
      Real(1e-14),                                /* relative_objective_tol */
  };
  Test(lp_config);
}
#endif  // ifdef TEST_DOUBLEDOUBLE

// The listing on https://netlib.org/lp/data/readme is:
//     5.0500000078E+01.

TEST_CASE("scsd6", "[scsd6]") {
  typedef double Real;
  const TestConfig<Real> lp_config{
      kFeasibleNetlib + std::string("scsd6.mps"), /* filename */
      false,                                      /* verbose */
      false,                                      /* tighten_infinite_bounds */
      false,                                      /* print_data */
      Real(5.050000007714434e1),                  /* objective */
      Real(1e-7),                                 /* relative_objective_tol */
  };
  Test(lp_config);
}

#ifdef TEST_DOUBLEDOUBLE
TEST_CASE("scsd6 DoubleDouble", "[scsd6 DoubleDouble]") {
  typedef mantis::DoubleMantissa<double> Real;
  const TestConfig<Real> lp_config{
      kFeasibleNetlib + std::string("scsd6.mps"), /* filename */
      false,                                      /* verbose */
      false,                                      /* tighten_infinite_bounds */
      false,                                      /* print_data */
      Real(5.050000007714434e1),                  /* objective */
      Real(1e-14),                                /* relative_objective_tol */
  };
  Test(lp_config);
}
#endif  // ifdef TEST_DOUBLEDOUBLE

// The listing on https://netlib.org/lp/data/readme is:
//     9.0499999993E+02.

TEST_CASE("scsd8", "[scsd8]") {
  typedef double Real;
  const TestConfig<Real> lp_config{
      kFeasibleNetlib + std::string("scsd8.mps"), /* filename */
      false,                                      /* verbose */
      false,                                      /* tighten_infinite_bounds */
      false,                                      /* print_data */
      Real(9.049999999254644e2),                  /* objective */
      Real(1e-7),                                 /* relative_objective_tol */
  };
  Test(lp_config);
}

#ifdef TEST_DOUBLEDOUBLE
TEST_CASE("scsd8 DoubleDouble", "[scsd8 DoubleDouble]") {
  typedef mantis::DoubleMantissa<double> Real;
  const TestConfig<Real> lp_config{
      kFeasibleNetlib + std::string("scsd8.mps"), /* filename */
      false,                                      /* verbose */
      false,                                      /* tighten_infinite_bounds */
      false,                                      /* print_data */
      Real(9.049999999254644e2),                  /* objective */
      Real(1e-14),                                /* relative_objective_tol */
  };
  Test(lp_config);
}
#endif  // ifdef TEST_DOUBLEDOUBLE

// The listing on https://netlib.org/lp/data/readme is:
//     1.4122500000E+03.

TEST_CASE("sctap1", "[sctap1]") {
  typedef double Real;
  const TestConfig<Real> lp_config{
      kFeasibleNetlib + std::string("sctap1.mps"), /* filename */
      false,                                       /* verbose */
      false,                                       /* tighten_infinite_bounds */
      false,                                       /* print_data */
      Real(1.412250000000000e3),                   /* objective */
      Real(1e-7),                                  /* relative_objective_tol */
  };
  Test(lp_config);
}

#ifdef TEST_DOUBLEDOUBLE
TEST_CASE("sctap1 DoubleDouble", "[sctap1 DoubleDouble]") {
  typedef mantis::DoubleMantissa<double> Real;
  const TestConfig<Real> lp_config{
      kFeasibleNetlib + std::string("sctap1.mps"), /* filename */
      false,                                       /* verbose */
      false,                                       /* tighten_infinite_bounds */
      false,                                       /* print_data */
      Real(1.412250000000000e3),                   /* objective */
      Real(1e-14),                                 /* relative_objective_tol */
  };
  Test(lp_config);
}
#endif  // ifdef TEST_DOUBLEDOUBLE

// The listing on https://netlib.org/lp/data/readme is:
//     1.7248071429E+03.

TEST_CASE("sctap2", "[sctap2]") {
  typedef double Real;
  const TestConfig<Real> lp_config{
      kFeasibleNetlib + std::string("sctap2.mps"), /* filename */
      false,                                       /* verbose */
      false,                                       /* tighten_infinite_bounds */
      false,                                       /* print_data */
      Real(1.724807142857143e3),                   /* objective */
      Real(1e-7),                                  /* relative_objective_tol */
  };
  Test(lp_config);
}

#ifdef TEST_DOUBLEDOUBLE
TEST_CASE("sctap2 DoubleDouble", "[sctap2 DoubleDouble]") {
  typedef mantis::DoubleMantissa<double> Real;
  const TestConfig<Real> lp_config{
      kFeasibleNetlib + std::string("sctap2.mps"), /* filename */
      false,                                       /* verbose */
      false,                                       /* tighten_infinite_bounds */
      false,                                       /* print_data */
      Real(1.724807142857143e3),                   /* objective */
      Real(1e-14),                                 /* relative_objective_tol */
  };
  Test(lp_config);
}
#endif  // ifdef TEST_DOUBLEDOUBLE

// The listing on https://netlib.org/lp/data/readme is:
//     1.4240000000E+03.

TEST_CASE("sctap3", "[sctap3]") {
  typedef double Real;
  const TestConfig<Real> lp_config{
      kFeasibleNetlib + std::string("sctap3.mps"), /* filename */
      false,                                       /* verbose */
      false,                                       /* tighten_infinite_bounds */
      false,                                       /* print_data */
      Real(1.424000000000000e3),                   /* objective */
      Real(1e-7),                                  /* relative_objective_tol */
  };
  Test(lp_config);
}

#ifdef TEST_DOUBLEDOUBLE
TEST_CASE("sctap3 DoubleDouble", "[sctap3 DoubleDouble]") {
  typedef mantis::DoubleMantissa<double> Real;
  const TestConfig<Real> lp_config{
      kFeasibleNetlib + std::string("sctap3.mps"), /* filename */
      false,                                       /* verbose */
      false,                                       /* tighten_infinite_bounds */
      false,                                       /* print_data */
      Real(1.424000000000000e3),                   /* objective */
      Real(1e-14),                                 /* relative_objective_tol */
  };
  Test(lp_config);
}
#endif  // ifdef TEST_DOUBLEDOUBLE

// The listing on https://netlib.org/lp/data/readme is:
//     1.5711600000E+04.

TEST_CASE("seba", "[seba]") {
  typedef double Real;
  const TestConfig<Real> lp_config{
      kFeasibleNetlib + std::string("seba.mps"), /* filename */
      false,                                     /* verbose */
      false,                                     /* tighten_infinite_bounds */
      false,                                     /* print_data */
      Real(1.571160000000000e4),                 /* objective */
      Real(1e-7),                                /* relative_objective_tol */
  };
  Test(lp_config);
}

#ifdef TEST_DOUBLEDOUBLE
TEST_CASE("seba DoubleDouble", "[seba DoubleDouble]") {
  typedef mantis::DoubleMantissa<double> Real;
  const TestConfig<Real> lp_config{
      kFeasibleNetlib + std::string("seba.mps"), /* filename */
      false,                                     /* verbose */
      false,                                     /* tighten_infinite_bounds */
      false,                                     /* print_data */
      Real(1.571160000000000e4),                 /* objective */
      Real(1e-14),                               /* relative_objective_tol */
  };
  Test(lp_config);
}
#endif  // ifdef TEST_DOUBLEDOUBLE

// The listing on https://netlib.org/lp/data/readme is:
//     -7.6589318579E+04.

TEST_CASE("share1b", "[share1b]") {
  typedef double Real;
  const TestConfig<Real> lp_config{
      kFeasibleNetlib + std::string("share1b.mps"), /* filename */
      false,                                        /* verbose */
      false,                      /* tighten_infinite_bounds */
      false,                      /* print_data */
      Real(-7.658931857918567e4), /* objective */
      Real(1e-7),                 /* relative_objective_tol */
  };
  Test(lp_config);
}

#ifdef TEST_DOUBLEDOUBLE
TEST_CASE("share1b DoubleDouble", "[share1b DoubleDouble]") {
  typedef mantis::DoubleMantissa<double> Real;
  const TestConfig<Real> lp_config{
      kFeasibleNetlib + std::string("share1b.mps"), /* filename */
      false,                                        /* verbose */
      false,                      /* tighten_infinite_bounds */
      false,                      /* print_data */
      Real(-7.658931857918567e4), /* objective */
      Real(1e-14),                /* relative_objective_tol */
  };
  Test(lp_config);
}
#endif  // ifdef TEST_DOUBLEDOUBLE

// The listing on https://netlib.org/lp/data/readme is:
//     -4.1573224074E+02.

TEST_CASE("share2b", "[share2b]") {
  typedef double Real;
  const TestConfig<Real> lp_config{
      kFeasibleNetlib + std::string("share2b.mps"), /* filename */
      false,                                        /* verbose */
      false,                      /* tighten_infinite_bounds */
      false,                      /* print_data */
      Real(-4.157322407414193e2), /* objective */
      Real(1e-7),                 /* relative_objective_tol */
  };
  Test(lp_config);
}

#ifdef TEST_DOUBLEDOUBLE
TEST_CASE("share2b DoubleDouble", "[share2b DoubleDouble]") {
  typedef mantis::DoubleMantissa<double> Real;
  const TestConfig<Real> lp_config{
      kFeasibleNetlib + std::string("share2b.mps"), /* filename */
      false,                                        /* verbose */
      false,                      /* tighten_infinite_bounds */
      false,                      /* print_data */
      Real(-4.157322407414193e2), /* objective */
      Real(1e-14),                /* relative_objective_tol */
  };
  Test(lp_config);
}
#endif  // ifdef TEST_DOUBLEDOUBLE

// The listing on https://netlib.org/lp/data/readme is:
//     1.2088253460E+09.

TEST_CASE("shell", "[shell]") {
  typedef double Real;
  const TestConfig<Real> lp_config{
      kFeasibleNetlib + std::string("shell.mps"), /* filename */
      false,                                      /* verbose */
      false,                                      /* tighten_infinite_bounds */
      false,                                      /* print_data */
      Real(1.208825346000000e9),                  /* objective */
      Real(1e-7),                                 /* relative_objective_tol */
  };
  Test(lp_config);
}

#ifdef TEST_DOUBLEDOUBLE
TEST_CASE("shell DoubleDouble", "[shell DoubleDouble]") {
  typedef mantis::DoubleMantissa<double> Real;
  const TestConfig<Real> lp_config{
      kFeasibleNetlib + std::string("shell.mps"), /* filename */
      false,                                      /* verbose */
      false,                                      /* tighten_infinite_bounds */
      false,                                      /* print_data */
      Real(1.208825346000000e9),                  /* objective */
      Real(1e-14),                                /* relative_objective_tol */
  };
  Test(lp_config);
}
#endif  // ifdef TEST_DOUBLEDOUBLE

// The listing on https://netlib.org/lp/data/readme is:
//     1.7933245380E+06.

TEST_CASE("ship04l", "[ship04l]") {
  typedef double Real;
  const TestConfig<Real> lp_config{
      kFeasibleNetlib + std::string("ship04l.mps"), /* filename */
      false,                                        /* verbose */
      false,                     /* tighten_infinite_bounds */
      false,                     /* print_data */
      Real(1.793324537970356e6), /* objective */
      Real(1e-7),                /* relative_objective_tol */
  };
  Test(lp_config);
}

#ifdef TEST_DOUBLEDOUBLE
TEST_CASE("ship04l DoubleDouble", "[ship04l DoubleDouble]") {
  typedef mantis::DoubleMantissa<double> Real;
  const TestConfig<Real> lp_config{
      kFeasibleNetlib + std::string("ship04l.mps"), /* filename */
      false,                                        /* verbose */
      false,                     /* tighten_infinite_bounds */
      false,                     /* print_data */
      Real(1.793324537970356e6), /* objective */
      Real(1e-14),               /* relative_objective_tol */
  };
  Test(lp_config);
}
#endif  // ifdef TEST_DOUBLEDOUBLE

// The listing on https://netlib.org/lp/data/readme is:
//     1.7987147004E+06.

TEST_CASE("ship04s", "[ship04s]") {
  typedef double Real;
  const TestConfig<Real> lp_config{
      kFeasibleNetlib + std::string("ship04s.mps"), /* filename */
      false,                                        /* verbose */
      false,                     /* tighten_infinite_bounds */
      false,                     /* print_data */
      Real(1.798714700445392e6), /* objective */
      Real(1e-7),                /* relative_objective_tol */
  };
  Test(lp_config);
}

#ifdef TEST_DOUBLEDOUBLE
TEST_CASE("ship04s DoubleDouble", "[ship04s DoubleDouble]") {
  typedef mantis::DoubleMantissa<double> Real;
  const TestConfig<Real> lp_config{
      kFeasibleNetlib + std::string("ship04s.mps"), /* filename */
      false,                                        /* verbose */
      false,                     /* tighten_infinite_bounds */
      false,                     /* print_data */
      Real(1.798714700445392e6), /* objective */
      Real(1e-14),               /* relative_objective_tol */
  };
  Test(lp_config);
}
#endif  // ifdef TEST_DOUBLEDOUBLE

// The listing on https://netlib.org/lp/data/readme is:
//     1.9090552114E+06.

TEST_CASE("ship08l", "[ship08l]") {
  typedef double Real;
  const TestConfig<Real> lp_config{
      kFeasibleNetlib + std::string("ship08l.mps"), /* filename */
      false,                                        /* verbose */
      false,                     /* tighten_infinite_bounds */
      false,                     /* print_data */
      Real(1.909055211389132e6), /* objective */
      Real(1e-7),                /* relative_objective_tol */
  };
  Test(lp_config);
}

#ifdef TEST_DOUBLEDOUBLE
TEST_CASE("ship08l DoubleDouble", "[ship08l DoubleDouble]") {
  typedef mantis::DoubleMantissa<double> Real;
  const TestConfig<Real> lp_config{
      kFeasibleNetlib + std::string("ship08l.mps"), /* filename */
      false,                                        /* verbose */
      false,                     /* tighten_infinite_bounds */
      false,                     /* print_data */
      Real(1.909055211389132e6), /* objective */
      Real(1e-14),               /* relative_objective_tol */
  };
  Test(lp_config);
}
#endif  // ifdef TEST_DOUBLEDOUBLE

// The listing on https://netlib.org/lp/data/readme is:
//     1.9200982105E+06.

TEST_CASE("ship08s", "[ship08s]") {
  typedef double Real;
  const TestConfig<Real> lp_config{
      kFeasibleNetlib + std::string("ship08s.mps"), /* filename */
      false,                                        /* verbose */
      false,                     /* tighten_infinite_bounds */
      false,                     /* print_data */
      Real(1.920098210534620e6), /* objective */
      Real(1e-7),                /* relative_objective_tol */
  };
  Test(lp_config);
}

#ifdef TEST_DOUBLEDOUBLE
TEST_CASE("ship08s DoubleDouble", "[ship08s DoubleDouble]") {
  typedef mantis::DoubleMantissa<double> Real;
  const TestConfig<Real> lp_config{
      kFeasibleNetlib + std::string("ship08s.mps"), /* filename */
      false,                                        /* verbose */
      false,                     /* tighten_infinite_bounds */
      false,                     /* print_data */
      Real(1.920098210534620e6), /* objective */
      Real(1e-14),               /* relative_objective_tol */
  };
  Test(lp_config);
}
#endif  // ifdef TEST_DOUBLEDOUBLE

// The listing on https://netlib.org/lp/data/readme is:
//     1.4701879193E+06.

TEST_CASE("ship12l", "[ship12l]") {
  typedef double Real;
  const TestConfig<Real> lp_config{
      kFeasibleNetlib + std::string("ship12l.mps"), /* filename */
      false,                                        /* verbose */
      false,                     /* tighten_infinite_bounds */
      false,                     /* print_data */
      Real(1.470187919329265e6), /* objective */
      Real(1e-7),                /* relative_objective_tol */
  };
  Test(lp_config);
}

#ifdef TEST_DOUBLEDOUBLE
TEST_CASE("ship12l DoubleDouble", "[ship12l DoubleDouble]") {
  typedef mantis::DoubleMantissa<double> Real;
  const TestConfig<Real> lp_config{
      kFeasibleNetlib + std::string("ship12l.mps"), /* filename */
      false,                                        /* verbose */
      false,                     /* tighten_infinite_bounds */
      false,                     /* print_data */
      Real(1.470187919329265e6), /* objective */
      Real(1e-14),               /* relative_objective_tol */
  };
  Test(lp_config);
}
#endif  // ifdef TEST_DOUBLEDOUBLE

// The listing on https://netlib.org/lp/data/readme is:
//     1.4892361344E+06.

TEST_CASE("ship12s", "[ship12s]") {
  typedef double Real;
  const TestConfig<Real> lp_config{
      kFeasibleNetlib + std::string("ship12s.mps"), /* filename */
      false,                                        /* verbose */
      false,                     /* tighten_infinite_bounds */
      false,                     /* print_data */
      Real(1.489236134406129e6), /* objective */
      Real(1e-7),                /* relative_objective_tol */
  };
  Test(lp_config);
}

#ifdef TEST_DOUBLEDOUBLE
TEST_CASE("ship12s DoubleDouble", "[ship12s DoubleDouble]") {
  typedef mantis::DoubleMantissa<double> Real;
  const TestConfig<Real> lp_config{
      kFeasibleNetlib + std::string("ship12s.mps"), /* filename */
      false,                                        /* verbose */
      false,                     /* tighten_infinite_bounds */
      false,                     /* print_data */
      Real(1.489236134406129e6), /* objective */
      Real(1e-14),               /* relative_objective_tol */
  };
  Test(lp_config);
}
#endif  // ifdef TEST_DOUBLEDOUBLE

// The listing on https://netlib.org/lp/data/readme is:
//     1.5394362184E+07.

TEST_CASE("sierra", "[sierra]") {
  typedef double Real;
  const TestConfig<Real> lp_config{
      kFeasibleNetlib + std::string("sierra.mps"), /* filename */
      false,                                       /* verbose */
      false,                                       /* tighten_infinite_bounds */
      false,                                       /* print_data */
      Real(1.539436218363193e7),                   /* objective */
      Real(5e-7),                                  /* relative_objective_tol */
  };
  Test(lp_config);
}

#ifdef TEST_DOUBLEDOUBLE
TEST_CASE("sierra DoubleDouble", "[sierra DoubleDouble]") {
  typedef mantis::DoubleMantissa<double> Real;
  const TestConfig<Real> lp_config{
      kFeasibleNetlib + std::string("sierra.mps"), /* filename */
      false,                                       /* verbose */
      false,                                       /* tighten_infinite_bounds */
      false,                                       /* print_data */
      Real(1.539436218363193e7),                   /* objective */
      Real(5e-12),                                 /* relative_objective_tol */
  };
  Test(lp_config);
}
#endif  // ifdef TEST_DOUBLEDOUBLE

// The listing on https://netlib.org/lp/data/readme is:
//     -2.5126695119E+02.

TEST_CASE("stair", "[stair]") {
  typedef double Real;
  const TestConfig<Real> lp_config{
      kFeasibleNetlib + std::string("stair.mps"), /* filename */
      false,                                      /* verbose */
      false,                                      /* tighten_infinite_bounds */
      false,                                      /* print_data */
      Real(-2.512669511929633e2),                 /* objective */
      Real(1e-7),                                 /* relative_objective_tol */
  };
  Test(lp_config);
}

#ifdef TEST_DOUBLEDOUBLE
TEST_CASE("stair DoubleDouble", "[stair DoubleDouble]") {
  typedef mantis::DoubleMantissa<double> Real;
  const TestConfig<Real> lp_config{
      kFeasibleNetlib + std::string("stair.mps"), /* filename */
      false,                                      /* verbose */
      false,                                      /* tighten_infinite_bounds */
      false,                                      /* print_data */
      Real(-2.512669511929633e2),                 /* objective */
      Real(1e-14),                                /* relative_objective_tol */
  };
  Test(lp_config);
}
#endif  // ifdef TEST_DOUBLEDOUBLE

// The listing on https://netlib.org/lp/data/readme is:
//     1.2576995000E+03.

TEST_CASE("standata", "[standata]") {
  typedef double Real;
  const TestConfig<Real> lp_config{
      kFeasibleNetlib + std::string("standata.mps"), /* filename */
      false,                                         /* verbose */
      false,                     /* tighten_infinite_bounds */
      false,                     /* print_data */
      Real(1.257699500000000e3), /* objective */
      Real(1e-7),                /* relative_objective_tol */
  };
  Test(lp_config);
}

#ifdef TEST_DOUBLEDOUBLE
TEST_CASE("standata DoubleDouble", "[standata DoubleDouble]") {
  typedef mantis::DoubleMantissa<double> Real;
  const TestConfig<Real> lp_config{
      kFeasibleNetlib + std::string("standata.mps"), /* filename */
      false,                                         /* verbose */
      false,                     /* tighten_infinite_bounds */
      false,                     /* print_data */
      Real(1.257699500000000e3), /* objective */
      Real(1e-14),               /* relative_objective_tol */
  };
  Test(lp_config);
}
#endif  // ifdef TEST_DOUBLEDOUBLE

// The listing on https://netlib.org/lp/data/readme is:
//     1.4060175000E+03.

TEST_CASE("standmps", "[standmps]") {
  typedef double Real;
  const TestConfig<Real> lp_config{
      kFeasibleNetlib + std::string("standmps.mps"), /* filename */
      false,                                         /* verbose */
      false,                     /* tighten_infinite_bounds */
      false,                     /* print_data */
      Real(1.406017500000000e3), /* objective */
      Real(1e-7),                /* relative_objective_tol */
  };
  Test(lp_config);
}

#ifdef TEST_DOUBLEDOUBLE
TEST_CASE("standmps DoubleDouble", "[standmps DoubleDouble]") {
  typedef mantis::DoubleMantissa<double> Real;
  const TestConfig<Real> lp_config{
      kFeasibleNetlib + std::string("standmps.mps"), /* filename */
      false,                                         /* verbose */
      false,                     /* tighten_infinite_bounds */
      false,                     /* print_data */
      Real(1.406017500000000e3), /* objective */
      Real(1e-14),               /* relative_objective_tol */
  };
  Test(lp_config);
}
#endif  // ifdef TEST_DOUBLEDOUBLE

// The listing on https://netlib.org/lp/data/readme is:
//     -4.1131976219E+04.

TEST_CASE("stocfor1", "[stocfor1]") {
  typedef double Real;
  const TestConfig<Real> lp_config{
      kFeasibleNetlib + std::string("stocfor1.mps"), /* filename */
      false,                                         /* verbose */
      false,                      /* tighten_infinite_bounds */
      false,                      /* print_data */
      Real(-4.113197621943641e4), /* objective */
      Real(1e-7),                 /* relative_objective_tol */
  };
  Test(lp_config);
}

#ifdef TEST_DOUBLEDOUBLE
TEST_CASE("stocfor1 DoubleDouble", "[stocfor1 DoubleDouble]") {
  typedef mantis::DoubleMantissa<double> Real;
  const TestConfig<Real> lp_config{
      kFeasibleNetlib + std::string("stocfor1.mps"), /* filename */
      false,                                         /* verbose */
      false,                      /* tighten_infinite_bounds */
      false,                      /* print_data */
      Real(-4.113197621943641e4), /* objective */
      Real(1e-14),                /* relative_objective_tol */
  };
  Test(lp_config);
}
#endif  // ifdef TEST_DOUBLEDOUBLE

// The listing on https://netlib.org/lp/data/readme is:
//     -3.9024408538E+04.

TEST_CASE("stocfor2", "[stocfor2]") {
  typedef double Real;
  const TestConfig<Real> lp_config{
      kFeasibleNetlib + std::string("stocfor2.mps"), /* filename */
      false,                                         /* verbose */
      false,                      /* tighten_infinite_bounds */
      false,                      /* print_data */
      Real(-3.902440853788203e4), /* objective */
      Real(1e-7),                 /* relative_objective_tol */
  };
  Test(lp_config);
}

#ifdef TEST_DOUBLEDOUBLE
TEST_CASE("stocfor2 DoubleDouble", "[stocfor2 DoubleDouble]") {
  typedef mantis::DoubleMantissa<double> Real;
  const TestConfig<Real> lp_config{
      kFeasibleNetlib + std::string("stocfor2.mps"), /* filename */
      false,                                         /* verbose */
      false,                      /* tighten_infinite_bounds */
      false,                      /* print_data */
      Real(-3.902440853788203e4), /* objective */
      Real(1e-14),                /* relative_objective_tol */
  };
  Test(lp_config);
}
#endif  // ifdef TEST_DOUBLEDOUBLE

// TODO(Jack Poulson): Add support for the updated version of 'stocfor3'.

// TODO(Jack Poulson): Add support for the 'truss' problem.

// The listing on https://netlib.org/lp/data/readme is:
//     2.9214776509E-01.

TEST_CASE("tuff", "[tuff]") {
  typedef double Real;
  const TestConfig<Real> lp_config{
      kFeasibleNetlib + std::string("tuff.mps"), /* filename */
      false,                                     /* verbose */
      false,                                     /* tighten_infinite_bounds */
      false,                                     /* print_data */
      Real(2.921477650936128e-1),                /* objective */
      Real(1e-7),                                /* relative_objective_tol */
  };
  Test(lp_config);
}

#ifdef TEST_DOUBLEDOUBLE
TEST_CASE("tuff DoubleDouble", "[tuff DoubleDouble]") {
  typedef mantis::DoubleMantissa<double> Real;
  const TestConfig<Real> lp_config{
      kFeasibleNetlib + std::string("tuff.mps"), /* filename */
      false,                                     /* verbose */
      false,                                     /* tighten_infinite_bounds */
      false,                                     /* print_data */
      Real(2.921477650936128e-1),                /* objective */
      Real(1e-14),                               /* relative_objective_tol */
  };
  Test(lp_config);
}
#endif  // ifdef TEST_DOUBLEDOUBLE

// The listing on https://netlib.org/lp/data/readme is:
//     1.2983146246E+05.

TEST_CASE("vtp.base", "[vtp.base]") {
  typedef double Real;
  const TestConfig<Real> lp_config{
      kFeasibleNetlib + std::string("vtp.base.mps"), /* filename */
      false,                                         /* verbose */
      false,                     /* tighten_infinite_bounds */
      false,                     /* print_data */
      Real(1.298314624613614e5), /* objective */
      Real(1e-7),                /* relative_objective_tol */
  };
  Test(lp_config);
}

#ifdef TEST_DOUBLEDOUBLE
TEST_CASE("vtp.base DoubleDouble", "[vtp.base DoubleDouble]") {
  typedef mantis::DoubleMantissa<double> Real;
  const TestConfig<Real> lp_config{
      kFeasibleNetlib + std::string("vtp.base.mps"), /* filename */
      false,                                         /* verbose */
      false,                     /* tighten_infinite_bounds */
      false,                     /* print_data */
      Real(1.298314624613614e5), /* objective */
      Real(1e-14),               /* relative_objective_tol */
  };
  Test(lp_config);
}
#endif  // ifdef TEST_DOUBLEDOUBLE

// The listing on https://netlib.org/lp/data/readme is:
//     1.4429024116E+00.

TEST_CASE("wood1p", "[wood1p]") {
  typedef double Real;
  const TestConfig<Real> lp_config{
      kFeasibleNetlib + std::string("wood1p.mps"), /* filename */
      false,                                       /* verbose */
      false,                                       /* tighten_infinite_bounds */
      false,                                       /* print_data */
      Real(1.442902411573409),                     /* objective */
      Real(1e-7),                                  /* relative_objective_tol */
  };
  Test(lp_config);
}

#ifdef TEST_DOUBLEDOUBLE
TEST_CASE("wood1p DoubleDouble", "[wood1p DoubleDouble]") {
  typedef mantis::DoubleMantissa<double> Real;
  const TestConfig<Real> lp_config{
      kFeasibleNetlib + std::string("wood1p.mps"), /* filename */
      false,                                       /* verbose */
      false,                                       /* tighten_infinite_bounds */
      false,                                       /* print_data */
      Real(1.442902411573409),                     /* objective */
      Real(5e-14),                                 /* relative_objective_tol */
  };
  Test(lp_config);
}
#endif  // ifdef TEST_DOUBLEDOUBLE

// The listing on https://netlib.org/lp/data/readme is:
//     1.3044763331E+00.

TEST_CASE("woodw", "[woodw]") {
  typedef double Real;
  const TestConfig<Real> lp_config{
      kFeasibleNetlib + std::string("woodw.mps"), /* filename */
      false,                                      /* verbose */
      false,                                      /* tighten_infinite_bounds */
      false,                                      /* print_data */
      Real(1.304476333084229),                    /* objective */
      Real(1e-7),                                 /* relative_objective_tol */
  };
  Test(lp_config);
}

#ifdef TEST_DOUBLEDOUBLE
TEST_CASE("woodw DoubleDouble", "[woodw DoubleDouble]") {
  typedef mantis::DoubleMantissa<double> Real;
  const TestConfig<Real> lp_config{
      kFeasibleNetlib + std::string("woodw.mps"), /* filename */
      false,                                      /* verbose */
      false,                                      /* tighten_infinite_bounds */
      false,                                      /* print_data */
      Real(1.304476333084229),                    /* objective */
      Real(1e-14),                                /* relative_objective_tol */
  };
  Test(lp_config);
}
#endif  // ifdef TEST_DOUBLEDOUBLE
