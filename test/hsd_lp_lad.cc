/*
 * Copyright (c) 2019 Jack Poulson <jack@hodgestar.com>
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
#define CATCH_CONFIG_MAIN
#include <iostream>
#include <limits>
#include "catch2/catch.hpp"
#include "conic.hpp"
#include "quotient/timer.hpp"

// If defined, the tests are also run in double-double arithmetic.
#define TEST_DOUBLEDOUBLE

// The best solution appears to have residual one norm
//   13.0000000000000000000000000000010,
// leading to a relative residual norm of
//   2.36363636363636363636363636363635e-1.
template <typename Real>
void LeastAbsoluteDeviations(bool verbose, bool print_data,
                             Real relative_residual_tolerance) {
  // A = | 1, 2, 1 |
  //     | 3, 4, 0 |
  //     | 5, 6, 0 |
  //     | 7, 8, 0 |
  //     | 0, 0, 1 |
  const conic::Int height = 5;
  const conic::Int width = 3;
  catamari::CoordinateMatrix<Real> A;
  A.Resize(height, width);
  A.AddEntries(std::vector<catamari::MatrixEntry<Real>>{
      {0, 0, Real(1)},
      {0, 1, Real(2)},
      {0, 2, Real(1)},
      {1, 0, Real(3)},
      {1, 1, Real(4)},
      {2, 0, Real(5)},
      {2, 1, Real(6)},
      {3, 0, Real(7)},
      {3, 1, Real(8)},
      {4, 2, Real(1)},
  });

  // b = | 9  |.
  //     | 10 |
  //     | 11 |
  //     | 12 |
  //     | 13 |
  catamari::BlasMatrix<Real> b;
  b.Resize(height, 1);
  b(0) = Real(9.);
  b(1) = Real(10.);
  b(2) = Real(11.);
  b(3) = Real(12.);
  b(4) = Real(13.);

  if (print_data) {
    std::cout << "ASparse=[\n"
              << A << "];\n"
              << "b=[\n"
              << b << "];\n"
              << std::endl;
  }

  const Real target_relative_residual = 2.36363636363636363636363636363635e-1;

  const conic::hsd_qp::SolveControl<Real> control;

  catamari::BlasMatrix<Real> solution;
  const bool solved = conic::hsd_qp::LeastAbsoluteDeviations(
      A, b.ConstView(), control, &solution);
  REQUIRE(solved);
  if (print_data) {
    std::cout << "solution=[\n" << solution << "];" << std::endl;
  }

  // Measure the original one-norm.
  Real target_one_norm = 0;
  for (conic::Int i = 0; i < height; ++i) {
    target_one_norm += std::abs(b(i));
  }

  // Measure the residual one-norm.
  catamari::BlasMatrix<Real> residual = b;
  catamari::ApplySparse(Real(-1), A, solution.ConstView(), Real(1),
                        &residual.view);
  Real residual_one_norm = 0;
  for (conic::Int i = 0; i < height; ++i) {
    residual_one_norm += std::abs(residual(i));
  }

  const Real relative_residual_norm = residual_one_norm / target_one_norm;
  const Real relative_residual_deviation =
      (relative_residual_norm - target_relative_residual) /
      (target_relative_residual + Real(1));

  REQUIRE(relative_residual_deviation <= relative_residual_tolerance);
}

TEST_CASE("LAD float", "[LAD float]") {
  const bool kVerbose = false;
  const bool kPrintData = false;
  const float relative_residual_tolerance = 1e-3;
  LeastAbsoluteDeviations(kVerbose, kPrintData, relative_residual_tolerance);
}

TEST_CASE("LAD", "[LAD]") {
  const bool kVerbose = false;
  const bool kPrintData = false;
  const double relative_residual_tolerance = 1e-6;
  LeastAbsoluteDeviations(kVerbose, kPrintData, relative_residual_tolerance);
}

#ifdef TEST_DOUBLEDOUBLE
TEST_CASE("LAD DoubleDouble", "[LAD DoubleDouble]") {
  const bool kVerbose = false;
  const bool kPrintData = false;
  typedef mantis::DoubleMantissa<double> DoubleDouble;
  const DoubleDouble relative_residual_tolerance = 1e-13;
  LeastAbsoluteDeviations(kVerbose, kPrintData, relative_residual_tolerance);
}
#endif  // ifdef TEST_DOUBLEDOUBLE
