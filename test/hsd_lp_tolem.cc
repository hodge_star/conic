/*
 * Copyright (c) 2019 Jack Poulson <jack@hodgestar.com>
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
#define CATCH_CONFIG_MAIN
#include <iostream>
#include <limits>
#include "catch2/catch.hpp"
#include "conic.hpp"
#include "quotient/timer.hpp"

// This test suite runs problems posed by David Gale's "The Theory of Linear
// Economic Models".

// If defined, the tests are also run in double-double arithmetic.
#define TEST_DOUBLEDOUBLE

// Tests Chapter 1, Example 5:
//
//   argsup_x { | 2 |' | x_1 | : | 1  3     1 | | x_1 | <= | 4 |, x >= 0 }.
//              | 4 |  | x_2 |   | 2  1       | | x_2 |    | 3 |
//              | 1 |  | x_3 |   |    1  4  1 | | x_3 |    | 3 |
//              | 1 |  | x_4 |                  | x_4 |
//
// The conversion to standard form involves setting:
//
//   c = | -2 |,
//       | -4 |
//       | -1 |
//       | -1 |
//
// and introducing slack variables to replace the inequalities. That is,
//
//   B x <= u
//
// becomes B x + s = u, or
//
//   A | x | = | B  I | | x | = u.
//     | s |            | s |
//
// As argued in The Theory of Linear Economic Models Example 5, the optimal
// solution has value 6.5. This is proven through the primal-dual certificate
//
//       |  1  |       | 11 / 10 |
//   x = |  1  |,  y = |  9 / 20 |.
//       | 1/2 |       |  1 / 4  |
//       |  0  |
//
template <typename Real>
void Chapter1Example5(bool verbose, bool print_data, Real relative_error_tol,
                      Real relative_objective_tol) {
  const conic::Int num_primal = 7;
  const conic::Int num_dual = 3;
  conic::hsd_qp::Problem<Real> problem;

  problem.equality_matrix.Resize(num_dual, num_primal);
  problem.equality_matrix.AddEntries(std::vector<catamari::MatrixEntry<Real>>{
      {0, 0, Real(1)},
      {0, 1, Real(3)},
      {0, 3, Real(1)},
      {1, 0, Real(2)},
      {1, 1, Real(1)},
      {2, 1, Real(1)},
      {2, 2, Real(4)},
      {2, 3, Real(1)},
  });
  for (conic::Int i = 0; i < num_dual; ++i) {
    problem.equality_matrix.AddEntry(i, 4 + i, Real(1));
  }

  problem.equality_rhs.Resize(num_dual, 1);
  problem.equality_rhs(0) = Real(4);
  problem.equality_rhs(1) = Real(3);
  problem.equality_rhs(2) = Real(3);

  problem.linear_objective.Resize(num_primal, 1, Real(0));
  problem.linear_objective(0) = Real(-2);
  problem.linear_objective(1) = Real(-4);
  problem.linear_objective(2) = Real(-1);
  problem.linear_objective(3) = Real(-1);

  problem.quadratic_objective.Resize(num_primal, num_primal);

  problem.FillNorms();
  if (print_data) {
    std::cout << problem << std::endl;
  }

  conic::hsd_qp::SolveControl<Real> control;
  control.output.verbose = verbose;
  conic::hsd_qp::Iterate<Real> certificate;
  const conic::Int num_iterations =
      conic::hsd_qp::Solve(problem, control, &certificate);
  if (print_data) {
    std::cout << certificate << std::endl;
  }

  // Measure the primal residual norm.
  conic::hsd_qp::Residuals<Real> state;
  state.FillRelativeError(problem, certificate);
  std::cout << "Ran for " << num_iterations << " iterations w/ relative error "
            << state.relative_error << std::endl;
  if (verbose) {
    certificate.PrintNorms(std::cout);
    state.PrintRelativeError(std::cout);
  }

  const Real target_objective = Real(-6.5);
  const Real relative_obj_deviation =
      std::abs((state.objective.primal) - target_objective) /
      (std::abs(target_objective) + 1);

  std::cout << "c' (x / tau): " << state.objective.primal << std::endl;
  std::cout << "relative_obj_deviation: " << relative_obj_deviation
            << std::endl;
  state.PrintRelativeError(std::cout);
  REQUIRE(num_iterations < control.max_iterations);
  REQUIRE(certificate.certificate_type == conic::OPTIMALITY_CERTIFICATE);
  REQUIRE(state.relative_error <= relative_error_tol);
  REQUIRE(relative_obj_deviation <= relative_objective_tol);
}

// Tests Chapter 1, Example 6, which corresponds to the transportation problem
// with cost matrix:
//
//           M_1 M_2 M_3
//          ------------
//     P_1 |  1   2   3
//     P_2 |  2   4   6
//
// where P_1 and P_2 are the production centers, with supplies sigma_1 = 4 and
// sigma_2 = 7, respectively, and M_1, M_2, and M_3 are the markets, with
// demands delta_1 = 2, delta_2 = 3, and delta_3 = 5.
//
// The corresponding transportation problem is thus to distribute sufficient
// resources from the production centers to the markets so that market demand
// is met, and production supply not exceeded, with minimal transportation cost.
//
// This produces the Linear Program
//
//   arginf_x { | 1 |' | x_{1, 1} | :
//              | 2 |  | x_{1, 2} |
//              | 3 |  | x_{1, 3} |
//              | 2 |  | x_{2, 1} |
//              | 4 |  | x_{2, 2} |
//              | 6 |  | x_{2, 3} |
//
//       |  1   1   1             | | x_{1, 1} | <= |  4 |, x >= 0 }.
//       |              1   1   1 | | x_{1, 2} |    |  7 |
//       | -1          -1         | | x_{1, 3} |    | -2 |
//       |     -1          -1     | | x_{2, 1} |    | -3 |
//       |         -1          -1 | | x_{2, 2} |    | -5 |
//                                  | x_{2, 3} |
//
// We convert into standard form by appending slack variables which convert the
// inequalities into equalities. We essentially only need to horizontally
// concatenate an identity matrix of order 5 onto the sparse matrix.
//
// As argued in The Theory of Linear Economic Models Example 6, the optimal
// solution has value 34. This is proven through the primal dual certificate
//
//        | 0 |        | 3 |
//        | 0 |        | 0 |
//    x = | 4 |,   y = | 2 |.
//        | 2 |        | 4 |
//        | 3 |        | 6 |
//        | 1 |
//
template <typename Real>
void Chapter1Example6(bool verbose, bool print_data, Real relative_error_tol,
                      Real relative_objective_tol) {
  const conic::Int num_production = 2;
  const conic::Int num_markets = 3;

  conic::BlasMatrix<Real> costs(num_production, num_markets);
  costs(0, 0) = 1;
  costs(0, 1) = 2;
  costs(0, 2) = 3;
  costs(1, 0) = 2;
  costs(1, 1) = 4;
  costs(1, 2) = 6;

  conic::BlasMatrix<Real> supplies(num_production, 1);
  supplies(0) = 4;
  supplies(1) = 7;

  conic::BlasMatrix<Real> demands(num_markets, 1);
  demands(0) = 2;
  demands(1) = 3;
  demands(2) = 5;

  conic::hsd_qp::Problem<Real> problem;
  conic::hsd_qp::TransportationProblem(costs, supplies, demands, &problem);
  if (print_data) {
    std::cout << problem << std::endl;
  }

  conic::hsd_qp::SolveControl<Real> control;
  control.output.verbose = verbose;
  conic::hsd_qp::Iterate<Real> certificate;
  const conic::Int num_iterations =
      conic::hsd_qp::Solve(problem, control, &certificate);
  if (print_data) {
    std::cout << certificate << std::endl;
  }

  // Measure the primal residual norm.
  conic::hsd_qp::Residuals<Real> state;
  state.FillRelativeError(problem, certificate);
  std::cout << "Ran for " << num_iterations << " iterations w/ relative error "
            << state.relative_error << std::endl;
  if (verbose) {
    certificate.PrintNorms(std::cout);
    state.PrintRelativeError(std::cout);
  }

  const Real target_objective = Real(34);
  const Real relative_obj_deviation =
      std::abs((state.objective.primal) - target_objective) /
      (std::abs(target_objective) + 1);

  std::cout << "c' (x / tau): " << state.objective.primal << std::endl;
  std::cout << "relative_obj_deviation: " << relative_obj_deviation
            << std::endl;
  state.PrintRelativeError(std::cout);
  REQUIRE(num_iterations < control.max_iterations);
  REQUIRE(certificate.certificate_type == conic::OPTIMALITY_CERTIFICATE);
  REQUIRE(state.relative_error <= relative_error_tol);
  REQUIRE(relative_obj_deviation <= relative_objective_tol);
}

// Exercise 2 from Chapter 1.
// It is a request for a feasible solution of a transportation problem with
// supplies = [120, 75, 205, 145, 90] and demands = [235, 50, 115, 80, 150].
template <typename Real>
void Chapter1Exercise2(bool verbose, bool print_data, Real relative_error_tol) {
  const conic::Int num_production = 5;
  const conic::Int num_markets = 5;

  conic::BlasMatrix<Real> costs(num_production, num_markets, Real(0));

  conic::BlasMatrix<Real> supplies(num_production, 1);
  supplies(0) = 120;
  supplies(1) = 75;
  supplies(2) = 205;
  supplies(3) = 145;
  supplies(4) = 90;

  conic::BlasMatrix<Real> demands(num_markets, 1);
  demands(0) = 235;
  demands(1) = 50;
  demands(2) = 115;
  demands(3) = 80;
  demands(4) = 150;

  conic::hsd_qp::Problem<Real> problem;
  conic::hsd_qp::TransportationProblem(costs, supplies, demands, &problem);
  if (print_data) {
    std::cout << problem << std::endl;
  }

  conic::hsd_qp::SolveControl<Real> control;
  control.output.verbose = verbose;
  conic::hsd_qp::Iterate<Real> certificate;
  const conic::Int num_iterations =
      conic::hsd_qp::Solve(problem, control, &certificate);
  if (print_data) {
    std::cout << certificate << std::endl;
  }

  // Measure the primal residual norm.
  conic::hsd_qp::Residuals<Real> state;
  state.FillRelativeError(problem, certificate);
  std::cout << "Ran for " << num_iterations << " iterations w/ relative error "
            << state.relative_error << std::endl;
  if (verbose) {
    certificate.PrintNorms(std::cout);
    state.PrintRelativeError(std::cout);
  }

  std::cout << "c' (x / tau): " << state.objective.primal << std::endl;
  state.PrintRelativeError(std::cout);
  REQUIRE(num_iterations < control.max_iterations);
  REQUIRE(certificate.certificate_type == conic::OPTIMALITY_CERTIFICATE);
  REQUIRE(state.relative_error <= relative_error_tol);
}

// Exercise 3 from Chapter 1.
// It is a request to prove infeasibility of the problem:
//
//   argsup_x { |  3 |' | x_1 | : |  2  5 | <= |  3 |, x >= 0 }.
//              | -2 |  | x_2 |   | -3  8 |    | -5 |
//
template <typename Real>
void Chapter1Exercise3(bool verbose, bool print_data) {
  const conic::Int num_primal = 4;
  const conic::Int num_dual = 2;

  conic::hsd_qp::Problem<Real> problem;

  problem.linear_objective.Resize(num_primal, 1, Real(0));
  problem.linear_objective(0) = Real(-3);
  problem.linear_objective(1) = Real(2);

  problem.quadratic_objective.Resize(num_primal, num_primal);

  problem.equality_matrix.Resize(num_dual, num_primal);
  problem.equality_matrix.AddEntries(std::vector<catamari::MatrixEntry<Real>>{
      {0, 0, Real(2)},
      {0, 1, Real(5)},
      {1, 0, Real(-3)},
      {1, 1, Real(8)},
  });
  for (conic::Int i = 0; i < num_dual; ++i) {
    problem.equality_matrix.AddEntry(i, 2 + i, Real(1));
  }

  problem.equality_rhs.Resize(num_dual, 1);
  problem.equality_rhs(0) = Real(3);
  problem.equality_rhs(1) = Real(-5);

  if (print_data) {
    std::cout << problem << std::endl;
  }

  conic::hsd_qp::SolveControl<Real> control;
  control.output.verbose = verbose;
  conic::hsd_qp::Iterate<Real> certificate;
  const conic::Int num_iterations =
      conic::hsd_qp::Solve(problem, control, &certificate);
  if (print_data) {
    std::cout << certificate << std::endl;
  }

  // Measure the primal residual norm.
  std::cout << "Ran for " << num_iterations << " iterations" << std::endl;
  if (verbose) {
    certificate.PrintNorms(std::cout);
  }

  REQUIRE(num_iterations < control.max_iterations);

  REQUIRE(certificate.certificate_type ==
          conic::PRIMAL_INFEASIBILITY_CERTIFICATE);
}

// Exercise 4 from Chapter 1.
// It is a request to prove the unboundedness of the primal LP -- equivalently,
// infeasibility of the dual LP -- of the problem
//
//   argsup_x { | 1 |' | x_1 | : | -3   2 | | x_1 | <= | -1 |, x >= 0 }.
//              | 1 |  | x_2 |   |  1  -1 | | x_2 |    |  2 |
//
template <typename Real>
void Chapter1Exercise4(bool verbose, bool print_data) {
  const conic::Int num_primal = 4;
  const conic::Int num_dual = 2;

  conic::hsd_qp::Problem<Real> problem;

  problem.linear_objective.Resize(num_primal, 1, Real(0));
  problem.linear_objective(0) = Real(-1);
  problem.linear_objective(1) = Real(-1);

  problem.quadratic_objective.Resize(num_primal, num_primal);

  problem.equality_matrix.Resize(num_dual, num_primal);
  problem.equality_matrix.AddEntry(0, 0, Real(-3));
  problem.equality_matrix.AddEntry(0, 1, Real(2));
  problem.equality_matrix.AddEntry(1, 0, Real(1));
  problem.equality_matrix.AddEntry(1, 1, Real(-1));
  for (conic::Int i = 0; i < num_dual; ++i) {
    problem.equality_matrix.AddEntry(i, 2 + i, Real(1));
  }

  problem.equality_rhs.Resize(num_dual, 1);
  problem.equality_rhs(0) = Real(-1);
  problem.equality_rhs(1) = Real(2);

  if (print_data) {
    std::cout << problem << std::endl;
  }

  conic::hsd_qp::SolveControl<Real> control;
  control.output.verbose = verbose;
  conic::hsd_qp::Iterate<Real> certificate;
  const conic::Int num_iterations =
      conic::hsd_qp::Solve(problem, control, &certificate);
  if (print_data) {
    std::cout << certificate << std::endl;
  }

  // Measure the primal residual norm.
  std::cout << "Ran for " << num_iterations << " iterations" << std::endl;
  if (verbose) {
    certificate.PrintNorms(std::cout);
  }

  REQUIRE(num_iterations < control.max_iterations);

  REQUIRE(certificate.certificate_type ==
          conic::DUAL_INFEASIBILITY_CERTIFICATE);
}

// Tests Chapter 1, Exercise 12:
//
//   argsup_x { 1' x : | 1  1       | | x_1 | <= | 3 |, x >= 0 }.
//                     |       1  1 | | x_2 |    | 1 |
//                     |    1  1    | | x_3 |    | 1 |
//                     | 1     1    | | x_4 |    | 1 |
//                     |       1  1 |            | 3 |
//
// It is argued that there is an optimal solution x = [1; 1; 0; 1], which
// therefore implies the optimal objective value -- of the minimization problem
// -- of -3.
//
template <typename Real>
void Chapter1Exercise12(bool verbose, bool print_data, Real relative_error_tol,
                        Real relative_objective_tol) {
  const conic::Int num_primal = 9;
  const conic::Int num_dual = 5;
  conic::hsd_qp::Problem<Real> problem;

  problem.equality_matrix.Resize(num_dual, num_primal);
  problem.equality_matrix.AddEntries(std::vector<catamari::MatrixEntry<Real>>{
      {0, 0, Real(1)},
      {0, 1, Real(1)},
      {1, 2, Real(1)},
      {1, 3, Real(1)},
      {2, 1, Real(1)},
      {2, 2, Real(1)},
      {3, 0, Real(1)},
      {3, 2, Real(1)},
      {4, 2, Real(1)},
      {4, 3, Real(1)},
  });
  for (conic::Int i = 0; i < num_dual; ++i) {
    problem.equality_matrix.AddEntry(i, 4 + i, Real(1));
  }

  problem.equality_rhs.Resize(num_dual, 1);
  problem.equality_rhs(0) = Real(3);
  problem.equality_rhs(1) = Real(1);
  problem.equality_rhs(2) = Real(1);
  problem.equality_rhs(3) = Real(1);
  problem.equality_rhs(4) = Real(3);

  problem.linear_objective.Resize(num_primal, 1, Real(0));
  for (conic::Int i = 0; i < 4; ++i) {
    problem.linear_objective(i) = Real(-1);
  }

  problem.quadratic_objective.Resize(num_primal, num_primal);

  problem.FillNorms();
  if (print_data) {
    std::cout << problem << std::endl;
  }

  conic::hsd_qp::SolveControl<Real> control;
  control.output.verbose = verbose;
  conic::hsd_qp::Iterate<Real> certificate;
  const conic::Int num_iterations =
      conic::hsd_qp::Solve(problem, control, &certificate);
  if (print_data) {
    std::cout << certificate << std::endl;
  }

  // Measure the primal residual norm.
  conic::hsd_qp::Residuals<Real> state;
  state.FillRelativeError(problem, certificate);
  std::cout << "Ran for " << num_iterations << " iterations w/ relative error "
            << state.relative_error << std::endl;
  if (verbose) {
    certificate.PrintNorms(std::cout);
    state.PrintRelativeError(std::cout);
  }

  const Real target_objective = Real(-3);
  const Real relative_obj_deviation =
      std::abs((state.objective.primal) - target_objective) /
      (std::abs(target_objective) + 1);

  std::cout << "c' (x / tau): " << state.objective.primal << std::endl;
  std::cout << "relative_obj_deviation: " << relative_obj_deviation
            << std::endl;
  state.PrintRelativeError(std::cout);
  REQUIRE(num_iterations < control.max_iterations);
  REQUIRE(certificate.certificate_type == conic::OPTIMALITY_CERTIFICATE);
  REQUIRE(state.relative_error <= relative_error_tol);
  REQUIRE(relative_obj_deviation <= relative_objective_tol);
}

// Tests Chapter 1, Exercise 13:
//
//   argsup_x { |  1 |' | x_1 | : | -2   1 | | x_1 | <= | 2 |, x >= 0 }.
//              | -1 |  | x_2 |   |  1  -2 | | x_2 |    | 2 |
//                                |  1   1 |            | 5 |
//
// It is claimed that the optimal solution is x = [4; 1], which therefore
// implies the optimal objective value -- of the minimization problem -- of
// -3.
//
template <typename Real>
void Chapter1Exercise13(bool verbose, bool print_data, Real relative_error_tol,
                        Real relative_objective_tol) {
  const conic::Int num_primal = 5;
  const conic::Int num_dual = 3;
  conic::hsd_qp::Problem<Real> problem;

  problem.equality_matrix.Resize(num_dual, num_primal);
  problem.equality_matrix.AddEntry(0, 0, Real(-2));
  problem.equality_matrix.AddEntry(0, 1, Real(1));
  problem.equality_matrix.AddEntry(1, 0, Real(1));
  problem.equality_matrix.AddEntry(1, 1, Real(-2));
  problem.equality_matrix.AddEntry(2, 0, Real(1));
  problem.equality_matrix.AddEntry(2, 1, Real(1));
  for (conic::Int i = 0; i < num_dual; ++i) {
    problem.equality_matrix.AddEntry(i, 2 + i, Real(1));
  }

  problem.equality_rhs.Resize(num_dual, 1);
  problem.equality_rhs(0) = Real(2);
  problem.equality_rhs(1) = Real(2);
  problem.equality_rhs(2) = Real(5);

  problem.linear_objective.Resize(num_primal, 1, Real(0));
  problem.linear_objective(0) = Real(-1);
  problem.linear_objective(1) = Real(1);

  problem.quadratic_objective.Resize(num_primal, num_primal);

  problem.FillNorms();
  if (print_data) {
    std::cout << problem << std::endl;
  }

  conic::hsd_qp::SolveControl<Real> control;
  control.output.verbose = verbose;
  conic::hsd_qp::Iterate<Real> certificate;
  const conic::Int num_iterations =
      conic::hsd_qp::Solve(problem, control, &certificate);
  if (print_data) {
    std::cout << certificate << std::endl;
  }

  // Measure the primal residual norm.
  conic::hsd_qp::Residuals<Real> state;
  state.FillRelativeError(problem, certificate);
  std::cout << "Ran for " << num_iterations << " iterations w/ relative error "
            << state.relative_error << std::endl;
  if (verbose) {
    certificate.PrintNorms(std::cout);
    state.PrintRelativeError(std::cout);
  }

  const Real target_objective = Real(-3);
  const Real relative_obj_deviation =
      std::abs((state.objective.primal) - target_objective) /
      (std::abs(target_objective) + 1);

  std::cout << "c' (x / tau): " << state.objective.primal << std::endl;
  std::cout << "relative_obj_deviation: " << relative_obj_deviation
            << std::endl;
  state.PrintRelativeError(std::cout);
  REQUIRE(num_iterations < control.max_iterations);
  REQUIRE(certificate.certificate_type == conic::OPTIMALITY_CERTIFICATE);
  REQUIRE(state.relative_error <= relative_error_tol);
  REQUIRE(relative_obj_deviation <= relative_objective_tol);
}

// Tests Chapter 1, Exercise 14: a transportation problem with cost matrix
//
//        M_1 M_2 M_3 M_4
//        ---------------,
//   P_1 | 4   4   9   3
//   P_2 | 3   5   8   8
//   P_3 | 2   6   5   7
//
// supplies = [3, 5, 7], and demands = [2, 5, 4, 4]. It is claimed that one of
// the optimal solutions is:
//
//   | 0  0  0  3 |.
//   | 0  5  0  0 |
//   | 2  0  4  1 |
//
// Its corresponding value would then be:
//
//   3 * 3 + 5 * 5 + 2 * 2 + 4 * 5 + 1 * 7 = 65.
//
template <typename Real>
void Chapter1Exercise14(bool verbose, bool print_data, Real relative_error_tol,
                        Real relative_objective_tol) {
  const conic::Int num_producers = 3;
  const conic::Int num_markets = 4;

  conic::BlasMatrix<Real> costs(num_producers, num_markets);
  costs(0, 0) = 4;
  costs(0, 1) = 4;
  costs(0, 2) = 9;
  costs(0, 3) = 3;
  costs(1, 0) = 3;
  costs(1, 1) = 5;
  costs(1, 2) = 8;
  costs(1, 3) = 8;
  costs(2, 0) = 2;
  costs(2, 1) = 6;
  costs(2, 2) = 5;
  costs(2, 3) = 7;

  conic::BlasMatrix<Real> supplies(num_producers, 1);
  supplies(0) = 3;
  supplies(1) = 5;
  supplies(2) = 7;

  conic::BlasMatrix<Real> demands(num_markets, 1);
  demands(0) = 2;
  demands(1) = 5;
  demands(2) = 4;
  demands(3) = 4;

  conic::hsd_qp::Problem<Real> problem;
  conic::hsd_qp::TransportationProblem(costs, supplies, demands, &problem);
  if (print_data) {
    std::cout << problem << std::endl;
  }

  conic::hsd_qp::SolveControl<Real> control;
  control.output.verbose = verbose;
  conic::hsd_qp::Iterate<Real> certificate;
  const conic::Int num_iterations =
      conic::hsd_qp::Solve(problem, control, &certificate);
  if (print_data) {
    std::cout << certificate << std::endl;
  }

  // Measure the primal residual norm.
  conic::hsd_qp::Residuals<Real> state;
  state.FillRelativeError(problem, certificate);
  std::cout << "Ran for " << num_iterations << " iterations w/ relative error "
            << state.relative_error << std::endl;
  if (verbose) {
    certificate.PrintNorms(std::cout);
    state.PrintRelativeError(std::cout);
  }

  const Real target_objective = Real(65);
  const Real relative_obj_deviation =
      std::abs((state.objective.primal) - target_objective) /
      (std::abs(target_objective) + 1);

  std::cout << "c' (x / tau): " << state.objective.primal << std::endl;
  std::cout << "relative_obj_deviation: " << relative_obj_deviation
            << std::endl;
  state.PrintRelativeError(std::cout);
  REQUIRE(num_iterations < control.max_iterations);
  REQUIRE(certificate.certificate_type == conic::OPTIMALITY_CERTIFICATE);
  REQUIRE(state.relative_error <= relative_error_tol);
  REQUIRE(relative_obj_deviation <= relative_objective_tol);
}

// We solve the optimal assignment problem of Chapter 1, Exercise 19, which has
// ratings matrix:
//
//        J_1 J_2 J_3 J_4 J_5
//        -------------------
//   W_1 | 12  9  10   3   8
//   W_2 |  6  6   2   2   9
//   W_3 |  6  8  10  11   9
//   W_4 |  6  3   4   1   1
//   W_5 | 11  1  10   9  12
//
// It is argued that the optimal solution is:
//
//   | 1  0  0  0  0 |,
//   | 0  0  0  0  1 |
//   | 0  0  0  1  0 |
//   | 0  1  0  0  0 |
//   | 0  0  1  0  0 |
//
// which has combined rating:
//
//   12 + 9 + 11 + 3 + 10 = 45.
//
// The minimization problem will therefore have optimal value -45.
//
// We note that there exist many optimal solutions, including those with
// non-integral solutions. We could post-process the IPM solution to find an
// integral solution, but we do not do so here.
//
template <typename Real>
void Chapter1Exercise19(bool verbose, bool print_data, Real relative_error_tol,
                        Real relative_objective_tol) {
  const conic::Int num_workers = 5;
  const conic::Int num_tasks = 5;

  conic::BlasMatrix<Real> ratings(num_workers, num_tasks);
  ratings(0, 0) = 12;
  ratings(0, 1) = 9;
  ratings(0, 2) = 10;
  ratings(0, 3) = 3;
  ratings(0, 4) = 8;
  ratings(1, 0) = 6;
  ratings(1, 1) = 6;
  ratings(1, 2) = 2;
  ratings(1, 3) = 2;
  ratings(1, 4) = 9;
  ratings(2, 0) = 6;
  ratings(2, 1) = 8;
  ratings(2, 2) = 10;
  ratings(2, 3) = 11;
  ratings(2, 4) = 9;
  ratings(3, 0) = 6;
  ratings(3, 1) = 3;
  ratings(3, 2) = 4;
  ratings(3, 3) = 1;
  ratings(3, 4) = 1;
  ratings(4, 0) = 11;
  ratings(4, 1) = 1;
  ratings(4, 2) = 10;
  ratings(4, 3) = 9;
  ratings(4, 4) = 12;

  conic::hsd_qp::Problem<Real> problem;
  conic::hsd_qp::OptimalAssignment(ratings, &problem);
  if (print_data) {
    std::cout << problem << std::endl;
  }

  conic::hsd_qp::SolveControl<Real> control;
  control.output.verbose = verbose;
  conic::hsd_qp::Iterate<Real> certificate;
  const conic::Int num_iterations =
      conic::hsd_qp::Solve(problem, control, &certificate);
  if (print_data) {
    std::cout << certificate << std::endl;
  }

  // Measure the primal residual norm.
  conic::hsd_qp::Residuals<Real> state;
  state.FillRelativeError(problem, certificate);
  std::cout << "Ran for " << num_iterations << " iterations w/ relative error "
            << state.relative_error << std::endl;
  if (verbose) {
    certificate.PrintNorms(std::cout);
    state.PrintRelativeError(std::cout);
  }

  const Real target_objective = Real(-45);
  const Real relative_obj_deviation =
      std::abs((state.objective.primal) - target_objective) /
      (std::abs(target_objective) + 1);

  std::cout << "c' (x / tau): " << state.objective.primal << std::endl;
  std::cout << "relative_obj_deviation: " << relative_obj_deviation
            << std::endl;
  state.PrintRelativeError(std::cout);
  REQUIRE(num_iterations < control.max_iterations);
  REQUIRE(certificate.certificate_type == conic::OPTIMALITY_CERTIFICATE);
  REQUIRE(state.relative_error <= relative_error_tol);
  REQUIRE(relative_obj_deviation <= relative_objective_tol);
}

// We solve the 'evasion' matrix game with payoff |x - y| over the integers
// 1, 2, 3, 4.
//
//       | 0 1 2 3 |
//   A = | 1 0 1 2 |.
//       | 2 1 0 1 |
//       | 3 2 1 0 |
//
// One optimal primal mixed strategy is (1 / 2, 0, 0, 1 / 2) and optimal dual
// mixed strategy is (0, 1 / 2, 1 / 2, 0). The resulting payoff would be:
//
//   1 / 4 sum(| 1, 2 |) = 3 / 2.
//             | 2, 1 |
//
template <typename Real>
void Chapter7Example1(bool use_gale_kuhn_tucker_approach, bool verbose,
                      bool print_data, Real relative_value_tol) {
  const conic::Int num_rows = 4;
  const conic::Int num_columns = 4;
  catamari::CoordinateMatrix<Real> payoff_matrix;
  payoff_matrix.Resize(num_rows, num_columns);
  payoff_matrix.ReserveEntryAdditions(num_rows * num_columns);
  for (conic::Int i = 0; i < num_rows; ++i) {
    for (conic::Int j = 0; j < num_columns; ++j) {
      payoff_matrix.QueueEntryAddition(i, j, std::abs(Real(i) - Real(j)));
    }
  }
  payoff_matrix.FlushEntryQueues();

  const Real target_value = Real(3) / Real(2);

  conic::hsd_qp::SolveControl<Real> control;
  control.output.verbose = verbose;
  catamari::BlasMatrix<Real> row_minimization_strategy;
  catamari::BlasMatrix<Real> column_maximization_strategy;
  const Real value = conic::hsd_qp::MatrixGame(
      payoff_matrix, control, use_gale_kuhn_tucker_approach,
      &row_minimization_strategy, &column_maximization_strategy);

  const Real relative_value_err =
      std::abs(value - target_value) / (std::abs(target_value) + 1);
  REQUIRE(relative_value_err <= relative_value_tol);

  Real expected_payoff = 0;
  for (const auto& entry : payoff_matrix.Entries()) {
    expected_payoff += row_minimization_strategy(entry.row) * entry.value *
                       column_maximization_strategy(entry.column);
  }
  const Real expected_payoff_err =
      std::abs(expected_payoff - target_value) / (std::abs(target_value) + 1);
  REQUIRE(expected_payoff_err <= relative_value_tol);
}

// The "five-step silent dual" problem of Example 2 of Chapter 7 has payoff:
//
//       |  0,   3,  7, 11,  20 |
//       | -3,   0, -1,  2,   5 |
//   A = | -7,   1,  0, -7,  -5 |,
//       | -11, -2,  7,  0, -15 |
//       | -20, -5,  5, 15,   0 |
//
// which is transposed from the original formulation due to our conventions.
// It is worth noting that the transposed problem can take on a different
// solution, as (anti-)symmetry is broken when adding the positive shift.
//
// It is argued that the only valid mixed strategies are:
//
//   (0, 5 / 11, 5 / 11, 0, 1 / 11).
//
// The resulting value is -- for any symmetric game -- zero.
//
template <typename Real>
void Chapter7Example2(bool use_gale_kuhn_tucker_approach, bool verbose,
                      bool print_data, Real relative_value_tol) {
  const conic::Int num_rows = 5;
  const conic::Int num_columns = 5;
  catamari::CoordinateMatrix<Real> payoff_matrix;
  payoff_matrix.Resize(num_rows, num_columns);

  // Fill the strictly upper triangle.
  payoff_matrix.AddEntries(std::vector<catamari::MatrixEntry<Real>>{
      {0, 1, Real(3)},
      {0, 2, Real(7)},
      {0, 3, Real(11)},
      {0, 4, Real(20)},
      {1, 2, Real(-1)},
      {1, 3, Real(2)},
      {1, 4, Real(5)},
      {2, 3, Real(-7)},
      {2, 4, Real(-5)},
      {3, 4, Real(-15)},
  });

  // Fill the strictly lower triangle.
  payoff_matrix.AddEntries(std::vector<catamari::MatrixEntry<Real>>{
      {1, 0, Real(-3)},
      {2, 0, Real(-7)},
      {3, 0, Real(-11)},
      {4, 0, Real(-20)},
      {2, 1, Real(1)},
      {3, 1, Real(-2)},
      {4, 1, Real(-5)},
      {3, 2, Real(7)},
      {4, 2, Real(5)},
      {4, 3, Real(15)},
  });

  const Real target_value = Real(0);

  catamari::BlasMatrix<Real> target_solution(num_rows, 1);
  target_solution(0) = Real(0);
  target_solution(1) = Real(5) / Real(11);
  target_solution(2) = Real(5) / Real(11);
  target_solution(3) = Real(0);
  target_solution(4) = Real(1) / Real(11);

  conic::hsd_qp::SolveControl<Real> control;
  control.output.verbose = verbose;
  catamari::BlasMatrix<Real> row_minimization_strategy;
  catamari::BlasMatrix<Real> column_maximization_strategy;
  const Real value = conic::hsd_qp::MatrixGame(
      payoff_matrix, control, use_gale_kuhn_tucker_approach,
      &row_minimization_strategy, &column_maximization_strategy);

  const Real relative_value_err =
      std::abs(value - target_value) / (std::abs(target_value) + 1);
  REQUIRE(relative_value_err <= relative_value_tol);

  Real expected_payoff = 0;
  for (const auto& entry : payoff_matrix.Entries()) {
    expected_payoff += row_minimization_strategy(entry.row) * entry.value *
                       column_maximization_strategy(entry.column);
  }
  const Real expected_payoff_err =
      std::abs(expected_payoff - target_value) / (std::abs(target_value) + 1);
  REQUIRE(expected_payoff_err <= relative_value_tol);

  // Test the row minimization strategy.
  for (conic::Int i = 0; i < num_rows; ++i) {
    const Real relative_err =
        std::abs(row_minimization_strategy(i) - target_solution(i)) /
        (std::abs(target_solution(i)) + 1);
    REQUIRE(relative_err <= relative_value_tol);
  }

  // Test the column maximization strategy.
  for (conic::Int i = 0; i < num_rows; ++i) {
    const Real relative_err =
        std::abs(column_maximization_strategy(i) - target_solution(i)) /
        (std::abs(target_solution(i)) + 1);
    REQUIRE(relative_err <= relative_value_tol);
  }
}

// The "rock/paper/scissors" game of Example 3 of Chapter 7 has payoff:
//
//      |  0, -1,  1 |
//  A = |  1,  0, -1 |
//      | -1,  1,  0 |
//
// which is transposed from the original formulation due to our conventions.
//
// It is argued that the only valid mixed strategies are:
//
//   (1 / 3, 1 / 3, 1 / 3).
//
// The resulting value is -- for any symmetric game -- zero.
//
template <typename Real>
void Chapter7Example3(bool use_gale_kuhn_tucker_approach, bool verbose,
                      bool print_data, Real relative_value_tol) {
  const conic::Int num_rows = 3;
  const conic::Int num_columns = 3;
  catamari::CoordinateMatrix<Real> payoff_matrix;
  payoff_matrix.Resize(num_rows, num_columns);

  // Fill the strictly upper triangle.
  payoff_matrix.AddEntry(0, 1, Real(-1));
  payoff_matrix.AddEntry(0, 2, Real(1));
  payoff_matrix.AddEntry(1, 2, Real(-1));

  // Fill the strictly lower triangle.
  payoff_matrix.AddEntry(1, 0, Real(1));
  payoff_matrix.AddEntry(2, 0, Real(-1));
  payoff_matrix.AddEntry(2, 1, Real(1));

  const Real target_value = Real(0);
  catamari::BlasMatrix<Real> target_solution(num_rows, 1, Real(1) / Real(3));

  conic::hsd_qp::SolveControl<Real> control;
  control.output.verbose = verbose;
  catamari::BlasMatrix<Real> row_minimization_strategy;
  catamari::BlasMatrix<Real> column_maximization_strategy;
  const Real value = conic::hsd_qp::MatrixGame(
      payoff_matrix, control, use_gale_kuhn_tucker_approach,
      &row_minimization_strategy, &column_maximization_strategy);

  const Real relative_value_err =
      std::abs(value - target_value) / (std::abs(target_value) + 1);
  REQUIRE(relative_value_err <= relative_value_tol);

  Real expected_payoff = 0;
  for (const auto& entry : payoff_matrix.Entries()) {
    expected_payoff += row_minimization_strategy(entry.row) * entry.value *
                       column_maximization_strategy(entry.column);
  }
  const Real expected_payoff_err =
      std::abs(expected_payoff - target_value) / (std::abs(target_value) + 1);
  REQUIRE(expected_payoff_err <= relative_value_tol);

  // Test the row minimization strategy.
  for (conic::Int i = 0; i < num_rows; ++i) {
    const Real relative_err =
        std::abs(row_minimization_strategy(i) - target_solution(i)) /
        (std::abs(target_solution(i)) + 1);
    REQUIRE(relative_err <= relative_value_tol);
  }

  // Test the column maximization strategy.
  for (conic::Int i = 0; i < num_rows; ++i) {
    const Real relative_err =
        std::abs(column_maximization_strategy(i) - target_solution(i)) /
        (std::abs(target_solution(i)) + 1);
    REQUIRE(relative_err <= relative_value_tol);
  }
}

// The unnamed game of Example 4 of Chapter 7 has payoff:
//
//      |  0,  1,  0, -1 |
//  A = | -1,  0,  1,  0 |,
//      |  0, -1,  0,  1 |
//      |  1,  0, -1,  0 |
//
// which is transposed from the original formulation due to our conventions.
//
// It is argued that there are many valid mixed strategies, such as:
//
//   (1 / 4, 1 / 4, 1 / 4, 1 / 4).
//
// The resulting value is -- for any symmetric game -- zero.
//
template <typename Real>
void Chapter7Example4(bool use_gale_kuhn_tucker_approach, bool verbose,
                      bool print_data, Real relative_value_tol) {
  const conic::Int num_rows = 4;
  const conic::Int num_columns = 4;
  catamari::CoordinateMatrix<Real> payoff_matrix;
  payoff_matrix.Resize(num_rows, num_columns);

  // Fill the strictly upper triangle.
  payoff_matrix.AddEntry(0, 1, Real(1));
  payoff_matrix.AddEntry(0, 3, Real(-1));
  payoff_matrix.AddEntry(1, 2, Real(1));
  payoff_matrix.AddEntry(2, 3, Real(1));

  // Fill the strictly lower triangle.
  payoff_matrix.AddEntry(1, 0, Real(-1));
  payoff_matrix.AddEntry(3, 0, Real(1));
  payoff_matrix.AddEntry(2, 1, Real(-1));
  payoff_matrix.AddEntry(3, 2, Real(-1));

  const Real target_value = Real(0);

  conic::hsd_qp::SolveControl<Real> control;
  control.output.verbose = verbose;
  catamari::BlasMatrix<Real> row_minimization_strategy;
  catamari::BlasMatrix<Real> column_maximization_strategy;
  const Real value = conic::hsd_qp::MatrixGame(
      payoff_matrix, control, use_gale_kuhn_tucker_approach,
      &row_minimization_strategy, &column_maximization_strategy);

  const Real relative_value_err =
      std::abs(value - target_value) / (std::abs(target_value) + 1);
  REQUIRE(relative_value_err <= relative_value_tol);

  Real expected_payoff = 0;
  for (const auto& entry : payoff_matrix.Entries()) {
    expected_payoff += row_minimization_strategy(entry.row) * entry.value *
                       column_maximization_strategy(entry.column);
  }
  const Real expected_payoff_err =
      std::abs(expected_payoff - target_value) / (std::abs(target_value) + 1);
  REQUIRE(expected_payoff_err <= relative_value_tol);
}

// The "baseball" game of Chapter 7, Section 9, has payoff:
//
//
//      |  2, -1, -1 |
//  P = | -1,  2, -1 |.
//      | -1, -1,  1 |
//
// The optimal value of the game is not provided in the chapter, but one can
// verify that there is an optimal value of -1 / 7, with primal and dual mixed
// strategies:
//
//   (2 / 7, 2 / 7, 3 / 7).
//
template <typename Real>
void Chapter7Baseball(bool use_gale_kuhn_tucker_approach, bool verbose,
                      bool print_data, Real relative_value_tol) {
  const conic::Int num_rows = 3;
  const conic::Int num_columns = 3;
  catamari::CoordinateMatrix<Real> payoff_matrix;
  payoff_matrix.Resize(num_rows, num_columns);

  payoff_matrix.AddEntry(0, 0, Real(2));
  payoff_matrix.AddEntry(0, 1, Real(-1));
  payoff_matrix.AddEntry(0, 2, Real(-1));
  payoff_matrix.AddEntry(1, 0, Real(-1));
  payoff_matrix.AddEntry(1, 1, Real(2));
  payoff_matrix.AddEntry(1, 2, Real(-1));
  payoff_matrix.AddEntry(2, 0, Real(-1));
  payoff_matrix.AddEntry(2, 1, Real(-1));
  payoff_matrix.AddEntry(2, 2, Real(1));

  const Real target_value = Real(-1) / Real(7);

  catamari::BlasMatrix<Real> target_solution(num_rows, 1);
  target_solution(0) = Real(2) / Real(7);
  target_solution(1) = Real(2) / Real(7);
  target_solution(2) = Real(3) / Real(7);

  conic::hsd_qp::SolveControl<Real> control;
  control.output.verbose = verbose;
  catamari::BlasMatrix<Real> row_minimization_strategy;
  catamari::BlasMatrix<Real> column_maximization_strategy;
  const Real value = conic::hsd_qp::MatrixGame(
      payoff_matrix, control, use_gale_kuhn_tucker_approach,
      &row_minimization_strategy, &column_maximization_strategy);

  const Real relative_value_err =
      std::abs(value - target_value) / (std::abs(target_value) + 1);
  REQUIRE(relative_value_err <= relative_value_tol);

  Real expected_payoff = 0;
  for (const auto& entry : payoff_matrix.Entries()) {
    expected_payoff += row_minimization_strategy(entry.row) * entry.value *
                       column_maximization_strategy(entry.column);
  }
  const Real expected_payoff_err =
      std::abs(expected_payoff - target_value) / (std::abs(target_value) + 1);
  REQUIRE(expected_payoff_err <= relative_value_tol);

  // Test the row minimization strategy.
  for (conic::Int i = 0; i < num_rows; ++i) {
    const Real relative_err =
        std::abs(row_minimization_strategy(i) - target_solution(i)) /
        (std::abs(target_solution(i)) + 1);
    REQUIRE(relative_err <= relative_value_tol);
  }

  // Test the column maximization strategy.
  for (conic::Int i = 0; i < num_rows; ++i) {
    const Real relative_err =
        std::abs(column_maximization_strategy(i) - target_solution(i)) /
        (std::abs(target_solution(i)) + 1);
    REQUIRE(relative_err <= relative_value_tol);
  }
}

TEST_CASE("Chapter1Example5 float", "[Chapter1Example5 float]") {
  const bool kVerbose = false;
  const bool kPrintData = false;
  const float relative_error_tol = 1e-3;
  const float relative_objective_tol = 1e-2;
  Chapter1Example5(kVerbose, kPrintData, relative_error_tol,
                   relative_objective_tol);
}

TEST_CASE("Chapter1Example5 double", "[Chapter1Example5 double]") {
  const bool kVerbose = false;
  const bool kPrintData = false;
  const double relative_error_tol = 1e-7;
  const double relative_objective_tol = 1e-4;
  Chapter1Example5(kVerbose, kPrintData, relative_error_tol,
                   relative_objective_tol);
}

#ifdef TEST_DOUBLEDOUBLE
TEST_CASE("Chapter1Example5 DoubleDouble", "[Chapter1Example5 DoubleDouble]") {
  const bool kVerbose = false;
  const bool kPrintData = false;
  typedef mantis::DoubleMantissa<double> DoubleDouble;
  const DoubleDouble relative_error_tol = 1e-14;
  const DoubleDouble relative_objective_tol = 1e-10;
  Chapter1Example5(kVerbose, kPrintData, relative_error_tol,
                   relative_objective_tol);
}
#endif  // ifdef TEST_DOUBLEDOUBLE

TEST_CASE("Chapter1Example6 float", "[Chapter1Example6 float]") {
  const bool kVerbose = false;
  const bool kPrintData = false;
  const float relative_error_tol = 1e-3;
  const float relative_objective_tol = 1e-2;
  Chapter1Example6(kVerbose, kPrintData, relative_error_tol,
                   relative_objective_tol);
}

TEST_CASE("Chapter1Example6 double", "[Chapter1Example6 double]") {
  const bool kVerbose = false;
  const bool kPrintData = false;
  const double relative_error_tol = 1e-7;
  const double relative_objective_tol = 1e-4;
  Chapter1Example6(kVerbose, kPrintData, relative_error_tol,
                   relative_objective_tol);
}

#ifdef TEST_DOUBLEDOUBLE
TEST_CASE("Chapter1Example6 DoubleDouble", "[Chapter1Example6 DoubleDouble]") {
  const bool kVerbose = false;
  const bool kPrintData = false;
  typedef mantis::DoubleMantissa<double> DoubleDouble;
  const DoubleDouble relative_error_tol = 1e-14;
  const DoubleDouble relative_objective_tol = 1e-10;
  Chapter1Example6(kVerbose, kPrintData, relative_error_tol,
                   relative_objective_tol);
}
#endif  // ifdef TEST_DOUBLEDOUBLE

TEST_CASE("Chapter1Exercise2 float", "[Chapter1Exercise2 float]") {
  const bool kVerbose = false;
  const bool kPrintData = false;
  const float relative_error_tol = 1e-3;
  Chapter1Exercise2(kVerbose, kPrintData, relative_error_tol);
}

TEST_CASE("Chapter1Exercise2 double", "[Chapter1Exercise2 double]") {
  const bool kVerbose = false;
  const bool kPrintData = false;
  const double relative_error_tol = 1e-7;
  Chapter1Exercise2(kVerbose, kPrintData, relative_error_tol);
}

#ifdef TEST_DOUBLEDOUBLE
TEST_CASE("Chapter1Exercise2 DoubleDouble",
          "[Chapter1Exercise2 DoubleDouble]") {
  const bool kVerbose = false;
  const bool kPrintData = false;
  typedef mantis::DoubleMantissa<double> DoubleDouble;
  const DoubleDouble relative_error_tol = 1e-14;
  Chapter1Exercise2(kVerbose, kPrintData, relative_error_tol);
}
#endif  // ifdef TEST_DOUBLEDOUBLE

TEST_CASE("Chapter1Exercise3 float", "[Chapter1Exercise3 float]") {
  const bool kVerbose = false;
  const bool kPrintData = false;
  Chapter1Exercise3<float>(kVerbose, kPrintData);
}

TEST_CASE("Chapter1Exercise3 double", "[Chapter1Exercise3 double]") {
  const bool kVerbose = false;
  const bool kPrintData = false;
  Chapter1Exercise3<double>(kVerbose, kPrintData);
}

#ifdef TEST_DOUBLEDOUBLE
TEST_CASE("Chapter1Exercise3 DoubleDouble",
          "[Chapter1Exercise3 DoubleDouble]") {
  const bool kVerbose = false;
  const bool kPrintData = false;
  typedef mantis::DoubleMantissa<double> DoubleDouble;
  Chapter1Exercise3<DoubleDouble>(kVerbose, kPrintData);
}
#endif  // ifdef TEST_DOUBLEDOUBLE

TEST_CASE("Chapter1Exercise4 float", "[Chapter1Exercise4 float]") {
  const bool kVerbose = false;
  const bool kPrintData = false;
  Chapter1Exercise4<float>(kVerbose, kPrintData);
}

TEST_CASE("Chapter1Exercise4 double", "[Chapter1Exercise4 double]") {
  const bool kVerbose = false;
  const bool kPrintData = false;
  Chapter1Exercise4<double>(kVerbose, kPrintData);
}

#ifdef TEST_DOUBLEDOUBLE
TEST_CASE("Chapter1Exercise4 DoubleDouble",
          "[Chapter1Exercise4 DoubleDouble]") {
  const bool kVerbose = false;
  const bool kPrintData = false;
  typedef mantis::DoubleMantissa<double> DoubleDouble;
  Chapter1Exercise4<DoubleDouble>(kVerbose, kPrintData);
}
#endif  // ifdef TEST_DOUBLEDOUBLE

TEST_CASE("Chapter1Exercise12 float", "[Chapter1Exercise12 float]") {
  const bool kVerbose = false;
  const bool kPrintData = false;
  const float relative_error_tol = 1e-3;
  const float relative_objective_tol = 1e-2;
  Chapter1Exercise12(kVerbose, kPrintData, relative_error_tol,
                     relative_objective_tol);
}

TEST_CASE("Chapter1Exercise12 double", "[Chapter1Exercise12 double]") {
  const bool kVerbose = false;
  const bool kPrintData = false;
  const double relative_error_tol = 1e-7;
  const double relative_objective_tol = 1e-4;
  Chapter1Exercise12(kVerbose, kPrintData, relative_error_tol,
                     relative_objective_tol);
}

#ifdef TEST_DOUBLEDOUBLE
TEST_CASE("Chapter1Exercise12 DoubleDouble",
          "[Chapter1Exercise12 DoubleDouble]") {
  const bool kVerbose = false;
  const bool kPrintData = false;
  typedef mantis::DoubleMantissa<double> DoubleDouble;
  const DoubleDouble relative_error_tol = 1e-14;
  const DoubleDouble relative_objective_tol = 1e-10;
  Chapter1Exercise12(kVerbose, kPrintData, relative_error_tol,
                     relative_objective_tol);
}
#endif  // ifdef TEST_DOUBLEDOUBLE

TEST_CASE("Chapter1Exercise13 float", "[Chapter1Exercise13 float]") {
  const bool kVerbose = false;
  const bool kPrintData = false;
  const float relative_error_tol = 1e-3;
  const float relative_objective_tol = 1e-2;
  Chapter1Exercise13(kVerbose, kPrintData, relative_error_tol,
                     relative_objective_tol);
}

TEST_CASE("Chapter1Exercise13 double", "[Chapter1Exercise13 double]") {
  const bool kVerbose = false;
  const bool kPrintData = false;
  const double relative_error_tol = 1e-7;
  const double relative_objective_tol = 1e-4;
  Chapter1Exercise13(kVerbose, kPrintData, relative_error_tol,
                     relative_objective_tol);
}

#ifdef TEST_DOUBLEDOUBLE
TEST_CASE("Chapter1Exercise13 DoubleDouble",
          "[Chapter1Exercise13 DoubleDouble]") {
  const bool kVerbose = false;
  const bool kPrintData = false;
  typedef mantis::DoubleMantissa<double> DoubleDouble;
  const DoubleDouble relative_error_tol = 1e-14;
  const DoubleDouble relative_objective_tol = 1e-10;
  Chapter1Exercise13(kVerbose, kPrintData, relative_error_tol,
                     relative_objective_tol);
}
#endif  // ifdef TEST_DOUBLEDOUBLE

TEST_CASE("Chapter1Exercise14 float", "[Chapter1Exercise14 float]") {
  const bool kVerbose = false;
  const bool kPrintData = false;
  const float relative_error_tol = 1e-3;
  const float relative_objective_tol = 1e-2;
  Chapter1Exercise14(kVerbose, kPrintData, relative_error_tol,
                     relative_objective_tol);
}

TEST_CASE("Chapter1Exercise14 double", "[Chapter1Exercise14 double]") {
  const bool kVerbose = false;
  const bool kPrintData = false;
  const double relative_error_tol = 1e-7;
  const double relative_objective_tol = 1e-4;
  Chapter1Exercise14(kVerbose, kPrintData, relative_error_tol,
                     relative_objective_tol);
}

#ifdef TEST_DOUBLEDOUBLE
TEST_CASE("Chapter1Exercise14 DoubleDouble",
          "[Chapter1Exercise14 DoubleDouble]") {
  const bool kVerbose = false;
  const bool kPrintData = false;
  typedef mantis::DoubleMantissa<double> DoubleDouble;
  const DoubleDouble relative_error_tol = 1e-14;
  const DoubleDouble relative_objective_tol = 1e-10;
  Chapter1Exercise14(kVerbose, kPrintData, relative_error_tol,
                     relative_objective_tol);
}
#endif  // ifdef TEST_DOUBLEDOUBLE

TEST_CASE("Chapter1Exercise19 float", "[Chapter1Exercise19 float]") {
  const bool kVerbose = false;
  const bool kPrintData = false;
  const float relative_error_tol = 1e-3;
  const float relative_objective_tol = 1e-2;
  Chapter1Exercise19(kVerbose, kPrintData, relative_error_tol,
                     relative_objective_tol);
}

TEST_CASE("Chapter1Exercise19 double", "[Chapter1Exercise19 double]") {
  const bool kVerbose = false;
  const bool kPrintData = false;
  const double relative_error_tol = 1e-7;
  const double relative_objective_tol = 1e-4;
  Chapter1Exercise19(kVerbose, kPrintData, relative_error_tol,
                     relative_objective_tol);
}

#ifdef TEST_DOUBLEDOUBLE
TEST_CASE("Chapter1Exercise19 DoubleDouble",
          "[Chapter1Exercise19 DoubleDouble]") {
  const bool kVerbose = false;
  const bool kPrintData = false;
  typedef mantis::DoubleMantissa<double> DoubleDouble;
  const DoubleDouble relative_error_tol = 1e-14;
  const DoubleDouble relative_objective_tol = 1e-10;
  Chapter1Exercise19(kVerbose, kPrintData, relative_error_tol,
                     relative_objective_tol);
}
#endif  // ifdef TEST_DOUBLEDOUBLE

TEST_CASE("Chapter7Example1 float", "[Chapter7Example1 float]") {
  const bool kVerbose = false;
  const bool kPrintData = false;
  const float relative_value_tol = 1e-3;
  Chapter7Example1(true /* use_gale_kuhn_tucker_approach */, kVerbose,
                   kPrintData, relative_value_tol);
  Chapter7Example1(false /* use_gale_kuhn_tucker_approach */, kVerbose,
                   kPrintData, relative_value_tol);
}

TEST_CASE("Chapter7Example1 double", "[Chapter7Example1 double]") {
  const bool kVerbose = false;
  const bool kPrintData = false;
  const double relative_value_tol = 1e-6;
  Chapter7Example1(true /* use_gale_kuhn_tucker_approach */, kVerbose,
                   kPrintData, relative_value_tol);
  Chapter7Example1(false /* use_gale_kuhn_tucker_approach */, kVerbose,
                   kPrintData, relative_value_tol);
}

#ifdef TEST_DOUBLEDOUBLE
TEST_CASE("Chapter7Example1 DoubleDouble", "[Chapter7Example1 DoubleDouble]") {
  const bool kVerbose = false;
  const bool kPrintData = false;
  const mantis::DoubleMantissa<double> relative_value_tol = 1e-13;
  Chapter7Example1(true /* use_gale_kuhn_tucker_approach */, kVerbose,
                   kPrintData, relative_value_tol);
  Chapter7Example1(false /* use_gale_kuhn_tucker_approach */, kVerbose,
                   kPrintData, relative_value_tol);
}
#endif  // ifdef TEST_DOUBLEDOUBLE

TEST_CASE("Chapter7Example2 float", "[Chapter7Example2 float]") {
  const bool kVerbose = false;
  const bool kPrintData = false;
  const float relative_value_tol = 1e-3;
  Chapter7Example2(true /* use_gale_kuhn_tucker_approach */, kVerbose,
                   kPrintData, relative_value_tol);
  Chapter7Example2(false /* use_gale_kuhn_tucker_approach */, kVerbose,
                   kPrintData, relative_value_tol);
}

TEST_CASE("Chapter7Example2 double", "[Chapter7Example2 double]") {
  const bool kVerbose = false;
  const bool kPrintData = false;
  const double relative_value_tol = 1e-6;
  Chapter7Example2(true /* use_gale_kuhn_tucker_approach */, kVerbose,
                   kPrintData, relative_value_tol);
  Chapter7Example2(false /* use_gale_kuhn_tucker_approach */, kVerbose,
                   kPrintData, relative_value_tol);
}

#ifdef TEST_DOUBLEDOUBLE
TEST_CASE("Chapter7Example2 DoubleDouble", "[Chapter7Example2 DoubleDouble]") {
  const bool kVerbose = false;
  const bool kPrintData = false;
  const mantis::DoubleMantissa<double> relative_value_tol = 1e-13;
  Chapter7Example2(true /* use_gale_kuhn_tucker_approach */, kVerbose,
                   kPrintData, relative_value_tol);
  Chapter7Example2(false /* use_gale_kuhn_tucker_approach */, kVerbose,
                   kPrintData, relative_value_tol);
}
#endif  // ifdef TEST_DOUBLEDOUBLE

TEST_CASE("Chapter7Example3 float", "[Chapter7Example3 float]") {
  const bool kVerbose = false;
  const bool kPrintData = false;
  const float relative_value_tol = 1e-3;
  Chapter7Example3(true /* use_gale_kuhn_tucker_approach */, kVerbose,
                   kPrintData, relative_value_tol);
  Chapter7Example3(false /* use_gale_kuhn_tucker_approach */, kVerbose,
                   kPrintData, relative_value_tol);
}

TEST_CASE("Chapter7Example3 double", "[Chapter7Example3 double]") {
  const bool kVerbose = false;
  const bool kPrintData = false;
  const double relative_value_tol = 1e-6;
  Chapter7Example3(true /* use_gale_kuhn_tucker_approach */, kVerbose,
                   kPrintData, relative_value_tol);
  Chapter7Example3(false /* use_gale_kuhn_tucker_approach */, kVerbose,
                   kPrintData, relative_value_tol);
}

#ifdef TEST_DOUBLEDOUBLE
TEST_CASE("Chapter7Example3 DoubleDouble", "[Chapter7Example3 DoubleDouble]") {
  const bool kVerbose = false;
  const bool kPrintData = false;
  const mantis::DoubleMantissa<double> relative_value_tol = 1e-13;
  Chapter7Example3(true /* use_gale_kuhn_tucker_approach */, kVerbose,
                   kPrintData, relative_value_tol);
  Chapter7Example3(false /* use_gale_kuhn_tucker_approach */, kVerbose,
                   kPrintData, relative_value_tol);
}
#endif  // ifdef TEST_DOUBLEDOUBLE

TEST_CASE("Chapter7Example4 float", "[Chapter7Example4 float]") {
  const bool kVerbose = false;
  const bool kPrintData = false;
  const float relative_value_tol = 1e-3;
  Chapter7Example4(true /* use_gale_kuhn_tucker_approach */, kVerbose,
                   kPrintData, relative_value_tol);
  Chapter7Example4(false /* use_gale_kuhn_tucker_approach */, kVerbose,
                   kPrintData, relative_value_tol);
}

TEST_CASE("Chapter7Example4 double", "[Chapter7Example4 double]") {
  const bool kVerbose = false;
  const bool kPrintData = false;
  const double relative_value_tol = 1e-6;
  Chapter7Example4(true /* use_gale_kuhn_tucker_approach */, kVerbose,
                   kPrintData, relative_value_tol);
  Chapter7Example4(false /* use_gale_kuhn_tucker_approach */, kVerbose,
                   kPrintData, relative_value_tol);
}

#ifdef TEST_DOUBLEDOUBLE
TEST_CASE("Chapter7Example4 DoubleDouble", "[Chapter7Example4 DoubleDouble]") {
  const bool kVerbose = false;
  const bool kPrintData = false;
  const mantis::DoubleMantissa<double> relative_value_tol = 1e-13;
  Chapter7Example4(true /* use_gale_kuhn_tucker_approach */, kVerbose,
                   kPrintData, relative_value_tol);
  Chapter7Example4(false /* use_gale_kuhn_tucker_approach */, kVerbose,
                   kPrintData, relative_value_tol);
}
#endif  // ifdef TEST_DOUBLEDOUBLE

TEST_CASE("Chapter7Baseball float", "[Chapter7Baseball float]") {
  const bool kVerbose = false;
  const bool kPrintData = false;
  const float relative_value_tol = 1e-3;
  Chapter7Baseball(true /* use_gale_kuhn_tucker_approach */, kVerbose,
                   kPrintData, relative_value_tol);
  Chapter7Baseball(false /* use_gale_kuhn_tucker_approach */, kVerbose,
                   kPrintData, relative_value_tol);
}

TEST_CASE("Chapter7Baseball double", "[Chapter7Baseball double]") {
  const bool kVerbose = false;
  const bool kPrintData = false;
  const double relative_value_tol = 1e-6;
  Chapter7Baseball(true /* use_gale_kuhn_tucker_approach */, kVerbose,
                   kPrintData, relative_value_tol);
  Chapter7Baseball(false /* use_gale_kuhn_tucker_approach */, kVerbose,
                   kPrintData, relative_value_tol);
}

#ifdef TEST_DOUBLEDOUBLE
TEST_CASE("Chapter7Baseball DoubleDouble", "[Chapter7Baseball DoubleDouble]") {
  const bool kVerbose = false;
  const bool kPrintData = false;
  const mantis::DoubleMantissa<double> relative_value_tol = 1e-13;
  Chapter7Baseball(true /* use_gale_kuhn_tucker_approach */, kVerbose,
                   kPrintData, relative_value_tol);
  Chapter7Baseball(false /* use_gale_kuhn_tucker_approach */, kVerbose,
                   kPrintData, relative_value_tol);
}
#endif  // ifdef TEST_DOUBLEDOUBLE
