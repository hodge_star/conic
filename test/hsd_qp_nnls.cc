/*
 * Copyright (c) 2019 Jack Poulson <jack@hodgestar.com>
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
#define CATCH_CONFIG_MAIN
#include <iostream>
#include <limits>
#include "catch2/catch.hpp"
#include "conic.hpp"
#include "quotient/timer.hpp"

// If defined, the tests are also run in double-double arithmetic.
#define TEST_DOUBLEDOUBLE

// This problem's least squares solution is already non-negative, and it has
// a relative residual norm of roughly 2.51822624373469397402424723255475e-1.
template <typename Real>
void Test1(bool verbose, bool print_data, Real relative_residual_tolerance) {
  // A = | 1, 2, 1 |
  //     | 3, 4, 0 |
  //     | 5, 6, 0 |
  //     | 7, 8, 0 |
  //     | 0, 0, 1 |
  const conic::Int height = 5;
  const conic::Int width = 3;
  catamari::CoordinateMatrix<Real> A;
  A.Resize(height, width);
  A.AddEntry(0, 0, Real(1.));
  A.AddEntry(0, 1, Real(2.));
  A.AddEntry(0, 2, Real(1.));
  A.AddEntry(1, 0, Real(3.));
  A.AddEntry(1, 1, Real(4.));
  A.AddEntry(2, 0, Real(5.));
  A.AddEntry(2, 1, Real(6.));
  A.AddEntry(3, 0, Real(7.));
  A.AddEntry(3, 1, Real(8.));
  A.AddEntry(4, 2, Real(1.));

  // b = | 9  |.
  //     | 10 |
  //     | 11 |
  //     | 12 |
  //     | 13 |
  catamari::BlasMatrix<Real> b;
  b.Resize(height, 1);
  b(0) = Real(9.);
  b(1) = Real(10.);
  b(2) = Real(11.);
  b(3) = Real(12.);
  b(4) = Real(13.);

  if (print_data) {
    std::cout << "ASparse=[\n"
              << A << "];\n"
              << "b=[\n"
              << b << "];\n"
              << std::endl;
  }

  const Real target_relative_residual = 2.51822624373469397402424723255475e-1;

  catamari::BlasMatrix<Real> solution;
  const conic::hsd_qp::SolveControl<Real> control;
  const bool solved = conic::hsd_qp::NonnegativeLeastSquares(
      A, b.ConstView(), control, &solution);
  REQUIRE(solved);
  if (print_data) {
    std::cout << "solution=[" << solution << "];" << std::endl;
  }

  // Measure the residual.
  catamari::BlasMatrix<Real> residual = b;
  catamari::ApplySparse(Real(-1), A, solution.ConstView(), Real(1),
                        &residual.view);
  const Real target_norm = catamari::EuclideanNorm(b.ConstView());
  const Real residual_norm = catamari::EuclideanNorm(residual.ConstView());
  const Real relative_residual_norm = residual_norm / target_norm;
  std::cout << "|| b ||_2 = " << target_norm << "\n"
            << "|| b - A x ||_2 = " << residual_norm << std::endl;

  const Real relative_residual_deviation =
      std::abs(relative_residual_norm - target_relative_residual) /
      (target_relative_residual + Real(1));
  REQUIRE(relative_residual_deviation <= relative_residual_tolerance);
}

TEST_CASE("NNLS float", "[NNLS float]") {
  const bool kVerbose = false;
  const bool kPrintData = false;
  const float relative_residual_tolerance = 1e-3;
  Test1(kVerbose, kPrintData, relative_residual_tolerance);
}

TEST_CASE("NNLS", "[NNLS]") {
  const bool kVerbose = false;
  const bool kPrintData = false;
  const double relative_residual_tolerance = 1e-6;
  Test1(kVerbose, kPrintData, relative_residual_tolerance);
}

#ifdef TEST_DOUBLEDOUBLE
TEST_CASE("NNLS DoubleDouble", "[NNLS DoubleDouble]") {
  const bool kVerbose = false;
  const bool kPrintData = false;
  typedef mantis::DoubleMantissa<double> DoubleDouble;
  const DoubleDouble relative_residual_tolerance = 1e-13;
  Test1(kVerbose, kPrintData, relative_residual_tolerance);
}
#endif  // ifdef TEST_DOUBLEDOUBLE
