/*
 * Copyright (c) 2019 Jack Poulson <jack@hodgestar.com>
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
#define CATCH_CONFIG_MAIN
#include <iostream>
#include <limits>
#include "catch2/catch.hpp"
#include "conic.hpp"
#include "quotient/timer.hpp"

// If defined, the tests are also run in double-double arithmetic.
#define TEST_DOUBLEDOUBLE

template <typename Real>
void Test1(bool verbose, bool print_data, Real relative_residual_tolerance) {
  // A = | 1, 2, 1 |
  //     | 3, 4, 0 |
  //     | 5, 6, 0 |
  //     | 7, 8, 0 |
  //     | 0, 0, 1 |
  const conic::Int height = 5;
  const conic::Int width = 3;
  catamari::CoordinateMatrix<Real> A;
  A.Resize(height, width);
  A.AddEntry(0, 0, Real(1.));
  A.AddEntry(0, 1, Real(2.));
  A.AddEntry(0, 2, Real(1.));
  A.AddEntry(1, 0, Real(3.));
  A.AddEntry(1, 1, Real(4.));
  A.AddEntry(2, 0, Real(5.));
  A.AddEntry(2, 1, Real(6.));
  A.AddEntry(3, 0, Real(7.));
  A.AddEntry(3, 1, Real(8.));
  A.AddEntry(4, 2, Real(1.));

  // b = | 9    |.
  //     | 1e13 |
  //     | 11   |
  //     | 12   |
  //     | 13   |
  catamari::BlasMatrix<Real> b;
  b.Resize(height, 1);
  b(0) = Real(9.);
  b(1) = Real(1e13);
  b(2) = Real(11.);
  b(3) = Real(12.);
  b(4) = Real(13.);

  if (print_data) {
    std::cout << "ASparse=[\n"
              << A << "];\n"
              << "b=[\n"
              << b << "];\n"
              << std::endl;
  }

  catamari::BlasMatrix<Real> solution;
  conic::hsd_qp::SolveControl<Real> control;
  control.output.verbose = verbose;
  const bool solved = conic::hsd_qp::NonnegativeLeastSquares(
      A, b.ConstView(), control, &solution);
  REQUIRE(solved);
  if (print_data) {
    std::cout << "solution=[" << solution << "];" << std::endl;
  }

  // Measure the residual.
  catamari::BlasMatrix<Real> residual = b;
  catamari::ApplySparse(Real(-1), A, solution.ConstView(), Real(1),
                        &residual.view);
  const Real target_norm = catamari::EuclideanNorm(b.ConstView());
  const Real residual_norm = catamari::EuclideanNorm(residual.ConstView());
  std::cout << "|| b ||_2 = " << target_norm << "\n"
            << "|| b - A x ||_2 = " << residual_norm << std::endl;
}

template <typename Real>
void Test2(bool verbose, bool print_data, Real relative_residual_tolerance) {
  // A = | 1, 2, 1 |
  //     | 3, 4, 0 |
  //     | 5, 6, 0 |
  //     | 7, 8, 0 |
  //     | 0, 0, 1 |
  const conic::Int height = 5;
  const conic::Int width = 3;
  catamari::CoordinateMatrix<Real> A;
  A.Resize(height, width);
  A.AddEntry(0, 0, Real(1.));
  A.AddEntry(0, 1, Real(2.));
  A.AddEntry(0, 2, Real(1.));
  A.AddEntry(1, 0, Real(3.));
  A.AddEntry(1, 1, Real(4.));
  A.AddEntry(2, 0, Real(5.));
  A.AddEntry(2, 1, Real(6.));
  A.AddEntry(3, 0, Real(7.));
  A.AddEntry(3, 1, Real(8.));
  A.AddEntry(4, 2, Real(1.));

  // b = |     9 |.
  //     |  1e14 |
  //     |    11 |
  //     | -1e13 |
  //     |    13 |
  catamari::BlasMatrix<Real> b;
  b.Resize(height, 1);
  b(0) = Real(9.);
  b(1) = Real(1e14);
  b(2) = Real(11.);
  b(3) = Real(-1e13);
  b(4) = Real(13.);

  if (print_data) {
    std::cout << "ASparse=[\n"
              << A << "];\n"
              << "b=[\n"
              << b << "];\n"
              << std::endl;
  }

  catamari::BlasMatrix<Real> solution;
  conic::hsd_qp::SolveControl<Real> control;
  control.output.verbose = verbose;
  const bool solved = conic::hsd_qp::NonnegativeLeastSquares(
      A, b.ConstView(), control, &solution);
  REQUIRE(solved);
  if (print_data) {
    std::cout << "solution=[" << solution << "];" << std::endl;
  }

  // Measure the residual.
  catamari::BlasMatrix<Real> residual = b;
  catamari::ApplySparse(Real(-1), A, solution.ConstView(), Real(1),
                        &residual.view);
  const Real target_norm = catamari::EuclideanNorm(b.ConstView());
  const Real residual_norm = catamari::EuclideanNorm(residual.ConstView());
  std::cout << "|| b ||_2 = " << target_norm << "\n"
            << "|| b - A x ||_2 = " << residual_norm << std::endl;
}

template <typename Real>
void Test3(bool verbose, bool print_data, Real relative_residual_tolerance) {
  // A = |     1,    2, 1, -100 |
  //     |     3, 4e12, 0,    5 |
  //     |  5e-8,    6, 0, 2e10 |
  //     |     7,    8, 0,   -7 |
  //     | -3e15,    0, 1,    8 |
  //     |    -8,  3e3, 5,   -2 |
  const conic::Int height = 6;
  const conic::Int width = 4;
  catamari::CoordinateMatrix<Real> A;
  A.Resize(height, width);
  A.AddEntry(0, 0, Real(1));
  A.AddEntry(0, 1, Real(2));
  A.AddEntry(0, 2, Real(1));
  A.AddEntry(0, 3, Real(-100));
  A.AddEntry(1, 0, Real(3));
  A.AddEntry(1, 1, Real(4e12));
  A.AddEntry(1, 3, Real(5));
  A.AddEntry(2, 0, Real(5e-8));
  A.AddEntry(2, 1, Real(6));
  A.AddEntry(2, 3, Real(2e10));
  A.AddEntry(3, 0, Real(7));
  A.AddEntry(3, 1, Real(8));
  A.AddEntry(3, 3, Real(-7));
  A.AddEntry(4, 0, Real(-3e15));
  A.AddEntry(4, 2, Real(1));
  A.AddEntry(4, 3, Real(8));
  A.AddEntry(5, 0, Real(-8));
  A.AddEntry(5, 1, Real(3e3));
  A.AddEntry(5, 2, Real(5));
  A.AddEntry(5, 3, Real(-2));

  // b = |     9 |.
  //     |  1e14 |
  //     |    11 |
  //     | -1e13 |
  //     |    13 |
  //     |   2e8 |
  catamari::BlasMatrix<Real> b;
  b.Resize(height, 1);
  b(0) = Real(9.);
  b(1) = Real(1e14);
  b(2) = Real(11.);
  b(3) = Real(-1e13);
  b(4) = Real(13.);
  b(5) = Real(2e8);

  if (print_data) {
    std::cout << "ASparse=[\n"
              << A << "];\n"
              << "b=[\n"
              << b << "];\n"
              << std::endl;
  }

  catamari::BlasMatrix<Real> solution;
  conic::hsd_qp::SolveControl<Real> control;
  control.output.verbose = verbose;
  const bool solved = conic::hsd_qp::NonnegativeLeastSquares(
      A, b.ConstView(), control, &solution);
  REQUIRE(solved);
  if (print_data) {
    std::cout << "solution=[" << solution << "];" << std::endl;
  }

  // Measure the residual.
  catamari::BlasMatrix<Real> residual = b;
  catamari::ApplySparse(Real(-1), A, solution.ConstView(), Real(1),
                        &residual.view);
  const Real target_norm = catamari::EuclideanNorm(b.ConstView());
  const Real residual_norm = catamari::EuclideanNorm(residual.ConstView());
  std::cout << "|| b ||_2 = " << target_norm << "\n"
            << "|| b - A x ||_2 = " << residual_norm << std::endl;
}

TEST_CASE("Test1", "[Test1]") {
  const bool kVerbose = false;
  const bool kPrintData = false;
  const double relative_residual_tolerance = 1e-6;
  Test1(kVerbose, kPrintData, relative_residual_tolerance);
}

#ifdef TEST_DOUBLEDOUBLE
TEST_CASE("Test1 DoubleDouble", "[Test1 DoubleDouble]") {
  const bool kVerbose = false;
  const bool kPrintData = false;
  typedef mantis::DoubleMantissa<double> DoubleDouble;
  const DoubleDouble relative_residual_tolerance = 1e-13;
  Test1(kVerbose, kPrintData, relative_residual_tolerance);
}
#endif  // ifdef TEST_DOUBLEDOUBLE

TEST_CASE("Test2", "[Test2]") {
  const bool kVerbose = false;
  const bool kPrintData = false;
  const double relative_residual_tolerance = 1e-6;
  Test2(kVerbose, kPrintData, relative_residual_tolerance);
}

#ifdef TEST_DOUBLEDOUBLE
TEST_CASE("Test2 DoubleDouble", "[Test2 DoubleDouble]") {
  const bool kVerbose = false;
  const bool kPrintData = false;
  typedef mantis::DoubleMantissa<double> DoubleDouble;
  const DoubleDouble relative_residual_tolerance = 1e-13;
  Test2(kVerbose, kPrintData, relative_residual_tolerance);
}
#endif  // ifdef TEST_DOUBLEDOUBLE

TEST_CASE("Test3", "[Test3]") {
  const bool kVerbose = false;
  const bool kPrintData = false;
  const double relative_residual_tolerance = 1e-6;
  Test3(kVerbose, kPrintData, relative_residual_tolerance);
}

#ifdef TEST_DOUBLEDOUBLE
TEST_CASE("Test3 DoubleDouble", "[Test3 DoubleDouble]") {
  const bool kVerbose = false;
  const bool kPrintData = false;
  typedef mantis::DoubleMantissa<double> DoubleDouble;
  const DoubleDouble relative_residual_tolerance = 1e-13;
  Test3(kVerbose, kPrintData, relative_residual_tolerance);
}
#endif  // ifdef TEST_DOUBLEDOUBLE
