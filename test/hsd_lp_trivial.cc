/*
 * Copyright (c) 2019 Jack Poulson <jack@hodgestar.com>
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
#define CATCH_CONFIG_MAIN
#include <iostream>
#include <limits>
#include "catch2/catch.hpp"
#include "conic.hpp"
#include "quotient/timer.hpp"

// If defined, the tests are also run in double-double arithmetic.
#define TEST_DOUBLEDOUBLE

// We test the primal problem
//
//   min_x { 1' x : x >= 0 }.
//
// The dual is then:
//
//   max_{y, z} { b' y : A' y + z = c, z >= 0 },
//
// which simplifies to
//
//   { z : z = c, z >= 0 }.
//
template <typename Real>
void TrivialFeasible(bool verbose, bool print_data,
                     Real relative_obj_tolerance) {
  conic::hsd_qp::Problem<Real> problem;
  problem.equality_matrix.Resize(0, 1);
  problem.equality_rhs.Resize(0, 1);
  problem.linear_objective.Resize(1, 1);
  problem.linear_objective(0) = Real(1);
  problem.quadratic_objective.Resize(1, 1);
  problem.FillNorms();
  if (print_data) {
    std::cout << problem << std::endl;
  }

  const Real target_objective = Real(0);

  conic::hsd_qp::SolveControl<Real> control;
  control.output.verbose = verbose;
  static const Real kEpsilon = std::numeric_limits<Real>::epsilon();
  const Real relative_error_tol =
      control.optimality.relative_tolerance_coefficient *
      std::pow(kEpsilon, control.optimality.relative_tolerance_exponent);

  conic::hsd_qp::Iterate<Real> certificate;
  const conic::Int num_iterations =
      conic::hsd_qp::Solve(problem, control, &certificate);
  if (print_data) {
    std::cout << certificate << std::endl;
  }

  // Measure the primal residual norm.
  conic::hsd_qp::Residuals<Real> state;
  state.FillRelativeError(problem, certificate);
  std::cout << "Ran for " << num_iterations << " iterations w/ relative error "
            << state.relative_error << std::endl;
  if (verbose) {
    certificate.PrintNorms(std::cout);
    state.PrintRelativeError(std::cout);
  }
  REQUIRE(certificate.certificate_type == conic::OPTIMALITY_CERTIFICATE);
  REQUIRE(state.relative_error <= relative_error_tol);

  const Real relative_obj_error =
      std::abs(state.objective.primal - target_objective) /
      (std::abs(target_objective) + 1);
  REQUIRE(relative_obj_error <= relative_obj_tolerance);
}

// We test
//
//   min_x { 1' x : x = -1, x >= 0 }.
//
// The dual is then:
//
//   max_{y, z} { b' y : A' y + z = c, z >= 0 },
//
// which simplifies to
//
//   max_{y, z} { -y : y + z = 1, z >= 0 }.
//
// The dual solution is then unbounded, as maximizing -y involves minimizing
// y. The constraint y + z = 1 can be satisfied with y = -phi, z = phi + 1,
// for any phi >= -1. Thus, the dual solution will produce unbounded |y| and
// |z|.
//
template <typename Real>
void TrivialPrimalInfeasible(bool verbose, bool print_data) {
  conic::hsd_qp::Problem<Real> problem;
  problem.equality_matrix.Resize(1, 1);
  problem.equality_matrix.AddEntry(0, 0, Real(1));
  problem.equality_rhs.Resize(1, 1);
  problem.equality_rhs(0) = Real(-1);
  problem.linear_objective.Resize(1, 1);
  problem.linear_objective(0) = Real(1);
  problem.quadratic_objective.Resize(1, 1);
  problem.FillNorms();
  if (print_data) {
    std::cout << problem << std::endl;
  }

  conic::hsd_qp::SolveControl<Real> control;
  control.output.verbose = verbose;
  conic::hsd_qp::Iterate<Real> certificate;
  const conic::Int num_iterations =
      conic::hsd_qp::Solve(problem, control, &certificate);
  if (print_data) {
    std::cout << certificate << std::endl;
  }

  std::cout << "Ran for " << num_iterations << " iterations" << std::endl;
  if (verbose) {
    certificate.PrintNorms(std::cout);
  }
  REQUIRE(certificate.certificate_type ==
          conic::PRIMAL_INFEASIBILITY_CERTIFICATE);
}

// We test
//
//   min_x { -1' x : x >= 0 },
//
// which has dual
//
//   max_{y, z} { b' y : A' y + z = c, z >= 0 },
//
// which simplifies to
//
//   { z = -1, z >= 0 }.
//
// The primal solution is unbounded and the dual is infeasible.
//
template <typename Real>
void TrivialDualInfeasible(bool verbose, bool print_data) {
  conic::hsd_qp::Problem<Real> problem;
  problem.equality_matrix.Resize(0, 1);
  problem.equality_rhs.Resize(0, 1);
  problem.linear_objective.Resize(1, 1);
  problem.linear_objective(0) = Real(-1);
  problem.quadratic_objective.Resize(1, 1);
  problem.FillNorms();
  if (print_data) {
    std::cout << problem << std::endl;
  }

  conic::hsd_qp::SolveControl<Real> control;
  control.output.verbose = verbose;
  conic::hsd_qp::Iterate<Real> certificate;
  const conic::Int num_iterations =
      conic::hsd_qp::Solve(problem, control, &certificate);
  if (print_data) {
    std::cout << certificate << std::endl;
  }

  std::cout << "Ran for " << num_iterations << " iterations" << std::endl;
  if (verbose) {
    certificate.PrintNorms(std::cout);
  }
  REQUIRE(certificate.certificate_type ==
          conic::DUAL_INFEASIBILITY_CERTIFICATE);
}

TEST_CASE("TrivialFeasible float", "[TrivialFeasible float]") {
  const bool kVerbose = false;
  const bool kPrintData = false;
  const float relative_obj_tolerance = 1e-3;
  TrivialFeasible(kVerbose, kPrintData, relative_obj_tolerance);
}

TEST_CASE("TrivialFeasible double", "[TrivialFeasible double]") {
  const bool kVerbose = false;
  const bool kPrintData = false;
  const double relative_obj_tolerance = 1e-6;
  TrivialFeasible(kVerbose, kPrintData, relative_obj_tolerance);
}

#ifdef TEST_DOUBLEDOUBLE
TEST_CASE("TrivialFeasible DoubleDouble", "[TrivialFeasible DoubleDouble]") {
  typedef mantis::DoubleMantissa<double> DoubleDouble;
  const bool kVerbose = false;
  const bool kPrintData = false;
  const DoubleDouble relative_obj_tolerance = 1e-13;
  TrivialFeasible(kVerbose, kPrintData, relative_obj_tolerance);
}
#endif  // ifdef TEST_DOUBLEDOUBLE

TEST_CASE("TrivialPrimalInfeasibility float",
          "[TrivialPrimalInfeasibility float]") {
  const bool kVerbose = false;
  const bool kPrintData = false;
  TrivialPrimalInfeasible<float>(kVerbose, kPrintData);
}

TEST_CASE("TrivialPrimalInfeasibility double",
          "[TrivialPrimalInfeasibility double]") {
  const bool kVerbose = false;
  const bool kPrintData = false;
  TrivialPrimalInfeasible<double>(kVerbose, kPrintData);
}

#ifdef TEST_DOUBLEDOUBLE
TEST_CASE("TrivialPrimalInfeasibility DoubleDouble",
          "[TrivialPrimalInfeasibility DoubleDouble]") {
  const bool kVerbose = false;
  const bool kPrintData = false;
  TrivialPrimalInfeasible<mantis::DoubleMantissa<double>>(kVerbose, kPrintData);
}
#endif  // ifdef TEST_DOUBLEDOUBLE

TEST_CASE("TrivialDualInfeasibility float",
          "[TrivialDualInfeasibility float]") {
  const bool kVerbose = false;
  const bool kPrintData = false;
  TrivialDualInfeasible<float>(kVerbose, kPrintData);
}

TEST_CASE("TrivialDualInfeasibility double",
          "[TrivialDualInfeasibility double]") {
  const bool kVerbose = false;
  const bool kPrintData = false;
  TrivialDualInfeasible<double>(kVerbose, kPrintData);
}

#ifdef TEST_DOUBLEDOUBLE
TEST_CASE("TrivialDualInfeasibility DoubleDouble",
          "[TrivialDualInfeasibility DoubleDouble]") {
  const bool kVerbose = false;
  const bool kPrintData = false;
  TrivialDualInfeasible<mantis::DoubleMantissa<double>>(kVerbose, kPrintData);
}
#endif  // ifdef TEST_DOUBLEDOUBLE
