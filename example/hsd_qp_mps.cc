/*
 * Copyright (c) 2019 Jack Poulson <jack@hodgestar.com>
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
#include <iostream>
#include <limits>
#include "conic.hpp"
#include "quotient/timer.hpp"
#include "specify.hpp"

// Configuration for a netlib LP test.
template <typename Real>
struct NetlibLPTestConfig {
  // The location of the file containing the (uncompressed) MPS file.
  std::string filename;

  // The configuration parameters for the LP solver.
  conic::hsd_qp::SolveControl<Real> control;

  // Whether infinite bounds should be tightened.
  bool tighten_infinite_bounds = false;

  // Whether the problem data and solution should be printed.
  bool print_data = false;
};

template <typename Real>
void NetlibLPTest(const NetlibLPTestConfig<Real>& lp_config) {
  conic::ReadQuadraticProgramMPSConfig config;
  config.verbose = true;
  config.filename = lp_config.filename;

  conic::bounded_qp::Problem<Real> bounded_problem;
  auto bounded_info = conic::ReadQuadraticProgramMPS(config, &bounded_problem);

  bounded_problem.TightenBounds(
      lp_config.tighten_infinite_bounds, config.verbose);

  conic::hsd_qp::Problem<Real> standard_problem;
  conic::BoundedToStandardQPReduction<Real> reduction(bounded_problem);
  reduction.FormStandardProblem(&standard_problem);

  std::cout << "info.bounds_section.num_fixed_bounds: "
            << bounded_info.bounds_section.num_fixed_bounds << "\n"
            << "info.bounds_section.num_upper_bounds: "
            << bounded_info.bounds_section.num_upper_bounds << "\n"
            << "info.bounds_section.num_lower_bounds: "
            << bounded_info.bounds_section.num_lower_bounds << "\n"
            << "info.bounds_section.num_free_bounds: "
            << bounded_info.bounds_section.num_free_bounds << "\n"
            << "info.row_section.num_equality_rows: "
            << bounded_info.row_section.num_equality_rows << "\n"
            << "info.objective_shift: " << bounded_info.objective_shift << "\n"
            << "reduction.ObjectiveShift(): " << reduction.ObjectiveShift()
            << std::endl;

  if (lp_config.print_data) {
    std::cout << "Bounded problem:\n" << bounded_problem << std::endl;
    std::cout << "Standard problem:\n" << standard_problem << std::endl;
  }

  conic::hsd_qp::Iterate<Real> standard_certificate;
  quotient::Timer timer;
  timer.Start();
  auto control = lp_config.control;
  const Real kEpsilon = std::numeric_limits<Real>::epsilon();
  const Real relative_error_tol =
      control.optimality.relative_tolerance_coefficient *
      std::pow(kEpsilon, control.optimality.relative_tolerance_exponent);
  const conic::Int num_iterations =
      conic::hsd_qp::Solve(standard_problem, control, &standard_certificate);
  timer.Stop();
  if (lp_config.print_data) {
    std::cout << "Standard certificate:\n" << standard_certificate << std::endl;
  }
  standard_problem.PrintNorms(std::cout);
  standard_certificate.PrintNorms(std::cout);

  std::cout << "certificate_type: " << standard_certificate.certificate_type
            << std::endl;

  conic::hsd_qp::Residuals<Real> standard_residuals;
  standard_residuals.iterate_normalization =
      control.optimality.iterate_normalization;
  standard_residuals.FillRelativeError(standard_problem, standard_certificate);
  std::cout << "primal: " << standard_residuals.objective.primal << "\n"
            << "dual:   " << standard_residuals.objective.dual << std::endl;
  if (standard_certificate.certificate_type == conic::OPTIMALITY_CERTIFICATE ||
      standard_certificate.certificate_type == conic::INVALID_CERTIFICATE) {
    std::cout << lp_config.filename << " ran for " << num_iterations
              << " iterations (" << timer.TotalSeconds()
              << " seconds) w/ relative error "
              << standard_residuals.relative_error << std::endl;
    const Real standard_primal_objective = standard_residuals.objective.primal +
        reduction.ObjectiveShift();
    const Real standard_dual_objective = standard_residuals.objective.dual +
        reduction.ObjectiveShift();
    std::cout << "standard_primal_objective: " << standard_primal_objective
              << std::endl;
    std::cout << "standard_dual_objective: " << standard_dual_objective
              << std::endl;
    if (standard_residuals.relative_error > relative_error_tol) {
      std::cout << "ERROR: Relative tolerance was "
                << standard_residuals.relative_error << " > "
                << relative_error_tol << std::endl;
    }
  }
  standard_residuals.PrintRelativeError(std::cout);

  // Backtransform the standard-form iterate and compute its primal objective.
  conic::bounded_qp::Iterate<Real> bounded_certificate;
  reduction.BacktransformIterate(standard_certificate, &bounded_certificate);
  if (lp_config.print_data) {
    std::cout << "Bounded certificate:\n" << bounded_certificate << std::endl;
  }
  const catamari::ConstBlasMatrixView<Real> bounded_primal_solution =
      bounded_certificate.PrimalSolution();
  Real bounded_primal_objective = bounded_info.objective_shift;
  const conic::Int num_primal_bounded =
      bounded_problem.equality_matrix.NumColumns();
  for (conic::Int i = 0; i < num_primal_bounded; ++i) {
    bounded_primal_objective += bounded_problem.linear_objective(i) *
        bounded_primal_solution(i);
  }
  for (const auto& entry : bounded_problem.quadratic_objective.Entries()) {
    bounded_primal_objective += bounded_primal_solution(entry.row) *
        entry.value * bounded_primal_solution(entry.column) / Real(2);
  }
  std::cout << "bounded_primal_objective: " << bounded_primal_objective
            << std::endl;

  // Manually compute the primal feasibility residual for the bounded cert.
  const Real bounded_equality_rhs_two_norm =
      catamari::EuclideanNorm(bounded_problem.equality_rhs.ConstView());
  catamari::BlasMatrix<Real> bounded_residual = bounded_problem.equality_rhs;
  catamari::ApplySparse(
      Real(-1), bounded_problem.equality_matrix, bounded_primal_solution,
      Real(1), &bounded_residual.view);
  const Real bounded_primal_equality_residual_two_norm =
      catamari::EuclideanNorm(bounded_residual.ConstView());
  const Real bounded_primal_equality_relative_residual =
      bounded_primal_equality_residual_two_norm /
      (bounded_equality_rhs_two_norm + 1);
  std::cout << "bounded || b ||_2: " << bounded_equality_rhs_two_norm << "\n"
            << "bounded || b - A x ||_2 / (|| b ||_2 + 1): "
            << bounded_primal_equality_relative_residual << std::endl;

  // Print any entries of the bounded problem which are outside of the bounds.
  Real max_distance = 0;
  Real max_relative_distance = 0;
  for (conic::Int i = 0; i < num_primal_bounded; ++i) {
    const Real lower_bound = bounded_problem.lower_bounds[i];
    const Real upper_bound = bounded_problem.upper_bounds[i];
    const Real value = bounded_primal_solution(i);
    if (std::isfinite(lower_bound) && std::isfinite(upper_bound)) {
      if (value < lower_bound || value > upper_bound) {
        const Real distance =
            value < lower_bound ? lower_bound - value : value - upper_bound;
        const Real relative_distance =
            value < lower_bound ? distance / (std::abs(lower_bound) + 1) :
            distance / (std::abs(upper_bound) + 1);

        std::cout << "bounded primal solution entry " << i << " was "
                  << static_cast<double>(value) << ", which is " << distance
                  << " away from ["
                  << static_cast<double>(lower_bound) << ", "
                  << static_cast<double>(upper_bound) << "]" << std::endl;

        max_distance = std::max(max_distance, distance);
        max_relative_distance =
            std::max(max_relative_distance, relative_distance);
      }
    } else if (std::isfinite(lower_bound)) {
      if (value < lower_bound) {
        const Real distance = lower_bound - value;
        const Real relative_distance = distance / (std::abs(lower_bound) + 1);

        std::cout << "bounded primal solution entry " << i << " was "
                  << static_cast<double>(value) << ", which was less than "
                  << static_cast<double>(lower_bound) << std::endl;

        max_distance = std::max(max_distance, distance);
        max_relative_distance =
            std::max(max_relative_distance, relative_distance);
      }
    } else if (std::isfinite(upper_bound)) {
      if (value > upper_bound) {
        const Real distance = value - upper_bound;
        const Real relative_distance = distance / (std::abs(upper_bound) + 1);

        max_distance = std::max(max_distance, distance);
        std::cout << "bounded primal solution entry " << i << " was "
                  << static_cast<double>(value) << ", which was greater than "
                  << static_cast<double>(upper_bound) << std::endl;

        max_distance = std::max(max_distance, distance);
        max_relative_distance =
            std::max(max_relative_distance, relative_distance);
      }
    }
  }
  if (max_distance > Real(0)) {
    std::cout << "Maximum distance from bound was " << max_distance
              << std::endl;
    std::cout << "Maximum relative distance was " << max_relative_distance
              << std::endl;
  }
}

int main(int argc, char* argv[]) {
  typedef mantis::DoubleMantissa<double> DoubleDouble;

  specify::ArgumentParser parser(argc, argv);
  const std::string mps = parser.RequiredInput<std::string>(
      "mps", "Path to an MPS linear program description.");
  const bool verbose = parser.OptionalInput<bool>(
      "verbose", "Print progress information?", true);
  const bool analyze_residuals = parser.OptionalInput<bool>(
      "analyze_residuals", "Print component residuals?", false);
  const bool analyze_iterate_residuals = parser.OptionalInput<bool>(
      "analyze_iterate_residuals", "Print iterate component residuals?", false);
  const bool monitor_convergence = parser.OptionalInput<bool>(
      "monitor_convergence",
      "Create an output file monitoring IPM convergence?", true);
  const std::string double_convergence_file =
      parser.OptionalInput<std::string>(
          "double_convergence_file",
          "Filename for double-precision convergence file",
          "convergence_double.txt");
  const std::string double_double_convergence_file =
      parser.OptionalInput<std::string>(
          "double_double_convergence_file",
          "Filename for double-double-precision convergence file",
          "convergence_double_double.txt");
  const bool tighten_infinite_bounds = parser.OptionalInput<bool>(
      "tighten_infinite_bounds", "Attempt to tighten infinite bounds?", false);
  const bool print_data = parser.OptionalInput<bool>(
      "print_data", "Print the matrices and solutions?", false);
  const double relative_tolerance_coefficient = parser.OptionalInput<double>(
      "relative_tolerance_coefficient",
      "Coefficient in relative tolerance coefficient * eps^exponent", 7.4e-3);
  const double relative_tolerance_exponent = parser.OptionalInput<double>(
      "relative_tolerance_exponent",
      "Exponent in relative tolerance coefficient * eps^exponent", 0.375);
  const bool iterate_normalization = parser.OptionalInput<bool>(
      "iterate_normalization",
      "Whether relative errors should use iterate instead of RHS norms", false);
  const double desired_relative_residual = parser.OptionalInput<double>(
      "desired_relative_residual",
      "Ideal relative residual for the unregularized KKT solve.",
      1e-6);
  const double demanded_relative_residual = parser.OptionalInput<double>(
      "demanded_relative_residual",
      "Maximum allowed relative residual for the unregularized KKT solve.",
      0.9);
  const bool use_augmented = parser.OptionalInput<bool>(
      "use_augmented", "Factor the augmented system?", true);
  const bool geometric_equilibration = parser.OptionalInput<bool>(
      "geometric_equilibration",
      "Use geometric (instead of Ruiz) equilibration?", false);
  const int num_equilibration_passes = parser.OptionalInput<int>(
      "num_equilibration_passes", "Number of equilibration passes", 4);
  const int max_iterative_refinement_iterations = parser.OptionalInput<int>(
      "max_iterative_refinement_iterations",
      "Maximum number of iterations of IR to perform for each linear solve.",
      6);
  const bool project_dual_slack = parser.OptionalInput<bool>(
      "project_dual_slack",
      "Project dual slacks so they map to neighborhood of x o z = mu e?",
      false);
  const int max_gondzio_correctors = parser.OptionalInput<int>(
      "max_gondzio_correctors", "Maximum # of Gondzio higher-order correctors",
      2);
  const double gondzio_step_shift = parser.OptionalInput<double>(
      "gondzio_step_shift", "The constant increase in Gondzio corrections",
      0.08);
  const double gondzio_step_ratio = parser.OptionalInput<double>(
      "gondzio_step_ratio", "The step ratio in Gondzio corrections", 1.08);
  const double gondzio_min_accept_ratio = parser.OptionalInput<double>(
      "gondzio_min_accept_ratio",
      "The minimum ratio of the Gondzio vs. old step length to accept.", 0.9);
  const double initial_factor_primal_solution_prox_reg_coefficient =
      parser.OptionalInput<double>(
          "initial_factor_primal_solution_prox_reg_coefficient",
          "The primal solution has proximal reg of magnitude coeff * eps^exp.",
          1e-10);
  const double initial_factor_primal_solution_prox_reg_exponent =
      parser.OptionalInput<double>(
          "initial_factor_primal_solution_prox_reg_exponent",
          "The primal solution has proximal reg of magnitude coeff * eps^exp.",
          0.25);
  const double initial_factor_dual_solution_prox_reg_coefficient =
      parser.OptionalInput<double>(
          "initial_factor_dual_solution_prox_reg_coefficient",
          "The dual solution has proximal reg of magnitude coeff * eps^exp.",
          1e-8);
  const double initial_factor_dual_solution_prox_reg_exponent =
      parser.OptionalInput<double>(
          "initial_factor_dual_solution_prox_reg_exponent",
          "The dual solution has proximal reg of magnitude coeff * eps^exp.",
          0.25);
  const double initial_target_primal_solution_prox_reg_coefficient =
      parser.OptionalInput<double>(
          "initial_target_primal_solution_prox_reg_coefficient",
          "The primal solution has proximal reg of magnitude coeff * eps^exp.",
          1e-13);
  const double initial_target_primal_solution_prox_reg_exponent =
      parser.OptionalInput<double>(
          "initial_target_primal_solution_prox_reg_exponent",
          "The primal solution has proximal reg of magnitude coeff * eps^exp.",
          0.25);
  const double initial_target_dual_solution_prox_reg_coefficient =
      parser.OptionalInput<double>(
          "initial_target_dual_solution_prox_reg_coefficient",
          "The dual solution has proximal reg of magnitude coeff * eps^exp.",
          1e-11);
  const double initial_target_dual_solution_prox_reg_exponent =
      parser.OptionalInput<double>(
          "initial_target_dual_solution_prox_reg_exponent",
          "The dual solution has proximal reg of magnitude coeff * eps^exp.",
          0.25);
  const double min_average_complement_ratio = parser.OptionalInput<double>(
      "min_average_complement_ratio",
      "The min complement will be >= this fraction of the average complement.",
       1e-3);
  const double min_average_duality_gap_ratio = parser.OptionalInput<double>(
      "min_average_duality_gap_ratio",
      "The min complement will be >= this fraction of the average gap.",
       1e-4);
  const double max_average_complement_ratio = parser.OptionalInput<double>(
      "max_average_complement_ratio",
      "The min complement will be <= this fraction of the average complement.",
       0.02);
  const bool equilibrate = parser.OptionalInput<bool>(
      "equilibrate", "Dynamically equilibrate the linear systems?", false);
  const bool dynamically_regularize = parser.OptionalInput<bool>(
      "dynamically_regularize", "Dynamically regularize factorizations?",
      false);
  const bool relative_dynamic_regularization = parser.OptionalInput<bool>(
      "relative_dynamic_regularization",
      "Scale the dynamic regularization by the matrix norm?", false);
  const double dynamic_regularization_exponent = parser.OptionalInput<double>(
      "dynamic_regularization_exponent",
      "The power of machine epsilon to use for dynamic regularization.", 0.75);
  const double average_infeasibility_upper_bound_affine_allowance =
      parser.OptionalInput<double>(
          "average_infeasibility_upper_bound_affine_allowance",
          "The multiple of the average infeasibility gap upper bound the "
          "barrier is allowed to decrease to.", 1e-2);
  if (!parser.OK()) {
    return 0;
  }

  conic::hsd_qp::SolveControl<double> control;
  control.output.verbose = verbose;
  control.output.analyze_residuals = analyze_residuals;
  control.output.analyze_iterate_residuals = analyze_iterate_residuals;
  control.output.monitor_convergence = monitor_convergence;
  control.output.convergence_filename = double_convergence_file;
  control.barrier.average_infeasibility_upper_bound_affine_allowance =
      average_infeasibility_upper_bound_affine_allowance;
  control.optimality.relative_tolerance_coefficient =
      relative_tolerance_coefficient;
  control.optimality.relative_tolerance_exponent = relative_tolerance_exponent;
  control.optimality.iterate_normalization = iterate_normalization;
  control.scaling.geometric_equilibration = geometric_equilibration;
  control.scaling.num_equilibration_passes = num_equilibration_passes;
  control.linear_solve.use_augmented = use_augmented;
  control.linear_solve.desired_relative_residual = desired_relative_residual;
  control.linear_solve.demanded_relative_residual = demanded_relative_residual;
  control.linear_solve.ldl.equilibrate = equilibrate;
  control.linear_solve.max_iterative_refinement_iterations =
      max_iterative_refinement_iterations;
  control.dual_slack_projection.enabled = project_dual_slack;
  control.correctors.max_gondzio_correctors = max_gondzio_correctors;
  control.correctors.gondzio_step_shift = gondzio_step_shift;
  control.correctors.gondzio_step_ratio = gondzio_step_ratio;
  control.correctors.gondzio_min_accept_ratio = gondzio_min_accept_ratio;

  control.proximal_regularization.initial_factor_primal_solution_coefficient =
      initial_factor_primal_solution_prox_reg_coefficient;
  control.proximal_regularization.initial_factor_primal_solution_exponent =
      initial_factor_primal_solution_prox_reg_exponent;
  control.proximal_regularization.initial_factor_dual_solution_coefficient =
      initial_factor_dual_solution_prox_reg_coefficient;
  control.proximal_regularization.initial_factor_dual_solution_exponent =
      initial_factor_dual_solution_prox_reg_exponent;

  control.proximal_regularization.initial_target_primal_solution_coefficient =
      initial_target_primal_solution_prox_reg_coefficient;
  control.proximal_regularization.initial_target_primal_solution_exponent =
      initial_target_primal_solution_prox_reg_exponent;
  control.proximal_regularization.initial_target_dual_solution_coefficient =
      initial_target_dual_solution_prox_reg_coefficient;
  control.proximal_regularization.initial_target_dual_solution_exponent =
      initial_target_dual_solution_prox_reg_exponent;

  control.complement_conditioning.min_average_complement_ratio =
      min_average_complement_ratio;
  control.complement_conditioning.min_average_duality_gap_ratio =
      min_average_duality_gap_ratio;
  control.complement_conditioning.max_average_complement_ratio =
      max_average_complement_ratio;

  catamari::DynamicRegularizationControl<double> dynamic_reg;
  dynamic_reg.enabled = dynamically_regularize;
  dynamic_reg.relative = relative_dynamic_regularization;
  dynamic_reg.positive_threshold_exponent = dynamic_regularization_exponent;
  dynamic_reg.negative_threshold_exponent = dynamic_regularization_exponent;
  control.linear_solve.ldl.SetDynamicRegularization(dynamic_reg);
  const NetlibLPTestConfig<double> lp_config{
      mps,
      control,
      tighten_infinite_bounds,
      print_data,
  };
  NetlibLPTest(lp_config);

  conic::hsd_qp::SolveControl<DoubleDouble> control_doubledouble;
  control_doubledouble.output.verbose = verbose;
  control_doubledouble.output.analyze_residuals = analyze_residuals;
  control_doubledouble.output.analyze_iterate_residuals =
      analyze_iterate_residuals;
  control_doubledouble.output.monitor_convergence = monitor_convergence;
  control_doubledouble.output.convergence_filename =
      double_double_convergence_file;
  control_doubledouble.barrier.
      average_infeasibility_upper_bound_affine_allowance =
          average_infeasibility_upper_bound_affine_allowance;
  control_doubledouble.optimality.relative_tolerance_coefficient =
      relative_tolerance_coefficient;
  control_doubledouble.optimality.relative_tolerance_exponent =
      relative_tolerance_exponent;
  control_doubledouble.optimality.iterate_normalization = iterate_normalization;
  control_doubledouble.scaling.geometric_equilibration =
      geometric_equilibration;
  control_doubledouble.scaling.num_equilibration_passes =
      num_equilibration_passes;
  control_doubledouble.linear_solve.use_augmented = use_augmented;
  control_doubledouble.linear_solve.desired_relative_residual =
      desired_relative_residual;
  control_doubledouble.linear_solve.demanded_relative_residual =
      demanded_relative_residual;
  control_doubledouble.linear_solve.ldl.equilibrate = equilibrate;
  control_doubledouble.linear_solve.max_iterative_refinement_iterations =
      max_iterative_refinement_iterations;
  control_doubledouble.dual_slack_projection.enabled = project_dual_slack;
  control_doubledouble.correctors.max_gondzio_correctors =
      max_gondzio_correctors;
  control_doubledouble.correctors.gondzio_step_shift = gondzio_step_shift;
  control_doubledouble.correctors.gondzio_step_ratio = gondzio_step_ratio;
  control_doubledouble.correctors.gondzio_min_accept_ratio =
      gondzio_min_accept_ratio;

  control_doubledouble.proximal_regularization.initial_factor_primal_solution_coefficient =
      initial_factor_primal_solution_prox_reg_coefficient;
  control_doubledouble.proximal_regularization.initial_factor_primal_solution_exponent =
      initial_factor_primal_solution_prox_reg_exponent;
  control_doubledouble.proximal_regularization.initial_factor_dual_solution_coefficient =
      initial_factor_dual_solution_prox_reg_coefficient;
  control_doubledouble.proximal_regularization.initial_factor_dual_solution_exponent =
      initial_factor_dual_solution_prox_reg_exponent;

  control_doubledouble.proximal_regularization.initial_target_primal_solution_coefficient =
      initial_target_primal_solution_prox_reg_coefficient;
  control_doubledouble.proximal_regularization.initial_target_primal_solution_exponent =
      initial_target_primal_solution_prox_reg_exponent;
  control_doubledouble.proximal_regularization.initial_target_dual_solution_coefficient =
      initial_target_dual_solution_prox_reg_coefficient;
  control_doubledouble.proximal_regularization.initial_target_dual_solution_exponent =
      initial_target_dual_solution_prox_reg_exponent;

  control_doubledouble.complement_conditioning.min_average_complement_ratio =
      min_average_complement_ratio;
  control_doubledouble.complement_conditioning.min_average_duality_gap_ratio =
      min_average_duality_gap_ratio;
  control_doubledouble.complement_conditioning.max_average_complement_ratio =
      max_average_complement_ratio;

  catamari::DynamicRegularizationControl<DoubleDouble> dynamic_reg_doubledouble;
  dynamic_reg_doubledouble.enabled = dynamically_regularize;
  dynamic_reg_doubledouble.relative = relative_dynamic_regularization;
  dynamic_reg_doubledouble.positive_threshold_exponent =
      dynamic_regularization_exponent;
  dynamic_reg_doubledouble.negative_threshold_exponent =
      dynamic_regularization_exponent;
  control_doubledouble.linear_solve.ldl.SetDynamicRegularization(
      dynamic_reg_doubledouble);
  const NetlibLPTestConfig<DoubleDouble> lp_config_doubledouble{
      mps,
      control_doubledouble,
      tighten_infinite_bounds,
      print_data,
  };
  NetlibLPTest(lp_config_doubledouble);
}
