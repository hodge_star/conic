/*
 * Copyright (c) 2019 Jack Poulson <jack@hodgestar.com>
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
#ifndef CONIC_QP_MPS_H_
#define CONIC_QP_MPS_H_

#include <map>
#include <set>
#include <sstream>
#include <string>
#include <vector>

#include "conic/hsd_qp/problem.hpp"
#include "conic/imports.hpp"

namespace conic {

enum QuadraticProgramFormat {
  // arginf_x { c' x + (1 / 2) x' Q x : A x = b, l <= x <= u }
  BOUNDED_QUADRATIC_PROGRAM,

  // arginf_x { c' x + (1 / 2) x' Q x : A x = b, x >= 0 }
  NONNEGATIVE_QUADRATIC_PROGRAM,
};

// A structure holding the metadata for a Mathematical Programming System
// specification of a Quadratic Programming problem.
//
// Please see http://lpsolve.sourceforge.net/5.5/mps-format.htm for a
// detailed discussion of the MPS file format.
//
// Note that some of the netlib lp_data LP examples (e.g., tuff.mps) are not
// well-formed. For example, the fourth equality constraint of tuff.mps is
// empty.
template <typename Real>
struct QuadraticProgramMPSInfo {
  // Metadata for the NAME section of the MPS file.
  struct NameSectionInfo {
    // The name of the input.
    std::string name;
  };

  // Metadata for the ROWS section of the MPS file.
  struct RowSectionInfo {
    // The name of the cost vector.
    std::string cost_name;

    // The number of rows of the form g x <= upper_bound.
    Int num_lesser_rows = 0;

    // The number of rows of the form -g x <= -lower_bound.
    Int num_greater_rows = 0;

    // The number of rows of the form a x = beta.
    Int num_equality_rows = 0;

    // The number of rows that are actually setting an entry of the cost vector.
    Int num_nonconstraining_rows = 0;
  };

  // Metadata for the COLUMNS section of the MPS file.
  struct ColumnSectionInfo {
    // The total number of nonzero entries in the equality constraints A x = b.
    Int num_equality_entries = 0;

    // The total number of nonzero entries in the inequality constraints
    // G x <= h.
    Int num_inequality_entries = 0;
  };

  // Metadata for the QUADOBJ section of the MPS file.
  struct QuadraticObjectiveSectionInfo {
    // The number of nonzero entries in the quadratic objective matrix.
    Int num_entries = 0;
  };

  // Metadata for the BOUNDS section of the MPS file.
  struct BoundsSectionInfo {
    // The name of the BOUNDS section.
    std::string bounds_name;

    // The total number of variable upper bounds.
    Int num_upper_bounds = 0;

    // The total number of variable lower bounds.
    Int num_lower_bounds = 0;

    // The total number of fixed variables.
    Int num_fixed_bounds = 0;

    // The total number of free variables.
    Int num_free_bounds = 0;
  };

  // Metadata for the RANGES section of the MPS file.
  struct RangeSectionInfo {
    // The name of the RANGES section.
    std::string range_name;

    // The number of range constraints (lower_bound <= g x <= upper_bound).
    Int num_ranges = 0;
  };

  // Metadata for the RHS section of the MPS file.
  struct RHSSectionInfo {
    // The name of the right-hand side section.
    std::string rhs_name;

    // The number of right-hand sides.
    Int num_rhs = 0;
  };

  NameSectionInfo name_section;

  RowSectionInfo row_section;

  ColumnSectionInfo column_section;

  QuadraticObjectiveSectionInfo quadobj_section;

  BoundsSectionInfo bounds_section;

  RangeSectionInfo range_section;

  RHSSectionInfo rhs_section;

  Int num_artificial_variables = 0;

  Real objective_shift = 0;
};

struct ReadQuadraticProgramMPSConfig {
  // The path to the MPS file to be read.
  std::string filename;

  // Whether the MPS file is in a compressed form.
  // NOTE: Compression is not yet supported.
  bool compressed = false;

  // Whether to default to minimizing or maximizing the objective.
  bool minimize = true;

  // The PILOT netlib lp_data model appears to require this variable to be
  // set to true.
  bool keep_nonnegative_with_zero_upper_bound = true;

  // The style of the linear program to be solved.
  QuadraticProgramFormat format = NONNEGATIVE_QUADRATIC_PROGRAM;

  // Whether progress information should be printed during loading.
  bool verbose = false;
};

struct WriteQuadraticProgramMPSConfig {
  // The path to the MPS file to be written.
  std::string filename;

  // Whether the output should be created in compressed form.
  // NOTE: Enabling compression is not yet supported.
  bool compressed = false;
};

// Pretty prints the QP MPS info.
template <typename Real>
std::ostream& operator<<(std::ostream& os,
                         const QuadraticProgramMPSInfo<Real>& info);

// We adopt the conventional assumption that variables live within
// [0, infinity) if no bounds are specified; we modify the assumption as
// little as possible when explicit bounds are given. For example, if only a
// positive upper bound of 7 is specified, the interval will be changed to
// [0, 7]. If a positive lower bound of 4 was instead specified, the interval
// would become [4, infinity).
//
// If the upper bound is negative, then the lower bound is reset to -infinity;
// if the upper bound is exactly zero, then there is disagreement in how to
// handle the lower bound. Many MPS readers reset the lower bound to -infinity,
// but ILOG does not. We provide an option to choose between the two behaviors.
//
// Handling 'RANGES' sections requires some extra care
// (Cf. http://lpsolve.sourceforge.net/5.5/mps-format.htm). In particular, the
// 'RANGES' section allowes 'GREATER', 'LESSER', and 'EQUALITY' rows to be
// transformed into two-sided bounds. That is to say, constraints of the form
//
//   g_i x >= d_i,
//   g_i x <= d_i, or
//   g_i x = d_i,
//
// are transformed into
//
//   d_i <= g_i x <= d_i + |r|, or
//   d_i - |r| <= g_i x <= d_i,
//
// depending upon the sign of the variable 'r' specified in the 'RANGES' section
// (in a format that mirrors the 'RHS' section). More specifically, the sign of
// 'r' is irrelevant for 'GREATER' and 'LESSER' rows, but a positive value for
// 'r' cases an 'EQUALITY' constraint to become the *lower* bound of an interval
// constraint of width 'r', whereas a negative value for 'r' transforms the
// 'EQUALITY' constraint into an *upper* bound on an interval of length '|r|'.
//
// It is of use to recognize that, if a 'RANGES' section modifies an 'EQUALITY'
// constraint, then we can henceforth treat said equality constraint as a
// 'GREATER' row if 'r' was positive, and as a 'LESSER' row if 'r' was negative.
// Thus, we are left with deciding how best to introduce additional inequality
// constraints.
template <typename Real>
QuadraticProgramMPSInfo<Real> ReadQuadraticProgramMPS(
    const ReadQuadraticProgramMPSConfig& config,
    hsd_qp::Problem<Real>* problem);

template <typename Real>
void WriteQuadraticProgramMPS(const WriteQuadraticProgramMPSConfig& config,
                              const hsd_qp::Problem<Real>& problem);

namespace mps {

enum Section {
  MPS_BOUNDS,
  MPS_COLUMNS,
  MPS_END,
  MPS_MARKER,
  MPS_NAME,
  MPS_NONE,
  MPS_OBJSENSE,
  MPS_QUADOBJ,
  MPS_RANGES,
  MPS_RHS,
  MPS_ROWS,
  MPS_SOS,
};

enum RowType {
  MPS_LESSER_ROW,
  MPS_GREATER_ROW,
  MPS_EQUALITY_ROW,
  MPS_NONCONSTRAINING_ROW,
};

// Metadata for a single row constraint.
struct RowMetadata {
  // The type of constraint that the row represents (if any).
  RowType type;

  // The index of this row in the list of rows with the same type.
  Int type_index;

  // Because we delete rows with no nonzeros (e.g., for the tuff example),
  // we must keep track of the number of nonzeros.
  Int num_nonzeros = 0;

  // Whether a row, say g, has a range modifier applied to it, so that an
  // auxiliary variable, say zeta, is introduced for enforcing lower and
  // upper bounds lower_bound <= g x <= upper_bound via:
  //
  //   [g, -1] [x; zeta] = 0,
  //   lower_bound <= zeta <= upper_bound.
  //
  bool has_range_modifier = false;

  // For nontrivial rows with a range modifiers, this will store the index of
  // the row in the list of such range-modified rows.
  Int range_index = -1;

  // Whether this is an equality row converted from a greater row.
  bool converted_greater_row = false;

  // Whether this is an equality row converted from a lesser row.
  bool converted_lesser_row = false;

  // If this is an equality row converted from a greater or lesser row, this is
  // the index of the conversion.
  Int converted_inequality_index = -1;
};

// A specification of a RANGE constraint on a variable.
template <typename Real>
struct RangeData {
  // The name of the row that the RANGE constraint is applied to.
  std::string row_name;

  // If true, the RANGE modifier is specified using the lower bound and a gap
  // size, otherwise it is an upper bound and a gap size.
  bool rhs_is_lower_bound;

  // The distance between the lower and upper bound.
  Real range_size;

  // The value and index of the lower bound.
  Real lower_bound = -std::numeric_limits<Real>::infinity();
  Int lower_bound_index = -1;

  // The value and index of the upper bound.
  Real upper_bound = std::numeric_limits<Real>::infinity();
  Int upper_bound_index = -1;
};

// Metadata for a non-artificial variable.
template <typename Real>
struct VariableMetadata {
  // The column index assigned to this variable.
  Int index = -1;

  // The number of nonzeros interacting with this variable.
  Int num_nonzeros = 0;

  // Specification of a possible lower bound on the variable.
  bool lower_bounded = false;
  Int lower_bound_index = -1;
  Real lower_bound;

  // Specification of a possible upper bound on the variable.
  bool upper_bounded = false;
  Int upper_bound_index = -1;
  Real upper_bound;

  // Specification of a possible fixed value for the variable.
  bool fixed = false;
  Real fixed_value;

  // Specification of whether the variable is free.
  bool free = false;
  Int free_index = -1;
};

template <typename Real>
struct TrivialEqualityData {
  std::string variable_name;
  Real single_nonzero;
};

enum QPMatrixType {
  QP_OBJECTIVE_SHIFT,      // A constant shift for the objective
  QP_LINEAR_OBJECTIVE,     // The 'c' in "c' x"
  QP_QUADRATIC_OBJECTIVE,  // The 'Q' in "(1 / 2) x' Q x"
  QP_EQUALITY_MATRIX,      // The 'A' in 'A x = b'
  QP_EQUALITY_RHS,         // The 'b' in 'A x = b'
  QP_CONE_MATRIX,          // The 'G' in  'G x <= h'
  QP_CONE_RHS              // The 'h' in 'G x <= h'
};

template <typename Real>
struct QPEntry {
  QPMatrixType type;
  Int row;
  Int column;
  Real value;
};

// We form either the "bounded" primal problem
//
//   arginf_{x,s} { c' x + (1 / 2) x' Q x | A x = b, l <= x <= u },
//
// or the "standard" primal problem
//
//   arginf_{x,s} { c' x + (1 / 2) x' Q x | A x = b, x >= 0 },
//
// where the 'fixed' rows occur in the final positions of the vector 'x'.
//
template <typename Real>
class Reader {
 public:
  // Constructs an MPS reader based upon a configuration.
  Reader(const ReadQuadraticProgramMPSConfig& config);

  // Returns the list of entries from the file. This should be called after
  // a call to 'Info'.
  std::vector<QPEntry<Real>> GetEntries();

  // Returns a reference to the constructed MPS metadata.
  const QuadraticProgramMPSInfo<Real>& Info() const;

  // The list of explicit lower bounds, if they exist, for the primal solution
  // variables.
  const Buffer<Real>& LowerBounds() const;

  // The list of explicit upper bounds, if they exist, for the primal solution
  // variables.
  const Buffer<Real>& UpperBounds() const;

  // Returns a dictionary mapping variable names to their (bound) metadata.
  const std::map<std::string, VariableMetadata<Real>>& VariableMap() const;

  // Returns a list of lower and upper bound pairs resulting from RANGES
  // constraints.
  const std::vector<RangeData<Real>>& RangeList() const;

  // Returns whether a line of an MPS file is a comment.
  static bool IsCommentLine(std::stringstream& line_stream);

  // Returns whether a line of an MPS file contains constraint data.
  static bool IsDataLine(std::stringstream& line_stream);

  // Converts a section token of an MPS file into the corresponding Section.
  static Section DecodeSection(const std::string& token);

  // Converts a row type token of an MPS file into the corresponding Rowtype.
  static RowType DecodeRowType(const std::string& token);

 private:
  // A copy of the configuration used to construct the reader.
  ReadQuadraticProgramMPSConfig config_;

  // An input stream corresponding to the MPS file.
  std::ifstream file_;

  // The resulting summary of the MPS file.
  QuadraticProgramMPSInfo<Real> info_;

  // A dictionary mapping row names to their corresponding row metadata.
  std::map<std::string, RowMetadata> row_map_;

  // A dictionary mapping variable names to their corresponding metadata.
  std::map<std::string, VariableMetadata<Real>> variable_map_;

  // A list of lower and upper bound pairs resulting from RANGES constraints.
  std::vector<RangeData<Real>> range_list_;

  // A map from the original MPS_EQUALITY_ROW row name to the trivial equality
  // struct, which stores the variable name and the floating-point value
  // of the single nonzero in the row (eventually).
  std::map<std::string, TrivialEqualityData<Real>> trivial_equality_map_;

  // The set of <= rows which have no nonzeros and are therefore of the form
  // 0 x = 0 <= upper_bound.
  std::set<std::string> empty_lesser_rows_;

  // The set of >= rows which have no nonzeros and are therefore of the form
  // 0 x = 0 >= lower_bound.
  std::set<std::string> empty_greater_rows_;

  // The number of MPS_LESSER_ROW's and MPS_GREATER_ROW's to be converted into
  // MPS_EQUALITY_ROW's due to config_.explicit_bounds being true.
  Int num_converted_inequality_rows_ = 0;

  // The RHS section typically has a name followed by either one or two pairs
  // per row, but some models (e.g., dfl001.mps) do not involve a name.
  bool initialized_rhs_section_ = false;
  bool rhs_has_name_;

  // The RANGES section typically has a name followed by either one or two
  // pairs per row, but some models do not involve a name.
  bool initialized_ranges_section_ = false;
  bool ranges_has_name_;

  // The BOUNDS section typically has a bound type marker, followed by a
  // bound set name, followed by a variable name, and, if applicable,
  // a numeric value). But some models (e.g., dfl001.mps) do not involve a
  // bound set name.
  bool initialized_bounds_section_ = false;
  bool bounds_has_name_;

  // If 'config_.explicit_bounds' was true, then this will contain the list of
  // values in [-infinity, infinity) of the lower bounds on the primal solution
  // variables.
  Buffer<Real> lower_bounds_;

  // If 'config_.explicit_bounds' was true, then this will contain the list of
  // values in (-infinity, infinity] of the upper bounds on the primal solution
  // variables.
  Buffer<Real> upper_bounds_;

  // Incorporates the metadata from a row data line.
  void ProcessRowDataLine(const std::string& line);

  // Incorporates the metadata from a column data line.
  void ProcessColumnDataLine(const std::string& line);

  // Incorporates the metadata from a quadratic objective data line.
  void ProcessQuadraticObjectiveDataLine(const std::string& line);

  // Enqueues nonzeros using a column data line.
  void QueueColumnDataLine(const std::string& line,
                           std::vector<QPEntry<Real>>* queued_entries);

  // Enqueues nonzero(s) from a quadratic objective data line.
  void QueueQuadraticObjectiveDataLine(
      const std::string& line, std::vector<QPEntry<Real>>* queued_entries);

  // Incorporates the metadata from an RHS data line.
  void ProcessRHSDataLine(const std::string& line);

  // Enqueues nonzeros using an RHS data line.
  void QueueRHSDataLine(const std::string& line,
                        std::vector<QPEntry<Real>>* queued_entries);

  // Incorporates the metadata from a bounds data line.
  void ProcessBoundsDataLine(const std::string& line);

  // Incorporates the metadata from a RANGES data line.
  void ProcessRangesDataLine(const std::string& line);

  // Enqueues the entries associated with enforcing the variable bounds.
  void QueueVariableBounds(std::vector<QPEntry<Real>>* queued_entries);

  // After reading in the row data, this can be used to fast-forward to see if
  // apply any 'RANGES' modifiers before processing the column data.
  //
  // Any 'RANGES' induced constraint of the form
  //
  //   lower_bound <= g x <= upper_bound
  //
  // can be expressed as
  //
  //   [g, -1] [x; zeta] = 0,
  //   lower_bound <= zeta <= upper_bound,
  //
  // by introducing the auxiliary variable 'zeta'. And, if we are to use this
  // approach, the initial 'EQUALITY', 'GREATER', or 'LESSER' row 'g' becomes
  // an 'EQUALITY' row, and so we temporarily fast-forward through the file to
  // check for 'RANGES' modifiers after processing the 'ROWS' section.
  void PeekAtRanges(const std::string& section_token);

  // After reading in the column data, we can iterate through the row map to
  // delete any empty rows and convert any equality rows with a single nonzero
  // to a fixed state.
  void SimplifyRowTypes();

  // After having deleted empty rows, we can iterate over the Range list
  // and assign the unique range index associated with each range modifier
  // within the RowMetadata.
  void SyncRangeData();

  // Make another pass through the COLUMNS section to store the variable name
  // associated with each trivial equality row.
  void StoreTrivialEqualityRowVariableNames(
      const std::ifstream::pos_type& column_section_beg);

  // Iterate through the variable map and make use of the requested
  // conventions for ensuring consistent / default bound types.
  //
  // We return true if the bounds are trivially consistent, and false if the
  // problem is obviously primal infeasible.
  bool FinalizeBoundTypes();

  // Compute the number of bounds of each type and store the indices of each
  // bound within its type list.
  void CountBoundTypes();
};

}  // namespace mps
}  // namespace conic

#include "conic/qp_mps-impl.hpp"

#endif  // ifndef CONIC_QP_MPS_H_
