/*
 * Copyright (c) 2019 Jack Poulson <jack@hodgestar.com>
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
#ifndef CONIC_IMPORTS_H_
#define CONIC_IMPORTS_H_

#include "catamari.hpp"

#ifdef CATAMARI_DEBUG
#define CONIC_DEBUG
#endif  // ifdef CATAMARI_DEBUG

#define CONIC_ASSERT(condition, msg) CATAMARI_ASSERT(condition, msg)

namespace conic {

using catamari::BlasMatrix;
using catamari::BlasMatrixView;
using catamari::Buffer;
using catamari::ComplexBase;
using catamari::ConstBlasMatrixView;
using catamari::CoordinateMatrix;
using catamari::DisableIf;
using catamari::EnableIf;
using catamari::EuclideanNorm;
using catamari::Int;
using catamari::MatrixEntry;
using catamari::MaxNorm;
using catamari::RefinedSolveControl;
using catamari::SparseLDL;
using catamari::SparseLDLControl;
using catamari::SparseLDLResult;

// TODO(Jack Poulson): Move this implementation into catamari.
template <typename T>
void Copy(const ConstBlasMatrixView<T>& input, BlasMatrixView<T>* output) {
  const Int height = input.Height();
  const Int width = input.Width();
  for (Int j = 0; j < width; ++j) {
    for (Int i = 0; i < height; ++i) {
      output->Entry(i, j) = input(i, j);
    }
  }
}

// TODO(Jack Poulson): Move into catamari.
template <typename Real>
void FormSparseTranspose(const CoordinateMatrix<Real>& matrix,
                         CoordinateMatrix<Real>* transpose_matrix) {
  const Int height = matrix.NumRows();
  const Int width = matrix.NumColumns();
  transpose_matrix->Resize(width, height);
  const auto& entries = matrix.Entries();
  transpose_matrix->ReserveEntryAdditions(entries.Size());
  for (const auto& entry : entries) {
    transpose_matrix->QueueEntryAddition(entry.column, entry.row, entry.value);
  }
  transpose_matrix->FlushEntryQueues();
}

}  // namespace conic

#endif  // ifndef CONIC_IMPORTS_H_
