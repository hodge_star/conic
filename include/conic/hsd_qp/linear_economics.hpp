/*
 * Copyright (c) 2019 Jack Poulson <jack@hodgestar.com>
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
#ifndef CONIC_HSD_QP_LINEAR_ECONOMICS_H_
#define CONIC_HSD_QP_LINEAR_ECONOMICS_H_

#include "conic/hsd_qp/problem.hpp"
#include "conic/imports.hpp"
#include "conic/util.hpp"

namespace conic {
namespace hsd_qp {

// Given a payoff matrix P, the left, 'x', player, solves:
//
//   min_{x in P_n} max_{y in P_n} x' P y,
//
// where P_n is the probability simplex of order n,
//
//   P_n = { x in R_+^n : \sum_{i=1}^n x_i = 1 }.
//
// Similarly, the right, 'y', player, solves:
//
//   max_{y in P_n} min_{x in P_n} x' P y.
//
// The left player's problem is equivalent to:
//
//   min_{x in P_n} max_i e_i' P' x.
//
// This problem can be formulated as:
//
//   min_{x, u} { u : [-P', e] [x; u] >= 0, e' x = 1, x >= 0 },
//
// or
//
//   min_{x, u_+, u_-} { u_+ - u_- :
//       [-P', e, -e] [x; u_+, u_-] >= 0, e' x = 1, x, u_+, u_- >= 0 }.
//
// In full form, this becomes
//
//   min_{x, u_+, u_-, s } { |  0 |' |  x  | :
//                           |  1 |  | u_+ |
//                           | -1 |  | u_- |
//                           |  0 |  |  s  |
//
//     | -P', e, -e, -I | |  x  | = | 0 |,  |  x  | >= 0 }.
//     |  e', 0   0   0 | | u_+ |   | 1 |   | u_+ |
//                        | u_- |           | u_- |
//                        |  s  |           |  s  |
//
// The dual problem is then of the form:
//
//   max_{y, w, z, v_+, v_-, t} { | 0 |' | y | :
//                                | 1 |  | w |
//
//     | -P,  e | | y | + |  z  | = |  0 |,  |  z  | >= 0 }.
//     |  e', 0 | | w |   | v_+ |   |  1 |   | v_+ |
//     | -e', 0 |         | v_- |   | -1 |   | v_- |
//     | -I,  0 |         |  t  |   |  0 |   |  t  |
//
template <typename Real>
void MatrixGameProblem(const CoordinateMatrix<Real>& payoff_matrix,
                       Problem<Real>* problem);

// As an alternative to the above standard approach, we can use the approach of:
//
//   Gale, D., Kuhn, H.W., and Tucker, A.W.,
//   "Linear Programming and the Theory of Games", Section 11,
//   "The Simplex Method for Solving Matrix Games", Equation 1, 1951.
//
// It returns a linear programming program equivalent with the mixed-strategy
// matrix game with the given m x n payoff matrix, P. The minimum value of the
// payoff matrix is computed, and then a positive shift, alpha = 1 - min(P), is
// added to each entry of P to define a matrix A = P + alpha * ones(m, n).
//
// A linear program,
//
//   arginf_{x, s} { 1_n' x : [A, -I] [x; s] = 1_m, x, s >= 0 }
//
// is then constructed. The 'x' component of its solution implies an optimal
// value of 1 / one_norm(x) - alpha and an optimal column maximization strategy
// of x / one_norm(x).
//
// The associated dual problem is
//
//   argsup_{y, z} { 1_m' y : [A'; -I] y + [z_T; z_B] = 1_{m + n}, z >= 0 },
//
// which provides an optimal row minimization strategy of y / one_norm(y).
//
template <typename Real>
Real MatrixGameGKTProblem(const CoordinateMatrix<Real>& payoff_matrix,
                          Problem<Real>* problem);

// Returns the row minimization mixed strategy, 'u', and column maximization
// mixed strategy, 'v', of the matrix game with the given payoff matrix, 'P'.
//
// That is, given a payoff matrix P, we associate the row minimization mixed
// strategy with a vector 'u' and the column maximization strategy with a vector
// 'v', each having one-norm of 1, such that the associated payout is
//
//   u' P v.
//
// Care should therefore be taken to ensure the appropriate transposition of
// the payout matrix, as conventions vary.
//
template <typename Real>
Real MatrixGame(const CoordinateMatrix<Real>& payoff_matrix,
                const SolveControl<Real>& control,
                bool use_gale_kuhn_tucker_approach,
                BlasMatrix<Real>* row_minimization_strategy,
                BlasMatrix<Real>* column_maximization_strategy);

// Constructs a transportation problem in the sense of David Gale's The Theory
// of Linear Economic Models, which involves finding a cheapest possible means
// of moving goods from the m "production centers", P_1, ..., P_m, to the
// "markets", M_1, ..., M_n, in a manner which meets the market demands without
// exceeding the production supplies.
//
// The transportation costs from the production centers to markets are typically
// provided in a cost matrix of the form:
//
//             M_1      ...     M_n
//          ---------------------------
//     P_1 | c_{1, 1}   ...  c_{1, n}
//      .  |    .       .        .
//      .  |    .        .       .
//      .  |    .         .      .
//     P_m | c_{m, 1}   ...  c_{m, n}
//
// We formulate the problem as a standard Linear Program in the manner suggested
// by Gale.
//
template <typename Real>
void TransportationProblem(const BlasMatrix<Real>& costs,
                           const BlasMatrix<Real>& supplies,
                           const BlasMatrix<Real>& demands,
                           Problem<Real>* problem);

// Constructs a linear programming formulation of an optimal assignment problem.
// Given m workers, W_1, ..., W_m, and n assignments, J_1, ..., J_n, where at
// most one worker can be assigned to each task at a time, and each worker can
// only be assigned to one task at a time, find the most effective assignment
// given each worker's rating for each task. The ratings are provided in an
// m x n matrix.
//
// The formulation is thus:
//
//   argsup_x { \sum_{i, j} r_{i, j} x_{i, j} :
//       \sum_j x_{i, j} = 1, for all i,
//       \sum_i x_{i, j} = 1, for all j,  0 <= x <= 1 }.
//
// While there will always exist an optimal solution with integral values, there
// is no reason to assume that an interior point method will return it.
template <typename Real>
void OptimalAssignment(const BlasMatrix<Real>& ratings, Problem<Real>* problem);

}  // namespace hsd_qp
}  // namespace conic

#include "conic/hsd_qp/linear_economics-impl.hpp"

#endif  // ifndef CONIC_HSD_QP_LINEAR_ECONOMICS_H_
