/*
 * Copyright (c) 2019 Jack Poulson <jack@hodgestar.com>
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
#ifndef CONIC_HSD_QP_FIRST_ORDER_OPTIMALITY_IMPL_H_
#define CONIC_HSD_QP_FIRST_ORDER_OPTIMALITY_IMPL_H_

#include <exception>

#include "conic/hsd_qp/first_order_optimality.hpp"

namespace conic {
namespace hsd_qp {

// We compute the regularization values *before* adding in the quadratic
// objective, as we could have an extremely ill-conditioned Q with unit
// entries, such as 4 * jordan(1 / 2, n) * jordan(1 / 2, n)', which has
// diagonal entries [1, 5, 5, ..., 5], but has a condition number growing
// exponentially in n -- due to an exponentially small positive eigenvalue,
// as the two-norm is bounded from above by 9.
template <typename Real>
bool PrimalSolutionProximalRegularization(
    const Problem<Real>& problem, const Iterate<Real>& iterate,
    Real augmented_scale, const RegularizationState<Real>& reg_state,
    bool maintain_minimum, BlasMatrix<Real>* factor_theta,
    BlasMatrix<Real>* target_primal_solution_prox_reg,
    BlasMatrix<Real>* factor_primal_solution_prox_reg) {
  const Int num_primal = problem.equality_matrix.NumColumns();
  const ConstBlasMatrixView<Real>& primal_solution = iterate.PrimalSolution();
  const ConstBlasMatrixView<Real>& dual_slack = iterate.DualSlack();

  target_primal_solution_prox_reg->Resize(num_primal, 1);
  factor_primal_solution_prox_reg->Resize(num_primal, 1);
  factor_theta->Resize(num_primal, 1);
  for (Int i = 0; i < num_primal; ++i) {
    factor_theta->Entry(i) = dual_slack(i) / primal_solution(i);
  }
  for (Int i = 0; i < num_primal; ++i) {
    if (maintain_minimum) {
      target_primal_solution_prox_reg->Entry(i) =
          std::max(augmented_scale * reg_state.target_primal_solution_prox_reg -
                       factor_theta->Entry(i),
                   Real(0));
      factor_primal_solution_prox_reg->Entry(i) = std::max(
          augmented_scale * reg_state.factored_primal_solution_prox_reg -
              factor_theta->Entry(i),
          Real(0));
    } else {
      target_primal_solution_prox_reg->Entry(i) =
          augmented_scale * reg_state.target_primal_solution_prox_reg;
      factor_primal_solution_prox_reg->Entry(i) =
          augmented_scale * reg_state.factored_primal_solution_prox_reg;
    }

    factor_theta->Entry(i) += factor_primal_solution_prox_reg->Entry(i);
  }

  bool diagonal_quadratic = true;
  for (const auto& entry : problem.quadratic_objective.Entries()) {
    if (entry.row == entry.column) {
      factor_theta->Entry(entry.row) += entry.value;
    } else {
      diagonal_quadratic = false;
    }
  }

  return diagonal_quadratic;
}

template <typename Real>
void DualSolutionProximalRegularization(
    const Problem<Real>& problem, const BlasMatrix<Real>& factor_theta,
    Real augmented_scale, const RegularizationState<Real>& reg_state,
    bool row_dependent_dual_solution_reg,
    BlasMatrix<Real>* target_dual_solution_prox_reg,
    BlasMatrix<Real>* factor_dual_solution_prox_reg) {
  const Int num_dual = problem.equality_matrix.NumRows();
  target_dual_solution_prox_reg->Resize(num_dual, 1);
  factor_dual_solution_prox_reg->Resize(num_dual, 1);
  if (row_dependent_dual_solution_reg) {
    // Compute the dual solution proximal regularization diagonal given the
    // rescaled squared norms || A(i, :) / sqrt(Theta) ||_2^2.
    const Buffer<MatrixEntry<Real>>& entries =
        problem.equality_matrix.Entries();
    for (Int i = 0; i < num_dual; ++i) {
      Real row_squared_norm = 0;
      const Int row_entry_offset = problem.equality_matrix.RowEntryOffset(i);
      const Int num_row_entries = problem.equality_matrix.NumRowEntries(i);
      for (Int k = 0; k < num_row_entries; ++k) {
        const MatrixEntry<Real>& entry = entries[row_entry_offset + k];
        row_squared_norm +=
            entry.value * entry.value / factor_theta(entry.column);
      }

      const Real scale = std::max(row_squared_norm, Real(1));

      target_dual_solution_prox_reg->Entry(i) =
          reg_state.target_dual_solution_prox_reg * scale;
      factor_dual_solution_prox_reg->Entry(i) =
          reg_state.factored_dual_solution_prox_reg * scale;
    }
  } else {
    const Real target_value =
        reg_state.target_dual_solution_prox_reg * augmented_scale;
    const Real factor_value =
        reg_state.factored_dual_solution_prox_reg * augmented_scale;
    for (Int i = 0; i < num_dual; ++i) {
      target_dual_solution_prox_reg->Entry(i) = target_value;
      factor_dual_solution_prox_reg->Entry(i) = factor_value;
    }
  }
}

// Form the target and factorization full KKT matrices
//
//    |     -(Q + R_x)         A'  |       -c       | I      |
//    |          A            R_y  |       -b       |        |
//    |----------------------------|----------------|--------|
//  S |  -(c + 2 Q x / tau)'   b'  | x' Q x / tau^2 |    -1  | S,
//    |----------------------------|----------------|--------|
//    |          Z                 |                | X      |
//    |                            |      kappa     |    tau |
//
// where S = diag(s_x, s_y, sigma_tau, s_z, sigma_kappa).
//
// We take as input the prescaled
//
//   bottom_left_block = sigma_tau [-(c + 2 Q x / tau)', b'] S_aug
//
// and
//
//   upper_right_block = S_aug [-c; -b] sigma_tau,
//
// as well as the prescaled
//
//   bottom_right_objective = sigma_tau (x' Q x / tau^2) sigma_tau.
//
template <typename Real>
void FillFullKKTMatrix(
    const Problem<Real>& problem, const Iterate<Real>& iterate,
    const ConstBlasMatrixView<Real>& diagonal_scale,
    const ConstBlasMatrixView<Real>& bottom_left_block,
    const ConstBlasMatrixView<Real>& upper_right_block,
    Real bottom_right_objective,
    const ConstBlasMatrixView<Real>& primal_solution_prox_reg,
    const ConstBlasMatrixView<Real>& dual_solution_prox_reg, bool initialize,
    CoordinateMatrix<Real>* full_kkt) {
  const Int num_primal = problem.equality_matrix.NumColumns();
  const Int num_dual = problem.equality_matrix.NumRows();
  const Int tau_offset = num_primal + num_dual;
  const Int dual_slack_offset = num_primal + num_dual + 1;
  const Int kappa_offset = 2 * num_primal + num_dual + 1;
  const ConstBlasMatrixView<Real>& primal_solution = iterate.PrimalSolution();
  const ConstBlasMatrixView<Real>& dual_slack = iterate.DualSlack();
  const Real tau = iterate.Tau();
  const Real kappa = iterate.Kappa();

  auto place_entry = [&](Int row, Int column, Real value) {
    const Real scaled_value =
        diagonal_scale(row) * value * diagonal_scale(column);
    if (initialize) {
      full_kkt->QueueEntryAddition(row, column, scaled_value);
    } else {
      full_kkt->ReplaceEntry(row, column, scaled_value);
    }
  };

  auto add_entry = [&](Int row, Int column, Real value) {
    const Real scaled_value =
        diagonal_scale(row) * value * diagonal_scale(column);
    if (initialize) {
      full_kkt->QueueEntryAddition(row, column, scaled_value);
    } else {
      full_kkt->AddToEntry(row, column, scaled_value);
    }
  };

  auto place_prescaled_entry = [&](Int row, Int column, Real value) {
    if (initialize) {
      full_kkt->QueueEntryAddition(row, column, value);
    } else {
      full_kkt->ReplaceEntry(row, column, value);
    }
  };

  if (initialize) {
    const Int full_kkt_size = 2 * num_primal + num_dual + 2;
    const Int num_full_kkt_entries = 2 * problem.equality_matrix.NumEntries() +
                                     problem.quadratic_objective.NumEntries() +
                                     1 + 2 * (num_primal + num_dual) +
                                     2 * num_primal + 2 + full_kkt_size;
    full_kkt->Resize(full_kkt_size, full_kkt_size);
    full_kkt->ReserveEntryAdditions(num_full_kkt_entries);
  }
  for (const auto& entry : problem.equality_matrix.Entries()) {
    const Int row = num_primal + entry.row;
    const Int column = entry.column;
    place_entry(row, column, entry.value);
    place_entry(column, row, entry.value);
  }
  for (Int i = 0; i < num_primal + num_dual; ++i) {
    place_prescaled_entry(tau_offset, i, bottom_left_block(0, i));
    place_prescaled_entry(i, tau_offset, upper_right_block(i, 0));
  }
  place_prescaled_entry(tau_offset, tau_offset, bottom_right_objective);
  for (Int i = 0; i < num_primal; ++i) {
    const Int row = dual_slack_offset + i;
    const Int column = i;
    place_entry(row, column, dual_slack(i));
    place_entry(column, row, Real(1));
  }
  {
    const Int row = kappa_offset;
    const Int column = tau_offset;
    place_entry(row, column, kappa);
    place_entry(column, row, Real(-1));
  }
  for (Int i = 0; i < num_primal; ++i) {
    const Int row = dual_slack_offset + i;
    place_entry(row, row, primal_solution(i));
  }
  {
    const Int row = kappa_offset;
    place_entry(row, row, tau);
  }
  if (problem.quadratic_objective.Entries().Empty()) {
    for (Int i = 0; i < num_primal; ++i) {
      const Int row = i;
      place_entry(row, row, -primal_solution_prox_reg(i));
    }
  } else {
    if (!initialize) {
      for (Int i = 0; i < num_primal; ++i) {
        const Int row = i;
        place_entry(row, row, Real(0));
      }
    }
    for (const auto& entry : problem.quadratic_objective.Entries()) {
      place_entry(entry.row, entry.column, -entry.value);
    }
    for (Int i = 0; i < num_primal; ++i) {
      const Int row = i;
      add_entry(row, row, -primal_solution_prox_reg(i));
    }
  }
  for (Int i = 0; i < num_dual; ++i) {
    const Int row = num_primal + i;
    place_entry(row, row, dual_solution_prox_reg(i));
  }
  if (initialize) {
    full_kkt->FlushEntryQueues();
  }
}

template <typename Real>
void FirstOrderOptimality<Real>::InitializeMatrix(
    const Problem<Real>& problem, const Iterate<Real>& iterate,
    const ConstBlasMatrixView<Real>& diagonal_scale,
    const RegularizationState<Real>& reg_state, bool factor_schur_complement) {
  const Int num_primal = problem.equality_matrix.NumColumns();
  const Int num_dual = problem.equality_matrix.NumRows();
  const Int tau_offset = num_primal + num_dual;
  const ConstBlasMatrixView<Real> primal_solution = iterate.PrimalSolution();
  const ConstBlasMatrixView<Real> dual_slack = iterate.DualSlack();
  const Real tau = iterate.Tau();

  factor_schur_complement_ = factor_schur_complement;

  // We introduce a diagonal congruence rescaling via
  //
  //     S = diag(s_x, s_y, sigma_tau, s_z, sigma_kappa)
  //
  // to
  //
  //  |     -(Q + R_x)         A'  |       -c       | I      |
  //  |          A            R_y  |       -b       |        |
  //  |----------------------------|----------------|--------|
  //  |  -(c + 2 Q x / tau)'   b'  | x' Q x / tau^2 |    -1  |,
  //  |----------------------------|----------------|--------|
  //  |          Z                 |                | X      |
  //  |                            |      kappa     |    tau |
  //
  // which has a Schur complement of
  //
  //       | -(Q + R_x + Z inv(X))   A'  |              -c              |
  //       |           A            R_y  |              -b              |
  // S_aug |-----------------------------|------------------------------| S_aug,
  //       |  -(c + 2 Q x / tau)'    b'  | x' Q x / tau^2 + kappa / tau |
  //
  // where S_aug = diag(s_x, s_y, sigma_tau).
  //
  // Likewise, the Schur complement reduction to the dual solution variable is
  //
  //   S_y (R_y + A inv(Q + R_x + Z inv(X)) A') S_y.
  //
  // In each case, the Schur complement is the congruence transformation via the
  // restricted scaling matrix of the unscaled result.
  //

  Real augmented_scale = Real(1);
  const bool relative_augmented_scale = false;
  if (relative_augmented_scale) {
    for (Int i = 0; i < num_primal; ++i) {
      const Real delta = dual_slack(i) / primal_solution(i);
      augmented_scale = std::max(augmented_scale, delta);
    }
  }

  BlasMatrix<Real> target_primal_solution_prox_reg;
  BlasMatrix<Real> factor_primal_solution_prox_reg;
  const bool diagonal_quadratic = PrimalSolutionProximalRegularization(
      problem, iterate, augmented_scale, reg_state,
      maintain_primal_solution_minimum_, &factor_theta_,
      &target_primal_solution_prox_reg, &factor_primal_solution_prox_reg);
  if (factor_schur_complement_ && !diagonal_quadratic) {
    std::cerr << "Disabling Schur complement factorization due to non-diagonal "
                 "quadratic objective."
              << std::endl;
    factor_schur_complement_ = false;
  }

  BlasMatrix<Real> target_dual_solution_prox_reg;
  BlasMatrix<Real> factor_dual_solution_prox_reg;
  DualSolutionProximalRegularization(
      problem, factor_theta_, augmented_scale, reg_state,
      row_dependent_dual_solution_reg_, &target_dual_solution_prox_reg,
      &factor_dual_solution_prox_reg);

  // Build the regularized version of the J00 matrix, which becomes symmetric
  // quasi-definite.
  auto queue_factor_upper_left = [&](Int row, Int column, Real value) {
    const Real scaled_value =
        diagonal_scale(row) * value * diagonal_scale(column);
    factor_upper_left_block_.QueueEntryAddition(row, column, scaled_value);
  };
  factor_upper_left_block_.Resize(num_primal + num_dual, num_primal + num_dual);
  factor_upper_left_block_.ReserveEntryAdditions(
      num_primal + num_dual + 2 * problem.equality_matrix.NumEntries() +
      problem.quadratic_objective.NumEntries());
  for (Int i = 0; i < num_primal; ++i) {
    queue_factor_upper_left(i, i, -factor_theta_(i));
  }
  for (const auto& entry : problem.quadratic_objective.Entries()) {
    if (entry.row == entry.column) {
      // We already handled this value in factor_theta_.
      continue;
    }
    queue_factor_upper_left(entry.row, entry.column, -entry.value);
  }
  for (const auto& entry : problem.equality_matrix.Entries()) {
    const Int row = num_primal + entry.row;
    const Int column = entry.column;
    queue_factor_upper_left(row, column, entry.value);
    queue_factor_upper_left(column, row, entry.value);
  }
  for (Int i = 0; i < num_dual; ++i) {
    const Int row = num_primal + i;
    const Real value = factor_dual_solution_prox_reg(i);
    queue_factor_upper_left(row, row, value);
  }
  factor_upper_left_block_.FlushEntryQueues();

  // Form the upper-right block:
  //  | S_x     | | -c | sigma_tau
  //  |     S_y | | -b |
  auto place_upper_right = [&](Int row, Real value) {
    const Real scaled_value =
        diagonal_scale(row) * value * diagonal_scale(tau_offset);
    upper_right_block_(row, 0) = scaled_value;
  };
  upper_right_block_.Resize(num_primal + num_dual, 1);
  for (Int i = 0; i < num_primal; ++i) {
    place_upper_right(i, -problem.linear_objective(i));
  }
  for (Int i = 0; i < num_dual; ++i) {
    place_upper_right(num_primal + i, -problem.equality_rhs(i));
  }

  // Form the lower-left block:
  //   sigma_tau | -(c + 2 Q x / tau)'  b' | | S_x     |
  //                                         |     S_y |
  auto place_lower_left = [&](Int column, Real value) {
    const Real scaled_value =
        diagonal_scale(tau_offset) * value * diagonal_scale(column);
    bottom_left_block_(0, column) = scaled_value;
  };
  bottom_left_block_.Resize(1, num_primal + num_dual);
  if (problem.quadratic_objective.Entries().Empty()) {
    for (Int i = 0; i < num_primal; ++i) {
      place_lower_left(i, -problem.linear_objective(i));
    }
  } else {
    BlasMatrix<Real> combined_objective = problem.linear_objective;
    catamari::ApplySparse(Real(2) / tau, problem.quadratic_objective,
                          primal_solution, Real(1), &combined_objective.view);
    for (Int i = 0; i < num_primal; ++i) {
      place_lower_left(i, -combined_objective(i));
    }
  }
  for (Int i = 0; i < num_dual; ++i) {
    place_lower_left(num_primal + i, problem.equality_rhs(i));
  }

  // Form the bottom-right entry:
  //   sigma_tau (x' Q x / tau^2 + kappa / tau) sigma_tau.
  Real bottom_right_objective = 0;
  for (const auto& entry : problem.quadratic_objective.Entries()) {
    bottom_right_objective += entry.value * primal_solution(entry.row) *
                              primal_solution(entry.column) / (tau * tau);
  }
  bottom_right_entry_ =
      iterate.Kappa() / iterate.Tau() + bottom_right_objective;
  bottom_right_entry_ *=
      diagonal_scale(tau_offset) * diagonal_scale(tau_offset);
  bottom_right_objective *=
      diagonal_scale(tau_offset) * diagonal_scale(tau_offset);

  FillFullKKTMatrix(problem, iterate, diagonal_scale,
                    bottom_left_block_.ConstView(),
                    upper_right_block_.ConstView(), bottom_right_objective,
                    target_primal_solution_prox_reg.ConstView(),
                    target_dual_solution_prox_reg.ConstView(),
                    true /* initialize */, &target_kkt_);
  FillFullKKTMatrix(problem, iterate, diagonal_scale,
                    bottom_left_block_.ConstView(),
                    upper_right_block_.ConstView(), bottom_right_objective,
                    factor_primal_solution_prox_reg.ConstView(),
                    factor_dual_solution_prox_reg.ConstView(),
                    true /* initialize */, &factor_kkt_);

  if (factor_schur_complement_) {
    // Queue the S_y A inv(Q + R_x + inv(X) Z) A' S_y updates.
    problem.FormEqualityMatrixTranspose();
    const Buffer<MatrixEntry<Real>>& equality_entries =
        problem.equality_matrix_transpose.Entries();

    Int num_outer_product_entries = 0;
    for (Int i = 0; i < num_primal; ++i) {
      const Int num_col_nonzeros =
          problem.equality_matrix_transpose.NumRowEntries(i);
      num_outer_product_entries += num_col_nonzeros * num_col_nonzeros;
    }

    auto queue_upper_left_schur = [&](Int row, Int column, Real value) {
      const Real scaled_value = diagonal_scale(num_primal + row) * value *
                                diagonal_scale(num_primal + column);
      upper_left_schur_complement_.QueueEntryAddition(row, column,
                                                      scaled_value);
    };

    upper_left_schur_complement_.Resize(num_dual, num_dual);
    upper_left_schur_complement_.ReserveEntryAdditions(
        num_outer_product_entries);
    for (Int i = 0; i < num_primal; ++i) {
      const Int col_offset =
          problem.equality_matrix_transpose.RowEntryOffset(i);
      const Int num_col_nonzeros =
          problem.equality_matrix_transpose.NumRowEntries(i);
      for (Int k = 0; k < num_col_nonzeros; ++k) {
        const MatrixEntry<Real>& entry_k = equality_entries[col_offset + k];
        const Int row = entry_k.column;
        for (Int l = 0; l < num_col_nonzeros; ++l) {
          const MatrixEntry<Real>& entry_l = equality_entries[col_offset + l];
          const Int column = entry_l.column;
          const Real value = entry_k.value * entry_l.value / factor_theta_(i);
          queue_upper_left_schur(row, column, value);
        }
      }
    }
    upper_left_schur_complement_.FlushEntryQueues();

    // Shift the diagonal upwards.
    upper_left_schur_complement_.ReserveEntryAdditions(num_dual);
    for (Int i = 0; i < num_dual; ++i) {
      const Real value = factor_dual_solution_prox_reg(i);
      queue_upper_left_schur(i, i, value);
    }
    upper_left_schur_complement_.FlushEntryQueues();
  }

  factored_ = false;
  refactoring_ = false;
}

template <typename Real>
void FirstOrderOptimality<Real>::ModifyMatrix(
    const Problem<Real>& problem, const Iterate<Real>& iterate,
    const ConstBlasMatrixView<Real>& diagonal_scale,
    const RegularizationState<Real>& reg_state) {
  const Int num_primal = problem.equality_matrix.NumColumns();
  const Int num_dual = problem.equality_matrix.NumRows();
  const Int tau_offset = num_primal + num_dual;
  const ConstBlasMatrixView<Real> primal_solution = iterate.PrimalSolution();
  const ConstBlasMatrixView<Real> dual_slack = iterate.DualSlack();
  const Real tau = iterate.Tau();

  Real augmented_scale = Real(1);
  const bool relative_augmented_scale = false;
  if (relative_augmented_scale) {
    for (Int i = 0; i < num_primal; ++i) {
      const Real delta = dual_slack(i) / primal_solution(i);
      augmented_scale = std::max(augmented_scale, delta);
    }
  }

  BlasMatrix<Real> target_primal_solution_prox_reg;
  BlasMatrix<Real> factor_primal_solution_prox_reg;
  PrimalSolutionProximalRegularization(
      problem, iterate, augmented_scale, reg_state,
      maintain_primal_solution_minimum_, &factor_theta_,
      &target_primal_solution_prox_reg, &factor_primal_solution_prox_reg);

  BlasMatrix<Real> target_dual_solution_prox_reg;
  BlasMatrix<Real> factor_dual_solution_prox_reg;
  DualSolutionProximalRegularization(
      problem, factor_theta_, augmented_scale, reg_state,
      row_dependent_dual_solution_reg_, &target_dual_solution_prox_reg,
      &factor_dual_solution_prox_reg);

  // Build the regularized version of the J00 matrix, which becomes symmetric
  // quasi-definite:
  //
  //   | S_x      | | -Theta   A' | | S_x      |.
  //   |      S_y | |    A    R_y | |      S_y |
  //
  auto place_factor_upper_left = [&](Int row, Int column, Real value) {
    const Real scaled_value =
        diagonal_scale(row) * value * diagonal_scale(column);
    factor_upper_left_block_.ReplaceEntry(row, column, scaled_value);
  };
  for (Int i = 0; i < num_primal; ++i) {
    place_factor_upper_left(i, i, -factor_theta_(i));
  }
  for (const auto& entry : problem.quadratic_objective.Entries()) {
    if (entry.row == entry.column) {
      // We already handled this value in factor_theta_.
      continue;
    }
    place_factor_upper_left(entry.row, entry.column, -entry.value);
  }
  for (const auto& entry : problem.equality_matrix.Entries()) {
    const Int row = num_primal + entry.row;
    const Int column = entry.column;
    place_factor_upper_left(row, column, entry.value);
    place_factor_upper_left(column, row, entry.value);
  }
  for (Int i = 0; i < num_dual; ++i) {
    const Int row = num_primal + i;
    const Real value = factor_dual_solution_prox_reg(i);
    place_factor_upper_left(row, row, value);
  }

  // Form the upper-right block:
  //  | S_x     | | -c | sigma_tau
  //  |     S_y | | -b |
  auto place_upper_right = [&](Int row, Real value) {
    const Real scaled_value =
        diagonal_scale(row) * value * diagonal_scale(tau_offset);
    upper_right_block_(row, 0) = scaled_value;
  };
  for (Int i = 0; i < num_primal; ++i) {
    place_upper_right(i, -problem.linear_objective(i));
  }
  for (Int i = 0; i < num_dual; ++i) {
    place_upper_right(num_primal + i, -problem.equality_rhs(i));
  }

  // Form the lower-left block:
  //   sigma_tau | -(c + 2 Q x / tau)'  b' | | S_x     |
  //                                         |     S_y |
  auto place_lower_left = [&](Int column, Real value) {
    const Real scaled_value =
        diagonal_scale(tau_offset) * value * diagonal_scale(column);
    bottom_left_block_(0, column) = scaled_value;
  };
  if (problem.quadratic_objective.Entries().Empty()) {
    for (Int i = 0; i < num_primal; ++i) {
      place_lower_left(i, -problem.linear_objective(i));
    }
  } else {
    BlasMatrix<Real> combined_objective = problem.linear_objective;
    catamari::ApplySparse(Real(2) / tau, problem.quadratic_objective,
                          primal_solution, Real(1), &combined_objective.view);
    for (Int i = 0; i < num_primal; ++i) {
      place_lower_left(i, -combined_objective(i));
    }
  }
  for (Int i = 0; i < num_dual; ++i) {
    place_lower_left(num_primal + i, problem.equality_rhs(i));
  }

  // Form the bottom-right entry:
  //   sigma_tau (x' Q x / tau^2 + kappa / tau) sigma_tau.
  Real bottom_right_objective = 0;
  for (const auto& entry : problem.quadratic_objective.Entries()) {
    bottom_right_objective += entry.value * primal_solution(entry.row) *
                              primal_solution(entry.column) / (tau * tau);
  }
  bottom_right_entry_ =
      iterate.Kappa() / iterate.Tau() + bottom_right_objective;
  bottom_right_entry_ *=
      diagonal_scale(tau_offset) * diagonal_scale(tau_offset);
  bottom_right_objective *=
      diagonal_scale(tau_offset) * diagonal_scale(tau_offset);

  FillFullKKTMatrix(problem, iterate, diagonal_scale,
                    bottom_left_block_.ConstView(),
                    upper_right_block_.ConstView(), bottom_right_objective,
                    target_primal_solution_prox_reg.ConstView(),
                    target_dual_solution_prox_reg.ConstView(),
                    false /* initialize */, &target_kkt_);
  FillFullKKTMatrix(problem, iterate, diagonal_scale,
                    bottom_left_block_.ConstView(),
                    upper_right_block_.ConstView(), bottom_right_objective,
                    factor_primal_solution_prox_reg.ConstView(),
                    factor_dual_solution_prox_reg.ConstView(),
                    false /* initialize */, &factor_kkt_);

  if (factor_schur_complement_) {
    // Reset the Schur complement to 0.
    for (auto& entry : upper_left_schur_complement_.Entries()) {
      entry.value = 0;
    }

    auto add_upper_left_schur = [&](Int row, Int column, Real value) {
      const Real scaled_value = diagonal_scale(num_primal + row) * value *
                                diagonal_scale(num_primal + column);
      upper_left_schur_complement_.AddToEntry(row, column, scaled_value);
    };

    // Add the S_y A inv(Q + R_x + inv(X) Z) A' S_y updates.
    const Buffer<MatrixEntry<Real>>& equality_entries =
        problem.equality_matrix_transpose.Entries();
    for (Int i = 0; i < num_primal; ++i) {
      const Int col_offset =
          problem.equality_matrix_transpose.RowEntryOffset(i);
      const Int num_col_nonzeros =
          problem.equality_matrix_transpose.NumRowEntries(i);
      for (Int k = 0; k < num_col_nonzeros; ++k) {
        const MatrixEntry<Real>& entry_k = equality_entries[col_offset + k];
        const Int row = entry_k.column;
        for (Int l = 0; l < num_col_nonzeros; ++l) {
          const MatrixEntry<Real>& entry_l = equality_entries[col_offset + l];
          const Int column = entry_l.column;
          const Real value = entry_k.value * entry_l.value / factor_theta_(i);
          add_upper_left_schur(row, column, value);
        }
      }
    }

    // Shift the diagonal upwards.
    for (Int i = 0; i < num_dual; ++i) {
      const Real value = factor_dual_solution_prox_reg(i);
      add_upper_left_schur(i, i, value);
    }
  }

  factored_ = false;
}

template <typename Real>
bool FirstOrderOptimality<Real>::FactorMatrix(
    const Problem<Real>& problem, const Iterate<Real>& iterate,
    const ConstBlasMatrixView<Real>& diagonal_scale,
    const SparseLDLControl<Real>& ldl_control,
    const RefinedSolveControl<Real>& refined_solve_control,
    const catamari::FGMRESControl<Real>& fgmres_control,
    Real demanded_relative_residual, bool verbose) {
  if (!factored_) {
    if (factor_schur_complement_) {
      // Factor the Schur-complement of the upper-left block.
      if (refactoring_) {
        upper_left_schur_complement_ldl_result_ =
            upper_left_schur_complement_ldl_.RefactorWithFixedSparsityPattern(
                upper_left_schur_complement_, ldl_control);
      } else {
        upper_left_schur_complement_ldl_result_ =
            upper_left_schur_complement_ldl_.Factor(
                upper_left_schur_complement_, ldl_control);
      }
      if (upper_left_schur_complement_ldl_result_.num_successful_pivots !=
          upper_left_schur_complement_.NumRows()) {
        return false;
      }
    } else {
      // Factor the upper-left block.
      if (refactoring_) {
        upper_left_block_ldl_result_ =
            upper_left_block_ldl_.RefactorWithFixedSparsityPattern(
                factor_upper_left_block_, ldl_control);
      } else {
        upper_left_block_ldl_result_ =
            upper_left_block_ldl_.Factor(factor_upper_left_block_, ldl_control);
      }
      if (upper_left_block_ldl_result_.num_successful_pivots !=
          factor_upper_left_block_.NumRows()) {
        return false;
      }
    }

    const Int num_primal = iterate.PrimalSolution().Height();
    const Int num_dual = iterate.DualSolution().Height();

    BlasMatrix<Real> scaled_rhs_dual;
    auto apply_inverse_via_schur_complement = [&](BlasMatrixView<Real>* rhs) {
      BlasMatrixView<Real> rhs_dual_solution =
          rhs->Submatrix(num_primal, 0, num_dual, rhs->width);
      BlasMatrixView<Real> rhs_augmented =
          rhs->Submatrix(0, 0, num_primal + num_dual, rhs->width);

      ReduceFromAugmentedToDualSolution(problem, iterate, diagonal_scale,
                                        &rhs_augmented);

      upper_left_schur_complement_ldl_.Solve(&rhs_dual_solution);

      ExpandFromDualSolutionToAugmented(problem, iterate, diagonal_scale,
                                        &rhs_augmented);
    };

    auto apply_inverse_via_augmented = [&](BlasMatrixView<Real>* rhs) {
      upper_left_block_ldl_.Solve(rhs);
    };

    auto solve = [&](BlasMatrixView<Real>* rhs) {
      auto apply_sparse = [&](Real alpha,
                              const ConstBlasMatrixView<Real>& input, Real beta,
                              BlasMatrixView<Real>* output) {
        catamari::ApplySparse(alpha, factor_upper_left_block_, input, beta,
                              output);

        if (factor_schur_complement_) {
          for (const auto& perturbation :
               upper_left_schur_complement_ldl_result_.dynamic_regularization) {
            const Int index = perturbation.first + num_primal;
            const Real regularization = perturbation.second;
            for (Int j = 0; j < input.width; ++j) {
              output->Entry(index, j) +=
                  alpha * regularization * input(index, j);
            }
          }
        } else {
          for (const auto& perturbation :
               upper_left_block_ldl_result_.dynamic_regularization) {
            const Int index = perturbation.first;
            const Real regularization = perturbation.second;
            for (Int j = 0; j < input.width; ++j) {
              output->Entry(index, j) +=
                  alpha * regularization * input(index, j);
            }
          }
        }
      };

      BlasMatrix<Real> solution;
      catamari::FGMRESStatus<Real> solve_status;
      if (factor_schur_complement_) {
        auto apply_refined = [&](BlasMatrixView<Real>* input) {
          catamari::RefinedSolve(apply_sparse,
                                 apply_inverse_via_schur_complement,
                                 refined_solve_control, input);
        };
        solve_status =
            catamari::FGMRES(apply_sparse, apply_refined, fgmres_control,
                             rhs->ToConst(), &solution);
      } else {
        auto apply_refined = [&](BlasMatrixView<Real>* input) {
          catamari::RefinedSolve(apply_sparse, apply_inverse_via_augmented,
                                 refined_solve_control, input);
        };
        solve_status =
            catamari::FGMRES(apply_sparse, apply_refined, fgmres_control,
                             rhs->ToConst(), &solution);
      }
      for (Int j = 0; j < rhs->width; ++j) {
        for (Int i = 0; i < rhs->height; ++i) {
          rhs->Entry(i, j) = solution(i, j);
        }
      }

      return solve_status;
    };

    // Solve against the J01 block.
    solved_upper_right_block_ = upper_right_block_;
    catamari::FGMRESStatus<Real> solve_status =
        solve(&solved_upper_right_block_.view);
    if (solve_status.relative_error > demanded_relative_residual) {
      if (verbose) {
        std::cout << "Inaccurate solve of relative norm: "
                  << static_cast<double>(solve_status.relative_error)
                  << std::endl;
      }
      return false;
    }

    // Form the Schur complement onto J11.
    bottom_right_schur_complement_ = bottom_right_entry_;
    for (Int k = 0; k < bottom_left_block_.Width(); ++k) {
      bottom_right_schur_complement_ -=
          bottom_left_block_(0, k) * solved_upper_right_block_(k);
    }
  }

  factored_ = true;
  refactoring_ = true;
  return true;
}

template <typename Real>
void FirstOrderOptimality<Real>::ReduceFromFullToHSDAugmented(
    const Iterate<Real>& iterate,
    const ConstBlasMatrixView<Real>& diagonal_scale,
    const RegularizationState<Real>& reg_state,
    BlasMatrixView<Real>* rhs) const {
  const ConstBlasMatrixView<Real>& iter_primal_solution =
      iterate.PrimalSolution();
  const Int num_primal = iter_primal_solution.Height();
  const Int num_dual = iterate.DualSolution().Height();
  const Int tau_offset = num_primal + num_dual;
  const Int dual_slack_offset = num_primal + num_dual + 1;
  const Int kappa_offset = 2 * num_primal + num_dual + 1;

  BlasMatrixView<Real> rhs_primal_solution =
      rhs->Submatrix(0, 0, num_primal, rhs->width);
  BlasMatrixView<Real> rhs_tau = rhs->Submatrix(tau_offset, 0, 1, rhs->width);
  BlasMatrixView<Real> rhs_dual_slack =
      rhs->Submatrix(dual_slack_offset, 0, num_primal, rhs->width);
  BlasMatrixView<Real> rhs_kappa =
      rhs->Submatrix(kappa_offset, 0, 1, rhs->width);

  // The Schur complement reductions of the congruence scaling of the full
  // regularized KKT system
  //
  //  |     -(Q + R_x)         A'  |       -c       | I      |
  //  |          A            R_y  |       -b       |        |
  //  |----------------------------|----------------|--------|
  //  |  -(c + 2 Q x / tau)'   b'  | x' Q x / tau^2 |    -1  |
  //  |----------------------------|----------------|--------|
  //  |          Z                 |                | X      |
  //  |                            |      kappa     |    tau |
  //
  // involve the row multiplier updates
  //
  //  -inv(S_z X S_z) (S_x S_z) = -inv(X) inv(S_z) S_x,
  //
  //  inv(sigma_kappa^2 tau) (sigma_tau sigma_kappa) =
  //      inv(tau) (sigma_tau / sigma_kappa).

  for (Int j = 0; j < rhs->width; ++j) {
    for (Int i = 0; i < num_primal; ++i) {
      const Real update = -rhs_dual_slack(i, j) / iter_primal_solution(i);
      const Real scaled_update =
          update * diagonal_scale(i) / diagonal_scale(dual_slack_offset + i);
      rhs_primal_solution(i, j) += scaled_update;
    }

    const Real update = rhs_kappa(0, j) / iterate.Tau();
    const Real scaled_update =
        update * diagonal_scale(tau_offset) / diagonal_scale(kappa_offset);
    rhs_tau(0, j) += scaled_update;
  }
}

template <typename Real>
void FirstOrderOptimality<Real>::ExpandFromHSDAugmentedToFull(
    const Iterate<Real>& iterate,
    const ConstBlasMatrixView<Real>& diagonal_scale,
    const RegularizationState<Real>& reg_state,
    BlasMatrixView<Real>* rhs) const {
  const ConstBlasMatrixView<Real>& iter_primal_solution =
      iterate.PrimalSolution();
  const ConstBlasMatrixView<Real>& iter_dual_slack = iterate.DualSlack();
  const Int num_primal = iter_primal_solution.Height();
  const Int num_dual = iterate.DualSolution().Height();
  const Int tau_offset = num_primal + num_dual;
  const Int dual_slack_offset = num_primal + num_dual + 1;
  const Int kappa_offset = 2 * num_primal + num_dual + 1;

  BlasMatrixView<Real> rhs_primal_solution =
      rhs->Submatrix(0, 0, num_primal, rhs->width);
  BlasMatrixView<Real> rhs_tau = rhs->Submatrix(tau_offset, 0, 1, rhs->width);
  BlasMatrixView<Real> rhs_dual_slack =
      rhs->Submatrix(dual_slack_offset, 0, num_primal, rhs->width);
  BlasMatrixView<Real> rhs_kappa =
      rhs->Submatrix(kappa_offset, 0, 1, rhs->width);

  for (Int j = 0; j < rhs->width; ++j) {
    // (S_z Z S_x) dx + (S_z X S_z) dz = z_rhs,
    //
    // dz = inv(S_z X S_z) (z_rhs - S_z Z S_x dx)
    //    = inv(X) (z_rhs / S_z^2 - Z (S_x / S_z) dx).

    for (Int i = 0; i < num_primal; ++i) {
      const Real scaled_rhs =
          rhs_dual_slack(i, j) / (diagonal_scale(dual_slack_offset + i) *
                                  diagonal_scale(dual_slack_offset + i));
      const Real product = iter_dual_slack(i) * rhs_primal_solution(i, j);
      const Real scaled_product =
          product * diagonal_scale(i) / diagonal_scale(dual_slack_offset + i);
      rhs_dual_slack(i, j) =
          (scaled_rhs - scaled_product) / iter_primal_solution(i);
    }

    // (sigma_kappa kappa sigma_tau) dtau + (sigma_kappa^2 tau) dkappa =
    //     kappa_rhs.
    //
    // dkappa
    //   = inv(sigma_kappa^2 tau) (kappa_rhs - sigma_kappa kappa sigma_tau
    //   dtau). = inv(tau sigma_kappa) (kappa_rhs / sigma_kappa - kappa dtau
    //   sigma_tau).
    const Real sigma_tau = diagonal_scale(tau_offset);
    const Real sigma_kappa = diagonal_scale(kappa_offset);
    const Real scaled_rhs = rhs_kappa(0, j) / sigma_kappa;
    const Real product = iterate.Kappa() * rhs_tau(0, j);
    const Real scaled_product = product * sigma_tau;
    rhs_kappa(0, j) =
        (scaled_rhs - scaled_product) / (iterate.Tau() * sigma_kappa);
  }
}

template <typename Real>
void FirstOrderOptimality<Real>::ApplyHSDAugmented(
    const Problem<Real>& problem, Real alpha,
    const ConstBlasMatrixView<Real>& hsd_augmented_input, Real beta,
    BlasMatrixView<Real>* hsd_augmented_output) const {
  const Int num_primal = problem.equality_matrix.NumColumns();
  const Int num_dual = problem.equality_matrix.NumRows();
  const ConstBlasMatrixView<Real>& augmented_input =
      hsd_augmented_input.Submatrix(0, 0, num_primal + num_dual,
                                    hsd_augmented_input.width);
  const ConstBlasMatrixView<Real>& input_tau = hsd_augmented_input.Submatrix(
      num_primal + num_dual, 0, 1, hsd_augmented_input.width);
  BlasMatrixView<Real> augmented_output = hsd_augmented_output->Submatrix(
      0, 0, num_primal + num_dual, hsd_augmented_input.width);
  BlasMatrixView<Real> output_tau = hsd_augmented_output->Submatrix(
      num_primal + num_dual, 0, 1, hsd_augmented_input.width);

  catamari::ApplySparse(alpha, factor_upper_left_block_, augmented_input, beta,
                        &augmented_output);

  for (Int j = 0; j < hsd_augmented_input.width; ++j) {
    for (Int i = 0; i < num_primal + num_dual; ++i) {
      augmented_output(i, j) +=
          alpha * upper_right_block_(i, 0) * input_tau(0, j);
    }

    output_tau(0, j) *= beta;
    for (Int i = 0; i < num_primal + num_dual; ++i) {
      output_tau(0, j) +=
          alpha * bottom_left_block_(0, i) * augmented_input(i, j);
    }
    output_tau(0, j) += alpha * bottom_right_entry_ * input_tau(0, j);
  }
}

template <typename Real>
void FirstOrderOptimality<Real>::ReduceFromAugmentedToDualSolution(
    const Problem<Real>& problem, const Iterate<Real>& iterate,
    const ConstBlasMatrixView<Real>& diagonal_scale,
    BlasMatrixView<Real>* rhs) const {
  const Int num_primal = problem.equality_matrix.NumColumns();
  const Int num_dual = problem.equality_matrix.NumRows();
  BlasMatrixView<Real> rhs_primal_solution =
      rhs->Submatrix(0, 0, num_primal, rhs->width);
  BlasMatrixView<Real> rhs_dual_solution =
      rhs->Submatrix(num_primal, 0, num_dual, rhs->width);

  BlasMatrix<Real> scaled_rhs_primal;
  scaled_rhs_primal.Resize(num_primal, rhs->width);
  for (Int j = 0; j < rhs->width; ++j) {
    for (Int i = 0; i < num_primal; ++i) {
      scaled_rhs_primal(i, j) =
          rhs_primal_solution(i, j) / (factor_theta_(i) * diagonal_scale(i));
    }
  }
  for (Int j = 0; j < rhs->width; ++j) {
    for (Int i = 0; i < num_dual; ++i) {
      rhs_dual_solution(i, j) /= diagonal_scale(num_primal + i);
    }
  }
  catamari::ApplySparse(Real(1), problem.equality_matrix,
                        scaled_rhs_primal.ConstView(), Real(1),
                        &rhs_dual_solution);
  for (Int j = 0; j < rhs->width; ++j) {
    for (Int i = 0; i < num_dual; ++i) {
      rhs_dual_solution(i, j) *= diagonal_scale(num_primal + i);
    }
  }
}

template <typename Real>
void FirstOrderOptimality<Real>::ExpandFromDualSolutionToAugmented(
    const Problem<Real>& problem, const Iterate<Real>& iterate,
    const ConstBlasMatrixView<Real>& diagonal_scale,
    BlasMatrixView<Real>* rhs) const {
  const Int num_primal = problem.equality_matrix.NumColumns();
  const Int num_dual = problem.equality_matrix.NumRows();
  BlasMatrixView<Real> rhs_primal_solution =
      rhs->Submatrix(0, 0, num_primal, rhs->width);
  BlasMatrixView<Real> rhs_dual_solution =
      rhs->Submatrix(num_primal, 0, num_dual, rhs->width);

  BlasMatrix<Real> scaled_rhs_dual(num_dual, rhs->width);
  for (Int j = 0; j < rhs->width; ++j) {
    for (Int i = 0; i < num_dual; ++i) {
      scaled_rhs_dual(i, j) =
          rhs_dual_solution(i, j) * diagonal_scale(num_primal + i);
    }
  }
  for (Int j = 0; j < rhs->width; ++j) {
    for (Int i = 0; i < num_primal; ++i) {
      rhs_primal_solution(i, j) /= diagonal_scale(i);
    }
  }
  catamari::ApplyAdjointSparse(Real(1), problem.equality_matrix,
                               scaled_rhs_dual.ConstView(), Real(-1),
                               &rhs_primal_solution);
  for (Int j = 0; j < rhs->width; ++j) {
    for (Int i = 0; i < num_primal; ++i) {
      rhs_primal_solution(i, j) /= factor_theta_(i) * diagonal_scale(i);
    }
  }
}

template <typename Real>
void FirstOrderOptimality<Real>::ExpandFromAugmentedToHSDAugmented(
    const Problem<Real>& problem, BlasMatrixView<Real>* rhs) const {
  const Int num_primal = problem.equality_matrix.NumColumns();
  const Int num_dual = problem.equality_matrix.NumRows();
  BlasMatrixView<Real> rhs_augmented =
      rhs->Submatrix(0, 0, num_primal + num_dual, rhs->width);
  BlasMatrixView<Real> rhs_tau =
      rhs->Submatrix(num_primal + num_dual, 0, 1, rhs->width);

  // q1 := q1 - J10 q0.
  for (Int k = 0; k < num_primal + num_dual; ++k) {
    for (Int j = 0; j < rhs->width; ++j) {
      rhs_tau(0, j) -= bottom_left_block_(0, k) * rhs_augmented(k, j);
    }
  }

  // q1 := inv(J11 - J10 inv(J00) J01) q1.
  for (Int j = 0; j < rhs->width; ++j) {
    rhs_tau(0, j) /= bottom_right_schur_complement_;
  }

  // q0 := q0 - (inv(J00) J01) q1.
  for (Int i = 0; i < num_primal + num_dual; ++i) {
    for (Int j = 0; j < rhs->width; ++j) {
      rhs_augmented(i, j) -= solved_upper_right_block_(i, 0) * rhs_tau(0, j);
    }
  }
}

template <typename Real>
void FirstOrderOptimality<Real>::SolveHSDAugmentedSystem(
    const Problem<Real>& problem, const Iterate<Real>& iterate,
    const ConstBlasMatrixView<Real>& diagonal_scale, bool print_residuals,
    BlasMatrixView<Real>* hsd_augmented_rhs) const {
  const Int num_primal = problem.equality_matrix.NumColumns();
  const Int num_dual = problem.equality_matrix.NumRows();

  BlasMatrix<Real> hsd_augmented_rhs_copy;
  if (print_residuals) {
    hsd_augmented_rhs_copy.Resize(hsd_augmented_rhs->height,
                                  hsd_augmented_rhs->width);
    for (Int j = 0; j < hsd_augmented_rhs->width; ++j) {
      for (Int i = 0; i < hsd_augmented_rhs->height; ++i) {
        hsd_augmented_rhs_copy(i, j) = hsd_augmented_rhs->Entry(i, j);
      }
    }
    const Real hsd_augmented_rhs_two_norm =
        EuclideanNorm(hsd_augmented_rhs_copy.ConstView());
    std::cout << "hsd_augmented_rhs_two_norm="
              << static_cast<double>(hsd_augmented_rhs_two_norm) << std::endl;
  }

  BlasMatrixView<Real> rhs_dual_solution = hsd_augmented_rhs->Submatrix(
      num_primal, 0, num_dual, hsd_augmented_rhs->width);
  BlasMatrixView<Real> rhs_augmented = hsd_augmented_rhs->Submatrix(
      0, 0, num_primal + num_dual, hsd_augmented_rhs->width);

  BlasMatrix<Real> augmented_solution_rhs;
  if (print_residuals) {
    augmented_solution_rhs.Resize(num_primal + num_dual,
                                  hsd_augmented_rhs->width);
    BlasMatrixView<Real> primal_solution_rhs = augmented_solution_rhs.Submatrix(
        0, 0, num_primal, hsd_augmented_rhs->width);
    BlasMatrixView<Real> dual_solution_rhs = augmented_solution_rhs.Submatrix(
        num_primal, 0, num_dual, hsd_augmented_rhs->width);
    for (Int j = 0; j < hsd_augmented_rhs->width; ++j) {
      for (Int i = 0; i < num_primal + num_dual; ++i) {
        augmented_solution_rhs(i, j) = rhs_augmented(i, j);
      }
    }
    const Real primal_solution_rhs_two_norm =
        EuclideanNorm(primal_solution_rhs.ToConst());
    const Real dual_solution_rhs_two_norm =
        EuclideanNorm(dual_solution_rhs.ToConst());
    const Real augmented_solution_rhs_two_norm =
        EuclideanNorm(augmented_solution_rhs.ConstView());
    std::cout << "primal_solution_rhs_two_norm="
              << static_cast<double>(primal_solution_rhs_two_norm) << std::endl;
    std::cout << "dual_solution_rhs_two_norm="
              << static_cast<double>(dual_solution_rhs_two_norm) << std::endl;
    std::cout << "augmented_solution_rhs_two_norm="
              << static_cast<double>(augmented_solution_rhs_two_norm)
              << std::endl;
  }
  if (factor_schur_complement_) {
    ReduceFromAugmentedToDualSolution(problem, iterate, diagonal_scale,
                                      &rhs_augmented);

    BlasMatrix<Real> schur_dual_solution_rhs;
    if (print_residuals) {
      schur_dual_solution_rhs.Resize(num_dual, hsd_augmented_rhs->width);
      for (Int j = 0; j < hsd_augmented_rhs->width; ++j) {
        for (Int i = 0; i < num_dual; ++i) {
          schur_dual_solution_rhs(i, j) = rhs_dual_solution(i, j);
        }
      }
      const Real schur_dual_solution_rhs_two_norm =
          EuclideanNorm(schur_dual_solution_rhs.ConstView());
      std::cout << "schur_dual_solution_rhs_two_norm="
                << static_cast<double>(schur_dual_solution_rhs_two_norm)
                << std::endl;
    }
    upper_left_schur_complement_ldl_.Solve(&rhs_dual_solution);
    if (print_residuals) {
      catamari::ApplySparse(Real(-1), upper_left_schur_complement_,
                            rhs_dual_solution.ToConst(), Real(1),
                            &schur_dual_solution_rhs.view);
      const Real schur_dual_solution_residual_two_norm =
          EuclideanNorm(schur_dual_solution_rhs.ConstView());
      std::cout << "schur_dual_solution_residual_two_norm="
                << static_cast<double>(schur_dual_solution_residual_two_norm)
                << std::endl;
    }

    ExpandFromDualSolutionToAugmented(problem, iterate, diagonal_scale,
                                      &rhs_augmented);
  } else {
    upper_left_block_ldl_.Solve(&rhs_augmented);
  }
  if (print_residuals) {
    catamari::ApplySparse(Real(-1), factor_upper_left_block_,
                          rhs_augmented.ToConst(), Real(1),
                          &augmented_solution_rhs.view);

    BlasMatrixView<Real> primal_solution_rhs = augmented_solution_rhs.Submatrix(
        0, 0, num_primal, hsd_augmented_rhs->width);
    BlasMatrixView<Real> dual_solution_rhs = augmented_solution_rhs.Submatrix(
        num_primal, 0, num_dual, hsd_augmented_rhs->width);

    const Real primal_solution_residual_two_norm =
        EuclideanNorm(primal_solution_rhs.ToConst());
    const Real dual_solution_residual_two_norm =
        EuclideanNorm(dual_solution_rhs.ToConst());
    const Real augmented_solution_residual_two_norm =
        EuclideanNorm(augmented_solution_rhs.ConstView());
    std::cout << "primal_solution_residual_two_norm="
              << static_cast<double>(primal_solution_residual_two_norm)
              << std::endl;
    std::cout << "dual_solution_residual_two_norm="
              << static_cast<double>(dual_solution_residual_two_norm)
              << std::endl;
    std::cout << "augmented_solution_residual_two_norm="
              << static_cast<double>(augmented_solution_residual_two_norm)
              << std::endl;
  }

  ExpandFromAugmentedToHSDAugmented(problem, hsd_augmented_rhs);

  if (print_residuals) {
    ApplyHSDAugmented(problem, Real(-1), hsd_augmented_rhs->ToConst(), Real(1),
                      &hsd_augmented_rhs_copy.view);
    const Real hsd_augmented_residual_two_norm =
        EuclideanNorm(hsd_augmented_rhs_copy.ConstView());
    std::cout << "hsd_augmented_residual_two_norm="
              << static_cast<double>(hsd_augmented_residual_two_norm)
              << std::endl;
  }
}

template <typename Real>
void FirstOrderOptimality<Real>::AnalyzeResidualComponents(
    bool factor_regularization, const Problem<Real>& problem,
    const ConstBlasMatrixView<Real>& solution,
    const ConstBlasMatrixView<Real>& rhs) const {
  const Int num_primal = problem.equality_matrix.NumColumns();
  const Int num_dual = problem.equality_matrix.NumRows();
  const Real rhs_two_norm = EuclideanNorm(rhs);

  BlasMatrix<Real> rhs_copy(rhs.height, rhs.width);
  for (Int j = 0; j < rhs.width; ++j) {
    for (Int i = 0; i < rhs.height; ++i) {
      rhs_copy(i, j) = rhs(i, j);
    }
  }

  BlasMatrixView<Real> rhs_primal_solution =
      rhs_copy.Submatrix(0, 0, num_primal, rhs.width);
  BlasMatrixView<Real> rhs_dual_solution =
      rhs_copy.Submatrix(num_primal, 0, num_dual, rhs.width);
  BlasMatrixView<Real> rhs_tau =
      rhs_copy.Submatrix(num_primal + num_dual, 0, 1, rhs.width);
  BlasMatrixView<Real> rhs_dual_slack =
      rhs_copy.Submatrix(num_primal + num_dual + 1, 0, num_primal, rhs.width);
  BlasMatrixView<Real> rhs_kappa =
      rhs_copy.Submatrix(2 * num_primal + num_dual + 1, 0, 1, rhs.width);
  const Real rhs_primal_solution_two_norm =
      EuclideanNorm(rhs_primal_solution.ToConst());
  const Real rhs_dual_solution_two_norm =
      EuclideanNorm(rhs_dual_solution.ToConst());
  const Real rhs_tau_two_norm = EuclideanNorm(rhs_tau.ToConst());
  const Real rhs_dual_slack_two_norm = EuclideanNorm(rhs_dual_slack.ToConst());
  const Real rhs_kappa_two_norm = EuclideanNorm(rhs_kappa.ToConst());
  if (factor_regularization) {
    catamari::ApplySparse(Real(-1), factor_kkt_, solution, Real(1),
                          &rhs_copy.view);
  } else {
    catamari::ApplySparse(Real(-1), target_kkt_, solution, Real(1),
                          &rhs_copy.view);
  }
  const Real residual_two_norm = EuclideanNorm(rhs_copy.ConstView());
  const Real residual_primal_solution_two_norm =
      EuclideanNorm(rhs_primal_solution.ToConst());
  const Real residual_dual_solution_two_norm =
      EuclideanNorm(rhs_dual_solution.ToConst());
  const Real residual_tau_two_norm = EuclideanNorm(rhs_tau.ToConst());
  const Real residual_dual_slack_two_norm =
      EuclideanNorm(rhs_dual_slack.ToConst());
  const Real residual_kappa_two_norm = EuclideanNorm(rhs_kappa.ToConst());
  std::cout << "|| rhs              || = " << static_cast<double>(rhs_two_norm)
            << ", || resid                  || = "
            << static_cast<double>(residual_two_norm) << "\n"
            << "|| dual eq          || = "
            << static_cast<double>(rhs_primal_solution_two_norm)
            << ", || dual eq resid          || = "
            << static_cast<double>(residual_primal_solution_two_norm) << "\n"
            << "|| primal eq        || = "
            << static_cast<double>(rhs_dual_solution_two_norm)
            << ", || primal eq resid        || = "
            << static_cast<double>(residual_dual_solution_two_norm) << "\n"
            << "|| hsd gap          || = "
            << static_cast<double>(rhs_tau_two_norm)
            << ", || hsd gap resid          || = "
            << static_cast<double>(residual_tau_two_norm) << "\n"
            << "|| complement       || = "
            << static_cast<double>(rhs_dual_slack_two_norm)
            << ", || complement resid       || = "
            << static_cast<double>(residual_dual_slack_two_norm) << "\n"
            << "|| scale complement || = "
            << static_cast<double>(rhs_kappa_two_norm)
            << ", || scale complement resid || = "
            << static_cast<double>(residual_kappa_two_norm) << std::endl;
}

template <typename Real>
bool FirstOrderOptimality<Real>::Solve(
    bool factor_regularization, const Problem<Real>& problem,
    const Iterate<Real>& iterate,
    const ConstBlasMatrixView<Real>& diagonal_scale,
    const Residuals<Real>& residuals,
    const RegularizationState<Real>& reg_state,
    const RefinedSolveControl<Real>& refined_solve_control,
    const catamari::FGMRESControl<Real>& fgmres_control,
    Real demanded_relative_residual, bool verbose, bool analyze_residuals,
    bool analyze_iterate_residuals, Iterate<Real>* update) {
  CATAMARI_ASSERT(factored_, "Must factor before solving.");
  const Int num_primal = iterate.PrimalSolution().Height();
  const Int num_dual = iterate.DualSolution().Height();
  BlasMatrixView<Real> update_full = update->stacked_vectors.AllComponents();

  // Push the residuals into the initial value of the right-hand sides.
  // The only negated residual is that of the first block row, whose sign was
  // flipped to ensure a symmetric augmented system for the primal and dual
  // solution updates.
  {
    BlasMatrixView<Real>& update_primal_solution = update->PrimalSolution();
    for (Int i = 0; i < num_primal; ++i) {
      update_primal_solution(i) = -residuals.dual_equality.residual(i);
    }

    BlasMatrixView<Real>& update_dual_solution = update->DualSolution();
    for (Int i = 0; i < num_dual; ++i) {
      update_dual_solution(i) = residuals.primal_equality.residual(i);
    }

    update->Tau() = residuals.hsd_gap;

    BlasMatrixView<Real>& update_dual_slack = update->DualSlack();
    for (Int i = 0; i < num_primal; ++i) {
      update_dual_slack(i) = residuals.complementarity.residual(i);
    }

    update->Kappa() = residuals.scale_complement_residual;
  }

  // Perform the initial rescaling for the scaled system:
  //   (D A D) (inv(D) x) = (D b).
  for (Int i = 0; i < update_full.height; ++i) {
    update_full(i) *= diagonal_scale(i);
  }

  // Make a copy of the scaled right-hand side if we need to analyze residuals.
  BlasMatrix<Real> scaled_input;
  if (analyze_residuals) {
    scaled_input = update_full;
  }

  BlasMatrix<Real> scaled_rhs_primal;
  auto apply_inverse = [&](BlasMatrixView<Real>* rhs) {
    BlasMatrix<Real> rhs_copy;
    if (analyze_iterate_residuals) {
      rhs_copy.Resize(rhs->height, rhs->width);
      for (Int j = 0; j < rhs->width; ++j) {
        for (Int i = 0; i < rhs->height; ++i) {
          rhs_copy(i, j) = rhs->Entry(i, j);
        }
      }
    }

    ReduceFromFullToHSDAugmented(iterate, diagonal_scale, reg_state, rhs);

    BlasMatrixView<Real> rhs_augmented_hsd =
        rhs->Submatrix(0, 0, num_primal + num_dual + 1, rhs->width);
    SolveHSDAugmentedSystem(problem, iterate, diagonal_scale,
                            analyze_iterate_residuals, &rhs_augmented_hsd);

    ExpandFromHSDAugmentedToFull(iterate, diagonal_scale, reg_state, rhs);

    if (analyze_iterate_residuals) {
      std::cout << "Iterate residuals with target regularization" << std::endl;
      AnalyzeResidualComponents(false /* factor_regularization */, problem,
                                rhs->ToConst(), rhs_copy.ConstView());
    }
  };

  auto apply_sparse_factor = [&](Real alpha,
                                 const ConstBlasMatrixView<Real>& input,
                                 Real beta, BlasMatrixView<Real>* output) {
    catamari::ApplySparse(alpha, factor_kkt_, input, beta, output);
    if (factor_schur_complement_) {
      for (const auto& perturbation :
           upper_left_schur_complement_ldl_result_.dynamic_regularization) {
        const Int index = perturbation.first + num_primal;
        const Real regularization = perturbation.second;
        for (Int j = 0; j < input.width; ++j) {
          output->Entry(index, j) += alpha * regularization * input(index, j);
        }
      }
    } else {
      for (const auto& perturbation :
           upper_left_block_ldl_result_.dynamic_regularization) {
        const Int index = perturbation.first;
        const Real regularization = perturbation.second;
        for (Int j = 0; j < input.width; ++j) {
          output->Entry(index, j) += alpha * regularization * input(index, j);
        }
      }
    }
  };

  auto apply_sparse = [&](Real alpha, const ConstBlasMatrixView<Real>& input,
                          Real beta, BlasMatrixView<Real>* output) {
    if (factor_regularization) {
      catamari::ApplySparse(alpha, factor_kkt_, input, beta, output);
      if (factor_schur_complement_) {
        for (const auto& perturbation :
             upper_left_schur_complement_ldl_result_.dynamic_regularization) {
          const Int index = perturbation.first + num_primal;
          const Real regularization = perturbation.second;
          for (Int j = 0; j < input.width; ++j) {
            output->Entry(index, j) += alpha * regularization * input(index, j);
          }
        }
      } else {
        for (const auto& perturbation :
             upper_left_block_ldl_result_.dynamic_regularization) {
          const Int index = perturbation.first;
          const Real regularization = perturbation.second;
          for (Int j = 0; j < input.width; ++j) {
            output->Entry(index, j) += alpha * regularization * input(index, j);
          }
        }
      }
    } else {
      catamari::ApplySparse(alpha, target_kkt_, input, beta, output);
    }
  };

  auto apply_refined = [&](BlasMatrixView<Real>* input) {
    catamari::RefinedSolve(apply_sparse_factor, apply_inverse,
                           refined_solve_control, input);
  };

  BlasMatrix<Real> solution;
  catamari::FGMRESStatus<Real> solve_status =
      catamari::FGMRES(apply_sparse, apply_refined, fgmres_control,
                       update_full.ToConst(), &solution);
  if (solve_status.relative_error > demanded_relative_residual) {
    if (verbose) {
      std::cout << "Inaccurate solve of relative norm: "
                << static_cast<double>(solve_status.relative_error)
                << std::endl;
    }
    return false;
  }

  // Copy the solution into the update_full matrix.
  for (Int j = 0; j < solution.Width(); ++j) {
    for (Int i = 0; i < solution.Height(); ++i) {
      update_full(i, j) = solution(i, j);
    }
  }

  if (analyze_residuals) {
    std::cout << "Scaled input residuals:" << std::endl;
    AnalyzeResidualComponents(false /* factor_regularization */, problem,
                              update_full.ToConst(), scaled_input.ConstView());
  }

  // Perform the final rescaling for the scaled system:
  //   (D A D) (inv(D) x) = (D b).
  for (Int i = 0; i < update_full.height; ++i) {
    update_full(i) *= diagonal_scale(i);
  }

  return true;
}

}  // namespace hsd_qp
}  // namespace conic

#endif  // ifndef CONIC_HSD_QP_FIRST_ORDER_OPTIMALITY_IMPL_H_
