/*
 * Copyright (c) 2019 Jack Poulson <jack@hodgestar.com>
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
#ifndef CONIC_HSD_QP_UTIL_H_
#define CONIC_HSD_QP_UTIL_H_

#include "conic/hsd_qp/iterate.hpp"
#include "conic/hsd_qp/stacked_primal_dual_vectors.hpp"
#include "conic/imports.hpp"
#include "conic/util.hpp"

namespace conic {
namespace hsd_qp {

// A configuration structure for an optional manual manipulation of the
// complements x_i * z_i, where x is the primal solution vector and z is the
// dual slack vector.
template <typename Real>
struct ComplementConditioningControl {
  // If true, the following ratios of the average complement and average duality
  // gap are used to define a manual perturbation of the primal or dual slack
  // entries that ensures a sufficient lower bound on the complements.
  bool enabled = false;

  // The minimum allowable ratio of a complement to the average complement.
  Real min_average_complement_ratio = Real(1e-3);

  // The minimum allowable ratio of a complement to the average duality gap
  // (unless overridden with max_average_complement_ratio).
  Real min_average_duality_gap_ratio = Real(1e-4);

  // The maximum allowable ratio of a complement to the average complement.
  Real max_average_complement_ratio = Real(0.02);
};

template <typename Real>
Real AverageComplement(const Iterate<Real>& iterate);

template <typename Real>
Real UpdatedAverageComplement(const Iterate<Real>& iterate, Real primal_step,
                              Real dual_step, const Iterate<Real>& update);

template <typename Real>
Real ComplementRatio(const Iterate<Real>& iterate, bool verbose);

// Ensure that the slack products are not too small relative to the accuracy
// of the current iterate.
template <typename Real>
void ConditionComplements(const Residuals<Real>& state,
                          const ComplementConditioningControl<Real>& control,
                          bool verbose, Iterate<Real>* iterate);

// We compute the minimum allowed step length for the x, tau, and kappa updates
// as recommended by the 'Primal and dual step sizes' section of Xu, Hung, and
// Ye's "A Simplified Homogeneous and Self-Dual Linear Programming Algorithm
// and its Implementation".
template <typename Real>
Real PositiveOrthantMaximumPrimalStepSize(const Iterate<Real>& iterate,
                                          const Iterate<Real>& update,
                                          Real max_step_size);

// We compute the minimum allowed step length for the z, tau, and kappa updates
// as recommended by the 'Primal and dual step sizes' section of Xu, Hung, and
// Ye's "A Simplified Homogeneous and Self-Dual Linear Programming Algorithm
// and its Implementation".
template <typename Real>
Real PositiveOrthantMaximumDualStepSize(const Iterate<Real>& iterate,
                                        const Iterate<Real>& update,
                                        Real max_step_size);

// We use the approach recommended by Xu, Hung, and Ye to determine: (1) if
// we should allow different primal and dual step sizes, and (2) how to ensure
// that we handle the tau and kappa updates appropriately.
template <typename Real>
std::pair<Real, Real> PositiveOrthantPrimalDualStepSizes(
    const Iterate<Real>& iterate, const Iterate<Real>& update,
    const StepSizeConfig<Real>& config);

// Mehrotra's barrier parameter choice from:
//
//   S. Mehrotra, "On the implementation of a primal-dual interior point
//   method", SIAM J. Optimization, Vol. 2, No. 4, pp. 575--601, 1992.
//
template <typename Real>
void MehrotraBarrier(const Iterate<Real>& iterate, Real affine_barrier,
                     Real primal_step, Real dual_step,
                     const Iterate<Real>& update, bool verbose, Real* centering,
                     Real* mehrotra_barrier);

// Updates iterate += step * update.
template <typename Real>
void UpdateIterate(Real step, const Iterate<Real>& update,
                   Iterate<Real>* iterate);

// Updates the iterate using separate primal and dual step sizes.
template <typename Real>
void UpdateIterate(Real primal_step, Real dual_step,
                   const Iterate<Real>& update, Iterate<Real>* iterate);

}  // namespace hsd_qp
}  // namespace conic

#include "conic/hsd_qp/util-impl.hpp"

#endif  // ifndef CONIC_HSD_QP_UTIL_H_
