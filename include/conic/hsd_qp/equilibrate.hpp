/*
 * Copyright (c) 2019 Jack Poulson <jack@hodgestar.com>
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
#ifndef CONIC_HSD_QP_EQUILIBRATE_H_
#define CONIC_HSD_QP_EQUILIBRATE_H_

#include "conic/hsd_qp/iterate.hpp"
#include "conic/hsd_qp/problem.hpp"
#include "conic/imports.hpp"

namespace conic {
namespace hsd_qp {

// Given an original Linear Programming problem:
//
//   arginf_x { c' x : A x = b, x >= 0 },
//
// we represent a rescaling of the problem via choosing a diagonal row scaling
// matrix, D, a diagonal column scaling E, and an objective scaling gamma.
//
// We can derive the scaling beginning with the objective function
//
//   c' x = (inv(E) c)' (E x),
//
// then introducing an objective scaling factor, gamma, which does not change
// the solution:
//
//   (c' x) / gamma = (inv(gamma) inv(E) c)' (E x) = c_s' x_s.
//
// We introduce a row scaling on the equality constraints to form:
//
//   inv(D) (A x - b) = (inv(D) A inv(E)) (E x) - (inv(D) b) = A_s x_s - b_s.
//
// The rescaled problem is thus:
//
//   arginf_x { c_s' x_s : A_s x_s = b_s, x_s >= 0 },
//
// where:
//
//   c_s := inv(gamma) inv(E) c,
//   A_s := inv(D) A inv(E),
//   x_s := E x, and
//   b_s := inv(D) b.
//
// The impact on the dual solution can be understood by starting with:
//
//   argsup_{y, z} { b' y : A' y + z = c, z >= 0 }.
//
// In terms of the rescaled variables,
//
//   argsup_{y_s, z_s} { b_s' y_s : A_s' y_s + z_s = c_s, z_s >= 0 }.
//
// Expanding the equality constraint yields:
//
//   inv(E) A' inv(D) y_s + z_s = inv(gamma) inv(E) c.
//
// Then
//
//   inv(gamma) inv(E) [A' [gamma inv(D) y_s] + [gamma E z_s] - c] = 0.
//
// We can thus identify:
//
//   y = gamma inv(D) y_s, and
//   z = gamma E z_s.
//
// One can extend to quadratic programming by scaling the quadratic objective
// conformally with the linear objective.
//
template <typename Real>
struct Scaling {
  // A rescaling of the primal right-hand side vector, b,
  //
  //   b |-> b / primal_scale.
  //
  // Extracting a scale factor of 'primal_scale' from 'b' similarly
  // extracts a scaling factor from the primal variable 'x':
  //
  //   x |-> x / primal_scale.
  //
  Real primal_scale;

  // A rescaling of the objective vector, c,
  //
  //   c |-> c / dual_scale.
  //
  // Extracting a scale factor of 'dual_scale' from 'c' similarly extracts a
  // scaling factor from the dual variables, 'y', and 'z':
  //
  //   | y | |-> | y | / dual_scale.
  //   | z |     | z |
  //
  Real dual_scale;

  // A rescaling of the entire solution via
  //
  //   | x | |-> | x | / tau_scale.
  //   | y |     | y |
  //   | z |     | z |
  //
  Real tau_scale;

  // The diagonal of the row scaling matrix D for the equality constraints.
  BlasMatrix<Real> equality_row_scaling;

  // The diagonal of the column scaling matrix E for the equality constraints.
  BlasMatrix<Real> equality_column_scaling;
};

// Rescales a linear program by iteratively reducing the dynamic range of the
// nonzero coefficients in each row and column of the homogeneous self-dual
// augmented system matrix
//
//   |  Q  -A'  c |
//   |  A      -b |.
//   | -c'  b'    |
//
template <typename Real>
void GeometricEquilibration(Problem<Real>* problem, Scaling<Real>* scaling,
                            Int num_passes = 4, bool verbose = false);

// Rescales a linear programming problem in-place and returns the corresponding
// scaling information. Our approach is essentially the simple algorithm
// described in [Ruiz-2001] applied to the homogeneous self-dual augmented
// system matrix
//
//   |  Q  -A'  c |
//   |  A      -b |.
//   | -c'  b'    |
//
template <typename Real>
void RuizEquilibration(Problem<Real>* problem, Scaling<Real>* scaling,
                       Int num_passes = 4, bool verbose = false);

// Backtransforms the solution to an equilibrated Linear Program to the original
// scaling.
template <typename Real>
void RescaleIterate(const Scaling<Real>& scaling, Iterate<Real>* iterate);

// References:
//
// [Ruiz-2001]
//   Daniel Ruiz, "A Scaling Algorithm to Equilibrate Both Rows and Columns
//   Norms in Matrices", Technical Report RAL-TR-2001-034, Rutherford Appleton
//   Laboratory, 2001.
//

}  // namespace hsd_qp
}  // namespace conic

#include "conic/hsd_qp/equilibrate-impl.hpp"

#endif  // ifndef CONIC_HSD_QP_EQUILIBRATE_H_
