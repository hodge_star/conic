/*
 * Copyright (c) 2019 Jack Poulson <jack@hodgestar.com>
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
#ifndef CONIC_HSD_QP_STACKED_PRIMAL_DUAL_VECTORS_H_
#define CONIC_HSD_QP_STACKED_PRIMAL_DUAL_VECTORS_H_

#include "conic/imports.hpp"

namespace conic {
namespace hsd_qp {

template <typename Real>
struct StackedPrimalDualVectors {
  // A default constructor which assumes all zero vector lengths.
  StackedPrimalDualVectors();

  // Initializes primal-dual estimates of the given sizes.
  StackedPrimalDualVectors(Int num_primal, Int num_dual,
                           Int num_directions = 1);

  // A copy constructor.
  StackedPrimalDualVectors(const StackedPrimalDualVectors<Real>& vectors);

  // A copy operation.
  StackedPrimalDualVectors<Real>& operator=(
      const StackedPrimalDualVectors<Real>& vectors);

  void Resize(Int num_primal, Int num_dual, Int num_directions = 1);

  // Returns the number of primal variables.
  Int NumPrimal() const;

  // Returns the number of dual variables.
  Int NumDual() const;

  // Returns the number of stacked vectors.
  Int NumDirections() const;

  // Returns a view of the full concatenated set of components.
  BlasMatrixView<Real>& AllComponents();
  const ConstBlasMatrixView<Real>& AllComponents() const;

  // Returns a view of the primal solution component of the direction.
  BlasMatrixView<Real>& PrimalSolutions();
  const ConstBlasMatrixView<Real>& PrimalSolutions() const;

  // Returns a view of the dual solution component of the direction.
  BlasMatrixView<Real>& DualSolutions();
  const ConstBlasMatrixView<Real>& DualSolutions() const;

  // Returns a view of the tau components.
  BlasMatrixView<Real>& Tau();
  const ConstBlasMatrixView<Real>& Tau() const;

  // Returns a view of the dual slack component of the direction.
  BlasMatrixView<Real>& DualSlacks();
  const ConstBlasMatrixView<Real>& DualSlacks() const;

  // Returns a view of the kappa components.
  BlasMatrixView<Real>& Kappa();
  const ConstBlasMatrixView<Real>& Kappa() const;

  // Returns a view of the concatenation of the primal and dual solutions.
  BlasMatrixView<Real>& AugmentedSystemComponents();
  const ConstBlasMatrixView<Real>& AugmentedSystemComponents() const;

  // Returns a view of the concatenation of the primal and dual solutions,
  // plus the 'tau' variable.
  BlasMatrixView<Real>& HSDAugmentedSystemComponents();
  const ConstBlasMatrixView<Real>& HSDAugmentedSystemComponents() const;

 private:
  // The underlying storage mechanism. The variables are stored in the order
  //   1. primal_solution,
  //   2. dual_solution,
  //   3. tau,
  //   4. dual_slack,
  //   5. kappa,
  // so that 1--2 contiguously comprise the 'augmented system' components and
  // 1--3 comprise the 'HSD augmented system' components.
  BlasMatrix<Real> directions_;

  // A view into the concatenation of all components.
  BlasMatrixView<Real> all_components_;
  ConstBlasMatrixView<Real> const_all_components_;

  // A view into 'directions_' of the primal solution vectors.
  BlasMatrixView<Real> primal_solutions_;
  ConstBlasMatrixView<Real> const_primal_solutions_;

  // A view into 'directions_' of the dual solution vectors.
  BlasMatrixView<Real> dual_solutions_;
  ConstBlasMatrixView<Real> const_dual_solutions_;

  // A view into 'directions_' of the 'tau' components.
  BlasMatrixView<Real> tau_;
  ConstBlasMatrixView<Real> const_tau_;

  // A view into 'directions_' of the dual slack vectors.
  BlasMatrixView<Real> dual_slacks_;
  ConstBlasMatrixView<Real> const_dual_slacks_;

  // A view into 'directions_' of the 'kappa' components.
  BlasMatrixView<Real> kappa_;
  ConstBlasMatrixView<Real> const_kappa_;

  // A view into 'directions_' of the concatenated primal and dual solutions.
  BlasMatrixView<Real> augmented_system_components_;
  ConstBlasMatrixView<Real> const_augmented_system_components_;

  // A view into 'directions_' of the concatenated primal and dual solutions,
  // plus the tau variable.
  BlasMatrixView<Real> hsd_augmented_system_components_;
  ConstBlasMatrixView<Real> const_hsd_augmented_system_components_;
};

}  // namespace hsd_qp
}  // namespace conic

#include "conic/hsd_qp/stacked_primal_dual_vectors-impl.hpp"

#endif  // ifndef CONIC_HSD_QP_STACKED_PRIMAL_DUAL_VECTORS_H_
