/*
 * Copyright (c) 2019 Jack Poulson <jack@hodgestar.com>
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
#ifndef CONIC_HSD_QP_SOLVE_H_
#define CONIC_HSD_QP_SOLVE_H_

#include "conic/hsd_qp/equilibrate.hpp"
#include "conic/hsd_qp/iterate.hpp"
#include "conic/hsd_qp/problem.hpp"
#include "conic/hsd_qp/projection.hpp"
#include "conic/hsd_qp/residuals.hpp"
#include "conic/hsd_qp/stacked_primal_dual_vectors.hpp"
#include "conic/hsd_qp/util.hpp"
#include "conic/imports.hpp"
#include "conic/util.hpp"

namespace conic {
namespace hsd_qp {

// Configuration parameters for the dynamic estimation of which primal solution
// (in our formulation, also slack) indices are converging to zero.
template <typename Real>
struct IndicatorConfig {
  // The sum of x_i^{k + 1} / x_i^k and |1 - z_i^{k + 1} / z_i^k| must be
  // less than or equal to this value before the Tapia indicator suggests that
  // x_i is zero at the optimal solution.
  //
  // We note that the usage of this indicator should be questioned in our
  // implementation, which incorporates proximal regularization and therefore
  // is unlikely to lead to orders of magnitude increase in the dual variable
  // in a single iteration.
  Real tapia_sum_tolerance = Real(0.2);

  // The ratio x_i^{k + 1} / z_i^{k + 1}.
  Real primal_dual_tolerance = Real(1e-4);
};

// A summary, over the Interior Point Method's history, of how frequently we
// assumed a particular primal solution variable was converging to zero.
template <typename Real>
struct IndicatorStatistic {
  // The primal Tapia indicator is x_i^{k + 1} / x_i^k.
  Real primal_tapia_indicator;

  // The dual Tapia indicator is z_i^{k + 1} / z_i^k.
  Real dual_tapia_indicator;

  // The prima-dual indicator is x_i^{k + 1} / z_i^{k + 1}.
  Real primal_dual_indicator;

  // Whether we believe this x_i index is zero at the optimum.
  bool assumed_zero = false;

  // The number of iterations in a row that this indicator has been active.
  Int num_successive_iterations_assumed_zero = 0;

  // The total number of iterations this indicator has been active.
  Int num_total_iterations_assumed_zero = 0;
};

// The choice of dynamic scaling of the first-order optimality conditions. The
// primary purpose is to combat components of the concatenation of primal and
// dual solution and slack variables from being ignored by an equally-weighted
// two-norm.
enum ScalingStrategy {
  // Do not perform any dynamic rescaling.
  NO_SCALING,

  // Scale the affine complementarity residual norms up to roughly the maximum
  // norm of the other residuals.
  SCALE_UP_COMPLEMENTS,

  // Scale by the inverse of the primal solution/slack, x, and primal scaling
  // slack variable, tau.
  SCALE_BY_SLACKS,

  // Push towards each affine residual norm being roughly equal.
  SCALE_BY_AFFINE,
};

// Configuration parameters for the equilibration and dynamic scaling phases
// of the Interior Point Method.
template <typename Real>
struct ScalingControl {
  // If true, we attempt to minimize the geometric mean of the minimum and
  // maximum nonzero absolute value in each row and column of the homogeneous
  // self-dual embedding matrix
  //
  //  |  Q  -A'  c |
  //  |  A      -b |.
  //  | -c'  b'
  //
  // Otherwise, we iteratively minimize the max-norms of the rows and columns.
  bool geometric_equilibration = false;

  // The number of equilibration sweeps to perform before running the
  // interior-point method.
  Int num_equilibration_passes = 4;

  // The strategy used to dynamically rescale the first-order optimality
  // system.
  ScalingStrategy strategy = SCALE_UP_COMPLEMENTS;

  // The maximum power of epsilon that may be used for rescaling the
  // first-order optimality conditions with a diagonal congruence, i.e.,
  //   (D J D) (inv(D) u) = (D q).
  Real max_rescale_exponent = -0.2;
};

// Configuration parameters for determining optimality of the IPM.
template <typename Real>
struct OptimalityDetectionControl {
  // If true, the primal equality and dual equality residuals are normalized as
  //
  //   || b tau - A x            ||_2 / (|| x ||_2 + tau), and
  //   || c tau + Q x - A' y - z ||_2 / (|| z ||_2 + tau),
  //
  // as opposed to the right-hand side normalization convention of
  //
  //   || b tau - A x            ||_2 / (|| b tau ||_2 + tau), and
  //   || c tau + Q x - A' y - z ||_2 / (|| c tau ||_2 + tau).
  //
  bool iterate_normalization = false;

  // The demanded relative tolerance will be:
  //   coefficient * eps^exponent,
  // where eps is the epsilon associated with 'Real'.
  Real relative_tolerance_coefficient = 7.4e-3;
  Real relative_tolerance_exponent = 0.375;

  // The ratio the relative tolerance must decrease by at each iteration after
  // achieving the target relative error in order to continue iterating.
  Real relative_error_decrease_ratio = 0.8;
};

// A configuration structure for the detection of infeasibility in the IPM.
template <typename Real>
struct InfeasibilityDetectionControl {
  // The multiple of the relative tolerance that the complement ratio must
  // decrease by before testing for tau / kappa being sufficiently small.
  const Real complement_tightening = Real(1e-6);

  // The multiple of the relative tolerance that tau / kappa must be less than
  // before infeasibility is declared.
  const Real tau_over_kappa_tightening = Real(1);
};

// Configuration of the barrier decrease process.
template <typename Real>
struct BarrierControl {
  // With no residual error, we have
  //
  //   |(c + Q x)' x - b' y| = |((A' y + z)' x - (A x)' y| =  |x' z| = x' z.
  //
  // Accounting for an approximately feasible iterate, we have the found:
  //
  // |(c + Q x)' x - b' y| <=
  //     x' z + || c + Q x - A' y - z ||_2 || x ||_2 +
  //            || b - A x            ||_2 || y ||_2.
  //
  // In the presence of a scaling factor tau, the above becomes
  //
  //   |(c + Q x)' x - b' y| <=
  //
  //       [x' z + || c tau + Q x - A' y - z || || x || +
  //               || b tau - A x            || || y ||] / tau.
  //
  // So we bound the next iteration's average barrier, x' z / num_primal, from
  // below with a multiple of
  //
  //   (|| c tau + Q x - A' y - z || || x || + || b tau - A x || || y ||) /
  //       num_primal.
  //
  // This multiple is referred to as the
  // 'average_infeasibility_upper_bound_affine_allowance'.
  //
  // TODO(Jack Poulson): Determine if it is acceptable to continue measuring
  // this upper bound in the rescaled variables.
  //
  // TODO(Jack Poulson): Test whether the values 1e-3 and 1e-4 lead to better
  // performance over the NETLIB LP feasible test suite.
  //
  Real average_infeasibility_upper_bound_affine_allowance = Real(1e-2);

  // Affine barriers of this value or higher are always considered numerically
  // safe, whether or not they are significantly lower than the
  // 'average_infeasibility_upper_bound_affine_allowance' times the average
  // affine infeasibility gap upper bound.
  Real safe_affine = Real(1e-3);

  // The minimum ratio of the current average complement allowable for the
  // affine barrier parameter.
  Real min_affine_to_average_ratio = 1e-3;

  // The maximum ratio of the current average complement allowable for the
  // affine barrier parameter.
  Real max_affine_to_average_ratio = Real(0.9);
};

// A configuration structure for Mehrotra and Gondzio correctors.
template <typename Real>
struct CorrectorControl {
  // The minimum ratio of a step length, relative to its original value, when
  // including a Mehrotra corrector before allowing its incorporation.
  Real mehrotra_accept_ratio = 0.1;

  // When applying a (Gondzio) higher-order corrector -- see subsection 2.2 of
  // [1] -- complements are updated differently depending upon their size
  // relative to the average complement. This paramter controls the ratio of
  // the average complement which forms the lower boundary of the (nonsymmetric)
  // generalization of a symmetric neighborhood.
  Real gondzio_lower_ratio = 0.2;

  // This multiple of the average complement determines the upper boundary of
  // the (nonsymmetric) generalization of a symmetric neighborhood and is the
  // partner to 'gondzio_lower_ratio'.
  Real gondzio_upper_ratio = 20.;

  // When setting a 'soft' target for complements which lie above
  //
  //    gondzio_upper_ratio * average_complement,
  //
  // we set the target to be equal to
  //
  //    gondzio_upper_decrease_ratio * complement
  //
  // to avoid the large updates implied by a target of
  // gondzio_upper_ratio * average_complement or average_complement.
  Real gondzio_upper_decrease_ratio = 0.2;

  // This is the multiplicative component of the affine rule for increasing the
  // predictor step size into its target value for determining a Gondzio
  // higher-order corrector. Again, see subsection 2.2 of [1].
  Real gondzio_step_ratio = 1.08;

  // This is the additive component of the affine rule for increasing the
  // predictor step size into its target value for determining a Gondzio
  // higher-order corrector. Again, see subsection 2.2 of [1].
  Real gondzio_step_shift = 0.08;

  // The minimum ratio of a step length, relative to its original value, such
  // that we will not stop computing Gondzio correctors.
  Real gondzio_demand_continue_ratio = 1.1;

  // The minimum ratio of a step length, relative to its original value, when
  // including a higher-order corrector before allowing further updates to be
  // computed.
  Real gondzio_min_continue_ratio = 1.01;

  // The minimum ratio of a step length, relative to its original value, when
  // including a higher-order corrector before allowing its incorporation.
  Real gondzio_min_accept_ratio = 0.9;

  // The maximum number of higher-order (Gondzio) correctors to be computed
  // for each step.
  //
  // TODO(Jack Poulson): Add support for automatically choosing this based
  // upon the number of nonzeros in the factorization.
  Int max_gondzio_correctors = 2;
};

// Configuration parameters for proximal two-norm regularization of the primal
// solution and dual solution vectors, as well as how they should change due
// to successful and failed solves.
template <typename Real>
struct ProximalRegularizationControl {
  // The factorization's proximal two-norm regularization scalar for the primal
  // solution will initially take the form
  //   coefficient * eps^{exponent},
  // where eps is the epsilon for the 'Real' type.
  Real initial_factor_primal_solution_coefficient = Real(1e-10);
  Real initial_factor_primal_solution_exponent = 0.25;

  // The factorization's proximal two-norm regularization scalar for the dual
  // solution will initially take the form
  //   coefficient * eps^{exponent},
  // where eps is the epsilon for the 'Real' type.
  Real initial_factor_dual_solution_coefficient = Real(1e-8);
  Real initial_factor_dual_solution_exponent = 0.25;

  // The ratio to multiply the factorization regularization by each time a
  // factorization or affine solve fails.
  Real factor_increase_factor = 10;

  // The ratio to multiply the factorization regularization by each time
  // factorization and solves are considered successful.
  Real factor_decay_factor = 0.8;

  // The target system's proximal two-norm regularization scalar for the primal
  // solution will initially take the form
  //   coefficient * eps^{exponent},
  // where eps is the epsilon for the 'Real' type.
  Real initial_target_primal_solution_coefficient = Real(1e-13);
  Real initial_target_primal_solution_exponent = 0.25;

  // The target system's proximal two-norm regularization scalar for the dual
  // solution will initially take the form
  //   coefficient * eps^{exponent},
  // where eps is the epsilon for the 'Real' type.
  Real initial_target_dual_solution_coefficient = Real(1e-11);
  Real initial_target_dual_solution_exponent = 0.25;

  // The ratio to multiply the target regularization by each time a
  // factorization or affine solve fails.
  Real target_increase_factor = 2;

  // The ratio to multiply the target regularization by each time
  // factorization and solves are considered successful.
  Real target_decay_factor = 0.8;
};

// Configuration parameters for the approximate solution of the (regularized)
// first-order optimality conditions.
template <typename Real>
struct LinearSolveControl {
  // If true, the augmented system involving the primal and dual solution
  // variables is directly factored. Otherwise, the Schur complement onto the
  // dual solution variables is instead.
  bool use_augmented = true;

  // The configuration for the sparse-direct solver for the first-order
  // optimality conditions.
  SparseLDLControl<Real> ldl;

  // The maximum number of iterations allowed for Flexible GMRES.
  Int max_fgmres_iterations = 10;

  // The preferred residual tolerance of the Flexible GMRES solver. We will
  // iterate until either this tolerance, or the maximum number of iterations,
  // is met.
  Real desired_relative_residual = 1e-6;

  // The unregularized first-order optimality systems are solved using the
  // iteratively-refined solution of the proximally-regularized system as a
  // preconditioner. We run up to, say, 10 iterations of Flexible GMRES with
  // said preconditioner and declare failure if the relative residual was not
  // reduced below this value.
  Real demanded_relative_residual = 0.9;

  // The maximum number of iterations of iterative refinement to perform for
  // each solve.
  Int max_iterative_refinement_iterations = 6;

  LinearSolveControl() {
    ldl.SetFactorizationType(catamari::kLDLAdjointFactorization);
  }

  void ConfigureDynamicRegularization(Int num_primal, Int num_dual) {
    catamari::DynamicRegularizationControl<Real> dynamic_regularization =
        ldl.supernodal_control.dynamic_regularization;
    if (use_augmented) {
      dynamic_regularization.signatures.Resize(num_primal + num_dual);
      for (Int i = 0; i < num_primal; ++i) {
        dynamic_regularization.signatures[i] = false;
      }
      for (Int i = num_primal; i < num_primal + num_dual; ++i) {
        dynamic_regularization.signatures[i] = true;
      }
    } else {
      dynamic_regularization.signatures.Resize(num_dual);
      for (Int i = 0; i < num_dual; ++i) {
        dynamic_regularization.signatures[i] = true;
      }
    }
    ldl.SetDynamicRegularization(dynamic_regularization);
  }
};

// Configuration parameters for projecting the dual slack vector, z, via the
// dual equality constraint z = Q x + c - A' y, subject to bounding the
// resulting complements to be within given lower and upper scalar multiples
// of the average complement.
template <typename Real>
struct DualSlackProjectionControl {
  // If true, at each iteration the dual slack vector, z, is computed as the
  // projection of the residual c - A' y onto the two-sided infinity
  // neighborhood defined by x o z = mu e, where
  // mu = (x' z + tau kappa) / (n + 1).
  bool enabled = false;

  // If true, information relating to the projection is printed.
  bool verbose = false;

  // This defines the lowest acceptible ratio of the average complement that we
  // are allowed to decrease an entry of the dual slack down to.
  Real lower_ratio_limit = Real(0.333);

  // This defines the highest acceptible ratio of the average complement that we
  // are allowed to increase an entry of the dual slack up to.
  Real upper_ratio_limit = Real(3.);
};

// Configuration parameters for the IPM's console/file output.
struct OutputControl {
  // If true, progress information is printed throughout execution.
  bool verbose = false;

  // If true, the (relative) residuals of each block component of the linear
  // system solves are printed.
  bool analyze_residuals = false;

  // If true, the (relative) residuals of each block component of each iterate
  // in the linear system solves are printed.
  bool analyze_iterate_residuals = false;

  // If convergence monitoring is enabled, then summary information is printed
  // into a file with path specified by 'convergence_filename'.
  bool monitor_convergence = false;

  // The filename to output the convergence information into -- if convergence
  // monitoring is enabled.
  std::string convergence_filename = "hsd_qp_convergence.txt";
};

// Starting from the interior-point primal quadratic programming formulation:
//
//   arginf_{x} { c' x + (1 / 2) x' Q x + mu Phi(x) : A x = b, x >= 0 },
//
// where
//
//   Phi(x) = -\sum_i ln(x_i)
//
// is the *barrier* function for the positive orthant. Let us form the
// Lagrangian
//
//   L(x; y, z) = c' x + (1 / 2) x' Q x + mu Phi(z) - y' (A x - b) + z' x.
//
// The first-order optimality conditions are then d(L)_{x; y, z} = 0.
// The components are of the form:
//
//   (d_x)(L)_{x; y, z} = c + Q x - A' y - z = 0,
//
//   (d_y)(L)_{x; y, z} = A x - b = 0,
//
//   (d_s)(L)_{x; y, z} = -mu inv(z) + x = 0,
//
// The inverse in the last condition should be interpreted in the sense of
// the Jordan algebra formed by products of real numbers, and the resulting
// *complementarity* condition is:
//
//   x o z = mu e,
//
// where 'o' is the Jordan algebra product and 'e' is the identity element
// of the Jordan algebra (the vector of all ones).
//
// We use an extension of Xu, Hung, and Ye's feasibility simplification of the
// formulation of
//
//   Yinyu Ye, Michael J. Todd, and Shinji Mizuno,
//   "An O(sqrt(N) L)-iteration homogeneous and self-dual linear programming
//   algorithm"
//
// as described in
//
//   Xiaojie Xu, Pi-Fang Hung, and Yinyu Ye,
//   "A simplified homogenous and self-dual linear programming algorithm and
//   its implementation", September 1993.
//
// In the case of linear programming, they solve the feasibility problem
//
//    |       A   -b | |  y  |   |   0   |   | 0 |
//   {| -A'        c | |  x  | - |   z   | = | 0 |,  x, z, tau, kappa >= 0 },
//    |  b'  -c'     | | tau |   | kappa |   | 0 |
//
// rather than
//
//  arginf_{x, y, z, tau, theta, kappa} { (x_0' z_0 + 1) theta :
//
//    |         A      -b     r_b  | |   y   |   |   0   |   |       0       |
//    |  -A'            c    -r_c  | |   x   | - |   z   | = |       0       |,
//    |   b'   -c'           r_gap | |  tau  |   | kappa |   |       0       |
//    | -r_b'  r_c'  -r_gap        | | theta |   |   0   |   | -x_0' z_0 - 1 |
//
//    x, z, tau, kappa >= 0 },
//
// where r_b = b - A x_0, r_c = c - A' y_0 - z_0, r_gap = 1 + (c' x_0 - b' y_0),
// and 'e' denotes the identity in the associated Jordan algebra: the vector of
// all ones.
//
// Introducing a barrier parameter mu, such that x o z = mu e and
// tau kappa = mu, we follow Meszaros [2] to arrive at the Newton update:
//
//  |                A          |      -b        |         | |   dy   |
//  | -A'            Q          |       c        | -I      | |   dx   |
//  |---------------------------|----------------|---------| |--------|
//  |  b'   -(c + 2 Q x / tau)' | x' Q x / tau^2 |     -1  | |  dtau  | =
//  |---------------------------|----------------|---------| |--------|
//  |                Z          |                |  X      | |   dz   |
//  |                           |     kappa      |     tau | | dkappa |
//
//      | -A x + b tau                        |
//      | A' y - c tau - Q x + z              |
//      |-------------------------------------|
//      | -b' y + c' x + x' Q x / tau + kappa |.
//      |-------------------------------------|
//      | mu e - x o z                        |
//      | mu - tau kappa                      |
//
// Eliminating the dz and dkappa variables yields:
//
//  |               A          |             -b               | |   dy   |
//  | -A'      Q + inv(X) Z    |              c               | |   dx   |
//  |--------------------------|------------------------------| |--------| =
//  |  b'  -(c + 2 Q x / tau)' | x' Q x / tau^2 + kappa / tau | |  dtau  |
//
//    | (-A x + b tau)                                                    |
//    | (A' y - c tau - Q x + z) + inv(X) (mu e - x o z)                  |.
//    |-------------------------------------------------------------------|
//    | (-b' y + c' x + x' Q x / tau + kappa) + inv(tau) (mu - tau kappa) |
//
template <typename Real>
struct SolveControl {
  // The maximum number of interior point iterations.
  Int max_iterations = 100;

  // Configuration of optimality detection.
  OptimalityDetectionControl<Real> optimality;

  // Configuration of infeasibility detection.
  InfeasibilityDetectionControl<Real> infeasibility;

  // Configuration of the barrier decrease process.
  BarrierControl<Real> barrier;

  // Configuration parameters for the approximate solution of the (regularized)
  // first-order optimality conditions.
  LinearSolveControl<Real> linear_solve;

  // The configuration of the proximal regularization of the first-order
  // optimality conditions.
  ProximalRegularizationControl<Real> proximal_regularization;

  // Configuration parameters for the equilibration and dynamic scaling phases.
  ScalingControl<Real> scaling;

  // Configuration of the projection of the dual slack vector, z, via
  // z = Q x + c - A' y.
  DualSlackProjectionControl<Real> dual_slack_projection;

  // Configuration of the optional, manual complement conditioning procedure.
  ComplementConditioningControl<Real> complement_conditioning;

  // Configuration of the Mehrotra and Gondzio complement correction process.
  CorrectorControl<Real> correctors;

  // The affine step size configuration.
  StepSizeConfig<Real> affine_step;

  // The combined -- affine plus corrector(s) -- step size configuration.
  StepSizeConfig<Real> combined_step;

  // Configuration of an (attempted) projection onto strictly complementary
  // optimality or infeasibility certificate.
  ProjectionControl<Real> feasible_projection;
  ProjectionControl<Real> infeasible_projection;

  // Configuration of the solver output.
  OutputControl output;

  // A constructor which ensures that the factorization type is set to
  // indefinite Hermitian.
  SolveControl() {
    linear_solve.ldl.SetFactorizationType(catamari::kLDLAdjointFactorization);
  }
};

// Attempt to solve a primal-dual pair of convex quadratic programs:
//
//   arginf_x { c' x + (1 / 2) x' Q x : A x = b, x >= 0 },
//   argsup_{x, y, z} { b' y - (1 / 2) x' Q x : A' y + z = c + Q x, z >= 0 },
//
// or return a certificate of infeasibility if the problem is ill-posed.
// We return the number of iterations.
//
template <typename Real>
Int Solve(const Problem<Real>& original_problem,
          const SolveControl<Real>& control, Iterate<Real>* certificate);

// References:
//
//  [1]: Marco Colombo and Jacek Gondzio,
//       "Further development of multiple centrality correctors for Interior
//       Point Methods.", University of Edinburgh, MS-2005-001, revised
//       2006. URL: https://www.maths.ed.ac.uk/~gondzio/reports/mcjgMCC.pdf
//
//  [2]: Csaba Meszaros,
//       "The practical behavior of the homogeneous self-dual formulations in
//       interior point methods", Cent Eur J Oper Res, 23:913, 2015.
//       DOI: https://doi.org/10.1007/s10100-013-0336-1
//
//  [3]: E. D. Andersen, C. Roos, and T. Terlaky,
//       "On implementing a primal-dual interior-point method for conic
//       quadratic optimization", Dec. 18, 2000.
//       URL: http://www.optimization-online.org/DB_FILE/2000/12/245.pdf.
//

}  // namespace hsd_qp
}  // namespace conic

#include "conic/hsd_qp/solve-impl.hpp"

#endif  // ifndef CONIC_HSD_QP_SOLVE_H_
