/*
 * Copyright (c) 2019 Jack Poulson <jack@hodgestar.com>
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
#ifndef CONIC_HSD_QP_PROJECTION_H_
#define CONIC_HSD_QP_PROJECTION_H_

#include "conic/hsd_qp/problem.hpp"

namespace conic {
namespace hsd_qp {

// Configuration parameters for projecting an approximate optimality or
// infeasibility certificate.
template <typename Real>
struct ProjectionControl {
  // The proximal two-norm regularizations for the projection step.
  Real primal_solution_prox_reg_exponent = Real(0.25);
  Real dual_solution_prox_reg_exponent = Real(0.25);
  Real scalar_prox_reg_exponent = Real(0.25);

  // The relative residual of the projection's linear solve is allowed to be
  // at most this factor times the relative tolerance.
  Real tightening = Real(1e-3);
};

// Solve a generaliation of the "Case 1" projection from pg. 61 of
//
//   Yinyu Ye, Michael J. Todd, and Shinji Mizuno,
//   "An O(sqrt(n) L)-iteration Homogeneous and Self-Dual Linear Programming
//   Algorithm", Math. of Operations Research, Vol. 19, No. 1, pp. 53--67,
//   1994,
//
// to quadratic programs using the technique described in
//
//   Csaba Meszaros, "The practical behavior of the homogeneous self–dual
//   formulations in interior pointmethods", CEJOR, 2015,
//
//   argmin_{\hat{x_B}, \hat{y}, \hat{tau}} {
//       ||\hat{y} - y||_2^2 + ||\hat{x_B} - x_B||_2^2 + |\hat{tau} - tau|^2 :
//
//       |           Q_B             -A_B' |          c_B         | | x_B |
//       |           A_B                   |          -b          | |  y  | =
//       |---------------------------------|----------------------| |-----|
//       | -(c_B + 2 Q_B x_B / tau)'   b'  | x_B' Q_B x_B / tau^2 | | tau |
//
//           | 0 |
//           | 0 | }.
//           |---|
//           | 0 |
//
// Given relative weights, C_x, C_y, and C_tau, we arrive at Newton updates
//
// |       Q_B + C_x I          -A_B' |              c_B             | |dx_B|
// |          A_B               C_y I |              -b              | |dy  | =
// |----------------------------------|------------------------------| |----|
// | -(c_B + 2 Q_B x_B / tau)'    b'  | x_B' Q_B x_B / tau^2 + C_tau | |dtau|
//
//     | -Q_B x_B + A_B' y - c_B tau          |
//     | -A_B x_B + b tau                     |.
//     |--------------------------------------|
//     | c_B' x_B + x_B' Q_B x_B / tau - b' y |
//
// Negating the first row, we arrive at:
//
// |      -(Q_B + C_x I)         A_B' |            -c_B'             | |dx_B|
// |           A_B              C_y I |             -b               | | dy | =
// |----------------------------------|------------------------------| |----|
// | -(c_B + 2 Q_B x_B / tau)'    b   | x_B' Q_B x_B / tau^2 + C_tau | |dtau|
//
//       | Q_B x_B - A_B' y + c_B tau           |
//       | -A_B x_B + b tau                     |.
//       |--------------------------------------|
//       | c_B' x_B + x_B' Q_B x_B / tau - b' y |
//
// We perform a line search from the last iterate towards the approximate
// projection in order to guarantee non-negativity of x and z and positivity
// of tau.
//
template <typename Real>
bool FeasibleProjection(const Problem<Real>& problem, Real relative_tolerance,
                        const ProjectionControl<Real>& projection_control,
                        const SparseLDLControl<Real>& ldl_control,
                        const RefinedSolveControl<Real>& refined_solve_control,
                        const Iterate<Real>& iterate,
                        const Buffer<bool>& basis_indicator, bool verbose,
                        Iterate<Real>* projected_iterate);

// Assuming that Q = 0, we solve the "Case 2" projection from pg. 61 of
//
//   Yinyu Ye, Michael J. Todd, and Shinji Mizuno,
//   "An O(sqrt(n) L)-iteration Homogeneous and Self-Dual Linear Programming
//   Algorithm", Math. of Operations Research, Vol. 19, No. 1, pp. 53--67,
//   1994,
//
// argmin_{\hat{x_B}, \hat{y}, \hat{kappa}} {
//     ||\hat{y} - y||_2^2 + ||\hat{x_B} - x_B||_2^2 + |\hat{kappa} - kappa|^2 :
//
//     |        -A_B'     | |  x_B  |   | 0 |
//     |  A_B             | |   y   | = | 0 | }.
//     | -c_B'    b'   -1 | | kappa |   | 0 |
//
// We can perform a Newton update for this system of the form:
//
//  | C_x I  -A_B' |             | | dx_B   |   | A_B' y                  |
//  |  A_B   C_y I |             | | dy     | = | -A_B x_B                |.
//  |--------------|-------------| |--------|   |-------------------------|
//  | -c_B'    b'  | C_kappa - 1 | | dkappa |   | c_B' x_B - b' y + kappa |
//
// Negating the first row, we arrive at:
//
//  | -C_x I   A_B' |             | | dx_B   |   | -A_B' y                 |
//  |   A_B   C_y I |             | | dy     | = | -A_B x_B                |.
//  |---------------|-------------| |--------|   |-------------------------|
//  |  -c_B'    b'  | C_kappa - 1 | | dkappa |   | c_B' x_B - b' y + kappa |
//
// The projection is only accepted if it is strictly complementary
// (x_B > 0, z_N > 0) and its relative error is less than or equal to the
// existing iterate's.
//
// TODO(Jack Poulson): Determine how to simultaneously enforce Q_B x_B = 0 and
// A_B x_B = 0 with a symmetric quasi-semidefinite system.
//
template <typename Real>
bool InfeasibleProjection(
    const Problem<Real>& problem, Real relative_tolerance,
    const ProjectionControl<Real>& projection_control,
    const SparseLDLControl<Real>& ldl_control,
    const RefinedSolveControl<Real>& refined_solve_control,
    const Iterate<Real>& iterate, const Buffer<bool>& basis_indicator,
    bool verbose, Iterate<Real>* projected_iterate);

}  // namespace hsd_qp
}  // namespace conic

#include "conic/hsd_qp/projection-impl.hpp"

#endif  // ifndef CONIC_HSD_QP_PROJECTION_H_
