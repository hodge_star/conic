/*
 * Copyright (c) 2019 Jack Poulson <jack@hodgestar.com>
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
#ifndef CONIC_HSD_QP_DELSARTE_H_
#define CONIC_HSD_QP_DELSARTE_H_

#include "conic/hsd_qp/problem.hpp"
#include "conic/hsd_qp/solve.hpp"
#include "conic/imports.hpp"
#include "conic/util.hpp"

namespace conic {
namespace hsd_qp {

// Constructs a Delsarte linear programming formulation for an upper bound on
// the number of code words in F_q^n with minimum distance d, where q is the
// order of the finite field and n is the length of the code.
//
// Given a prime power q and a positive integer n, we can define a Kravchuk, or
// Krawtchouk, polynomial as
//
//   K_k(x; n) = \sum_{j=0}^k (-1)^j (q - 1)^{k - j)
//       choose(x, j) choose(n - x, k - j),
//
// for k = 0, 1, ..., n.
//
// Given a linear code of length n over the finite field of order q, Delsarte
// showed [1] that a result of MacWilliams [2] implies the dictionary upper
// bound of N = \sum_{j=0}^n w_j, where:
//
//   w_0 = 1,
//   w_1 = 0,
//   ...
//   w_{d-1} = 0,
//   w_j >= 0,  d <= j <= n,
//   \sum_{j=0}^n w_j K_i(j; n) >= 0,  0 <= i <= n.
//
// Consider K_i(0; n) = \sum_{j=0}^i (-1)^j (q - 1)^(i - j)
//     choose(0, j) choose(n, i - j) =
//     (q - 1)^i choose(n, i).
//
// Each w_j can be interpreted as the j'th element of the distance distribution
// of a linear code over F_q^n with N total code words:
//
//   w_j = (1 / N) cardinality({(x, y) in code^2 : distance(x, y) = j}).
//
// We can express this linear program in standard form by introducing (n + 1)
// slacks for the nontrivial inequality constraints, s_i, i = 0, ..., n, and
// posing the linear program
//
//   arginf_x { c' x : A x = b, x >= 0 },
//
// where
//
//  x = | w_{d:n} |,  c = | -1 |,  A = | (K_i(j; n))_{i=0:n, j=d:n}, -I |,
//      | s_{0:n} |       |  0 |
//
//  b = | (-K_i(0; n))_{i=0:n} |.
//
// The upper bound on the number of code words is then one more than the
// negative of the resulting objective value.
//
// References:
//
//  [1] Philippe Delsarte, "An algebraic approach to the association schemes of
//      coding theory", Philips Research Reports Supplement, 10, 1973.
//
//  [2] F. Jessie MacWilliams, "A theorem on the distribution of weights in a
//      systematic code", Bell System Technical Journal, 42:79--94, 1963.
//
template <typename Real>
void DelsarteBoundProblem(Int field_order, Int num_elements, Int distance,
                          Problem<Real>* problem);

// Constructs and solves for a Delsarte upper bound on the number of code words
// in a linear code over F_q^n with minimal distance of d.
template <typename Real>
Real DelsarteBound(Int field_order, Int num_elements, Int distance,
                   const SolveControl<Real>& control);

// Given a prime power q and a positive integer n, we can define a Kravchuk, or
// Krawtchouk, polynomial as
//
//   K_k(x; n) = \sum_{j=0}^k (-1)^j (q - 1)^{k - j)
//       choose(x, j) choose(n - x, k - j),
//
// for degree k = 0, 1, ..., n.
//
template <typename Real>
Real KravchukPolynomial(Int prime_power, Int n, Int degree, Int argument);

// Returns the number of different combinations of subsets of a given size that
// can be chosen from a ground set of a given size.
template <typename Real>
Real Choose(Int set_size, Int subset_size);

}  // namespace hsd_qp
}  // namespace conic

#include "conic/hsd_qp/delsarte-impl.hpp"

#endif  // ifndef CONIC_HSD_QP_DELSARTE_H_
