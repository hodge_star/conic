/*
 * Copyright (c) 2019 Jack Poulson <jack@hodgestar.com>
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
#ifndef CONIC_HSD_QP_LEAST_ABSOLUTE_DEVIATIONS_IMPL_H_
#define CONIC_HSD_QP_LEAST_ABSOLUTE_DEVIATIONS_IMPL_H_

#include "conic/hsd_qp/problem.hpp"
#include "conic/imports.hpp"
#include "conic/util.hpp"

namespace conic {
namespace hsd_qp {

template <typename Real>
void LeastAbsoluteDeviationsProblem(const CoordinateMatrix<Real>& matrix,
                                    const ConstBlasMatrixView<Real>& target,
                                    Problem<Real>* problem) {
  const Int matrix_height = matrix.NumRows();
  const Int matrix_width = matrix.NumColumns();
  const Int num_primal = 2 * (matrix_height + matrix_width);
  const Int num_dual = matrix_height;

  // The linear component of the objective is the indicator over the residuals.
  problem->linear_objective.Resize(num_primal, 1, Real(0));
  for (Int i = 0; i < 2 * matrix_height; ++i) {
    problem->linear_objective(i + 2 * matrix_width) = Real(1);
  }

  // The quadratic component of the objective is zero.
  problem->quadratic_objective.Resize(num_primal, num_primal);

  // The equality constraint matrix is | A, -A, I, -I |.
  problem->equality_matrix.Resize(num_dual, num_primal);
  problem->equality_matrix.ReserveEntryAdditions(2 * matrix.NumEntries() +
                                                 2 * matrix_height);
  for (const auto& entry : matrix.Entries()) {
    problem->equality_matrix.QueueEntryAddition(entry.row, entry.column,
                                                entry.value);
    problem->equality_matrix.QueueEntryAddition(
        entry.row, entry.column + matrix_width, -entry.value);
  }
  for (Int i = 0; i < matrix_height; ++i) {
    problem->equality_matrix.QueueEntryAddition(i, i + 2 * matrix_width,
                                                Real(1));
    problem->equality_matrix.QueueEntryAddition(
        i, i + 2 * matrix_width + matrix_height, Real(-1));
  }
  problem->equality_matrix.FlushEntryQueues();

  // The equality right-hand side vector is the original target.
  problem->equality_rhs = target;

  problem->FillNorms();
}

template <typename Real>
bool LeastAbsoluteDeviations(const CoordinateMatrix<Real>& matrix,
                             const ConstBlasMatrixView<Real>& target,
                             const SolveControl<Real>& control,
                             BlasMatrix<Real>* solution) {
  Problem<Real> problem;
  LeastAbsoluteDeviationsProblem(matrix, target, &problem);

  Iterate<Real> iterate;
  Solve(problem, control, &iterate);
  if (iterate.certificate_type != OPTIMALITY_CERTIFICATE) {
    std::cout << "Could not solve Least Absolute Deviations LP." << std::endl;
    return false;
  }

  const Int solution_size = matrix.NumColumns();
  solution->Resize(solution_size, 1);
  const ConstBlasMatrixView<Real> primal_solution = iterate.PrimalSolution();
  const Real tau = iterate.Tau();
  for (Int i = 0; i < solution_size; ++i) {
    const Real positive_component = primal_solution(i) / tau;
    const Real negative_component = primal_solution(i + solution_size) / tau;
    solution->Entry(i) = positive_component - negative_component;
  }
  return true;
}

}  // namespace hsd_qp
}  // namespace conic

#endif  // ifndef CONIC_HSD_QP_LEAST_ABSOLUTE_DEVIATIONS_IMPL_H_
