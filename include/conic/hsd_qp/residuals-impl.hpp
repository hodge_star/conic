/*
 * Copyright (c) 2019 Jack Poulson <jack@hodgestar.com>
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
#ifndef CONIC_HSD_QP_RESIDUALS_IMPL_H_
#define CONIC_HSD_QP_RESIDUALS_IMPL_H_

#include <exception>
#include <limits>

#include "conic/hsd_qp/problem.hpp"

namespace conic {
namespace hsd_qp {

template <typename Real>
void Residuals<Real>::FillPrimalObjective(const Problem<Real>& problem,
                                          const Iterate<Real>& iterate) {
  have_quadratic_objective_ = !problem.quadratic_objective.Entries().Empty();

  const ConstBlasMatrixView<Real> iter_primal_solution =
      iterate.PrimalSolution();
  const Int num_primal = iter_primal_solution.Height();
  const Real tau = iterate.Tau();

  // primal_objective := c' x / tau + (1 / 2) x' Q x / tau^2.
  objective.primal = 0;
  for (Int i = 0; i < num_primal; ++i) {
    objective.primal += problem.linear_objective(i) * iter_primal_solution(i);
  }
  for (const auto& entry : problem.quadratic_objective.Entries()) {
    objective.primal += iter_primal_solution(entry.row) * entry.value *
                        iter_primal_solution(entry.column) / (2 * tau);
  }
  objective.primal /= tau;
}

template <typename Real>
void Residuals<Real>::FillDualObjective(const Problem<Real>& problem,
                                        const Iterate<Real>& iterate) {
  have_quadratic_objective_ = !problem.quadratic_objective.Entries().Empty();

  const ConstBlasMatrixView<Real> iter_primal_solution =
      iterate.PrimalSolution();
  const ConstBlasMatrixView<Real> iter_dual_solution = iterate.DualSolution();
  const Int num_dual = iter_dual_solution.Height();
  const Real tau = iterate.Tau();

  // dual_objective := (b' y - (1 / 2) x' Q x / tau) / tau.
  objective.dual = 0;
  for (Int i = 0; i < num_dual; ++i) {
    objective.dual += problem.equality_rhs(i) * iter_dual_solution(i);
  }
  for (const auto& entry : problem.quadratic_objective.Entries()) {
    objective.dual -= iter_primal_solution(entry.row) * entry.value *
                      iter_primal_solution(entry.column) / (2 * tau);
  }
  objective.dual /= tau;
}

template <typename Real>
void Residuals<Real>::FillEqualityResiduals(const Problem<Real>& problem,
                                            const Iterate<Real>& iterate) {
  have_quadratic_objective_ = !problem.quadratic_objective.Entries().Empty();

  const Int num_primal = problem.equality_matrix.NumColumns();
  const Int num_dual = problem.equality_matrix.NumRows();
  const ConstBlasMatrixView<Real>& iter_primal_solution =
      iterate.PrimalSolution();
  const ConstBlasMatrixView<Real>& iter_dual_solution = iterate.DualSolution();
  const ConstBlasMatrixView<Real>& iter_dual_slack = iterate.DualSlack();
  const Real tau = iterate.Tau();

  // Form the image of the primal solution vector under the equality matrix,
  // A x.
  primal_solution_equality_image.residual.Resize(num_dual, 1, Real(0));
  catamari::ApplySparse(Real(1), problem.equality_matrix, iter_primal_solution,
                        Real(1), &primal_solution_equality_image.residual.view);

  // Form the image of the primal solution vector under the quadratic objective
  // matrix, Q x.
  primal_solution_objective_image.residual.Resize(num_primal, 1, Real(0));
  catamari::ApplySparse(Real(1), problem.quadratic_objective,
                        iter_primal_solution, Real(1),
                        &primal_solution_objective_image.residual.view);

  // Normalize the infeasibility residuals via |c' x|.
  Real unscaled_linear_primal = 0;
  for (Int i = 0; i < num_primal; ++i) {
    unscaled_linear_primal +=
        problem.linear_objective(i) * iter_primal_solution(i);
  }
  primal_solution_equality_image.CompleteFromResidual(
      std::abs(unscaled_linear_primal));
  primal_solution_objective_image.CompleteFromResidual(
      std::abs(unscaled_linear_primal));
  max_dual_infeasibility =
      std::max(primal_solution_equality_image.relative_residual_norm,
               primal_solution_objective_image.relative_residual_norm);

  // Form b tau - A x.
  primal_equality.residual.Resize(num_dual, 1, Real(0));
  for (Int i = 0; i < num_dual; ++i) {
    primal_equality.residual(i) = problem.equality_rhs(i) * tau -
                                  primal_solution_equality_image.residual(i);
  }

  if (iterate_normalization) {
    primal_equality.CompleteFromResidual(EuclideanNorm(iter_primal_solution) +
                                         tau);
  } else {
    primal_equality.CompleteFromResidual(tau * problem.equality_rhs_two_norm +
                                         tau);
  }

  // Form primal infeasibility candidate A' y + z.
  primal_infeasibility.residual = iter_dual_slack;
  catamari::ApplyAdjointSparse(Real(1), problem.equality_matrix,
                               iter_dual_solution, Real(1),
                               &primal_infeasibility.residual.view);

  // Normalize via |b' y|.
  Real unscaled_linear_dual = 0;
  for (Int i = 0; i < num_dual; ++i) {
    unscaled_linear_dual += problem.equality_rhs(i) * iter_dual_solution(i);
  }
  primal_infeasibility.CompleteFromResidual(std::abs(unscaled_linear_dual));

  // Form -(c tau + Q x) and save || c tau + Q x ||_2.
  dual_equality.residual.Resize(num_primal, 1);
  if (have_quadratic_objective_) {
    for (Int i = 0; i < num_primal; ++i) {
      dual_equality.residual(i) =
          -(problem.linear_objective(i) * tau +
            primal_solution_objective_image.residual(i));
    }
  } else {
    for (Int i = 0; i < num_primal; ++i) {
      dual_equality.residual(i) = -problem.linear_objective(i) * tau;
    }
  }
  scaled_linearized_objective_two_norm =
      EuclideanNorm(dual_equality.residual.ConstView());

  // Combine (A' y + z) and (-c tau + Q x).
  for (Int i = 0; i < num_primal; ++i) {
    dual_equality.residual(i) += primal_infeasibility.residual(i);
  }

  if (iterate_normalization) {
    dual_equality.CompleteFromResidual(EuclideanNorm(iter_dual_slack) + tau);
  } else {
    dual_equality.CompleteFromResidual(scaled_linearized_objective_two_norm +
                                       tau);
  }
}

template <typename Real>
void Residuals<Real>::FillHSDGap(const Problem<Real>& problem,
                                 const Iterate<Real>& iterate) {
  have_quadratic_objective_ = !problem.quadratic_objective.Entries().Empty();

  const ConstBlasMatrixView<Real>& iter_primal_solution =
      iterate.PrimalSolution();
  const ConstBlasMatrixView<Real>& iter_dual_solution = iterate.DualSolution();
  const Int num_primal = problem.equality_matrix.NumColumns();
  const Int num_dual = problem.equality_matrix.NumRows();
  const Real tau = iterate.Tau();

  // (c + Q x / tau)' x - b' y + kappa.
  hsd_gap = 0;
  for (Int i = 0; i < num_primal; ++i) {
    hsd_gap += problem.linear_objective(i) * iter_primal_solution(i);
  }
  for (Int i = 0; i < num_dual; ++i) {
    hsd_gap -= problem.equality_rhs(i) * iter_dual_solution(i);
  }
  for (const auto& entry : problem.quadratic_objective.Entries()) {
    hsd_gap += iter_primal_solution(entry.row) * entry.value *
               iter_primal_solution(entry.column) / tau;
  }
  hsd_gap += iterate.Kappa();
}

template <typename Real>
void Residuals<Real>::FillRelativeError(const Problem<Real>& problem,
                                        const Iterate<Real>& iterate) {
  FillEqualityResiduals(problem, iterate);
  FillHSDGap(problem, iterate);

  FillPrimalObjective(problem, iterate);
  FillDualObjective(problem, iterate);
  objective.CompleteFromPrimalDual();

  relative_error = std::max({dual_equality.relative_residual_norm,
                             primal_equality.relative_residual_norm,
                             objective.relative_gap});

  infeasibility_relative_error = std::max({
      dual_equality.residual_two_norm /
          (scaled_linearized_objective_two_norm / iterate.Tau() + 1),
      primal_equality.residual_two_norm / (problem.equality_rhs_two_norm + 1),
      std::abs(hsd_gap) * iterate.Tau() / (std::abs(objective.dual) + 1),
  });
}

template <typename Real>
void Residuals<Real>::PrintRelativeError(std::ostream& os) const {
  if (iterate_normalization) {
    if (have_quadratic_objective_) {
      os << "|| c tau + Q x - A' y - z ||_2 / (|| z ||_2 + tau): "
         << static_cast<double>(dual_equality.relative_residual_norm) << "\n"
         << "|| b tau - A x            ||_2 / (|| x ||_2 + tau): "
         << static_cast<double>(primal_equality.relative_residual_norm) << "\n";
    } else {
      os << "|| c tau + A' y - z ||_2 / (|| z ||_2 + tau): "
         << static_cast<double>(dual_equality.relative_residual_norm) << "\n"
         << "|| b tau - A x      ||_2 / (|| x ||_2 + tau): "
         << static_cast<double>(primal_equality.relative_residual_norm) << "\n";
    }
  } else {
    if (have_quadratic_objective_) {
      os << "|| c tau + Q x - A' y - z ||_2 / (|| c tau + Q x ||_2 + tau): "
         << static_cast<double>(dual_equality.relative_residual_norm) << "\n"
         << "|| b tau - A x            ||_2 / (|| b tau       ||_2 + tau): "
         << static_cast<double>(primal_equality.relative_residual_norm) << "\n";
    } else {
      os << "|| c tau + A' y - z ||_2 / (|| c tau ||_2 + tau): "
         << static_cast<double>(dual_equality.relative_residual_norm) << "\n"
         << "|| b tau - A x      ||_2 / (|| b tau ||_2 + tau): "
         << static_cast<double>(primal_equality.relative_residual_norm) << "\n";
    }
  }

  if (have_quadratic_objective_) {
    os << "|(c + Q x / tau)' x - b' y| / "
          "(|b' y - (1 / 2) x' Q x / tau| + tau): "
       << static_cast<double>(objective.relative_gap) << "\n"
       << "relative error: " << static_cast<double>(relative_error) << "\n";

    os << "(c + Q x / tau)' x - b' y + kappa: " << hsd_gap << "\n";

    os << "|| A x || / |c' x|: "
       << primal_solution_equality_image.relative_residual_norm << "\n"
       << "|| Q x || / |c' x|: "
       << primal_solution_objective_image.relative_residual_norm << "\n"
       << "|| A' y + z || / |b' y|: "
       << primal_infeasibility.relative_residual_norm << std::endl;
  } else {
    os << "|c' x - b' y| / (|b' y| + tau): "
       << static_cast<double>(objective.relative_gap) << "\n"
       << "relative error: " << static_cast<double>(relative_error) << "\n";

    os << "c' x - b' y + kappa: " << hsd_gap << "\n";

    os << "|| A x || / |c' x|: "
       << primal_solution_equality_image.relative_residual_norm << "\n"
       << "|| A' y + z || / |b' y|: "
       << primal_infeasibility.relative_residual_norm << std::endl;
  }
}

}  // namespace hsd_qp
}  // namespace conic

#endif  // ifndef CONIC_HSD_QP_RESIDUALS_IMPL_H_
