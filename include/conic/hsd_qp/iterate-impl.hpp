/*
 * Copyright (c) 2019 Jack Poulson <jack@hodgestar.com>
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
#ifndef CONIC_HSD_QP_ITERATE_IMPL_H_
#define CONIC_HSD_QP_ITERATE_IMPL_H_

#include <exception>
#include <limits>

#include "conic/hsd_qp/iterate.hpp"

namespace conic {
namespace hsd_qp {

template <typename Real>
BlasMatrixView<Real>& Iterate<Real>::PrimalSolution() {
  return stacked_vectors.PrimalSolutions();
}

template <typename Real>
const ConstBlasMatrixView<Real>& Iterate<Real>::PrimalSolution() const {
  return stacked_vectors.PrimalSolutions();
}

template <typename Real>
BlasMatrixView<Real>& Iterate<Real>::DualSolution() {
  return stacked_vectors.DualSolutions();
}

template <typename Real>
const ConstBlasMatrixView<Real>& Iterate<Real>::DualSolution() const {
  return stacked_vectors.DualSolutions();
}

template <typename Real>
Real& Iterate<Real>::Tau() {
  return stacked_vectors.Tau()(0);
}

template <typename Real>
const Real& Iterate<Real>::Tau() const {
  return stacked_vectors.Tau()(0);
}

template <typename Real>
BlasMatrixView<Real>& Iterate<Real>::DualSlack() {
  return stacked_vectors.DualSlacks();
}

template <typename Real>
const ConstBlasMatrixView<Real>& Iterate<Real>::DualSlack() const {
  return stacked_vectors.DualSlacks();
}

template <typename Real>
Real& Iterate<Real>::Kappa() {
  return stacked_vectors.Kappa()(0);
}

template <typename Real>
const Real& Iterate<Real>::Kappa() const {
  return stacked_vectors.Kappa()(0);
}

template <typename Real>
void Iterate<Real>::PrintNorms(std::ostream& os) const {
  os << "|| x ||_2 = " << static_cast<double>(EuclideanNorm(PrimalSolution()))
     << ", || x ||_max = " << static_cast<double>(MaxNorm(PrimalSolution()))
     << "\n"
     << "|| y ||_2 = " << static_cast<double>(EuclideanNorm(DualSolution()))
     << ", || y ||_max = " << static_cast<double>(MaxNorm(DualSolution()))
     << "\n"
     << "|| z ||_2 = " << static_cast<double>(EuclideanNorm(DualSlack()))
     << ", || z ||_max = " << static_cast<double>(MaxNorm(DualSlack())) << "\n"
     << "tau = " << static_cast<double>(Tau()) << "\n"
     << "kappa = " << static_cast<double>(Kappa()) << "\n"
     << "certificate_type = " << certificate_type << std::endl;
}

template <typename Real>
void Iterate<Real>::EnsureInPositiveOrthant() const {
#ifdef CONIC_DEBUG
  Int num_primal_slack_outside = 0;
  Int num_dual_slack_outside = 0;
  const Int num_primal = PrimalSolution().Height();
  for (Int i = 0; i < num_primal; ++i) {
    const Real primal_solution = PrimalSolution()(i);
    if (primal_solution <= Real(0)) {
      ++num_primal_slack_outside;
    }
    if (DualSlack()(i) <= Real(0)) {
      ++num_dual_slack_outside;
    }
  }
  CONIC_ASSERT(Tau() > Real(0), "tau was non-positive.");
  CONIC_ASSERT(Kappa() > Real(0), "kappa was non-positive.");
  CONIC_ASSERT(num_primal_slack_outside == 0, "Primal slack left interior.");
  CONIC_ASSERT(num_dual_slack_outside == 0, "Dual slack left interior.");
#endif  // ifdef CONIC_DEBUG
}

template <typename Real>
std::ostream& operator<<(std::ostream& os, const Iterate<Real>& iterate) {
  os << "x=[\n"
     << iterate.PrimalSolution() << "];\n"
     << "y=[\n"
     << iterate.DualSolution() << "];\n"
     << "z=[\n"
     << iterate.DualSlack() << "];\n"
     << "tau=" << iterate.Tau() << "\n"
     << "kappa=" << iterate.Kappa() << std::endl;
  return os;
}

}  // namespace hsd_qp
}  // namespace conic

#endif  // ifndef CONIC_HSD_QP_ITERATE_IMPL_H_
