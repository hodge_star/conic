/*
 * Copyright (c) 2019 Jack Poulson <jack@hodgestar.com>
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
#ifndef CONIC_HSD_QP_RESIDUALS_H_
#define CONIC_HSD_QP_RESIDUALS_H_

#include "conic/hsd_qp/iterate.hpp"
#include "conic/hsd_qp/problem.hpp"
#include "conic/hsd_qp/stacked_primal_dual_vectors.hpp"
#include "conic/imports.hpp"
#include "conic/residuals.hpp"

namespace conic {
namespace hsd_qp {

// Residuals associated with the Homogeneous and Self-Dual formulation of a
// Quadratic Program in standard form.
template <typename Real>
class Residuals {
 public:
  // The (rescaled) primal and dual objectives and their (relative) gap:
  //   primal_objective = c' x / tau + (1 / 2) x' Q x / tau^2,
  //   dual_objective = b' y / tau - (1 / 2) x' Q x / tau^2,
  //   relative_gap =
  //       |(c + Q (x / tau))' (x / tau) - b' (y / tau)| /
  //           (1 + |b' (y / tau) - (1 / 2) (x / tau)' Q (x / tau)|) =
  //       |(c + Q (x / tau))' x - b' y| /
  //           (tau + |b' y - (1 / 2) x' Q x / tau|).
  ObjectiveResidual<Real> objective;

  // We store the two-norm of the scaled (by tau) linearized objective function,
  //   || c tau + Q x ||_2.
  Real scaled_linearized_objective_two_norm;

  // The relative error for an optimality certificate for the problem:
  //
  //  max(
  //    || c tau + Q x - A' y - z ||_2 / (|| c tau + Q x ||_2 + tau),
  //    || b tau - A x            ||_2 / (|| b tau       ||_2 + tau),
  //    | c' x - b' y + x' Q x / tau | / (|b' y - (1 / 2) x' Q x / tau| + tau)),
  //
  // or
  //
  //  max(
  //    || c tau + Q x - A' y - z ||_2 / (|| z ||_2 + tau),
  //    || b tau - A x            ||_2 / (|| x ||_2 + tau),
  //    | c' x - b' y + x' Q x / tau | / (|b' y - (1 / 2) x' Q x / tau| + tau)).
  //
  Real relative_error;

  // The relative error used to determine whether an infeasibility certificate
  // should be checked:
  //
  //  max(
  //    || c tau + Q x - A' y - z ||_2 / (|| c + Q (x / tau) ||_2 + 1),
  //    || b tau - A x            ||_2 / (|| b               ||_2 + 1),
  //    | (c + Q (x / tau))' x - b' y + kappa | /
  //        (|b' y - (1 / 2) x' Q x / tau| + tau)).
  //
  Real infeasibility_relative_error;

  // primal_equality.residual := -A x + b tau
  EqualityResidual<Real> primal_equality;

  // dual_equality.residual := A' y - c tau + z.
  EqualityResidual<Real> dual_equality;

  // hsd_gap := c' x - b' y + kappa.
  Real hsd_gap;

  // complementarity.residual is canonically mu - x o z.
  EqualityResidual<Real> complementarity;

  // scale_complement_residual is canonically mu - tau kappa.
  Real scale_complement_residual;

  // Certificates of primal infeasibility involve a straight-forward application
  // of the Farkas Lemma:
  //
  //   Either there exists a solution x to { A x = b, x >= 0 }, or there exists
  //   a solution y to { A' y <= 0, b' y > 0 }.
  //
  // We therefore require an approximate certificate of primal infeasibility to
  // satisfy
  //
  //   || A' y + z ||_2 / |b' y| <= tolerance  and  b' y > 0.
  //
  // We set primal_infeasibility.residual := A' y + z.
  EqualityResidual<Real> primal_infeasibility;

  // A certificate of dual infeasibility is easiest to interpret as a
  // certificate of primal unboundedness. The primal objective function is
  //
  //   c' x + (1 / 2) x' Q x,
  //
  // and so, if there is a ray { gamma u : gamma > 0 } for some unit vector u
  // in (R_+)^n which approaches a primal objective of -infinity as gamma
  // increases to +infinity, then we must have that u is in the null space of
  // the quadratic objective matrix, Q.
  //
  // We can recall that an approximate certificate of dual infeasibility for a
  // standard-form *linear* program is a non-negative vector x such that
  //
  //   || A x ||_2 / |c' x| <= tolerance  and  c' x < 0.
  //
  // Given our above analysis, we augment the dual infeasibility certificate to
  // demand
  //
  //   max(|| A x ||_2, || Q x ||_2) / |c' x| <= tolerance  and c' x < 0.
  //
  // We set
  //
  //   primal_solution_equality_image := A x,
  //   primal_solution_objective_image := Q x,
  //
  // and manually compute the maximum of the two normalized residual norms,
  // which we store in 'max_dual_infeasibility'.
  EqualityResidual<Real> primal_solution_equality_image;
  EqualityResidual<Real> primal_solution_objective_image;
  Real max_dual_infeasibility;

  // If true, we normalize the primal equality residual via || x / tau || + 1
  // and the dual equality residual with || z / tau || + 1; otherwise, they are
  // respectively normalized via || b || + 1 and || c + Q (x / tau) || + 1.
  bool iterate_normalization = false;

  // Fills the primal objective value,
  //   c' (x / tau) + (1 / 2) (x / tau)' Q (x / tau).
  void FillPrimalObjective(const Problem<Real>& problem,
                           const Iterate<Real>& iterate);

  // Fills the dual objective value,
  //   b' (y / tau) - (1 / 2) (x / tau)' Q (x / tau).
  void FillDualObjective(const Problem<Real>& problem,
                         const Iterate<Real>& iterate);

  // Fills the primal equality residual, dual equality residual, primal
  // infeasibility residual, and dual infeasibility residuals, as well as their
  // relative residual norms.
  void FillEqualityResiduals(const Problem<Real>& problem,
                             const Iterate<Real>& iterate);

  // Fills the HSD gap.
  void FillHSDGap(const Problem<Real>& problem, const Iterate<Real>& iterate);

  // Computes the relative error of the current approximate solution.
  void FillRelativeError(const Problem<Real>& problem,
                         const Iterate<Real>& iterate);

  // Prints the components of the relative error of the approximate solution.
  void PrintRelativeError(std::ostream& os) const;

 private:
  // This is set to true if the last subroutine call involving a Problem had
  // a quadratic objective with at least one entry.
  bool have_quadratic_objective_ = false;
};

// For tracking the regularization parameters of the interior point method.
template <typename Real>
struct RegularizationState {
  // The two-norm regularization parameter for the primal solution, x.
  Real target_primal_solution_prox_reg;
  Real factored_primal_solution_prox_reg;

  // The two-norm regularization parameter for the dual solution, y.
  Real target_dual_solution_prox_reg;
  Real factored_dual_solution_prox_reg;
};

}  // namespace hsd_qp
}  // namespace conic

#include "conic/hsd_qp/residuals-impl.hpp"

#endif  // ifndef CONIC_HSD_QP_RESIDUALS_H_
