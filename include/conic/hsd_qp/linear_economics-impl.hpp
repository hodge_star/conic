/*
 * Copyright (c) 2019 Jack Poulson <jack@hodgestar.com>
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
#ifndef CONIC_HSD_QP_LINEAR_ECONOMICS_IMPL_H_
#define CONIC_HSD_QP_LINEAR_ECONOMICS_IMPL_H_

#include "conic/hsd_qp/linear_economics.hpp"

namespace conic {
namespace hsd_qp {

template <typename Real>
void MatrixGameProblem(const CoordinateMatrix<Real>& payoff_matrix,
                       Problem<Real>* problem) {
  const Int num_rows = payoff_matrix.NumRows();
  const Int num_columns = payoff_matrix.NumColumns();
  const Int num_orig_entries = payoff_matrix.NumEntries();
  const Int num_primal = num_rows + num_columns + 2;
  const Int num_dual = num_columns + 1;

  // The primal linear objective is [0; 1; -1; 0].
  problem->linear_objective.Resize(num_primal, 1, Real(0));
  problem->linear_objective(num_rows) = Real(1);
  problem->linear_objective(num_rows + 1) = Real(-1);

  // The primal quadratic objective is zero.
  problem->quadratic_objective.Resize(num_primal, num_primal);

  // Construct the equality matrix:
  //
  //   | -P', e, -e, -I |.
  //   |  e', 0,  0,  0 |
  //
  const Int num_entries = num_orig_entries + 3 * num_columns + num_rows;
  problem->equality_matrix.Resize(num_dual, num_primal);
  problem->equality_matrix.ReserveEntryAdditions(num_entries);
  for (const auto& entry : payoff_matrix.Entries()) {
    problem->equality_matrix.QueueEntryAddition(entry.column, entry.row,
                                                -entry.value);
  }
  for (Int i = 0; i < num_columns; ++i) {
    problem->equality_matrix.QueueEntryAddition(i, num_rows, Real(1));
    problem->equality_matrix.QueueEntryAddition(i, num_rows + 1, Real(-1));
    problem->equality_matrix.QueueEntryAddition(i, num_rows + 2 + i, Real(-1));
  }
  for (Int i = 0; i < num_rows; ++i) {
    problem->equality_matrix.QueueEntryAddition(num_columns, i, Real(1));
  }
  problem->equality_matrix.FlushEntryQueues();

  // Construct the equality right-hand side vector, [0; 1].
  problem->equality_rhs.Resize(num_dual, 1, Real(0));
  problem->equality_rhs(num_columns) = Real(1);

  problem->FillNorms();
}

template <typename Real>
Real MatrixGameGKTProblem(const CoordinateMatrix<Real>& payoff_matrix,
                          Problem<Real>* problem) {
  const Int num_rows = payoff_matrix.NumRows();
  const Int num_columns = payoff_matrix.NumColumns();
  const Int num_orig_entries = payoff_matrix.NumEntries();
  const Int num_primal = num_rows + num_columns;
  const Int num_dual = num_rows;

  // Compute the minimum value in the payoff matrix.
  Real minimum_value;
  if (num_orig_entries < num_rows * num_columns) {
    // There exists at least one zero entry.
    minimum_value = Real(0);
  } else {
    minimum_value = std::numeric_limits<Real>::max();
  }
  for (const auto& entry : payoff_matrix.Entries()) {
    minimum_value = std::min(minimum_value, entry.value);
  }

  // The primal linear objective is [1_n; 0_m].
  problem->linear_objective.Resize(num_primal, 1, Real(0));
  for (Int i = 0; i < num_columns; ++i) {
    problem->linear_objective(i) = Real(1);
  }

  // The primal quadratic objective is zero.
  problem->quadratic_objective.Resize(num_primal, num_primal);

  // Construct the equality matrix, [A, -I].
  const Real shift =
      minimum_value <= Real(0) ? Real(1) - minimum_value : Real(0);
  problem->equality_matrix.Resize(num_dual, num_primal);
  const Int num_entries = (shift > Real(0) ? num_rows * num_columns : 0) +
                          num_orig_entries + num_rows;
  problem->equality_matrix.ReserveEntryAdditions(num_entries);
  if (shift > Real(0)) {
    for (Int j = 0; j < num_columns; ++j) {
      for (Int i = 0; i < num_rows; ++i) {
        problem->equality_matrix.QueueEntryAddition(i, j, shift);
      }
    }
  }
  for (const auto& entry : payoff_matrix.Entries()) {
    problem->equality_matrix.QueueEntryAddition(entry.row, entry.column,
                                                entry.value);
  }
  for (Int i = 0; i < num_rows; ++i) {
    problem->equality_matrix.QueueEntryAddition(i, i + num_columns, Real(-1));
  }
  problem->equality_matrix.FlushEntryQueues();

  // Construct the equality right-hand side vector, 1_m.
  problem->equality_rhs.Resize(num_dual, 1, Real(1));

  problem->FillNorms();

  return shift;
}

template <typename Real>
Real MatrixGame(const CoordinateMatrix<Real>& payoff_matrix,
                const SolveControl<Real>& control,
                bool use_gale_kuhn_tucker_approach,
                BlasMatrix<Real>* row_minimization_strategy,
                BlasMatrix<Real>* column_maximization_strategy) {
  const Int num_rows = payoff_matrix.NumRows();
  const Int num_columns = payoff_matrix.NumColumns();

  Real shift = 0;
  Problem<Real> problem;
  if (use_gale_kuhn_tucker_approach) {
    shift = MatrixGameGKTProblem(payoff_matrix, &problem);
  } else {
    MatrixGameProblem(payoff_matrix, &problem);
  }

  Iterate<Real> iterate;
  Solve(problem, control, &iterate);
  if (iterate.certificate_type != OPTIMALITY_CERTIFICATE) {
    std::cerr << "Could not solve auxiliary linear program." << std::endl;
    row_minimization_strategy->Resize(0, 0);
    column_maximization_strategy->Resize(0, 0);
    return std::numeric_limits<Real>::quiet_NaN();
  }
  const ConstBlasMatrixView<Real> primal_solution = iterate.PrimalSolution();
  const ConstBlasMatrixView<Real> dual_solution = iterate.DualSolution();
  const Real tau = iterate.Tau();

  row_minimization_strategy->Resize(num_rows, 1);
  column_maximization_strategy->Resize(num_columns, 1);
  if (use_gale_kuhn_tucker_approach) {
    Real dual_one_norm = 0;
    for (Int i = 0; i < num_rows; ++i) {
      dual_one_norm += dual_solution(i) / tau;
    }

    for (Int i = 0; i < num_rows; ++i) {
      row_minimization_strategy->Entry(i) =
          dual_solution(i) / (tau * dual_one_norm);
    }

    Real primal_one_norm = 0;
    for (Int i = 0; i < num_columns; ++i) {
      primal_one_norm += primal_solution(i) / tau;
    }

    for (Int i = 0; i < num_columns; ++i) {
      column_maximization_strategy->Entry(i) =
          primal_solution(i) / (tau * primal_one_norm);
    }

    const Real value = Real(1) / primal_one_norm - shift;

    return value;
  } else {
    for (Int i = 0; i < num_rows; ++i) {
      row_minimization_strategy->Entry(i) = primal_solution(i) / tau;
    }
    for (Int i = 0; i < num_columns; ++i) {
      column_maximization_strategy->Entry(i) = dual_solution(i) / tau;
    }

    const Real value =
        (primal_solution(num_rows) - primal_solution(num_rows + 1)) / tau;

    return value;
  }
}

template <typename Real>
void TransportationProblem(const BlasMatrix<Real>& costs,
                           const BlasMatrix<Real>& supplies,
                           const BlasMatrix<Real>& demands,
                           Problem<Real>* problem) {
  const Int num_production = costs.Height();
  const Int num_markets = costs.Width();
  const Int num_dual = num_production + num_markets;
  const Int num_primal = num_production * num_markets + num_dual;

  problem->linear_objective.Resize(num_primal, 1, Real(0));
  for (Int i = 0; i < num_production; ++i) {
    for (Int j = 0; j < num_markets; ++j) {
      problem->linear_objective(i * num_markets + j) = costs(i, j);
    }
  }

  problem->quadratic_objective.Resize(num_primal, num_primal);

  problem->equality_matrix.Resize(num_dual, num_primal);
  problem->equality_matrix.ReserveEntryAdditions(
      2 * num_markets * num_production + num_dual);

  // Add in the supply limitations.
  for (Int i = 0; i < num_production; ++i) {
    for (Int j = 0; j < num_markets; ++j) {
      problem->equality_matrix.QueueEntryAddition(i, i * num_markets + j,
                                                  Real(1));
    }
  }

  // Add in the market demand constraints.
  for (Int j = 0; j < num_markets; ++j) {
    for (Int i = 0; i < num_production; ++i) {
      problem->equality_matrix.QueueEntryAddition(
          num_production + j, i * num_markets + j, Real(-1));
    }
  }

  // Add in the identity matrix for the slacks.
  for (Int i = 0; i < num_dual; ++i) {
    problem->equality_matrix.QueueEntryAddition(
        i, num_production * num_markets + i, Real(1));
  }

  problem->equality_matrix.FlushEntryQueues();

  problem->equality_rhs.Resize(num_dual, 1);
  for (Int i = 0; i < num_production; ++i) {
    problem->equality_rhs(i) = supplies(i);
  }
  for (Int j = 0; j < num_markets; ++j) {
    problem->equality_rhs(j + num_production) = -demands(j);
  }

  problem->FillNorms();
}

template <typename Real>
void OptimalAssignment(const BlasMatrix<Real>& ratings,
                       Problem<Real>* problem) {
  const Int num_workers = ratings.Height();
  const Int num_tasks = ratings.Width();
  const Int num_primal = 2 * num_workers * num_tasks;
  const Int num_dual = num_workers + num_tasks + num_workers * num_tasks;

  problem->linear_objective.Resize(num_primal, 1, Real(0));
  for (Int i = 0; i < num_workers; ++i) {
    for (Int j = 0; j < num_tasks; ++j) {
      problem->linear_objective(i * num_tasks + j) = -ratings(i, j);
    }
  }

  problem->quadratic_objective.Resize(num_primal, num_primal);

  problem->equality_matrix.Resize(num_dual, num_primal);
  problem->equality_matrix.ReserveEntryAdditions(4 * num_workers * num_tasks);
  for (Int i = 0; i < num_workers; ++i) {
    for (Int j = 0; j < num_tasks; ++j) {
      problem->equality_matrix.QueueEntryAddition(i, i * num_tasks + j,
                                                  Real(1));
    }
  }
  for (Int j = 0; j < num_tasks; ++j) {
    for (Int i = 0; i < num_workers; ++i) {
      problem->equality_matrix.QueueEntryAddition(num_workers + j,
                                                  i * num_tasks + j, Real(1));
    }
  }
  for (Int i = 0; i < num_workers; ++i) {
    for (Int j = 0; j < num_tasks; ++j) {
      const Int index = i * num_tasks + j;
      problem->equality_matrix.QueueEntryAddition(
          num_workers + num_tasks + index, index, Real(1));
      problem->equality_matrix.QueueEntryAddition(
          num_workers + num_tasks + index, num_workers * num_tasks + index,
          Real(1));
    }
  }
  problem->equality_matrix.FlushEntryQueues();

  problem->equality_rhs.Resize(num_dual, 1, Real(1));

  problem->FillNorms();
}

}  // namespace hsd_qp
}  // namespace conic

#endif  // ifndef CONIC_HSD_QP_LINEAR_ECONOMICS_IMPL_H_
