/*
 * Copyright (c) 2019 Jack Poulson <jack@hodgestar.com>
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
#ifndef CONIC_HSD_QP_PROBLEM_H_
#define CONIC_HSD_QP_PROBLEM_H_

#include "conic/hsd_qp/stacked_primal_dual_vectors.hpp"
#include "conic/imports.hpp"

namespace conic {
namespace hsd_qp {

// A representation of a quadratic program in the nonnegative form:
//
//   arginf_x { c' x + (1 / 2) x' Q x : A x = b, x >= 0 }.
//
// We use an extension of the Xu, Hung, and Ye's feasibility simplification of
// the homogeneous self-dual linear programming formulation of Ye, Todd, and
// Mizono:
//
//    |       A   -b | |  y  |   |   0   |   | 0 |
//   {| -A'        c | |  x  | - |   z   | = | 0 |,  x, z, tau, kappa >= 0 },
//    |  b'  -c'     | | tau |   | kappa |   | 0 |
//
// rather than
//
//  arginf_{x, y, z, tau, theta, kappa} { (x_0' z_0 + 1) theta :
//
//    |         A      -b     r_b  | |   y   |   |   0   |   |       0       |
//    |  -A'            c    -r_c  | |   x   | - |   z   | = |       0       |,
//    |   b'   -c'           r_gap | |  tau  |   | kappa |   |       0       |
//    | -r_b'  r_c'  -r_gap        | | theta |   |   0   |   | -x_0' z_0 - 1 |
//
//    x, z, tau, kappa >= 0 },
//
// where r_b = b - A x_0, r_c = c - A' y_0 - z_0, r_gap = 1 + (c' x_0 - b' y_0),
// and 'e' denotes the identity in the associated Jordan algebra: the vector of
// all ones.
//
// The initial guess (x_0=e, z_0=e, tau_0=1, kappa_0=1, y_0=0) is strictly
// feasible and is the canonical choice.
//
// An extension to quadratically constrained quadratic programming is given in:
//
//   Csaba Meszaros, "The practical behavior of the homogeneous self-dual
//   formulations in interior point methods", CEJOR, Feb. 2013,
//
// but we only make use of the extension to (convex) quadratic programming.
//
//    |              A          -b | |  y  |   |   0   |   | 0 |
//   {| -A'          Q           c | |  x  | - |   z   | = | 0 |,
//    |  b'  -(c + Q x / tau)'     | | tau |   | kappa |   | 0 |
//
//      x, z, tau, kappa >= 0 }.
//
template <typename Real>
struct Problem {
  // The symmetric, quasi-semidefinite matrix 'Q' in the objective function:
  //   c' x + (1 / 2) x' Q x.
  CoordinateMatrix<Real> quadratic_objective;
  Real quadratic_objective_max_norm;

  // The column vector 'c' in the objective function:
  //   c' x + (1 / 2) x' Q x.
  BlasMatrix<Real> linear_objective;
  Real linear_objective_two_norm;

  // The matrix 'A' in the primal equality constraint 'A x = b'.
  CoordinateMatrix<Real> equality_matrix;
  Real equality_matrix_max_norm;

  // If we use a Cholesky factorization of the Schur complement of the first-
  // order optimality conditions onto the dual solution variables, then it is
  // helpful to have the equality matrix in column-major storage. Equivalently,
  // we form the transpose of the equality matrix in row-major storage.
  mutable bool have_equality_matrix_transpose = false;
  mutable CoordinateMatrix<Real> equality_matrix_transpose;

  // The column vector 'b' in the primal equality constraint 'A x = b'.
  BlasMatrix<Real> equality_rhs;
  Real equality_rhs_two_norm;

  // Fills the norm variables from the vectors.
  void FillNorms();

  // Prints the norms to the given output stream.
  void PrintNorms(std::ostream& os) const;

  // Ensures that a cache of the transpose of the equality matrix is available.
  void FormEqualityMatrixTranspose() const;
};

// Pretty-prints the linear programming problem.
template <typename Real>
std::ostream& operator<<(std::ostream& os, const Problem<Real>& problem);

}  // namespace hsd_qp
}  // namespace conic

#include "conic/hsd_qp/problem-impl.hpp"

#endif  // ifndef CONIC_HSD_QP_PROBLEM_H_
