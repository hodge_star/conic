/*
 * Copyright (c) 2019 Jack Poulson <jack@hodgestar.com>
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
#ifndef CONIC_HSD_QP_ITERATE_H_
#define CONIC_HSD_QP_ITERATE_H_

#include "conic/certificate_type.hpp"
#include "conic/hsd_qp/stacked_primal_dual_vectors.hpp"
#include "conic/imports.hpp"
#include "conic/residuals.hpp"

namespace conic {
namespace hsd_qp {

// An approximate certificate for an approximate solution of the (possibly
// infeasible) primal-dual quadratic program:
//
//   arginf_x { c' x + (1 / 2) x' Q x : A x = b, x >= 0 },
//
//   argsup_{x, y, z} { b' y - (1 / 2) x' Q x : A' y + z = c + Q x, z >= 0 }.
//
// We use Meszaros's extension of Xu, Hung, and Ye's simplified feasibility
// formulation:
//
//    |              A          -b | |  y  |   |   0   |   | 0 |
//   {| -A'          Q           c | |  x  | - |   z   | = | 0 |,
//    |  b'  -(c + Q x / tau)'     | | tau |   | kappa |   | 0 |
//
//      x, z, tau, kappa >= 0 }.
//
// If we get to an HSD feasible iterate (x, y, tau; z, kappa) which has
// tau ~= 0, then we can check if we have Farkas infeasible certificates.
//
// The Farkas Lemma guarantees that there is either a solution to
//
//   { A' y <= c + Q x } or to { A x = 0, Q x = 0, x >= 0, c' x < 0 },
//
// and to
//
//   { A x = b, x >= 0 } or { A' y <= 0, b' y > 0 }.
//
// If the HSD solution achieves tau ~= 0, in the sense of tau << kappa, then
// we will have three important consequences:
//
//   1. b' y - c' x - x' Q x / tau ~= kappa should then imply || Q x ||
//      to be proportional to tau, i.e., Q x ~= 0.
//
//   2. A x ~= b tau should then imply A x ~= 0, and
//
//   3. A' y <=~ c tau + Q x, due to (1), should imply A' y <=~ 0.
//
// As discussed in the "Duality in quadratic optimization" section of MOSEK's
// Modeling Cookbook 3.1 at
//
//   https://docs.mosek.com/modeling-cookbook/qcqo.html,
//
// the primal problem is infeasible if and only if there is y such that
// A' y <= 0 and b' y > 0, and the dual problem is infeasible if and only if
// there is x >= 0 such that A x = 0, Q x = 0, and c' x < 0. We are therefore
// justified in checking the values of b' y and c' x to declare a certificate
// of primal or dual infeasibility if tau becomes sufficiently small.
//
// This is similar to the approach of Xu, Hung, and Ye advocated for Linear
// Programs.
//
template <typename Real>
struct Iterate {
  // The certificate status of this iterate.
  CertificateType certificate_type = INVALID_CERTIFICATE;

  // A representation of x, y, tau, z, and kappa.
  StackedPrimalDualVectors<Real> stacked_vectors;

  // Returns a view of the primal solution vector.
  BlasMatrixView<Real>& PrimalSolution();
  const ConstBlasMatrixView<Real>& PrimalSolution() const;

  // Returns a view of the dual solution vector.
  BlasMatrixView<Real>& DualSolution();
  const ConstBlasMatrixView<Real>& DualSolution() const;

  // The non-negative parameter tau in 'A x - b tau = 0'.
  Real& Tau();
  const Real& Tau() const;

  // Returns a view of the dual slack vector.
  BlasMatrixView<Real>& DualSlack();
  const ConstBlasMatrixView<Real>& DualSlack() const;

  // The slack variable 'kappa' for
  // "b' y - c' x - x' Q x / tau - kappa = 0".
  Real& Kappa();
  const Real& Kappa() const;

  // Prints the two-norm and max-norm of each component of the certificate.
  void PrintNorms(std::ostream& os) const;

  // In debug mode, this routine asserts that the primal and dual slacks are
  // in the interior of the positive orthant.
  void EnsureInPositiveOrthant() const;
};

// Pretty-prints the certificate.
template <typename Real>
std::ostream& operator<<(std::ostream& os, const Iterate<Real>& iterate);

}  // namespace hsd_qp
}  // namespace conic

#include "conic/hsd_qp/iterate-impl.hpp"

#endif  // ifndef CONIC_HSD_QP_ITERATE_H_
