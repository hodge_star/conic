/*
 * Copyright (c) 2019 Jack Poulson <jack@hodgestar.com>
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
#ifndef CONIC_HSD_QP_NONNEGATIVE_LEAST_SQUARES_H_
#define CONIC_HSD_QP_NONNEGATIVE_LEAST_SQUARES_H_

#include "conic/hsd_qp/problem.hpp"
#include "conic/imports.hpp"
#include "conic/util.hpp"

namespace conic {
namespace hsd_qp {

// We will formulate a Quadratic Programming problem via converting a
// Non-negative Least Squares regression problem,
//
//   argmin_x { || A x - b ||_2 , x >= 0 }
//
// into the quadratic programming standard form:
//
//   argmin_{x, r^+, r^-} { (1 / 2) | x   |' | 0     | | x   | :
//                                  | r^+ |  |   I   | | r^+ |
//                                  | r^- |  |     I | | r^- |
//
//    | A, I, -I | | x   | = b,  x, r^+, r^- >= 0 }.
//                 | r^+ |
//                 | r^- |
//
template <typename Real>
void NonnegativeLeastSquaresProblem(const CoordinateMatrix<Real>& matrix,
                                    const ConstBlasMatrixView<Real>& target,
                                    Problem<Real>* problem);

// Attempts to solve a Non-negative Least Squares regression problem via
// an interior point method for quadratic programming. If successful, the
// function returns true and fills 'solution' with the approximate solution;
// otherwise, the function returns false.
template <typename Real>
bool NonnegativeLeastSquares(const CoordinateMatrix<Real>& matrix,
                             const ConstBlasMatrixView<Real>& target,
                             const SolveControl<Real>& control,
                             BlasMatrix<Real>* solution);

}  // namespace hsd_qp
}  // namespace conic

#include "conic/hsd_qp/nonnegative_least_squares-impl.hpp"

#endif  // ifndef CONIC_HSD_QP_NONNEGATIVE_LEAST_SQUARES_H_
