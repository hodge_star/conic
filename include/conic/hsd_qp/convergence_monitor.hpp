/*
 * Copyright (c) 2019 Jack Poulson <jack@hodgestar.com>
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
#ifndef CONIC_HSD_QP_CONVERGENCE_MONITOR_H_
#define CONIC_HSD_QP_CONVERGENCE_MONITOR_H_

#include <fstream>

#include "conic/hsd_qp/iterate.hpp"
#include "conic/hsd_qp/residuals.hpp"

namespace conic {
namespace hsd_qp {

class ConvergenceMonitor {
 public:
  // Attempts to open a file with the specified filename for ouputting the
  // convergence history to. If the file is successfully opened, the header
  // information is printed and the return value is 'true'; otherwise, the
  // return value is false.
  bool Open(const std::string& filename);

  // Writes a single line of the convergence history given the iteration number,
  // the current iterate, and the residuals for the unscaled iterate for the
  // original problem.
  template <typename Real>
  void WriteLine(Int iteration, const Iterate<Real>& iterate,
                 const Residuals<Real>& residuals,
                 const RegularizationState<Real>& reg_state,
                 Real last_primal_step, Real last_dual_step);

  // Returns whether there is a currently opened file.
  bool IsOpen() const;

  // Closes any active file.
  void Close();

 private:
  // The number of character columns used for outputting the iteration number.
  static const int kIterateOutputWidth = 4;

  // The number of character columns used for outputiting the relative primal
  // feasibility, || b tau - A x ||_2 normalized by either || b tau || + tau or
  // || x || + tau.
  static const int kPrimalFeasibilityOutputWidth = 10;

  // The number of character columns used for outputting the relative dual
  // feasibility, || c tau + Q x - A' y - z ||_2 normalized by either
  // || c tau || + tau or || z || + tau.
  static const int kDualFeasibilityOutputWidth = 9;

  // The number of character columns used for outputting the primal objective
  // estimate, (c' x  + (1 / 2) x' Q x / tau) / tau.
  static const int kPrimalObjectiveOutputWidth = 10;

  // The number of character columns used for outputting the dual objective
  // estimate, (b' y  - (1 / 2) x' Q x / tau) / tau.
  static const int kDualObjectiveOutputWidth = 10;

  // The number of character columns used for outputting the relative duality
  // gap.
  static const int kRelativeGapOutputWidth = 9;

  // The number of character columns used for ouputting the tau variable.
  static const int kTauOutputWidth = 9;

  // The number of character columns used for ouputting the kappa variable.
  static const int kKappaOutputWidth = 9;

  // The number of character columns used for outputting the average complement
  // of x and z.
  static const int kAverageComplementOutputWidth = 9;

  // The number of character columns used for outputting the maximum ratio
  // between the complements of x and z.
  static const int kComplementRatioOutputWidth = 9;

  // The number of character columns used for outputting the minimum ratio of
  // the dual slack to the primal slack, z / x.
  static const int kMinSlackRatioOutputWidth = 9;

  // The number of character columns used for outputting the maximum ratio of
  // the dual slack to the primal slack, z / x.
  static const int kMaxSlackRatioOutputWidth = 9;

  // The number of character columns used for outputting the last primal step
  // length.
  static const int kLastPrimalStepOutputWidth = 10;

  // The number of character columns used for outputting the last dual step
  // length.
  static const int kLastDualStepOutputWidth = 9;

  // The number of character columns used for outputting the primal solution's
  // proximal regularization coefficient.
  static const int kPrimalProximalRegularizationOutputWidth = 9;

  // The number of character columns used for outputting the dual solution's
  // proximal regularization coefficient.
  static const int kDualProximalRegularizationOutputWidth = 9;

  // The number of character columns used for outputting the primal solution's
  // two-norm.
  static const int kPrimalSolutionNormOutputWidth = 9;

  // The number of character columns used for outputting the dual solution's
  // two-norm.
  static const int kDualSolutionNormOutputWidth = 9;

  // The output file we will write the convergence information to.
  std::ofstream file_;

  // Writes the header line of the convergence file.
  void WriteHeader();
};

}  // namespace hsd_qp
}  // namespace conic

#include "conic/hsd_qp/convergence_monitor-impl.hpp"

#endif  // ifndef CONIC_HSD_QP_CONVERGENCE_MONITOR_H_
