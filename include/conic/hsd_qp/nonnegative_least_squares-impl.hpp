/*
 * Copyright (c) 2019 Jack Poulson <jack@hodgestar.com>
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
#ifndef CONIC_HSD_QP_NONNEGATIVE_LEAST_SQUARES_IMPL_H_
#define CONIC_HSD_QP_NONNEGATIVE_LEAST_SQUARES_IMPL_H_

#include "conic/hsd_qp/problem.hpp"
#include "conic/imports.hpp"
#include "conic/util.hpp"

namespace conic {
namespace hsd_qp {

template <typename Real>
void NonnegativeLeastSquaresProblem(const CoordinateMatrix<Real>& matrix,
                                    const ConstBlasMatrixView<Real>& target,
                                    Problem<Real>* problem) {
  const Int matrix_height = matrix.NumRows();
  const Int matrix_width = matrix.NumColumns();
  const Int num_primal = matrix_width + 2 * matrix_height;
  const Int num_dual = matrix_height;

  // The linear component of the objective is zero.
  problem->linear_objective.Resize(num_primal, 1, Real(0));

  // The quadratic component of the objective is diag(0, I, I).
  problem->quadratic_objective.Resize(num_primal, num_primal);
  problem->quadratic_objective.ReserveEntryAdditions(2 * matrix_height);
  for (Int i = 0; i < 2 * matrix_height; ++i) {
    problem->quadratic_objective.QueueEntryAddition(i + matrix_width,
                                                    i + matrix_width, Real(1));
  }
  problem->quadratic_objective.FlushEntryQueues();

  // The equality constraint matrix is | A, I, -I |.
  problem->equality_matrix.Resize(num_dual, num_primal);
  problem->equality_matrix.ReserveEntryAdditions(matrix.NumEntries() +
                                                 2 * matrix_height);
  for (const auto& entry : matrix.Entries()) {
    problem->equality_matrix.QueueEntryAddition(entry.row, entry.column,
                                                entry.value);
  }
  for (Int i = 0; i < matrix_height; ++i) {
    problem->equality_matrix.QueueEntryAddition(i, i + matrix_width, Real(1));
    problem->equality_matrix.QueueEntryAddition(
        i, i + matrix_width + matrix_height, Real(-1));
  }
  problem->equality_matrix.FlushEntryQueues();

  // The equality right-hand side vector is the original target.
  problem->equality_rhs = target;

  problem->FillNorms();
}

template <typename Real>
bool NonnegativeLeastSquares(const CoordinateMatrix<Real>& matrix,
                             const ConstBlasMatrixView<Real>& target,
                             const SolveControl<Real>& control,
                             BlasMatrix<Real>* solution) {
  Problem<Real> problem;
  NonnegativeLeastSquaresProblem(matrix, target, &problem);

  Iterate<Real> iterate;
  Solve(problem, control, &iterate);
  if (iterate.certificate_type != OPTIMALITY_CERTIFICATE) {
    std::cout << "Could not solve non-negative least squares QP." << std::endl;
    return false;
  }

  const Int solution_size = matrix.NumColumns();
  solution->Resize(solution_size, 1);
  const ConstBlasMatrixView<Real> primal_solution = iterate.PrimalSolution();
  const Real tau = iterate.Tau();
  for (Int i = 0; i < solution_size; ++i) {
    solution->Entry(i) = primal_solution(i) / tau;
  }
  return true;
}

}  // namespace hsd_qp
}  // namespace conic

#endif  // ifndef CONIC_HSD_QP_NONNEGATIVE_LEAST_SQUARES_IMPL_H_
