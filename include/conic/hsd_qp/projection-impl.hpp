/*
 * Copyright (c) 2019 Jack Poulson <jack@hodgestar.com>
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
#ifndef CONIC_HSD_QP_PROJECTION_IMPL_H_
#define CONIC_HSD_QP_PROJECTION_IMPL_H_

#include "conic/hsd_qp/projection.hpp"

namespace conic {
namespace hsd_qp {

template <typename Real>
bool FeasibleProjection(const Problem<Real>& problem, Real relative_tolerance,
                        const ProjectionControl<Real>& projection_control,
                        const SparseLDLControl<Real>& ldl_control,
                        const RefinedSolveControl<Real>& refined_solve_control,
                        const Iterate<Real>& iterate,
                        const Buffer<bool>& basis_indicator, bool verbose,
                        Iterate<Real>* projected_iterate) {
  const ConstBlasMatrixView<Real>& primal_solution = iterate.PrimalSolution();
  const ConstBlasMatrixView<Real>& dual_slack = iterate.DualSlack();
  const ConstBlasMatrixView<Real>& dual_solution = iterate.DualSolution();
  const Int num_primal = problem.equality_matrix.NumColumns();
  const Int num_dual = problem.equality_matrix.NumRows();
  const Real tau = iterate.Tau();
  const Buffer<MatrixEntry<Real>>& equality_entries =
      problem.equality_matrix_transpose.Entries();

  static const Real kEpsilon = std::numeric_limits<Real>::epsilon();
  const Real primal_solution_prox_reg =
      std::pow(kEpsilon, projection_control.primal_solution_prox_reg_exponent);
  const Real dual_solution_prox_reg =
      std::pow(kEpsilon, projection_control.dual_solution_prox_reg_exponent);
  const Real tau_prox_reg =
      std::pow(kEpsilon, projection_control.scalar_prox_reg_exponent);

  problem.FormEqualityMatrixTranspose();

  Int basis_size = 0;
  Int num_basis_equality_entries = 0;
  Buffer<Int> basis_map(num_primal, -1);
  for (Int i = 0; i < num_primal; ++i) {
    if (basis_indicator[i]) {
      basis_map[i] = basis_size++;
      num_basis_equality_entries +=
          problem.equality_matrix_transpose.NumRowEntries(i);
    }
  }

  // Form the upper-left matrix,
  //
  //   | -(Q_B + C_x I)   A_B' |.
  //   |       A_B       C_y I |
  //
  CoordinateMatrix<Real> upper_left;
  upper_left.Resize(basis_size + num_dual, basis_size + num_dual);
  upper_left.ReserveEntryAdditions(basis_size + num_dual +
                                   2 * num_basis_equality_entries +
                                   problem.quadratic_objective.NumEntries());
  for (const auto& entry : problem.quadratic_objective.Entries()) {
    const Int row_basis_index = basis_map[entry.row];
    const Int column_basis_index = basis_map[entry.column];
    if (row_basis_index < 0 || column_basis_index < 0) {
      continue;
    }
    upper_left.QueueEntryAddition(row_basis_index, column_basis_index,
                                  -entry.value);
  }
  for (Int i = 0; i < num_primal; ++i) {
    if (basis_indicator[i]) {
      const Int basis_index = basis_map[i];
      upper_left.QueueEntryAddition(basis_index, basis_index,
                                    -primal_solution_prox_reg);
      const Int col_offset =
          problem.equality_matrix_transpose.RowEntryOffset(i);
      const Int num_col_nonzeros =
          problem.equality_matrix_transpose.NumRowEntries(i);
      for (Int k = 0; k < num_col_nonzeros; ++k) {
        const MatrixEntry<Real>& entry = equality_entries[col_offset + k];
        const Int column = basis_index;
        const Int row = basis_size + entry.column;
        upper_left.QueueEntryAddition(row, column, entry.value);
        upper_left.QueueEntryAddition(column, row, entry.value);
      }
    }
  }
  for (Int i = 0; i < num_dual; ++i) {
    upper_left.QueueEntryAddition(basis_size + i, basis_size + i,
                                  dual_solution_prox_reg);
  }
  upper_left.FlushEntryQueues();

  // Form the full matrix,
  //
  //   |      -(Q_B + C_x I)         A_B' |            -c_B'             |
  //   |           A_B              C_y I |             -b               |,
  //   |----------------------------------|------------------------------|
  //   | -(c_B + 2 Q_B x_B / tau)'    b'  | x_B' Q_B x_B / tau^2 + C_tau |
  //
  // plus the upper-right vector and lower-left vector.
  //
  BlasMatrix<Real> upper_right(basis_size + num_dual, 1);
  BlasMatrix<Real> bottom_left(1, basis_size + num_dual);
  Real bottom_right = tau_prox_reg;
  CoordinateMatrix<Real> full;
  full.Resize(basis_size + num_dual + 1, basis_size + num_dual + 1);
  full.ReserveEntryAdditions(3 * basis_size + 3 * num_dual +
                             2 * num_basis_equality_entries + 1 +
                             problem.quadratic_objective.NumEntries());
  for (const auto& entry : problem.quadratic_objective.Entries()) {
    const Int row_basis_index = basis_map[entry.row];
    const Int column_basis_index = basis_map[entry.column];
    if (row_basis_index < 0 || column_basis_index < 0) {
      continue;
    }
    full.QueueEntryAddition(row_basis_index, column_basis_index, -entry.value);
  }

  // Form -(c_B + 2 Q_B x_B / tau)' and x_B' Q_B x_B / tau^2 + C_tau.
  for (Int i = 0; i < num_primal; ++i) {
    if (basis_indicator[i]) {
      const Int basis_index = basis_map[i];
      bottom_left(0, basis_index) = -problem.linear_objective(i);
    }
  }
  for (const auto& entry : problem.quadratic_objective.Entries()) {
    const Int row_basis_index = basis_map[entry.row];
    const Int column_basis_index = basis_map[entry.column];
    if (row_basis_index < 0 || column_basis_index < 0) {
      continue;
    }
    bottom_left(0, row_basis_index) -=
        Real(2) * entry.value * primal_solution(entry.column) / tau;
    bottom_right += primal_solution(entry.row) * entry.value *
                    primal_solution(entry.column) / (tau * tau);
  }

  for (Int i = 0; i < num_primal; ++i) {
    if (basis_indicator[i]) {
      const Int basis_index = basis_map[i];
      full.QueueEntryAddition(basis_index, basis_index,
                              -primal_solution_prox_reg);
      const Int col_offset =
          problem.equality_matrix_transpose.RowEntryOffset(i);
      const Int num_col_nonzeros =
          problem.equality_matrix_transpose.NumRowEntries(i);
      for (Int k = 0; k < num_col_nonzeros; ++k) {
        const MatrixEntry<Real>& entry = equality_entries[col_offset + k];
        const Int column = basis_index;
        const Int row = basis_size + entry.column;
        full.QueueEntryAddition(row, column, entry.value);
        full.QueueEntryAddition(column, row, entry.value);
      }

      upper_right(basis_index) = -problem.linear_objective(i);

      full.QueueEntryAddition(basis_size + num_dual, basis_index,
                              bottom_left(0, basis_index));
      full.QueueEntryAddition(basis_index, basis_size + num_dual,
                              upper_right(basis_index));
    }
  }

  for (Int i = 0; i < num_dual; ++i) {
    full.QueueEntryAddition(basis_size + i, basis_size + i,
                            dual_solution_prox_reg);

    bottom_left(0, basis_size + i) = problem.equality_rhs(i);
    full.QueueEntryAddition(basis_size + num_dual, basis_size + i,
                            bottom_left(0, basis_size + i));

    upper_right(basis_size + i) = -problem.equality_rhs(i);
    full.QueueEntryAddition(basis_size + i, basis_size + num_dual,
                            upper_right(basis_size + i));
  }
  full.QueueEntryAddition(basis_size + num_dual, basis_size + num_dual,
                          bottom_right);
  full.FlushEntryQueues();

  // Try to factor the upper-left matrix.
  auto ldl_control_mod = ldl_control;
  ldl_control_mod.supernodal_control.dynamic_regularization.enabled = false;
  ldl_control_mod.scalar_control.dynamic_regularization.enabled = false;
  SparseLDL<Real> upper_left_ldl;
  const auto upper_left_ldl_result =
      upper_left_ldl.Factor(upper_left, ldl_control_mod);
  if (upper_left_ldl_result.num_successful_pivots != basis_size + num_dual) {
    if (verbose) {
      std::cout << "Could not factor upper-left piece of projection matrix."
                << std::endl;
    }
    return false;
  }

  // Solve against the upper-right vector.
  upper_left_ldl.RefinedSolve(upper_left, refined_solve_control,
                              &upper_right.view);

  // Form the Schur complement onto the bottom-right entry.
  Real schur_complement = bottom_right;
  for (Int i = 0; i < basis_size + num_dual; ++i) {
    schur_complement -= bottom_left(0, i) * upper_right(i);
  }

  // Define the routine for applying the inverse.
  auto apply_inverse = [&](BlasMatrixView<Real>* rhs) {
    BlasMatrixView<Real> rhs_upper =
        rhs->Submatrix(0, 0, rhs->height - 1, rhs->width);
    BlasMatrixView<Real> rhs_bottom =
        rhs->Submatrix(rhs->height - 1, 0, 1, rhs->width);

    upper_left_ldl.Solve(&rhs_upper);

    // q1 := q1 - J10 q0.
    for (Int k = 0; k < basis_size + num_dual; ++k) {
      for (Int j = 0; j < rhs->width; ++j) {
        rhs_bottom(0, j) -= bottom_left(0, k) * rhs_upper(k, j);
      }
    }

    // q1 := inv(J11 - J10 inv(J00) J01) q1.
    for (Int j = 0; j < rhs->width; ++j) {
      rhs_bottom(0, j) /= schur_complement;
    }

    // q0 := q0 - (inv(J00) J01) q1.
    for (Int i = 0; i < basis_size + num_dual; ++i) {
      for (Int j = 0; j < rhs->width; ++j) {
        rhs_upper(i, j) -= upper_right(i, 0) * rhs_bottom(0, j);
      }
    }
  };

  // Define the routine for applying the forward operator.
  auto apply_matrix = [&](Real alpha, const ConstBlasMatrixView<Real>& input,
                          Real beta, BlasMatrixView<Real>* output) {
    catamari::ApplySparse(alpha, full, input, beta, output);
  };

  // Form the right-hand side.
  BlasMatrix<Real> rhs;
  rhs.Resize(basis_size + num_dual + 1, 1);

  // Form Q_B x_B - A_B' y + c_B tau.
  for (Int i = 0; i < num_primal; ++i) {
    if (basis_indicator[i]) {
      const Int basis_index = basis_map[i];

      // Initialize with c_B tau.
      rhs(basis_index) = problem.linear_objective(i) * iterate.Tau();

      // Add in the -A_B' y contribution.
      const Int col_offset =
          problem.equality_matrix_transpose.RowEntryOffset(i);
      const Int num_col_nonzeros =
          problem.equality_matrix_transpose.NumRowEntries(i);
      for (Int k = 0; k < num_col_nonzeros; ++k) {
        const MatrixEntry<Real>& entry = equality_entries[col_offset + k];
        rhs(basis_index) -= entry.value * dual_solution(entry.column);
      }
    }
  }

  // Add in the Q_B x_B contribution.
  for (const auto& entry : problem.quadratic_objective.Entries()) {
    const Int row_basis_index = basis_map[entry.row];
    const Int column_basis_index = basis_map[entry.column];
    if (row_basis_index < 0 || column_basis_index < 0) {
      continue;
    }
    rhs(row_basis_index) += entry.value * primal_solution(entry.column);
  }

  // Form -A_B x_B + b tau.
  for (Int i = 0; i < num_dual; ++i) {
    rhs(basis_size + i) = problem.equality_rhs(i) * iterate.Tau();
  }
  for (Int i = 0; i < num_primal; ++i) {
    if (basis_indicator[i]) {
      // Add in -A_B x_B.
      const Int col_offset =
          problem.equality_matrix_transpose.RowEntryOffset(i);
      const Int num_col_nonzeros =
          problem.equality_matrix_transpose.NumRowEntries(i);
      for (Int k = 0; k < num_col_nonzeros; ++k) {
        const MatrixEntry<Real>& entry = equality_entries[col_offset + k];
        rhs(basis_size + entry.column) -= entry.value * primal_solution(i);
      }
    }
  }

  // Form c_B' x_B + x_B' Q_B x_B / tau - b' y.
  rhs(basis_size + num_dual) = 0;
  for (Int i = 0; i < num_primal; ++i) {
    if (basis_indicator[i]) {
      rhs(basis_size + num_dual) +=
          problem.linear_objective(i) * primal_solution(i);
    }
  }
  for (const auto& entry : problem.quadratic_objective.Entries()) {
    const Int row_basis_index = basis_map[entry.row];
    const Int column_basis_index = basis_map[entry.column];
    if (row_basis_index < 0 || column_basis_index < 0) {
      continue;
    }
    rhs(basis_size + num_dual) += primal_solution(entry.row) * entry.value *
                                  primal_solution(entry.column) / tau;
  }
  for (Int i = 0; i < num_dual; ++i) {
    rhs(basis_size + num_dual) -= problem.equality_rhs(i) * dual_solution(i);
  }

  // Compute the projection updates for {x_B, y, tau}.
  auto solve_status = catamari::RefinedSolve(apply_matrix, apply_inverse,
                                             refined_solve_control, &rhs.view);
  if (verbose) {
    std::cout << "solve_status.residual_relative_max_norm: "
              << solve_status.residual_relative_max_norm << std::endl;
  }
  if (solve_status.residual_relative_max_norm >
      projection_control.tightening * relative_tolerance) {
    if (verbose) {
      std::cout << "Inaccurate projection solve of relative norm: "
                << static_cast<double>(solve_status.residual_relative_max_norm)
                << std::endl;
    }
    return false;
  }

  // Form the projected x, y, z, and tau, using a line search to prevent the
  // non-negativity of x_B and z_N and to enforce positivity of tau.
  //
  // Since the updated solutions will be of the form:
  //
  //   | x_B^* | := | x_B + alpha dx_B |,
  //   | x_N^* |    | x_N - alpha dx_N |
  //
  //   y^* := y + alpha dy,
  //
  //   tau^* := tau + alpha dtau,
  //
  //   kappa := kappa - alpha kappa,
  //
  // and then, finally,
  //
  // |z_B^*| := |Q_{B, B}, Q_{B, N}| |x_B^*| + |c_B| tau^* - |A_B'| y^*,
  // |z_N^*|    |Q_{N, B}, Q_{N, N}| | 0   |   |c_N|         |A_N'|
  //
  // where we expect z_B^* = 0 for alpha = 1 if our basis was selected
  // correctly. We thus have, in expanded form,
  //
  //   z_B^* := z_B - alpha z_B,
  //
  //   z_N^* := Q_{N, B} (x_B + alpha dx_B) + c_N tau^* - A_N' (y + alpha dy),
  //         =        (Q_{N, B} x_B + c_N tau - A_N' y) +
  //            alpha (Q_{N, B} dx_B + c_N dtau - A_N' dy),
  //
  // so that we could precompute the four matrix-vector products in parentheses
  // and perform an analytical line search. But we instead use the current
  // value for z rather than its projection to ensure positivity.
  //
  BlasMatrix<Real> dual_slack_update(num_primal, 1, Real(0));
  const Buffer<MatrixEntry<Real>>& quadratic_entries =
      problem.quadratic_objective.Entries();
  for (Int i = 0; i < num_primal; ++i) {
    if (basis_indicator[i]) {
      dual_slack_update(i) = -dual_slack(i);
    } else {
      // Initialize with c tau and c dtau.
      dual_slack_update(i) =
          problem.linear_objective(i) * rhs(basis_size + num_dual);

      // Add in Q_{N, B} dx_B
      const Int quadratic_row_offset =
          problem.quadratic_objective.RowEntryOffset(i);
      const Int num_quadratic_row_nonzeros =
          problem.quadratic_objective.NumRowEntries(i);
      for (Int k = 0; k < num_quadratic_row_nonzeros; ++k) {
        const MatrixEntry<Real>& entry =
            quadratic_entries[quadratic_row_offset + k];
        const Int column_basis_index = basis_map[entry.column];
        if (column_basis_index < 0) {
          continue;
        }

        dual_slack_update(i) += entry.value * rhs(column_basis_index);
      }

      // Subtract A_N' dy.
      const Int equality_col_offset =
          problem.equality_matrix_transpose.RowEntryOffset(i);
      const Int num_equality_col_nonzeros =
          problem.equality_matrix_transpose.NumRowEntries(i);
      for (Int k = 0; k < num_equality_col_nonzeros; ++k) {
        const MatrixEntry<Real>& entry =
            equality_entries[equality_col_offset + k];
        dual_slack_update(i) -= entry.value * rhs(basis_size + entry.column);
      }
    }
  }

  // Perform the line search.
  const Real kTauBackoff = 0.999;
  Real step_size = 1;

  // Upper bound the step size using the primal solution update.
  for (Int i = 0; i < num_primal; ++i) {
    const Int basis_index = basis_map[i];
    if (basis_index >= 0) {
      const Real update = rhs(basis_index);
      if (update < 0) {
        const Real upper_bound = primal_solution(i) / -update;
        step_size = std::min(step_size, upper_bound);
      }
    }
  }

  // Upper bound the step size using the tau update.
  const Real tau_update = rhs(basis_size + num_dual);
  if (tau_update < Real(0)) {
    const Real upper_bound = kTauBackoff * tau / -tau_update;
    step_size = std::min(step_size, upper_bound);
  }

  // Upper bound the step size using the dual slack update.
  for (Int i = 0; i < num_primal; ++i) {
    const Real update = dual_slack_update(i);
    if (update < Real(0)) {
      const Real upper_bound = dual_slack(i) / -update;
      step_size = std::min(step_size, upper_bound);
    }
  }

  // Apply the updates.
  if (verbose) {
    std::cout << "projection step size: " << step_size << std::endl;
  }
  *projected_iterate = iterate;
  BlasMatrixView<Real> projected_primal_solution =
      projected_iterate->PrimalSolution();
  BlasMatrixView<Real> projected_dual_slack = projected_iterate->DualSlack();
  BlasMatrixView<Real> projected_dual_solution =
      projected_iterate->DualSolution();
  for (Int i = 0; i < num_primal; ++i) {
    const Int basis_index = basis_map[i];
    if (basis_index >= 0) {
      projected_primal_solution(i) += step_size * rhs(basis_index);
    } else {
      projected_primal_solution(i) *= Real(1) - step_size;
    }
  }
  for (Int i = 0; i < num_dual; ++i) {
    projected_dual_solution(i) += step_size * rhs(basis_size + i);
  }
  for (Int i = 0; i < num_primal; ++i) {
    projected_dual_slack(i) += step_size * dual_slack_update(i);
  }
  projected_iterate->Tau() += step_size * tau_update;
  projected_iterate->Kappa() *= Real(1) - step_size;

  return true;
}

template <typename Real>
bool InfeasibleProjection(
    const Problem<Real>& problem, Real relative_tolerance,
    const ProjectionControl<Real>& projection_control,
    const SparseLDLControl<Real>& ldl_control,
    const RefinedSolveControl<Real>& refined_solve_control,
    const Iterate<Real>& iterate, const Buffer<bool>& basis_indicator,
    bool verbose, Iterate<Real>* projected_iterate) {
  const ConstBlasMatrixView<Real>& primal_solution = iterate.PrimalSolution();
  const ConstBlasMatrixView<Real>& dual_solution = iterate.DualSolution();
  const Int num_primal = problem.equality_matrix.NumColumns();
  const Int num_dual = problem.equality_matrix.NumRows();
  const Buffer<MatrixEntry<Real>>& equality_entries =
      problem.equality_matrix_transpose.Entries();
  if (problem.quadratic_objective_max_norm != Real(0)) {
    return false;
  }

  static const Real kEpsilon = std::numeric_limits<Real>::epsilon();
  const Real primal_solution_prox_reg =
      std::pow(kEpsilon, projection_control.primal_solution_prox_reg_exponent);
  const Real dual_solution_prox_reg =
      std::pow(kEpsilon, projection_control.dual_solution_prox_reg_exponent);
  const Real kappa_prox_reg =
      std::pow(kEpsilon, projection_control.scalar_prox_reg_exponent);

  problem.FormEqualityMatrixTranspose();

  Int basis_size = 0;
  Int num_basis_equality_entries = 0;
  Buffer<Int> basis_map(num_primal, -1);
  for (Int i = 0; i < num_primal; ++i) {
    if (basis_indicator[i]) {
      basis_map[i] = basis_size++;
      num_basis_equality_entries +=
          problem.equality_matrix_transpose.NumRowEntries(i);
    }
  }

  // Form the upper-left matrix,
  //
  //   | -C_x I   A_B' |.
  //   |   A_B   C_y I |
  //
  CoordinateMatrix<Real> upper_left;
  upper_left.Resize(basis_size + num_dual, basis_size + num_dual);
  upper_left.ReserveEntryAdditions(basis_size + num_dual +
                                   2 * num_basis_equality_entries);
  for (Int i = 0; i < num_primal; ++i) {
    if (basis_indicator[i]) {
      const Int basis_index = basis_map[i];
      upper_left.QueueEntryAddition(basis_index, basis_index,
                                    -primal_solution_prox_reg);
      const Int col_offset =
          problem.equality_matrix_transpose.RowEntryOffset(i);
      const Int num_col_nonzeros =
          problem.equality_matrix_transpose.NumRowEntries(i);
      for (Int k = 0; k < num_col_nonzeros; ++k) {
        const MatrixEntry<Real>& entry = equality_entries[col_offset + k];
        const Int column = basis_index;
        const Int row = basis_size + entry.column;
        upper_left.QueueEntryAddition(row, column, entry.value);
        upper_left.QueueEntryAddition(column, row, entry.value);
      }
    }
  }
  for (Int i = 0; i < num_dual; ++i) {
    upper_left.QueueEntryAddition(basis_size + i, basis_size + i,
                                  dual_solution_prox_reg);
  }
  upper_left.FlushEntryQueues();

  // Form the full matrix,
  //
  //   | -C_x I   A_B' |             |
  //   |   A_B   C_y I |             |,
  //   |---------------|-------------|
  //   |  -c_B'    b'  | C_kappa - 1 |
  //
  // plus the lower-left vector.
  //
  BlasMatrix<Real> bottom_left(1, basis_size + num_dual);
  CoordinateMatrix<Real> full;
  full.Resize(basis_size + num_dual + 1, basis_size + num_dual + 1);
  full.ReserveEntryAdditions(2 * basis_size + 2 * num_dual +
                             2 * num_basis_equality_entries + 1);
  for (Int i = 0; i < num_primal; ++i) {
    if (basis_indicator[i]) {
      const Int basis_index = basis_map[i];
      full.QueueEntryAddition(basis_index, basis_index,
                              -primal_solution_prox_reg);
      const Int col_offset =
          problem.equality_matrix_transpose.RowEntryOffset(i);
      const Int num_col_nonzeros =
          problem.equality_matrix_transpose.NumRowEntries(i);
      for (Int k = 0; k < num_col_nonzeros; ++k) {
        const MatrixEntry<Real>& entry = equality_entries[col_offset + k];
        const Int column = basis_index;
        const Int row = basis_size + entry.column;
        full.QueueEntryAddition(row, column, entry.value);
        full.QueueEntryAddition(column, row, entry.value);
      }

      full.QueueEntryAddition(basis_size + num_dual, basis_index,
                              -problem.linear_objective(i));
      bottom_left(0, basis_index) = -problem.linear_objective(i);
    }
  }
  for (Int i = 0; i < num_dual; ++i) {
    full.QueueEntryAddition(basis_size + i, basis_size + i,
                            dual_solution_prox_reg);
    full.QueueEntryAddition(basis_size + num_dual, basis_size + i,
                            problem.equality_rhs(i));
    bottom_left(0, basis_size + i) = problem.equality_rhs(i);
  }
  full.QueueEntryAddition(basis_size + num_dual, basis_size + num_dual,
                          kappa_prox_reg - Real(1));
  full.FlushEntryQueues();

  // Try to factor the upper-left matrix.
  auto ldl_control_mod = ldl_control;
  ldl_control_mod.supernodal_control.dynamic_regularization.enabled = false;
  ldl_control_mod.scalar_control.dynamic_regularization.enabled = false;
  SparseLDL<Real> upper_left_ldl;
  const auto upper_left_ldl_result =
      upper_left_ldl.Factor(upper_left, ldl_control_mod);
  if (upper_left_ldl_result.num_successful_pivots != basis_size + num_dual) {
    if (verbose) {
      std::cout << "Could not factor upper-left piece of projection matrix."
                << std::endl;
    }
    return false;
  }

  // Form the Schur complement onto the bottom-right entry.
  Real schur_complement = kappa_prox_reg - Real(1);

  // Define the routine for applying the inverse.
  auto apply_inverse = [&](BlasMatrixView<Real>* rhs) {
    BlasMatrixView<Real> rhs_upper =
        rhs->Submatrix(0, 0, rhs->height - 1, rhs->width);
    BlasMatrixView<Real> rhs_bottom =
        rhs->Submatrix(rhs->height - 1, 0, 1, rhs->width);

    upper_left_ldl.Solve(&rhs_upper);

    // q1 := q1 - J10 q0.
    for (Int k = 0; k < basis_size + num_dual; ++k) {
      for (Int j = 0; j < rhs->width; ++j) {
        rhs_bottom(0, j) -= bottom_left(0, k) * rhs_upper(k, j);
      }
    }

    // q1 := inv(J11 - J10 inv(J00) J01) q1.
    for (Int j = 0; j < rhs->width; ++j) {
      rhs_bottom(0, j) /= schur_complement;
    }
  };

  // Define the routine for applying the forward operator.
  auto apply_matrix = [&](Real alpha, const ConstBlasMatrixView<Real>& input,
                          Real beta, BlasMatrixView<Real>* output) {
    catamari::ApplySparse(alpha, full, input, beta, output);
  };

  // Form the right-hand side.
  BlasMatrix<Real> rhs;
  rhs.Resize(basis_size + num_dual + 1, 1);

  // Form -A_B' y
  for (Int i = 0; i < num_primal; ++i) {
    if (basis_indicator[i]) {
      const Int basis_index = basis_map[i];
      rhs(basis_index) = Real(0);
      const Int col_offset =
          problem.equality_matrix_transpose.RowEntryOffset(i);
      const Int num_col_nonzeros =
          problem.equality_matrix_transpose.NumRowEntries(i);
      for (Int k = 0; k < num_col_nonzeros; ++k) {
        const MatrixEntry<Real>& entry = equality_entries[col_offset + k];
        rhs(basis_index) -= entry.value * dual_solution(entry.column);
      }
    }
  }

  // Form -A_B x_B
  for (Int i = 0; i < num_dual; ++i) {
    rhs(basis_size + i) = Real(0);
  }
  for (Int i = 0; i < num_primal; ++i) {
    if (basis_indicator[i]) {
      const Int col_offset =
          problem.equality_matrix_transpose.RowEntryOffset(i);
      const Int num_col_nonzeros =
          problem.equality_matrix_transpose.NumRowEntries(i);
      for (Int k = 0; k < num_col_nonzeros; ++k) {
        const MatrixEntry<Real>& entry = equality_entries[col_offset + k];
        rhs(basis_size + entry.column) -= entry.value * primal_solution(i);
      }
    }
  }

  // Form c_B' x_B - b' y + kappa.
  rhs(basis_size + num_dual) = iterate.Kappa();
  for (Int i = 0; i < num_primal; ++i) {
    if (basis_indicator[i]) {
      rhs(basis_size + num_dual) +=
          problem.linear_objective(i) * primal_solution(i);
    }
  }
  for (Int i = 0; i < num_dual; ++i) {
    rhs(basis_size + num_dual) -= problem.equality_rhs(i) * dual_solution(i);
  }

  // Compute the projection updates for {x_B, y, kappa}.
  auto solve_status = catamari::RefinedSolve(apply_matrix, apply_inverse,
                                             refined_solve_control, &rhs.view);
  if (verbose) {
    std::cout << "solve_status.residual_relative_max_norm: "
              << solve_status.residual_relative_max_norm << std::endl;
  }
  if (solve_status.residual_relative_max_norm >
      projection_control.tightening * relative_tolerance) {
    if (verbose) {
      std::cout << "Inaccurate projection solve of relative norm: "
                << static_cast<double>(solve_status.residual_relative_max_norm)
                << std::endl;
    }
    return false;
  }

  // Form the projected x, y, z, and kappa, testing for non-negativity of x_B,
  // z_N, and kappa.
  *projected_iterate = iterate;
  Int num_negative = 0;
  BlasMatrixView<Real> projected_primal_solution =
      projected_iterate->PrimalSolution();
  BlasMatrixView<Real> projected_dual_slack = projected_iterate->DualSlack();
  BlasMatrixView<Real> projected_dual_solution =
      projected_iterate->DualSolution();
  projected_iterate->Kappa() += rhs(basis_size + num_dual);
  for (Int i = 0; i < num_dual; ++i) {
    projected_dual_solution(i) += rhs(basis_size + i);
  }
  if (projected_iterate->Kappa() < Real(0)) {
    if (verbose) {
      std::cout << "projected_kappa: " << projected_iterate->Kappa()
                << std::endl;
    }
    ++num_negative;
  }
  for (Int i = 0; i < num_primal; ++i) {
    if (basis_indicator[i]) {
      const Int basis_index = basis_map[i];
      projected_primal_solution(i) += rhs(basis_index);

      if (projected_primal_solution(i) < Real(0)) {
        if (verbose) {
          std::cout << "projected_primal_solution(" << i
                    << ")=" << projected_primal_solution(i) << std::endl;
        }
        ++num_negative;
      }

      projected_dual_slack(i) = Real(0);
    } else {
      projected_primal_solution(i) = Real(0);

      // z_N = c_N tau - A_N' y.
      projected_dual_slack(i) =
          problem.linear_objective(i) * projected_iterate->Tau();
      const Int col_offset =
          problem.equality_matrix_transpose.RowEntryOffset(i);
      const Int num_col_nonzeros =
          problem.equality_matrix_transpose.NumRowEntries(i);
      for (Int k = 0; k < num_col_nonzeros; ++k) {
        const MatrixEntry<Real>& entry = equality_entries[col_offset + k];
        projected_dual_slack(i) -=
            entry.value * projected_dual_solution(entry.column);
      }

      if (projected_dual_slack(i) < Real(0)) {
        if (verbose) {
          std::cout << "projected_dual_slack(" << i
                    << ")=" << projected_dual_slack(i) << std::endl;
        }
        ++num_negative;
      }
    }
  }

  // Reject the proposal if any entries in x_B, z, or kappa were sufficiently
  // negative.
  if (verbose) {
    std::cout << "num_negative=" << num_negative << std::endl;
  }
  if (num_negative > 0) {
    return false;
  }

  return true;
}

}  // namespace hsd_qp
}  // namespace conic

#endif  // ifndef CONIC_HSD_QP_PROJECTION_IMPL_H_
