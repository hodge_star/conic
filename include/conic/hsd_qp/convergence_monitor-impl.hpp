/*
 * Copyright (c) 2019 Jack Poulson <jack@hodgestar.com>
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
#ifndef CONIC_HSD_QP_CONVERGENCE_MONITOR_IMPL_H_
#define CONIC_HSD_QP_CONVERGENCE_MONITOR_IMPL_H_

#include <iomanip>

#include "conic/hsd_qp/convergence_monitor.hpp"

namespace conic {
namespace hsd_qp {

inline bool ConvergenceMonitor::Open(const std::string& filename) {
  file_.open(filename);
  if (file_.is_open()) {
    WriteHeader();
    return true;
  } else {
    std::cerr << "Could not open the convergence output file " << filename
              << std::endl;
    return false;
  }
}

inline void ConvergenceMonitor::WriteHeader() {
  if (!file_.is_open()) {
    return;
  }
  file_ << std::setw(kIterateOutputWidth) << "Iter"
        << " " << std::setw(kPrimalFeasibilityOutputWidth) << "PrimalFeas"
        << " " << std::setw(kDualFeasibilityOutputWidth) << "DualFeas"
        << " " << std::setw(kPrimalObjectiveOutputWidth) << "PrimalObj"
        << " " << std::setw(kDualObjectiveOutputWidth) << "DualObj"
        << " " << std::setw(kRelativeGapOutputWidth) << "RelGap"
        << " " << std::setw(kTauOutputWidth) << "Tau"
        << " " << std::setw(kKappaOutputWidth) << "Kappa"
        << " " << std::setw(kAverageComplementOutputWidth) << "AvgComp"
        << " " << std::setw(kComplementRatioOutputWidth) << "CompRatio"
        << " " << std::setw(kMinSlackRatioOutputWidth) << "Min z / x"
        << " " << std::setw(kMaxSlackRatioOutputWidth) << "Max z / x"
        << " " << std::setw(kLastPrimalStepOutputWidth) << "PrimalStep"
        << " " << std::setw(kLastDualStepOutputWidth) << "DualStep"
        << " " << std::setw(kPrimalProximalRegularizationOutputWidth)
        << "PrimalReg"
        << " " << std::setw(kDualProximalRegularizationOutputWidth) << "DualReg"
        << " " << std::setw(kPrimalSolutionNormOutputWidth) << "|| x ||_2"
        << " " << std::setw(kDualSolutionNormOutputWidth) << "|| y ||_2"
        << std::endl;
}

template <typename Real>
void ConvergenceMonitor::WriteLine(Int iteration, const Iterate<Real>& iterate,
                                   const Residuals<Real>& residuals,
                                   const RegularizationState<Real>& reg_state,
                                   Real last_primal_step, Real last_dual_step) {
  if (!file_.is_open()) {
    return;
  }

  const ConstBlasMatrixView<Real> primal_solution = iterate.PrimalSolution();
  const ConstBlasMatrixView<Real> dual_solution = iterate.DualSolution();
  const ConstBlasMatrixView<Real> dual_slack = iterate.DualSlack();
  const Int num_primal = primal_solution.Height();

  Real average_complement = 0;
  for (Int i = 0; i < num_primal; ++i) {
    const Real complement = primal_solution(i) * dual_slack(i);
    average_complement += complement;
  }
  average_complement += iterate.Tau() * iterate.Kappa();
  average_complement /= num_primal + 1;

  Real min_complement = average_complement;
  Real max_complement = average_complement;
  for (Int i = 0; i < num_primal; ++i) {
    const Real complement = primal_solution(i) * dual_slack(i);
    min_complement = std::min(min_complement, complement);
    max_complement = std::max(max_complement, complement);
  }
  min_complement = std::min(min_complement, iterate.Tau() * iterate.Kappa());
  max_complement = std::max(max_complement, iterate.Tau() * iterate.Kappa());
  const Real complement_ratio = max_complement / min_complement;

  Real min_ratio = std::numeric_limits<Real>::max();
  Real max_ratio = 0;
  for (Int i = 0; i < num_primal; ++i) {
    const Real ratio = dual_slack(i) / primal_solution(i);
    min_ratio = std::min(min_ratio, ratio);
    max_ratio = std::max(max_ratio, ratio);
  }

  const Real primal_solution_norm = EuclideanNorm(primal_solution);
  const Real dual_solution_norm = EuclideanNorm(dual_solution);

  file_ << std::setw(kIterateOutputWidth) << iteration << " "
        << std::setw(kPrimalFeasibilityOutputWidth) << std::scientific
        << std::setprecision(3)
        << static_cast<double>(residuals.primal_equality.relative_residual_norm)
        << " " << std::setw(kDualFeasibilityOutputWidth) << std::scientific
        << std::setprecision(3)
        << static_cast<double>(residuals.dual_equality.relative_residual_norm)
        << " " << std::setw(kPrimalObjectiveOutputWidth) << std::scientific
        << std::setprecision(3)
        << static_cast<double>(residuals.objective.primal) << " "
        << std::setw(kDualObjectiveOutputWidth) << std::scientific
        << std::setprecision(3) << static_cast<double>(residuals.objective.dual)
        << " " << std::setw(kRelativeGapOutputWidth) << std::scientific
        << std::setprecision(3)
        << static_cast<double>(residuals.objective.relative_gap) << " "
        << std::setw(kTauOutputWidth) << std::scientific << std::setprecision(3)
        << static_cast<double>(iterate.Tau()) << " "
        << std::setw(kKappaOutputWidth) << std::scientific
        << std::setprecision(3) << static_cast<double>(iterate.Kappa()) << " "
        << std::setw(kAverageComplementOutputWidth) << std::scientific
        << std::setprecision(3) << static_cast<double>(average_complement)
        << " " << std::setw(kComplementRatioOutputWidth)
        << static_cast<double>(complement_ratio) << " "
        << std::setw(kMinSlackRatioOutputWidth) << std::scientific
        << std::setprecision(3) << static_cast<double>(min_ratio) << " "
        << std::setw(kMaxSlackRatioOutputWidth) << std::scientific
        << std::setprecision(3) << static_cast<double>(max_ratio) << " "
        << std::setw(kLastPrimalStepOutputWidth) << std::scientific
        << std::setprecision(3) << static_cast<double>(last_primal_step) << " "
        << std::setw(kLastDualStepOutputWidth) << std::scientific
        << std::setprecision(3) << static_cast<double>(last_dual_step) << " "
        << std::setw(kPrimalProximalRegularizationOutputWidth)
        << std::scientific << std::setprecision(3)
        << static_cast<double>(reg_state.factored_primal_solution_prox_reg)
        << " " << std::setw(kDualProximalRegularizationOutputWidth)
        << std::scientific << std::setprecision(3)
        << static_cast<double>(reg_state.factored_dual_solution_prox_reg) << " "
        << std::setw(kPrimalSolutionNormOutputWidth) << std::scientific
        << std::setprecision(3) << static_cast<double>(primal_solution_norm)
        << " " << std::setw(kDualSolutionNormOutputWidth) << std::scientific
        << std::setprecision(3) << static_cast<double>(dual_solution_norm)
        << std::endl;
}

inline void ConvergenceMonitor::Close() { file_.close(); }

inline bool ConvergenceMonitor::IsOpen() const { return file_.is_open(); }

}  // namespace hsd_qp
}  // namespace conic

#endif  // ifndef CONIC_HSD_QP_CONVERGENCE_MONITOR_IMPL_H_
