/*
 * Copyright (c) 2019 Jack Poulson <jack@hodgestar.com>
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
#ifndef CONIC_HSD_QP_EQUILIBRATE_IMPL_H_
#define CONIC_HSD_QP_EQUILIBRATE_IMPL_H_

#include <limits>

#include "conic/hsd_qp/equilibrate.hpp"

namespace conic {
namespace hsd_qp {

template <typename Real>
void GeometricEquilibration(Problem<Real>* problem, Scaling<Real>* scaling,
                            Int num_passes, bool verbose) {
  const Int num_primal = problem->equality_matrix.NumColumns();
  const Int num_dual = problem->equality_matrix.NumRows();
  const Real kEpsilon = std::numeric_limits<Real>::epsilon();
  const Real kMaxValue = std::numeric_limits<Real>::max();

  // TODO(Jack Poulson): Make this configurable.
  const Real kNonzeroThreshold = std::pow(kEpsilon, Real(0.9));

  scaling->primal_scale = Real(1);
  scaling->dual_scale = Real(1);
  scaling->tau_scale = Real(1);
  scaling->equality_row_scaling.Resize(num_dual, 1, Real(1));
  scaling->equality_column_scaling.Resize(num_primal, 1, Real(1));

  auto incorporate_value = [&](const Real& value, Real* min_abs,
                               Real* max_abs) {
    const Real value_abs = std::abs(value);
    if (value_abs < kNonzeroThreshold) {
      return;
    }
    *min_abs = std::min(*min_abs, value_abs);
    *max_abs = std::max(*max_abs, value_abs);
  };

  // Compute the geometric means of the minimum and maximum absolute values.
  auto compute_geometric_mean = [&](const Buffer<Real>& min_abs,
                                    const Buffer<Real>& max_abs,
                                    Buffer<Real>* geometric_mean) {
    const Int num_entries = min_abs.Size();
    geometric_mean->Resize(num_entries);
    for (Int i = 0; i < num_entries; ++i) {
      (*geometric_mean)[i] = std::sqrt(min_abs[i] * max_abs[i]);
    }
  };

  // Initialize the column ranges of the homogeneous self-dual augmented matrix
  //
  //   |  Q  -A'  c |
  //   |  A      -b |.
  //   | -c'  b'    |
  //
  Buffer<Real> min_abs(num_primal + num_dual + 1, kMaxValue);
  Buffer<Real> max_abs(num_primal + num_dual + 1, Real(0));
  for (const auto& entry : problem->quadratic_objective.Entries()) {
    incorporate_value(entry.value, &min_abs[entry.column],
                      &max_abs[entry.column]);
  }
  for (const auto& entry : problem->equality_matrix.Entries()) {
    incorporate_value(entry.value, &min_abs[entry.column],
                      &max_abs[entry.column]);
    incorporate_value(entry.value, &min_abs[num_primal + entry.row],
                      &max_abs[num_primal + entry.row]);
  }
  for (Int j = 0; j < num_primal; ++j) {
    incorporate_value(problem->linear_objective(j), &min_abs[j], &max_abs[j]);
    incorporate_value(problem->linear_objective(j),
                      &min_abs[num_primal + num_dual],
                      &max_abs[num_primal + num_dual]);
  }
  for (Int i = 0; i < num_dual; ++i) {
    incorporate_value(problem->equality_rhs(i), &min_abs[num_primal + i],
                      &max_abs[num_primal + i]);
    incorporate_value(problem->equality_rhs(i), &min_abs[num_primal + num_dual],
                      &max_abs[num_primal + num_dual]);
  }

  // Compute the geometric means.
  Buffer<Real> rescaling;
  compute_geometric_mean(min_abs, max_abs, &rescaling);

  // Perform the rescaling iterations.
  for (Int pass = 0; pass < num_passes; ++pass) {
    if (verbose) {
      // Print the current dynamic range.
      const Real min_abs_value =
          *std::min_element(min_abs.begin(), min_abs.end());
      const Real max_abs_value =
          *std::max_element(max_abs.begin(), max_abs.end());
      const Real dynamic_range = max_abs_value / min_abs_value;
      std::cout << "Pass " << pass << ":\n"
                << "  min_abs: " << min_abs_value
                << ", max_abs: " << max_abs_value
                << ", dynamic_range: " << dynamic_range << std::endl;
    }

    // Divide the rows and columns of the HSD augmented matrix,
    //
    //   |  Q  -A'   c |
    //   |  A       -b |
    //   | -c'  b'     |
    //
    // by the geometric means of the rows and columns (if the maximum wasn't 0).
    // Simultaneously compute the new minimum and maximum absolute values.
    min_abs.Resize(num_primal + num_dual + 1, kMaxValue);
    max_abs.Resize(num_primal + num_dual + 1, Real(0));
    for (auto& entry : problem->quadratic_objective.Entries()) {
      const Real row_mean = rescaling[entry.row];
      const Real column_mean = rescaling[entry.column];
      if (row_mean > Real(0)) {
        entry.value /= row_mean;
      }
      if (column_mean > Real(0)) {
        entry.value /= column_mean;
      }
      incorporate_value(entry.value, &min_abs[entry.column],
                        &max_abs[entry.column]);
    }
    for (auto& entry : problem->equality_matrix.Entries()) {
      const Real row_mean = rescaling[num_primal + entry.row];
      const Real column_mean = rescaling[entry.column];
      if (row_mean > Real(0)) {
        entry.value /= row_mean;
      }
      if (column_mean > Real(0)) {
        entry.value /= column_mean;
      }
      incorporate_value(entry.value, &min_abs[entry.column],
                        &max_abs[entry.column]);
      incorporate_value(entry.value, &min_abs[num_primal + entry.row],
                        &max_abs[num_primal + entry.row]);
    }
    for (Int i = 0; i < num_primal; ++i) {
      const Real row_mean = rescaling[num_primal + num_dual];
      const Real column_mean = rescaling[i];
      if (row_mean > Real(0)) {
        problem->linear_objective(i) /= row_mean;
      }
      if (column_mean > Real(0)) {
        problem->linear_objective(i) /= column_mean;
        scaling->equality_column_scaling(i) *= column_mean;
      }
      incorporate_value(problem->linear_objective(i), &min_abs[i], &max_abs[i]);
      incorporate_value(problem->linear_objective(i),
                        &min_abs[num_primal + num_dual],
                        &max_abs[num_primal + num_dual]);
    }
    for (Int i = 0; i < num_dual; ++i) {
      const Real row_mean = rescaling[num_primal + num_dual];
      const Real column_mean = rescaling[num_primal + i];
      if (row_mean > Real(0)) {
        problem->equality_rhs(i) /= row_mean;
      }
      if (column_mean > Real(0)) {
        problem->equality_rhs(i) /= column_mean;
        scaling->equality_row_scaling(i) *= column_mean;
      }
      incorporate_value(problem->equality_rhs(i), &min_abs[num_primal + i],
                        &max_abs[num_primal + i]);
      incorporate_value(problem->equality_rhs(i),
                        &min_abs[num_primal + num_dual],
                        &max_abs[num_primal + num_dual]);
    }
    if (rescaling[num_primal + num_dual] > Real(0)) {
      scaling->tau_scale *= rescaling[num_primal + num_dual];
    }

    // Compute the new geometric means.
    compute_geometric_mean(min_abs, max_abs, &rescaling);
  }

  if (verbose) {
    // Print the current dynamic range.
    const Real min_abs_value =
        *std::min_element(min_abs.begin(), min_abs.end());
    const Real max_abs_value =
        *std::max_element(max_abs.begin(), max_abs.end());
    const Real dynamic_range = max_abs_value / min_abs_value;
    std::cout << "Final result:\n"
              << "  min_abs: " << min_abs_value
              << ", max_abs: " << max_abs_value
              << ", dynamic_range: " << dynamic_range << std::endl;
  }

  problem->FillNorms();
}

template <typename Real>
void RuizEquilibration(Problem<Real>* problem, Scaling<Real>* scaling,
                       Int num_passes, bool verbose) {
  const Int num_primal = problem->equality_matrix.NumColumns();
  const Int num_dual = problem->equality_matrix.NumRows();

  scaling->primal_scale = Real(1);
  scaling->dual_scale = Real(1);
  scaling->tau_scale = Real(1);
  scaling->equality_row_scaling.Resize(num_dual, 1, Real(1));
  scaling->equality_column_scaling.Resize(num_primal, 1, Real(1));

  auto incorporate_value = [&](const Real& value, Real* max_abs) {
    const Real value_abs = std::abs(value);
    *max_abs = std::max(*max_abs, value_abs);
  };

  // Initialize the column norms of the homogeneous self-dual augmented matrix
  //
  //   |  Q  -A'  c |
  //   |  A      -b |.
  //   | -c'  b'    |
  //
  Buffer<Real> max_abs(num_primal + num_dual + 1, Real(0));
  for (const auto& entry : problem->quadratic_objective.Entries()) {
    incorporate_value(entry.value, &max_abs[entry.column]);
  }
  for (const auto& entry : problem->equality_matrix.Entries()) {
    incorporate_value(entry.value, &max_abs[entry.column]);
    incorporate_value(entry.value, &max_abs[num_primal + entry.row]);
  }
  for (Int j = 0; j < num_primal; ++j) {
    incorporate_value(problem->linear_objective(j), &max_abs[j]);
    incorporate_value(problem->linear_objective(j),
                      &max_abs[num_primal + num_dual]);
  }
  for (Int i = 0; i < num_dual; ++i) {
    incorporate_value(problem->equality_rhs(i), &max_abs[num_primal + i]);
    incorporate_value(problem->equality_rhs(i),
                      &max_abs[num_primal + num_dual]);
  }

  // Compute the square-roots of the column norms.
  Buffer<Real> rescaling(num_primal + num_dual + 1);
  for (Int i = 0; i < num_primal + num_dual + 1; ++i) {
    rescaling[i] = std::sqrt(max_abs[i]);
  }

  // Perform the rescaling iterations.
  for (Int pass = 0; pass < num_passes; ++pass) {
    if (verbose) {
      // Print the current dynamic range.
      const Real min_norm = *std::min_element(max_abs.begin(), max_abs.end());
      const Real max_norm = *std::max_element(max_abs.begin(), max_abs.end());
      const Real range = max_norm / min_norm;
      std::cout << "Pass " << pass << ":\n"
                << "  min_norm: " << min_norm << ", max_norm: " << max_norm
                << ", range: " << range << std::endl;
    }

    // Divide the rows and columns of the HSD augmented matrix,
    //
    //   |  Q  -A'   c |
    //   |  A       -b |
    //   | -c'  b'     |
    //
    // by the square-roots of the row and column norms (if not zero).
    // Simultaneously compute the new maximum absolute values.
    max_abs.Resize(num_primal + num_dual + 1, Real(0));
    for (auto& entry : problem->quadratic_objective.Entries()) {
      const Real row_mean = rescaling[entry.row];
      const Real column_mean = rescaling[entry.column];
      if (row_mean > Real(0)) {
        entry.value /= row_mean;
      }
      if (column_mean > Real(0)) {
        entry.value /= column_mean;
      }
      incorporate_value(entry.value, &max_abs[entry.column]);
    }
    for (auto& entry : problem->equality_matrix.Entries()) {
      const Real row_mean = rescaling[num_primal + entry.row];
      const Real column_mean = rescaling[entry.column];
      if (row_mean > Real(0)) {
        entry.value /= row_mean;
      }
      if (column_mean > Real(0)) {
        entry.value /= column_mean;
      }
      incorporate_value(entry.value, &max_abs[entry.column]);
      incorporate_value(entry.value, &max_abs[num_primal + entry.row]);
    }
    for (Int i = 0; i < num_primal; ++i) {
      const Real row_mean = rescaling[num_primal + num_dual];
      const Real column_mean = rescaling[i];
      if (row_mean > Real(0)) {
        problem->linear_objective(i) /= row_mean;
      }
      if (column_mean > Real(0)) {
        problem->linear_objective(i) /= column_mean;
        scaling->equality_column_scaling(i) *= column_mean;
      }
      incorporate_value(problem->linear_objective(i), &max_abs[i]);
      incorporate_value(problem->linear_objective(i),
                        &max_abs[num_primal + num_dual]);
    }
    for (Int i = 0; i < num_dual; ++i) {
      const Real row_mean = rescaling[num_primal + num_dual];
      const Real column_mean = rescaling[num_primal + i];
      if (row_mean > Real(0)) {
        problem->equality_rhs(i) /= row_mean;
      }
      if (column_mean > Real(0)) {
        problem->equality_rhs(i) /= column_mean;
        scaling->equality_row_scaling(i) *= column_mean;
      }
      incorporate_value(problem->equality_rhs(i), &max_abs[num_primal + i]);
      incorporate_value(problem->equality_rhs(i),
                        &max_abs[num_primal + num_dual]);
    }
    if (rescaling[num_primal + num_dual] > Real(0)) {
      scaling->tau_scale *= rescaling[num_primal + num_dual];
    }

    // Compute the new square-roots.
    for (Int i = 0; i < num_primal + num_dual + 1; ++i) {
      rescaling[i] = std::sqrt(max_abs[i]);
    }
  }
  if (verbose) {
    // Print the current dynamic range.
    const Real min_norm = *std::min_element(max_abs.begin(), max_abs.end());
    const Real max_norm = *std::max_element(max_abs.begin(), max_abs.end());
    const Real range = max_norm / min_norm;
    std::cout << "Final result:\n"
              << "  min_norm: " << min_norm << ", max_norm: " << max_norm
              << ", range: " << range << std::endl;
  }

  problem->FillNorms();
}

template <typename Real>
void RescaleIterate(const Scaling<Real>& scaling, Iterate<Real>* iterate) {
  // Rescale primal_solution, via x := inv(E) x_s.
  BlasMatrixView<Real> primal_solution = iterate->PrimalSolution();
  const Int num_primal = primal_solution.Height();
  for (Int i = 0; i < num_primal; ++i) {
    primal_solution(i) *= scaling.tau_scale * scaling.primal_scale /
                          scaling.equality_column_scaling(i);
  }

  // Rescale dual_solution, via y := gamma inv(D) y_s.
  BlasMatrixView<Real> dual_solution = iterate->DualSolution();
  const Int num_dual = dual_solution.Height();
  for (Int i = 0; i < num_dual; ++i) {
    dual_solution(i) *= scaling.tau_scale * scaling.dual_scale /
                        scaling.equality_row_scaling(i);
  }

  // Rescale dual_slack, via z := gamma E z_s.
  BlasMatrixView<Real> dual_slack = iterate->DualSlack();
  for (Int i = 0; i < num_primal; ++i) {
    dual_slack(i) *= scaling.tau_scale * scaling.dual_scale *
                     scaling.equality_column_scaling(i);
  }
}

}  // namespace hsd_qp
}  // namespace conic

#endif  // ifndef CONIC_HSD_QP_EQUILIBRATE_IMPL_H_
