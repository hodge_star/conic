/*
 * Copyright (c) 2019 Jack Poulson <jack@hodgestar.com>
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
#ifndef CONIC_HSD_QP_LEAST_ABSOLUTE_DEVIATIONS_H_
#define CONIC_HSD_QP_LEAST_ABSOLUTE_DEVIATIONS_H_

#include "conic/hsd_qp/problem.hpp"
#include "conic/imports.hpp"
#include "conic/util.hpp"

namespace conic {
namespace hsd_qp {

// We will formulate a Linear Programming problem via converting a
// Least Absolute Deviations regression problem,
//
//   argmin_x { || A x - b ||_1 , x >= 0 }
//
// into the linear programming standard form:
//
//   argmin_{x^+, x^-, r^+, r^-} { | 0 | | x^+ |' :
//                                 | 0 | | x^- |
//                                 | 1 | | r^+ |
//                                 | 1 | | r^- |
//
//    | A, -A, I, -I | | x^+ | = b,  x^+, x^-, r^+, r^- >= 0 }.
//                     | x^- |
//                     | r^+ |
//                     | r^- |
//
template <typename Real>
void LeastAbsoluteDeviationsProblem(const CoordinateMatrix<Real>& matrix,
                                    const ConstBlasMatrixView<Real>& target,
                                    Problem<Real>* problem);

// Attempts to solve a Least Absolute Deviations regression problem via
// an interior point method for linear programming. If successful, the
// function returns true and fills 'solution' with the approximate solution;
// otherwise, the function returns false.
template <typename Real>
bool LeastAbsoluteDeviations(const CoordinateMatrix<Real>& matrix,
                             const ConstBlasMatrixView<Real>& target,
                             const SolveControl<Real>& control,
                             BlasMatrix<Real>* solution);

}  // namespace hsd_qp
}  // namespace conic

#include "conic/hsd_qp/least_absolute_deviations-impl.hpp"

#endif  // ifndef CONIC_HSD_QP_LEAST_ABSOLUTE_DEVIATIONS_H_
