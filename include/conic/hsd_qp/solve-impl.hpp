/*
 * Copyright (c) 2019 Jack Poulson <jack@hodgestar.com>
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
#ifndef CONIC_HSD_QP_SOLVE_IMPL_H_
#define CONIC_HSD_QP_SOLVE_IMPL_H_

#include <exception>
#include <limits>

#include "conic/hsd_qp/convergence_monitor.hpp"
#include "conic/hsd_qp/first_order_optimality.hpp"
#include "conic/hsd_qp/projection.hpp"
#include "conic/hsd_qp/solve.hpp"
#include "conic/hsd_qp/util.hpp"
#include "conic/imports.hpp"

namespace conic {
namespace hsd_qp {

template <typename Real>
void UpdateIndicators(const Iterate<Real>& old_iterate,
                      const Iterate<Real>& new_iterate,
                      const IndicatorConfig<Real>& config,
                      Buffer<IndicatorStatistic<Real>>* indicators) {
  const ConstBlasMatrixView<Real> old_primal_solution =
      old_iterate.PrimalSolution();
  const ConstBlasMatrixView<Real> new_primal_solution =
      new_iterate.PrimalSolution();
  const ConstBlasMatrixView<Real> old_dual_slack = old_iterate.DualSlack();
  const ConstBlasMatrixView<Real> new_dual_slack = new_iterate.DualSlack();
  const Real old_tau = old_iterate.Tau();
  const Real new_tau = new_iterate.Tau();

  const Int num_primal = old_primal_solution.Height();

  for (Int i = 0; i < num_primal; ++i) {
    const Real old_scaled_primal_solution = old_primal_solution(i) / old_tau;
    const Real new_scaled_primal_solution = new_primal_solution(i) / new_tau;
    const Real old_scaled_dual_slack = old_dual_slack(i) / old_tau;
    const Real new_scaled_dual_slack = new_dual_slack(i) / new_tau;

    IndicatorStatistic<Real>& indicator = (*indicators)[i];
    indicator.primal_tapia_indicator =
        new_scaled_primal_solution / old_scaled_primal_solution;
    indicator.dual_tapia_indicator =
        new_scaled_dual_slack / old_scaled_dual_slack;
    indicator.primal_dual_indicator =
        new_scaled_primal_solution / new_scaled_dual_slack;

    const Real tapia_sum = indicator.primal_tapia_indicator +
                           std::abs(Real(1) - indicator.dual_tapia_indicator);

    if (tapia_sum <= config.tapia_sum_tolerance ||
        indicator.primal_dual_indicator <= config.primal_dual_tolerance) {
      ++indicator.num_successive_iterations_assumed_zero;
      ++indicator.num_total_iterations_assumed_zero;
      indicator.assumed_zero = true;
    } else {
      indicator.assumed_zero = false;
      indicator.num_successive_iterations_assumed_zero = 0;
    }
  }
}

template <typename Real>
void InitializeRegularizationState(
    const ProximalRegularizationControl<Real>& control,
    RegularizationState<Real>* reg_state) {
  static constexpr Real kEpsilon = std::numeric_limits<Real>::epsilon();

  reg_state->factored_primal_solution_prox_reg =
      control.initial_factor_primal_solution_coefficient *
      std::pow(kEpsilon, control.initial_factor_primal_solution_exponent);
  reg_state->factored_dual_solution_prox_reg =
      control.initial_factor_dual_solution_coefficient *
      std::pow(kEpsilon, control.initial_factor_dual_solution_exponent);

  reg_state->target_primal_solution_prox_reg =
      control.initial_target_primal_solution_coefficient *
      std::pow(kEpsilon, control.initial_target_primal_solution_exponent);
  reg_state->target_dual_solution_prox_reg =
      control.initial_target_dual_solution_coefficient *
      std::pow(kEpsilon, control.initial_target_dual_solution_exponent);
}

template <typename Real>
bool UpdatePrimalSolutionRegularizationState(
    Real factor_update_factor, Real target_update_factor, bool verbose,
    RegularizationState<Real>* reg_state) {
  // TODO(Jack Poulson): Make these configurable.
  const Real kMaxFactorPrimalSolutionProxReg = 1;
  const Real kMaxTargetPrimalSolutionProxReg = 1;

  bool nontrivial_update = true;
  if (factor_update_factor >= Real(1)) {
    CONIC_ASSERT(target_update_factor >= Real(1),
                 "Assumed expansive target_update_factor");
    if (reg_state->factored_primal_solution_prox_reg >=
            kMaxFactorPrimalSolutionProxReg &&
        reg_state->target_primal_solution_prox_reg >=
            kMaxTargetPrimalSolutionProxReg) {
      nontrivial_update = false;
    }

    reg_state->factored_primal_solution_prox_reg *= factor_update_factor;
    reg_state->target_primal_solution_prox_reg *= target_update_factor;

    reg_state->factored_primal_solution_prox_reg =
        std::min(reg_state->factored_primal_solution_prox_reg,
                 kMaxFactorPrimalSolutionProxReg);
    reg_state->target_primal_solution_prox_reg =
        std::min(reg_state->target_primal_solution_prox_reg,
                 kMaxTargetPrimalSolutionProxReg);

    if (verbose) {
      std::cout << "Tightened to factored_primal_solution_prox_reg: "
                << reg_state->factored_primal_solution_prox_reg << std::endl;
    }
  } else if (factor_update_factor < Real(1)) {
    CONIC_ASSERT(target_update_factor <= Real(1),
                 "Assumed contractive target_update_factor");
    reg_state->factored_primal_solution_prox_reg *= factor_update_factor;
    reg_state->target_primal_solution_prox_reg *= target_update_factor;

    if (verbose) {
      std::cout << "Relaxed to factored_primal_solution_prox_reg: "
                << reg_state->factored_primal_solution_prox_reg << std::endl;
    }
  }

  return nontrivial_update;
}

template <typename Real>
bool UpdateDualSolutionRegularizationState(
    Real factor_update_factor, Real target_update_factor, bool verbose,
    RegularizationState<Real>* reg_state) {
  // TODO(Jack Poulson): Make these configurable.
  const Real kMaxFactorDualSolutionProxReg = 1;
  const Real kMaxTargetDualSolutionProxReg = 1;

  bool nontrivial_update = true;
  if (factor_update_factor >= Real(1)) {
    CONIC_ASSERT(target_update_factor >= Real(1),
                 "Assumed expansive target_update_factor");
    if (reg_state->factored_dual_solution_prox_reg >=
            kMaxFactorDualSolutionProxReg &&
        reg_state->target_dual_solution_prox_reg >=
            kMaxTargetDualSolutionProxReg) {
      nontrivial_update = false;
    }

    reg_state->factored_dual_solution_prox_reg *= factor_update_factor;
    reg_state->target_dual_solution_prox_reg *= target_update_factor;

    reg_state->factored_dual_solution_prox_reg =
        std::min(reg_state->factored_dual_solution_prox_reg,
                 kMaxFactorDualSolutionProxReg);
    reg_state->target_dual_solution_prox_reg =
        std::min(reg_state->target_dual_solution_prox_reg,
                 kMaxTargetDualSolutionProxReg);

    if (verbose) {
      std::cout << "Tightened to factored_dual_solution_prox_reg: "
                << reg_state->factored_dual_solution_prox_reg << std::endl;
    }
  } else if (factor_update_factor < Real(1)) {
    CONIC_ASSERT(target_update_factor <= Real(1),
                 "Assumed contractive target_update_factor");
    reg_state->factored_dual_solution_prox_reg *= factor_update_factor;
    reg_state->target_dual_solution_prox_reg *= target_update_factor;

    if (verbose) {
      std::cout << "Relaxed to factored_dual_solution_prox_reg: "
                << reg_state->factored_dual_solution_prox_reg << std::endl;
    }
  }

  return nontrivial_update;
}

// log_base(increase_factor * base^exponent) =
//     log_base(increase_factor) + log_base(base^exponent) =
//     log(increase_factor) / log(base) + exponent.
template <typename Real>
void IncreaseExponent(Real increase_factor, Real* exponent) {
  static constexpr Real base = std::numeric_limits<Real>::epsilon();
  *exponent += std::log(increase_factor) / std::log(base);
  *exponent = std::max(*exponent, Real(0));
}

template <typename Real>
void UpdateDynamicRegularization(Real increase_factor,
                                 SparseLDLControl<Real>* control) {
  IncreaseExponent(increase_factor,
                   &control->scalar_control.dynamic_regularization
                        .positive_threshold_exponent);
  IncreaseExponent(increase_factor,
                   &control->scalar_control.dynamic_regularization
                        .negative_threshold_exponent);
  IncreaseExponent(increase_factor,
                   &control->supernodal_control.dynamic_regularization
                        .positive_threshold_exponent);
  IncreaseExponent(increase_factor,
                   &control->supernodal_control.dynamic_regularization
                        .negative_threshold_exponent);
}

template <typename Real>
void PrintUpdateResiduals(const Problem<Real>& problem,
                          bool iterate_normalization,
                          const RegularizationState<Real>& reg_state,
                          const Iterate<Real>& iterate,
                          const Iterate<Real>& update, std::ostream& os) {
  Iterate<Real> updated_iterate = iterate;
  UpdateIterate(Real(1), update, &updated_iterate);

  std::cout << "Proposed update norms: " << std::endl;
  update.PrintNorms(std::cout);

  std::cout << "Regularization state: "
            << "\n"
            << "  target_primal_solution_prox_reg: "
            << static_cast<double>(reg_state.target_primal_solution_prox_reg)
            << "\n"
            << "  target_dual_solution_prox_reg: "
            << static_cast<double>(reg_state.target_dual_solution_prox_reg)
            << "\n"
            << "  factored_primal_solution_prox_reg: "
            << static_cast<double>(reg_state.factored_primal_solution_prox_reg)
            << "\n"
            << "  factored_dual_solution_prox_reg: "
            << static_cast<double>(reg_state.factored_dual_solution_prox_reg)
            << std::endl;

  std::cout << "Proposed updated residuals (updated tau: "
            << static_cast<double>(updated_iterate.Tau()) << "):" << std::endl;
  Residuals<Real> residuals;
  residuals.iterate_normalization = iterate_normalization;

  residuals.FillRelativeError(problem, updated_iterate);
  residuals.PrintRelativeError(std::cout);
}

template <typename Real>
void FormAffineComplementarity(const SolveControl<Real>& control,
                               const Iterate<Real>& iterate,
                               Real affine_barrier,
                               Residuals<Real>* residuals) {
  const ConstBlasMatrixView<Real> iter_primal_solution =
      iterate.PrimalSolution();
  const ConstBlasMatrixView<Real> iter_dual_slack = iterate.DualSlack();
  const Int num_primal = iter_primal_solution.Height();

  residuals->complementarity.residual.Resize(num_primal, 1, Real(0));
  for (Int i = 0; i < num_primal; ++i) {
    const Real primal_solution = iter_primal_solution(i);
    const Real primal_slack = primal_solution;
    const Real dual_slack = iter_dual_slack(i);
    residuals->complementarity.residual(i) =
        affine_barrier - primal_slack * dual_slack;
  }
  residuals->complementarity.CompleteFromResidual(Real(1));
  if (control.output.verbose) {
    std::cout << "complementarity.residual_norm: "
              << residuals->complementarity.residual_two_norm << std::endl;
  }

  residuals->scale_complement_residual =
      affine_barrier - iterate.Tau() * iterate.Kappa();
}

template <typename Real>
bool MehrotraCorrectorDirection(
    const Problem<Real>& problem, const SolveControl<Real>& control,
    const RefinedSolveControl<Real>& refined_solve_control,
    const catamari::FGMRESControl<Real>& fgmres_control,
    const Iterate<Real>& iterate,
    const ConstBlasMatrixView<Real>& diagonal_scale,
    const RegularizationState<Real>& reg_state, bool factor_regularization,
    const Iterate<Real>& affine_direction, Real demanded_relative_residual,
    Real predictor_barrier, Real corrector_barrier, bool second_order,
    Residuals<Real>* residuals,
    FirstOrderOptimality<Real>* first_order_optimality, Iterate<Real>* update) {
  const Int num_primal = iterate.PrimalSolution().Height();
  const ConstBlasMatrixView<Real>& affine_primal_solutions =
      affine_direction.PrimalSolution();
  const ConstBlasMatrixView<Real>& affine_dual_slacks =
      affine_direction.DualSlack();

  residuals->complementarity.residual.Resize(num_primal, 1, Real(0));
  for (Int i = 0; i < num_primal; ++i) {
    // See Eq. (6) in Colombo and Gondzio's "Further Development of Multiple
    // Centrality Correctors for Interior Point Methods."
    residuals->complementarity.residual(i) =
        corrector_barrier - predictor_barrier;
    if (second_order) {
      residuals->complementarity.residual(i) -=
          affine_primal_solutions(i) * affine_dual_slacks(i);
    }
  }
  residuals->complementarity.CompleteFromResidual(Real(1));

  residuals->scale_complement_residual = corrector_barrier - predictor_barrier;
  if (second_order) {
    residuals->scale_complement_residual -=
        affine_direction.Tau() * affine_direction.Kappa();
  }

  const bool status = first_order_optimality->Solve(
      factor_regularization, problem, iterate, diagonal_scale, *residuals,
      reg_state, refined_solve_control, fgmres_control,
      demanded_relative_residual, control.output.verbose,
      control.output.analyze_residuals,
      control.output.analyze_iterate_residuals, update);

  return status;
}

template <typename Real>
bool GondzioCorrectorDirection(
    const Problem<Real>& problem, const SolveControl<Real>& control,
    const RefinedSolveControl<Real>& refined_solve_control,
    const catamari::FGMRESControl<Real>& fgmres_control,
    const Iterate<Real>& iterate,
    const ConstBlasMatrixView<Real>& diagonal_scale,
    const RegularizationState<Real>& reg_state, bool factor_regularization,
    const Iterate<Real>& predictor, Real primal_predictor_step,
    Real dual_predictor_step, Real demanded_relative_residual,
    Residuals<Real>* residuals,
    FirstOrderOptimality<Real>* first_order_optimality, Iterate<Real>* update) {
  const ConstBlasMatrixView<Real> iter_primal_solution =
      iterate.PrimalSolution();
  const ConstBlasMatrixView<Real> iter_dual_slack = iterate.DualSlack();
  const ConstBlasMatrixView<Real> pred_primal_solution =
      predictor.PrimalSolution();
  const ConstBlasMatrixView<Real> pred_dual_slack = predictor.DualSlack();
  const Int num_primal = iter_primal_solution.Height();
  if (control.output.verbose) {
    std::cout << "Computing a Gondzio corrector." << std::endl;
  }

  // If the tau kappa complement should undergo Gondzio higher-order correction.
  const bool scalar_gondzio = true;

  const Real primal_step =
      std::min(primal_predictor_step * control.correctors.gondzio_step_ratio +
                   control.correctors.gondzio_step_shift,
               Real(1));
  const Real dual_step =
      std::min(dual_predictor_step * control.correctors.gondzio_step_ratio +
                   control.correctors.gondzio_step_shift,
               Real(1));

  const Real average_complement = AverageComplement(iterate);
  const Real updated_average_complement =
      UpdatedAverageComplement(iterate, primal_step, dual_step, predictor);

  const Real upper_target =
      control.correctors.gondzio_upper_ratio * updated_average_complement;
  const Real lower_target =
      control.correctors.gondzio_lower_ratio * updated_average_complement;

  Real min_complement = average_complement;
  Real max_complement = 0;

  residuals->complementarity.residual.Resize(num_primal, 1, Real(0));
  for (Int i = 0; i < num_primal; ++i) {
    const Real primal_solution = iter_primal_solution(i);
    const Real dual_slack = iter_dual_slack(i);
    const Real updated_primal_solution =
        primal_solution + primal_step * pred_primal_solution(i);
    const Real updated_dual_slack = dual_slack + dual_step * pred_dual_slack(i);
    const Real complement = primal_solution * dual_slack;
    const Real updated_complement =
        updated_primal_solution * updated_dual_slack;
    min_complement = std::min(min_complement, complement);
    max_complement = std::max(max_complement, complement);

    Real target;
    if (updated_complement > upper_target) {
      target =
          control.correctors.gondzio_upper_decrease_ratio * updated_complement;
    } else if (updated_complement < lower_target) {
      // One could argue for choosing the lower target rather than the average,
      // but this is an aspirational increase unlikely to be fully met. For
      // that reason, we attempt to overshoot to the average.
      target = updated_average_complement;
    } else {
      // TODO(Jack Poulson): Make use of the fact that we do not need high
      // accuracy in this component, as the complement merely needs to stay
      // within the box.
      target = updated_complement;
    }

    residuals->complementarity.residual(i) = target - updated_complement;
  }
  residuals->complementarity.CompleteFromResidual(Real(1));

  if (scalar_gondzio) {
    const Real complement = iterate.Tau() * iterate.Kappa();
    const Real updated_complement =
        (iterate.Tau() + primal_step * predictor.Tau()) *
        (iterate.Kappa() + dual_step * predictor.Kappa());

    min_complement = std::min(min_complement, complement);
    max_complement = std::max(max_complement, complement);

    Real target;
    if (updated_complement > upper_target) {
      target =
          control.correctors.gondzio_upper_decrease_ratio * updated_complement;
    } else if (updated_complement < lower_target) {
      target = updated_average_complement;
    } else {
      target = updated_complement;
    }

    residuals->scale_complement_residual = target - updated_complement;
  } else {
    residuals->scale_complement_residual = Real(0);
  }

  if (control.output.verbose) {
    std::cout << "min_complement: " << static_cast<double>(min_complement)
              << ", max_complement: " << static_cast<double>(max_complement)
              << ", complement_ratio: "
              << static_cast<double>(max_complement / min_complement)
              << std::endl;
  }

  const bool status = first_order_optimality->Solve(
      factor_regularization, problem, iterate, diagonal_scale, *residuals,
      reg_state, refined_solve_control, fgmres_control,
      demanded_relative_residual, control.output.verbose,
      control.output.analyze_residuals,
      control.output.analyze_iterate_residuals, update);

  return status;
}

// Compute the residual z_prop := Q x + c tau - A' y and attempt to improve the
// accuracy by pushing entries of z into a two-sided infinity neighborhood of
//     x o z = mu e
// while improving accuracy.
template <typename Real>
void ProjectDualSlack(const Problem<Real>& problem,
                      const DualSlackProjectionControl<Real>& control,
                      Iterate<Real>* iterate) {
  if (!control.enabled) {
    return;
  }
  const Int num_primal = iterate->PrimalSolution().Height();
  const Real average_complement = AverageComplement(*iterate);
  BlasMatrixView<Real> iter_primal_solution = iterate->PrimalSolution();
  BlasMatrixView<Real> iter_dual_slack = iterate->DualSlack();

  BlasMatrix<Real> z_prop = problem.linear_objective;

  catamari::ApplyAdjointSparse(Real(-1), problem.equality_matrix,
                               iterate->DualSolution().ToConst(),
                               iterate->Tau(), &z_prop.view);

  if (!problem.quadratic_objective.Entries().Empty()) {
    catamari::ApplyAdjointSparse(Real(1), problem.quadratic_objective,
                                 iterate->PrimalSolution().ToConst(), Real(1),
                                 &z_prop.view);
  }

  const Real complement_lower = control.lower_ratio_limit * average_complement;
  const Real complement_upper = control.upper_ratio_limit * average_complement;
  for (Int i = 0; i < num_primal; ++i) {
    const Real primal_solution = iter_primal_solution(i);
    const Real dual_slack = iter_dual_slack(i);
    const Real complement = primal_solution * dual_slack;

    const Real classical_ratio = complement / average_complement;

    const Real prop_ratio = z_prop(i) / dual_slack;
    if (prop_ratio > Real(1)) {
      if (complement >= complement_upper) {
        if (control.verbose) {
          std::cout << "  could not increase since classical_ratio="
                    << classical_ratio << std::endl;
        }
      } else {
        const Real max_ratio = complement_upper / complement;
        const Real ratio = std::min(prop_ratio, max_ratio);
        if (control.verbose) {
          std::cout << "  increasing by ratio " << ratio << std::endl;
        }
        iter_dual_slack(i) = ratio * dual_slack;
      }
    } else if (prop_ratio < Real(1)) {
      if (complement <= complement_lower) {
        if (control.verbose) {
          std::cout << "  could not decrease since classical_ratio="
                    << classical_ratio << std::endl;
        }
      } else {
        const Real min_ratio = complement_lower / complement;
        const Real ratio = std::max(prop_ratio, min_ratio);
        if (control.verbose) {
          std::cout << "  decreasing by ratio " << ratio << std::endl;
        }
        iter_dual_slack(i) = ratio * dual_slack;
      }
    }
  }
}

template <typename Real>
void ComputeScaling(const Iterate<Real>& iterate,
                    const Residuals<Real>& residuals, ScalingStrategy strategy,
                    Real max_rescale, bool verbose,
                    BlasMatrix<Real>* diagonal_scale) {
  const Int num_primal = iterate.PrimalSolution().Height();
  const Int num_dual = iterate.DualSolution().Height();

  if (strategy == SCALE_UP_COMPLEMENTS) {
    const Real complement_scale = residuals.complementarity.residual_two_norm;
    const Real max_norm = std::max({
        residuals.dual_equality.residual_two_norm,
        residuals.primal_equality.residual_two_norm,
        std::abs(residuals.hsd_gap),
        complement_scale,
    });
    if (verbose) {
      std::cout << "dual_equality.residual_two_norm: "
                << residuals.dual_equality.residual_two_norm << "\n"
                << "primal_equality.residual_two_norm: "
                << residuals.primal_equality.residual_two_norm << "\n"
                << "hsd_gap: " << residuals.hsd_gap << "\n"
                << "complement_scale: " << complement_scale << std::endl;
    }

    auto clip_rescale = [&](Real scale) {
      return std::min(max_rescale,
                      std::max(Real(1) / max_rescale,
                               std::sqrt(max_norm / std::abs(scale))));
    };

    const Int tau_offset = num_primal + num_dual;
    const Int dual_slack_offset = num_primal + num_dual + 1;
    const Int kappa_offset = 2 * num_primal + num_dual + 1;
    diagonal_scale->Entry(tau_offset) = clip_rescale(residuals.hsd_gap);
    for (Int i = 0; i < num_primal; ++i) {
      diagonal_scale->Entry(dual_slack_offset + i) =
          clip_rescale(complement_scale);
    }
    diagonal_scale->Entry(kappa_offset) = clip_rescale(complement_scale);
  } else if (strategy == SCALE_BY_SLACKS) {
    const Int tau_offset = num_primal + num_dual;
    const ConstBlasMatrixView<Real>& primal_solution = iterate.PrimalSolution();
    for (Int i = 0; i < num_primal; ++i) {
      diagonal_scale->Entry(i) = std::min(
          max_rescale, std::max(Real(1) / max_rescale, primal_solution(i)));
    }
    diagonal_scale->Entry(tau_offset) =
        std::min(max_rescale, std::max(Real(1) / max_rescale, iterate.Tau()));
  } else if (strategy == SCALE_BY_AFFINE) {
    const Real complement_scale = residuals.complementarity.residual_two_norm;
    const Real max_norm = std::max({
        residuals.dual_equality.residual_two_norm,
        residuals.primal_equality.residual_two_norm,
        std::abs(residuals.hsd_gap),
        std::abs(residuals.scale_complement_residual),
        complement_scale,
    });
    if (verbose) {
      std::cout << "dual_equality.residual_two_norm: "
                << residuals.dual_equality.residual_two_norm << "\n"
                << "primal_equality.residual_two_norm: "
                << residuals.primal_equality.residual_two_norm << "\n"
                << "hsd_gap: " << residuals.hsd_gap << "\n"
                << "complement_scale: " << complement_scale << "\n"
                << "scale_complement_residual: "
                << residuals.scale_complement_residual << "\n"
                << "max_norm: " << max_norm << std::endl;
    }

    auto clip_rescale = [&](Real scale) {
      return std::min(max_rescale,
                      std::max(Real(1) / max_rescale,
                               std::sqrt(max_norm / std::abs(scale))));
    };

    const Int tau_offset = num_primal + num_dual;
    const Int dual_slack_offset = num_primal + num_dual + 1;
    const Int kappa_offset = 2 * num_primal + num_dual + 1;
    for (Int i = 0; i < num_primal; ++i) {
      diagonal_scale->Entry(i) =
          clip_rescale(residuals.dual_equality.residual_two_norm);
    }
    for (Int i = 0; i < num_dual; ++i) {
      diagonal_scale->Entry(num_primal + i) =
          clip_rescale(residuals.primal_equality.residual_two_norm);
    }
    diagonal_scale->Entry(tau_offset) = clip_rescale(residuals.hsd_gap);
    for (Int i = 0; i < num_primal; ++i) {
      diagonal_scale->Entry(dual_slack_offset + i) =
          clip_rescale(complement_scale);
    }
    diagonal_scale->Entry(kappa_offset) =
        clip_rescale(residuals.scale_complement_residual);
  }
}

template <typename Real>
void PostprocessInfeasibleIterate(
    const Problem<Real>& original_problem,
    const Problem<Real>& rescaled_problem, const Scaling<Real>& scaling,
    const Iterate<Real>& iterate, const SolveControl<Real>& control,
    const RefinedSolveControl<Real>& refined_solve_control,
    const Buffer<bool>& basis_indicator, Iterate<Real>* certificate) {
  static constexpr Real kEpsilon = std::numeric_limits<Real>::epsilon();
  const Real relative_tolerance =
      control.optimality.relative_tolerance_coefficient *
      std::pow(kEpsilon, control.optimality.relative_tolerance_exponent);

  Iterate<Real> projected_iterate;
  const bool projected = InfeasibleProjection(
      rescaled_problem, relative_tolerance, control.infeasible_projection,
      control.linear_solve.ldl, refined_solve_control, iterate, basis_indicator,
      control.output.verbose, &projected_iterate);
  if (projected) {
    *certificate = projected_iterate;
  }

  RescaleIterate(scaling, certificate);

  Residuals<Real> residuals;
  residuals.iterate_normalization = control.optimality.iterate_normalization;
  residuals.FillRelativeError(original_problem, *certificate);

  if (residuals.infeasibility_relative_error <= relative_tolerance) {
    if (residuals.objective.primal < Real(0) &&
        residuals.max_dual_infeasibility < relative_tolerance) {
      certificate->certificate_type = DUAL_INFEASIBILITY_CERTIFICATE;
      if (control.output.verbose) {
        std::cout << "Dual infeasibility certificate with primal: "
                  << residuals.objective.primal
                  << " and max(|| A x ||, || Q x ||) / |c' x|: "
                  << residuals.max_dual_infeasibility << std::endl;
      }
    } else if (rescaled_problem.equality_matrix_max_norm > Real(0) &&
               residuals.objective.dual > Real(0) &&
               residuals.primal_infeasibility.relative_residual_norm <
                   relative_tolerance) {
      certificate->certificate_type = PRIMAL_INFEASIBILITY_CERTIFICATE;
      if (control.output.verbose) {
        std::cout << "Primal infeasibility certificate with dual: "
                  << residuals.objective.dual << " and || A' y || / |b' y|: "
                  << residuals.primal_infeasibility.relative_residual_norm
                  << std::endl;
      }
    }
  }
}

template <typename Real>
void PostprocessApproximateSolution(
    const Problem<Real>& original_problem,
    const Problem<Real>& rescaled_problem, const Scaling<Real>& scaling,
    const Iterate<Real>& iterate, const SolveControl<Real>& control,
    const RefinedSolveControl<Real>& refined_solve_control,
    const Buffer<bool>& basis_indicator, Real best_relative_error,
    Iterate<Real>* certificate) {
  static constexpr Real kEpsilon = std::numeric_limits<Real>::epsilon();
  const Real relative_tolerance =
      control.optimality.relative_tolerance_coefficient *
      std::pow(kEpsilon, control.optimality.relative_tolerance_exponent);

  if (iterate.certificate_type == INVALID_CERTIFICATE) {
    std::cerr << "WARNING: Did not achieve desired relative error."
              << std::endl;
    *certificate = iterate;
  }

  Iterate<Real> projected_iterate;
  const bool projected = FeasibleProjection(
      rescaled_problem, relative_tolerance, control.feasible_projection,
      control.linear_solve.ldl, refined_solve_control, iterate, basis_indicator,
      control.output.verbose, &projected_iterate);
  if (projected) {
    Iterate<Real> unscaled_iterate = projected_iterate;
    RescaleIterate(scaling, &unscaled_iterate);

    Residuals<Real> residuals;
    residuals.iterate_normalization = control.optimality.iterate_normalization;
    residuals.FillRelativeError(original_problem, unscaled_iterate);

    if (iterate.certificate_type == OPTIMALITY_CERTIFICATE) {
      if (residuals.relative_error < best_relative_error) {
        if (control.output.verbose) {
          std::cout << "Accepted feasible projection with relative error "
                    << residuals.relative_error << std::endl;
        }
        projected_iterate.certificate_type = OPTIMALITY_CERTIFICATE;
        *certificate = projected_iterate;
      } else {
        if (control.output.verbose) {
          std::cout << "Rejecting feasible projection since its relative "
                    << "error is " << residuals.relative_error
                    << " vs. the existing value of " << best_relative_error
                    << std::endl;
        }
      }
    } else if (residuals.relative_error < best_relative_error) {
      if (residuals.relative_error < relative_tolerance) {
        projected_iterate.certificate_type = OPTIMALITY_CERTIFICATE;
      }
      *certificate = projected_iterate;
    }
  } else {
    auto projection_control = control.dual_slack_projection;
    projection_control.lower_ratio_limit = Real(0);
    projection_control.upper_ratio_limit = 1e50;

    projected_iterate = *certificate;
    ProjectDualSlack(rescaled_problem, projection_control, &projected_iterate);

    if (iterate.certificate_type == OPTIMALITY_CERTIFICATE) {
      Iterate<Real> unscaled_iterate = projected_iterate;
      RescaleIterate(scaling, &unscaled_iterate);

      Residuals<Real> residuals;
      residuals.iterate_normalization =
          control.optimality.iterate_normalization;
      residuals.FillRelativeError(original_problem, unscaled_iterate);

      if (residuals.relative_error < best_relative_error) {
        *certificate = projected_iterate;
      }
    } else {
      *certificate = projected_iterate;
    }
  }

  RescaleIterate(scaling, certificate);
}

// Check for convergence by measuring progress -- or lack thereof -- in the
// original scaling.
template <typename Real>
bool CheckConvergence(Int num_iter, const Problem<Real>& problem,
                      const Residuals<Real>& residuals,
                      const SolveControl<Real>& control,
                      Real relative_tolerance, Real initial_average_complement,
                      Real initial_tau_over_kappa, Real max_rescaled_norm,
                      Iterate<Real>* iterate, Iterate<Real>* certificate,
                      Real* best_relative_error) {
  if (iterate->certificate_type != OPTIMALITY_CERTIFICATE &&
      residuals.relative_error <= relative_tolerance) {
    iterate->certificate_type = OPTIMALITY_CERTIFICATE;
    *best_relative_error = residuals.relative_error;
    *certificate = *iterate;
    if (control.output.verbose) {
      std::cout << "Met target after " << num_iter << " iter's since "
                << static_cast<double>(residuals.relative_error)
                << " <= " << relative_tolerance << std::endl;
    }
  } else if (iterate->certificate_type == OPTIMALITY_CERTIFICATE) {
    if (residuals.relative_error <
        control.optimality.relative_error_decrease_ratio *
            *best_relative_error) {
      // We significantly decreased the relative error.
      *certificate = *iterate;
      *best_relative_error = residuals.relative_error;
    } else if (residuals.relative_error < *best_relative_error) {
      // We decreased the relative error, but not significantly enough.
      *certificate = *iterate;
      *best_relative_error = residuals.relative_error;
      return true;
    } else {
      // Stop, as we didn't even decrease the relative error.
      return true;
    }
  } else if (residuals.infeasibility_relative_error <= relative_tolerance &&
             residuals.objective.primal < Real(0) &&
             residuals.max_dual_infeasibility < relative_tolerance) {
    iterate->certificate_type = DUAL_INFEASIBILITY_CERTIFICATE;
    *certificate = *iterate;
    if (control.output.verbose) {
      std::cout << "Dual infeasibility certificate with primal: "
                << residuals.objective.primal
                << " and max(|| A x ||, || Q x ||) / |c' x|: "
                << residuals.max_dual_infeasibility << std::endl;
    }
    return true;
  } else if (residuals.infeasibility_relative_error <= relative_tolerance &&
             problem.equality_matrix_max_norm > Real(0) &&
             residuals.objective.dual > Real(0) &&
             residuals.primal_infeasibility.relative_residual_norm <
                 relative_tolerance) {
    iterate->certificate_type = PRIMAL_INFEASIBILITY_CERTIFICATE;
    *certificate = *iterate;
    if (control.output.verbose) {
      std::cout << "Primal infeasibility certificate with dual: "
                << residuals.objective.dual << " and || A' y || / |b' y|: "
                << residuals.primal_infeasibility.relative_residual_norm
                << std::endl;
    }
    return true;
  } else if (residuals.infeasibility_relative_error <= relative_tolerance) {
    // Check if the infeasibility criteria of Xu, Hung, and Ye is satisfied.
    // We generalize their recommendation:
    //   mu / mu^0 < 10^{-14},
    //   (tau / kappa) / (tau^0 / kappa^0) < 10^{-8}.
    // We do so by treating the target of 10^{-8} as the relative tolerance
    // and the extra factor of 10^{-6} as a constant rather than dependent on
    // the machine epsilon.
    const Real average_complement = AverageComplement(*iterate);
    const Real tau_over_kappa = iterate->Tau() / iterate->Kappa();

    if (average_complement / initial_average_complement <
            control.infeasibility.complement_tightening * relative_tolerance &&
        max_rescaled_norm * tau_over_kappa / initial_tau_over_kappa <
            control.infeasibility.tau_over_kappa_tightening *
                relative_tolerance) {
      // We are confident that this is an (approximately) infeasibile problem.
      // We have found an infeasibility certificate.
      iterate->certificate_type = INFEASIBILITY_CERTIFICATE;
      *certificate = *iterate;
      if (control.output.verbose) {
        std::cout << "Abiguous infeasibility status with "
                  << "tau / kappa = " << iterate->Tau() << " / "
                  << iterate->Kappa() << " = " << tau_over_kappa << std::endl;
      }
      return true;
    } else {
      *best_relative_error = residuals.relative_error;
    }
  } else {
    *best_relative_error = residuals.relative_error;
  }

  return false;
}

// An enum for representing the status of the attempt at computing the affine
// update direction.
enum AffineDirectionStatus {
  AFFINE_FACTORIZATION_FAILED,
  AFFINE_SOLVE_FAILED,
  AFFINE_SOLVE_SUCCEEDED,
};

// Compute the affine search direction. Like HOPDM, rather than using a
// 'pure' affine search direction, we use a small target barrier value.
template <typename Real>
AffineDirectionStatus ComputeAffineDirection(
    Int num_iter, const Problem<Real>& problem, const Iterate<Real>& iterate,
    Real relative_residual_norm_decrease_ratio, Real tau_decrease_ratio,
    const SolveControl<Real>& control,
    const RefinedSolveControl<Real>& refined_solve_control,
    const catamari::FGMRESControl<Real>& fgmres_control,
    const RegularizationState<Real>& reg_state, Real relative_tolerance,
    Real demanded_relative_residual, BlasMatrix<Real>* diagonal_scale,
    FirstOrderOptimality<Real>* first_order_optimality, Real* affine_barrier,
    bool* use_affine_direction, Iterate<Real>* predictor,
    Residuals<Real>* residuals, bool* factor_regularization) {
  const ConstBlasMatrixView<Real> primal_solution = iterate.PrimalSolution();
  const ConstBlasMatrixView<Real> dual_solution = iterate.DualSolution();
  const Int num_primal = primal_solution.Height();

  static constexpr Real kEpsilon = std::numeric_limits<Real>::epsilon();
  const Real max_rescale =
      std::pow(kEpsilon, control.scaling.max_rescale_exponent);

  const Real primal_infeasibility_gap_bound =
      residuals->primal_equality.residual_two_norm *
      EuclideanNorm(dual_solution);
  const Real dual_infeasibility_gap_bound =
      residuals->dual_equality.residual_two_norm *
      EuclideanNorm(primal_solution);
  const Real average_infeasibility_gap_bound =
      (primal_infeasibility_gap_bound + dual_infeasibility_gap_bound) /
      num_primal;

  // We only guard the lower bound of the complements if tau is not decreasing
  // sufficiently fast.
  Real proposed_affine_target_lower_bound = 0;
  const Real kTauDecreaseRatioCutoff = Real(0.5);
  const Real kRelativeResidualNormDecreaseRatioCutoff = Real(0.75);
  if (tau_decrease_ratio > kTauDecreaseRatioCutoff &&
      relative_residual_norm_decrease_ratio >
          kRelativeResidualNormDecreaseRatioCutoff) {
    proposed_affine_target_lower_bound = std::min(
        control.barrier.safe_affine,
        control.barrier.average_infeasibility_upper_bound_affine_allowance *
            average_infeasibility_gap_bound);
  }

  // Initialize the target affine barrier to the lowest allowed value.
  const Real average_complement = AverageComplement(iterate);
  *affine_barrier =
      control.barrier.min_affine_to_average_ratio * average_complement;

  // Propose a candidate target affine barrier by taking the maximum between
  // the current target value and a lower bound based upon the average
  // infeasibility gap upper bound.
  *affine_barrier =
      std::max(*affine_barrier, proposed_affine_target_lower_bound);

  // Ensure that the target affine barrier does not exceed its maximum allowed
  // ratio to the current average complement.
  *affine_barrier =
      std::min(*affine_barrier, control.barrier.max_affine_to_average_ratio *
                                    average_complement);

  // TODO(Jack Poulson): Decide if there is any scenario where we should use
  // the affine direction by itself.
  *use_affine_direction = false;

  if (control.output.verbose) {
    std::cout << "average_complement:    "
              << static_cast<double>(average_complement) << "\n"
              << "affine_barrier: " << static_cast<double>(*affine_barrier)
              << "\n"
              << "proposed_affine_target_lower_bound: "
              << proposed_affine_target_lower_bound << "\n"
              << "|| c tau + Q x - A' y - z || || x ||: "
              << dual_infeasibility_gap_bound << "\n"
              << "|| b tau - A x            || || y ||: "
              << primal_infeasibility_gap_bound << "\n"
              << "last tau decrease ratio: " << tau_decrease_ratio << std::endl;
  }

  FormAffineComplementarity(control, iterate, *affine_barrier, residuals);

  ComputeScaling(iterate, *residuals, control.scaling.strategy, max_rescale,
                 control.output.verbose, diagonal_scale);

  if (num_iter == 0) {
    first_order_optimality->InitializeMatrix(
        problem, iterate, diagonal_scale->ConstView(), reg_state,
        !control.linear_solve.use_augmented);
  } else {
    first_order_optimality->ModifyMatrix(
        problem, iterate, diagonal_scale->ConstView(), reg_state);
  }
  const bool factored_matrix = first_order_optimality->FactorMatrix(
      problem, iterate, diagonal_scale->ConstView(), control.linear_solve.ldl,
      refined_solve_control, fgmres_control, demanded_relative_residual,
      control.output.verbose);
  if (!factored_matrix) {
    return AFFINE_FACTORIZATION_FAILED;
  }

  *factor_regularization = false;
  const bool computed_affine = first_order_optimality->Solve(
      *factor_regularization, problem, iterate, diagonal_scale->ConstView(),
      *residuals, reg_state, refined_solve_control, fgmres_control,
      demanded_relative_residual, control.output.verbose,
      control.output.analyze_residuals,
      control.output.analyze_iterate_residuals, predictor);
  if (computed_affine) {
    return AFFINE_SOLVE_SUCCEEDED;
  } else {
    return AFFINE_SOLVE_FAILED;
  }
}

template <typename Real>
bool ComputeCorrectedDirection(
    const Problem<Real>& problem, const BlasMatrix<Real>& diagonal_scale,
    const RegularizationState<Real>& reg_state,
    const SolveControl<Real>& control,
    const RefinedSolveControl<Real>& refined_solve_control,
    const catamari::FGMRESControl<Real>& fgmres_control,
    const Iterate<Real>& iterate,
    const std::pair<Real, Real>& affine_step_sizes, Real affine_barrier,
    Real mehrotra_barrier, Real centering, bool proportional_decrease,
    bool factor_regularization, Real demanded_relative_residual,
    FirstOrderOptimality<Real>* first_order_optimality,
    Iterate<Real>* predictor, Iterate<Real>* corrector,
    Residuals<Real>* residuals, std::pair<Real, Real>* combined_step_sizes) {
  const Int num_primal = problem.equality_matrix.NumColumns();
  const Int num_dual = problem.equality_matrix.NumRows();
  if (proportional_decrease) {
    // We rescale down the primal and dual equality residuals, and the HSD gap,
    // using the negation of the centering parameter, 'gamma' from [3]. The
    // result is that, when combined with the affine step's residual, we will
    // have a step which attempts to reduce the residuals down to 1 - gamma of
    // their original values.
    for (Int i = 0; i < num_primal; ++i) {
      residuals->dual_equality.residual(i) *= -centering;
    }
    for (Int i = 0; i < num_dual; ++i) {
      residuals->primal_equality.residual(i) *= -centering;
    }
    residuals->hsd_gap *= -centering;
  } else {
    for (Int i = 0; i < num_primal; ++i) {
      residuals->dual_equality.residual(i) = 0;
    }
    for (Int i = 0; i < num_dual; ++i) {
      residuals->primal_equality.residual(i) = 0;
    }
    residuals->hsd_gap = 0;
  }

  // Solve for the corrector direction.
  const bool computed_second_order_mehrotra = MehrotraCorrectorDirection(
      problem, control, refined_solve_control, fgmres_control, iterate,
      diagonal_scale.ConstView(), reg_state, factor_regularization, *predictor,
      demanded_relative_residual, affine_barrier, mehrotra_barrier,
      true /* second_order */, residuals, first_order_optimality, corrector);
  if (!computed_second_order_mehrotra) {
    if (control.output.verbose) {
      std::cout << "Failed to accurately compute second-order Mehrotra "
                << "corrector." << std::endl;
    }

    // Try to compute a first-order Mehrotra corrector.
    const bool computed_first_order_mehrotra = MehrotraCorrectorDirection(
        problem, control, refined_solve_control, fgmres_control, iterate,
        diagonal_scale.ConstView(), reg_state, factor_regularization,
        *predictor, demanded_relative_residual, affine_barrier,
        mehrotra_barrier, false /* second_order */, residuals,
        first_order_optimality, corrector);
    if (!computed_first_order_mehrotra) {
      if (control.output.verbose) {
        std::cout << "Failed to accurately compute first-order Mehrotra "
                  << "corrector." << std::endl;
      }
      return false;
    }
  }

  // Fall back to the affine (predictor) direction if the step sizes for the
  // combined direction would be sufficiently smaller than for the affine
  // direction.
  Iterate<Real> combined_update = *predictor;
  UpdateIterate(Real(1), *corrector, &combined_update);
  const Real affine_total_step_size =
      affine_step_sizes.first + affine_step_sizes.second;
  const std::pair<Real, Real> mehrotra_step_sizes =
      PositiveOrthantPrimalDualStepSizes(iterate, combined_update,
                                         control.combined_step);
  const Real mehrotra_total_step_size =
      mehrotra_step_sizes.first + mehrotra_step_sizes.second;
  if (mehrotra_total_step_size >=
      control.correctors.mehrotra_accept_ratio * affine_total_step_size) {
    *predictor = combined_update;
    *combined_step_sizes = mehrotra_step_sizes;
    if (control.output.verbose) {
      PrintUpdateResiduals(problem, control.optimality.iterate_normalization,
                           reg_state, iterate, *predictor, std::cout);
    }
  }

  // We can now drop the primal equality, dual equality, and HSD gap residual
  // targets down to zero since we are already targeting their reduction down
  // to (1 - centering) of their original values.
  for (Int i = 0; i < num_primal; ++i) {
    residuals->dual_equality.residual(i) = 0;
  }
  for (Int i = 0; i < num_dual; ++i) {
    residuals->primal_equality.residual(i) = 0;
  }
  residuals->hsd_gap = 0;

  Int max_gondzio_correctors = control.correctors.max_gondzio_correctors;
  for (Int gondzio = 0; gondzio < max_gondzio_correctors; ++gondzio) {
    const bool computed_gondzio = GondzioCorrectorDirection(
        problem, control, refined_solve_control, fgmres_control, iterate,
        diagonal_scale.ConstView(), reg_state, factor_regularization,
        *predictor, combined_step_sizes->first, combined_step_sizes->second,
        demanded_relative_residual, residuals, first_order_optimality,
        corrector);
    if (!computed_gondzio) {
      if (control.output.verbose) {
        std::cout << "Could not compute Gondzio corrector " << gondzio
                  << std::endl;
      }
      break;
    }

    combined_update = *predictor;
    UpdateIterate(Real(1), *corrector, &combined_update);
    const std::pair<Real, Real> gondzio_step_sizes =
        PositiveOrthantPrimalDualStepSizes(iterate, combined_update,
                                           control.combined_step);
    const Real old_total_step_size =
        combined_step_sizes->first + combined_step_sizes->second;
    const Real new_total_step_size =
        gondzio_step_sizes.first + gondzio_step_sizes.second;

    if (new_total_step_size >=
        control.correctors.gondzio_min_accept_ratio * old_total_step_size) {
      *predictor = combined_update;
      *combined_step_sizes = gondzio_step_sizes;
      if (control.output.verbose) {
        std::cout << "Accepted Gondzio corrector " << gondzio
                  << " since the combined step size was "
                  << static_cast<double>(new_total_step_size)
                  << " vs. old size of "
                  << static_cast<double>(old_total_step_size) << std::endl;
        PrintUpdateResiduals(problem, control.optimality.iterate_normalization,
                             reg_state, iterate, *predictor, std::cout);
      }
    } else {
      if (control.output.verbose) {
        std::cout << "Rejected Gondzio corrector " << gondzio
                  << " since the combined step size was "
                  << static_cast<double>(new_total_step_size)
                  << " vs. old size of "
                  << static_cast<double>(old_total_step_size) << std::endl;
      }
      break;
    }

    if (new_total_step_size <
        control.correctors.gondzio_min_continue_ratio * old_total_step_size) {
      if (control.output.verbose) {
        std::cout << "Stopped correcting after correction " << gondzio
                  << " since the combined step size was "
                  << static_cast<double>(new_total_step_size)
                  << " rather than old size of "
                  << static_cast<double>(old_total_step_size) << std::endl;
      }
      break;
    } else if (new_total_step_size >=
               control.correctors.gondzio_demand_continue_ratio *
                   old_total_step_size) {
      if (control.output.verbose) {
        std::cout << "Demanding at least one more Gondzio correction."
                  << std::endl;
      }
      max_gondzio_correctors = std::max(max_gondzio_correctors, gondzio + 2);
    }
  }

  return true;
}

template <typename Real>
Int Solve(const Problem<Real>& original_problem,
          const SolveControl<Real>& control, Iterate<Real>* certificate) {
  const Int num_primal = original_problem.equality_matrix.NumColumns();
  const Int num_dual = original_problem.equality_matrix.NumRows();
  static constexpr Real kEpsilon = std::numeric_limits<Real>::epsilon();

  SolveControl<Real> control_mod = control;
  control_mod.linear_solve.ConfigureDynamicRegularization(num_primal, num_dual);

  RegularizationState<Real> reg_state;
  InitializeRegularizationState(control_mod.proximal_regularization,
                                &reg_state);

  const Real relative_tolerance =
      control_mod.optimality.relative_tolerance_coefficient *
      std::pow(kEpsilon, control_mod.optimality.relative_tolerance_exponent);

  const Real demanded_relative_residual =
      control_mod.linear_solve.demanded_relative_residual;

  Scaling<Real> scaling;
  Problem<Real> rescaled_problem = original_problem;

  if (control_mod.scaling.geometric_equilibration) {
    GeometricEquilibration(&rescaled_problem, &scaling,
                           control_mod.scaling.num_equilibration_passes,
                           control_mod.output.verbose);
  } else {
    RuizEquilibration(&rescaled_problem, &scaling,
                      control_mod.scaling.num_equilibration_passes,
                      control_mod.output.verbose);
  }
  rescaled_problem.FillNorms();
  if (control_mod.output.verbose) {
    std::cout << "Original norms:" << std::endl;
    original_problem.PrintNorms(std::cout);
    std::cout << "Rescaled norms:" << std::endl;
    rescaled_problem.PrintNorms(std::cout);
  }
  const Real max_rescaled_norm = std::max({
      rescaled_problem.linear_objective_two_norm,
      rescaled_problem.equality_rhs_two_norm,
      Real(1),
  });

  // Use the canonical initialization.
  Iterate<Real> iterate;
  iterate.stacked_vectors.Resize(num_primal, num_dual);
  for (Int i = 0; i < num_primal; ++i) {
    iterate.PrimalSolution()(i) = Real(1);
    iterate.DualSlack()(i) = Real(1);
  }
  for (Int i = 0; i < num_dual; ++i) {
    iterate.DualSolution()(i) = Real(0);
  }
  iterate.Tau() = Real(1);
  iterate.Kappa() = Real(1);
  const Real initial_tau_over_kappa = Real(1);
  const Real initial_average_complement = Real(1);

  // Compute the initial primal and dual equality residual norms.
  Residuals<Real> residuals;
  residuals.iterate_normalization =
      control_mod.optimality.iterate_normalization;
  residuals.FillRelativeError(rescaled_problem, iterate);

  FirstOrderOptimality<Real> first_order_optimality;

  RefinedSolveControl<Real> inner_refined_solve_control;
  inner_refined_solve_control.verbose = control_mod.output.verbose;
  inner_refined_solve_control.max_iters =
      control.linear_solve.max_iterative_refinement_iterations;

  RefinedSolveControl<Real> refined_solve_control;
  refined_solve_control.verbose = control_mod.output.verbose;
  refined_solve_control.max_iters =
      control.linear_solve.max_iterative_refinement_iterations;

  catamari::FGMRESControl<Real> fgmres_control;
  fgmres_control.verbose = control_mod.output.verbose;
  fgmres_control.max_iterations =
      control_mod.linear_solve.max_fgmres_iterations;
  fgmres_control.relative_tolerance_coefficient =
      control_mod.linear_solve.desired_relative_residual;
  fgmres_control.relative_tolerance_exponent = 0;

  // For storing a candidate update direction or right-hand side.
  Iterate<Real> predictor;
  predictor.stacked_vectors.Resize(num_primal, num_dual);

  // For storing corrector right-hand sides or update directions.
  Iterate<Real> corrector;
  corrector.stacked_vectors.Resize(num_primal, num_dual);

  // We will rescale the first-order optimality conditions as
  //   (D J D) (inv(D) x) = (inv(D) b),
  // primarily to encourage that the complementarity residuals are not washed
  // out in the (approximate) linear solves.
  BlasMatrix<Real> diagonal_scale(2 * num_primal + num_dual + 2, 1);
  for (Int i = 0; i < diagonal_scale.Height(); ++i) {
    diagonal_scale(i) = Real(1);
  }

  // We do not follow the traditional analysis, e.g., of
  //
  //   E. D. Andersen, C. Roos, and T. Terlaky,
  //   "On implementing a primal-dual interior-point method for conic
  //   quadratic optimization", Dec. 18, 2000.
  //   URL: http://www.optimization-online.org/DB_FILE/2000/12/245.pdf,
  //
  // and scale the residual corrections by 1 - eta, where eta is the
  // centering parameter that multiplies the average complement to specify
  // the Mehrotra barrier parameter.
  //
  // We deviate from this convention because we sometimes use an affine
  // target barrier that is a modest multiple of the average complement,
  // rather than the traditional value of zero, so that we can compensate for
  // the accumulated inaccuracies in the approximate linear system solves --
  // especially those arising from the proximal regularization terms. These
  // larger affine target values allow us to keep the barrier parameter from
  // becoming very small before the primal and dual iterates become
  // approximately feasible; as the barrier parameter decreases, the first-order
  // optimality linear systems become more ill-conditioned and achieving
  // approximate primal and dual feasibility becomes harder.
  //
  // If we were to adopt the traditional "1 - centering" approach to always
  // proportionally decreasing the infeasibility residuals and barrier
  // parameter, then our above scheme would cease to progress on the reduction
  // of the primal and dual infeasibility when picking centering values near
  // one.
  //
  // Our approach is analogous to that of Gondzio's HOPDM, but, rather than
  // manually perturbing complement ratios that are too small relative to the
  // primal and dual infeasibility residual norms, we use these norms to compute
  // a lower bound on the affine barrier parameter. And we incorporate a
  // constraint on the maximum ratio between the maximum and minimum complements
  // into our line searches.
  //
  // TODO(Jack Poulson): Determine a switching mechanism between "proportional"
  // and "pure Newton" residual scalings rather than hardcoding this choice.
  // The switch could take place when the affine barrier lower bound is
  // enforced. But, it's not yet clear to me that there is more than an
  // analytical benefit to the proportional decrease given the significant
  // impact of our approximate and regularized linear solves.
  //
  const bool proportional_decrease = false;

  // We will track the Tapia indicators using the approach of:
  //
  //   A. S. El-Bakry, R. A. Tapia, and Y. Zhang,
  //   "A study of indicators for identifying zero variables in interior-point
  //   methods", TR91-15, RICE, 1991.
  //
  IndicatorConfig<Real> indicator_config;
  Buffer<IndicatorStatistic<Real>> indicators(num_primal);

  ConvergenceMonitor convergence_monitor;
  convergence_monitor.Open(control_mod.output.convergence_filename);

  Int num_iter = 0;
  Real last_primal_step = 0;
  Real last_dual_step = 0;
  Real last_relative_residual_norm = 1;
  Real relative_residual_norm_decrease_ratio = 1;
  Real tau_decrease_ratio = 1;
  Real best_relative_error = 1;
  iterate.certificate_type = INVALID_CERTIFICATE;
  certificate->certificate_type = INVALID_CERTIFICATE;
  for (; num_iter < control_mod.max_iterations; ++num_iter) {
    Iterate<Real> unscaled_iterate = iterate;
    RescaleIterate(scaling, &unscaled_iterate);
    residuals.FillRelativeError(original_problem, unscaled_iterate);
    const Real relative_residual_norm =
        std::max(residuals.primal_equality.relative_residual_norm,
                 residuals.dual_equality.relative_residual_norm);
    if (num_iter == 0) {
      relative_residual_norm_decrease_ratio = Real(1);
    } else {
      relative_residual_norm_decrease_ratio =
          relative_residual_norm / last_relative_residual_norm;
    }
    last_relative_residual_norm = relative_residual_norm;
    convergence_monitor.WriteLine(num_iter, iterate, residuals, reg_state,
                                  last_primal_step, last_dual_step);
    if (control.output.verbose) {
      std::cout << "Unscaled iterate at start of iter " << num_iter << "\n"
                << "primal: " << residuals.objective.primal << "\n"
                << "dual:   " << residuals.objective.dual << std::endl;
      unscaled_iterate.PrintNorms(std::cout);
      residuals.PrintRelativeError(std::cout);
    }

    const bool converged = CheckConvergence(
        num_iter, original_problem, residuals, control_mod, relative_tolerance,
        initial_average_complement, initial_tau_over_kappa, max_rescaled_norm,
        &iterate, certificate, &best_relative_error);
    if (converged) {
      break;
    }

    iterate.EnsureInPositiveOrthant();
    ProjectDualSlack(rescaled_problem, control_mod.dual_slack_projection,
                     &iterate);
    ConditionComplements(residuals, control_mod.complement_conditioning,
                         control_mod.output.verbose, &iterate);

    // Recompute the updated residuals and error.
    residuals.FillRelativeError(rescaled_problem, iterate);
    if (control_mod.output.verbose) {
      std::cout << "Scaled, updated iterate in iter " << num_iter << std::endl;
      iterate.PrintNorms(std::cout);
      residuals.PrintRelativeError(std::cout);
    }

    Real affine_barrier;
    bool use_affine_direction;
    bool factor_regularization;
    const AffineDirectionStatus affine_status = ComputeAffineDirection(
        num_iter, rescaled_problem, iterate,
        relative_residual_norm_decrease_ratio, tau_decrease_ratio, control_mod,
        inner_refined_solve_control, fgmres_control, reg_state,
        relative_tolerance, demanded_relative_residual, &diagonal_scale,
        &first_order_optimality, &affine_barrier, &use_affine_direction,
        &predictor, &residuals, &factor_regularization);
    if (affine_status == AFFINE_FACTORIZATION_FAILED) {
      if (control_mod.output.verbose) {
        std::cerr << "Could not factor affine matrix." << std::endl;
      }

      // Increase the factorization regularization parameters.
      UpdatePrimalSolutionRegularizationState(
          control_mod.proximal_regularization.factor_increase_factor, Real(1),
          control_mod.output.verbose, &reg_state);
      UpdateDualSolutionRegularizationState(
          control_mod.proximal_regularization.factor_increase_factor, Real(1),
          control_mod.output.verbose, &reg_state);
      UpdateDynamicRegularization(
          control_mod.proximal_regularization.factor_increase_factor,
          &control_mod.linear_solve.ldl);

      continue;
    } else if (affine_status == AFFINE_SOLVE_FAILED) {
      if (control_mod.output.verbose) {
        std::cerr << "Could not solve affine system." << std::endl;
      }

      // We decrease the gap between the primal and dual regularization
      // coefficients.
      //
      // TODO(Jack Poulson): Make this ratio configurable.
      const Real kRegularizationRatioDecrease = 50;
      const Real primal_solution_ratio =
          reg_state.factored_primal_solution_prox_reg /
          reg_state.target_primal_solution_prox_reg;
      const Real dual_solution_ratio =
          reg_state.factored_dual_solution_prox_reg /
          reg_state.target_dual_solution_prox_reg;
      if (primal_solution_ratio >= kRegularizationRatioDecrease) {
        UpdatePrimalSolutionRegularizationState(
            Real(1), kRegularizationRatioDecrease, control_mod.output.verbose,
            &reg_state);
      } else {
        UpdatePrimalSolutionRegularizationState(
            control_mod.proximal_regularization.factor_increase_factor,
            control_mod.proximal_regularization.target_increase_factor,
            control_mod.output.verbose, &reg_state);
      }

      if (dual_solution_ratio >= kRegularizationRatioDecrease) {
        UpdateDualSolutionRegularizationState(
            Real(1), kRegularizationRatioDecrease, control_mod.output.verbose,
            &reg_state);
      } else {
        UpdateDualSolutionRegularizationState(
            control_mod.proximal_regularization.factor_increase_factor,
            control_mod.proximal_regularization.target_increase_factor,
            control_mod.output.verbose, &reg_state);
      }

      continue;
    }
    if (control_mod.output.verbose) {
      PrintUpdateResiduals(rescaled_problem,
                           control_mod.optimality.iterate_normalization,
                           reg_state, iterate, predictor, std::cout);
    }

    // Compute the barrier parameters
    const std::pair<Real, Real> affine_step_sizes =
        PositiveOrthantPrimalDualStepSizes(iterate, predictor,
                                           control_mod.affine_step);
    if (control_mod.output.verbose) {
      std::cout << "Affine step sizes: "
                << static_cast<double>(last_primal_step) << ", "
                << static_cast<double>(last_dual_step) << std::endl;
    }
    if (use_affine_direction) {
      // Step in the affine direction.
      last_primal_step = affine_step_sizes.first;
      last_dual_step = affine_step_sizes.second;
      if (control_mod.output.verbose) {
        std::cout << "Using affine direction." << std::endl;
      }

      Iterate<Real> old_iterate = iterate;
      UpdateIterate(last_primal_step, last_dual_step, predictor, &iterate);
      UpdateIndicators(old_iterate, iterate, indicator_config, &indicators);
      tau_decrease_ratio = iterate.Tau() / old_iterate.Tau();

      continue;
    }
    Real centering, mehrotra_barrier;
    MehrotraBarrier(iterate, affine_barrier, affine_step_sizes.first,
                    affine_step_sizes.second, predictor,
                    control_mod.output.verbose, &centering, &mehrotra_barrier);
    if (!std::isfinite(centering) || !std::isfinite(mehrotra_barrier)) {
      *certificate = iterate;
      std::cout << "Encountered non-finite barrier parameter." << std::endl;
      break;
    }

    // Attempt to incorporate Mehrotra and Gondzio correctors into the
    // predictor.
    std::pair<Real, Real> combined_step_sizes = affine_step_sizes;
    const bool computed_corrected = ComputeCorrectedDirection(
        rescaled_problem, diagonal_scale, reg_state, control_mod,
        inner_refined_solve_control, fgmres_control, iterate, affine_step_sizes,
        affine_barrier, mehrotra_barrier, centering, proportional_decrease,
        factor_regularization, demanded_relative_residual,
        &first_order_optimality, &predictor, &corrector, &residuals,
        &combined_step_sizes);

    // Step in the combined direction.
    last_primal_step = combined_step_sizes.first;
    last_dual_step = combined_step_sizes.second;
    if (control_mod.output.verbose) {
      std::cout << "Using combined step sizes: "
                << static_cast<double>(last_primal_step) << ", "
                << static_cast<double>(last_dual_step) << std::endl;
    }
    Iterate<Real> old_iterate = iterate;
    UpdateIterate(last_primal_step, last_dual_step, predictor, &iterate);
    UpdateIndicators(old_iterate, iterate, indicator_config, &indicators);
    tau_decrease_ratio = iterate.Tau() / old_iterate.Tau();

    if (computed_corrected) {
      // Loosen the proximal regularization parameters since all solves were
      // successful.
      UpdatePrimalSolutionRegularizationState(
          control_mod.proximal_regularization.factor_decay_factor,
          control_mod.proximal_regularization.target_decay_factor,
          control_mod.output.verbose, &reg_state);
      UpdateDualSolutionRegularizationState(
          control_mod.proximal_regularization.factor_decay_factor,
          control_mod.proximal_regularization.target_decay_factor,
          control_mod.output.verbose, &reg_state);
      UpdateDynamicRegularization(
          control_mod.proximal_regularization.factor_decay_factor,
          &control_mod.linear_solve.ldl);
    }

    if (control_mod.output.verbose) {
      std::cout << std::endl;
    }
  }

  // Extract the current state of the basis indicators.
  Buffer<bool> basis_indicator(num_primal);
  for (Int i = 0; i < num_primal; ++i) {
    basis_indicator[i] = !indicators[i].assumed_zero;
  }

  if (certificate->certificate_type == INFEASIBILITY_CERTIFICATE ||
      certificate->certificate_type == PRIMAL_INFEASIBILITY_CERTIFICATE ||
      certificate->certificate_type == DUAL_INFEASIBILITY_CERTIFICATE) {
    PostprocessInfeasibleIterate(original_problem, rescaled_problem, scaling,
                                 iterate, control_mod, refined_solve_control,
                                 basis_indicator, certificate);
  } else {
    PostprocessApproximateSolution(original_problem, rescaled_problem, scaling,
                                   iterate, control_mod, refined_solve_control,
                                   basis_indicator, best_relative_error,
                                   certificate);
  }

  return num_iter;
}

}  // namespace hsd_qp
}  // namespace conic

// References:
//
//  [1]: Marco Colombo and Jacek Gondzio,
//       "Further development of multiple centrality correctors for Interior
//       Point Methods.", University of Edinburgh, MS-2005-001, revised
//       2006. URL: https://www.maths.ed.ac.uk/~gondzio/reports/mcjgMCC.pdf
//
//  [2]: Csaba Meszaros,
//       "The practical behavior of the homogeneous self-dual formulations in
//       interior point methods", Cent Eur J Oper Res, 23:913, 2015.
//       DOI: https://doi.org/10.1007/s10100-013-0336-1
//
//  [3]: E. D. Andersen, C. Roos, and T. Terlaky,
//       "On implementing a primal-dual interior-point method for conic
//       quadratic optimization", Dec. 18, 2000.
//       URL: http://www.optimization-online.org/DB_FILE/2000/12/245.pdf.
//

#endif  // ifndef CONIC_HSD_QP_SOLVE_IMPL_H_
