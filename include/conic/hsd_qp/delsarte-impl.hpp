/*
 * Copyright (c) 2019 Jack Poulson <jack@hodgestar.com>
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
#ifndef CONIC_HSD_QP_DELSARTE_IMPL_H_
#define CONIC_HSD_QP_DELSARTE_IMPL_H_

#include "conic/hsd_qp/delsarte.hpp"

namespace conic {
namespace hsd_qp {

template <typename Real>
void DelsarteBoundProblem(Int field_order, Int num_elements, Int distance,
                          Problem<Real>* problem) {
  // Recall that we can express this linear program in standard form by
  // introducing (n + 1) slacks for the nontrivial inequality constraints, s_i,
  // i = 0, ..., n, and posing the linear program
  //
  //   arginf_x { c' x : A x = b, x >= 0 },
  //
  // where
  //
  //  x = | w_{d:n} |,  c = | -1 |,  A = | (K_i(j; n))_{i=0:n, j=d:n}, -I |,
  //      | s_{0:n} |       |  0 |
  //
  //  b = | (-K_i(0; n))_{i=0:n} |.
  //
  // The entries of w correspond to distance distribution values, the s vector
  // to slacks, and K_i(j; n) is an evaluation of a degree i Kravchuk
  // polynomial.
  //

  const Int num_dist_bounds = num_elements + 1 - distance;
  const Int num_slack = num_elements + 1;
  const Int num_primal = num_dist_bounds + num_slack;
  const Int num_dual = num_slack;

  // Fill the linear objective as negative one over the distance distribution
  // variables and zero over the slack variables.
  problem->linear_objective.Resize(num_primal, 1, Real(0));
  for (Int i = 0; i < num_dist_bounds; ++i) {
    problem->linear_objective(i) = Real(-1);
  }

  // Set the quadratic portion of the objective to zero.
  problem->quadratic_objective.Resize(num_primal, num_primal);

  // Fill the Kravchuk polynomial and negative identity components of the
  // equality matrix.
  problem->equality_matrix.Resize(num_dual, num_primal);
  problem->equality_matrix.ReserveEntryAdditions(num_dual * num_dist_bounds +
                                                 num_dual);
  for (Int i = 0; i < num_dual; ++i) {
    for (Int j = 0; j < num_dist_bounds; ++j) {
      const Real value =
          KravchukPolynomial<Real>(field_order, num_elements, i, j + distance);
      problem->equality_matrix.QueueEntryAddition(i, j, value);
    }
  }
  for (Int i = 0; i < num_dual; ++i) {
    problem->equality_matrix.QueueEntryAddition(i, i + num_dist_bounds,
                                                Real(-1));
  }
  problem->equality_matrix.FlushEntryQueues();

  // Fill the equality right-hand side vector, which results from moving the
  // interaction of the first-column of the MacWilliams identity to the
  // right-hand side since the zero-distance distribution component is
  // identically one.
  problem->equality_rhs.Resize(num_dual, 1);
  for (Int i = 0; i < num_dual; ++i) {
    problem->equality_rhs(i) =
        -KravchukPolynomial<Real>(field_order, num_elements, i, 0);
  }

  problem->FillNorms();
}

template <typename Real>
Real DelsarteBound(Int field_order, Int num_elements, Int distance,
                   const SolveControl<Real>& control) {
  Problem<Real> problem;
  DelsarteBoundProblem(field_order, num_elements, distance, &problem);

  Iterate<Real> iterate;
  Solve(problem, control, &iterate);
  if (iterate.certificate_type != OPTIMALITY_CERTIFICATE) {
    std::cerr << "Unable to compute Delsarte bound: "
              << iterate.certificate_type << std::endl;
    return Real(-1);
  }

  const ConstBlasMatrixView<Real> primal_solution = iterate.PrimalSolution();
  const Real tau = iterate.Tau();

  Real upper_bound = 1;
  for (Int i = 0; i < num_elements - distance + 1; ++i) {
    upper_bound += primal_solution(i) / tau;
  }

  return upper_bound;
}

template <typename Real>
Real KravchukPolynomial(Int prime_power, Int n, Int degree, Int argument) {
  Real sum = 0;
  int sign = 1;
  for (Int j = 0; j <= degree; ++j, sign *= -1) {
    const Real term = sign * std::pow(Real(prime_power - 1), degree - j) *
                      Choose<Real>(argument, j) *
                      Choose<Real>(n - argument, degree - j);
    sum += term;
  }
  return sum;
}

template <typename Real>
Real Choose(Int groundset_size, Int subset_size) {
  CONIC_ASSERT(subset_size >= 0, "Invalid choose evaluation.");
  CONIC_ASSERT(groundset_size >= 0, "Invalid choose evaluation.");
  if (subset_size > groundset_size) {
    return Real(0);
  }

  Real product = 1;
  for (Int j = 0; j < subset_size; ++j) {
    product *= Real(groundset_size - j) / Real(subset_size - j);
  }
  return product;
}

}  // namespace hsd_qp
}  // namespace conic

#endif  // ifndef CONIC_HSD_QP_DELSARTE_IMPL_H_
