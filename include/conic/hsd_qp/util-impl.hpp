/*
 * Copyright (c) 2019 Jack Poulson <jack@hodgestar.com>
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
#ifndef CONIC_HSD_QP_UTIL_IMPL_H_
#define CONIC_HSD_QP_UTIL_IMPL_H_

#include <exception>
#include <limits>

#include "conic/hsd_qp/util.hpp"
#include "conic/imports.hpp"

namespace conic {
namespace hsd_qp {

template <typename Real>
Real AverageComplement(const Iterate<Real>& iterate) {
  const ConstBlasMatrixView<Real> iter_primal_solution =
      iterate.PrimalSolution();
  const ConstBlasMatrixView<Real> iter_dual_slack = iterate.DualSlack();
  const Int num_primal = iter_primal_solution.Height();

  Real average_complement = 0;
  for (Int i = 0; i < num_primal; ++i) {
    const Real primal_solution = iter_primal_solution(i);
    average_complement += primal_solution * iter_dual_slack(i);
  }
  average_complement += iterate.Tau() * iterate.Kappa();
  average_complement /= num_primal + 1;

  return average_complement;
}

template <typename Real>
Real UpdatedAverageComplement(const Iterate<Real>& iterate, Real primal_step,
                              Real dual_step, const Iterate<Real>& update) {
  const ConstBlasMatrixView<Real> iter_primal_solution =
      iterate.PrimalSolution();
  const ConstBlasMatrixView<Real> iter_dual_slack = iterate.DualSlack();
  const Int num_primal = iter_primal_solution.Height();
  const ConstBlasMatrixView<Real> primal_solution_update =
      update.PrimalSolution();
  const ConstBlasMatrixView<Real> dual_slack_update = update.DualSlack();

  Real average_complement = 0;
  for (Int i = 0; i < num_primal; ++i) {
    const Real primal_solution =
        iter_primal_solution(i) + primal_step * primal_solution_update(i);
    const Real dual_slack =
        iter_dual_slack(i) + dual_step * dual_slack_update(i);
    average_complement += primal_solution * dual_slack;
  }
  average_complement /= num_primal + 1;

  return average_complement;
}

template <typename Real>
Real ComplementRatio(const Iterate<Real>& iterate, bool verbose) {
  const ConstBlasMatrixView<Real> iter_primal_solution =
      iterate.PrimalSolution();
  const ConstBlasMatrixView<Real> iter_dual_slack = iterate.DualSlack();
  const Int num_primal = iter_primal_solution.Height();

  Int min_index = -1;
  Int max_index = -1;
  Real min_complement = std::numeric_limits<Real>::max();
  Real max_complement = 0;
  for (Int i = 0; i < num_primal; ++i) {
    const Real primal_solution = iter_primal_solution(i);
    const Real complement = primal_solution * iter_dual_slack(i);
    if (complement < min_complement) {
      min_complement = complement;
      min_index = i;
    }
    if (complement > max_complement) {
      max_complement = complement;
      max_index = i;
    }
  }
  {
    const Real complement = iterate.Tau() * iterate.Kappa();
    if (complement < min_complement) {
      min_complement = complement;
      min_index = num_primal;
    }
    if (complement > max_complement) {
      max_complement = complement;
      max_index = num_primal;
    }
  }

  if (verbose) {
    const Real min_primal = min_index == num_primal
                                ? iterate.Tau()
                                : iter_primal_solution(min_index);
    const Real max_primal = max_index == num_primal
                                ? iterate.Tau()
                                : iter_primal_solution(max_index);

    const Real min_dual =
        min_index == num_primal ? iterate.Kappa() : iter_dual_slack(min_index);
    const Real max_dual =
        max_index == num_primal ? iterate.Kappa() : iter_dual_slack(max_index);

    std::cout << "minimum complement of " << min_complement << " is at index "
              << min_index
              << " with primal_solution=" << static_cast<double>(min_primal)
              << " and dual_slack=" << static_cast<double>(min_dual) << "\n";
    std::cout << "maximum complement of " << max_complement << " is at index "
              << max_index
              << " with primal_solution=" << static_cast<double>(max_primal)
              << " and dual_slack=" << static_cast<double>(max_dual) << "\n";
    std::cout << "Complement ratio: "
              << static_cast<double>(max_complement / min_complement)
              << std::endl;
  }

  return max_complement / min_complement;
}

template <typename Real>
Real UpdatedComplementRatio(const Iterate<Real>& iterate,
                            const Real primal_step, const Real dual_step,
                            const Iterate<Real>& update) {
  const ConstBlasMatrixView<Real> iter_primal_solution =
      iterate.PrimalSolution();
  const ConstBlasMatrixView<Real> iter_dual_slack = iterate.DualSlack();
  const ConstBlasMatrixView<Real> update_primal_solution =
      update.PrimalSolution();
  const ConstBlasMatrixView<Real> update_dual_slack = update.DualSlack();
  const Int num_primal = iter_primal_solution.Height();

  Real min_complement = std::numeric_limits<Real>::max();
  Real max_complement = 0;

  // Compute the new taus and kappa.
  const Real tau_primal = iterate.Tau() + primal_step * update.Tau();
  const Real tau_dual = iterate.Tau() + dual_step * update.Tau();
  const Real tau = std::min(tau_primal, tau_dual);
  const Real kappa =
      iterate.Kappa() +
      (tau_primal <= tau_dual ? dual_step : primal_step) * update.Kappa();
  {
    const Real complement = tau * kappa;
    if (complement < min_complement) {
      min_complement = complement;
    }
    if (complement > max_complement) {
      max_complement = complement;
    }
  }

  for (Int i = 0; i < num_primal; ++i) {
    const Real primal_solution =
        (tau / tau_primal) *
        (iter_primal_solution(i) + primal_step * update_primal_solution(i));
    const Real dual_slack =
        (tau / tau_dual) *
        (iter_dual_slack(i) + dual_step * update_dual_slack(i));
    const Real complement = primal_solution * dual_slack;
    if (complement < min_complement) {
      min_complement = complement;
    }
    if (complement > max_complement) {
      max_complement = complement;
    }
  }

  return max_complement / min_complement;
}

template <typename Real>
void EnforceMinimumPositiveOrthantComplement(Real min_complement, bool verbose,
                                             Iterate<Real>* iterate) {
  BlasMatrixView<Real> iter_primal_solution = iterate->PrimalSolution();
  BlasMatrixView<Real> iter_dual_slack = iterate->DualSlack();
  const Int num_primal = iter_primal_solution.Height();

  if (verbose) {
    std::cout << "Complement ratio before push:\n";
    ComplementRatio(*iterate, verbose);
  }

  for (Int i = 0; i < num_primal; ++i) {
    const Real primal_slack = iter_primal_solution(i);
    const Real dual_slack = iter_dual_slack(i);
    const Real complement = primal_slack * dual_slack;
    if (complement < min_complement) {
      if (primal_slack <= dual_slack) {
        const Real new_primal_slack = min_complement / dual_slack;
        if (verbose) {
          std::cout << "Pushing x from " << primal_slack << " up to "
                    << new_primal_slack << " (dual_slack: " << dual_slack
                    << ") at index " << i << std::endl;
        }
        iter_primal_solution(i) = new_primal_slack;
      } else {
        const Real new_dual_slack = min_complement / primal_slack;
        if (verbose) {
          std::cout << "Pushing z from " << dual_slack << " up to "
                    << new_dual_slack << " (primal_slack: " << primal_slack
                    << ")"
                    << " at index " << i << std::endl;
        }
        iter_dual_slack(i) = new_dual_slack;
      }
    }
  }
  {
    const Real complement = iterate->Tau() * iterate->Kappa();
    if (complement < min_complement) {
      if (iterate->Tau() < iterate->Kappa()) {
        const Real new_tau = min_complement / iterate->Kappa();
        if (verbose) {
          std::cout << "Pushing tau from " << iterate->Tau() << " up to "
                    << new_tau << " (kappa: " << iterate->Kappa() << ")"
                    << std::endl;
        }
        iterate->Tau() = new_tau;
      } else {
        const Real new_kappa = min_complement / iterate->Tau();
        if (verbose) {
          std::cout << "Pushing kappa from " << iterate->Kappa() << " up to "
                    << new_kappa << " (tau: " << iterate->Tau() << ")"
                    << std::endl;
        }
        iterate->Kappa() = new_kappa;
      }
    }
  }

  if (verbose) {
    std::cout << "Complement ratio after push:\n";
    ComplementRatio(*iterate, verbose);
  }
}

template <typename Real>
void ConditionComplements(const Residuals<Real>& state,
                          const ComplementConditioningControl<Real>& control,
                          bool verbose, Iterate<Real>* iterate) {
  if (!control.enabled) {
    return;
  }
  BlasMatrixView<Real> iter_primal_solution = iterate->PrimalSolution();
  BlasMatrixView<Real> iter_dual_slack = iterate->DualSlack();
  const Int num_primal = iter_primal_solution.Height();

  Real average_complement = 0;
  for (Int i = 0; i < num_primal; ++i) {
    const Real primal_solution = iter_primal_solution(i);
    const Real primal_slack = primal_solution;
    const Real dual_slack = iter_dual_slack(i);
    average_complement += primal_slack * dual_slack;
  }
  average_complement += iterate->Tau() * iterate->Kappa();
  average_complement /= num_primal + 1;

  const Real min_complement =
      control.min_average_complement_ratio * average_complement;

  if (verbose) {
    std::cout << "average_complement: "
              << static_cast<double>(average_complement)
              << ", min_complement: " << static_cast<double>(min_complement)
              << std::endl;
  }

  EnforceMinimumPositiveOrthantComplement(min_complement, verbose, iterate);
}

template <typename Real>
Real PositiveOrthantMaximumPrimalStepSize(const Iterate<Real>& iterate,
                                          const Iterate<Real>& update,
                                          Real max_step_size) {
  const ConstBlasMatrixView<Real> iter_primal_solution =
      iterate.PrimalSolution();
  const Int num_primal = iter_primal_solution.Height();
  const ConstBlasMatrixView<Real> primal_solution_update =
      update.PrimalSolution();
  Real step_size = max_step_size;
  for (Int i = 0; i < num_primal; ++i) {
    const Real primal_solution = iter_primal_solution(i);
    const Real primal_update = primal_solution_update(i);
    const Real primal_slack = primal_solution;
    CONIC_ASSERT(primal_slack > Real(0), "primal_slack(" + std::to_string(i) +
                                             ") was " +
                                             std::to_string(primal_slack));
    if (primal_update < Real(0)) {
      const Real step_upper_bound = -primal_slack / primal_update;
      step_size = std::min(step_size, step_upper_bound);
    }
  }
  if (update.Tau() < Real(0)) {
    CONIC_ASSERT(iterate.Tau() > Real(0),
                 "tau was " + std::to_string(iterate.Tau()));
    const Real step_upper_bound = -iterate.Tau() / update.Tau();
    step_size = std::min(step_size, step_upper_bound);
  }
  if (update.Kappa() < Real(0)) {
    CONIC_ASSERT(iterate.Kappa() > Real(0),
                 "kappa was " + std::to_string(iterate.Kappa()));
    const Real step_upper_bound = -iterate.Kappa() / update.Kappa();
    step_size = std::min(step_size, step_upper_bound);
  }
  return step_size;
}

template <typename Real>
Real PositiveOrthantMaximumDualStepSize(const Iterate<Real>& iterate,
                                        const Iterate<Real>& update,
                                        Real max_step_size) {
  const ConstBlasMatrixView<Real> iter_primal_solution =
      iterate.PrimalSolution();
  const ConstBlasMatrixView<Real> iter_dual_slack = iterate.DualSlack();
  const Int num_primal = iter_primal_solution.Height();
  const ConstBlasMatrixView<Real> dual_slack_update = update.DualSlack();
  Real step_size = max_step_size;
  for (Int i = 0; i < num_primal; ++i) {
    const Real dual_slack = iter_dual_slack(i);
    CONIC_ASSERT(dual_slack > Real(0), "dual_slack(" + std::to_string(i) +
                                           ") was " +
                                           std::to_string(dual_slack));
    const Real dual_update = dual_slack_update(i);
    if (dual_update < Real(0)) {
      step_size = std::min(step_size, -dual_slack / dual_update);
    }
  }
  if (update.Tau() < Real(0)) {
    CONIC_ASSERT(iterate.Tau() > Real(0),
                 "tau was " + std::to_string(iterate.Tau()));
    const Real step_upper_bound = -iterate.Tau() / update.Tau();
    step_size = std::min(step_size, step_upper_bound);
  }
  if (update.Kappa() < Real(0)) {
    CONIC_ASSERT(iterate.Kappa() > Real(0),
                 "kappa was " + std::to_string(iterate.Kappa()));
    const Real step_upper_bound = -iterate.Kappa() / update.Kappa();
    step_size = std::min(step_size, step_upper_bound);
  }
  return step_size;
}

template <typename Real>
std::pair<Real, Real> PositiveOrthantPrimalDualStepSizes(
    const Iterate<Real>& iterate, const Iterate<Real>& update,
    const StepSizeConfig<Real>& config) {
  static const Real kEpsilon = std::numeric_limits<Real>::epsilon();
  static const Real step_ratio =
      Real(1) - config.step_ratio_coefficient *
                    std::pow(kEpsilon, config.step_ratio_exponent);

  // TODO(Jack Poulson): Make this configurable.
  const bool allow_different_step_sizes = true;

  Real primal_step = PositiveOrthantMaximumPrimalStepSize(iterate, update,
                                                          config.max_step_size);
  Real dual_step =
      PositiveOrthantMaximumDualStepSize(iterate, update, config.max_step_size);

  primal_step = step_ratio * primal_step;
  dual_step = step_ratio * dual_step;

  const Real old_complement_ratio = ComplementRatio(iterate, config.verbose);
  const Real allowed_complement_ratio =
      std::max(config.allowed_complement_ratio_multiple * old_complement_ratio,
               config.max_allowed_complement_ratio);

  if (allow_different_step_sizes) {
    const ConstBlasMatrixView<Real> primal_solution = iterate.PrimalSolution();
    const ConstBlasMatrixView<Real> dual_slack = iterate.DualSlack();
    const ConstBlasMatrixView<Real> primal_solution_update =
        update.PrimalSolution();
    const ConstBlasMatrixView<Real> dual_slack_update = update.DualSlack();
    const Int num_primal = primal_solution.Height();
    if (primal_step > dual_step) {
      Real inner_product = 0;
      for (Int i = 0; i < num_primal; ++i) {
        inner_product += dual_slack(i) * primal_solution_update(i);
      }

      if (inner_product > Real(0)) {
        const Real min_step = std::min(primal_step, dual_step);
        primal_step = dual_step = min_step;
      }
    } else if (primal_step < dual_step) {
      Real inner_product = 0;
      for (Int i = 0; i < num_primal; ++i) {
        inner_product += primal_solution(i) * dual_slack_update(i);
      }

      if (inner_product > Real(0)) {
        const Real min_step = std::min(primal_step, dual_step);
        primal_step = dual_step = min_step;
      }
    }
  } else {
    const Real min_step = std::min(primal_step, dual_step);
    primal_step = dual_step = min_step;
  }

  Real complement_ratio =
      UpdatedComplementRatio(iterate, primal_step, dual_step, update);
  while (complement_ratio > allowed_complement_ratio) {
    if (config.verbose) {
      std::cout << "Rejected primal_step: " << primal_step
                << ", dual_step: " << dual_step
                << " since complement_ratio: " << complement_ratio << std::endl;
    }

    primal_step *= config.backoff_ratio;
    dual_step *= config.backoff_ratio;
    const Real max_step = std::max(primal_step, dual_step);
    if (max_step < config.backoff_exit) {
      if (config.verbose) {
        std::cout << "Breaking backtracking since max_step: " << max_step
                  << std::endl;
      }
      break;
    }

    complement_ratio =
        UpdatedComplementRatio(iterate, primal_step, dual_step, update);
  }

  primal_step = std::min(primal_step, config.max_step_imbalance * dual_step);
  dual_step = std::min(dual_step, config.max_step_imbalance * primal_step);

  if (config.verbose) {
    std::cout << "Accepted primal_step: " << primal_step
              << ", dual_step: " << dual_step
              << " with complement_ratio: " << complement_ratio << std::endl;
  }

  return std::make_pair(primal_step, dual_step);
}

template <typename Real>
void MehrotraBarrier(const Iterate<Real>& iterate, Real affine_barrier,
                     Real primal_step, Real dual_step,
                     const Iterate<Real>& update, bool verbose, Real* centering,
                     Real* mehrotra_barrier) {
  const ConstBlasMatrixView<Real> iter_primal_solution =
      iterate.PrimalSolution();
  const ConstBlasMatrixView<Real> iter_dual_slack = iterate.DualSlack();
  const Int num_primal = iter_primal_solution.Height();
  const ConstBlasMatrixView<Real> primal_solution_update =
      update.PrimalSolution();
  const ConstBlasMatrixView<Real> dual_slack_update = update.DualSlack();

  Real average_complement = 0;
  Real affine_average_complement = 0;
  for (Int i = 0; i < num_primal; ++i) {
    const Real primal_slack = iter_primal_solution(i);
    CONIC_ASSERT(primal_slack > Real(0), "primal_slack(" + std::to_string(i) +
                                             ") was " +
                                             std::to_string(primal_slack));
    const Real dual_slack = iter_dual_slack(i);
    CONIC_ASSERT(dual_slack > Real(0), "dual_slack(" + std::to_string(i) +
                                           ") was " +
                                           std::to_string(dual_slack));
    average_complement += primal_slack * dual_slack;

    const Real update_primal = primal_solution_update(i);
    const Real update_dual = dual_slack_update(i);
    const Real affine_primal = primal_slack + primal_step * update_primal;
    const Real affine_dual = dual_slack + dual_step * update_dual;
    affine_average_complement += affine_primal * affine_dual;
  }
  average_complement += iterate.Tau() * iterate.Kappa();
  affine_average_complement += (iterate.Tau() + primal_step * update.Tau()) *
                               (iterate.Kappa() + dual_step * update.Kappa());
  average_complement /= num_primal + 1;
  affine_average_complement /= num_primal + 1;

  const Real proposed_centering =
      std::pow(affine_average_complement / average_complement, 3);
  *mehrotra_barrier = proposed_centering * average_complement;

  Real max_affine_average_complement_ratio;
  const Real min_step = std::min(primal_step, dual_step);
  if (min_step >= Real(0.2)) {
    max_affine_average_complement_ratio = 0.1;
  } else if (min_step >= Real(0.1)) {
    max_affine_average_complement_ratio = 0.2;
  } else {
    max_affine_average_complement_ratio = 0.333;
  }
  *mehrotra_barrier =
      std::min(*mehrotra_barrier,
               max_affine_average_complement_ratio * affine_average_complement);

  // Ensure that the barrier is not excessively small relative to the average
  // complement from stepping in the affine direction, as it could lead to
  // complements that do not work for the current feasibility residuals.
  *mehrotra_barrier =
      std::max(*mehrotra_barrier, Real(1e-3) * affine_average_complement);

  // Prevent the Mehrotra barrier from decreasing too quickly.
  *mehrotra_barrier = std::max(*mehrotra_barrier, affine_barrier);

  *centering = *mehrotra_barrier / average_complement;

  if (verbose) {
    std::cout << "  average_complement:        "
              << static_cast<double>(average_complement) << "\n"
              << "  affine_average_complement: "
              << static_cast<double>(affine_average_complement) << "\n"
              << "  centering:         " << static_cast<double>(*centering)
              << std::endl;
  }
}

template <typename Real>
void UpdateIterate(Real step, const Iterate<Real>& update,
                   Iterate<Real>* iterate) {
  Axpy(step, update.PrimalSolution(), &iterate->PrimalSolution());
  Axpy(step, update.DualSolution(), &iterate->DualSolution());
  Axpy(step, update.DualSlack(), &iterate->DualSlack());
  iterate->Tau() += step * update.Tau();
  iterate->Kappa() += step * update.Kappa();
}

template <typename Real>
void UpdateIterate(Real primal_step, Real dual_step,
                   const Iterate<Real>& update, Iterate<Real>* iterate) {
  BlasMatrixView<Real> primal_solution = iterate->PrimalSolution();
  BlasMatrixView<Real> dual_solution = iterate->DualSolution();
  BlasMatrixView<Real> dual_slack = iterate->DualSlack();
  const Int num_primal = primal_solution.Height();
  const Int num_dual = dual_solution.Height();

  const Real tau_primal = iterate->Tau() + primal_step * update.Tau();
  const Real tau_dual = iterate->Tau() + dual_step * update.Tau();
  iterate->Tau() = std::min(tau_primal, tau_dual);

  if (tau_primal <= tau_dual) {
    iterate->Kappa() += dual_step * update.Kappa();
  } else {
    iterate->Kappa() += primal_step * update.Kappa();
  }

  Axpy(primal_step, update.PrimalSolution(), &primal_solution);
  for (Int i = 0; i < num_primal; ++i) {
    primal_solution(i) *= iterate->Tau() / tau_primal;
  }

  Axpy(dual_step, update.DualSolution(), &dual_solution);
  for (Int i = 0; i < num_dual; ++i) {
    dual_solution(i) *= iterate->Tau() / tau_dual;
  }

  Axpy(dual_step, update.DualSlack(), &dual_slack);
  for (Int i = 0; i < num_primal; ++i) {
    dual_slack(i) *= iterate->Tau() / tau_dual;
  }
}

}  // namespace hsd_qp
}  // namespace conic

#endif  // ifndef CONIC_HSD_QP_UTIL_IMPL_H_
