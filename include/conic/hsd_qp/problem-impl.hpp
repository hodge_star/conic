/*
 * Copyright (c) 2019 Jack Poulson <jack@hodgestar.com>
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
#ifndef CONIC_HSD_QP_PROBLEM_IMPL_H_
#define CONIC_HSD_QP_PROBLEM_IMPL_H_

#include <exception>
#include <limits>

#include "conic/hsd_qp/problem.hpp"

namespace conic {
namespace hsd_qp {

template <typename Real>
void Problem<Real>::FillNorms() {
  linear_objective_two_norm = EuclideanNorm(linear_objective.ConstView());
  quadratic_objective_max_norm = MaxNorm(quadratic_objective);
  equality_matrix_max_norm = MaxNorm(equality_matrix);
  equality_rhs_two_norm = EuclideanNorm(equality_rhs.ConstView());
}

template <typename Real>
void Problem<Real>::PrintNorms(std::ostream& os) const {
  os << "|| c ||_2:   " << static_cast<double>(linear_objective_two_norm)
     << "\n"
     << "|| Q ||_max: " << static_cast<double>(quadratic_objective_max_norm)
     << "\n"
     << "|| A ||_max: " << static_cast<double>(equality_matrix_max_norm) << "\n"
     << "|| b ||_2:   " << static_cast<double>(equality_rhs_two_norm)
     << std::endl;
}

template <typename Real>
void Problem<Real>::FormEqualityMatrixTranspose() const {
  if (!have_equality_matrix_transpose) {
    FormSparseTranspose(equality_matrix, &equality_matrix_transpose);
    have_equality_matrix_transpose = true;
  }
}

template <typename Real>
std::ostream& operator<<(std::ostream& os, const Problem<Real>& problem) {
  os << "c=[\n"
     << problem.linear_objective << "];\n"
     << "QSparse=[\n"
     << problem.quadratic_objective << "];\n"
     << "ASparse=[\n"
     << problem.equality_matrix << "];\n"
     << "b=[\n"
     << problem.equality_rhs << "];\n"
     << std::endl;
  return os;
}

}  // namespace hsd_qp
}  // namespace conic

#endif  // ifndef CONIC_HSD_QP_PROBLEM_IMPL_H_
