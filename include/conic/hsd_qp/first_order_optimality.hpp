/*
 * Copyright (c) 2019 Jack Poulson <jack@hodgestar.com>
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
#ifndef CONIC_HSD_QP_FIRST_ORDER_OPTIMALITY_H_
#define CONIC_HSD_QP_FIRST_ORDER_OPTIMALITY_H_

#include "conic/hsd_qp/problem.hpp"
#include "conic/imports.hpp"

namespace conic {
namespace hsd_qp {

// Rather than factoring the matrix associated with the system
//
//  |               A           |              -b              | |   dy   |
//  |  -A'     Q + inv(X) Z     |               c              | |   dx   |
//  |---------------------------|------------------------------| |--------| =
//  |   b'  -(c + 2 Q x / tau)' | x' Q x / tau^2 + kappa / tau | |  dtau  |
//
//    | q_{hsd_primal_equality}                 |
//    | q_{hsd_dual_equality} + inv(X) q_{comp} |
//    |-----------------------------------------|,
//    | q_{hsd_gap} + inv(tau) q_{scalar_comp}  |
//
// we instead negate the second row and then swap the dy and dx ordering. The
// result is:
//
//  |  -(Q + inv(X) Z)     A'  |              -c               | |   dx   |
//  |         A                |              -b               | |   dy   | =
//  |--------------------------|-------------------------------| |--------|
//  | -(c + 2 Q x / tau)'  b'  | x' Q x / tau^2 + kappa / tau  | |  dtau  |
//
//    | -q_{hsd_dual_equality} - inv(X) q_{comp} |
//    | q_{hsd_primal_equality}                  |
//    |------------------------------------------|.
//    | q_{hsd_gap} + inv(tau) q_{scalar_comp}   |
//
// We label the upper-left matrix as J00, the upper-right as J01, the
// lower-left as J10, and the bottom-right as J11.
//
// We (plan to soon) add support for 'masked' primal solution indices: if
// indicators, such as the primal-dual indicator x_j / z_j, indicate that x_j
// is converging to an optimal value of zero, then we modify the above system to
// enforce this constraint through changing the corresponding row of the
// linearization of the complementarity condition, which would typically have
// resulted in
//
//   z_i dx_i + x_i dz_i = (q_{comp})_i,
//
// where the full KKT system would typically have been
//
//  |     -(Q + R_x)         A'  |       -c       | I      |
//  |          A            R_y  |       -b       |        |
//  |----------------------------|----------------|--------|
//  |  -(c + 2 Q x / tau)'   b'  | x' Q x / tau^2 |    -1  |.
//  |----------------------------|----------------|--------|
//  |          Z                 |                | X      |
//  |                            |      kappa     |    tau |
//
// We replace the j'th row of the linearized complementarity condition with the
// constraint
//
//   dx_j = - alpha x_j,
//
// for some alpha in [0, 1].
//
// Rather than performing the typical Schur complement elimination with pivot
// x_j, we eliminate the column of the full KKT system corresponding to dx_j
// by subtracting its multiplication by dx_j = -alpha x_j from the right-hand
// side. To preserve symmetry, we enforce the constraint
//
//   (-Q dx + A' dy - c dtau + dz)_j = (-q_{hsd_dual_equality})_j
//
// via setting
//
//   dz_j := (-q_{hsd_dual_equality} + c dtau + Q dx - A' dy)_j
//
// after the solve. Notice that this constraint is generally contradictory to
// enforcing
//
//   z_j dx_j + x_j dz_j = mu - x_j z_j
//
// or a perturbation thereof, and allows us to have a chance of correcting a
// false assumption of x_j converging to zero via the primal-dual indicator
// x_j / z_j of the next iteration.
//
// To avoid the need for a different symbolic factorization for each different
// variable masking pattern, or a different sparsity pattern for the full KKT
// matrices, we incorporate explicit zeros in the full KKT matrices where
// necessary and replace the obsolete rows and columns of the augmented system
//
//   | -(Q + R_x + inv(X) Z)    A' |
//   |           A             R_y |
//
// with -e_j and -e_j', and set the j'th right-hand side value to zero. Again,
// we emphasize that we fill the replaced entry pattern with explicit zeros
// outside of the diagonal.
//
// The Schur complement elimination of this modified system, which is only
// relevant when Q is diagonal, equates to ignoring outer-product updates over
// the masked variables.
//
template <typename Real>
class FirstOrderOptimality {
 public:
  // Form the blocks of the matrix for the first time.
  void InitializeMatrix(const Problem<Real>& problem,
                        const Iterate<Real>& iterate,
                        const ConstBlasMatrixView<Real>& diagonal_scale,
                        const RegularizationState<Real>& reg_state,
                        bool factor_schur_complement);

  // Reform the blocks of the matrix from a new state in a manner which makes
  // use of the same nonzero structure as the last initialization.
  void ModifyMatrix(const Problem<Real>& problem, const Iterate<Real>& iterate,
                    const ConstBlasMatrixView<Real>& diagonal_scale,
                    const RegularizationState<Real>& reg_state);

  // Forms the block factorization of the first-order optimality matrix:
  //   J01 := inv(J00) J01,
  //   J11 := J11 - J10 J01, and
  //   J11 := inv(J11).
  bool FactorMatrix(const Problem<Real>& problem, const Iterate<Real>& iterate,
                    const ConstBlasMatrixView<Real>& diagonal_scale,
                    const SparseLDLControl<Real>& ldl_control,
                    const RefinedSolveControl<Real>& refined_solve_control,
                    const catamari::FGMRESControl<Real>& fgmres_control,
                    Real demanded_relative_residual, bool verbose);

  // Solve the diagonally-scaled linear system using the factored matrix.
  //
  // We employ the block triangular factorization:
  //
  //   | J00 | J01 |   | J00 |           0            | | I | inv(J00) J01 |
  //   |-----|-----| = |-----|------------------------| |---|--------------|
  //   | J10 | J11 |   | J10 | J11 - J10 inv(J00) J01 | | 0 |      I       |
  //
  // where we have a factorization of J00 and have overwritten the Schur
  // complement J11 - J10 inv(J00) J01 with its inverse. After solving, we
  // undo the diagonal scaling via applying [inv(X); I; inv(tau); 1].
  //
  // We can apply the inverse of our block matrix via sequentially applying
  // the inverse of the block lower triangular, then block upper triangular,
  // matrices. That is, for a right-hand side q, we first apply the
  // inverse of the block lower-triangular matrix via:
  //
  //   q0 := inv(J00) q0,
  //   q1 := q1 - J10 q0,
  //   q1 := inv(J11 - J10 inv(J00) J01) q1.
  //
  // We can then solve against the block upper-triangular matrix via:
  //
  //   q0 := q0 - (inv(J00) J01) q1.
  //
  // It is worth emphasizing that inv(J00) can be applied via iterative
  // refinement using the factorization of the symmetric quasi-definite
  // regularization of J00, inv(J00) J01 is precomputed, as is the inverse of
  // the two-by-two Schur complement J11 - J10 inv(J00) J01.
  //
  // The only step that with a nontrivial chance of failure is the application
  // of inv(J00) via iterative refinement.
  //
  // TODO(Jack Poulson): Decide the input format given a nontrivial variable
  // mask.
  //
  bool Solve(bool factor_regularization, const Problem<Real>& problem,
             const Iterate<Real>& iterate,
             const ConstBlasMatrixView<Real>& diagonal_scale,
             const Residuals<Real>& residuals,
             const RegularizationState<Real>& reg_state,
             const RefinedSolveControl<Real>& refined_solve_control,
             const catamari::FGMRESControl<Real>& fgmres_control,
             Real demanded_relative_residual, bool verbose,
             bool analyze_residuals, bool analyze_iterate_residuals,
             Iterate<Real>* update);

 private:
  // If true, the matrix has been factored since its last formation.
  bool factored_ = false;

  // If true, the matrix has been previously factored with the same sparsity.
  bool refactoring_ = false;

  // If true, the scaled squared norms || A(i, :) / sqrt(Theta) ||_2^2 should
  // be used to multiply the dual solution proximal regularization scalar.
  bool row_dependent_dual_solution_reg_ = false;

  // If true, the proximal regularization is only applied to maintain a minimum
  // value of diag(Q) + Z inv(X) after regularization. If false, rather than
  // maintaining said minimum, we also shift upward by said value.
  bool maintain_primal_solution_minimum_ = false;

  // The proximal regularization of J00,
  //
  //  | -(Q + inv(X) Z)   A' |
  //  |        A         R_y |
  //
  // where R_y is a non-negative diagonal matrix with diagonal entries typically
  // of magnitude roughly sqrt(epsilon).
  //
  CoordinateMatrix<Real> factor_upper_left_block_;
  SparseLDL<Real> upper_left_block_ldl_;
  SparseLDLResult<Real> upper_left_block_ldl_result_;
  BlasMatrix<Real> factor_theta_;

  // The optional Schur complement of the proximal regularization of J00.
  //   A inv(Q + inv(X) Z) A' + R_y.
  bool factor_schur_complement_ = false;
  CoordinateMatrix<Real> upper_left_schur_complement_;
  SparseLDL<Real> upper_left_schur_complement_ldl_;
  SparseLDLResult<Real> upper_left_schur_complement_ldl_result_;

  // The (m + n)-by-one dense matrix J01.
  BlasMatrix<Real> upper_right_block_;

  // The (m + n)-by-one dense matrix inv(J00) J01.
  BlasMatrix<Real> solved_upper_right_block_;

  // The one-by-(m + n) dense matrix J10.
  BlasMatrix<Real> bottom_left_block_;

  // The scalar J11.
  Real bottom_right_entry_;

  // The Schur complement onto the bottom-right entry:
  //   S11 = J11 - J10 (inv(J00) J01).
  Real bottom_right_schur_complement_;

  // Regularized versions of:
  //
  // |        -Q            A' |      -c        |  I      | | dx     |
  // |         A               |      -b        |         | | dy     |
  // |-------------------------|----------------|---------| |--------|
  // | -(c + 2 Q x / tau)'  b' | x' Q x / tau^2 |     -1  | | dtau   | =
  // |-------------------------|----------------|---------| |--------|
  // |         Z               |                |  X      | | dz     |
  // |                         |     kappa      |     tau | | dkappa |
  //
  //   | -q_{hsd_deg}     |
  //   |  q_{hsd_peq}     |
  //   |------------------|
  //   |  q_{hsd_gap}     |.
  //   |------------------|
  //   |  q_{comp}        |
  //   |  q_{scalar_comp} |
  //
  CoordinateMatrix<Real> target_kkt_;
  CoordinateMatrix<Real> factor_kkt_;

  void ReduceFromFullToHSDAugmented(
      const Iterate<Real>& iterate,
      const ConstBlasMatrixView<Real>& diagonal_scale,
      const RegularizationState<Real>& reg_state,
      BlasMatrixView<Real>* rhs) const;

  // We solve for dz and dkappa from the congruence scaling of the full
  // regularized KKT system
  //
  //  |     -(Q + R_x)         A'  |       -c       | I      |
  //  |          A            R_y  |       -b       |        |
  //  |----------------------------|----------------|--------|
  //  |  -(c + 2 Q x / tau)'   b'  | x' Q x / tau^2 |    -1  |
  //  |----------------------------|----------------|--------|
  //  |          Z                 |                | X      |
  //  |                            |      kappa     |    tau |
  void ExpandFromHSDAugmentedToFull(
      const Iterate<Real>& iterate,
      const ConstBlasMatrixView<Real>& diagonal_scale,
      const RegularizationState<Real>& reg_state,
      BlasMatrixView<Real>* rhs) const;

  // We employ the block triangular factorization:
  //
  //   | J00 | J01 |   | J00 |           0            | | I | inv(J00) J01 |
  //   |-----|-----| = |-----|------------------------| |---|--------------|
  //   | J10 | J11 |   | J10 | J11 - J10 inv(J00) J01 | | 0 |      I       |
  //
  // where we have a factorization of J00 and have overwritten the Schur
  // complement J11 - J10 inv(J00) J01 with its inverse. After solving, we
  // undo the diagonal scaling via applying [inv(X); I; inv(tau); 1].
  //
  // We can apply the inverse of our block matrix via sequentially applying
  // the inverse of the block lower triangular, then block upper triangular,
  // matrices. That is, for a right-hand side q, we first apply the
  // inverse of the block lower-triangular matrix via:
  //
  //   q0 := inv(J00) q0,
  //   q1 := q1 - J10 q0,
  //   q1 := inv(J11 - J10 inv(J00) J01) q1.
  //
  // We can then solve against the block upper-triangular matrix via:
  //
  //   q0 := q0 - (inv(J00) J01) q1.
  //
  // This routine should be invoked after having already solved
  //
  //   q0 := inv(J00) q0.
  //
  void ExpandFromAugmentedToHSDAugmented(const Problem<Real>& problem,
                                         BlasMatrixView<Real>* rhs) const;

  // We find the reduction of wx and wy onto wy from the augmented system
  //
  //   S_aug | -Theta,  A' | S_aug | dx | = | wx |
  //         |    A,   R_y |       | dy |   | wy |
  //
  // corresponding to the Schur complement reduction onto the dy variables.
  //
  // This corresponds to overwriting wy with:
  //
  //   wy + (S_y A S_x) inv(S_x Theta S_x) wx =
  //       wy + S_y A inv(S_x Theta) wx =
  //       S_y (inv(S_y) wy + A (inv(S_x Theta) wx)).
  //
  void ReduceFromAugmentedToDualSolution(
      const Problem<Real>& problem, const Iterate<Real>& iterate,
      const ConstBlasMatrixView<Real>& diagonal_scale,
      BlasMatrixView<Real>* rhs) const;

  // We solve for dx given dy using the congruence scaled system
  //
  //   S_aug | -Theta,  A' | S_aug | dx | = | wx |.
  //         |    A,   R_y |       | dy |   | wy |
  //
  // We manipulate the linear constraint
  //
  //   -S_x Theta S_x dx + S_x A' S_y dy = wx
  //
  // to find the assignment
  //
  //   dx = inv(-S_x Theta S_x) (wx - S_x A' S_y dy)
  //      = inv(Theta S_x) (A' (S_y dy) - inv(S_x) wx).
  //
  void ExpandFromDualSolutionToAugmented(
      const Problem<Real>& problem, const Iterate<Real>& iterate,
      const ConstBlasMatrixView<Real>& diagonal_scale,
      BlasMatrixView<Real>* rhs) const;

  void ApplyHSDAugmented(const Problem<Real>& problem, Real alpha,
                         const ConstBlasMatrixView<Real>& hsd_augmented_input,
                         Real beta,
                         BlasMatrixView<Real>* hsd_augmented_output) const;

  void AnalyzeResidualComponents(bool factor_regularization,
                                 const Problem<Real>& problem,
                                 const ConstBlasMatrixView<Real>& solution,
                                 const ConstBlasMatrixView<Real>& rhs) const;

  void SolveHSDAugmentedSystem(const Problem<Real>& problem,
                               const Iterate<Real>& iterate,
                               const ConstBlasMatrixView<Real>& diagonal_scale,
                               bool print_residuals,
                               BlasMatrixView<Real>* hsd_augmented_rhs) const;
};

}  // namespace hsd_qp
}  // namespace conic

#include "conic/hsd_qp/first_order_optimality-impl.hpp"

#endif  // ifndef CONIC_HSD_QP_FIRST_ORDER_OPTIMALITY_H_
