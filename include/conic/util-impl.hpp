/*
 * Copyright (c) 2019 Jack Poulson <jack@hodgestar.com>
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
#ifndef CONIC_UTIL_IMPL_H_
#define CONIC_UTIL_IMPL_H_

#include <exception>
#include <limits>

#include "conic/util.hpp"

namespace conic {

template <typename Real>
void Axpy(Real scale, const ConstBlasMatrixView<Real>& update,
          BlasMatrixView<Real>* state) {
  const Int height = state->height;
  for (Int i = 0; i < height; ++i) {
    state->Entry(i) += scale * update(i);
  }
}

template <typename Real>
Int NumNonpositive(const ConstBlasMatrixView<Real>& slack) {
  Int num_nonpositive = 0;
  for (Int i = 0; i < slack.height; ++i) {
    if (slack(i) <= Real(0)) {
      ++num_nonpositive;
    }
  }
  return num_nonpositive;
}

}  // namespace conic

#endif  // ifndef CONIC_UTIL_IMPL_H_
