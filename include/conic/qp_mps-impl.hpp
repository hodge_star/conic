/*
 * Copyright (c) 2019 Jack Poulson <jack@hodgestar.com>
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
#ifndef CONIC_QP_MPS_IMPL_H_
#define CONIC_QP_MPS_IMPL_H_

#include "conic/bounded_qp/problem.hpp"
#include "conic/bounded_to_standard_qp_reduction.hpp"
#include "conic/imports.hpp"
#include "conic/qp_mps.hpp"

namespace conic {

template <typename Real>
std::ostream& operator<<(std::ostream& os,
                         const QuadraticProgramMPSInfo<Real>& info) {
  // TODO(Jack Poulson)
  return os;
}

template <typename Real>
QuadraticProgramMPSInfo<Real> ReadQuadraticProgramMPS(
    const ReadQuadraticProgramMPSConfig& config,
    bounded_qp::Problem<Real>* problem) {
  auto config_mod = config;
  config_mod.format = BOUNDED_QUADRATIC_PROGRAM;
  mps::Reader<Real> reader(config_mod);
  const QuadraticProgramMPSInfo<Real>& info = reader.Info();

  const auto& variable_map = reader.VariableMap();

  const Int num_primal = variable_map.size() + info.num_artificial_variables;
  const Int num_dual = info.row_section.num_equality_rows;
  const Int num_equality_entries = info.column_section.num_equality_entries;

  // Read in the updates from the file.
  const std::vector<mps::QPEntry<Real>> entries = reader.GetEntries();

  // Form the problem from the updates.
  problem->linear_objective.Resize(num_primal, 1, Real(0));
  problem->quadratic_objective.Resize(num_primal, num_primal);
  problem->quadratic_objective.ReserveEntryAdditions(
      info.quadobj_section.num_entries);
  problem->equality_matrix.Empty();
  problem->equality_matrix.Resize(num_dual, num_primal);
  problem->equality_matrix.ReserveEntryAdditions(num_equality_entries);
  problem->equality_rhs.Resize(num_dual, 1, Real(0));
  for (const auto& entry : entries) {
    if (entry.type == mps::QP_LINEAR_OBJECTIVE) {
      problem->linear_objective(entry.row) = entry.value;
    } else if (entry.type == mps::QP_QUADRATIC_OBJECTIVE) {
      problem->quadratic_objective.QueueEntryAddition(entry.row, entry.column,
                                                      entry.value);
    } else if (entry.type == mps::QP_EQUALITY_MATRIX) {
      problem->equality_matrix.QueueEntryAddition(entry.row, entry.column,
                                                  entry.value);
    } else if (entry.type == mps::QP_EQUALITY_RHS) {
      problem->equality_rhs(entry.row) = entry.value;
    } else if (entry.type == mps::QP_OBJECTIVE_SHIFT) {
      problem->objective_shift = entry.value;
    } else {
      std::cerr << "Invalid queued entry." << std::endl;
    }
  }
  problem->equality_matrix.FlushEntryQueues();
  problem->quadratic_objective.FlushEntryQueues();

  problem->lower_bounds = reader.LowerBounds();
  problem->upper_bounds = reader.UpperBounds();
  problem->num_fixed = info.bounds_section.num_fixed_bounds;

  problem->FillNorms();

  return info;
}

template <typename Real>
QuadraticProgramMPSInfo<Real> ReadQuadraticProgramMPS(
    const ReadQuadraticProgramMPSConfig& config,
    hsd_qp::Problem<Real>* problem) {
  bounded_qp::Problem<Real> bounded_problem;
  auto config_mod = config;
  config_mod.format = BOUNDED_QUADRATIC_PROGRAM;
  auto bounded_info = ReadQuadraticProgramMPS(config_mod, &bounded_problem);

  // We default to only tightening finite bounds (to avoid introducing
  // additional variables).
  const bool tighten_infinite_bounds = false;
  const bool verbose = false;
  bounded_problem.TightenBounds(tighten_infinite_bounds, verbose);

  BoundedToStandardQPReduction<Real> reduction(bounded_problem);
  reduction.FormStandardProblem(problem);

  // TODO(Jack Poulson): Finish updating info.
  auto info = bounded_info;
  info.objective_shift = reduction.ObjectiveShift();

  return info;
}

template <typename Real>
void WriteQuadraticProgramMPS(const WriteQuadraticProgramMPSConfig& config,
                              const bounded_qp::Problem<Real>& problem) {
  // TODO(Jack Poulson)
  std::cerr << "There is not yet support for writing MPS files." << std::endl;
}

template <typename Real>
void WriteQuadraticProgramMPS(const WriteQuadraticProgramMPSConfig& config,
                              const hsd_qp::Problem<Real>& problem) {
  // TODO(Jack Poulson)
  std::cerr << "There is not yet support for writing MPS files." << std::endl;
}

namespace mps {

static const char kBoundsSectionStr[] = "BOUNDS";
static const char kColumnsSectionStr[] = "COLUMNS";
static const char kConstantsSectionStr[] = "CONSTANTS";
static const char kConstraintsSectionStr[] = "CONSTRAINTS";
static const char kEndDataSectionStr[] = "ENDATA";
static const char kGroupsSectionStr[] = "GROUPS";
static const char kHessianSectionStr[] = "HESSIAN";
static const char kMarkerSectionStr[] = "MARKER";
static const char kNameSectionStr[] = "NAME";
static const char kObjSenseSectionStr[] = "OBJSENSE";
static const char kQSectionSectionStr[] = "QSECTION";
static const char kQuadObjSectionStr[] = "QUADOBJ";
static const char kQuadraticSectionStr[] = "QUADRATIC";
static const char kQuadsSectionStr[] = "QUADS";
static const char kRangesSectionStr[] = "RANGES";
static const char kRHSSectionStr[] = "RHS";
static const char kRHSPrimeSectionStr[] = "RHS'";
static const char kRowsSectionStr[] = "ROWS";
static const char kSOSSectionStr[] = "SOS";
static const char kVariablesSectionStr[] = "VARIABLES";

static const char kFixedStr[] = "FX";
static const char kFreeStr[] = "FR";
static const char kLowerBoundStr[] = "LO";
static const char kNonnegativeStr[] = "PL";
static const char kNonpositiveStr[] = "MI";
static const char kUpperBoundStr[] = "UP";

static const char kEqualityRowStr[] = "E";
static const char kGreaterRowStr[] = "G";
static const char kLesserRowStr[] = "L";
static const char kNonconstrainingRowStr[] = "N";

// Returns a replacement of the input token with all uppercase characters.
std::string ToUpper(const std::string& token) {
  std::string upper_token = token;
  std::transform(upper_token.begin(), upper_token.end(), upper_token.begin(),
                 ::toupper);
  return upper_token;
}

template <typename T>
bool ExtractToken(std::stringstream& stream, T& token,
                  const std::string& error_msg) {
  if (!(stream >> token)) {
    std::cerr << error_msg << std::endl;
    return false;
  } else {
    return true;
  }
}

template <typename T>
bool ExtractToken(std::stringstream& stream, T& token) {
  return static_cast<bool>(stream >> token);
}

template <typename Real>
bool Reader<Real>::IsCommentLine(std::stringstream& line_stream) {
  const char first_char = line_stream.peek();
  return first_char == '*' || first_char == '#';
}

template <typename Real>
bool Reader<Real>::IsDataLine(std::stringstream& line_stream) {
  const char first_char = line_stream.peek();
  return first_char == ' ' || first_char == '\t';
}

template <typename Real>
Section Reader<Real>::DecodeSection(const std::string& token) {
  Section section;
  const std::string upper_token = ToUpper(token);
  if (upper_token == kBoundsSectionStr) {
    section = MPS_BOUNDS;
  } else if (upper_token == kColumnsSectionStr ||
             upper_token == kVariablesSectionStr) {
    section = MPS_COLUMNS;
  } else if (upper_token == kEndDataSectionStr) {
    section = MPS_END;
  } else if (upper_token == kMarkerSectionStr) {
    section = MPS_MARKER;
  } else if (upper_token == kNameSectionStr) {
    section = MPS_NAME;
  } else if (upper_token == kObjSenseSectionStr) {
    section = MPS_OBJSENSE;
  } else if (upper_token == kHessianSectionStr ||
             upper_token == kQSectionSectionStr ||
             upper_token == kQuadObjSectionStr ||
             upper_token == kQuadraticSectionStr ||
             upper_token == kQuadsSectionStr) {
    section = MPS_QUADOBJ;
  } else if (upper_token == kRangesSectionStr) {
    section = MPS_RANGES;
  } else if (upper_token == kConstantsSectionStr ||
             upper_token == kRHSSectionStr ||
             upper_token == kRHSPrimeSectionStr) {
    section = MPS_RHS;
  } else if (upper_token == kConstraintsSectionStr ||
             upper_token == kGroupsSectionStr ||
             upper_token == kRowsSectionStr) {
    section = MPS_ROWS;
  } else if (upper_token == kSOSSectionStr) {
    section = MPS_SOS;
  } else {
    std::cerr << "No valid section was decoded." << std::endl;
    section = MPS_NONE;
  }
  return section;
}

template <typename Real>
RowType Reader<Real>::DecodeRowType(const std::string& token) {
  RowType row_type;
  const std::string upper_token = ToUpper(token);
  if (upper_token == kLesserRowStr) {
    row_type = MPS_LESSER_ROW;
  } else if (upper_token == kGreaterRowStr) {
    row_type = MPS_GREATER_ROW;
  } else if (upper_token == kEqualityRowStr) {
    row_type = MPS_EQUALITY_ROW;
  } else if (upper_token == kNonconstrainingRowStr) {
    row_type = MPS_NONCONSTRAINING_ROW;
  } else {
    std::cerr << "No valid row type was decoded." << std::endl;
    row_type = MPS_NONCONSTRAINING_ROW;
  }
  return row_type;
}

template <typename Real>
void Reader<Real>::ProcessRowDataLine(const std::string& line) {
  std::stringstream stream(line);
  const std::string kStreamError = "Couldn't process row data line";

  std::string row_type;
  if (!ExtractToken(stream, row_type, kStreamError)) {
    return;
  }

  std::string row_name;
  if (!ExtractToken(stream, row_name, kStreamError)) {
    return;
  }

  std::string token;
  if (ExtractToken(stream, token)) {
    std::cerr << "Too many row data line tokens in: " << line << std::endl;
    return;
  }

  // We will set all but the 'type_index' field, as it is occasionally the case
  // (e.g., in tuff.mps) that rows can be empty.
  RowMetadata row_metadata;
  row_metadata.type = DecodeRowType(row_type);
  row_map_[row_name] = row_metadata;

  if (row_metadata.type == MPS_NONCONSTRAINING_ROW) {
    if (info_.row_section.num_nonconstraining_rows == 1) {
      info_.row_section.cost_name = row_name;
    }
  } else if (row_metadata.type != MPS_LESSER_ROW &&
             row_metadata.type != MPS_GREATER_ROW &&
             row_metadata.type != MPS_EQUALITY_ROW) {
    std::cerr << "Invalid row token string." << std::endl;
  }
}

template <typename Real>
void Reader<Real>::ProcessColumnDataLine(const std::string& line) {
  std::stringstream stream(line);
  const std::string kStreamError = "Couldn't process column data line.";

  std::string variable_name;
  ExtractToken(stream, variable_name, kStreamError);

  auto variable_iter = variable_map_.find(variable_name);
  if (variable_iter == variable_map_.end()) {
    VariableMetadata<Real> variable_metadata;
    variable_map_[variable_name] = variable_metadata;
    variable_iter = variable_map_.find(variable_name);
  }
  auto& variable_metadata = variable_iter->second;

  // We do not yet know if there are one or two pairs of entries to read.
  for (Int pair = 0; pair < 2; ++pair) {
    std::string row_name;
    if (!(stream >> row_name)) {
      if (pair == 0) {
        std::cerr << kStreamError << std::endl;
      } else {
        break;
      }
    }

    double value_double;
    ExtractToken(stream, value_double, kStreamError);
    if (value_double == 0.) {
      continue;
    }
    ++variable_metadata.num_nonzeros;

    auto row_iter = row_map_.find(row_name);
    if (row_iter == row_map_.end()) {
      std::cerr << "Could not find row " << row_name << std::endl;
      break;
    }
    auto& row_metadata = row_iter->second;
    ++row_metadata.num_nonzeros;
  }
}

template <typename Real>
void Reader<Real>::ProcessQuadraticObjectiveDataLine(const std::string& line) {
  std::stringstream stream(line);
  const std::string kStreamError =
      "Couldn't process quadratic objective data line.";

  std::string row_name;
  ExtractToken(stream, row_name, kStreamError);

  std::string column_name;
  ExtractToken(stream, column_name, kStreamError);

  // There should be one entry to read.
  double value_double;
  ExtractToken(stream, value_double, kStreamError);
  if (value_double == 0.) {
    return;
  }

  if (row_name == column_name) {
    // This is a nonzero diagonal entry.
    info_.quadobj_section.num_entries += 1;
  } else {
    // This is a nonzero off-diagonal entry, so we will also contribute its
    // transpose.
    info_.quadobj_section.num_entries += 2;
  }
}

template <typename Real>
void Reader<Real>::QueueColumnDataLine(
    const std::string& line, std::vector<QPEntry<Real>>* queued_entries) {
  std::stringstream stream(line);
  const std::string kStreamError = "Could not process COLUMNS section.";

  std::string variable_name;
  ExtractToken(stream, variable_name, kStreamError);

  auto variable_iter = variable_map_.find(variable_name);
  if (variable_iter == variable_map_.end()) {
    std::cerr << "Could not find variable " << variable_name << std::endl;
  }
  const auto& variable_metadata = variable_iter->second;

  const Int column = variable_metadata.index;
  CONIC_ASSERT(
      column < Int(variable_map_.size()) + info_.num_artificial_variables,
      "QueueColumnDataLine column was " + std::to_string(column));

  // There should be either one or two pairs of entries left to read.
  for (Int pair = 0; pair < 2; ++pair) {
    std::string row_name;
    if (!(stream >> row_name)) {
      if (pair == 0) {
        std::cerr << kStreamError << std::endl;
      } else {
        break;
      }
    }

    double column_value_double;
    ExtractToken(stream, column_value_double, kStreamError);
    const Real column_value = column_value_double;

    QPEntry<Real> entry;
    auto row_iter = row_map_.find(row_name);
    if (row_iter == row_map_.end()) {
      auto trivial_equality_iter = trivial_equality_map_.find(row_name);
      if (trivial_equality_iter == trivial_equality_map_.end()) {
        std::cerr << "Could not find row " << row_name << std::endl;
      }

      if (column_value == Real(0)) {
        std::cerr << "Trivial rows are not yet allowed to have their only "
                     "specified 'nonzero' value be zero."
                  << std::endl;
      }
      if (config_.verbose) {
        std::cout << "Trivial equality row " << row_name
                  << " only had a nonzero in column " << variable_name
                  << std::endl;
      }

      auto& trivial_data = trivial_equality_iter->second;
      trivial_data.variable_name = variable_name;
      trivial_data.single_nonzero = column_value;
      continue;
    }

    const auto& row_metadata = row_iter->second;
    if (row_metadata.type == MPS_EQUALITY_ROW) {
      // Handle the assignment A(row, column) = column_value.
      const Int row = row_metadata.type_index;
      entry.type = QP_EQUALITY_MATRIX;
      entry.row = row;
      entry.column = column;
      entry.value = column_value;
      queued_entries->push_back(entry);
    } else if (row_metadata.type == MPS_LESSER_ROW) {
      // Handle the assignment G(row, column) = column_value.
      const Int row = row_metadata.type_index;
      entry.type = QP_CONE_MATRIX;
      entry.row = row;
      entry.column = column;
      entry.value = column_value;
      queued_entries->push_back(entry);
    } else if (row_metadata.type == MPS_GREATER_ROW) {
      // Handle the assignment G(row, column) = -column_value.
      const Int row =
          info_.row_section.num_lesser_rows + row_metadata.type_index;
      entry.type = QP_CONE_MATRIX;
      entry.row = row;
      entry.column = column;
      entry.value = -column_value;
      queued_entries->push_back(entry);
    } else if (row_metadata.type == MPS_NONCONSTRAINING_ROW) {
      if (row_metadata.type_index == 0) {
        // c(column) = column_value
        entry.type = QP_LINEAR_OBJECTIVE;
        entry.row = column;
        entry.column = 0;
        entry.value = config_.minimize ? column_value : -column_value;
        queued_entries->push_back(entry);
      }
    }
  }
}

template <typename Real>
void Reader<Real>::QueueQuadraticObjectiveDataLine(
    const std::string& line, std::vector<QPEntry<Real>>* queued_entries) {
  std::stringstream stream(line);
  const std::string kStreamError = "Could not process QUADOBJ section.";

  std::string row_variable_name;
  ExtractToken(stream, row_variable_name, kStreamError);
  auto row_variable_iter = variable_map_.find(row_variable_name);
  if (row_variable_iter == variable_map_.end()) {
    std::cerr << "Could not find row variable " << row_variable_name
              << std::endl;
  }
  const auto& row_variable_metadata = row_variable_iter->second;
  const Int row = row_variable_metadata.index;

  std::string column_variable_name;
  ExtractToken(stream, column_variable_name, kStreamError);
  auto column_variable_iter = variable_map_.find(column_variable_name);
  if (column_variable_iter == variable_map_.end()) {
    std::cerr << "Could not find column variable " << column_variable_name
              << std::endl;
  }
  const auto& column_variable_metadata = column_variable_iter->second;
  const Int column = column_variable_metadata.index;

  // There should be one entry to read.
  double value_double;
  ExtractToken(stream, value_double, kStreamError);
  if (value_double == 0.) {
    return;
  }
  const Real value = value_double;

  QPEntry<Real> entry;
  entry.type = QP_QUADRATIC_OBJECTIVE;
  entry.row = row;
  entry.column = column;
  entry.value = value;
  queued_entries->push_back(entry);

  if (row != column) {
    // Also contribute the transposed version of off-diagonal entries.
    entry.row = column;
    entry.column = row;
    queued_entries->push_back(entry);
  }
}

template <typename Real>
void Reader<Real>::ProcessRHSDataLine(const std::string& line) {
  const std::string kStreamError = "Invalid 'RHS' section.";

  if (!initialized_rhs_section_) {
    std::stringstream test_stream(line);

    Int num_tokens = 0;
    std::string token;
    while (test_stream >> token) {
      ++num_tokens;
    }

    if (num_tokens == 2 || num_tokens == 4) {
      // There were either one or two pairs with no name.
      rhs_has_name_ = false;
      info_.rhs_section.num_rhs = 1;
    } else if (num_tokens == 3 || num_tokens == 5) {
      // There was either one or two pairs with a name.
      rhs_has_name_ = true;
    } else {
      std::cerr << kStreamError << std::endl;
    }
    initialized_rhs_section_ = true;
  }

  std::stringstream stream(line);
  if (rhs_has_name_) {
    std::string rhs_name_candidate;
    ExtractToken(stream, rhs_name_candidate, kStreamError);

    if (info_.rhs_section.num_rhs == 0) {
      // It should currently be the case that rhs_name is empty.
      info_.rhs_section.rhs_name = rhs_name_candidate;
      info_.rhs_section.num_rhs = 1;
    } else if (rhs_name_candidate != info_.rhs_section.rhs_name) {
      // Only single problem instances are currently supported, so we choose
      // the first one.
      return;
    }
  }

  // There should be either one or two pairs of entries left to read.
  for (Int pair = 0; pair < 2; ++pair) {
    std::string row_name;
    if (!(stream >> row_name)) {
      if (pair == 0) {
        std::cerr << kStreamError << std::endl;
      } else {
        break;
      }
    }

    // We will store the RHS values later.
    double value_double;
    ExtractToken(stream, value_double, kStreamError);
  }
}

template <typename Real>
void Reader<Real>::QueueRHSDataLine(
    const std::string& line, std::vector<QPEntry<Real>>* queued_entries) {
  std::stringstream stream(line);
  const std::string kStreamError = "Invalid 'RHS' section.";

  if (rhs_has_name_) {
    // We just burn the name token, as we already stored it.
    std::string token;
    ExtractToken(stream, token, kStreamError);
  }

  // There should be either one or two pairs of entries left to read.
  for (Int pair = 0; pair < 2; ++pair) {
    std::string row_name;
    if (!(stream >> row_name)) {
      if (pair == 0) {
        std::cerr << kStreamError << std::endl;
      } else {
        break;
      }
    }

    double rhs_value_double;
    ExtractToken(stream, rhs_value_double, kStreamError);
    const Real rhs_value = rhs_value_double;

    auto row_iter = row_map_.find(row_name);
    if (row_iter == row_map_.end()) {
      if (empty_lesser_rows_.count(row_name)) {
        if (rhs_value < Real(0)) {
          std::cerr << "Row " << row_name
                    << " had an infeasible trivial upper bound of " << rhs_value
                    << std::endl;
        } else {
          if (config_.verbose) {
            std::cout << "Skipping trivial lesser row " << row_name
                      << " which has upper bound " << rhs_value << std::endl;
          }
        }
        continue;
      } else if (empty_greater_rows_.count(row_name)) {
        if (rhs_value > Real(0)) {
          std::cerr << "Row " << row_name
                    << " had an infeasible trivial lower bound of " << rhs_value
                    << std::endl;
        } else {
          if (config_.verbose) {
            std::cout << "Skipping trivial greater row " << row_name
                      << " which has upper bound " << rhs_value << std::endl;
          }
        }
        continue;
      }

      auto trivial_equality_iter = trivial_equality_map_.find(row_name);
      if (trivial_equality_iter == trivial_equality_map_.end()) {
        std::cerr << "Could not find trivial row " << row_name << std::endl;
      }
      const auto& trivial_data = trivial_equality_iter->second;

      auto variable_iter = variable_map_.find(trivial_data.variable_name);
      if (variable_iter == variable_map_.end()) {
        std::cerr << "Could not find variable " << trivial_data.variable_name
                  << " for trivial equality from row " << row_name << std::endl;
      }
      variable_iter->second.fixed_value =
          rhs_value / trivial_data.single_nonzero;
      continue;
    }

    const auto& row_metadata = row_iter->second;

    if (row_metadata.has_range_modifier) {
      // Fill in the explicit bound for the RANGE and queue two-sided bound
      // later.
      auto& range_data = range_list_[row_metadata.range_index];
      if (range_data.rhs_is_lower_bound) {
        range_data.lower_bound = rhs_value;
        range_data.upper_bound = rhs_value + range_data.range_size;
      } else {
        range_data.upper_bound = rhs_value;
        range_data.lower_bound = rhs_value - range_data.range_size;
      }
      continue;
    }

    if (row_metadata.type == MPS_EQUALITY_ROW) {
      if (row_metadata.converted_greater_row ||
          row_metadata.converted_lesser_row) {
        const Int num_nonfixed_variables =
            variable_map_.size() - info_.bounds_section.num_fixed_bounds;
        const Int column = num_nonfixed_variables + range_list_.size() +
                           row_metadata.converted_inequality_index;

        // Store the original bound.
        if (row_metadata.converted_greater_row) {
          lower_bounds_[column] = rhs_value;
        } else {
          upper_bounds_[column] = rhs_value;
        }
      } else {
        // b(row) = rhs_value
        const Int row = row_metadata.type_index;
        QPEntry<Real> entry;
        entry.type = QP_EQUALITY_RHS;
        entry.row = row;
        entry.column = 0;
        entry.value = rhs_value;
        queued_entries->push_back(entry);
      }
    } else if (row_metadata.type == MPS_LESSER_ROW) {
      // h(row) = rhs_value
      const Int row = row_metadata.type_index;
      QPEntry<Real> entry;
      entry.type = QP_CONE_RHS;
      entry.row = row;
      entry.column = 0;
      entry.value = rhs_value;
      queued_entries->push_back(entry);
    } else if (row_metadata.type == MPS_GREATER_ROW) {
      // h(row) = -rhs_value
      const Int row =
          info_.row_section.num_lesser_rows + row_metadata.type_index;
      QPEntry<Real> entry;
      entry.type = QP_CONE_RHS;
      entry.row = row;
      entry.column = 0;
      entry.value = -rhs_value;
      queued_entries->push_back(entry);
    } else if (row_metadata.type == MPS_NONCONSTRAINING_ROW) {
      // It seems that the convention is to *subtract*, rather than *add*
      // the RHS-specified objective shift value. We therefore negate the
      // read value.
      QPEntry<Real> entry;
      entry.type = QP_OBJECTIVE_SHIFT;
      entry.row = -1;
      entry.column = -1;
      entry.value = -rhs_value;
      queued_entries->push_back(entry);
      info_.objective_shift += entry.value;
    }
  }
}

template <typename Real>
void Reader<Real>::ProcessBoundsDataLine(const std::string& line) {
  const std::string kStreamError = "Invalid 'BOUNDS' section.";
  if (!initialized_bounds_section_) {
    std::stringstream test_stream(line);

    std::string bound_mark;
    ExtractToken(test_stream, bound_mark, kStreamError);

    std::string token;
    ExtractToken(test_stream, token, kStreamError);

    Int num_tokens = 2;
    std::string rhs_token;
    while (test_stream >> rhs_token) {
      ++num_tokens;
    }

    const std::string upper_bound_mark = ToUpper(bound_mark);
    if (upper_bound_mark == kLowerBoundStr ||
        upper_bound_mark == kUpperBoundStr || upper_bound_mark == kFixedStr) {
      if (num_tokens == 4) {
        bounds_has_name_ = true;
      } else if (num_tokens == 3) {
        bounds_has_name_ = false;
      } else {
        std::cerr << "Invalid " << bound_mark << " in 'BOUNDS' line"
                  << std::endl;
      }
    } else if (upper_bound_mark == kFreeStr ||
               upper_bound_mark == kNonpositiveStr ||
               upper_bound_mark == kNonnegativeStr) {
      if (num_tokens == 3) {
        bounds_has_name_ = true;
      } else if (num_tokens == 2) {
        bounds_has_name_ = false;
      } else {
        std::cerr << "Invalid " << bound_mark << " in 'BOUNDS' line"
                  << std::endl;
      }
    } else {
      std::cerr << "Unknown bound mark " << bound_mark << std::endl;
    }

    if (bounds_has_name_) {
      info_.bounds_section.bounds_name = token;
    }
    initialized_bounds_section_ = true;
  }

  std::stringstream stream(line);

  // We already have the first token of a bounding row, which should be of the
  // same general form as
  //
  //   FX BOUNDROW VARIABLENAME 1734.
  //
  // in the case of 'VARIABLENAME' being fixed ('FX') at the value 1734
  // (with this problem's bound name being 'BOUNDROW').
  std::string bound_mark;
  ExtractToken(stream, bound_mark, kStreamError);
  if (bounds_has_name_) {
    std::string bounds_name_candidate;
    ExtractToken(stream, bounds_name_candidate, kStreamError);

    if (info_.bounds_section.bounds_name != bounds_name_candidate) {
      // Only single problem instances are currently supported, so we choose
      // the first one.
      return;
    }
  }

  std::string variable_name;
  ExtractToken(stream, variable_name, kStreamError);

  auto variable_iter = variable_map_.find(variable_name);
  if (variable_iter == variable_map_.end()) {
    std::cerr << "Invalid 'BOUNDS' section for " << variable_name << std::endl;
  }
  auto& data = variable_iter->second;

  const std::string upper_bound_mark = ToUpper(bound_mark);
  if (upper_bound_mark == kUpperBoundStr) {
    double value_double;
    ExtractToken(stream, value_double, kStreamError);
    if (data.upper_bounded) {
      data.upper_bound = std::min(data.upper_bound, Real(value_double));
    } else {
      data.upper_bound = value_double;
    }
    data.upper_bounded = true;
  } else if (upper_bound_mark == kLowerBoundStr) {
    double value_double;
    ExtractToken(stream, value_double, kStreamError);
    if (data.lower_bounded) {
      data.lower_bound = std::max(data.lower_bound, Real(value_double));
    } else {
      data.lower_bound = value_double;
    }
    data.lower_bounded = true;
  } else if (upper_bound_mark == kFixedStr) {
    double value_double;
    ExtractToken(stream, value_double, kStreamError);
    data.fixed = true;
    data.fixed_value = value_double;
  } else if (upper_bound_mark == kFreeStr) {
    data.free = true;
  } else if (upper_bound_mark == kNonpositiveStr) {
    if (data.upper_bounded) {
      data.upper_bound = std::min(data.upper_bound, Real(0));
    } else {
      data.upper_bound = 0;
    }
    data.upper_bounded = true;
  } else if (upper_bound_mark == kNonnegativeStr) {
    if (data.lower_bounded) {
      data.lower_bound = std::max(data.lower_bound, Real(0));
    } else {
      data.lower_bound = 0;
    }
    data.lower_bounded = true;
  } else {
    std::cerr << kStreamError << std::endl;
  }
}

template <typename Real>
void Reader<Real>::ProcessRangesDataLine(const std::string& line) {
  const std::string kStreamError = "Invalid 'RANGES' section.";
  if (!initialized_ranges_section_) {
    std::stringstream test_stream(line);
    Int num_tokens = 0;
    std::string ranges_token;
    while (test_stream >> ranges_token) {
      ++num_tokens;
    }

    if (num_tokens == 2 || num_tokens == 4) {
      ranges_has_name_ = false;
      info_.range_section.num_ranges = 1;
    } else if (num_tokens == 3 || num_tokens == 5) {
      ranges_has_name_ = true;
    } else {
      std::cerr << kStreamError << std::endl;
    }
    initialized_ranges_section_ = true;
  }

  std::stringstream stream(line);
  if (ranges_has_name_) {
    std::string range_name_candidate;
    ExtractToken(stream, range_name_candidate, kStreamError);
    if (info_.range_section.num_ranges == 0) {
      info_.range_section.range_name = range_name_candidate;
      info_.range_section.num_ranges = 1;
    } else if (range_name_candidate != info_.range_section.range_name) {
      // Only single problem instances are currently supported, so we choose
      // the first one.
      return;
    }
  }

  // There should be either one or two pairs of entries left to read.
  for (Int pair = 0; pair < 2; ++pair) {
    std::string row_name;
    if (!(stream >> row_name)) {
      if (pair == 0) {
        std::cerr << kStreamError << std::endl;
      } else {
        break;
      }
    }

    double value_double;
    ExtractToken(stream, value_double, kStreamError);
    const Real value = value_double;
    if (value == Real(0)) {
      std::cerr << "Tried to force an empty RANGE." << std::endl;
    }

    auto row_iter = row_map_.find(row_name);
    if (row_iter == row_map_.end()) {
      std::cerr << "Could not find row " << row_name << std::endl;
    }

    RangeData<Real> range_data;
    range_data.row_name = row_name;
    range_data.range_size = std::abs(value);

    auto& row_metadata = row_iter->second;
    if (row_metadata.type == MPS_EQUALITY_ROW) {
      range_data.rhs_is_lower_bound = value > Real(0);
    } else if (row_metadata.type == MPS_GREATER_ROW) {
      range_data.rhs_is_lower_bound = true;
    } else if (row_metadata.type == MPS_LESSER_ROW) {
      range_data.rhs_is_lower_bound = false;
    } else {
      std::cerr << "Tried to apply a RANGE modifier to an invalid row type."
                << std::endl;
    }

    range_list_.push_back(range_data);

    row_metadata.type = MPS_EQUALITY_ROW;
    row_metadata.has_range_modifier = true;

    // We will set the row index by iterating through the range data list and
    // either deleting entries that had deleted rows or assigning the
    // appropriate index when they exist.
  }
}

template <typename Real>
void Reader<Real>::QueueVariableBounds(
    std::vector<QPEntry<Real>>* queued_entries) {
  CONIC_ASSERT(config_.format == BOUNDED_QUADRATIC_PROGRAM, "Invalid format");
  for (const auto& variable_constraint_iter : variable_map_) {
    const auto& data = variable_constraint_iter.second;
    if (data.upper_bounded) {
      upper_bounds_[data.index] = data.upper_bound;
    }
    if (data.lower_bounded) {
      lower_bounds_[data.index] = data.lower_bound;
    }
    if (data.fixed) {
      lower_bounds_[data.index] = data.fixed_value;
      upper_bounds_[data.index] = data.fixed_value;
    }
  }

  for (Int range_list_index = 0; range_list_index < Int(range_list_.size());
       ++range_list_index) {
    const Int num_nonfixed_variables =
        variable_map_.size() - info_.bounds_section.num_fixed_bounds;
    const Int column = num_nonfixed_variables + range_list_index;
    CONIC_ASSERT(
        column < Int(variable_map_.size()) + info_.num_artificial_variables,
        "Column in QueueVariableBounds ranges section was " +
            std::to_string(column));
    const auto& data = range_list_[range_list_index];

    QPEntry<Real> entry;

    // Introduce the auxiliary variable 'zeta' to directly apply the lower and
    // upper bounds to by transforming the inequality
    //
    //   lower_bound <= g x <= upper_bound
    //
    // into
    //
    //   [g, -1] [x; zeta] = 0,
    //   lower_bound <= zeta <= upper_bound.
    //
    // We need only add a single entry of -1 into the equality matrix.
    {
      const auto row_iter = row_map_.find(data.row_name);
      if (row_iter == row_map_.end()) {
        std::cerr << "Could not link range data to row map." << std::endl;
      }

      // A(row, column) = -1
      const Int row = row_iter->second.type_index;
      entry.type = QP_EQUALITY_MATRIX;
      entry.row = row;
      entry.column = column;
      entry.value = -1;
      queued_entries->push_back(entry);
    }

    if (data.lower_bound == -std::numeric_limits<Real>::infinity()) {
      // There was no RHS to set the datum for the range, so we default to zero.
      if (data.rhs_is_lower_bound) {
        lower_bounds_[column] = 0;
        upper_bounds_[column] = data.range_size;
      } else {
        lower_bounds_[column] = -data.range_size;
        upper_bounds_[column] = 0;
      }
    } else {
      lower_bounds_[column] = data.lower_bound;
      upper_bounds_[column] = data.upper_bound;
    }
  }

  for (const auto& row_iter : row_map_) {
    const auto& row_metadata = row_iter.second;
    if (row_metadata.type != MPS_EQUALITY_ROW) {
      continue;
    }
    if (!row_metadata.converted_greater_row &&
        !row_metadata.converted_lesser_row) {
      continue;
    }

    const Int row = row_metadata.type_index;
    const Int num_nonfixed_variables =
        variable_map_.size() - info_.bounds_section.num_fixed_bounds;
    const Int column = num_nonfixed_variables + range_list_.size() +
                       row_metadata.converted_inequality_index;
    CONIC_ASSERT(
        column < Int(variable_map_.size()) + info_.num_artificial_variables,
        "Column in QueueVariableBounds converted section was " +
            std::to_string(column));

    // We transform a x >= l or a x <= u into a x = zeta and either l <= zeta
    // or zeta <= u.
    QPEntry<Real> entry;
    entry.type = QP_EQUALITY_MATRIX;
    entry.row = row;
    entry.column = column;
    entry.value = Real(-1);
    queued_entries->push_back(entry);

    if (row_metadata.converted_greater_row) {
      if (!std::isfinite(lower_bounds_[column])) {
        // No value has been filled in yet, so default to zero.
        lower_bounds_[column] = 0;
      }
    } else {
      if (!std::isfinite(upper_bounds_[column])) {
        // No value has been filled in yet, so default to zero.
        upper_bounds_[column] = 0;
      }
    }
  }
}

template <typename Real>
void Reader<Real>::PeekAtRanges(const std::string& section_token) {
  std::ifstream::pos_type pos = file_.tellg();
  auto section = DecodeSection(section_token);

  std::string token, line, row_name;
  while (std::getline(file_, line)) {
    if (line.empty()) {
      continue;
    }
    std::stringstream stream(line);
    if (IsCommentLine(stream)) {
      continue;
    }
    const bool is_data_line = IsDataLine(stream);

    // The first token on each line should be a string. We will check it for
    // equivalence with each section string.
    if (!(stream >> token)) {
      // This line only consists of whitespace.
      continue;
    }

    if (!is_data_line) {
      section = DecodeSection(token);
      if (section == MPS_END) {
        break;
      }
      continue;
    }

    if (section != MPS_RANGES) {
      continue;
    }

    ProcessRangesDataLine(line);
  }

  file_.seekg(pos);
}

template <typename Real>
void Reader<Real>::SimplifyRowTypes() {
  for (auto iter = row_map_.begin(); iter != row_map_.end();) {
    auto& row_metadata = iter->second;
    if (row_metadata.num_nonzeros == 0) {
      if (row_metadata.type == MPS_NONCONSTRAINING_ROW) {
        std::cerr << "Objective entry " << iter->first << " was empty."
                  << std::endl;
        row_metadata.type_index = 0;
        ++iter;
      } else {
        if (row_metadata.type == MPS_EQUALITY_ROW) {
          if (config_.verbose) {
            std::cout << "Deleting empty equality row " << iter->first
                      << std::endl;
          }
        } else if (row_metadata.type == MPS_GREATER_ROW) {
          empty_greater_rows_.insert(iter->first);
          if (config_.verbose) {
            std::cout << "Deleting empty greater row " << iter->first
                      << std::endl;
          }
        } else if (row_metadata.type == MPS_LESSER_ROW) {
          empty_lesser_rows_.insert(iter->first);
          if (config_.verbose) {
            std::cout << "Deleting empty lesser row " << iter->first
                      << std::endl;
          }
        } else {
          std::cerr << "Unknown empty row type for " << iter->first
                    << std::endl;
        }

        // Delete this entry.
        iter = row_map_.erase(iter);
      }
    } else if (row_metadata.num_nonzeros == 1 &&
               row_metadata.type == MPS_EQUALITY_ROW &&
               !row_metadata.has_range_modifier) {
      // Convert to a fixed value. We will divide the corresponding RHS value
      // by the single nonzero.
      TrivialEqualityData<Real> trivial_equality;
      trivial_equality_map_[iter->first] = trivial_equality;
      iter = row_map_.erase(iter);

      // TODO(Jack Poulson): Exploit the fact that a trivial equality row that
      // involves a RANGE modifier would imply that the auxiliary variable is
      // proportional to the single nonzero column index's variable, and so we
      // could instead apply the (scaled) range requirements on said variable.
      // But perhaps this is rare enough to not necessitate special handling.
    } else {
      if (row_metadata.type == MPS_EQUALITY_ROW) {
        row_metadata.type_index = info_.row_section.num_equality_rows++;
      } else if (row_metadata.type == MPS_GREATER_ROW) {
        row_metadata.type = MPS_EQUALITY_ROW;
        row_metadata.type_index = info_.row_section.num_equality_rows++;
        row_metadata.converted_greater_row = true;
        row_metadata.converted_inequality_index =
            num_converted_inequality_rows_++;
        // We will add a nonzero for introducing the artificial variable.
        ++row_metadata.num_nonzeros;
      } else if (row_metadata.type == MPS_LESSER_ROW) {
        row_metadata.type = MPS_EQUALITY_ROW;
        row_metadata.type_index = info_.row_section.num_equality_rows++;
        row_metadata.converted_lesser_row = true;
        row_metadata.converted_inequality_index =
            num_converted_inequality_rows_++;
        // We will add a nonzero for introducing the artificial variable.
        ++row_metadata.num_nonzeros;
      } else if (row_metadata.type == MPS_NONCONSTRAINING_ROW) {
        row_metadata.type_index = 0;
      } else {
        std::cerr << "Unknown row type." << std::endl;
      }

      ++iter;
    }
  }
}

template <typename Real>
void Reader<Real>::SyncRangeData() {
  Int range_index = 0;
  for (auto iter = range_list_.begin(); iter != range_list_.end();) {
    // Attempt to find the row data associated with this range modifier.
    auto row_iter = row_map_.find(iter->row_name);
    if (row_iter == row_map_.end()) {
      // The row was deleted (due to not having any nonzeros). We will therefore
      // delete this member of the range list.
      iter = range_list_.erase(iter);
    } else {
      row_iter->second.range_index = range_index++;
      ++iter;
    }
  }
}

template <typename Real>
void Reader<Real>::StoreTrivialEqualityRowVariableNames(
    const std::ifstream::pos_type& column_section_beg) {
  auto pos = file_.tellg();
  file_.seekg(column_section_beg);

  std::string column_line, column_token;
  while (std::getline(file_, column_line)) {
    if (column_line.empty()) {
      continue;
    }
    std::stringstream column_stream(column_line);
    if (IsCommentLine(column_stream)) {
      continue;
    }
    const bool is_column_line = IsDataLine(column_stream);
    if (!is_column_line) {
      break;
    }

    std::string variable_name;
    if (!(column_stream >> variable_name)) {
      continue;
    }

    auto variable_iter = variable_map_.find(variable_name);
    if (variable_iter == variable_map_.end()) {
      std::cerr << "Invalid column token." << std::endl;
    }
    auto& variable_metadata = variable_iter->second;

    for (Int pair = 0; pair < 2; ++pair) {
      std::string row_name;
      if (!(column_stream >> row_name)) {
        if (pair == 0) {
          std::cerr << "Too few column tokens." << std::endl;
        } else {
          break;
        }
      }

      double value_double;
      if (!(column_stream >> value_double)) {
        std::cerr << "Too many column tokens." << std::endl;
      }
      const Real value = value_double;

      auto trivial_equality_iter = trivial_equality_map_.find(row_name);
      if (trivial_equality_iter == trivial_equality_map_.end()) {
        continue;
      }
      if (value == Real(0)) {
        std::cerr << "Trivial equality rows with a zero 'nonzero' are not yet "
                  << "supported." << std::endl;
      }
      auto& trivial_data = trivial_equality_iter->second;
      trivial_data.variable_name = variable_name;
      trivial_data.single_nonzero = value;
      if (config_.verbose) {
        std::cout << "Storing nonzero of " << value << " for " << variable_name
                  << " for trivial equality row " << row_name << std::endl;
      }

      // We initialize at zero and overwrite if there is relevant RHS data.
      variable_metadata.fixed = true;
      variable_metadata.fixed_value = 0;
    }
  }
  file_.seekg(pos);
}

template <typename Real>
bool Reader<Real>::FinalizeBoundTypes() {
  bool consistent_bounds = true;

  for (auto& entry : variable_map_) {
    auto& data = entry.second;

    // Handle explicit upper bounds.
    if (data.upper_bounded) {
      if (data.fixed) {
        if (data.fixed_value > data.upper_bound) {
          consistent_bounds = false;
          std::cerr << "Infeasible fixed value of " << data.fixed_value
                    << " and upper bound of " << data.upper_bound << "."
                    << std::endl;
        }
        data.upper_bounded = false;
      } else if (data.lower_bounded) {
        if (data.lower_bound < data.upper_bound) {
          // This should be the standard case.
        } else if (data.lower_bound == data.upper_bound) {
          // We will instead fix the variable.
          data.upper_bounded = false;
          data.lower_bounded = false;
          data.fixed = true;
          data.fixed_value = data.upper_bound;
          if (config_.verbose) {
            std::cout << "Fixing " << entry.first
                      << " since the lower and upper bounds matched."
                      << std::endl;
          }
        } else {
          consistent_bounds = false;
          std::cerr << "Infeasible lower bound of " << data.lower_bound
                    << " and upper bound of " << data.upper_bound << std::endl;
        }
      } else {
        // Handling the default nonnegativity is somewhat subtle and varies
        // between different MPS readers.
        if (data.upper_bound > Real(0)) {
          // Preserve the default nonnegativity assumption.
          data.lower_bounded = true;
          data.lower_bound = Real(0);
        } else if (data.upper_bound == Real(0)) {
          // There is disagreement on how to handle the lower bounds in the
          // case of a zero upper bound. We print a warning to signify this
          // corner case.
          if (config_.keep_nonnegative_with_zero_upper_bound) {
            data.lower_bounded = false;
            data.upper_bounded = false;
            data.fixed = true;
            data.fixed_value = 0;
            if (config_.verbose) {
              std::cout << "Fixing value at 0 due to 0 upper bound. "
                        << "If this is not desired, please modify "
                        << "keep_nonnegative_with_zero_upper_bound to false."
                        << std::endl;
            }
          } else {
            data.lower_bound = -std::numeric_limits<Real>::infinity();
            data.lower_bounded = false;
            if (config_.verbose) {
              std::cout << "Removing default nonnegativity due to zero "
                        << "upper bound. If this is not desired, please modify "
                        << "keep_nonnegative_with_zero_upper_bound to true."
                        << std::endl;
            }
          }
        }
      }
    }

    // Handle explicit lower bounds.
    if (data.lower_bounded) {
      // Any conflicts with an upper bound are already resolved.
      if (data.fixed) {
        if (data.fixed_value < data.lower_bound) {
          consistent_bounds = false;
          std::cerr << "Infeasible fixed value of " << data.fixed_value
                    << " and lower bound of " << data.lower_bound << "."
                    << std::endl;
        }
        data.lower_bounded = false;
      }
    }

    // Handle free values.
    if (data.free) {
      if (data.upper_bounded) {
        std::cerr << "Mixed free with upper bound." << std::endl;
      }
      if (data.lower_bounded) {
        std::cerr << "Mixed free with lower bound." << std::endl;
      }
      if (data.fixed) {
        std::cerr << "Mixed free with fixed." << std::endl;
      }
    }

    if (data.fixed) {
      CATAMARI_ASSERT(!data.lower_bounded,
                      "Variable was fixed and lower-bounded.");
      CATAMARI_ASSERT(!data.upper_bounded,
                      "Variable was fixed and upper-bounded.");
    }

    // Default to nonnegative if there were not any markings.
    const bool has_a_mark =
        data.upper_bounded || data.lower_bounded || data.fixed || data.free;
    if (!has_a_mark) {
      data.lower_bounded = true;
      data.lower_bound = Real(0);
    }
  }

  return consistent_bounds;
}

template <typename Real>
void Reader<Real>::CountBoundTypes() {
  info_.column_section.num_equality_entries = 0;
  info_.column_section.num_inequality_entries = 0;

  // Count the number of input row nonzeros.
  // We have already counted the number of equality, greater, and lesser rows.
  for (auto& row_iter : row_map_) {
    const auto& row_metadata = row_iter.second;
    if (row_metadata.type == MPS_EQUALITY_ROW) {
      // Handle the assignments A(row, column) = column_value.
      info_.column_section.num_equality_entries += row_metadata.num_nonzeros;
    } else if (row_metadata.type == MPS_LESSER_ROW) {
      // Handle the assignments G(row, column) = column_value.
      info_.column_section.num_inequality_entries += row_metadata.num_nonzeros;
    } else if (row_metadata.type == MPS_GREATER_ROW) {
      // Handle the assignments G(row, column) = -column_value.
      info_.column_section.num_inequality_entries += row_metadata.num_nonzeros;
    }
  }

  // Count the number of non-artificial variable bounds of each type, and the
  // number of associated equality and inequality nonzeros.
  for (auto& entry : variable_map_) {
    auto& data = entry.second;

    if (data.upper_bounded) {
      data.upper_bound_index = info_.bounds_section.num_upper_bounds++;
    }

    if (data.lower_bounded) {
      data.lower_bound_index = info_.bounds_section.num_lower_bounds++;
    }

    if (data.fixed) {
      // Account for the chi = fixed_value row.
      ++info_.bounds_section.num_fixed_bounds;
      ++info_.column_section.num_equality_entries;
    }

    if (data.free) {
      data.free_index = info_.bounds_section.num_free_bounds++;
    }
  }

  // Count the number of artificial RANGE bounds of each type, and the number of
  // associated equality and inequality nonzeros.
  for (auto& range_data : range_list_) {
    // Account for the introduction of the artificial variable zeta.
    ++info_.column_section.num_equality_entries;

    // Account for the -zeta <= lower_bound and zeta <= upper_bound rows.
    range_data.lower_bound_index = info_.bounds_section.num_lower_bounds++;
    range_data.upper_bound_index = info_.bounds_section.num_upper_bounds++;
  }
}

template <typename Real>
Reader<Real>::Reader(const ReadQuadraticProgramMPSConfig& config)
    : config_(config), file_(config.filename.c_str()) {
  if (config.compressed) {
    std::cerr << "Compressed reads are not yet supported." << std::endl;
  }
  if (!file_.is_open()) {
    std::cerr << "Could not open " << config.filename << std::endl;
  }

  // For quickly reparsing the column data section if there were any trivial
  // equality rows that required retrieving the associated single nonzero
  // column name.
  std::ifstream::pos_type column_section_beg;

  Section section = MPS_NONE;
  std::string line, token;
  while (std::getline(file_, line)) {
    if (line.empty()) {
      continue;
    }
    std::stringstream section_stream(line);
    if (IsCommentLine(section_stream)) {
      continue;
    }
    const bool is_data_line = IsDataLine(section_stream);

    if (!(section_stream >> token)) {
      // This was a whitespace line.
      continue;
    }

    if (!is_data_line) {
      if (section == MPS_ROWS) {
        PeekAtRanges(token);
      } else if (section == MPS_COLUMNS) {
        SimplifyRowTypes();
        SyncRangeData();
        if (trivial_equality_map_.size()) {
          StoreTrivialEqualityRowVariableNames(column_section_beg);
        }
      }

      // Determine the new section.
      section = DecodeSection(token);

      if (section == MPS_NAME) {
        if (!info_.name_section.name.empty()) {
          std::cerr << "Multiple 'NAME' sections." << std::endl;
        }
        if (!(section_stream >> info_.name_section.name)) {
          std::cerr << "Missing 'NAME' string." << std::endl;
        }
      } else if (section == MPS_OBJSENSE) {
        std::cerr << "OBJSENSE is not yet handled." << std::endl;
      } else if (section == MPS_COLUMNS) {
        column_section_beg = file_.tellg();
      } else if (section == MPS_END) {
        break;
      } else if (section == MPS_MARKER) {
        std::cerr << "MPS 'MARKER' section is not yet supported." << std::endl;
      } else if (section == MPS_SOS) {
        std::cerr << "MPS 'SOS' section is not yet supported." << std::endl;
      }

      continue;
    }

    // No section marker was found, so handle this data line.
    if (section == MPS_ROWS) {
      ProcessRowDataLine(line);
    } else if (section == MPS_COLUMNS) {
      ProcessColumnDataLine(line);
    } else if (section == MPS_RHS) {
      ProcessRHSDataLine(line);
    } else if (section == MPS_BOUNDS) {
      ProcessBoundsDataLine(line);
    } else if (section == MPS_RANGES) {
      // The RANGES section should have already been processed within
      // PeekAtRanges.
    } else if (section == MPS_QUADOBJ) {
      ProcessQuadraticObjectiveDataLine(line);
    } else {
      std::cerr << "Invalid MPS section of " << section << std::endl;
    }
  }

  if (info_.name_section.name.empty()) {
    std::cerr << "No nontrivial 'NAME' was found." << std::endl;
  }

  if (!info_.rhs_section.num_rhs) {
    // Any unmentioned values are assumed to be zero.
    info_.rhs_section.num_rhs = 1;
  }

  const bool consistent_bounds = FinalizeBoundTypes();
  if (!consistent_bounds) {
    // TODO(Jack Poulson): Declare primal infeasibility.
    std::cerr << "Inconsistent bounds in MPS." << std::endl;
  }
  CountBoundTypes();

  // Extract the number of variables; those from variable_map_ and range_list_
  // are potentially interwoven and are in the three sets of size:
  //
  //   * variable_map_.size() - info_.bounds_section.num_fixed_bounds,
  //   * info_.num_artificial_variables, and
  //   * info_.bounds_section.num_fixed_bounds.
  //
  info_.num_artificial_variables =
      range_list_.size() + num_converted_inequality_rows_;
  const Int num_nonfixed_variables =
      (variable_map_.size() - info_.bounds_section.num_fixed_bounds) +
      info_.num_artificial_variables;

  // We now force the fixed variables to come last.
  Int num_fixed_variables = 0;
  Int not_fixed_counter = 0;
  for (auto& entry : variable_map_) {
    auto& data = entry.second;
    if (data.fixed) {
      data.index = num_nonfixed_variables + num_fixed_variables++;
    } else {
      data.index = not_fixed_counter++;
    }
  }
  if (config_.verbose) {
    std::cout << "name: " << info_.name_section.name << "\n"
              << "num_fixed_variables: " << num_fixed_variables << "\n"
              << "num_nonfixed_variables: " << num_nonfixed_variables << "\n"
              << "num_artificial_variables: " << info_.num_artificial_variables
              << "\n"
              << "num_ranges: " << range_list_.size() << std::endl;
  }

  if (config_.format == BOUNDED_QUADRATIC_PROGRAM) {
    // Initialize the lower and upper bounds.
    const Int num_variables =
        variable_map_.size() + info_.num_artificial_variables;
    lower_bounds_.Resize(num_variables, -std::numeric_limits<Real>::infinity());
    upper_bounds_.Resize(num_variables, std::numeric_limits<Real>::infinity());
  }

  // Now that the initial pass over the file is done, we can set up for the
  // 'QueuedEntry' / 'GetEntry' cycle.
  file_.seekg(0, std::ios::beg);
}

template <typename Real>
std::vector<QPEntry<Real>> Reader<Real>::GetEntries() {
  std::string line, token;
  std::vector<QPEntry<Real>> queued_entries;
  mps::Section section = MPS_NONE;
  while (section != MPS_END && std::getline(file_, line)) {
    if (line.empty()) {
      continue;
    }
    std::stringstream section_stream(line);
    if (IsCommentLine(section_stream)) {
      continue;
    }
    const bool is_data_line = IsDataLine(section_stream);

    // The first token on each line should be a string. We will check it for
    // equivalence with each section string.
    if (!(section_stream >> token)) {
      // This line only consists of whitespace.
      continue;
    }

    if (!is_data_line) {
      // Handle any special section entrances.
      section = DecodeSection(token);
      if (section == MPS_END) {
        break;
      }
      continue;
    }

    // No section marker was found, so handle this data line.
    if (section == MPS_ROWS) {
      // We already have the row names.
    } else if (section == MPS_COLUMNS) {
      QueueColumnDataLine(line, &queued_entries);
    } else if (section == MPS_QUADOBJ) {
      QueueQuadraticObjectiveDataLine(line, &queued_entries);
    } else if (section == MPS_RHS) {
      QueueRHSDataLine(line, &queued_entries);
    } else if (section == MPS_BOUNDS) {
      // All bounds are handled at the bottom of this routine.
    } else if (section == MPS_RANGES) {
      continue;
    } else {
      std::cerr << "Invalid MPS section of " << section << std::endl;
    }
  }
  QueueVariableBounds(&queued_entries);

  return queued_entries;
}

template <typename Real>
const QuadraticProgramMPSInfo<Real>& Reader<Real>::Info() const {
  return info_;
}

template <typename Real>
const Buffer<Real>& Reader<Real>::LowerBounds() const {
  return lower_bounds_;
}

template <typename Real>
const Buffer<Real>& Reader<Real>::UpperBounds() const {
  return upper_bounds_;
}

template <typename Real>
const std::map<std::string, VariableMetadata<Real>>& Reader<Real>::VariableMap()
    const {
  return variable_map_;
}

template <typename Real>
const std::vector<RangeData<Real>>& Reader<Real>::RangeList() const {
  return range_list_;
}

}  // namespace mps
}  // namespace conic

#endif  // ifndef CONIC_QP_MPS_IMPL_H_
