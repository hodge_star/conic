/*
 * Copyright (c) 2019 Jack Poulson <jack@hodgestar.com>
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
#ifndef CONIC_CERTIFICATE_TYPE_H_
#define CONIC_CERTIFICATE_TYPE_H_

#include <iostream>

#include "conic/imports.hpp"

namespace conic {

// A primal-dual iterate can be marked as being one of the following four
// certificate types.
enum CertificateType {
  // An approximately optimal primal-dual pair has been found.
  OPTIMALITY_CERTIFICATE,

  // An approximate certificate of primal infeasibility has been found.
  PRIMAL_INFEASIBILITY_CERTIFICATE,

  // An approximate certificate of dual infeasibility has been found.
  DUAL_INFEASIBILITY_CERTIFICATE,

  // We only believe that the problem is approximately infeasible, but we do
  // not know if it is the primal and/or dual.
  INFEASIBILITY_CERTIFICATE,

  // No certificate has (yet) been found.
  INVALID_CERTIFICATE,
};

// Pretty-prints the certificate type.
std::ostream& operator<<(std::ostream& os, CertificateType type);

}  // namespace conic

#include "conic/certificate_type-impl.hpp"

#endif  // ifndef CONIC_CERTIFICATE_TYPE_H_
