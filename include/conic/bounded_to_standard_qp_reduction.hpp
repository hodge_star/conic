/*
 * Copyright (c) 2019 Jack Poulson <jack@hodgestar.com>
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
#ifndef CONIC_BOUNDED_TO_STANDARD_QP_REDUCTION_H_
#define CONIC_BOUNDED_TO_STANDARD_QP_REDUCTION_H_

#include "conic/bounded_qp/iterate.hpp"
#include "conic/bounded_qp/problem.hpp"
#include "conic/hsd_qp/iterate.hpp"
#include "conic/hsd_qp/problem.hpp"
#include "conic/imports.hpp"

namespace conic {

// A utility class for converting a "bounded" convex quadratic program, of the
// form:
//
//   arginf_x { c' x + (1 / 2) x' Q x : A x = b, l <= x <= u },
//
// into the form of a "standard" QP,
//
//   arginf_x { c' x + (1 / 2) x' Q x : A x = b, x >= 0 },
//
// for usage in a homogeneous self-dual embedding Interior Point Method. Along
// the way, we reduce out any fixed variables of the original bounded problem.
//
// This class is primarily used for converting the outputs of Mathematical
// Programming System (MPS) files.
//
// Given a bounded_qp::Problem, the 'num_fixed' member variable will be probed
// to determine how many pairs of matching lower and upper bounds occur in the
// final rows of the equality matrix.
//
// The fixed indices, which occur in the range
//
//   [num_unfixed_orig, num_primal_orig),
//
// are immediately subtracted out of the relevant entries of the expanded
// equality right-hand side vector and added into the objective shift, then the
// last columns of the original equality matrix are dropped. We must
// similarly drop entries from the quadratic objective matrix to account for
// these removals and modify the linear objective, as the partitioning
//
//   c = | c_0 |,  Q = | Q_{0, 0},  Q_{0, 1} |,
//       | c_1 |       | Q_{1, 0},  Q_{1, 1} |
//
// where the '1' variables are fixed, provides a decomposition of the objective:
//
//   c' x + (1 / 2) x' Q x =
//       (c_0 + Q_{0, 1} x_1)' x_0 + (1 / 2) x_0' Q_{0, 0} x_0 +
//       c_1' x_1 + (1 / 2) x_1' Q_{1, 1} x_1,
//
// which replaces the unfixed linear objective vector, c_0, with
//
//   c_0 + Q_{0, 1} x_1 = c_0 + Q_{1, 0}' x_1,
//
// and incorporates the objective shift,
//
//   c_1' x_1 + (1 / 2) x_1' Q_{1, 1} x_1,
//
// which is the objective of the restriction to the fixed variables.
//
// We then split the free variables into positive and negative components. For
// any row
//
//   \sum_{j=1}^n A_{i, j} x_j = b_i,
//
// where x_k is free, we split the variable into positive and negative
// components x_k^+ and x_k^- so that x_k = x_k^+ - x_k^- and
//
//   A_{i, k} (x_k^+ - x_k^-) + \sum_{1 <= j != k <= n} A_{i, j} x_j = b_i,
//
// which can be accomplished by placing x_k^+ into the position of x_k and
// extending A with a new column equal to the negation of column k so that
// x can be extended by x_k^- as:
//
//   \sum_{j=1}^{n + 1} \hat{A}_{i, j} \hat{x}_{j} = b_i.
//
// The linear objective vector needs to be extended in a similar manner as the
// equality matrix, A: we use the original value for the positive split entry
// and the negated value for the negated entry. Likewise, we keep the original
// off-diagonal row and column of the quadratic objective to translate from the
// original free variable to its positive component, but we negate it for the
// negative component and preserve the diagonal entries for both.
//
// Variables which only have lower bounds can be represented by their slacks
// by modifying the right-hand side entry. Supose x_k >= l_k and
//
//   \sum_{j=1}^n A_{i, j} x_j = b_i.
//
// Then, since
//
//   A_{i, k} x_k = A_{i, k} (x_k - l_k) + A_{i, k} l_k,
//
// replacing x_k := x_k - l_k yields
//
//   \sum_{j=1}^n A_{i, j} x_j = b_i - A_{i, k} l_k.
//
// Thus, using the original variables, we perform the replacement:
//
//   b_i -= A_{i, k} l_k.
//
// The linear objective vector need not be modified, but the objective function
// will need to be post-processed by adding the shift c_k l_k. The quadratic
// component can be analyzed via conformally decomposing the reordered problem
// as
//
//   x = | x_0 |,  l = | l_0 |,  c = | c_0 |,  Q = | Q_{0, 0}, Q_{0, 1} |,
//       | x_1 |       | l_1 |       | c_1 |       | Q_{1, 0}, Q_{1, 1} |
//
// where x_1 consists of the single lower-bounded entry to be replaced with its
// lower slack, x_1 - l_1. We have the combined linear and quadratic objective
// term
//
//   c' x + (1 / 2) x' Q x =
//
//     | c_0 |' | x_0 | + (1 / 2) | x_0 |' | Q_{0, 0}, Q_{0, 1} | | x_0 | =
//     | c_1 |  | x_1 |           | x_1 |  | Q_{1, 0}, Q_{1, 1} | | x_1 |
//
//     | c_0 + Q_{0, 1} l_1 | |    x_0    | +
//     | c_1 + Q_{1, 1} l_1 | | x_1 - l_1 |
//
//       (1 / 2) |    x_0    |' | Q_{0, 0}, Q_{0, 1} | |    x_0    | +
//               | x_1 - l_1 |  | Q_{1, 0}, Q_{1, 1} | | x_1 - l_1 |
//
//       c_1' l_1 + (1 / 2) l_1' Q_{1, 1} l_1,
//
// where the objective shift can be identified as
//
//   c_1' l_1 + (1 / 2) l_1' Q_{1, 1} l_1.
//
// Similarly, variables which only have upper bounds can be represented by
// *their* slacks, but an additional negation will need to be performed.
// Suppose x_k <= u_k and
//
//   \sum_{j=1}^n A_{i, j} x_j = b_i.
//
// Then, since
//
//   A_{i, k} x_k = (-A_{i, k}) (u_k - x_k) + A_{i, k} u_k, and
//   c_k x_k = (-c_k) (u_k - x_k) + c_k u_k.
//
// replacing x_k := u_k - x_k, negating the k'th column of A and the k'th
// entry of the objective vector c,
//
//   \sum_{j=1}^n A_{i, j} x_j = b_i + A_{i, k} u_k,
//
// but we note that, using the original sign of A_{i, k}, we would update
// b_i - A_{i, k} u_k. Thus, using the original variables, we perform the
// replacement
//
//   b_i -= A_{i, k} u_k.
//
// Using a similar decomposition as for the lower slack case, we have the
// combined linear and quadratic objective term
//
//   c' x + (1 / 2) x' Q x =
//
//     | c_0 |' | x_0 | + (1 / 2) | x_0 |' | Q_{0, 0}, Q_{0, 1} | | x_0 | =
//     | c_1 |  | x_1 |           | x_1 |  | Q_{1, 0}, Q_{1, 1} | | x_1 |
//
//     |  c_0 + Q_{0, 1} u_1 |' |    x_0    | +
//     | -c_1 - Q_{1, 1} u_1 |  | u_1 - x_1 |
//
//       (1 / 2) |    x_0    |' |  Q_{0, 0}, -Q_{0, 1} | |    x_0    | +
//               | u_1 - x_1 |  | -Q_{1, 0},  Q_{1, 1} | | u_1 - x_1 |
//
//       c_1' u_1 + (1 / 2) u_1' Q_{1, 1} u_1,
//
// where the objective shift can be identified as
//
//   c_1' u_1 + (1 / 2) u_1' Q_{1, 1} u_1.
//
// Finally, if a variable has a two-sided bound, say l_k <= x_k <= u_k, then
// both its lower and upper slacks, say, x_k - l_k and u_k - x_k, need to be
// explicitly represented. We can replace x_k with its lower slack
// representation in a manner similar to what is described above for
// variables which *only* have lower bounds. But we must then add an
// additional row of A in order to enforce the constraint that
//
//   (u_k - x_k) + (x_k - l_k) = u_k - l_k,
//
// which can be enforced, after the replacement x_k := x_k - l_k and the
// introduction of x_r = u_k - x_k, via:
//
//   x_k + x_r = u_k - l_k.
//
// We can then extend the linear and quadratic objectives by zero to the
// introduced upper slack variable.
//
template <typename Real>
class BoundedToStandardQPReduction {
 public:
  // Constructs the class from a given bounded Quadratic Program.
  BoundedToStandardQPReduction(
      const bounded_qp::Problem<Real>& bounded_problem);

  // Converts the bounded Quadratic Program into standard form.
  void FormStandardProblem(hsd_qp::Problem<Real>* standard_problem);

  // Converts an iterate in the standard formulation back into the original,
  // bounded formulation.
  void BacktransformIterate(const hsd_qp::Iterate<Real>& standard_iterate,
                            bounded_qp::Iterate<Real>* bounded_iterate) const;

  // Returns the value that should be added to the objective of the reduced
  // problem in order to match that of the original, "bounded" problem.
  Real ObjectiveShift() const;

 private:
  struct ReductionMetadata {
    // The number of fixed variables in the original bounded problem. They
    // should all occur in the final positions of the primal solution vector.
    Int num_fixed;

    // The number of primal solution variables in the original problem which
    // were not fixed to a particular value.
    Int num_nonfixed;

    // The number of free primal solution variables in the original problem.
    Int num_free;

    // The number of primal solution variables in the original problem which
    // only have a nontrivial lower bound.
    Int num_only_lower_bounds;

    // The number of primal solution variables in the original problem which
    // only have a nontrivial upper bound.
    Int num_only_upper_bounds;

    // The number of primal solution variables in the original problem which
    // have nontrivial lower and upper bounds.
    Int num_two_sided_bounds;

    // The number of primal variables in the standard-form problem.
    // The extended primal variables will be the union of the original unfixed
    // entries (or their identified slacks) and the partner negative splittings
    // of the free variables and the partner upper spack from two-sided bounds.
    // Thus, this value should equal
    //   num_nonfixed + num_free + num_two_sided_bounds.
    Int num_standard_primal;

    // The number of dual variables in the standard-form problem. This value
    // should equal
    //   num_nonempty_nonfixing_dual + num_two_sided_bounds.
    Int num_standard_dual;

    // Free variables are represented with separate positive and negative
    // components, and two-sided bounded variables with their lower and upper
    // slacks; the second variable in each case needs to be given a new index
    // after the set of original variable indices. Said indices are stored here
    // for each variable; singly-sided bounded variables do not require
    // auxiliary indices have have their values set to -1.
    Buffer<Int> auxiliary_index;

    // The equality matrix can be partitioned as:
    //
    //   A = | A_nonfixed,  A_fixed |.
    //
    // After eliminating fixed variables, we can restrict the matrix A to its
    // left A_nonfixed component, which consists of the column interacting
    // with non-fixed variables. It is possible that some of the rows of this
    // restricted matrix are empty, even if the full row was not, so we drop
    // such rows. We map from the original nonfixing rows to the packed indices
    // after dropping empty members with the following map.
    Buffer<Int> bounded_to_standard_equality_row_map;

    // The number of rows in the original equality matrix.
    Int num_dual;

    // The number of rows of the A_nonfixed matrix which are non-empty.
    Int num_nonempty_dual;

    // The number of nonzero entries which should appear in the equality matrix
    // of the standard-form problem.
    Int num_standard_equality_entries;

    // The number of nonzero entries which should appear in the quadratic
    // objective matrix of the standard-form problem.
    Int num_standard_quadratic_entries;

    // Fills the metadata given a bounded problem.
    void Fill(const bounded_qp::Problem<Real>& problem);
  };

  // A pointer to the original bounded problem. We are assuming that the
  // bounded problem passed into the constructor is not destructed before this
  // utility class.
  const bounded_qp::Problem<Real>* bounded_problem_ = nullptr;

  // The reduction metadata for the bounded problem.
  ReductionMetadata reduction_metadata_;

  // A pointer to the Homogeneous Self-Dual QP problem to be created from the
  // bounded problem.
  hsd_qp::Problem<Real>* standard_problem_ = nullptr;

  // The value to be added to the standard-form problem's objective to match
  // that of the original, "bounded" problem.
  Real objective_shift_ = 0;

  // Forms the resulting objective shift, updated linear objective, and updated
  // equality-constraint right-hand side vector from eliminating the fixed
  // variables -- which occur in the final positions -- of the bounded problem.
  void EliminateFixedVariables(Real* reduced_objective_shift,
                               BlasMatrix<Real>* reduced_linear_objective,
                               BlasMatrix<Real>* reduced_equality_rhs);

  // Forms the standard-form quadratic objective.
  void FormQuadraticObjective();

  // Forms the standard-form linear objective, and the constant shift, using
  // the reduced linear objective from the elimination of the fixed variables.
  void FormLinearObjectiveAndShift(
      Real reduced_objective_shift,
      const ConstBlasMatrixView<Real>& reduced_linear_objective);

  // Forms the standard-form equality matrix and right-hand side vector using
  // the reduced equality right-hand side vector from eliminating the fixed
  // variables.
  void FormEqualityConstraints(
      const ConstBlasMatrixView<Real>& reduced_equality_rhs);
};

}  // namespace conic

#include "conic/bounded_to_standard_qp_reduction-impl.hpp"

#endif  // ifndef CONIC_BOUNDED_TO_STANDARD_QP_REDUCTION_H_
