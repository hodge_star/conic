/*
 * Copyright (c) 2019 Jack Poulson <jack@hodgestar.com>
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
#ifndef CONIC_RESIDUALS_H_
#define CONIC_RESIDUALS_H_

#include "conic/imports.hpp"

namespace conic {

// A representation of an equality feasibility residual.
template <typename Real>
struct EqualityResidual {
  // The residual vector of the equality feasibility constraint.
  BlasMatrix<Real> residual;

  // The two-norm of the residual vector.
  Real residual_two_norm;

  // The two-norm of the residual vector divided by a scale factor.
  Real relative_residual_norm;

  // Sets the residual norm, and relative residual norm, from the residual
  // vector and a scale factor.
  void CompleteFromResidual(const Real normalization) {
    residual_two_norm = catamari::EuclideanNorm(residual.ConstView());
    relative_residual_norm = residual_two_norm / normalization;
  }
};

// A representation of the primal and dual objectives, the absolute value of
// their difference (the 'gap'), and the gap divided by a given scale factor.
template <typename Real>
struct ObjectiveResidual {
  // The primal objective.
  Real primal;

  // The dual objective.
  Real dual;

  // The absolute value of the difference between the primal and dual
  // objectives.
  Real gap;

  // The gap divided by the given scale factor.
  Real relative_gap;

  // Fills the gap, and relative gap, from the primal and dual objectives and a
  // scale factor.
  void CompleteFromPrimalDual() {
    gap = primal - dual;
    relative_gap = std::abs(gap) / (std::abs(dual) + 1);
  }
};

}  // namespace conic

#endif  // ifndef CONIC_RESIDUALS_H_
