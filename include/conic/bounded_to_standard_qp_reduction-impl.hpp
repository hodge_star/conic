/*
 * Copyright (c) 2019 Jack Poulson <jack@hodgestar.com>
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
#ifndef CONIC_BOUNDED_TO_STANDARD_QP_REDUCTION_IMPL_H_
#define CONIC_BOUNDED_TO_STANDARD_QP_REDUCTION_IMPL_H_

#include "conic/bounded_to_standard_qp_reduction.hpp"

namespace conic {

template <typename Real>
void BoundedToStandardQPReduction<Real>::ReductionMetadata::Fill(
    const bounded_qp::Problem<Real>& problem) {
  const Int num_bounded_primal = problem.equality_matrix.NumColumns();

  num_fixed = problem.num_fixed;
  num_nonfixed = num_bounded_primal - num_fixed;

  num_free = 0;
  num_only_lower_bounds = 0;
  num_only_upper_bounds = 0;
  num_two_sided_bounds = 0;
  auxiliary_index.Resize(num_nonfixed, -1);

  Int num_auxiliary = 0;
  for (Int i = 0; i < num_nonfixed; ++i) {
    const Real lower_bound = problem.lower_bounds[i];
    const Real upper_bound = problem.upper_bounds[i];
    if (std::isfinite(lower_bound) && std::isfinite(upper_bound)) {
      auxiliary_index[i] = num_nonfixed + num_auxiliary++;
      ++num_two_sided_bounds;
    } else if (std::isfinite(lower_bound)) {
      ++num_only_lower_bounds;
    } else if (std::isfinite(upper_bound)) {
      ++num_only_upper_bounds;
    } else {
      auxiliary_index[i] = num_nonfixed + num_auxiliary++;
      ++num_free;
    }
  }

  // Count the total number of entries in the expanded equality rows, and mark
  // the rows which have entries in them.
  num_standard_equality_entries = 0;
  num_dual = problem.equality_matrix.NumRows();
  Buffer<bool> has_row_entries(num_dual, false);
  for (const auto& entry : problem.equality_matrix.Entries()) {
    if (entry.column >= num_nonfixed) {
      // Do nothing: we drop the columns interacting with fixed variables.
    } else if (problem.Free(entry.column)) {
      // Account for the additional negated column of A.
      num_standard_equality_entries += 2;
      has_row_entries[entry.row] = true;
    } else {
      ++num_standard_equality_entries;
      has_row_entries[entry.row] = true;
    }
  }

  // Scan to decide the mapping from the truncated rows to the packed form
  // which skips empty rows.
  bounded_to_standard_equality_row_map.Resize(num_dual, -1);
  num_nonempty_dual = 0;
  for (Int row = 0; row < num_dual; ++row) {
    if (has_row_entries[row]) {
      bounded_to_standard_equality_row_map[row] = num_nonempty_dual++;
    }
  }

  // Account for the x_k + x_r = u_k - l_k constraints.
  num_standard_equality_entries += 2 * num_two_sided_bounds;

  num_standard_primal = num_nonfixed + num_free + num_two_sided_bounds;
  num_standard_dual = num_nonempty_dual + num_two_sided_bounds;

  num_standard_quadratic_entries = 0;
  for (const auto& entry : problem.quadratic_objective.Entries()) {
    if (entry.row >= num_nonfixed) {
      break;
    } else if (entry.column >= num_nonfixed) {
      continue;
    }

    // We place the entry or its negation in-place.
    ++num_standard_quadratic_entries;

    if (entry.row == entry.column) {
      if (problem.Free(entry.row)) {
        // We mirror the entry in the bottom-right quadrant.
        ++num_standard_quadratic_entries;
      }
    } else {
      if (problem.Free(entry.row)) {
        // We mirror the negation of the entry in the lower-left quadrant.
        ++num_standard_quadratic_entries;
      }
      if (problem.Free(entry.column)) {
        // We mirror the negation of the entry in the upper-right quadrant.
        ++num_standard_quadratic_entries;
      }
      if (problem.Free(entry.row) && problem.Free(entry.column)) {
        // We mirror the entry in the bottom-right quadrant.
        ++num_standard_quadratic_entries;
      }
    }
  }
}

template <typename Real>
BoundedToStandardQPReduction<Real>::BoundedToStandardQPReduction(
    const bounded_qp::Problem<Real>& bounded_problem) {
  bounded_problem_ = &bounded_problem;
}

template <typename Real>
void BoundedToStandardQPReduction<Real>::EliminateFixedVariables(
    Real* reduced_objective_shift, BlasMatrix<Real>* reduced_linear_objective,
    BlasMatrix<Real>* reduced_equality_rhs) {
  // We will use column-wise access of the equality matrix to form the reduced
  // equality right-hand side vector.
  if (reduction_metadata_.num_fixed > 0) {
    bounded_problem_->FormEqualityMatrixTranspose();
  }
  *reduced_objective_shift = bounded_problem_->objective_shift;
  *reduced_linear_objective = bounded_problem_->linear_objective;
  *reduced_equality_rhs = bounded_problem_->equality_rhs.Submatrix(
      0, 0, reduction_metadata_.num_dual, 1);
  const Buffer<MatrixEntry<Real>>& equality_transpose_entries =
      bounded_problem_->equality_matrix_transpose.Entries();
  const Buffer<MatrixEntry<Real>>& quadratic_entries =
      bounded_problem_->quadratic_objective.Entries();
  for (Int fixed = reduction_metadata_.num_fixed - 1; fixed >= 0; --fixed) {
    const Int column = reduction_metadata_.num_nonfixed + fixed;
    const Real fixed_value = bounded_problem_->lower_bounds[column];
    CONIC_ASSERT(
        bounded_problem_->upper_bounds[column] == fixed_value,
        "Expected matching bounds for fixed index " + std::to_string(column));
    const Real linear_objective = reduced_linear_objective->Entry(column);

    // Compute the linear contribution to the objective shift.
    *reduced_objective_shift += fixed_value * linear_objective;

    // Update the quadratic component of the objective shift and the
    // quadratic modifications to the linear objective.
    {
      const Int row_offset =
          bounded_problem_->quadratic_objective.RowEntryOffset(column);
      const Int row_size =
          bounded_problem_->quadratic_objective.NumRowEntries(column);
      for (Int k = 0; k < row_size; ++k) {
        const MatrixEntry<Real>& entry = quadratic_entries[row_offset + k];
        if (entry.column > column) {
          // We have already reduced the interacting variable, so skip it. And
          // all further entries should also be skipped.
          break;
        } else if (entry.column == column) {
          // This is a diagonal entry, so compute this variable's quadratic
          // contribution to the objective shift:
          //
          //   (1 / 2) x_1' Q_{1, 1} x_1.
          //
          *reduced_objective_shift +=
              fixed_value * entry.value * fixed_value / Real(2);
          break;
        } else {
          // This is an unreduced non-self interaction, so perform the piece of
          //
          //   c_0 += Q_{1, 0}' x_1,
          //
          // which takes the form
          //
          //   c_0(j) += Q(column, j) x(column).
          //
          reduced_linear_objective->Entry(entry.column) +=
              entry.value * fixed_value;
        }
      }
    }

    // Update the equality right-hand side vector by subtracting out the
    // contribution of the fixed variable. This is best performed by traversing
    // a column-wise representation of the equality matrix.
    {
      const Int column_offset =
          bounded_problem_->equality_matrix_transpose.RowEntryOffset(column);
      const Int column_size =
          bounded_problem_->equality_matrix_transpose.NumRowEntries(column);
      for (Int k = 0; k < column_size; ++k) {
        // Subtract the contribution of the fixed variable to A x = b.
        const MatrixEntry<Real>& entry =
            equality_transpose_entries[column_offset + k];
        reduced_equality_rhs->Entry(entry.column) -= entry.value * fixed_value;
      }
    }
  }
}

template <typename Real>
void BoundedToStandardQPReduction<Real>::FormQuadraticObjective() {
  const Int num_primal = reduction_metadata_.num_standard_primal;
  standard_problem_->quadratic_objective.Resize(num_primal, num_primal);
  standard_problem_->quadratic_objective.ReserveEntryAdditions(
      reduction_metadata_.num_standard_quadratic_entries);
  for (const auto& entry : bounded_problem_->quadratic_objective.Entries()) {
    if (entry.row >= reduction_metadata_.num_nonfixed) {
      break;
    } else if (entry.column >= reduction_metadata_.num_nonfixed) {
      continue;
    }

    // We place the original entry, or its negation, in-place.
    const bool negated_row = bounded_problem_->OnlyUpperBounded(entry.row);
    const bool negated_column =
        bounded_problem_->OnlyUpperBounded(entry.column);
    const bool negated = negated_row ^ negated_column;
    const Real value = negated ? -entry.value : entry.value;

    const bool free_row = bounded_problem_->Free(entry.row);
    const bool free_column = bounded_problem_->Free(entry.column);
    const Int auxiliary_row = reduction_metadata_.auxiliary_index[entry.row];
    const Int auxiliary_column =
        reduction_metadata_.auxiliary_index[entry.column];

    standard_problem_->quadratic_objective.QueueEntryAddition(
        entry.row, entry.column, value);

    if (entry.row == entry.column) {
      if (free_row) {
        // We mirror the entry in the bottom-right quadrant.
        standard_problem_->quadratic_objective.QueueEntryAddition(
            auxiliary_row, auxiliary_row, value);
      }
    } else {
      if (free_row) {
        // We mirror the negation of the entry in the lower-left quadrant.
        standard_problem_->quadratic_objective.QueueEntryAddition(
            auxiliary_row, entry.column, -value);
      }
      if (free_column) {
        // We mirror the negation of the entry in the upper-right quadrant.
        standard_problem_->quadratic_objective.QueueEntryAddition(
            entry.row, auxiliary_column, -value);
      }
      if (free_row && free_column) {
        // We mirror the entry in the bottom-right quadrant.
        standard_problem_->quadratic_objective.QueueEntryAddition(
            auxiliary_row, auxiliary_column, value);
      }
    }
  }
  standard_problem_->quadratic_objective.FlushEntryQueues();
}

template <typename Real>
void BoundedToStandardQPReduction<Real>::FormLinearObjectiveAndShift(
    Real reduced_objective_shift,
    const ConstBlasMatrixView<Real>& reduced_linear_objective) {
  const Int num_primal = reduction_metadata_.num_standard_primal;

  // Initialize the linear objective in a manner which only performs the free
  // variable conversions, but does not eliminate the lower or upper bounds.
  standard_problem_->linear_objective.Resize(num_primal, 1, Real(0));
  for (Int column = 0; column < reduction_metadata_.num_nonfixed; ++column) {
    const Real value = reduced_linear_objective(column);
    standard_problem_->linear_objective(column) = value;

    if (bounded_problem_->Free(column)) {
      const Int auxiliary_column = reduction_metadata_.auxiliary_index[column];
      standard_problem_->linear_objective(auxiliary_column) = -value;
    }
  }

  // Incorporate the Q_{:, k} l_k and Q_{:, k} u_k updates in order. We use the
  // restricted original quadratic objective and negate its entries dynamically.
  objective_shift_ = reduced_objective_shift;
  const Buffer<MatrixEntry<Real>>& quadratic_entries =
      bounded_problem_->quadratic_objective.Entries();
  for (Int column = 0; column < reduction_metadata_.num_nonfixed; ++column) {
    Real bound = 0;
    const Real lower_bound = bounded_problem_->lower_bounds[column];
    const Real upper_bound = bounded_problem_->upper_bounds[column];
    const Real linear_objective = standard_problem_->linear_objective(column);
    const Real diagonal_quadratic =
        bounded_problem_->quadratic_objective.Value(column, column);

    if (std::isfinite(lower_bound)) {
      bound = lower_bound;
    } else if (std::isfinite(upper_bound)) {
      bound = upper_bound;
    } else {
      // The variable must be free, so we need not incorporate an update to the
      // linear objective involving a bound and a column of the quadratic
      // objective.
      CONIC_ASSERT(bounded_problem_->Free(column),
                   "Variable was assumed free.");
      continue;
    }

    objective_shift_ +=
        linear_objective * bound + bound * diagonal_quadratic * bound / Real(2);

    if (bounded_problem_->OnlyUpperBounded(column)) {
      standard_problem_->linear_objective(column) =
          -standard_problem_->linear_objective(column);
    }

    const Int column_offset =
        bounded_problem_->quadratic_objective.RowEntryOffset(column);
    const Int column_size =
        bounded_problem_->quadratic_objective.NumRowEntries(column);
    for (Int k = 0; k < column_size; ++k) {
      const MatrixEntry<Real>& entry = quadratic_entries[column_offset + k];
      const Int row = entry.column;
      if (row >= reduction_metadata_.num_nonfixed) {
        // The fixed variables have been eliminated already.
        break;
      }

      // We need only negate when performing an update of a different, negated
      // (due to an applied only-upper-bounded transformation) variable's linear
      // objective coefficient. When self-updating an upper-bound-only variable,
      // the transformation is c_k := -c_k - Q_{k, k} u_k. A subsequent update
      // from a separate index, say, j != k, takes the form
      //
      //   c_k := c_k + (-Q_{j, k}) u_k,
      //
      // as the transformation x_k := u_k - x_k involves the negation of
      // off-diagonal entries Q_{l, k}, where l != k.
      //
      // We can arrive at a similar result if we perform the updates in the
      // opposite order:
      //
      //   -(c_k + Q_{j, k} u_k) - Q_{k, k} u_k =
      //   -c_k - Q_{k, k} u_k - Q_{j, k} u_k.
      //
      const bool negated =
          row <= column && bounded_problem_->OnlyUpperBounded(row);
      const Real value = negated ? -entry.value : entry.value;
      standard_problem_->linear_objective(row) += value * bound;

      if (bounded_problem_->Free(row)) {
        const Int auxiliary_row = reduction_metadata_.auxiliary_index[row];
        standard_problem_->linear_objective(auxiliary_row) -=
            entry.value * bound;
      }
    }
  }
}

template <typename Real>
void BoundedToStandardQPReduction<Real>::FormEqualityConstraints(
    const ConstBlasMatrixView<Real>& reduced_equality_rhs) {
  const Int num_primal = reduction_metadata_.num_standard_primal;
  const Int num_dual = reduction_metadata_.num_standard_dual;
  standard_problem_->equality_matrix.Resize(num_dual, num_primal);
  standard_problem_->equality_matrix.ReserveEntryAdditions(
      reduction_metadata_.num_standard_equality_entries);
  standard_problem_->equality_rhs.Resize(num_dual, 1, Real(0));
  for (Int row = 0; row < reduction_metadata_.num_dual; ++row) {
    const Int standard_row =
        reduction_metadata_.bounded_to_standard_equality_row_map[row];
    if (standard_row < 0) {
      continue;
    }
    standard_problem_->equality_rhs(standard_row) = reduced_equality_rhs(row);
  }
  for (const auto& entry : bounded_problem_->equality_matrix.Entries()) {
    if (entry.column >= reduction_metadata_.num_nonfixed) {
      // We have already eliminated the fixed contributions.
      continue;
    }

    const Int standard_row =
        reduction_metadata_.bounded_to_standard_equality_row_map[entry.row];
    CONIC_ASSERT(standard_row >= 0,
                 "Encountered entry in supposedly empty row");

    const Int column = entry.column;
    const Real lower_bound = bounded_problem_->lower_bounds[column];
    const Real upper_bound = bounded_problem_->upper_bounds[column];
    if (std::isfinite(lower_bound)) {
      // We translate x_k to x_k - l_k in the original position; for two-sided
      // bounded variables, we later add an equality constraint to u_k - x_k
      // and a linear constraint tying u_k - x_k to x_k - l_k.
      standard_problem_->equality_matrix.QueueEntryAddition(
          standard_row, column, entry.value);
      standard_problem_->equality_rhs(standard_row) -=
          entry.value * lower_bound;
    } else if (std::isfinite(upper_bound)) {
      CONIC_ASSERT(bounded_problem_->OnlyUpperBounded(column),
                   "Invalid bound type");
      // We translate x_k to u_k - x_k and negate the corresponding column of A.
      standard_problem_->equality_matrix.QueueEntryAddition(
          standard_row, column, -entry.value);
      standard_problem_->equality_rhs(standard_row) -=
          entry.value * upper_bound;
    } else {
      CONIC_ASSERT(bounded_problem_->Free(column),
                   "Variable was assumed free.");
      // We split x_k into x_k^+ and x_k^-, placing x_k^+ into the original
      // position and inserting x_k^- as a new variable (corresponding to
      // appending the negation of the column of A corresponding to x_k).
      standard_problem_->equality_matrix.QueueEntryAddition(
          standard_row, column, entry.value);
      standard_problem_->equality_matrix.QueueEntryAddition(
          standard_row, reduction_metadata_.auxiliary_index[column],
          -entry.value);
    }
  }

  // Enforce the two-sided constraints (x_k - l_k) + (u_k - x_k) = u_k - l_k.
  Int two_sided_index = 0;
  for (Int column = 0; column < reduction_metadata_.num_nonfixed; ++column) {
    const Real lower_bound = bounded_problem_->lower_bounds[column];
    const Real upper_bound = bounded_problem_->upper_bounds[column];
    if (std::isfinite(lower_bound) && std::isfinite(upper_bound)) {
      const Int two_sided_row =
          reduction_metadata_.num_nonempty_dual + two_sided_index++;
      const Int auxiliary_column = reduction_metadata_.auxiliary_index[column];

      standard_problem_->equality_matrix.QueueEntryAddition(two_sided_row,
                                                            column, Real(1));
      standard_problem_->equality_matrix.QueueEntryAddition(
          two_sided_row, auxiliary_column, Real(1));
      standard_problem_->equality_rhs(two_sided_row) =
          upper_bound - lower_bound;
    }
  }

  standard_problem_->equality_matrix.FlushEntryQueues();
}

template <typename Real>
void BoundedToStandardQPReduction<Real>::FormStandardProblem(
    hsd_qp::Problem<Real>* standard_problem) {
  standard_problem_ = standard_problem;

  reduction_metadata_.Fill(*bounded_problem_);

  // We start by eliminating the fixed variables. To do so, we accumulate the
  // shift and linear objective.
  Real reduced_objective_shift;
  BlasMatrix<Real> reduced_linear_objective;
  BlasMatrix<Real> reduced_equality_rhs;
  EliminateFixedVariables(&reduced_objective_shift, &reduced_linear_objective,
                          &reduced_equality_rhs);

  // Check for linear constraints of the form 0' x = beta, where beta != 0.
  // TODO(Jack Poulson): Compute || b ||_2 and use a relative tolerance.
  for (Int row = 0; row < reduction_metadata_.num_dual; ++row) {
    if (reduction_metadata_.bounded_to_standard_equality_row_map[row] < 0) {
      // This row of the reduced equality matrix is empty.
      if (reduced_equality_rhs(row) != Real(0)) {
        // TODO(Jack Poulson): Determine how to handle this situation with more
        // than an error message.
        std::cerr << "Empty reduced equality row " << row << " had reduced RHS "
                  << "value " << reduced_equality_rhs(row) << std::endl;
      }
    }
  }

  FormQuadraticObjective();
  FormLinearObjectiveAndShift(reduced_objective_shift,
                              reduced_linear_objective.ConstView());
  FormEqualityConstraints(reduced_equality_rhs.ConstView());

  standard_problem_->FillNorms();
}

template <typename Real>
void BoundedToStandardQPReduction<Real>::BacktransformIterate(
    const hsd_qp::Iterate<Real>& standard_iterate,
    bounded_qp::Iterate<Real>* bounded_iterate) const {
  const Int num_primal_bounded = bounded_problem_->equality_matrix.NumColumns();
  const Int num_dual_bounded = bounded_problem_->equality_matrix.NumRows();
  const Int num_fixed = bounded_problem_->num_fixed;
  const ConstBlasMatrixView<Real> standard_primal_solution =
      standard_iterate.PrimalSolution();
  const ConstBlasMatrixView<Real> standard_dual_solution =
      standard_iterate.DualSolution();
  const ConstBlasMatrixView<Real> standard_dual_slack =
      standard_iterate.DualSlack();
  const Real tau = standard_iterate.Tau();

  bounded_iterate->stacked_vectors.Resize(num_primal_bounded, num_dual_bounded);
  bounded_iterate->certificate_type = standard_iterate.certificate_type;
  BlasMatrixView<Real> bounded_primal_solution =
      bounded_iterate->PrimalSolution();
  BlasMatrixView<Real> bounded_dual_solution = bounded_iterate->DualSolution();
  BlasMatrixView<Real> bounded_dual_lower_slack =
      bounded_iterate->DualLowerSlack();
  BlasMatrixView<Real> bounded_dual_upper_slack =
      bounded_iterate->DualUpperSlack();

  const bool infeasibility_certificate =
      standard_iterate.certificate_type == PRIMAL_INFEASIBILITY_CERTIFICATE ||
      standard_iterate.certificate_type == DUAL_INFEASIBILITY_CERTIFICATE ||
      standard_iterate.certificate_type == INFEASIBILITY_CERTIFICATE;
  const Real iterate_scale = infeasibility_certificate ? Real(1) : tau;

  if (standard_iterate.certificate_type == PRIMAL_INFEASIBILITY_CERTIFICATE) {
    // TODO(Jack Poulson): Check that || A' y + z || / |b' y| stays small
    // after the backtransformation.
  } else if (standard_iterate.certificate_type ==
             DUAL_INFEASIBILITY_CERTIFICATE) {
    // TODO(Jack Poulson): Check that || Q x || / |c' x| and || A x || / |c' x|
    // stay small after the backtransformation.
  }

  // Fill the bounded primal solution vector.
  for (Int i = 0; i < num_primal_bounded; ++i) {
    const Real lower_bound = bounded_problem_->lower_bounds[i];
    const Real upper_bound = bounded_problem_->upper_bounds[i];

    if (i >= num_primal_bounded - num_fixed) {
      // We have a fixed variable, which should imply lower_bound = upper_bound.
      CONIC_ASSERT(bounded_problem_->lower_bounds[i] ==
                       bounded_problem_->upper_bounds[i],
                   "Fixed variable " + std::to_string(i) + " had bounds [" +
                       std::to_string(bounded_problem_->lower_bounds[i]) +
                       ", " +
                       std::to_string(bounded_problem_->upper_bounds[i]) + "]");
      if (infeasibility_certificate) {
        bounded_primal_solution(i) = 0;
      } else {
        bounded_primal_solution(i) = lower_bound;
      }
    } else if (std::isfinite(lower_bound) && std::isfinite(upper_bound)) {
      // The variable has a two-sided bound, so we can shift by the lower bound
      // to convert x_i - l_i into x_i.
      bounded_primal_solution(i) =
          standard_primal_solution(i) / iterate_scale + lower_bound;
    } else if (std::isfinite(lower_bound)) {
      // The variable only has a lower bound, so shift by the lower bound.
      bounded_primal_solution(i) =
          standard_primal_solution(i) / iterate_scale + lower_bound;
    } else if (std::isfinite(upper_bound)) {
      // The variable only has an upper bound, shift by the upper bound and
      // negate: u_i - x_i becomes x_i.
      bounded_primal_solution(i) =
          upper_bound - standard_primal_solution(i) / iterate_scale;
    } else {
      // The variable is free, so it was split between its positive and
      // negative components. The latter is in position auxiliary_index[i].
      bounded_primal_solution(i) =
          (standard_primal_solution(i) -
           standard_primal_solution(reduction_metadata_.auxiliary_index[i])) /
          iterate_scale;
    }
  }

  // Fill the bounded dual solution vector.
  for (Int i = 0; i < num_dual_bounded; ++i) {
    bounded_dual_solution(i) = standard_dual_solution(i) / iterate_scale;
  }

  // Fill the bounded lower dual slack vector.
  const Buffer<MatrixEntry<Real>>& equality_transpose_entries =
      bounded_problem_->equality_matrix_transpose.Entries();
  const Buffer<MatrixEntry<Real>>& quadratic_entries =
      bounded_problem_->quadratic_objective.Entries();
  for (Int i = 0; i < num_primal_bounded; ++i) {
    const Real lower_bound = bounded_problem_->lower_bounds[i];
    const Real upper_bound = bounded_problem_->upper_bounds[i];

    if (lower_bound == upper_bound) {
      CONIC_ASSERT(i + reduction_metadata_.num_fixed >= num_primal_bounded,
                   "Encountered fixed variable in wrong position.");
      if (infeasibility_certificate) {
        bounded_dual_lower_slack(i) = 0;
        bounded_dual_upper_slack(i) = 0;
      } else {
        // We demand that A' y + z - w = c + Q x, so we can just set
        // z = positive_part(c + Q x - A' y)_i,
        // w = negative_part(c + Q x - A' y)_i.

        // Initialize the residual to c_i.
        Real residual = bounded_problem_->linear_objective(i);

        // Add (Q x)_i.
        const Int quadratic_row_offset =
            bounded_problem_->quadratic_objective.RowEntryOffset(i);
        const Int quadratic_row_size =
            bounded_problem_->quadratic_objective.NumRowEntries(i);
        for (Int k = 0; k < quadratic_row_size; ++k) {
          const MatrixEntry<Real>& entry =
              quadratic_entries[quadratic_row_offset + k];
          residual += entry.value * bounded_primal_solution(entry.column);
        }

        // Subtract (A' y)_i.
        const Int equality_transpose_row_offset =
            bounded_problem_->equality_matrix_transpose.RowEntryOffset(i);
        const Int equality_transpose_row_size =
            bounded_problem_->equality_matrix_transpose.NumRowEntries(i);
        for (Int k = 0; k < equality_transpose_row_size; ++k) {
          const MatrixEntry<Real>& entry =
              equality_transpose_entries[equality_transpose_row_offset + k];
          residual -= entry.value * bounded_dual_solution(entry.column);
        }

        // Extract the positive and negative components.
        if (residual >= 0) {
          bounded_dual_lower_slack(i) = residual;
          bounded_dual_upper_slack(i) = 0;
        } else {
          bounded_dual_lower_slack(i) = 0;
          bounded_dual_upper_slack(i) = -residual;
        }
      }
    } else if (std::isfinite(lower_bound) && std::isfinite(upper_bound)) {
      bounded_dual_lower_slack(i) = standard_dual_slack(i) / iterate_scale;
      bounded_dual_upper_slack(i) =
          standard_dual_slack(reduction_metadata_.auxiliary_index[i]) /
          iterate_scale;
    } else if (std::isfinite(lower_bound)) {
      bounded_dual_lower_slack(i) = standard_dual_slack(i) / iterate_scale;
      bounded_dual_upper_slack(i) = 0;
    } else if (std::isfinite(upper_bound)) {
      bounded_dual_lower_slack(i) = 0;
      bounded_dual_upper_slack(i) = standard_dual_slack(i) / iterate_scale;
    } else {
      bounded_dual_lower_slack(i) = 0;
      bounded_dual_upper_slack(i) = 0;
    }
  }
}

template <typename Real>
Real BoundedToStandardQPReduction<Real>::ObjectiveShift() const {
  return objective_shift_;
}

}  // namespace conic

#endif  // ifndef CONIC_BOUNDED_TO_STANDARD_QP_REDUCTION_IMPL_H_
