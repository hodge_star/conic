/*
 * Copyright (c) 2019 Jack Poulson <jack@hodgestar.com>
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
#ifndef CONIC_CERTIFICATE_TYPE_IMPL_H_
#define CONIC_CERTIFICATE_TYPE_IMPL_H_

#include "conic/certificate_type.hpp"

namespace conic {

std::ostream& operator<<(std::ostream& os, CertificateType type) {
  if (type == OPTIMALITY_CERTIFICATE) {
    os << "OPTIMALITY_CERTIFICATE";
  } else if (type == PRIMAL_INFEASIBILITY_CERTIFICATE) {
    os << "PRIMAL_INFEASIBILITY_CERTIFICATE";
  } else if (type == DUAL_INFEASIBILITY_CERTIFICATE) {
    os << "DUAL_INFEASIBILITY_CERTIFICATE";
  } else if (type == INFEASIBILITY_CERTIFICATE) {
    os << "INFEASIBILITY_CERTIFICATE";
  } else {
    os << "INVALID_CERTIFICATE";
  }
  return os;
}

}  // namespace conic

#endif  // ifndef CONIC_CERTIFICATE_TYPE_IMPL_H_
