/*
 * Copyright (c) 2019 Jack Poulson <jack@hodgestar.com>
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
#ifndef CONIC_BOUNDED_QP_PROBLEM_H_
#define CONIC_BOUNDED_QP_PROBLEM_H_

#include "conic/bounded_qp/stacked_primal_dual_vectors.hpp"
#include "conic/imports.hpp"
#include "conic/residuals.hpp"

namespace conic {
namespace bounded_qp {

// A representation of a linear program in the bounded standard form:
//
//   arginf_x { c' x + (1 / 2) x' Q x : A x = b, l <= x <= u },
//
// where members of l can be either -infinity or finite, and members of u can
// be either +infinity or finite.
//
template <typename Real>
struct Problem {
  // While we do not incorporate this constant objective shift into the solver
  // itself, which uses the primal objective c' x + (1 / 2) x' Q x. But the
  // full primal objective is incremented by this shift. Typically, it is
  // specified via the RHS section of an MPS or SIF file.
  Real objective_shift = 0;

  // The column vector 'c' in the objective function:
  //     c' x + (1 / 2) x' Q x.
  BlasMatrix<Real> linear_objective;
  Real linear_objective_two_norm;

  // The symmetric quasi-semidefinite matrix 'Q' in the objective:
  //     c' x + (1 / 2) x' Q x.
  CoordinateMatrix<Real> quadratic_objective;
  Real quadratic_objective_max_norm;

  // The matrix 'A' in the primal equality constraint 'A x = b'.
  CoordinateMatrix<Real> equality_matrix;
  Real equality_matrix_max_norm;

  // If we use a Cholesky factorization of the Schur complement of the first-
  // order optimality conditions onto the dual solution variables, then it is
  // helpful to have the equality matrix in column-major storage. Equivalently,
  // we form the transpose of the equality matrix in row-major storage.
  mutable bool have_equality_matrix_transpose = false;
  mutable CoordinateMatrix<Real> equality_matrix_transpose;

  // The column vector 'b' in the primal equality constraint 'A x = b'.
  BlasMatrix<Real> equality_rhs;
  Real equality_rhs_two_norm;

  // The lower bounds vector, l, for the bounds l <= x <= u. If there is no
  // lower bound, the entry can be set to -infinity.
  Buffer<Real> lower_bounds;

  // The upper bounds vector, u, for the bounds l <= x <= u. If there is no
  // upper bound, the entry can be set to +infinity.
  Buffer<Real> upper_bounds;

  // The number of contiguous fixed variables at the end of the primal solution
  // vector and the last rows of the equality matrix. The last 'num_fixed'
  // rows of the equality matrix should be of the form e_k', the row vector of
  // all zeros except for a one in the k'th position, where k occurs in the
  // last 'num_fixed' entries of the primal solution vector.
  mutable Int num_fixed = 0;

  // Fills the norm variables from the vectors.
  void FillNorms();

  // Prints the norms to the given output stream.
  void PrintNorms(std::ostream& os) const;

  // Ensures that a cache of the transpose of the equality matrix is available.
  void FormEqualityMatrixTranspose() const;

  // Returns true if the variable of the given index has a finite lower bound.
  bool LowerBounded(Int index) const;

  // Returns true if the variable of the given index has a finite upper bound.
  bool UpperBounded(Int index) const;

  // Returns true if the variable of the given index has a finite lower bound
  // but no upper bound.
  bool OnlyLowerBounded(Int index) const;

  // Returns true if the variable of the given index has a finite upper bound
  // but no lower bound.
  bool OnlyUpperBounded(Int index) const;

  // Returns true if the variable of the given index has finite lower and
  // upper bounds.
  bool TwoSidedBounded(Int index) const;

  // Returns true if the variable of the given index has no lower or upper
  // bounds.
  bool Free(Int index) const;

  // Given the constraints
  //
  //   A x = b,  l <= x <= u,
  //
  // we attempt to tighten the lower bounds, l, and upper bounds, u. The
  // primary mechanism is that, for each equality constraint, say
  //
  //   a_i' x = beta_i,
  //
  // we can express it in the form
  //
  //   \sum_{j in pattern(a_i)} a_{i, j} x_j = beta_i,
  //
  // so that, for any particular pattern index j,
  //
  //   x_j = (beta_i - sum_{k in pattern(a_i) - {j}} a_{i, k} x_k) / a_{i, j}.
  //
  // If each of the a_{i, k} in pattern(a_i) are non-negative, we can impose an
  // upper bound on x_j via
  //
  //   x_j <= (beta_i - sum_{k in pattern(a_i) - {j}} a_{i, k} l_k) / a_{i, j}.
  //
  // Thus, if all of the relevant entries of A were non-negative, we could bound
  //
  //   x_j <= min_i
  //       (beta_i - sum_{k in pattern(a_i) - {j}} a_{i, k} l_k) / a_{i, j}.
  //
  //   x_j >= max_i
  //       (beta_i - sum_{k in pattern(a_i) - {j}} a_{i, k} u_k) / a_{i, j}.
  //
  // Clearly, for each sign change of a member of the pattern of a_i, we should
  // swap between the lower and upper bound.
  //
  // Further, after finding any nontrivial tightening of an index j, there is a
  // ripple effect of possible further tightenings on
  //
  //   union_{i where j in pattern(A(i,:))} pattern(A(i, :))
  //
  // possible direct impact will be on the bounds of pattern(a_j), but any
  // resulting tightening of those indices then implies possible impacts on
  // indices in the pattern of its corresponding column of A.
  void TightenBounds(bool tighten_infinite, bool verbose);
};

// Pretty-prints the linear programming problem.
template <typename Real>
std::ostream& operator<<(std::ostream& os, const Problem<Real>& problem);

// A certificate for an approximate solution of a bounded linear program.
// Both the primal and dual solutions are provided so that the duality gap can
// be computed.
//
// The primal problem is
//
//   arginf_x { c' x + (1 / 2) x' Q x : A x = b, l <= x <= u },
//
// and the dual problem is
//
//   argsup_{y, z, w} { b' y + l' z - u' w - (1 / 2) x' Q x :
//       A' y + z - w = c, z, w >= 0 }.
//
template <typename Real>
struct Certificate {
  // The concatenation of x and y.
  StackedPrimalDualVectors<Real> stacked_vectors;

  // Returns a view of the primal solution vector.
  BlasMatrixView<Real>& PrimalSolution();
  const ConstBlasMatrixView<Real>& PrimalSolution() const;

  // Returns a view of the dual solution vector.
  BlasMatrixView<Real>& DualSolution();
  const ConstBlasMatrixView<Real>& DualSolution() const;

  // Returns a view of the dual lower slack vector.
  BlasMatrixView<Real>& DualLowerSlack();
  const ConstBlasMatrixView<Real>& DualLowerSlack() const;

  // Returns a view of the dual upper slack vector.
  BlasMatrixView<Real>& DualUpperSlack();
  const ConstBlasMatrixView<Real>& DualUpperSlack() const;

  // Prints the two-norm and max-norm of each component of the certificate.
  void PrintNorms(std::ostream& os) const;

  // In debug mode, this routine asserts that the primal and dual slacks are
  // in the interior of the positive orthant.
  void EnsureInPositiveOrthant(const Problem<Real>& problem) const;
};

// Pretty-prints the certificate.
template <typename Real>
std::ostream& operator<<(std::ostream& os,
                         const Certificate<Real>& certificate);

}  // namespace bounded_qp
}  // namespace conic

#include "conic/bounded_qp/problem-impl.hpp"

#endif  // ifndef CONIC_BOUNDED_QP_PROBLEM_H_
