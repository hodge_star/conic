/*
 * Copyright (c) 2019 Jack Poulson <jack@hodgestar.com>
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
#ifndef CONIC_BOUNDED_QP_ITERATE_H_
#define CONIC_BOUNDED_QP_ITERATE_H_

#include "conic/bounded_qp/stacked_primal_dual_vectors.hpp"
#include "conic/certificate_type.hpp"
#include "conic/imports.hpp"
#include "conic/residuals.hpp"

namespace conic {
namespace bounded_qp {

// An approximate certificate for an approximate solution of the (possibly
// infeasible) primal-dual quadratic program:
//
//   arginf_x { c' x + (1 / 2) x' Q x : A x = b, l <= x <= u },
//
// which yields the Lagrangian
//
//   L_mu(x; y, z, w) = c' x + (1 / 2) x' Q x + y' (b - A x) - z' (x - l) -
//        w' (u - x) + mu sum_i ln(z_i) + mu sum_i ln(w_i),
//
// where we restrict l <= x <= u and z, w >= 0. The first-order optimality
// conditions are
//
//   d_x L = 0 = c + Q x - A' y - z + w,
//
//   d_y L = b - A x = 0,
//
//   d_z L = 0 = -(x - l) + mu inv(z), implies z o (x - l) = mu e,
//
//   d_w L = 0 = -(u - x) + mu inv(w), implies w o (u - x) = mu e.
//
// The corresponding dual problem can be found by reorganizing the Lagrangian
// as
//
//   L_mu(x; y, z, w) = (b' y + l' z - w' u - (1 / 2) x' Q x) +
//       x' (c + Q x - A' y - z + w) + mu sum_i ln(z_i) + mu sum_i ln(w_i),
//
// which we can interpret as a weak variant of the dual problem
//
//   argsup_{x, y, z, w} { b' y + l' z - w' u - (1 / 2) x' Q x :
//       A' y + z - w = c + Q x,  z, w >= 0 }.
//
template <typename Real>
struct Iterate {
  // The certificate status of this iterate.
  CertificateType certificate_type = INVALID_CERTIFICATE;

  // A representation of x, y, z, and w.
  StackedPrimalDualVectors<Real> stacked_vectors;

  // Returns a view of the primal solution vector.
  BlasMatrixView<Real>& PrimalSolution();
  const ConstBlasMatrixView<Real>& PrimalSolution() const;

  // Returns a view of the dual solution vector.
  BlasMatrixView<Real>& DualSolution();
  const ConstBlasMatrixView<Real>& DualSolution() const;

  // Returns a view of the dual lower slack vector, z.
  BlasMatrixView<Real>& DualLowerSlack();
  const ConstBlasMatrixView<Real>& DualLowerSlack() const;

  // Returns a view of the dual upper slack vector, w.
  BlasMatrixView<Real>& DualUpperSlack();
  const ConstBlasMatrixView<Real>& DualUpperSlack() const;

  // Prints the two-norm and max-norm of each component of the certificate.
  void PrintNorms(std::ostream& os) const;
};

// Pretty-prints the certificate.
template <typename Real>
std::ostream& operator<<(std::ostream& os, const Iterate<Real>& iterate);

}  // namespace bounded_qp
}  // namespace conic

#include "conic/bounded_qp/iterate-impl.hpp"

#endif  // ifndef CONIC_BOUNDED_QP_ITERATE_H_
