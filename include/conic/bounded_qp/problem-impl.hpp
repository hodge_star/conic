/*
 * Copyright (c) 2019 Jack Poulson <jack@hodgestar.com>
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
#ifndef CONIC_BOUNDED_QP_PROBLEM_IMPL_H_
#define CONIC_BOUNDED_QP_PROBLEM_IMPL_H_

#include <exception>
#include <limits>

#include "conic/bounded_qp/problem.hpp"

namespace conic {
namespace bounded_qp {

template <typename Real>
void Problem<Real>::FillNorms() {
  linear_objective_two_norm = EuclideanNorm(linear_objective.ConstView());
  quadratic_objective_max_norm = MaxNorm(quadratic_objective);
  equality_matrix_max_norm = MaxNorm(equality_matrix);
  equality_rhs_two_norm = EuclideanNorm(equality_rhs.ConstView());
}

template <typename Real>
void Problem<Real>::PrintNorms(std::ostream& os) const {
  os << "|| c ||_2:   " << linear_objective_two_norm << "\n"
     << "|| Q ||_max: " << quadratic_objective_max_norm << "\n"
     << "|| A ||_max: " << equality_matrix_max_norm << "\n"
     << "|| b ||_2:   " << equality_rhs_two_norm << std::endl;
}

template <typename Real>
void Problem<Real>::FormEqualityMatrixTranspose() const {
  if (!have_equality_matrix_transpose) {
    FormSparseTranspose(equality_matrix, &equality_matrix_transpose);
    have_equality_matrix_transpose = true;
  }
}

template <typename Real>
bool Problem<Real>::LowerBounded(Int index) const {
  return std::isfinite(lower_bounds[index]);
}

template <typename Real>
bool Problem<Real>::UpperBounded(Int index) const {
  return std::isfinite(upper_bounds[index]);
}

template <typename Real>
bool Problem<Real>::OnlyLowerBounded(Int index) const {
  return LowerBounded(index) && !UpperBounded(index);
}

template <typename Real>
bool Problem<Real>::OnlyUpperBounded(Int index) const {
  return !LowerBounded(index) && UpperBounded(index);
}

template <typename Real>
bool Problem<Real>::TwoSidedBounded(Int index) const {
  return LowerBounded(index) && UpperBounded(index);
}

template <typename Real>
bool Problem<Real>::Free(Int index) const {
  return !LowerBounded(index) && !UpperBounded(index);
}

template <typename Real>
std::ostream& operator<<(std::ostream& os, const Problem<Real>& problem) {
  os << "c=[\n"
     << problem.linear_objective << "];\n"
     << "QSparse=[\n"
     << problem.quadratic_objective << "];\n"
     << "ASparse=[\n"
     << problem.equality_matrix << "];\n"
     << "b=[\n"
     << problem.equality_rhs << "];\n"
     << "l=[" << problem.lower_bounds << "];\n"
     << "u=[" << problem.upper_bounds << "];" << std::endl;
  return os;
}

template <typename Real>
BlasMatrixView<Real>& Certificate<Real>::PrimalSolution() {
  return stacked_vectors.PrimalSolutions();
}

template <typename Real>
const ConstBlasMatrixView<Real>& Certificate<Real>::PrimalSolution() const {
  return stacked_vectors.PrimalSolutions();
}

template <typename Real>
BlasMatrixView<Real>& Certificate<Real>::DualSolution() {
  return stacked_vectors.DualSolutions();
}

template <typename Real>
const ConstBlasMatrixView<Real>& Certificate<Real>::DualSolution() const {
  return stacked_vectors.DualSolutions();
}

template <typename Real>
BlasMatrixView<Real>& Certificate<Real>::DualLowerSlack() {
  return stacked_vectors.DualLowerSlacks();
}

template <typename Real>
const ConstBlasMatrixView<Real>& Certificate<Real>::DualLowerSlack() const {
  return stacked_vectors.DualLowerSlacks();
}

template <typename Real>
BlasMatrixView<Real>& Certificate<Real>::DualUpperSlack() {
  return stacked_vectors.DualUpperSlacks();
}

template <typename Real>
const ConstBlasMatrixView<Real>& Certificate<Real>::DualUpperSlack() const {
  return stacked_vectors.DualUpperSlacks();
}

template <typename Real>
void Certificate<Real>::PrintNorms(std::ostream& os) const {
  os << "|| x ||_2 = " << EuclideanNorm(PrimalSolution())
     << ", || x ||_max = " << MaxNorm(PrimalSolution()) << "\n"
     << "|| y ||_2 = " << EuclideanNorm(DualSolution())
     << ", || y ||_max = " << MaxNorm(DualSolution()) << "\n"
     << "|| z ||_2 = " << EuclideanNorm(DualLowerSlack())
     << ", || z ||_max = " << MaxNorm(DualLowerSlack()) << "\n"
     << "|| w ||_2 = " << EuclideanNorm(DualUpperSlack())
     << ", || w ||_max = " << MaxNorm(DualUpperSlack()) << std::endl;
}

template <typename Real>
void Certificate<Real>::EnsureInPositiveOrthant(
    const Problem<Real>& problem) const {
#ifdef CONIC_DEBUG
  Int num_primal_lower_slack_outside = 0;
  Int num_primal_upper_slack_outside = 0;
  Int num_dual_lower_slack_outside = 0;
  Int num_dual_upper_slack_outside = 0;
  const Int num_primal = PrimalSolution().Height();
  for (Int i = 0; i < num_primal; ++i) {
    const Real primal_solution = PrimalSolution()(i);
    const Real lower_bound = problem.lower_bounds[i];
    const Real upper_bound = problem.upper_bounds[i];
    if (std::isfinite(lower_bound)) {
      if (primal_solution <= lower_bound) {
        ++num_primal_lower_slack_outside;
      }
      if (DualLowerSlack()(i) <= Real(0)) {
        ++num_dual_lower_slack_outside;
      }
    }
    if (std::isfinite(upper_bound)) {
      if (primal_solution >= upper_bound) {
        ++num_primal_upper_slack_outside;
      }
      if (DualUpperSlack()(i) <= Real(0)) {
        ++num_dual_upper_slack_outside;
      }
    }
  }
  CONIC_ASSERT(num_primal_lower_slack_outside == 0,
               "Primal lower slack left interior.");
  CONIC_ASSERT(num_primal_upper_slack_outside == 0,
               "Primal upper slack left interior.");
  CONIC_ASSERT(num_dual_lower_slack_outside == 0,
               "Dual lower slack left interior.");
  CONIC_ASSERT(num_dual_upper_slack_outside == 0,
               "Dual upper slack left interior.");
#endif  // ifdef CONIC_DEBUG
}

template <typename Real>
void Problem<Real>::TightenBounds(bool tighten_infinite_bounds, bool verbose) {
  const Int num_primal = equality_matrix.NumColumns();
  const Int num_dual = equality_matrix.NumRows();

  FormEqualityMatrixTranspose();

  Buffer<MatrixEntry<Real>>& entries = equality_matrix.Entries();
  Buffer<MatrixEntry<Real>>& transpose_entries =
      equality_matrix_transpose.Entries();

  // The maximum number of cascading variable bound tightening steps.
  // TODO(Jack Poulson): Make this an argument to the routine.
  const Int max_iterations = 5;

  Buffer<bool> active_pattern(num_dual, true);
  Buffer<bool> new_active_pattern(num_dual, false);
  for (Int iter = 0; iter < max_iterations; ++iter) {
    bool done_tightening = true;
    for (Int i = 0; i < num_dual; ++i) {
      if (!active_pattern[i]) {
        continue;
      }
      const Int row_offset = equality_matrix.RowEntryOffset(i);
      const Int row_size = equality_matrix.NumRowEntries(i);
      for (Int row_index = 0; row_index < row_size; ++row_index) {
        const MatrixEntry<Real>& entry = entries[row_offset + row_index];
        const Int j = entry.column;
        if (j >= num_primal - num_fixed) {
          // We do not attempt to tighten the bounds of fixed variables.
          continue;
        }
        if (entry.value == Real(0)) {
          // We cannot divide by this value to enforce a bound.
          continue;
        }
        const bool positive_i_j = entry.value > Real(0);

        // We will modify this if either the upper bound or lower bound
        // tightening procedures make progress.
        bool tightened_bound = false;

        if (tighten_infinite_bounds || std::isfinite(upper_bounds[j])) {
          // Denoting the entry pattern of row i with entry j removed as the set
          // P_{i, j} subset {0, 1, ..., num_primal - 1}, we have
          //
          //   x_j <= (b_i - sum_{k in P_{i, j}} a_{i, k} t_{i, j, k}) / a_{i,
          //   j},
          //
          // where t_{i, j, k} = u_k,  if a_{i, j} > 0 xor a_{i, k} > 0,
          //                     l_k,  otherwise.
          //
          bool infinite_upper_bound = false;
          Real upper_bound_candidate = equality_rhs(i);
          for (Int other_index = 0; other_index < row_size; ++other_index) {
            if (other_index == row_index) {
              continue;
            }
            const MatrixEntry<Real>& other_entry =
                entries[row_offset + other_index];
            const Int k = other_entry.column;
            const bool positive_i_k = other_entry.value > Real(0);
            const bool use_upper_bound = positive_i_j ^ positive_i_k;
            const Real bound =
                use_upper_bound ? upper_bounds[k] : lower_bounds[k];
            if (!std::isfinite(bound)) {
              infinite_upper_bound = true;
              break;
            }
            upper_bound_candidate -= other_entry.value * bound;
          }
          if (!infinite_upper_bound) {
            upper_bound_candidate /= entry.value;
            if (upper_bound_candidate < upper_bounds[j]) {
              if (verbose) {
                std::cout << "Tightened upper bound of [" << lower_bounds[j]
                          << ", " << upper_bounds[j] << "] to "
                          << upper_bound_candidate << std::endl;
              }
              upper_bounds[j] = upper_bound_candidate;
              tightened_bound = true;
            }
          }
        }

        if (tighten_infinite_bounds || std::isfinite(lower_bounds[j])) {
          // Try to tighten the lower bound.
          bool infinite_lower_bound = false;
          Real lower_bound_candidate = equality_rhs(i);
          for (Int other_index = 0; other_index < row_size; ++other_index) {
            if (other_index == row_index) {
              continue;
            }
            const MatrixEntry<Real>& other_entry =
                entries[row_offset + other_index];
            const Int k = other_entry.column;
            const bool positive_i_k = other_entry.value > Real(0);
            const bool use_lower_bound = positive_i_j ^ positive_i_k;
            const Real bound =
                use_lower_bound ? lower_bounds[k] : upper_bounds[k];
            if (!std::isfinite(bound)) {
              infinite_lower_bound = true;
              break;
            }
            lower_bound_candidate -= other_entry.value * bound;
          }
          if (!infinite_lower_bound) {
            lower_bound_candidate /= entry.value;
            if (lower_bound_candidate > lower_bounds[j]) {
              if (verbose) {
                std::cout << "Tightened lower bound of [" << lower_bounds[j]
                          << ", " << upper_bounds[j] << "] to "
                          << lower_bound_candidate << std::endl;
              }
              lower_bounds[j] = lower_bound_candidate;
              tightened_bound = true;
            }
          }
        }

        if (tightened_bound) {
          // We either tightened the lower or upper bound of variable j, so any
          // variable interacted with in a row containing variable j could
          // potentially have its bounds newly tightened.
          done_tightening = false;
          const Int column_offset = equality_matrix_transpose.RowEntryOffset(j);
          const Int column_size = equality_matrix_transpose.NumRowEntries(j);
          for (Int col_index = 0; col_index < column_size; ++col_index) {
            const MatrixEntry<Real>& trans_entry =
                transpose_entries[column_offset + col_index];
            new_active_pattern[trans_entry.column] = true;
          }
        }
      }
    }
    if (done_tightening) {
      break;
    }

    active_pattern = new_active_pattern;
    new_active_pattern.Resize(num_dual, false);
  }
}

template <typename Real>
std::ostream& operator<<(std::ostream& os,
                         const Certificate<Real>& certificate) {
  os << "x=[\n"
     << certificate.PrimalSolution() << "];\n"
     << "y=[\n"
     << certificate.DualSolution() << "];\n"
     << "z=[\n"
     << certificate.DualLowerSlack() << "];\n"
     << "w=[\n"
     << certificate.DualUpperSlack() << "];" << std::endl;
  return os;
}

}  // namespace bounded_qp
}  // namespace conic

#endif  // ifndef CONIC_BOUNDED_QP_PROBLEM_IMPL_H_
