/*
 * Copyright (c) 2019 Jack Poulson <jack@hodgestar.com>
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
#ifndef CONIC_BOUNDED_QP_STACKED_PRIMAL_DUAL_VECTORS_IMPL_H_
#define CONIC_BOUNDED_QP_STACKED_PRIMAL_DUAL_VECTORS_IMPL_H_

#include "conic/bounded_qp/stacked_primal_dual_vectors.hpp"

namespace conic {
namespace bounded_qp {

template <typename Real>
StackedPrimalDualVectors<Real>::StackedPrimalDualVectors() {
  Resize(0, 0, 1);
}

template <typename Real>
StackedPrimalDualVectors<Real>::StackedPrimalDualVectors(Int num_primal,
                                                         Int num_dual,
                                                         Int num_directions) {
  Resize(num_primal, num_dual, num_directions);
}

template <typename Real>
StackedPrimalDualVectors<Real>::StackedPrimalDualVectors(
    const StackedPrimalDualVectors<Real>& vectors) {
  *this = vectors;
}

template <typename Real>
StackedPrimalDualVectors<Real>& StackedPrimalDualVectors<Real>::operator=(
    const StackedPrimalDualVectors<Real>& vectors) {
  if (this == &vectors) {
    return *this;
  }
  const Int num_primal = vectors.NumPrimal();
  const Int num_dual = vectors.NumDual();
  const Int num_directions = vectors.NumDirections();
  Resize(num_primal, num_dual, num_directions);
  Copy(vectors.AllComponents(), &all_components_);

  return *this;
}

template <typename Real>
Int StackedPrimalDualVectors<Real>::NumPrimal() const {
  return primal_solutions_.Height();
}

template <typename Real>
Int StackedPrimalDualVectors<Real>::NumDual() const {
  return dual_solutions_.Height();
}

template <typename Real>
Int StackedPrimalDualVectors<Real>::NumDirections() const {
  return primal_solutions_.Width();
}

template <typename Real>
void StackedPrimalDualVectors<Real>::Resize(Int num_primal, Int num_dual,
                                            Int num_directions) {
  const Int total_size = 3 * num_primal + num_dual;
  directions_.Resize(total_size, num_directions);

  all_components_ = directions_.View();
  const_all_components_ = all_components_.ToConst();

  primal_solutions_ = directions_.Submatrix(0, 0, num_primal, num_directions);
  const_primal_solutions_ = primal_solutions_.ToConst();

  dual_solutions_ =
      directions_.Submatrix(num_primal, 0, num_dual, num_directions);
  const_dual_solutions_ = dual_solutions_.ToConst();

  dual_lower_slacks_ = directions_.Submatrix(num_primal + num_dual, 0,
                                             num_primal, num_directions);
  const_dual_lower_slacks_ = dual_lower_slacks_.ToConst();

  dual_upper_slacks_ = directions_.Submatrix(2 * num_primal + num_dual, 0,
                                             num_primal, num_directions);
  const_dual_upper_slacks_ = dual_upper_slacks_.ToConst();

  augmented_system_components_ =
      directions_.Submatrix(0, 0, num_primal + num_dual, num_directions);
  const_augmented_system_components_ = augmented_system_components_.ToConst();
}

template <typename Real>
BlasMatrixView<Real>& StackedPrimalDualVectors<Real>::AllComponents() {
  return all_components_;
}

template <typename Real>
const ConstBlasMatrixView<Real>& StackedPrimalDualVectors<Real>::AllComponents()
    const {
  return const_all_components_;
}

template <typename Real>
BlasMatrixView<Real>& StackedPrimalDualVectors<Real>::PrimalSolutions() {
  return primal_solutions_;
}

template <typename Real>
const ConstBlasMatrixView<Real>&
StackedPrimalDualVectors<Real>::PrimalSolutions() const {
  return const_primal_solutions_;
}

template <typename Real>
BlasMatrixView<Real>& StackedPrimalDualVectors<Real>::DualSolutions() {
  return dual_solutions_;
}

template <typename Real>
const ConstBlasMatrixView<Real>& StackedPrimalDualVectors<Real>::DualSolutions()
    const {
  return const_dual_solutions_;
}

template <typename Real>
BlasMatrixView<Real>& StackedPrimalDualVectors<Real>::DualLowerSlacks() {
  return dual_lower_slacks_;
}

template <typename Real>
const ConstBlasMatrixView<Real>&
StackedPrimalDualVectors<Real>::DualLowerSlacks() const {
  return const_dual_lower_slacks_;
}

template <typename Real>
BlasMatrixView<Real>& StackedPrimalDualVectors<Real>::DualUpperSlacks() {
  return dual_upper_slacks_;
}

template <typename Real>
const ConstBlasMatrixView<Real>&
StackedPrimalDualVectors<Real>::DualUpperSlacks() const {
  return const_dual_upper_slacks_;
}

template <typename Real>
BlasMatrixView<Real>&
StackedPrimalDualVectors<Real>::AugmentedSystemComponents() {
  return augmented_system_components_;
}

template <typename Real>
const ConstBlasMatrixView<Real>&
StackedPrimalDualVectors<Real>::AugmentedSystemComponents() const {
  return const_augmented_system_components_;
}

}  // namespace bounded_qp
}  // namespace conic

#endif  // ifndef CONIC_BOUNDED_QP_STACKED_PRIMAL_DUAL_VECTORS_IMPL_H_
