/*
 * Copyright (c) 2019 Jack Poulson <jack@hodgestar.com>
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
#ifndef CONIC_BOUNDED_QP_STACKED_PRIMAL_DUAL_VECTORS_H_
#define CONIC_BOUNDED_QP_STACKED_PRIMAL_DUAL_VECTORS_H_

#include "conic/imports.hpp"

namespace conic {
namespace bounded_qp {

template <typename Real>
struct StackedPrimalDualVectors {
  // A default constructor which assumes all zero vector lengths.
  StackedPrimalDualVectors();

  // Initializes primal-dual estimates of the given sizes.
  StackedPrimalDualVectors(Int num_primal, Int num_dual,
                           Int num_directions = 1);

  // A copy constructor.
  StackedPrimalDualVectors(const StackedPrimalDualVectors<Real>& vectors);

  // A copy operation.
  StackedPrimalDualVectors<Real>& operator=(
      const StackedPrimalDualVectors<Real>& vectors);

  void Resize(Int num_primal, Int num_dual, Int num_directions = 1);

  // Returns the number of primal variables.
  Int NumPrimal() const;

  // Returns the number of dual variables.
  Int NumDual() const;

  // Returns the number of stacked vectors.
  Int NumDirections() const;

  // Returns a view of the full concatenated set of components.
  BlasMatrixView<Real>& AllComponents();
  const ConstBlasMatrixView<Real>& AllComponents() const;

  // Returns a view of the primal solution component of the direction.
  BlasMatrixView<Real>& PrimalSolutions();
  const ConstBlasMatrixView<Real>& PrimalSolutions() const;

  // Returns a view of the dual solution component of the direction.
  BlasMatrixView<Real>& DualSolutions();
  const ConstBlasMatrixView<Real>& DualSolutions() const;

  // Returns a view of the dual lower slack component of the direction.
  BlasMatrixView<Real>& DualLowerSlacks();
  const ConstBlasMatrixView<Real>& DualLowerSlacks() const;

  // Returns a view of the dual upper slack component of the direction.
  BlasMatrixView<Real>& DualUpperSlacks();
  const ConstBlasMatrixView<Real>& DualUpperSlacks() const;

  // Returns a view of the concatenation of the primal and dual solutions.
  BlasMatrixView<Real>& AugmentedSystemComponents();
  const ConstBlasMatrixView<Real>& AugmentedSystemComponents() const;

 private:
  // The underlying storage mechanism. The variables are stored in the order
  //   1. primal_solution,
  //   2. dual_solution,
  //   3. dual_lower_slack, and
  //   4. dual_upper_slack,
  // so that 1--2 contiguously comprise the 'augmented system' components.
  BlasMatrix<Real> directions_;

  // A view into the concatenation of all components.
  BlasMatrixView<Real> all_components_;
  ConstBlasMatrixView<Real> const_all_components_;

  // A view into 'directions_' of the primal solution vectors.
  BlasMatrixView<Real> primal_solutions_;
  ConstBlasMatrixView<Real> const_primal_solutions_;

  // A view into 'directions_' of the dual solution vectors.
  BlasMatrixView<Real> dual_solutions_;
  ConstBlasMatrixView<Real> const_dual_solutions_;

  // A view into 'directions_' of the dual lower slack vectors.
  BlasMatrixView<Real> dual_lower_slacks_;
  ConstBlasMatrixView<Real> const_dual_lower_slacks_;

  // A view into 'directions_' of the dual upper slack vectors.
  BlasMatrixView<Real> dual_upper_slacks_;
  ConstBlasMatrixView<Real> const_dual_upper_slacks_;

  // A view into 'directions_' of the concatenated primal and dual solutions.
  BlasMatrixView<Real> augmented_system_components_;
  ConstBlasMatrixView<Real> const_augmented_system_components_;
};

}  // namespace bounded_qp
}  // namespace conic

#include "conic/bounded_qp/stacked_primal_dual_vectors-impl.hpp"

#endif  // ifndef CONIC_BOUNDED_QP_STACKED_PRIMAL_DUAL_VECTORS_H_
