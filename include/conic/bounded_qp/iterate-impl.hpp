/*
 * Copyright (c) 2019 Jack Poulson <jack@hodgestar.com>
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
#ifndef CONIC_BOUNDED_QP_ITERATE_IMPL_H_
#define CONIC_BOUNDED_QP_ITERATE_IMPL_H_

#include <exception>
#include <limits>

#include "conic/bounded_qp/iterate.hpp"

namespace conic {
namespace bounded_qp {

template <typename Real>
BlasMatrixView<Real>& Iterate<Real>::PrimalSolution() {
  return stacked_vectors.PrimalSolutions();
}

template <typename Real>
const ConstBlasMatrixView<Real>& Iterate<Real>::PrimalSolution() const {
  return stacked_vectors.PrimalSolutions();
}

template <typename Real>
BlasMatrixView<Real>& Iterate<Real>::DualSolution() {
  return stacked_vectors.DualSolutions();
}

template <typename Real>
const ConstBlasMatrixView<Real>& Iterate<Real>::DualSolution() const {
  return stacked_vectors.DualSolutions();
}

template <typename Real>
BlasMatrixView<Real>& Iterate<Real>::DualLowerSlack() {
  return stacked_vectors.DualLowerSlacks();
}

template <typename Real>
const ConstBlasMatrixView<Real>& Iterate<Real>::DualLowerSlack() const {
  return stacked_vectors.DualLowerSlacks();
}

template <typename Real>
BlasMatrixView<Real>& Iterate<Real>::DualUpperSlack() {
  return stacked_vectors.DualUpperSlacks();
}

template <typename Real>
const ConstBlasMatrixView<Real>& Iterate<Real>::DualUpperSlack() const {
  return stacked_vectors.DualUpperSlacks();
}

template <typename Real>
void Iterate<Real>::PrintNorms(std::ostream& os) const {
  os << "|| x ||_2 = " << static_cast<double>(EuclideanNorm(PrimalSolution()))
     << ", || x ||_max = " << static_cast<double>(MaxNorm(PrimalSolution()))
     << "\n"
     << "|| y ||_2 = " << static_cast<double>(EuclideanNorm(DualSolution()))
     << ", || y ||_max = " << static_cast<double>(MaxNorm(DualSolution()))
     << "\n"
     << "|| z ||_2 = " << static_cast<double>(EuclideanNorm(DualLowerSlack()))
     << ", || z ||_max = " << static_cast<double>(MaxNorm(DualLowerSlack()))
     << "\n"
     << "|| w ||_2 = " << static_cast<double>(EuclideanNorm(DualUpperSlack()))
     << ", || w ||_max = " << static_cast<double>(MaxNorm(DualUpperSlack()))
     << "\n"
     << "certificate_type = " << certificate_type << std::endl;
}

template <typename Real>
std::ostream& operator<<(std::ostream& os, const Iterate<Real>& iterate) {
  os << "x=[\n"
     << iterate.PrimalSolution() << "];\n"
     << "y=[\n"
     << iterate.DualSolution() << "];\n"
     << "z=[\n"
     << iterate.DualLowerSlack() << "];\n"
     << "w=[\n"
     << iterate.DualUpperSlack() << "];" << std::endl;
  return os;
}

}  // namespace bounded_qp
}  // namespace conic

#endif  // ifndef CONIC_BOUNDED_QP_ITERATE_IMPL_H_
