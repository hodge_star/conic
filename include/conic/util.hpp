/*
 * Copyright (c) 2019 Jack Poulson <jack@hodgestar.com>
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
#ifndef CONIC_UTIL_H_
#define CONIC_UTIL_H_

#include "conic/imports.hpp"

namespace conic {

// Configuration parameters for determining the step size for an update
// direction.
template <typename Real>
struct StepSizeConfig {
  // The maximum allowed step size.
  Real max_step_size = 1;

  // The percentage of step towards the boundary to take for initial proposal.
  // This value is recommended by Xu, Hung, and Ye in "A simplified homogeneous
  // and self-dual linear programming algorithm and its implementation".
  //
  // We use the formula 1 - step_ratio_coefficient * eps^{step_ratio_exponent}.
  Real step_ratio_coefficient = Real(1);
  Real step_ratio_exponent = Real(0.2685);

  // Each time a proposed step leads to an unacceptably high complement ratio,
  // the step length is multiplied by this 'backoff' ratio and remeasured.
  Real backoff_ratio = 0.99;

  // The backoff process is ended if the step size becomes less than this value.
  Real backoff_exit = 1e-4;

  // The maximum allowed complement ratio for the step.
  // TODO(Jack Poulson): Tune this value over the NETLIB LP test suite.
  Real max_allowed_complement_ratio = 6000;

  // The maximum allowed complement is further allowed to be this small
  // multiple of the current complement ratio.
  Real allowed_complement_ratio_multiple = 1.1;

  // The maximum imbalance ratio between the primal and dual step sizes.
  Real max_step_imbalance = 100;

  // If true, step size computation information is printed.
  bool verbose = false;
};

// Adds a scalar multiple of 'update' onto the current iterate.
template <typename Real>
void Axpy(Real scale, const ConstBlasMatrixView<Real>& update,
          BlasMatrixView<Real>* state);

// Returns the number of entries of a real vector that are not strictly
// positive.
template <typename Real>
Int NumNonpositive(const ConstBlasMatrixView<Real>& slack);

}  // namespace conic

#include "conic/util-impl.hpp"

#endif  // ifndef CONIC_UTIL_H_
