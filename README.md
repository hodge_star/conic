[**conic**](https://hodgestar.com/conic/) is a

[C++14](https://en.wikipedia.org/wiki/C%2B%2B14), header-only implementation of
a homogeneous self-dual interior point method for convex Quadratic Programming.
The supported precisions are `float`, `double`, and their double-mantissa
extensions.

[![Join the chat at https://gitter.im/hodge_star/community](https://badges.gitter.im/Join%20Chat.svg)](https://gitter.im/hodge_star/community?utm_source=badge&utm_medium=badge&utm_campaign=pr-badge&utm_content=badge)

### Dependencies
The only strict dependencies for manually including the headers in your project
are:

* [mantis](https://gitlab.com/hodge_star/mantis): A C++14 header-only,
MPL-licensed, implementation of real and complex double-mantissa arithmetic.

* [quotient](https://gitlab.com/hodge_star/quotient): A C++14 header-only,
MPL-licensed, implementation of the (Approximate) Minimum Degree reordering
method.

* [catamari](https://gitlab.com/hodge_star/catamari): A C++14 header-only,
MPL-licensed, implementation of sparse-direct LDL factorizations and
factorization-based Determinantal Point Process samplers.

But, if you would like to make use of the project's build system, the only
strict dependency is:

* [meson](http://mesonbuild.com): "Meson is an open source build system meant
to be both extremely fast, and, even more importantly, as user friendly as
possible." 

Meson will automatically install
[catamari](https://gitlab.com/hodge_star/catamari),
[quotient](https://gitlab.com/hodge_star/quotient),
[Catch2](https://github.com/catchorg/Catch2) (a header-only C++ unit-testing
library), and [specify](https://gitlab.com/hodge_star/specify)
(a C++14 header-only, command-line argument processor).

Further, it is strongly recommended that one have optimized implementations of
the Basic Linear Algebra Subprograms (BLAS) and the Linear Algebra PACKage
(LAPACK), such as [OpenBLAS](https://www.openblas.net),
[BLIS](https://github.com/flame/blis), or a proprietary alternative such as
[Intel MKL](https://software.intel.com/en-us/mkl).

### Quickstart
`conic` is built on top of the library
[catamari](https://gitlab.com/hodge_star/catamari), and so its quickstart
instructions are very similar to
[those of catamari](https://hodgestar.com/catamari/docs/master/quickstart.html).

`conic` contains a templated, header-only implementation of a homogeneous
self-dual interior point method for convex quadratic programming. A quadratic
program in standard form:

```math
\arg\inf_{x}\, \{\, c' x + \frac{1}{2} x' Q x : A x = b, x \ge 0 \,\},
```

can be specified using the template class `conic::hsd_qp::Problem<Real>`,
which has members:

* `linear_objective`: The `Real`-valued column vector
(`catamari::BlasMatrix<Real>`), $`c`$,

* `quadratic_objective`: The `Real`-valued sparse, symmetric quasi-semidefinite
matrix (`catamari::CoordinateMatrix<Real>`), $`Q`$,

* `equality_matrix`: The `Real`-valued sparse matrix (`catamari::CoordinateMatrix<Real>`), $`A`$,

* `equality_rhs`: The `Real`-valued column vector (`catamari::BlasMatrix<Real>`), $`b`$.

A utility class, [conic::BoundedToStandardQPReduction](https://gitlab.com/hodge_star/conic/blob/master/include/conic/bounded_to_standard_qp_reduction.hpp),
is provided for reducing "bounded" Quadratic Programs of the form
```math
\arg\inf_{x}\, \{\, c' x + \frac{1}{2} x' Q x : A x = b, l \le x \le u \,\},
```
into standard form.

As an example of manually forming a standard-form quadratic program, consider
the implementation of a Delsarte upper bound for the number of code words in a
linear code over $`\mathbf{F}_q^n`$ with a given minimum distance:

```cpp
namespace conic {
namespace hsd_qp {

template <typename Real>
void DelsarteBoundProblem(Int field_order, Int num_elements, Int distance,
                          Problem<Real>* problem) {
  // Recall that we can express this linear program in standard form by
  // introducing (n + 1) slacks for the nontrivial inequality constraints, s_i,
  // i = 0, ..., n, and posing the linear program
  //
  //   arginf_x { c' x : A x = b, x >= 0 },
  //
  // where
  //
  //  x = | w_{d:n} |,  c = | -1 |,  A = | (K_i(j; n))_{i=0:n, j=d:n}, -I |,
  //      | s_{0:n} |       |  0 |
  //
  //  b = | (-K_i(0; n))_{i=0:n} |.
  //
  // The entries of w correspond to distance distribution values, the s vector
  // to slacks, and K_i(j; n) is an evaluation of a degree i Kravchuk
  // polynomial.
  //

  const Int num_dist_bounds = num_elements + 1 - distance;
  const Int num_slack = num_elements + 1;
  const Int num_primal = num_dist_bounds + num_slack;
  const Int num_dual = num_slack;

  // Fill the linear objective as negative one over the distance distribution
  // variables and zero over the slack variables.
  problem->linear_objective.Resize(num_primal, 1, Real(0));
  for (Int i = 0; i < num_dist_bounds; ++i) {
    problem->linear_objective(i) = Real(-1);
  }

  // Set the quadratic portion of the objective to zero.
  problem->quadratic_objective.Resize(num_primal, num_primal);

  // Fill the Kravchuk polynomial and negative identity components of the
  // equality matrix.
  problem->equality_matrix.Resize(num_dual, num_primal);
  problem->equality_matrix.ReserveEntryAdditions(num_dual * num_dist_bounds +
                                                 num_dual);
  for (Int i = 0; i < num_dual; ++i) {
    for (Int j = 0; j < num_dist_bounds; ++j) {
      const Real value =
          KravchukPolynomial<Real>(field_order, num_elements, i, j + distance);
      problem->equality_matrix.QueueEntryAddition(i, j, value);
    }
  }
  for (Int i = 0; i < num_dual; ++i) {
    problem->equality_matrix.QueueEntryAddition(i, i + num_dist_bounds,
                                                Real(-1));
  }
  problem->equality_matrix.FlushEntryQueues();

  // Fill the equality right-hand side vector, which results from moving the
  // interaction of the first-column of the MacWilliams identity to the
  // right-hand side since the zero-distance distribution component is
  // identically one.
  problem->equality_rhs.Resize(num_dual, 1);
  for (Int i = 0; i < num_dual; ++i) {
    problem->equality_rhs(i) =
        -KravchukPolynomial<Real>(field_order, num_elements, i, 0);
  }
}

}  // namespace hsd_qp
}  // namespace conic
```

After formation of such a problem in standard form, a solution can be sought
using the routine `conic::hsd_qp::Solve`, which produces an
`conic::hsd_qp::Iterate<Real>` result. The members are:

* `certificate_type`: Set to either `conic::hsd_qp::OPTIMALITY_CERTIFICATE`, `conic::hsd_qp::PRIMAL_INFEASIBILITY_CERTIFICATE`, `conic::hsd_qp::DUAL_INFEASIBILITY_CERTIFICATE`, `conic::hsd_qp::INFEASIBILITY_CERTIFICATE`, or `conic::hsd_qp::INVALID_CERTIFICATE`.

* `PrimalSlack()`: If an approximately optimal solution was found, a
`catamari::BlasMatrixView<Real>` containing the scaled
(by $`\tau`$) primal solution vector, $`x`$. This vector should be
divided by $`\tau`$ to produce the unscaled primal solution.

* `DualSolution()`: If an approximately optimal solution was found, a
`catamari::BlasMatrixView<Real>` containing the scaled (by $`\tau`$) dual
solution vector, $`y`$. This vector should be divided by $`\tau`$ to
produce the unscaled dual solution.

* `DualSlack()`: If an approximately optimal solution was found, a
`catamari::BlasMatrixView<Real>` containing the scaled (by $`\tau`$) dual
slack vector, $`z`$. This vector should be divided by $`\tau`$ to
produce the unscaled dual slack.

* `Tau()`: The primal scaling variable, $`\tau`$.

* `Kappa()`: The dual scaling variable, $`\kappa`$.

* `stacked_vectors`: The vertical concatenation of the primal solution, dual
solution, $`\tau`$, dual slack, and $`\kappa`$ variables, contained in
a `conic::hsd_qp::StackedPrimalDualVector<Real>` class.

An example of solving a solution and making use of the first several components
of its scaled primal solution vector -- to determine an upper bound on code
word size -- is given as follows:

```cpp
namespace conic {
namespace hsd_qp {

template <typename Real>
Real DelsarteBound(Int field_order, Int num_elements, Int distance,
                   const SolveControl<Real>& control) {
  Problem<Real> problem;
  DelsarteBoundProblem(field_order, num_elements, distance, &problem);

  Iterate<Real> iterate;
  Solve(problem, control, &iterate);
  if (iterate.certificate_type != OPTIMALITY_CERTIFICATE) {
    std::cerr << "Unable to compute Delsarte bound: "
              << iterate.certificate_type << std::endl;
    return Real(-1);
  }

  const ConstBlasMatrixView<Real> primal_solution = iterate.PrimalSolution();
  const Real tau = iterate.Tau();

  Real upper_bound = 1;
  for (Int i = 0; i < num_elements - distance + 1; ++i) {
    upper_bound += primal_solution(i) / tau;
  }

  return upper_bound;
}

}  // namespace hsd_qp
}  // namespace conic
```

### Example drivers
One can load and solve Linear Programs stored in the [Mathematical Programming
System (MPS)](https://en.wikipedia.org/wiki/MPS_(format)) file format using the
[example/hsd_lp_mps.cc](https://gitlab.com/hodge_star/conic/blob/master/example/hsd_lp_mps.cc) example driver.

### Test suite
`conic` has an extensive LP test suite, including the
[feasible NETLIB LP tests](https://www.netlib.org/lp/data/index.html) and
[infeasible NETLIB LP tests](https://www.netlib.org/lp/infeas/index.html),
located in
[test/hsd_lp_lad.cc](https://gitlab.com/hodge_star/conic/blob/master/test/hsd_lp_lad.cc),
[test/hsd_lp_netlib_feasible.cc](https://gitlab.com/hodge_star/conic/blob/master/test/hsd_lp_netlib_feasible.cc),
[test/hsd_lp_netlib_infeasible.cc](https://gitlab.com/hodge_star/conic/blob/master/test/hsd_lp_netlib_infeasible.cc),
[test/hsd_lp_tolem.cc](https://gitlab.com/hodge_star/conic/blob/master/test/hsd_lp_tolem.cc), and [test/hsd_lp_trivial.cc](https://gitlab.com/hodge_star/conic/blob/master/test/hsd_lp_trivial.cc).

There are also QP tests in
[test/bounded_to_standard_qp_reduction.cc](https://gitlab.com/hodge_star/conic/blob/master/test/bounded_to_standard_qp_reduction.cc),
[test/hsd_qp_nnls.cc](https://gitlab.com/hodge_star/conic/blob/master/test/hsd_qp_nnls.cc),
and [test/hsd_qp_trivial.cc](https://gitlab.com/hodge_star/conic/blob/master/test/hsd_qp_trivial.cc).

#### Netlib LP test suite

The number of iterations and relative accuracy for normal-equation and
augmented-system KKT solvers for both IEEE fp64 and its double-mantissa
(fp64-x2) extension (via [mantis](https://gitlab.com/hodge_star/mantis))
with both solution and right-hand side relative error denominators.

##### Iterate normalization

We define the *iterate-normalized* relative error as:

```math
\max\{ \frac{\| c \tau - A^T y - z \|_2}{\|z\|_2 + \tau}, \frac{\| b \tau - A x \|_2}{\| x \|_2 + 1}, \frac{|c^T x - b^T y|}{|b^T y| + \tau}\}.
```

The results from running the
[NETLIB feasible LP tests](https://gitlab.com/hodge_star/conic/blob/master/test/hsd_lp_netlib_feasible.cpp)
in commit
[e649e45a7adeaa9df902b3c4060a33ec6aafd8cb](https://gitlab.com/hodge_star/conic/commit/e649e45a7adeaa9df902b3c4060a33ec6aafd8cb)
are as follows:

| name     | Normal fp64     | Augmented fp64 | Normal fp64-x2 | Augmented fp64-x2 |
|----------|-----------------|----------------|----------------|-------------------|
| 25fv47   | 19 its (4e-10)  | 25 its (6e-9)  | 23 its (2e-18) | 23 its (2e-18)    |
| 80bau3b  | 42 its (5e-9)   | 44 its (8e09)  | 39 its (4e-20) | 38 its (4e-20)    |
| adlittle | 12 its (2e-15)  | 10 its (2e-10) | 17 its (8e-22) | 15 its (3e-19)    |
| afiro    | 8 its (1e-13)   | 14 its (4e-24) | 14 its (4e-24) | 14 its (4e-24)    |
| agg      | 19 its (2e-11)  | 27 its (1e-15) | 22 its (1e-22) | 22 its (1e-22)    |
| agg2     | 19 its (3e-14)  | 25 its (3e-12) | 23 its (1e-23) | 21 its (1e-17)    |
| agg3     | 19 its (2e-14)  | 26 its (2e-12) | 22 its (1e-22) | 21 its (1e-19)    |
| bandm    | 13 its (1e-14)  | 16 its (6e-11) | 18 its (4e-23) | 17 its (3e-22)    |
| beaconfd | 11 its (4e-14)  | 20 its (9e-14) | 16 its (8e-24) | 14 its (1e-21)    |
| blend    | 10 its (7e-12)  | 17 its (1e-12) | 15 its (5e-24) | 14 its (9e-21)    |
| bnl1     | 30 its (4e-10)  | 36 its (3e-11) | 39 its (1e-20) | 40 its (1e-20)    |
| bnl2     | 27 its (2e-12)  | 29 its (7e-13) | 24 its (2e-20) | 27 its (2e-20)    |
| boeing1  | 24 its (1e-13)  | 16 its (4e-12) | 26 its (2e-21) | 20 its (3e-21)    |
| boeing2  | 16 its (4e-15)  | 15 its (4e-12) | 22 its (2e-20) | 20 its (2e-20)    |
| bore3d   | 18 its (5e-13)  | 16 its (3e-16) | 19 its (3e-23) | 22 its (1e-25)    |
| brandy   | 12 its (1e-10)  | 18 its (3e-9)  | 18 its (1e-22) | 17 its (2e-20)    |
| capri    | 22 its (2e-11)  | 23 its (4e-11) | 22 its (2e-22) | 22 its (2e-22)    |
| cycle    | 21 its (7e-9)   | 31 its (1e-10) | 25 its (1e-20) | 29 its (6e-23)    |
| czprob   | 24 its (1e-13)  | 35 its (8e-9)  | 32 its (3e-21) | 32 its (2e-19)    |
| d2q06c   | 23 its (9e-11)  | 32 its (2e-11) | 27 its (5e-18) | 29 its (5e-18)    |
| d6cube   | 25 its (2e-12)  | 16 its (7e-12) | 24 its (8e-23) | 24 its (8e-23)    |
| degen2   | 16 its (7e-13)  | 13 its (3e-11) | 16 its (2e-22) | 14 its (2e-22)    |
| degen3   | 21 its (3e-13)  | 20 its (3e-13) | 16 its (4e-19) | 16 its (4e-19)    |
| dfl001   | 54 its (2e-10)  | 49 its (1e-9)  | 46 its (4e-17) | 45 its (1e-18)    |
| e226     | 14 its (7e-15)  | 13 its (8e-11) | 18 its (2e-23) | 18 its (2e-23)    |
| etamacro | 20 its (7e-10)  | 26 its (6e-10) | 24 its (8e-18) | 25 its (1e-20)    |
| fffff800 | 25 its (1e-14)  | 29 its (6e-9)  | 28 its (6e-25) | 26 its (2e-18)    |
| finnis   | 32 its (3e-10)  | 27 its (9e-9)  | 31 its (3e-19) | 29 its (5e-18)    |
| fit1d    | 20 its (2e-13)  | 20 its (2e-13) | 25 its (6e-23) | 25 its (6e-23)    |
| fit1p    | 13 its (9e-15)  | 13 its (1e-14) | 17 its (3e-23) | 17 its (3e-23)    |
| fit2d    | 23 its (1e-12)  | 21 its (3e-10) | 28 its (2e-23) | 28 its (4e-23)    |
| fit2p    | 16 its (4e-12)  | 16 its (4e-12) | 20 its (2e-23) | 21 its (9e-24)    |
| forplan  | 26 its (2e-9)   | 29 its (9e-16) | 28 its (1e-27) | 29 its (1e-30)    |
| ganges   | 22 its (9e-9)   | 24 its (5e-12) | 22 its (4e-16) | 26 its (3e-16)    |
| gfrd-pnc | 12 its (8e-9)   | 18 its (5e-9)  | 18 its (9e-20) | 18 its (9e-20)    |
| greenbea | 61 its (9e-9)   | 43 its (2e-9)  | 44 its (6e-21) | 47 its (7e-19)    |
| greenbeb | 36 its (6e-13)  | 40 its (1e-9)  | 40 its (2e-20) | 37 its (2e-19)    |
| grow15   | 14 its (4e-12)  | 14 its (4e-12) | 19 its (2e-25) | 19 its (2e-25)    |
| grow22   | 17 its (2e-13)  | 17 its (2e-13) | 23 its (3e-30) | 22 its (3e-27)    |
| grow7    | 14 its (1e-14)  | 13 its (6e-11) | 19 its (5e-25) | 19 its (5e-25)    |
| israel   | 17 its (4e-12)  | 17 its (2e-11) | 23 its (3e-26) | 22 its (3e-23)    |
| kb2      | 12 its (1e-12)  | 15 its (3e-11) | 18 its (1e-23) | 18 its (1e-23)    |
| lofti    | 13 its (3e-10)  | 13 its (3e-10) | 17 its (4e-20) | 18 its (6e-23)    |
| maros    | 20 its (2e-10)  | 29 its (7e-12) | 24 its (5e-20) | 28 its (5e-20)    |
| maros-r7 | 14 its (4e-14)  | 14 its (4e-14) | 18 its (5e-22) | 18 its (4e-22)    |
| modszk1  | 25 its (8e-14)  | 26 its (5e-15) | 29 its (1e-18) | 26 its (1e-18)    |
| nesm     | 54 its (9e-10)  | 56 its (2e-10) | 37 its (1e-20) | 37 its (1e-20)    |
| perold   | 32 its (7e-13)  | 37 its (6e-11) | 29 its (1e-21) | 33 its (1e-20)    |
| pilot    | 48 its (3e-9)   | 46 its (4e-11) | 39 its (2e-23) | 43 its (2e-21)    |
| pilot.ja | 33 its (1e-9)   | 70 its (7e-10) | 38 its (1e-14) | 43 its (8e-20)    |
| pilot.we | 32 its (3e-13)  | 39 its (1e-10) | 33 its (6e-24) | 36 its (1e-19)    |
| pilot4   | 31 its (5e-11)  | 40 its (1e-9)  | 33 its (8e-26) | 36 its (3e-20)    |
| pilot87  | 34 its (2e-12)  | 48 its (8e-12) | 47 its (3e-21) | 42 its (6e-19)    |
| pilotnov | 16 its (1e-11)  | 23 its (2e-10) | 22 its (6e-19) | 22 its (3e-20)    |
| recipe   | 8 its (6e-14)   | 8 its (6e-14)  | 13 its (3e-25) | 14 its (5e-25)    |
| sc105    | 10 its (2e-16)  | 13 its (1e-12) | 16 its (2e-29) | 14 its (2e-23)    |
| sc205    | 14 its (2e-16)  | 17 its (3e-12) | 18 its (2e-30) | 15 its (2e-21)    |
| sc50a    | 11 its (2e-16)  | 9 its (2e-11)  | 16 its (4e-28) | 16 its (4e-28)    |
| sc50b    | 10 its (6e-16)  | 10 its (5e-12) | 15 its (3e-27) | 14 its (4e-24)    |
| scagr25  | 16 its (3e-11)  | 20 its (6e-15) | 21 its (6e-25) | 21 its (6e-25)    |
| scagr7   | 14 its (5e-12)  | 15 its (5e-12) | 19 its (6e-26) | 19 its (6e-26)    |
| scfxm1   | 14 its (7e-13)  | 20 its (4e-12) | 19 its (1e-25) | 20 its (6e-24)    |
| scfxm2   | 16 its (2e-13)  | 23 its (6e-11) | 21 its (5e-22) | 18 its (2e-19)    |
| scfxm3   | 16 its (4e-13)  | 21 its (2e-10) | 21 its (4e-22) | 20 its (4e-20)    |
| scorpion | 16 its (6e-12)  | 14 its (1e-11) | 15 its (9e-18) | 15 its (9e-18)    |
| scrs8    | 17 its (8e-14)  | 23 its (2e-13) | 22 its (9e-23) | 22 its (9e-23)    |
| scsd1    | 8 its (6e-15)   | 8 its (6e-15)  | 13 its (3e-25) | 13 its (3e-25)    |
| scsd6    | 13 its (6e-13)  | 13 its (1e-12) | 16 its (2e-18) | 16 its (2e-18)    |
| scsd8    | 9 its (1e-15)   | 9 its (1e-15)  | 13 its (6e-24) | 13 its (6e-24)    |
| sctap1   | 12 its (4e-11)  | 12 its (4e-11) | 17 its (3e-23) | 17 its (3e-23)    |
| sctap2   | 9 its (2e-13)   | 9 its (2e-13)  | 14 its (4e-24) | 14 its (4e-24)    |
| sctap3   | 11 its (9e-14)  | 11 its (9e-14) | 16 its (5e-25) | 16 its (5e-25)    |
| seba     | 17 its (7e-12)  | 18 its (9e-16) | 21 its (1e-22) | 23 its (1e-24)    |
| share1b  | 20 its (2e-11)  | 27 its (5e-9)  | 27 its (4e-32) | 23 its (1e-20)    |
| share2b  | 12 its (4e-14)  | 12 its (3e-10) | 16 its (4e-24) | 15 its (3e-21)    |
| shell    | 22 its (7e-14)  | 27 its (9e-10) | 27 its (4e-20) | 25 its (5e-16)    |
| ship04l  | 11 its (3e-11)  | 11 its (3e-11) | 16 its (4e-19) | 16 its (4e-19)    |
| ship04s  | 15 its (1e-13)  | 13 its (3e-14) | 16 its (6e-19) | 16 its (6e-19)    |
| ship08l  | 14 its (3e-11)  | 14 its (1e-11) | 18 its (2e-21) | 18 its (2e-21)    |
| ship08s  | 12 its (5e-12)  | 13 its (5e-12) | 16 its (7e-21) | 17 its (7e-23)    |
| ship12l  | 19 its (4e-12)  | 18 its (4e-12) | 16 its (2e-19) | 16 its (2e-19)    |
| ship12s  | 13 its (2e-9)   | 12 its (6e-12) | 15 its (1e-19) | 15 its (1e-19)    |
| sierra   | 22 its (1e-9)   | 23 its (6e-9)  | 22 its (1e-17) | 24 its (6e-17)    |
| stair    | 15 its (3e-13)  | 19 its (4e-9)  | 20 its (2e-22) | 18 its (4e-20)    |
| standata | 13 its (1e-15)  | 12 its (2e-14) | 18 its (6e-26) | 17 its (1e-25)    |
| standmps | 17 its (2e-15)  | 16 its (1e-14) | 21 its (1e-25) | 20 its (3e-25)    |
| stocfor1 | 16 its (2e-12)  | 16 its (2e-12) | 15 its (5e-22) | 15 its (5e-22)    |
| stocfor2 | 17 its (1e-10)  | 21 its (1e-10) | 21 its (2e-21) | 22 its (2e-21)    |
| tuff     | 15 its (3e-16)  | 17 its (4e-11) | 18 its (2e-25) | 18 its (1e-21)    |
| vtp.base | 14 its (6e-15)  | 15 its (1e-15) | 19 its (9e-26) | 20 its (6e-26)    |
| wood1p   | 26 its (4e-11)  | 26 its (4e-11) | 25 its (4e-15) | 25 its (4e-15)    |
| woodw    | 21 its (8e-12)  | 21 its (8e-12) | 27 its (3e-23) | 27 its (3e-23)    |

##### Right-hand side normalization

We define the *right-hand-side-normalized* relative error as:

```math
\max\{ \frac{\| c \tau - A^T y - z \|_2}{\|c \tau\|_2 + \tau}, \frac{\| b \tau - A x \|_2}{\| b \tau \|_2 + 1}, \frac{|c^T x - b^T y|}{|b^T y| + \tau}\}.
```

...

### License
`conic` is distributed under the
[Mozilla Public License, v. 2.0](https://www.mozilla.org/media/MPL/2.0/index.815ca599c9df.txt).
