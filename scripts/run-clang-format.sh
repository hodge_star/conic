#!/bin/bash

HEADERS=`find include/ -print | grep "\.h"`
TESTS=`find example/ -print | grep "\.cc"`
TESTS=`find test/ -print | grep "\.cc"`
SOURCE_FILES="${HEADERS} ${EXAMPLES} ${TESTS}"

# Run clang-format on the modified files.
clang-format -i -style=file ${SOURCE_FILES}
